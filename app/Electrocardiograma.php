<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Electrocardiograma extends Model {
    protected $table = 'electrocardiograma';
}