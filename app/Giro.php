<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Giro extends Model
{
    public function empresas() {
    	$this->hasMany('App\Empresa');
    }
}
