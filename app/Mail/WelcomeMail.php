<?php

namespace App\Mail;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $nombreAdmin;
    public $correoAdmin;
    public $passwordAdmin;
    public function __construct(Request $request,$password)
    {
        $this->nombreAdmin = $request->input('nombre_completo');
        $this->correoAdmin = $request->input('correo_contacto');
        $this->passwordAdmin = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->view('Mails.registro')
                ->from("noreply@humanly-sw.com","Health Administration Solution")
                ->subject('Un administrador de Health Administration Solution ha creado una cuenta para usted');
    }
}
