<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeccionFormulario extends Model {
    protected $table = 'seccion_formulario';

    public function preguntas()
    {
        return $this->hasMany(PreguntaFormulario::class, 'seccion_formulario_id', 'id');
    }
}
