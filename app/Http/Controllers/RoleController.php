<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Empresa;
use App\Empleado;
use App\User;
use App\HistorialClinico;
use App\Consulta;
use App\Accesos;
use App\EstudioProgramado;
use App\Estudios;
use App\Audiometria;
use App\EstudioEspirometria;
use App\Electrocardiograma;

class RoleController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	// public function index(Request $request) {
	// 	$user = Auth::user();
	//     $AdminSession = session('Admin');
	// 	if ($user->admin()) {
	//         return redirect('Empresas');
	// 		return view('Administrador/DashboardAdmi');
	// 		// return view('Administrador/DashboardAdmiBackUp');
	// 	} else if ($user->empresa()) {
	// 	$empresa = Empresa::select('id', 'nombre', 'logo')
	// 		->where('user_id', $user->id)
	// 		->first();
	// 		Session::put('empresa', $empresa);
	// 		$consultas = $this->consultas();
	// 		$empleados = $this->personal();
	// 		$femenino = $this->personalFemenino();
	//         $masculino = $this->personalMasculino();
	// 		if ($AdminSession=="") {
	//             return view('Empresa/Empresa',[
	//                 'consultas' =>$consultas,
	//                 'personal' => $empleados,
	//                 'femenino' => $femenino,
	//                 'masculino' => $masculino
	//             ]);
	// 		}else {
	//             return view('Empresa/Empresa',[
	//                 'userEmpresa' => 1,
	//                 //TODO: esto no se si va
	//                 'consultas' =>$consultas,
	//                 'personal' => $empleados,
	//                 'femenino' => $femenino,
	//                 'masculino' => $masculino
	//             ]);
	// 		}

	// 	}
	// 	elseif ($user->role_id!=1 && $user->role_id != 2) {
	// 		foreach ($user->accesos->groupBy('modulo_id') as $key => $value) {
	// 			$permisos = [];
	// 			foreach ($value as $acceso) {
	// 				$permisos[$acceso->AccesoCrud->first()->operacion] = true;
	// 			}
	// 			Session::put($acceso->modulosAccesos->first()->nombre,$permisos);
	// 		}

	// 	$empresa = Empresa::select('id', 'nombre', 'logo')
	// 		->where('id', $user->empresa_id)
	// 		->first();

	// 	Session::put('empresa', $empresa);
	// 	$consultas = $this->consultas();
	// 	$empleados = $this->personal();
	// 	$femenino = $this->personalFemenino();
	// 	$masculino = $this->personalMasculino();
	// 		return view('Empresa/Empresa',[
	// 		'consultas' =>$consultas,
	// 		'personal' => $empleados,
	// 		'femenino' => $femenino,
	// 		'masculino' => $masculino
	// 		]);
	// 	}else {
	// 		return abort(404);
	// 	}
	// }
	public function index(Request $request)
	{
		$user = Auth::user();
		$AdminSession = session('Admin');
		if ($user->admin()) {
			return redirect('Empresas');
			return view('Administrador/DashboardAdmi');

			// return view('Administrador/DashboardAdmiBackUp');
		} else if ($user->empresa()) {
			//$empresa = Empresa::select('id', 'nombre', 'logo')
			//->where('user_id', $user->id)
			//->first();
			$empresa = Empresa::select('*')->first();
			Session::put('empresa', $empresa);
			$consultas = $this->consultas();
			$empleados = $this->personal();
			$femenino = $this->personalFemenino();
			$masculino = $this->personalMasculino();
			$empleadosbase = $this->empleadostotales();
			$audios = $this->audios();
			$espiro = $this->espiro();
			$elect = $this->electro();
			$cigarros = array();
			$alcholes = array();
			$droguitas = array();
			// $g_sanguineos=array();

			foreach ($empleadosbase as $key => $persona) {
				$historia = $this->HistorialClinico($persona->CURP);
				if (!empty($historia)) {
					$nopatologico =  json_decode($historia->noPatologico);
					$cigarro = $nopatologico->cigarro;

					$alchol = $nopatologico->alcohol;
					$droga = $nopatologico->drogas;
					$g_sanguineos = $nopatologico->g_sanguineo;
					if ($cigarro->Cigarro == 'No') {

						array_push($cigarros, $persona->CURP);
					}
					if ($alchol->Alcohol == 'No') {

						array_push($alcholes, $persona->CURP);
					}
					if ($droga->Drogas == 'No') {

						array_push($droguitas, $persona->CURP);
					}
				}
			}

			$empleadost = count($empleadosbase);
			if ($empleadost == 0) {
				$empleadost = 1;
			}
			$ptabaquismo = (100 * (count($cigarros))) / $empleadost;
			$palcholes = (100 * (count($alcholes))) / $empleadost;
			$pdroguitas = (100 * (count($droguitas))) / $empleadost;
			$estrenimiento = array();
			$soplo = array();
			$Hipertension = array();
			$Bronquitis = array();
			$Diabetes = array();
			$Migrana = array();
			$Tuberculosis = array();
			$Gastritis = array();
			$Vesicula = array();
			$Anemia = array();
			$sangre = array();
			$Hepatitis = array();
			$Varicela = array();
			$Fracturas = array();
			$Epilepsia = array();
			$otros = array();
			$rinon = array();
			//MUJERES
			$estrenimientoM = array();
			$soploM = array();
			$HipertensionM = array();
			$BronquitisM = array();
			$DiabetesM = array();
			$MigranaM = array();
			$TuberculosisM = array();
			$GastritisM = array();
			$VesiculaM = array();
			$AnemiaM = array();
			$sangreM = array();
			$HepatitisM = array();
			$VaricelaM = array();
			$FracturasM = array();
			$EpilepsiaM = array();
			$otrosM = array();
			$rinonM = array();

			foreach ($empleadosbase as $key => $persona) {
				$historia = $this->HistorialClinico($persona->CURP);
				
				if($persona->genero=='Femenino'){
					if (!empty($historia)) {

						$patologico =  json_decode($historia->patologico);
						$enfermedades = $patologico->enfermPadecidas;
						if ($enfermedades != null) {
							foreach ($enfermedades as $value) {
								if ($value == "Estreñimiento") {
									array_push($estrenimientoM, $persona->CURP);
								} else if ($value == "Soplo del corazón") {
									array_push($soploM, $persona->CURP);
								} else if ($value == "Hipertensión") {
									array_push($HipertensionM, $persona->CURP);
								} else if ($value == "Bronquitis") {
									array_push($BronquitisM, $persona->CURP);
								} else if ($value == "Diabetes") {
									array_push($DiabetesM, $persona->CURP);
								} else if ($value == "Migraña") {
									array_push($MigranaM, $persona->CURP);
								} else if ($value == "Tuberculosis") {
									array_push($TuberculosisM, $persona->CURP);
								} else if ($value == "Gastritis") {
									array_push($GastritisM, $persona->CURP);
								} else if ($value == "Piedras en la vesícula") {
									array_push($VesiculaM, $persona->CURP);
								} else if ($value == "Piedras en el riñon") {
									array_push($rinonM, $persona->CURP);
								} else if ($value == "Anemia") {
									array_push($AnemiaM, $persona->CURP);
								} else if ($value == "Enfermedades de la sangre") {
									array_push($sangreM, $persona->CURP);
								} else if ($value == "Hepatitis") {
									array_push($HepatitisM, $persona->CURP);
								} else if ($value == "Varicela") {
									array_push($VaricelaM, $persona->CURP);
								} else if ($value == "Fracturas") {
									array_push($FracturasM, $persona->CURP);
								} else if ($value == "Epilepsia") {
									array_push($EpilepsiaM, $persona->CURP);
								} else {
									array_push($otrosM, $persona->CURP);
								}
							}
						}
					}
				}else{
					if (!empty($historia)) {

						$patologico =  json_decode($historia->patologico);
						$enfermedades = $patologico->enfermPadecidas;
						if ($enfermedades != null) {
							foreach ($enfermedades as $value) {
								if ($value == "Estreñimiento") {
									array_push($estrenimiento, $persona->CURP);
								} else if ($value == "Soplo del corazón") {
									array_push($soplo, $persona->CURP);
								} else if ($value == "Hipertensión") {
									array_push($Hipertension, $persona->CURP);
								} else if ($value == "Bronquitis") {
									array_push($Bronquitis, $persona->CURP);
								} else if ($value == "Diabetes") {
									array_push($Diabetes, $persona->CURP);
								} else if ($value == "Migraña") {
									array_push($Migrana, $persona->CURP);
								} else if ($value == "Tuberculosis") {
									array_push($Tuberculosis, $persona->CURP);
								} else if ($value == "Gastritis") {
									array_push($Gastritis, $persona->CURP);
								} else if ($value == "Piedras en la vesícula") {
									array_push($Vesicula, $persona->CURP);
								} else if ($value == "Piedras en el riñon") {
									array_push($rinon, $persona->CURP);
								} else if ($value == "Anemia") {
									array_push($Anemia, $persona->CURP);
								} else if ($value == "Enfermedades de la sangre") {
									array_push($sangre, $persona->CURP);
								} else if ($value == "Hepatitis") {
									array_push($Hepatitis, $persona->CURP);
								} else if ($value == "Varicela") {
									array_push($Varicela, $persona->CURP);
								} else if ($value == "Fracturas") {
									array_push($Fracturas, $persona->CURP);
								} else if ($value == "Epilepsia") {
									array_push($Epilepsia, $persona->CURP);
								} else {
									array_push($otros, $persona->CURP);
								}
							}
						}
					}
				}

				
				// if ($g_sanguineos=='A+') {

				// 	array_push($drogas, $persona->CURP);
				// }
			}
			$audiometrias = $this->estudios('1383');
			$espirometrias = $this->estudios('1382');
			$electro = $this->estudios('1372');
			$audiosnormales = array();
			$audiosanormales = array();
			foreach ($audios as $a) {
				if ($a->tipo_resultado == 'NORMAL') {
					array_push($audiosnormales, $a->curp);
				} else {
					array_push($audiosanormales, $a->curp);
				}
			}
			$espironormales = array();
			$espiroanormales = array();
			foreach ($espiro as $e) {
				if ($e->tipo_resultado == 'NORMAL') {
					array_push($espironormales, $e->curp);
				} else {
					array_push($espiroanormales, $e->curp);
				}
			}
			$electnormal = array();
			$electanormal = array();
			foreach ($elect as $e) {
				if ($e->tipo_resultado == 'NORMAL') {
					array_push($electnormal, $e->curp);
				} else {
					array_push($electanormal, $e->curp);
				}
			}
			if ($AdminSession == "") {
				return view('Empresa/Empresa', [
					'consultas' => $consultas,
					'personal' => $empleados,
					'femenino' => $femenino,
					'masculino' => $masculino,
					'cigarros' => number_format($ptabaquismo, 0),
					'alcholes' => number_format($palcholes, 0),
					'droguitas' => number_format($pdroguitas, 0),
					'estrenimiento' => count($estrenimiento),
					'soplo' => count($soplo),
					'Hipertension' => count($Hipertension),
					'Bronquitis' => count($Bronquitis),
					'Diabetes' => count($Diabetes),
					'Migrana' => count($Migrana),
					'Tuberculosis' => count($Tuberculosis),
					'Gastritis' => count($Gastritis),
					'Vesicula' => count($Vesicula),
					'rinon' => count($rinon),
					'Anemia' => count($Anemia),
					'sangre' => count($sangre),
					'Hepatitis' => count($Hepatitis),
					'Varicela' => count($Varicela),
					'Fracturas' => count($Fracturas),
					'Epilepsia' => count($Epilepsia),
					'otros' => count($otros),
					'estrenimientoM' => count($estrenimientoM),
					'soploM' => count($soploM),
					'HipertensionM' => count($HipertensionM),
					'BronquitisM' => count($BronquitisM),
					'DiabetesM' => count($DiabetesM),
					'MigranaM' => count($MigranaM),
					'TuberculosisM' => count($TuberculosisM),
					'GastritisM' => count($GastritisM),
					'VesiculaM' => count($VesiculaM),
					'rinonM' => count($rinonM),
					'AnemiaM' => count($AnemiaM),
					'sangreM' => count($sangreM),
					'HepatitisM' => count($HepatitisM),
					'VaricelaM' => count($VaricelaM),
					'FracturasM' => count($FracturasM),
					'EpilepsiaM' => count($EpilepsiaM),
					'otrosM' => count($otrosM),
					'audiosnormales' => count($audiosnormales),
					'audiosanormales' => count($audiosanormales),
					'espironormales' => count($espironormales),
					'espiroanormales' => count($espiroanormales),
					'electnormal' => count($electnormal),
					'electanormal' => count($electanormal),
					'audiometrias' => $audiometrias,
					'espirometrias' => $espirometrias,
					'electro' => $electro
				]);
			} else {
				
				return view('Empresa/Empresa', [
					'userEmpresa' => 1,
					//TODO: esto no se si va
					'consultas' => $consultas,
					'personal' => $empleados,
					'femenino' => $femenino,
					'masculino' => $masculino,
					'cigarros' => number_format($ptabaquismo, 0),
					'alcholes' => number_format($palcholes, 0),
					'droguitas' => number_format($pdroguitas, 0),
					'estrenimiento' => count($estrenimiento),
					'soplo' => count($soplo),
					'Hipertension' => count($Hipertension),
					'Bronquitis' => count($Bronquitis),
					'Diabetes' => count($Diabetes),
					'Migrana' => count($Migrana),
					'Tuberculosis' => count($Tuberculosis),
					'Gastritis' => count($Gastritis),
					'Vesicula' => count($Vesicula),
					'rinon' => count($rinon),
					'Anemia' => count($Anemia),
					'sangre' => count($sangre),
					'Hepatitis' => count($Hepatitis),
					'Varicela' => count($Varicela),
					'Fracturas' => count($Fracturas),
					'Epilepsia' => count($Epilepsia),
					'otros' => count($otros),
					'estrenimientoM' => count($estrenimientoM),
					'soploM' => count($soploM),
					'HipertensionM' => count($HipertensionM),
					'BronquitisM' => count($BronquitisM),
					'DiabetesM' => count($DiabetesM),
					'MigranaM' => count($MigranaM),
					'TuberculosisM' => count($TuberculosisM),
					'GastritisM' => count($GastritisM),
					'VesiculaM' => count($VesiculaM),
					'rinonM' => count($rinonM),
					'AnemiaM' => count($AnemiaM),
					'sangreM' => count($sangreM),
					'HepatitisM' => count($HepatitisM),
					'VaricelaM' => count($VaricelaM),
					'FracturasM' => count($FracturasM),
					'EpilepsiaM' => count($EpilepsiaM),
					'otrosM' => count($otrosM),
					'audiosnormales' => count($audiosnormales),
					'audiosanormales' => count($audiosanormales),
					'espironormales' => count($espironormales),
					'espiroanormales' => count($espiroanormales),
					'electnormal' => count($electnormal),
					'electanormal' => count($electanormal),
					'audiometrias' => $audiometrias,
					'espirometrias' => $espirometrias,
					'electro' => $electro
				]);
			}
		} elseif ($user->role_id != 1 && $user->role_id != 2) {
			$empresa = Empresa::select('*')->first();
			Session::put('empresa', $empresa);
			$consultas = $this->consultas();
			$empleados = $this->personal();
			$femenino = $this->personalFemenino();
			$masculino = $this->personalMasculino();
			$empleadosbase = $this->empleadostotales();
			$audios = $this->audios();
			$espiro = $this->espiro();
			$elect = $this->electro();
			$cigarros = array();
			$alcholes = array();
			$droguitas = array();
			// $g_sanguineos=array();

			foreach ($empleadosbase as $key => $persona) {
				$historia = $this->HistorialClinico($persona->CURP);
				if (!empty($historia)) {
					$nopatologico =  json_decode($historia->noPatologico);

					// return dd($nopatologico);
					$cigarro = $nopatologico->cigarro;

					$alchol = $nopatologico->alcohol;
					$droga = $nopatologico->drogas;
					$g_sanguineos = $nopatologico->g_sanguineo;
					if ($cigarro->Cigarro == 'No') {

						array_push($cigarros, $persona->CURP);
					}
					if ($alchol->Alcohol == 'No') {

						array_push($alcholes, $persona->CURP);
					}
					if ($droga->Drogas == 'No') {

						array_push($droguitas, $persona->CURP);
					}
				}
				// if ($g_sanguineos=='A+') {

				// 	array_push($drogas, $persona->CURP);
				// }
			}

			$empleadost = count($empleadosbase);
			$ptabaquismo = (100 * (count($cigarros))) / $empleadost;
			$palcholes = (100 * (count($alcholes))) / $empleadost;
			$pdroguitas = (100 * (count($droguitas))) / $empleadost;
			$estrenimiento = array();
			$soplo = array();
			$Hipertension = array();
			$Bronquitis = array();
			$Diabetes = array();
			$Migrana = array();
			$Tuberculosis = array();
			$Gastritis = array();
			$Vesicula = array();
			$Anemia = array();
			$sangre = array();
			$Hepatitis = array();
			$Varicela = array();
			$Fracturas = array();
			$Epilepsia = array();
			$otros = array();
			$rinon = array();
			//MUJERES
			$estrenimientoM = array();
			$soploM = array();
			$HipertensionM = array();
			$BronquitisM = array();
			$DiabetesM = array();
			$MigranaM = array();
			$TuberculosisM = array();
			$GastritisM = array();
			$VesiculaM = array();
			$AnemiaM = array();
			$sangreM = array();
			$HepatitisM = array();
			$VaricelaM = array();
			$FracturasM = array();
			$EpilepsiaM = array();
			$otrosM = array();
			$rinonM = array();

			foreach ($empleadosbase as $key => $persona) {
				$historia = $this->HistorialClinico($persona->CURP);
				if($persona->genero=='Femenino'){
					if (!empty($historia)) {

						$patologico =  json_decode($historia->patologico);
						$enfermedades = $patologico->enfermPadecidas;
						if ($enfermedades != null) {
							foreach ($enfermedades as $value) {
								if ($value == "Estreñimiento") {
									array_push($estrenimientoM, $persona->CURP);
								} else if ($value == "Soplo del corazón") {
									array_push($soploM, $persona->CURP);
								} else if ($value == "Hipertensión") {
									array_push($HipertensionM, $persona->CURP);
								} else if ($value == "Bronquitis") {
									array_push($BronquitisM, $persona->CURP);
								} else if ($value == "Diabetes") {
									array_push($DiabetesM, $persona->CURP);
								} else if ($value == "Migraña") {
									array_push($MigranaM, $persona->CURP);
								} else if ($value == "Tuberculosis") {
									array_push($TuberculosisM, $persona->CURP);
								} else if ($value == "Gastritis") {
									array_push($GastritisM, $persona->CURP);
								} else if ($value == "Piedras en la vesícula") {
									array_push($VesiculaM, $persona->CURP);
								} else if ($value == "Piedras en el riñon") {
									array_push($rinonM, $persona->CURP);
								} else if ($value == "Anemia") {
									array_push($AnemiaM, $persona->CURP);
								} else if ($value == "Enfermedades de la sangre") {
									array_push($sangreM, $persona->CURP);
								} else if ($value == "Hepatitis") {
									array_push($HepatitisM, $persona->CURP);
								} else if ($value == "Varicela") {
									array_push($VaricelaM, $persona->CURP);
								} else if ($value == "Fracturas") {
									array_push($FracturasM, $persona->CURP);
								} else if ($value == "Epilepsia") {
									array_push($EpilepsiaM, $persona->CURP);
								} else {
									array_push($otrosM, $persona->CURP);
								}
							}
						}
					}
				}else{
					if (!empty($historia)) {

						$patologico =  json_decode($historia->patologico);
						$enfermedades = $patologico->enfermPadecidas;
						if ($enfermedades != null) {
							foreach ($enfermedades as $value) {
								if ($value == "Estreñimiento") {
									array_push($estrenimiento, $persona->CURP);
								} else if ($value == "Soplo del corazón") {
									array_push($soplo, $persona->CURP);
								} else if ($value == "Hipertensión") {
									array_push($Hipertension, $persona->CURP);
								} else if ($value == "Bronquitis") {
									array_push($Bronquitis, $persona->CURP);
								} else if ($value == "Diabetes") {
									array_push($Diabetes, $persona->CURP);
								} else if ($value == "Migraña") {
									array_push($Migrana, $persona->CURP);
								} else if ($value == "Tuberculosis") {
									array_push($Tuberculosis, $persona->CURP);
								} else if ($value == "Gastritis") {
									array_push($Gastritis, $persona->CURP);
								} else if ($value == "Piedras en la vesícula") {
									array_push($Vesicula, $persona->CURP);
								} else if ($value == "Piedras en el riñon") {
									array_push($rinon, $persona->CURP);
								} else if ($value == "Anemia") {
									array_push($Anemia, $persona->CURP);
								} else if ($value == "Enfermedades de la sangre") {
									array_push($sangre, $persona->CURP);
								} else if ($value == "Hepatitis") {
									array_push($Hepatitis, $persona->CURP);
								} else if ($value == "Varicela") {
									array_push($Varicela, $persona->CURP);
								} else if ($value == "Fracturas") {
									array_push($Fracturas, $persona->CURP);
								} else if ($value == "Epilepsia") {
									array_push($Epilepsia, $persona->CURP);
								} else {
									array_push($otros, $persona->CURP);
								}
							}
						}
					}
				}
				// if ($g_sanguineos=='A+') {

				// 	array_push($drogas, $persona->CURP);
				// }
			}
			$audiometrias = $this->estudios('1383');
			$espirometrias = $this->estudios('1382');
			$electro = $this->estudios('1372');
			$audiosnormales = array();
			$audiosanormales = array();
			foreach ($audios as $a) {
				if ($a->tipo_resultado == 'NORMAL') {
					array_push($audiosnormales, $a->curp);
				} else {
					array_push($audiosanormales, $a->curp);
				}
			}
			$espironormales = array();
			$espiroanormales = array();
			foreach ($espiro as $e) {
				if ($e->tipo_resultado == 'NORMAL') {
					array_push($espironormales, $e->curp);
				} else {
					array_push($espiroanormales, $e->curp);
				}
			}
			$electnormal = array();
			$electanormal = array();
			foreach ($elect as $e) {
				if ($e->tipo_resultado == 'NORMAL') {
					array_push($electnormal, $e->curp);
				} else {
					array_push($electanormal, $e->curp);
				}
			}

			foreach ($user->accesos->groupBy('modulo_id') as $key => $value) {
				$permisos = [];
				foreach ($value as $acceso) {
					$permisos[$acceso->AccesoCrud->first()->operacion] = true;
				}
				Session::put($acceso->modulosAccesos->first()->nombre, $permisos);
			}

			$empresa = Empresa::select('id', 'nombre', 'logo')
				->where('id', $user->empresa_id)
				->first();

			Session::put('empresa', $empresa);
			$consultas = $this->consultas();
			$empleados = $this->personal();
			$femenino = $this->personalFemenino();
			$masculino = $this->personalMasculino();
			return view('Empresa/Empresa', [
				'consultas' => $consultas,
				'personal' => $empleados,
				'femenino' => $femenino,
				'masculino' => $masculino,
				'cigarros' => number_format($ptabaquismo, 0),
				'alcholes' => number_format($palcholes, 0),
				'droguitas' => number_format($pdroguitas, 0),
				'estrenimiento' => count($estrenimiento),
				'soplo' => count($soplo),
				'Hipertension' => count($Hipertension),
				'Bronquitis' => count($Bronquitis),
				'Diabetes' => count($Diabetes),
				'Migrana' => count($Migrana),
				'Tuberculosis' => count($Tuberculosis),
				'Gastritis' => count($Gastritis),
				'Vesicula' => count($Vesicula),
				'rinon' => count($rinon),
				'Anemia' => count($Anemia),
				'sangre' => count($sangre),
				'Hepatitis' => count($Hepatitis),
				'Varicela' => count($Varicela),
				'Fracturas' => count($Fracturas),
				'Epilepsia' => count($Epilepsia),
				'otros' => count($otros),
				'estrenimientoM' => count($estrenimientoM),
				'soploM' => count($soploM),
				'HipertensionM' => count($HipertensionM),
				'BronquitisM' => count($BronquitisM),
				'DiabetesM' => count($DiabetesM),
				'MigranaM' => count($MigranaM),
				'TuberculosisM' => count($TuberculosisM),
				'GastritisM' => count($GastritisM),
				'VesiculaM' => count($VesiculaM),
				'rinonM' => count($rinonM),
				'AnemiaM' => count($AnemiaM),
				'sangreM' => count($sangreM),
				'HepatitisM' => count($HepatitisM),
				'VaricelaM' => count($VaricelaM),
				'FracturasM' => count($FracturasM),
				'EpilepsiaM' => count($EpilepsiaM),
				'otrosM' => count($otrosM),
				'audiosnormales' => count($audiosnormales),
				'audiosanormales' => count($audiosanormales),
				'espironormales' => count($espironormales),
				'espiroanormales' => count($espiroanormales),
				'electnormal' => count($electnormal),
				'electanormal' => count($electanormal),
				'audiometrias' => $audiometrias,
				'espirometrias' => $espirometrias,
				'electro' => $electro
			]);
		} else {
			return abort(404);
		}
	}
	public function estudios($id)
	{
		return EstudioProgramado::where('estudios', 'like', '%' . $id . '%')
			->count();
	}
	public function tipoestudios($estudio, $tipo)
	{
		$comparacion = 0;
		if ($estudio == "espirometrias") {
			$estudios = $this->audios();
		} else if ($estudio == "audiometrias") {
			$estudios = $this->espiro();
		} else {
			$estudios = $this->electro();
		}
		if ($tipo == 1) {
			$comparacion = 'NORMAL';
		} else {
			$comparacion = 'ANORMAL';
		}
		$curps = array();
		foreach ($estudios as $a) {
			if ($a->tipo_resultado == $comparacion) {
				array_push($curps, $a->curp);
			}
		}
		return json_encode($curps);
	}
	public function audios()
	{
		return Audiometria::where('deleted_at', NULL)->get();
	}
	public function espiro()
	{
		return EstudioEspirometria::where('deleted_at', NULL)->get();
	}
	public function electro()
	{
		return Electrocardiograma::where('deleted_at', NULL)->get();
	}


	public function HistorialClinico($id)
	{
		return  HistorialClinico::where('CURP', $id)
			->orderBy('id', 'DESC')->first();
		// return Empleado::where('CURP',$curp)->first();
	}
	private function empleadostotales()
	{
		return Empleado::where('empresa_id', Session::get('empresa')->id)
			->where('status_id', 1)
			->get();
	}

	public function charts()
	{
	}

	public function consultas()
	{
		return Consulta::where('id_user', Auth::id())
			->where('estado', 'finalizado')
			->count();
	}

	private function personal()
	{
		return Empleado::where('empresa_id', Session::get('empresa')->id)
			->where('status_id', 1)
			->count();
	}
	private function personalFemenino()
	{
		return Empleado::where('empresa_id', Session::get('empresa')->id)
			->where('status_id', 1)
			->where('genero', 'like', '%' . 'Femenino' . '%')
			->count();
	}
	private function personalMasculino()
	{
		return Empleado::where('empresa_id', Session::get('empresa')->id)
			->where('status_id', 1)
			->where('genero', 'like', '%' . 'Masculino' . '%')
			->count();
	}




	public function admin($nomEmpresa)
	{
		//dd($nomEmpresa);
		$empresa = Empresa::select('id', 'nombre', 'logo', 'user_id')
			->where('nombre', $nomEmpresa)
			->first();
		$usuario = User::find($empresa->user_id);
		Session::put('Admin', Auth::user());
		//$AdminSession = session('Admin');
		Auth::login($usuario);
		$user = Auth::user();

		if ($user->admin()) {
			return view('Administrador/DashboardAdmi');
		} else if ($user->empresa()) {
			$empresa = Empresa::select('id', 'nombre', 'logo')
				->where('user_id', $user->id)
				->first();

			$AdminSession = session('Admin');
			Session::put('empresa', $empresa);
			return view('Empresa/Empresa', [
				'userEmpresa' => 1
			]);
		} else {
			return abort(404);
		}
	}
	public function salirAdmin()
	{
		$AdminSession = session('Admin');
		Auth::login($AdminSession);
		Session::put('Admin', "");
		$AdminSession = session('Admin');
		//dd($user = Auth::user());
		return redirect()->action('RouteController@verEmpresas');
	}
}
