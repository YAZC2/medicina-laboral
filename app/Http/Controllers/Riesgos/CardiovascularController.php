<?php

namespace App\Http\Controllers\Riesgos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;

use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;
use App\Estudios;
use App\HistorialClinico;
use App\Audiometria;
use App\AudiometriaResultado;
use App\datosUserHtds;
use App\Espirometria;
use App\Empresa;
use App\EspirometriaLnn;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CardiovascularController extends Controller
{
    public function cardiovascular()
    {
        $empresas=Empresa::All();
        $empleados=Empleado::where('status_id',1)->get();
        // return dd("Cardiovascular");
        return view('Riesgos/ListaCardiovascular')
        ->with('empleados', $empleados)
        ->with('empresas', $empresas);  
    }
    
}