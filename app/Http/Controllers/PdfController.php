<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archivo;
use App\Audiometria;
use App\Expediente;
use App\HistorialClinico;
use App\Empleado;
use App\Empresa;
use App\Configuracion;
use App\Consulta;
use App\ConsultaMedicamento;
use App\ConsultaProcedimiento;
use App\ConsultaTratamiento;
use App\datosUserHtds;
use Mail;
use App\EstudioProgramado;
use App\Electrocardiograma;
use App\Fisioterapia;
use Illuminate\Support\Facades\Session;


class PdfController extends Controller
{
  public function MostrarPdf($id)
  {
    $archivo = Archivo::find($id);
    $content = base64_decode($archivo->archivo);
    $pdf = \PDF::loadHTML($content);
    return $pdf->stream();
  }
  public function viewPdf($encryptedID, $formato)
  {
    $historialclinico = $this->identificacion($encryptedID);
    //return $historialclinico->examenFisico;
    if (empty($historialclinico)) {
      $empleado = $this->empleado($encryptedID);
      ini_set('max_execution_time', 300);
      ini_set("memory_limit", "512M");
      $pdf = \PDF::loadView('PDF/ErrorPDF', compact('empleado'));
      return $pdf->stream();
    } else {
      $identificacion = json_decode($historialclinico->identificacion);
      $heredofamiliar =  json_decode($historialclinico->heredofamiliar);
      $nopatologico =  json_decode($historialclinico->noPatologico);
      $patologico =  json_decode($historialclinico->patologico);
      $genicoobstetrico = json_decode($historialclinico->genicoobstetrico);
      $historiaLaboral =  json_decode($historialclinico->historialLaboral);
      $examen =  json_decode($historialclinico->examenFisico);
      $somnolencia =  json_decode($historialclinico->somnolencia);
      $resultados =  json_decode($historialclinico->resultado);
      $cadena_custodia = json_decode($historialclinico->cadena_custodia);
     
      if ($formato == 'admision_contratista') {
        $admision_contratistas = json_decode($historialclinico->admision_contratista);
        $empleado = $this->empleado($encryptedID);
        ini_set('max_execution_time', 300);
        ini_set("memory_limit", "512M");
        $pdf = \PDF::loadView('PDF/historialClinico', compact('identificacion', 'empleado', 'heredofamiliar', 'nopatologico', 'patologico', 'genicoobstetrico', 'historiaLaboral', 'examen', 'resultados', 'admision_contratistas', 'formato', 'somnolencia'));
        return $pdf->stream();
      } else if ($formato == "examen_medico") {
        $admision_contratistas = null;
        $empleado = $this->empleado($encryptedID);
        ini_set('max_execution_time', 300);
        ini_set("memory_limit", "512M");
        $pdf = \PDF::loadView('PDF/historialClinico', compact('identificacion', 'empleado', 'heredofamiliar', 'nopatologico', 'patologico', 'genicoobstetrico', 'historiaLaboral', 'examen', 'resultados', 'admision_contratistas', 'formato', 'somnolencia', 'cadena_custodia'));
        return $pdf->stream();
      } 
      else if ($formato == "historia_clinica_c") {
        $admision_contratistas = null;
        $empleado = $this->empleado($encryptedID);
        ini_set('max_execution_time', 300);
        ini_set("memory_limit", "512M");
        $pdf = \PDF::loadView('PDF/historia_clinica_cemex', compact('identificacion', 'empleado', 'heredofamiliar', 'nopatologico', 'patologico', 'genicoobstetrico', 'historiaLaboral', 'examen', 'resultados', 'admision_contratistas', 'formato', 'somnolencia', 'cadena_custodia'));
        return $pdf->stream();
      } 
      else if ($formato == "REG001") {
        $admision_contratistas = null;
        $empleado = $this->empleado($encryptedID);
        ini_set('max_execution_time', 300);
        ini_set("memory_limit", "512M");
        $pdf = \PDF::loadView('PDF/REG001', compact('identificacion', 'empleado', 'heredofamiliar', 'nopatologico', 'patologico', 'genicoobstetrico', 'historiaLaboral', 'examen', 'resultados', 'admision_contratistas', 'formato', 'somnolencia'));
        return $pdf->stream();
      } else if ($formato == "cadena_custodia") {
        $admision_contratistas = null;
        $empleado = $this->empleado($encryptedID);
        ini_set('max_execution_time', 300);
        ini_set("memory_limit", "512M");
        $pdf = \PDF::loadView('PDF/cadenacustodia', compact('identificacion', 'empleado', 'heredofamiliar', 'nopatologico', 'patologico', 'genicoobstetrico', 'historiaLaboral', 'examen', 'resultados', 'admision_contratistas', 'formato', 'somnolencia'));
        return $pdf->stream();
      } else {
        $admision_contratistas = null;
        $empleado = $this->empleado($encryptedID);
        ini_set('max_execution_time', 300);
        ini_set("memory_limit", "512M");
        $pdf = \PDF::loadView('PDF/historialSeguros', compact('identificacion', 'empleado', 'heredofamiliar', 'nopatologico', 'patologico', 'genicoobstetrico', 'historiaLaboral', 'examen', 'resultados', 'admision_contratistas', 'formato', 'somnolencia'));
        return $pdf->stream();
      }
      // $empleado=Empleado::find($historialclinico->curp);


    }
  }

  public function identificacion($id)
  {
    return  HistorialClinico::where('CURP', $id)
      ->orderBy('id', 'DESC')->first();
    // return Empleado::where('CURP',$curp)->first();
  }
  public function Empleado($id)
  {
    return Empleado::where('CURP', $id)->first();
  }

  public function recetaMiedica($id)
  {
    //return \Auth::user();
    $json = $this->consulta($id);
    $user = $this->medico(\Auth::user()->id);
    $empresa = $this->empresa(Session::get('empresa')->id);
    $paciente = $this->paciente($json->expediente_id);
    $medicamentos = $this->medicamentos($json->id);
    $pdf = \PDF::loadView('PDF/receta', compact('medicamentos', 'user', 'json', 'empresa', 'paciente'));
    return $pdf->stream();
  }

  public function procedimientos($id)
  {
    //return \Auth::user();
    $json = $this->consulta($id);
    $user = $this->medico(\Auth::user()->id);
    $empresa = $this->empresa(Session::get('empresa')->id);
    $paciente = $this->paciente($json->expediente_id);
    $procedimientos = consultaProcedimiento::where('consulta_id', $id)->get();
    //   dd($procedimientos);
    $pdf = \PDF::loadView('PDF/procedimientos', compact('procedimientos', 'user', 'json', 'empresa', 'paciente'));
    return $pdf->stream();
  }

  public function tratamiento($id)
  {
    //return \Auth::user();
    $json = $this->consulta($id);
    $user = $this->medico(\Auth::user()->id);
    $empresa = $this->empresa(Session::get('empresa')->id);
    $paciente = $this->paciente($json->expediente_id);
    $tratamiento = ConsultaTratamiento::where('consulta_id', $id)->first()->tratamiento;
    $pdf = \PDF::loadView('PDF/tratamiento', compact('tratamiento', 'user', 'json', 'empresa', 'paciente'));
    return $pdf->stream();
  }

  public function medicamentos($consulta_id)
  {
    return ConsultaMedicamento::where('consulta_id', $consulta_id)
      ->get();
  }


  public function consulta($id)
  {
    return  Consulta::find($id);
  }

  public function medico($id)
  {
    return Configuracion::where('id_user', $id)
      ->join('users', 'users.id', 'configuracion.id_user')
      ->first();
  }

  public function empresa($id)
  {
    return Empresa::find($id);
  }
  public function paciente($id)
  {
    $expediente = Expediente::find($id);
    return Empleado::where('CURP', $expediente->curp)
      ->where('status_id', 1)
      ->where('empresa_id', \Session::get('empresa')->id)
      ->first();
  }


  // NOTE: EMAIL


  public function emailNotas(Request $data)
  {
    $id = $data->id;
    $email = $data->email;
    $json = $this->consulta($id);
    $user = $this->medico(\Auth::user()->id);
    $empresa = $this->empresa(Session::get('empresa')->id);
    $paciente = $this->paciente($json->expediente_id);
    $tratamiento = ConsultaTratamiento::where('consulta_id', $id)->first();

    $data = array(
      'tratamiento' => $tratamiento,
      'user' => $user,
      'json' => $json,
      'empresa' => $empresa,
      'paciente' => $paciente,
    );

    Mail::send('Mails/notas', $data, function ($message) use ($email, $empresa) {
      $message->from('noreply@humanly-sw.com', $empresa->nombre);
      $message->to($email);
      $message->subject('Tratamiento médico');
    });

    return response()->json("Correcto", 200);
  }

  public function emailMiedica($id)
  {
    //return \Auth::user();
    try {
      $json = $this->consulta($id);
      $user = $this->medico(\Auth::user()->id);
      $empresa = $this->empresa(Session::get('empresa')->id);
      $paciente = $this->paciente($json->expediente_id);
      $medicamentos = $this->medicamentos($json->id);
      $data = array(
        'medicamentos' => $medicamentos,
        'user' => $user,
        'json' => $json,
        'empresa' => $empresa,
        'paciente' => $paciente,
      );
      Mail::send('Mails/receta', $data, function ($message) use ($paciente, $empresa) {
        $message->from('noreply@humanly-sw.com', $empresa->nombre);
        $message->to($paciente->email);
        $message->subject('Receta médica');
      });
      return response()->json('Correcto', 200);
    } catch (\Throwable $th) {
      dd('Reboto?', $th);
      return response()->json($th, 500);
      //throw $th;
    }
  }

  public function procedimientosEmail($id)
  {
    //return \Auth::user();
    $json = $this->consulta($id);
    $user = $this->medico(\Auth::user()->id);
    $empresa = $this->empresa(Session::get('empresa')->id);
    $paciente = $this->paciente($json->expediente_id);
    $procedimientos = consultaProcedimiento::where('consulta_id', $id)->get();


    $data = array(
      'procedimientos' => $procedimientos,
      'user' => $user,
      'json' => $json,
      'empresa' => $empresa,
      'paciente' => $paciente,
    );

    Mail::send('Mails/procedimientos', $data, function ($message) use ($paciente, $empresa) {
      $message->from('noreply@humanly-sw.com', $empresa->nombre);
      $message->to($paciente->email);
      $message->subject('Procedimientos médicos');
    });

    return response()->json("Correcto", 200);
  }

  public function tratamientoEmail($id)
  {
    //return \Auth::user();
    $json = $this->consulta($id);
    $user = $this->medico(\Auth::user()->id);
    $empresa = $this->empresa(Session::get('empresa')->id);
    $paciente = $this->paciente($json->expediente_id);
    $tratamiento = ConsultaTratamiento::where('consulta_id', $id)->first();

    $data = array(
      'tratamiento' => $tratamiento,
      'user' => $user,
      'json' => $json,
      'empresa' => $empresa,
      'paciente' => $paciente,
    );

    Mail::send('Mails/tratamiento', $data, function ($message) use ($paciente, $empresa) {
      $message->from('noreply@humanly-sw.com', $empresa->nombre);
      $message->to($paciente->email);
      $message->subject('Tratamiento médico');
    });

    return response()->json("Correcto", 200);
  }


  public function resultadoAudiometria($nim, $curp)
  {
    // $audiometria = Audiometria::find($audiometria_id);
    $estudio = EstudioProgramado::where('id', $nim)->first();
    $audiometria = Audiometria::where('curp', $curp)
      ->where('nim', $estudio->folio)
      ->orderBy('created_at', 'desc')
      ->first();
    $empleado = Empleado::where('curp', $curp)->first();
    $empresa = Empresa::where('id', $empleado->empresa_id)->first();

    $user_htds = datosUserHtds::where('ID_empleado', $audiometria->IdMedico)->first();

    if ($audiometria != null) {

      $d_500 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '500')->pluck('desibelios')->first();
      $d_1000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '1000')->pluck('desibelios')->first();
      $d_2000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '2000')->pluck('desibelios')->first();
      $d_4000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '4000')->pluck('desibelios')->first();
      $dshl_d = $d_500 + $d_1000 + $d_2000 + $d_4000;
      $indice_sal_d = round(($d_500 + $d_1000 + $d_2000) / 3);

      $i_500 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '500')->pluck('desibelios')->first();
      $i_1000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '1000')->pluck('desibelios')->first();
      $i_2000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '2000')->pluck('desibelios')->first();
      $i_4000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '4000')->pluck('desibelios')->first();
      $dshl_i = $i_500 + $i_1000 + $i_2000 + $i_4000;
      $indice_sal_i = round(($i_500 + $i_1000 + $i_2000) / 3);
    } else {
      $dshl_d = null;
      $dshl_i = null;
      $indice_sal_d = null;
      $indice_sal_i = null;
    }


    return view('PDF.resultado_audiometria')
      ->with('audiometria', $audiometria)
      ->with('dshl_d', $dshl_d)
      ->with('dshl_i', $dshl_i)
      ->with('indice_sal_d', $indice_sal_d)
      ->with('indice_sal_i', $indice_sal_i)
      ->with('user_htds', $user_htds)
      ->with('empresa', $empresa)
      ->with('empleado', $empleado);
  }
  public function resultadoAudiometria_correo($nim, $curp)
  {
    // $audiometria = Audiometria::find($audiometria_id);
    $estudio = EstudioProgramado::where('id', $nim)->first();
    $empleado = Empleado::where('id', $estudio->empleado_id)->first();
    $empresa = Empresa::where('id', $empleado->empresa_id)->first();
    $curp=$empleado->CURP;
    $audiometria = Audiometria::where('curp', $curp)
      ->where('nim', $estudio->folio)
      ->orderBy('created_at', 'desc')
      ->first();
  

    $user_htds = datosUserHtds::where('ID_empleado', $audiometria->IdMedico)->first();

    if ($audiometria != null) {

      $d_500 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '500')->pluck('desibelios')->first();
      $d_1000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '1000')->pluck('desibelios')->first();
      $d_2000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '2000')->pluck('desibelios')->first();
      $d_4000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '4000')->pluck('desibelios')->first();
      $dshl_d = $d_500 + $d_1000 + $d_2000 + $d_4000;
      $indice_sal_d = round(($d_500 + $d_1000 + $d_2000) / 3);

      $i_500 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '500')->pluck('desibelios')->first();
      $i_1000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '1000')->pluck('desibelios')->first();
      $i_2000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '2000')->pluck('desibelios')->first();
      $i_4000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '4000')->pluck('desibelios')->first();
      $dshl_i = $i_500 + $i_1000 + $i_2000 + $i_4000;
      $indice_sal_i = round(($i_500 + $i_1000 + $i_2000) / 3);
    } else {
      $dshl_d = null;
      $dshl_i = null;
      $indice_sal_d = null;
      $indice_sal_i = null;
    }


    return view('PDF.resultado_audiometria')
      ->with('audiometria', $audiometria)
      ->with('dshl_d', $dshl_d)
      ->with('dshl_i', $dshl_i)
      ->with('indice_sal_d', $indice_sal_d)
      ->with('indice_sal_i', $indice_sal_i)
      ->with('user_htds', $user_htds)
      ->with('empresa', $empresa)
      ->with('empleado', $empleado);
  }
  


  public function resultadoAudiometria2($audiometria_id, $curp)
  {
    $audiometria = Audiometria::find($audiometria_id);
    $empleado = Empleado::where('curp', $curp)->first();
    $empresa = Empresa::where('id', $empleado->empresa_id)->first();
    $user_htds = datosUserHtds::where('ID_empleado', $audiometria->IdMedico)->first();

    $d_500 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '500')->pluck('desibelios')->first();
    $d_1000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '1000')->pluck('desibelios')->first();
    $d_2000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '2000')->pluck('desibelios')->first();
    $d_4000 = $audiometria->resultados->where('oido', 'derecho')->where('frecuencia', '4000')->pluck('desibelios')->first();
    $dshl_d = $d_500 + $d_1000 + $d_2000 + $d_4000;
    $indice_sal_d = round(($d_500 + $d_1000 + $d_2000) / 3);

    $i_500 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '500')->pluck('desibelios')->first();
    $i_1000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '1000')->pluck('desibelios')->first();
    $i_2000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '2000')->pluck('desibelios')->first();
    $i_4000 = $audiometria->resultados->where('oido', 'izquierdo')->where('frecuencia', '4000')->pluck('desibelios')->first();
    $dshl_i = $i_500 + $i_1000 + $i_2000 + $i_4000;
    $indice_sal_i = round(($i_500 + $i_1000 + $i_2000) / 3);

    $pdf = \PDF::loadView('PDF/pruebaAudio', compact('audiometria', 'dshl_d', 'dshl_i', 'indice_sal_d', 'indice_sal_i', 'user_htds', 'empleado'));
    return $pdf->stream();

    return view('PDF.resultado_audiometria')
      ->with('audiometria', $audiometria)
      ->with('dshl_d', $dshl_d)
      ->with('dshl_i', $dshl_i)
      ->with('indice_sal_d', $indice_sal_d)
      ->with('indice_sal_i', $indice_sal_i)
      ->with('user_htds', $user_htds)
      ->with('empleado', $empleado);
  }
  
  public function resultado_electro($curp, $id_toma)
  {
    $empleado = Empleado::where('curp', $curp)->first();
    // $pdf=\PDF::loadView('PDF/receta',compact('medicamentos','user','json','empresa','paciente'));
    $estudioprogramado = EstudioProgramado::where('id', $id_toma)->first();
    $nim = $estudioprogramado->folio;
    $electrocardiograma = Electrocardiograma::where('curp', $curp)
      ->orWhere('nim', $nim)
      ->orderBy('created_at', 'desc')
      ->first();
    $pdf = \PDF::loadView('PDF/resultado_electro', compact('electrocardiograma', 'empleado'));
    return $pdf->stream();
  }

  //FORMATOS
  public function cadenacustodia()
  {
    //return \Auth::user();
    // $json = $this->consulta($id);
    // $user = $this->medico(\Auth::user()->id);
    // $empresa = $this->empresa(Session::get('empresa')->id);
    // $paciente = $this->paciente($json->expediente_id);
    // $tratamiento = ConsultaTratamiento::where('consulta_id',$id)->first()->tratamiento;
    $pdf = \PDF::loadView('PDF/cadenacustodia');
    return $pdf->stream();
  }
  public function fisioterapia($curp, $id_toma)
  {
    $empleado = Empleado::where('curp', $curp)->first();
    $estudioprogramado = EstudioProgramado::where('id', $id_toma)->first();
    $nim = $estudioprogramado->folio;
    $fisioterapia = Fisioterapia::where('curp', $curp)
      ->orWhere('nim', $nim)
      ->orderBy('created_at', 'desc')
      ->first();
    switch ($fisioterapia->oswestry1) {
      case '4':
        $intensidad_dolor = "El paciente presenta dolor muy leve de columna o pierna.";
        break;
      case '3':
        $intensidad_dolor = "El paciente presenta dolor moderado de columna o pierna.";
        break;
      case '2':
        $intensidad_dolor = "El paciente presenta dolor intenso de columna o pierna.";
        break;
      case '1':
        $intensidad_dolor = "El paciente presenta dolor muy intenso de columna o pierna.";
        break;
      default:
        $intensidad_dolor = null;
    }
    switch ($fisioterapia->oswestry2) {
      case '4':
        $act_vida_cotidina = "El paciente realiza todo solo y en forma normal,pero con dolor.";
        break;
      case '3':
        $act_vida_cotidina = "El paciente las realiza en forma más lenta y cuidadosa por el dolor.";
        break;
      case '2':
        $act_vida_cotidina = "El paciente ocasionalmente requiere ayuda.";
        break;
      case '1':
        $act_vida_cotidina = "El paciente requiere ayuda diario.";
        break;
      default:
        $act_vida_cotidina = null;
    }
    switch ($fisioterapia->oswestry3) {
      case '5':
        $levantar_objeto = "El paciente puede levantar objetos pesados desde el piso,pero con dolor.";
        break;
      case '4':
        $levantar_objeto = "El paciente no puede levantar objetos pesados del suelo 
        debido al dolor, pero si cargar un objeto pesado desde una mayor altura ,
        ej. desde una mesa.";
        break;
      case '3':
        $levantar_objeto = "El paciente solo puede levantar desde el suelo objetos de peso mediano.";
        break;
      case '2':
        $levantar_objeto = "El paciente solo puede levantar desde el suelo cosas muy livianas.";
        break;
      case '1':
        $levantar_objeto = "El paciente no puede levantar ni cargar nada.";
        break;
      default:
        $levantar_objeto = null;
    }
    switch ($fisioterapia->oswestry4) {
      case '5':
        $caminar = "El paciente no puede caminar más de 1-2km. debido al dolor.";
        break;
      case '4':
        $caminar = "El paciente no puede caminar más de 500-1000mt. debido al dolor.";
        break;
      case '3':
        $caminar = "El paciente no puede caminar más de 500 mt. debido al dolor.";
        break;
      case '2':
        $caminar = "El paciente solo puede caminar ayudado por uno o dos bastones.";
        break;
      case '1':
        $caminar = "El paciente está practicamente en cama, le cuesta mucho hasta ir al baño.";
        break;
      default:
        $caminar = null;
    }
    switch ($fisioterapia->oswestry5) {
      case '5':
        $sentarse = "El paciente solo se sienta en un asiento especial,sin dolor.";
        break;
      case '4':
        $sentarse = "El paciente no puede estar sentado más de una hora sin dolor.";
        break;
      case '3':
        $sentarse = "El paciente no puede estar sentado más de treinta minutos sin dolor.";
        break;
      case '2':
        $sentarse = "El paciente no puede permanecer sentado más de diez minutos sin dolor.";
        break;
      case '1':
        $sentarse = "El paciente no puede permanecer ningún instante sentado sin que sienta dolor.";
        break;
      default:
        $sentarse = null;
    }
    switch ($fisioterapia->oswestry6) {
      case '5':
        $pararse = "El paciente puede permanecer de pie,aunque con dolor.";
        break;
      case '4':
        $pararse = "El paciente no puede estar más de una hora parado libre de dolor.";
        break;
      case '3':
        $pararse = "El paciente no puede estar parado más de treinta minutos libre de dolor.";
        break;
      case '2':
        $pararse = "El paciente no puede permanecer parado más de diez minutos sin dolor.";
        break;
      case '1':
        $pararse = "El paciente no puede permanecer ningún instante de pie sin dolor.";
        break;
      default:
        $pararse = null;
    }
    switch ($fisioterapia->oswestry7) {
      case '5':
        $dormir = "Ocasionalmente el dolor le altera el sueño al paciente.";
        break;
      case '4':
        $dormir = "El paciente no logra dormir más de 6 hrs seguida,por el dolor.";
        break;
      case '3':
        $dormir = "El paciente por el dolor no logra dormir más de 4 hrs seguida.";
        break;
      case '2':
        $dormir = "El paciente por el dolor no logro dormir más de 2 hrs seguida.";
        break;
      case '1':
        $dormir = "El paciente no logra dormir nada sin dolor.";
        break;
      default:
        $dormir = null;
    }
    switch ($fisioterapia->oswestry8) {
      case '5':
        $actividad_sexual = "Normal, aunque con dolor ocasional de columna.";
        break;
      case '4':
        $actividad_sexual = "Casi normal pero con importante dolor de columna.";
        break;
      case '3':
        $actividad_sexual = "Seriamente limitada por el dolor de columna.";
        break;
      case '2':
        $actividad_sexual = "Casi sin actividad,por el dolor de columna.";
        break;
      case '1':
        $actividad_sexual = "Sin actividad,debido a los dolores de columna.";
        break;
      default:
        $actividad_sexual = null;
    }
    switch ($fisioterapia->oswestry9) {
      case '5':
        $actividad_social = "La actividad del paciente es normal pero aumenta el dolor.";
        break;
      case '4':
        $actividad_social = "Su dolor tiene poco impacto en su actividad social, 
      excepto aquellas más enérgeticas (ej. deportes).";
        break;
      case '3':
        $actividad_social = "El paciente sale muy poco,debido al dolor.";
        break;
      case '2':
        $actividad_social = "El paciente no sale nunca,debido al dolor.";
        break;
      case '1':
        $actividad_social = "El paciente no hace nada debido al dolor.";
        break;
      default:
        $actividad_social = null;
    }
    switch ($fisioterapia->oswestry10) {
      case '5':
        $viajar = "Sin problemas,pero le produce dolor.";
        break;
      case '4':
        $viajar = "El dolor es severo,pero el paciente logra viajes de 2 horas.";
        break;
      case '3':
        $viajar = "El paciente puede viajar menos de 1 hr.,por el dolor.";
        break;
      case '2':
        $viajar = "El paciente puede viajar menos de 30 minutos por el dolor.";
        break;
      case '1':
        $viajar = "El paciente solo viaja para ir al médico o al hospital.";
        break;
      default:
        $viajar = null;
    }

    $fisioterapia_anterior = Fisioterapia::where('curp', $curp)
      ->orWhere('nim', $nim)
      ->orderBy('created_at', 'desc')
      ->get();

    if (count($fisioterapia_anterior) > 1) {
      $fisioterapia_an = $fisioterapia_anterior[1];
    } else {
      $fisioterapia_an = null;
    }

    $pdf = \PDF::loadView(
      'PDF/resultado_fisioterapia',
      compact(
        'fisioterapia',
        'empleado',
        'fisioterapia_an',
        'intensidad_dolor',
        'act_vida_cotidina',
        'levantar_objeto',
        'caminar',
        'sentarse',
        'pararse',
        'dormir',
        'actividad_sexual',
        'actividad_social',
        'viajar'
      )
    );
    return $pdf->stream();
  }
  public function estudioscorreo($toma,$consecutivo,$correo){
    $estudios = EstudioProgramado::select('estudiosProgramados.id','estudiosProgramados.estudios','estudiosProgramados.folio','estudiosProgramados.empleado_id','estudiosProgramados.fecha_inicial','estudiosProgramados.fecha_final','empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP')
      ->join('empleados','estudiosProgramados.empleado_id','empleados.id')
      ->where('estudiosProgramados.folio',$toma.'/'.$consecutivo)
      ->where('empleados.status_id','1')
      ->first();
    return view('Mails.estudios')
    ->with('toma', $toma)
    ->with('correo', $correo)
    ->with('consecutivo',$consecutivo)
    ->with('estudios',$estudios);
  }
  public function correo_estudio(Request $data)
  {
    $nim = $data->nim;
    $curp='1';
    $email = $data->email;

   
    $empresa = $this->empresa(Session::get('empresa')->id);

    $data = array(
      'nim' => $nim,
      'curp' => $curp,
      'email' => $email,
      'empresa'=>$empresa
    );

    Mail::send('Mails/estudios_por_correo', $data, function ($message) use ($email, $empresa) {
      $message->from('noreply@humanly-sw.com', $empresa->nombre);
      $message->to($email);
      $message->subject('Estudios');
    });

    return response()->json("Correcto", 200);
  }
}
