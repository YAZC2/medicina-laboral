<?php
namespace App\Http\Controllers\Empresa;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;
use App\Toma;
use App\EstudiosSass;


class LaboratorioController extends Controller
{

    public function saveToma (Request $data)
    {
        try {
            DB::beginTransaction();
                $toma = new Toma();
                $toma->user_id = Auth::id();
                $toma->nim = $data->nim;
                $toma->expediente_id = $data->expediente;
                $toma->nota = $data->nota;
                $toma->save();
                $data = array(
                    'statu' => 200,
                    'message' => 'Toma creada correctamente',
                    'data' => $toma
                );
            DB::commit();
        } catch (\Throwable $th) {
           $data = array(
               'statu' => 403,
               'message' => 'Error al crear la toma',
               'data'=>$th
            );
        }

        return response()->json($data, $data['statu']);

    }

    public function tomaEstudios(Request $data)
    {
        $estudios = $data->data;
        $tomaId = $data->toma;

        try {
            DB::beginTransaction();
                foreach ($estudios as $estudio) {
                    $sass = new EstudiosSass();
                    $sass->toma_id = $tomaId;
                    $sass->toma_sass_id = $estudio['id_toma'];
                    $sass->estudio_sass_id = $estudio['id_examen'];
                    $sass->examen = $estudio['examen'];
                    $sass->seccion = $estudio['seccion'];
                    $sass->save();
                }
                $data = array(
                    'statu' => 200,
                    'message' => 'Toma creada correctamente',
                    'data' => true
                );
            DB::commit();
        } catch (\Throwable $th) {

            // $toma = Toma::find($tomaId);
            // $toma->delete();
            $data = array(
                'statu' => 403,
                'message' => 'Error al crear los estudios',
                'data'=>dd($th)
             );
        }
        return response()->json($data, $data['statu']);
    }

    public function showEstudios($id)
    {
      return EstudiosSass::where('toma_id',$id)->get();
    }

    public function resultadosEstudios($id)
    {

        try {

            $estudio =  EstudiosSass::find($id);          
             return view('Empresa/Estudios',compact('estudio'));
        } catch (\Throwable $th) {
            return abort(400);
        }

    }

    public function deleteToma($id)
    {

        $estudios = EstudiosSass::where('toma_id',$id)->delete();
        $toma = Toma::find($id)->delete();
        return $estudios;
    }

}
