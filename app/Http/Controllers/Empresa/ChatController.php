<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Mail;

use App\Mail\MailUsuarioSecundario;
use App\User;
use App\Empresa;
use App\Empleado;
use App\Covid;
use App\SassOrden;

class ChatController extends Controller {
  use AuthenticatesUsers;

  public function index()
  {
    return view('Empresa/chat/chat');
  }
}
