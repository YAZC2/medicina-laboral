<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EstudioProgramado;
use App\Categoria;
use App\Estudio;
use App\Empleado;
use App\Formatos;
class FormatosController extends Controller
{   
    /**
     * Obtener un estudio programado
     * 
     * Devuelve toda la información del estudio programado cuyo ID
     * coincida con el ID recibido como parámetro
     * 
     * @param Integer ID del estudio programado por devolver
     * @return json Datos del estudio programado
     */
    
    public function formato(){
      $formatos = Formatos::all();
      return view('Laboratorio.Medicina.formularios.ConfiFormularios')->with('formatos',$formatos);
   
   }
  public function actualiza_estatus(Request $request){
   $id = $request->id;
   $formatos = Formatos::find($id);
   
   if (!empty($formatos)) {
       $formatos->status=$request->status_n;
       $formatos->save();
      
   }
   return json_encode($formatos);

}      
   
}