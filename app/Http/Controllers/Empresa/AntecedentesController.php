<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Empleado;
use App\Expediente;
use App\HistorialClinico;
use App\Antecedente;
use App\AntecedenteForm;
use App\AntecedenteAnswer;
use App\MedicamentosActivos;
use App\AntecedenteHistorial;

class AntecedentesController extends Controller
{

    /**
     * Obitne todas las preguntas del antecedente seleccionado
     * @param  Request $data [id del antecedente]
     * @return [type]        [preguntas]
     */
    public function getAntecedentes(Request $data)
    {
        $id = $data->get('id');
        $antecedentes = AntecedenteAnswer::where('antecedente_id', $id)
            // ->whereIn('user_id',[0,Auth::id()])
            ->get();
        return $antecedentes;
    }

    /**
     * Crea un nuevoe Expediente
     * @param  [type] $curp [description]
     * @return [type]       [description]
     */
    public function newExpediente($curp)
    {
        $expediente = new Expediente;
        $expediente->curp = $curp;
        $expediente->save();
        return $expediente;
    }
    /**
     * Creacion o modificaicon de un campo de antecedentes form
     * @param  Request $data [description]
     * @return [type]        [description]
     */
    public function changedAntecedente(Request $data)
    {
        //return $data->all();
        $validator = \Validator::make($data->all(), [
            'answerId' => 'required',
            //'curp '=> 'required',
            'formId' => 'required',
            //'html' => 'required'
        ]);

        if ($validator->fails()) {
            $data = array(
                'status' => 500,
                'errors' => $validator->errors()
            );
        } else {
            $curp = $data->get('curp');
            $formId = $data->get('formId');
            $answerId = $data->get('answerId');

            $expediente = $this->verifyExpediente($curp);
            if ($data->validacion == "true") {
                $newAnswer = new AntecedenteAnswer();
                $newAnswer->antecedente_id = $formId;
                $newAnswer->pregunta = $answerId;
                $newAnswer->campo = "text";
                $newAnswer->user_id = Auth::id();
                $newAnswer->save();


                $antecedente = new Antecedente;
                $antecedente->id_expediente = $expediente->id;
                $antecedente->antecedenteF_id = $formId;
                $antecedente->answerA_id = $newAnswer->id;
                $antecedente->respuesta = $data->get('html');
                $antecedente->save();
                $antecedente->nuevo = "si";
            } else {
                $antecedente = $this->antecedenteVerify($expediente->id, $formId, $answerId);
                if ($antecedente == null) {
                    $antecedente = new Antecedente;
                    $fecha_antecedente=null;
                }
                if (empty($antecedente->updated_at)){
                   
                    
                }
                else{
                    $fecha_a=$antecedente->updated_at;
                    $fecha_antecedente= \Carbon::parse($fecha_a)->format('Y-m-d');
                   
                }
                $antecedente->id_expediente = $expediente->id;
                $antecedente->antecedenteF_id = $formId;
                $antecedente->answerA_id = $answerId;
                $antecedente->respuesta = $data->get('html');
                $antecedente->save();
                
                $historialantecedente = $this->historialantecedente($expediente->id, $formId, $answerId);
                          
               
                if (empty($historialantecedente->created_at)){
                    
                }
                else{
                    $fecha_h=$historialantecedente->created_at;
                     $fecha_historial = \Carbon::parse($fecha_h)->format('Y-m-d'); 
                    
                }

                if ($historialantecedente == null || $fecha_antecedente!=$fecha_historial) {
                    $historialantecedente = new AntecedenteHistorial;
                }
                $historialantecedente->id_expediente = $expediente->id;
                $historialantecedente->antecedenteF_id = $formId;
                $historialantecedente->answerA_id = $answerId;
                $historialantecedente->respuesta = $data->get('html');
                $historialantecedente->save();
            }

            $data = array(
                'status' => 200,
                'antecedente' => $antecedente
            );
        }
        return response()->json($data, $data['status']);
    }

    /**
     * Verifica que no haya un antecedente ya registrado
     * @param  [type] $expediente_id [expediente_id]
     * @param  [type] $formId        [antecedente form id]
     * @param  [type] $answerId      [fild del antecedente]
     * @return [type]                [antecedente]
     */
    public function antecedenteVerify($expediente_id, $formId, $answerId)
    {
        return Antecedente::where([
            ['id_expediente', $expediente_id],
            ['antecedenteF_id', $formId],
            ['answerA_id', $answerId]
        ])
        ->first();
    }
    public function historialantecedente($expediente_id, $formId, $answerId)
    {
        return AntecedenteHistorial::where([
            ['id_expediente', $expediente_id],
            ['antecedenteF_id', $formId],
            ['answerA_id', $answerId]
        ])
        ->orderBy('created_at','DESC')
        ->first();
    }
    public function verhistorialantecedentes($curp){
        $empleado=Empleado::where('curp',$curp)->first();
        $expediente_id=Expediente::where('CURP',$curp)->first();
        
        $historial_antecedentes=AntecedenteHistorial::select('af.nombre','aa.pregunta','historial_antecedentes.respuesta','historial_antecedentes.created_at','historial_antecedentes.updated_at')
       ->join('antecedentesForm as af','historial_antecedentes.antecedenteF_id','af.id')
        ->join('antecedentesAnswer as aa','aa.id','historial_antecedentes.answerA_id')
        ->orderBy('historial_antecedentes.created_at', 'DESC')->get();
        // return dd($historial_antecedentes);
      
        return view('Empresa.empleado.historialantecedentes')->with([
            'empleado' => $empleado,
            'historial_antecedentes'=>$historial_antecedentes
        ]);
     
    }
    /**
     * Eliminar un campo del formulario del antecedente
     * @param  Request $data formData
     * @return [type]        Estatu de la peticion
     */
    public function deleteFielAntecedente(Request $data)
    {

        $curp = $data->get('curp');
        $formId = $data->get('formId');
        $answerId = $data->get('answerId');
        $expediente = $this->verifyExpediente($curp);

        try {
            Antecedente::where([
                ['id_expediente', $expediente->id],
                ['antecedenteF_id', $formId],
                ['answerA_id', $answerId]
            ])->delete();

            $data = array(
                'statu' => 200,
                'id' => $answerId
            );

            return response()->json($data, $data['statu']);
        } catch (\Exception $e) {
            return response()->json('', 500);
        }
    }



    public function getFields(Request $data)
    {

        $curp = $data->get('curp');
        $formId = $data->get('id');
        $expediente = $this->verifyExpediente($curp);

        return Antecedente::where([
            ['id_expediente', $expediente->id],
            ['antecedenteF_id', $formId]
        ])
            // ->join('antecedentesAnswer as ans', 'antecedentes.answerA_id', 'ans.id')
            ->join('antecedentesAnswer as ans', 'antecedentes.answerA_id', 'ans.id')
            ->orderBy('antecedentes.created_at', 'asc')
            ->select("ans.*", "ans.id as ans_id", "antecedentes.*")
            ->get();
    }

    /**
     * Medicamentos Activos del paciente
     * @param  Request $data [data post]
     * @return [type]        [medicamento activo]
     */
    public function medicamentoActivo(Request $data)
    {
        $data->validate([
            'name' => 'required',
            'expediente_id' => 'required',
            'id' => 'required'
        ]);
        $medicamento = new MedicamentosActivos();
        $medicamento->expediente_id = $data->get('expediente_id');
        $medicamento->clave = $data->id;
        $medicamento->nombre = $data->name;
        $medicamento->save();
        return $medicamento;
    }

    public function deleteMedicamentoA($id)
    {
        $medicamento = MedicamentosActivos::find($id)
            ->delete();
        $data = array(
            'statu' => 200,
            'id' =>  $id
        );
        return response()->json($data, $data['statu']);
    }

    public function changeStatusAntecedentes($id, $status)
    {
        $status = ($status == "false" ? false : true);
        $antecedente = Antecedente::find($id);
        $antecedente->status = $status;
        // $antecedente->respuesta = "";
        $antecedente->save();
        $data = array(
            'status' => 200,
        );
        return response()->json($antecedente);
    }
}
