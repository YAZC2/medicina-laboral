<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use App\Roles;
use App\Accesos;
use App\Modulos;
use App\Crud;
use App\User;

class AccesosController extends Controller {
  public function index(){
    $roles = $this->roles();
    $modulos = $this->modulos();
    $cruds = $this->crud();
    return view('Empresa/accesos/accesos',compact('roles','modulos','cruds'));
  }

  public function roles(){
    return Roles::where('empresa_id',Session::get('empresa')->id)->get();
  }
  public function modulos(){
    return Modulos::all();
  }

  public function crud(){
    return Crud::all();
  }

  public function permisos($id){
    return Accesos::select('c.nombre as crud', 'm.nombre as modulo','roles.tipo','accesos.id')
    ->where('rol_id',$id)
    ->join('roles','roles.id','accesos.rol_id')
    ->join('crud as c','c.id','accesos.crud_id')
    ->join('modulos as m','m.id','accesos.modulo_id')
    ->get();
  }

  public function deleteAcc(Request $data){
    try {
      \DB::beginTransaction();
        $ac = Accesos::find($data->id);
        $ac->delete();
      \DB::commit();
      $data = array(
        'status' => 200 ,
        'message'=> 'succes',
        'data' =>$ac
      );
    } catch (\Exception $e) {
      $data = array(
        'status' => 401 ,
        'message'=> 'error'
      );
    }
    return response()->json($data,$data['status']);

  }

    public function addPermiso(Request $data){
            $rol_id = $data->role;
            $modulo_id = $data->modulo;
            $crud_id = $data->operacion;
            // $permiso = Accesos::where([
            //   ['rol_id', $rol_id],
            //   ['modulo_id' ,$modulo_id],
            //   ['crud_id' ,$crud_id]
            // ])
            // ->first();
            // if (!$permiso) {7
            $validador = false;
            foreach ($data->operacion as $key => $value) {
                $permiso = Accesos::where('rol_id',$rol_id)->where('modulo_id',$modulo_id)->where('crud_id',$value)->first();
                if($permiso == null)
                {
                    try {
                        \DB::beginTransaction();
                        $acc = new Accesos();
                        $acc->rol_id = $rol_id;
                        $acc->modulo_id = $modulo_id;
                        $acc->crud_id = $value;
                        $acc->save();
                        \DB::commit();
                        $data = array(
                        'status' => 200,
                        'message'=>'success',
                        'data'=>$acc
                        );
                        $validador = true;
                    } catch (\Exception $e) {
                        DB::rollback();
                        dd($e);
                        $data = array(
                        'statu' => 401,
                        'message' =>'error',
                        'data' => $e
                        );
                    }
                }
            }
            if($validador)
            {
                return response()->json($data,$data['status']);
            }else{
                $data = array('status' => 200,'message' => 'repeat','data' => 'repeat');
                return response()->json($data, 200);
            }
    }

  public function addRol(Request $data){
    try {
      $roles = $data->rol;
      $description = $data->description;
      \DB::beginTransaction();
        $rol = new Roles();
        $rol->tipo = $roles;
        $rol->descripcion = $description;
        $rol->empresa_id = \Session::get('empresa')->id;
        $rol->save();
      \DB::commit();
      $data = array(
        'statu' => 200,
        'message'=>'success',
        'data'=>$rol
      );
    } catch (\Throwable $e) {
      $data = array(
        'statu' => 401,
        'message' =>'error',
        'data' => $e
      );
    }
    return response()->json($data,$data['statu']);
  }

  public function addRolDelete(Request $data){

   $rol = $data->id;
   $user = User::where('role_id',$rol)->first();

   if (!$user) {
       try {
         \DB::beginTransaction();
         $rols =  Roles::find($rol);
         $rols->delete();
         Accesos::where('rol_id',$rol)->delete();
         \DB::commit();
         $data = array(
           'statu' => 200,
           'message'=>'success',
           'data'=>$rols
         );
       } catch (\Throwable $e) {
         dd($e);
         $data = array(
           'statu' => 401,
           'message' =>'error',
           'data' => $e
         );
     }
   }else {
     $data = array(
       'statu' => 403,
       'message' =>'hay usuarios con ese rol',
       'data' =>'user'
     );
   }
   return response()->json($data,$data['statu']);

 }

}
