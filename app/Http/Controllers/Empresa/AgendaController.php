<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Empleado;
use App\Agenda;

class AgendaController extends Controller {
  public function index(){
    $empleados = $this->empleados();
    $schedules = $this->todaySchedules();
    return view('Empresa/Agenda/agenda',compact('empleados','schedules'));
  }

  public function empleados(){
    return Empleado::where([
      'status_id'=>1,
      'empresa_id'=>Session::get('empresa')->id
    ])
    ->get();
  }

  public function appointment(Request $request){
    
    $request->validate([
      'date' =>'required',
      'endTime' =>'required',
      'startTime' =>'required',
      'paciente' =>'required',
      'consultation' =>'required',
    ]);
    $agenda = new Agenda;
    $agenda->user_id = \Auth::id();
    $agenda->empleado_id = $request->get('paciente');
    $agenda->date = $request->get('date');
    $agenda->start_time = $request->get('startTime');
    $agenda->end_time = $request->get('endTime');
    $agenda->statu = 1;
    $agenda->remember = $request->get('remember',2) == 'on' ? 1 : 2 ;
    $agenda->patient_remember = $request->get('patientRemember',2) == 'on' ? 1 : 2 ;
    $agenda->reason = $request->get('consultation');
    $agenda->save();

    $empelado = Empleado::find($request->get('paciente'));

    $data = array(
      'empleado' => $empelado,
      'agenda' =>$agenda
    );
    return response()->json($data,200);
  }

  public function getSchedule(){
    $schedules = Agenda::where('user_id',\Auth::id())->get();
    $arraySchedule=[];
    foreach ($schedules as $schedule) {
      $empleado = Empleado::find($schedule->empleado_id);
      $data = array(
        'agenda' => $schedule,
        'empleado' => $empleado
      );
      array_push($arraySchedule,$data);
    }

    return response()->json($arraySchedule,200);
  }

  public function updateScheduler(Request $request){
    $request->validate([
      'date' =>'required',
      'date_id' =>'required',
      'endTime' =>'required',
      'startTime' =>'required',
      'consultation' =>'required',
    ]);
    $agenda = Agenda::find($request->get('date_id'));
    $agenda->date = $request->get('date');
    $agenda->start_time = $request->get('startTime');
    $agenda->end_time = $request->get('endTime');
    $agenda->reason = $request->get('consultation');
    $agenda->save();

    $empleado = Empleado::find($agenda->empleado_id);

    $data = array(
      'agenda' => $agenda,
      'empleado'=> $empleado
    );

    return response()->json($data,200);
  }

  public function deleteSchedule($id){
    $agenda = Agenda::find($id);
    $agenda->delete();
    return $agenda;
  }


  public function notification(){

    $date = date("Y-m-d");
    $today = date("Y-m-d",strtotime($date."+ 1 days"));

     $agendas =  Agenda::where('user_id',\Auth::id())
    ->whereDate('date',$today)
    ->where('statu',1)
    ->where('remember',1)
    ->get();

    $arrayAgenda = [];
    foreach ($agendas as $agenda) {

      $empleado = Empleado::find($agenda->empleado_id);
      $data = array(
        'agenda' => $agenda,
        'empleado'=> $empleado
      );
      array_push($arrayAgenda,$data);
    }

    return response()->json($arrayAgenda,200);

  }

  public function todaySchedules(){
    $date = date("Y-m-d");
   $agendas =  Agenda::where('user_id',\Auth::id())
   ->whereDate('date',$date)
   ->where('statu',1)
   ->get();
   $arrayAgenda = [];
   foreach ($agendas as $agenda) {

     $empleado = Empleado::find($agenda->empleado_id);
     $data = array(
       'agenda' => $agenda,
       'empleado'=> $empleado
     );
     array_push($arrayAgenda,$data);
   }

   return $arrayAgenda;
  }

}
