<?php

namespace App\Http\Controllers\Empresa\Laboratorio;

use App\EstudioProgramado;
use App\Estudios;
use App\Empleado;
use App\Empresa;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class EstudiosController extends Controller {

    public function getEstudiosToma($id){
        $toma =  EstudioProgramado::find($id);
        $estudios = \json_decode($toma->estudios);
        $estudios = Estudios::whereIn('id',$estudios)->get();
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios
        );

        return response()->json($toma, 200);
    }
    public function obtenertomacorreo($nim1,$consecutivo){
        $toma =  EstudioProgramado::select('estudiosProgramados.id','estudiosProgramados.estudios','estudiosProgramados.folio','estudiosProgramados.empleado_id','estudiosProgramados.fecha_inicial','estudiosProgramados.fecha_final','empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP')
        ->join('empleados','estudiosProgramados.empleado_id','empleados.id')
        ->where('estudiosProgramados.folio',$nim1.'/'.$consecutivo)
        ->where('empleados.status_id','1')
        ->first();
        $estudios = \json_decode($toma->estudios);
        $estudios = Estudios::whereIn('id',$estudios)->get();
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios
        );

        return response()->json($toma, 200);
    }
    public function getEstudiosPro($id){
        $toma =  EstudioProgramado::find($id);
        $estudios = \json_decode($toma->estudios);
        $estudios = Estudios::whereIn('codigo',$estudios)->get();
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios
        );

        return response()->json($toma, 200);
    }
    public function Daicoms($id,$id_estudio){
         $toma =  EstudioProgramado::find($id);
         $estudios=Estudios::find($id_estudio);
         $empleado=Empleado::find($toma->empleado_id);
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios,
            'empleado'=>$empleado
        );
        
       
       //dd($toma);
       return view('Empresa.daicom')->with('toma',$toma,200);
       
    }
    public function Daicoms2($id,$id_estudio){
        $toma =  EstudioProgramado::find($id);
        $estudios=Estudios::find($id_estudio);
        $empleado=Empleado::find($toma->empleado_id);
       $toma = array(
           'toma' => $toma,
           'estudios' => $estudios,
           'empleado'=>$empleado
       );
       
      
      //dd($toma);
      return view('Laboratorio.Imagenologia.resultadoimagenologia')->with('toma',$toma,200);
      
   }
   public function image(Request $request){
   $nim=$request->nim;
   $codigo=$request->codigo;
    return view('Laboratorio.Imagenologia.resultadosassima')
    ->with('nim',$nim,200)
    ->with('codigo',$codigo,200);
    }

   public function labo_pdf(Request $request){
       $pdf=$request->pdf;
      
       return view('Laboratorio.Laboratorio.Pdf_base_64')->with('pdf',$pdf);
     
    //    return redirect('url_lab/'.$pdf);
      
   }
   public function url_lab(){
    return view('Laboratorio.Laboratorio.pdf_wo');
   }
    public function tablaestudios($id,$id_estudio){
        $toma =  EstudioProgramado::find($id);
         $estudios=Estudios::find($id_estudio);
         $empleado=Empleado::find($toma->empleado_id);
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios,
            'empleado'=>$empleado
        );
        return view('Empresa/Estudios')->with('toma',$toma,200);
       
    }
    public function estudio($codigo){
    $estudios=Estudios::where('codigo',$codigo)->get();
    return \json_decode($estudios);

    }
    public function programar_estudio(Request $request){
        $id=$request->id_paciente;
        $estudio_programado=EstudioProgramado::where('folio',$request->folio)->first();
        if ($estudio_programado==null){
        $empresa=$request->empresa_id;
        $elements=$request->estudiosempleado;
        $fecha_a=$request->fechaalta;
        $estudios_por_programar = [];
            foreach ($elements as $element)
            {
                $estudios=Estudios::where('codigo',(int)$element)->get();
                foreach ($estudios as $estudio){
                    $id_est=$estudio->id;
                }
                
            array_push($estudios_por_programar, (int)$id_est);
            }
        $token = Hash::make(Str::random(4));
        $estudios_JSON = json_encode($estudios_por_programar);
        $estudio_programado = new EstudioProgramado;
        $estudio_programado->estudios = $estudios_JSON;
        $estudio_programado->folio = $request->folio;
        $estudio_programado->empleado_id =  $id;
        $estudio_programado->status = 0;
        $estudio_programado->empresa_id =  $empresa;
        $estudio_programado->token = $token;
        $estudio_programado->fecha_inicial=$fecha_a;
        // $estudio_programado->folio = 'HS-'.time();
        $estudio_programado->save();
       return json_encode($estudio_programado);
        }
       else{
            abort(404);
       }


    }
    public function delete_estudio_pro($id)
    {
            $programado = EstudioProgramado::find($id);
            $programado->delete();
            
            return compact("programado");
       
    }
    public function editar_estudio(Request $request){
        $id_estudio_pro = $request->id;
        $estudio_programado = EstudioProgramado::find($id_estudio_pro);
        
        if (!empty($estudio_programado)) {
            $id=$request->id_paciente;
            $empresa=$request->empresa_id;
            $elements=$request->estudiosempleado;
            $fecha_a=$request->fechaalta;
            $estudios_por_programar = [];
            foreach ($elements as $element)
            {
                $estudios=Estudios::where('codigo',(int)$element)->get();
                foreach ($estudios as $estudio){
                    $id_est=$estudio->id;
                }
                
            array_push($estudios_por_programar, (int)$id_est);
            }
            $token = Hash::make(Str::random(4));
            $estudios_JSON = json_encode($estudios_por_programar);
            $estudio_programado->estudios = $estudios_JSON;
            $estudio_programado->folio = $request->folio;
            $estudio_programado->empleado_id =  $id;
            $estudio_programado->status = 0;
            $estudio_programado->empresa_id =  $empresa;
            $estudio_programado->token = $token;
            $estudio_programado->fecha_inicial=$fecha_a;
            // $estudio_programado->folio = 'HS-'.time();
            $estudio_programado->save();
            return json_encode($estudio_programado);
        }
        return json_encode($estudio_programado);
        
      
    }
    
    /*public function estudios_id_folio($id)
    {
        $estudios=Estudios::findOrFail($id);
        return view('Empresa.daicom',compact('estudios'));
        /*return view('Empresa.daicom')
        ->with('id',$id)
        ->with('folio',$folio);
        $buscar = $request->get('buscarpor');
        $tipo = $request->get('tipo');
        $personas = Estudios::buscarpor($id,$buscar)->Paginate(5);

        return view ('persona.index',compact('personas'));
       
    }*/

}
