<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

class IndicadoresController extends Controller {

  /**
   * Indicadores de un consultorio
   * @return vista
   */
  public function index(){
    $medicos =  $this->dataMedicos();
    return view('Empresa/indicadores/consultorio',compact('medicos'));
  }

  public function dataMedicos(){

    $empresa = Session::get('empresa');
    return $users = User::select('id','nombre')
    ->where('empresa_id',$empresa->id)
    ->count();


  }


}
