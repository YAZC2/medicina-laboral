<?php
namespace App\Http\Controllers\Empresa;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;
use App\Empresa;
use App\Empleado;
use App\EstudioProgramado;
use App\Grupo;
use App\Categoria;
use App\Estudios;


class EstudiosController extends Controller
{
    public function estudios(Request $request){
        $term = $request->get('term');
        return Estudios::where('nombre','like', '%'.$term.'%')
        ->get();
    }
}
