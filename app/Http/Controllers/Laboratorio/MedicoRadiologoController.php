<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;

use App\RegistroEntrada;
use App\EstudioProgramado;
use App\Expediente;
use App\Resultados;
use App\Empleado;
use App\Imagenologia;
use App\Estudios;
use App\Archivo;
use App\HistorialClinico;

class MedicoRadiologoController extends Controller {
    public function __construct($value = '') {

    }

    public function permiso() {
        if (Session::get('permiso') != 'radiologo') {
            return abort(403);
        }
    }

    public function radiologia() {
        $this->permiso();
        Session::get('permiso');
        $pacientes = $this->getPacientes();

        return view('Laboratorio/MedicoRadiologo/listaPacientes', [
            'pacientes' => $pacientes
        ]);
    }

    public function pacienteEstudios($id_encrypt) {
        $this->permiso();

        try {
            $id = decrypt($id_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }
        $dicomActually = $this->getEstudiosActually($id);
        //$dicomOld = $this->getEstudiosOld($id);
        $empleado = $this->getEmpleado($id);
        return view('Laboratorio/MedicoRadiologo/EstudiosPaciente', [
            'estudios' => $dicomActually,
          //  'estudios_old' => $dicomOld,
            'paciente' => $empleado
        ]);
    }

    public function infoEstudio($id_encrypt) {
        $this->permiso();
        try {
            $id = decrypt($id_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $registro = RegistroEntrada::find($id);
        $estudio = Estudios::find($registro->estudios_id);
        $dicom = $this->getDicom($registro->estudios_id, $registro->empleado_id);
        $empleado = $this->getEmpleado($registro->empleado_id);

        return view('Laboratorio/MedicoRadiologo/Estudio', [
            'paciente' => $empleado,
            'estudio' => $estudio,
            'dicoms' => $dicom,
            'registro' => $registro
        ]);
    }

    public function getDicom($estudio,$empleado_id) {
        $this->permiso();
        return  Imagenologia::where([
            ['empleado_id', $empleado_id],
            ['resultado_id', NULL],
            ['estudio_id', $estudio]
        ])->get();
    }


    public function getImage($filname) {
        $this->permiso();
        $file = Storage::disk('imagenologia')->get($filname);
        return new Response($file, 200);
    }


    public function getEstudiosActually($id) {
        $this->permiso();
        return RegistroEntrada::select('registrosentradas.id', 'es.nombre', 'registrosentradas.categoria_id', 'registrosentradas.updated_at')
            ->where([
                ['empleado_id', $id],
                ['status', 1],
                ['registrosentradas.categoria_id', 2]
            ])
            ->join('estudios as es', 'registrosentradas.estudios_id', '=', 'es.id')
            ->get();
    }

    public function getEstudiosOld($id) {
        $this->permiso();
        $expedientes = Expediente::select('id')
            ->where([
                ['status', 'finalizado'],
                ['empleado_id', $id]
            ])
            ->get();

        $expedientes_id = [];
        foreach ($expedientes as $expediente) {
            array_push($expedientes_id,$expediente->id);
        }

        return Resultados::select('resultados.created_at', 'resultados.comentarios', 'es.nombre', 'resultados.categoria_id')
            ->where('resultados.categoria_id', 2)
            ->whereIn('expediente_id', $expedientes_id)
            ->join('estudios as es', 'resultados.estudio_id', '=', 'es.id')
            ->get();
    }

    public function getEmpleado($id) {
        $this->permiso();
        return Empleado::where('id', $id)->first();
    }

    public function getPacientes() {
        $this->permiso();
        return RegistroEntrada::select('emp.id', 'emp.nombre', 'emp.CURP', 'emp.apellido_materno as apm', 'emp.apellido_paterno as app', 'emp.genero', 'emp.fecha_nacimiento')
            ->distinct('registrosentradas.empleado_id')
            ->where([
                ['status', 1],
                ['categoria_id', 2]
            ])
            ->join('empleados as emp', 'emp.id', '=', 'registrosentradas.empleado_id')
            ->get();
    }

    public function getExpediente($empleado){
      $expediente = Expediente::where([
          ['curp', $empleado->CURP],
      ])->first();

      if ($expediente == null) {
          $expediente_new = new Expediente;
          //$expediente_new->fecha = \Carbon::now();
          $expediente_new->curp = $empleado->CURP;
          $expediente_new->save();
          $expediente_id = $expediente_new->id;
      } else {
          $expediente_id = $expediente->id;
      }
      return $expediente_id;
    }

    public function historial($id){
      $historial = HistorialClinico::where('origen',1)
      ->where('estado','proceso')
      ->where('expediente_id',$id)
      ->latest()
      ->first();
      if ($historial ==null) {
        $hist = new HistorialClinico;
        $hist->expediente_id = $id;
        $hist->origen = 1;
        $hist->estado = 'proceso';
        $hist->save();
        $historial_id = $hist->id;
      }else {
        $historial_id = $historial->id;
      }

      return $historial_id;
    }

    public function guardarObservacion(Request $request) {
        $this->permiso();
        $this->validate($request, [
            'empleado_id' => 'required',
            'estudio_id' => 'required',
            'interpretacion' => 'required',
            'registro_id' => 'required'
        ]);

        try {
            $empleado_id = decrypt($request->get('empleado_id'));
            $estudio_id = decrypt($request->get('estudio_id'));
            $entrada_id = decrypt($request->get('registro_id'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        $comentarios = $request->get('interpretacion');

        $empleado = Empleado::find($empleado_id);
        $expediente_id = $this->getExpediente($empleado);
        // Obtenemos el id de la cita de estudios programados.
        $programacion = EstudioProgramado::where([
            ['empleado_id', $empleado_id],
            ['status', 1]
        ])->first();

        $look = EstudioProgramado::where([
            ['empleado_id', $empleado_id],
            ['status', 3]
        ])->first();

        if ($look) {
            $jsonParseEstudio = NUll;
        } else {
            $jsonParseEstudio = count(json_decode($programacion->estudios));
        }

        $historial_id = $this->historial($expediente_id);
        // NOTE: Historial Clinico

        // Generamos los resultados del estudio.
        $result = new Resultados;
        $result->comentarios = NULL;
        $result->hc = $historial_id;
        $result->estudio_id = $estudio_id;
        $result->categoria_id = 2;
        $result->estudiosProgramados_id = $programacion->id;
        $result->empresa_id = $programacion->empresa_id;
        $result->save();
        $result_id = $result->id;

        //Enviamos el PDF del resultado.
        $archivo = new Archivo;
        $archivo->archivo = base64_encode($comentarios);
        $archivo->tipo = 'html';
        $archivo->resultado_id = $result_id;
        $archivo->compartir = 'si';
        $archivo->save();

        // Confirmamos el estudios finalizado.
        $registro_entrada_update = RegistroEntrada::find($entrada_id);
        $registro_entrada_update->status = 2;
        $registro_entrada_update->save();

        // Asignación de imagenes dicom los resultados.
        $dicom =  Imagenologia::where([
            ['empleado_id', $empleado_id],
            ['resultado_id', null],
            ['estudio_id', $estudio_id]
        ])->update([
            'resultado_id' => $result_id
        ]);

        $result_prog = Resultados::where('estudiosProgramados_id', $programacion->id)->count();
        if ($result_prog == $jsonParseEstudio) {
            // Actualizamos el status a 2 a tabla estudiosProgramados para indicar que ha finalizado.
            $programacion->status = 2;
            $programacion->save();

            // Declaramos como historial finalizado.
            $hclinico = HistorialClinico::find($historial_id);
            $hclinico->estado = 'finalizado';
            $hclinico->save();
        }

        return json_encode($request->get('empleado_id'));
    }





}
