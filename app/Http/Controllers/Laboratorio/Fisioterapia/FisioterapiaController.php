<?php

namespace App\Http\Controllers\Laboratorio\Fisioterapia;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;
use App\Estudios;
use App\Empresa;
use App\HistorialClinico;
use App\Fisioterapia;
use App\datosUserHtds;
use App\EstudioEspirometria;

use DB;

class FisioterapiaController extends Controller
{
    public function fisio($curp, $id_estu)
    {
        $empleado = Empleado::where("curp", $curp)->first();
        $estudio = EstudioProgramado::where('id', $id_estu)->first();
        $fisioterapia = $this->obten_fisio($curp, $estudio->folio);
        if ($fisioterapia == null) {
            $fisioterapia = new Fisioterapia();
            $fisioterapia->nim = $estudio->folio;
            $fisioterapia->curp = $curp;
            $fisioterapia->save();
        }
        $empresa = Empresa::find($empleado->empresa_id);
        $user_htds = datosUserHtds::where('ID_empleado', Session::get('IdEmpleado'))->first();
        return view('Laboratorio.Medicina.fisioterapia.fisioterapia')
            ->with('empleado', $empleado)
            ->with('fisioterapia', $fisioterapia)
            ->with('user_htds', $user_htds)
            ->with('empresa', $empresa)
            ->with('estudio', $estudio);
    }
    public function obten_fisio($curp, $nim)
    {
        return Fisioterapia::where('curp', $curp)
            ->where('nim', $nim)
            ->orderBy('created_at', 'desc')
            ->first();
    }
    public function registrar_fisio(Request $request){
    //     $fisioterapia = Fisioterapia::where('curp', $request->curp)
    //   ->orWhere('nim', $request->nim)
    //   ->orderBy('created_at', 'desc')
    //   ->first();
      $fisioterapia=new Fisioterapia;
      $fisioterapia->nim=$request->nim;
      $fisioterapia->curp=$request->curp;
      $fisioterapia->oswestry1=$request->oswestry1;
      $fisioterapia->oswestry2=$request->oswestry2;
      $fisioterapia->oswestry3=$request->oswestry3;
      $fisioterapia->oswestry4=$request->oswestry4;
      $fisioterapia->oswestry5=$request->oswestry5;
      $fisioterapia->oswestry6=$request->oswestry6;
      $fisioterapia->oswestry7=$request->oswestry7;
      $fisioterapia->oswestry8=$request->oswestry8;
      $fisioterapia->oswestry9=$request->oswestry9;
      $fisioterapia->oswestry10=$request->oswestry10;
      $fisioterapia->dolor_espalda_baja=$request->dolor_espalda_baja;
      $fisioterapia->dolor_area_sombreada=$request->dolor_area_sombreada;
      $fisioterapia->dias_parte_baja=$request->dias_parte_baja;
      $fisioterapia->dia_mas_dolio=$request->dia_mas_dolio;
      $fisioterapia->baja_zona_lumbar=$request->baja_zona_lumbar;
      $fisioterapia->lumbar_pierna=$request->lumbar_pierna;
      $fisioterapia->doctor_consultado=$request->doctor_consultado;
      $fisioterapia->fisioterapia_consultado=$request->fisioterapia_consultado;
      $fisioterapia->enfermera_consultado=$request->enfermera_consultado;
      $fisioterapia->otro_consultado=$request->otro_consultado;
      $fisioterapia->ninguno_consultado=$request->ninguno_consultado;
      $fisioterapia->s_h_flexion_i=$request->s_h_flexion_i;
      $fisioterapia->s_h_extension_i=$request->s_h_extension_i;
      $fisioterapia->s_h_abduccion_i=$request->s_h_abduccion_i;
      $fisioterapia->s_h_r_externa_i=$request->s_h_r_externa_i;
      $fisioterapia->s_h_r_interna_i=$request->s_h_r_interna_i;
      $fisioterapia->s_h_flexion_d=$request->s_h_flexion_d;
      $fisioterapia->s_h_extension_d=$request->s_h_extension_d;
      $fisioterapia->s_h_abduccion_d=$request->s_h_abduccion_d;
      $fisioterapia->s_h_r_externa_d=$request->s_h_r_externa_d;
      $fisioterapia->s_h_r_interna_d=$request->s_h_r_interna_d;
      $fisioterapia->s_c_flexion_i=$request->s_c_flexion_i;
      $fisioterapia->s_c_pronacion_i=$request->s_c_pronacion_i;
      $fisioterapia->s_c_supinacion_i=$request->s_c_supinacion_i;
      $fisioterapia->s_c_flexion_d=$request->s_c_flexion_d;
      $fisioterapia->s_c_pronacion_d=$request->s_c_pronacion_d;
      $fisioterapia->s_c_supinacion_d=$request->s_c_supinacion_d;
      $fisioterapia->s_m_flexion_i=$request->s_m_flexion_i;
      $fisioterapia->s_m_extension_i=$request->s_m_extension_i;
      $fisioterapia->s_m_des_radial_i=$request->s_m_des_radial_i;
      $fisioterapia->s_m_des_cubital_i=$request->s_m_des_cubital_i;
      $fisioterapia->s_m_flexion_d=$request->s_m_flexion_d;
      $fisioterapia->s_m_extension_d=$request->s_m_extension_d;
      $fisioterapia->s_m_des_radial_d=$request->s_m_des_radial_d;
      $fisioterapia->s_m_des_cubital_d=$request->s_m_des_cubital_d;
      $fisioterapia->i_ca_flexion_i=$request->i_ca_flexion_i;
      $fisioterapia->i_ca_extension_i=$request->i_ca_extension_i;
      $fisioterapia->i_ca_abduccion_i=$request->i_ca_abduccion_i;
      $fisioterapia->i_ca_aduccion_i=$request->i_ca_aduccion_i;
      $fisioterapia->i_ca_r_externa_i=$request->i_ca_r_externa_i;
      $fisioterapia->i_ca_r_interna_i=$request->i_ca_r_interna_i;
      $fisioterapia->i_ca_flexion_d=$request->i_ca_flexion_d;
      $fisioterapia->i_ca_extension_d=$request->i_ca_extension_d;
      $fisioterapia->i_ca_abduccion_d=$request->i_ca_abduccion_d;
      $fisioterapia->i_ca_aduccion_d=$request->i_ca_aduccion_d;
      $fisioterapia->i_ca_r_externa_d=$request->i_ca_r_externa_d;
      $fisioterapia->i_ca_r_interna_d=$request->i_ca_r_interna_d;
      $fisioterapia->i_r_flexion_i=$request->i_r_flexion_i;
      $fisioterapia->i_r_extension_i=$request->i_r_extension_i;
      $fisioterapia->i_r_flexion_d=$request->i_r_flexion_d;
      $fisioterapia->i_r_extension_d=$request->i_r_extension_d;
      $fisioterapia->i_t_flexion_i=$request->i_t_flexion_i;
      $fisioterapia->i_t_extension_i=$request->i_t_extension_i;
      $fisioterapia->i_t_inversion_i=$request->i_t_inversion_i;
      $fisioterapia->i_t_eversion_i=$request->i_t_eversion_i;
      $fisioterapia->i_t_flexion_d=$request->i_t_flexion_d;
      $fisioterapia->i_t_extension_d=$request->i_t_extension_d;
      $fisioterapia->i_t_inversion_d=$request->i_t_inversion_d;
      $fisioterapia->i_t_eversion_d=$request->i_t_eversion_d;
      $fisioterapia->num_revision=$request->num_revision;
      $fisioterapia->resumen_valoracion=$request->resumen_valoracion;
      $fisioterapia->save();
      return compact($fisioterapia,'fisioterapia');

    }
}
