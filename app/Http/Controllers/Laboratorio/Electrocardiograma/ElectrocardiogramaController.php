<?php

namespace App\Http\Controllers\Laboratorio\Electrocardiograma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;
use App\Estudios;
use App\Empresa;
use App\HistorialClinico;
use App\Espirometria;
use App\datosUserHtds;
use App\EstudioEspirometria;
use App\Electrocardiograma;
use DB;


class ElectrocardiogramaController extends Controller
{
    
    public function electrocardiograma($curp, $id_estu)
  {
    $paciente = Empleado::where('curp', $curp)->first();

    $estudio = EstudioProgramado::where('id', $id_estu)->first();
    $electrocardiograma = Electrocardiograma::where('curp', $curp)
      ->where('nim', $estudio->folio)
      ->orderBy('created_at', 'desc')
      ->first();
    if ($electrocardiograma == null) {
      $electrocardiograma = new Electrocardiograma();
      $electrocardiograma->curp = $curp;
      $electrocardiograma->nim = $estudio->folio;
      $electrocardiograma->save();
    }
    return view('Laboratorio.Medicina.Electrocardiograma.Electrocariograma')
      ->with('paciente', $paciente)
      ->with('estudio', $estudio)
      ->with('electrocardiograma', $electrocardiograma);
  }
  public function save_electro(Request $request){
   
    $electrocardiograma = Electrocardiograma::where('curp', $request->curp)
      ->where('nim', $request->nim)
      ->orderBy('created_at', 'desc')
      ->first();
      $antiguo_documento1=null;
      $antiguo_documento2=null;
      $antiguo_interpretacionpdf=null;
    if ($electrocardiograma != null) {
     
      $antiguo_documento1 = $electrocardiograma->documento1;
      $antiguo_documento2 = $electrocardiograma->documento2;
      $antiguo_interpretacionpdf = $electrocardiograma->interpretacionpdf;
    
    }
    $estudioelectro = new Electrocardiograma();

    $estudioelectro=new Electrocardiograma();
    $estudioelectro->curp=$request->curp;
    $estudioelectro->nim=$request->nim;
    $estudioelectro->id_paciente=$request->id_paciente;
    $estudioelectro->frecuencia=$request->frecuencia;
    $estudioelectro->eje_p=$request->eje_p;
    $estudioelectro->ejeqrs=$request->ejeqrs;
    $estudioelectro->ejet=$request->ejet;
    $estudioelectro->rr=$request->rr;
    $estudioelectro->pr=$request->pr;
    $estudioelectro->qrs=$request->qrs;
    $estudioelectro->qtm=$request->qtm;
    $estudioelectro->qtc=$request->qtc;
    $estudioelectro->observaciones=$request->observaciones;
    $estudioelectro->conclusiones=$request->conclusiones;
    $estudioelectro->notas=$request->notas;
    $estudioelectro->tipo_resultado=$request->tipo_resultado;
    $archivo = $request->file('archivo');
    $archivo2 = $request->file('archivo1');
    $interpretacionpdf = $request->file('interpretacionpdf');
    $estudioelectro->documento1 = $this->upload_archivo($archivo, $antiguo_documento1);
    $estudioelectro->documento2 = $this->upload_archivo($archivo2, $antiguo_documento2);
    $estudioelectro->interpretacionpdf = $this->upload_archivo($interpretacionpdf, $antiguo_interpretacionpdf);
    $estudioelectro->save();
    return json_encode($estudioelectro);
  }
  public function upload_archivo($archivo, $antiguo_documento)
  {
    $archivo_nombre = '';
    if ($archivo) {
      Storage::disk('electrocardiograma')->delete($antiguo_documento);
      $archivo_nombre = time() . $archivo->getClientOriginalName();
      Storage::disk('electrocardiograma')->put($archivo_nombre, File::get($archivo));
    } else {
      $archivo_nombre = $antiguo_documento;
    }
    return $archivo_nombre;
  }
  public function trazo($curp,$nim){
    
    // $pdf=\PDF::loadView('PDF/receta',compact('medicamentos','user','json','empresa','paciente'));
    $estudio = EstudioProgramado::where('id', $nim)->first();
    $paciente = Empleado::where('id',$estudio->empleado_id)->first();
    $electrocardiograma = Electrocardiograma::where('curp', $curp)
    ->where('nim', $estudio->folio )
    ->orderBy('created_at', 'desc')
    ->first();
    return view('Laboratorio.Medicina.Electrocardiograma.TomasElectro')
    ->with('paciente', $paciente)
    ->with('electrocardiograma', $electrocardiograma);
  
  }
}