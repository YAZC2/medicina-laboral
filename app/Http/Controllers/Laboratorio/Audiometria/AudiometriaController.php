<?php

namespace App\Http\Controllers\Laboratorio\Audiometria;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;

use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;
use App\Estudios;
use App\HistorialClinico;
use App\Audiometria;
use App\AudiometriaResultado;
use App\datosUserHtds;
use App\Espirometria;
use App\Empresa;
use App\EspirometriaLnn;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AudiometriaController extends Controller
{
    // public function index($id,$registro_entrada_id)
    // {
    //     $empleado = Empleado::where("curp",$id)->first();
    //     $registro_entrada = Registroentrada::find($registro_entrada_id);
    //     if($empleado == null || $registro_entrada == null)
    //     {
    //         abort(404);
    //     }
    //     $empresa = null;
    //     if($empleado->empresa != null)
    //     {
    //         $empresa = $empleado->empresa;
    //     }
    //     $audiometria = Audiometria::where('registro_entrada_id',$registro_entrada->id)->first();

    //     if($audiometria == null)
    //     {
    //         $audiometria = new Audiometria();
    //         $audiometria->registro_entrada_id = $registro_entrada->id;
    //         $audiometria->save();
    //         $this->crear_audiometria_resultados($audiometria->id);
    //     }

    //     $user_htds = datosUserHtds::where('ID_empleado',Session::get('IdEmpleado'))->first();

    //     if($audiometria->resultado_id != null)
    //     {
    //         return redirect()->action('PdfController@resultadoAudiometria', ['id' => $audiometria->id]);
    //     }

    //     return view('Laboratorio/Medicina/audiometria/audiometria')
    //     ->with('empleado', $empleado)
    //     ->with('registro_entrada', $registro_entrada)
    //     ->with('audiometria', $audiometria)
    //     ->with('user_htds', $user_htds)
    //     ->with('empresa', $empresa);
    // }
    public function audiometria($curp,$id_estu)
    {
    
        $empleado = Empleado::where("curp", $curp)->first();
        $estudio = EstudioProgramado::where('id', $id_estu)->first();
        $registro_entrada = Registroentrada::find('2');
        $audiometria=$this->obten_audio($curp,$estudio->folio);
        if($audiometria==null){
            $audiometria = new Audiometria();
            $audiometria->registro_entrada_id = $registro_entrada->id;
            $audiometria->nim=$estudio->folio;
            $audiometria->curp=$curp;
            $audiometria->save();
            $this->crear_audiometria_resultados($audiometria->id);
        }
         
        $empresa = Empresa::find($empleado->empresa_id);
      
        
        $user_htds = datosUserHtds::where('ID_empleado', Session::get('IdEmpleado'))->first();

        // if ($audiometria->resultado_id != null) {
        //     return redirect()->action('PdfController@resultadoAudiometria', ['id' => $audiometria->id]);
        // }

        return view('Laboratorio/Medicina/audiometria/audiometria')
            ->with('empleado', $empleado)
            ->with('registro_entrada', $registro_entrada)
            ->with('audiometria', $audiometria)
            ->with('user_htds', $user_htds)
            ->with('empresa', $empresa)
            ->with('estudio',$estudio);
            
    }
    public function index()
    {
        $empleado = Empleado::where("curp", '781')->first();
        $registro_entrada = Registroentrada::find('2');
        if ($empleado == null || $registro_entrada == null) {
            abort(404);
        }
        $empresa = null;
        if ($empleado->empresa != null) {
            $empresa = $empleado->empresa;
        }
        $audiometria = Audiometria::where('registro_entrada_id', $registro_entrada->id)->first();

        if ($audiometria == null) {
            $audiometria = new Audiometria();
            $audiometria->registro_entrada_id = $registro_entrada->id;
            $audiometria->save();
            $this->crear_audiometria_resultados($audiometria->id);
        }

        $user_htds = datosUserHtds::where('ID_empleado', Session::get('IdEmpleado'))->first();

        if ($audiometria->resultado_id != null) {
            return redirect()->action('PdfController@resultadoAudiometria', ['id' => $audiometria->id]);
        }

        return view('Laboratorio/Medicina/audiometria/audiometria')
            ->with('empleado', $empleado)
            ->with('registro_entrada', $registro_entrada)
            ->with('audiometria', $audiometria)
            ->with('user_htds', $user_htds)
            ->with('empresa', $empresa);
    }
    public function save_audiometria(Request $request)
    {
        $empleado = Empleado::where("curp", $request->curp)->first();
        //    `check_explosion_cercana`, 
        //    `ec_oido`,
        //     `check_audifonos_musica`, 
        //     `am_volumen`, 
        //     `am_no_dias_semana`, 
        //     `am_cuantos_anios`, 
        //     `check_gripa_infancia`, 
        //     `check_gripa_actualidad`, 
        //     `check_infeccion_oidos`, 
        //     `check_hipertension`, 
        //     `check_paralisis_facial`, 
        //     `check_diabetes`, 
        //     `check_eventos`, 
        //     `e_frecuencia`, 
        //     `check_varicela`,
        //      `check_practicas`,
        //       `p_frecuencia`, 
        //       `check_medicamentos`, 
        //       `m_cual`,
        //        `check_familiar_problemas`, 
        //        `registro_entrada_id`, 
        //        `resultado_id`, 
        //        `interpretacion`, 
        //        `imagen`, 
        //        `documento`
        // $registro_entrada = Registroentrada::find($request->registro_id);

        // if($empleado == null || $registro_entrada == null)
        // {
        //     abort(404);
        // }

       
        $audiometria = Audiometria::find($request->audiometria_id);
            // if($audiometria==null){
            //     $audiometria=new Audiometria();
            // }
            $audiometria->curp = $request->curp;
            $audiometria->puesto_trabajo_solicitado = $request->puesto_trabajo;
            $audiometria->nim = $request->nim;

            if (isset($request->proteccion_auditiva)) {
                $audiometria->check_proteccion_auditiva = $request->proteccion_auditiva;
            } else {
                $audiometria->check_proteccion_auditiva = null;
            }

            if (isset($request->tce)) {
                $audiometria->check_tce = $request->tce;
            } else {
                $audiometria->check_tce = null;
            }

            if (isset($request->molestia_oido)) {
                $json_molestias = array();
                foreach ($request->molestia_oido as $value) {
                    array_push($json_molestias, $value);
                }
                $audiometria->molestias_oido = implode(",", $json_molestias);
            } else {
                $audiometria->molestias_oido = null;
            }
            $audiometria->tiempo_trabajando_exposicion = $request->tiempo_trabajando_exposion_ruido;

            $audiometria->tiempo_puesto = $request->tiempo_puesto;

            if (isset($request->explosion_cercana)) {
                $audiometria->check_explosion_cercana = $request->explosion_cercana;
                $audiometria->ec_oido = $request->explosion_oido;
            } else {
                $audiometria->check_explosion_cercana = null;
                $audiometria->ec_oido = null;
            }

            if (isset($request->audifonos_musica)) {
                $audiometria->check_audifonos_musica = $request->audifonos_musica;
                $audiometria->am_volumen = $request->audifonos_musica_volumen;
                $audiometria->am_no_dias_semana = $request->audifonos_musica_no_dias;
                $audiometria->am_cuantos_anios = $request->audifonos_musica_anios;
            } else {
                $audiometria->check_audifonos_musica = null;
                $audiometria->am_volumen = null;
                $audiometria->am_no_dias_semana = null;
                $audiometria->am_cuantos_anios = null;
            }

            if (isset($request->gripa_infancia)) {
                $audiometria->check_gripa_infancia = $request->gripa_infancia;
            } else {
                $audiometria->check_gripa_infancia = null;
            }

            if (isset($request->gripa_actualidad)) {
                $audiometria->check_gripa_actualidad = $request->gripa_actualidad;
            } else {
                $audiometria->check_gripa_actualidad = null;
            }

            if (isset($request->infeccion_oidos)) {
                $audiometria->check_infeccion_oidos = $request->infeccion_oidos;
            } else {
                $audiometria->check_infeccion_oidos = null;
            }

            if (isset($request->diabetes)) {
                $audiometria->check_diabetes = $request->diabetes;
            } else {
                $audiometria->check_diabetes = null;
            }

            if (isset($request->hipertension)) {
                $audiometria->check_hipertension = $request->hipertension;
            } else {
                $audiometria->check_hipertension = null;
            }

            if (isset($request->paralisis_facial)) {
                $audiometria->check_paralisis_facial = $request->paralisis_facial;
            } else {
                $audiometria->check_paralisis_facial = null;
            }

            if (isset($request->acude_eventos)) {
                $audiometria->check_eventos = $request->acude_eventos;
                $audiometria->e_frecuencia = $request->acude_eventos_frecuencia;
            } else {
                $audiometria->check_eventos = null;
                $audiometria->e_frecuencia = null;
            }

            if (isset($request->varicela)) {
                $audiometria->check_varicela = $request->varicela;
            } else {
                $audiometria->check_varicela = null;
            }

            if (isset($request->practica_actividades)) {
                $audiometria->check_practicas = $request->practica_actividades;
                $audiometria->p_frecuencia = $request->practica_actividades_frecuencia;
            } else {
                $audiometria->check_practicas = null;
                $audiometria->p_frecuencia = null;
            }

            if (isset($request->medicamentos_frecuentes)) {
                $audiometria->check_medicamentos = $request->medicamentos_frecuentes;
                $audiometria->m_cual = $request->medicamentos_frecuentes_cuales;
            } else {
                $audiometria->check_medicamentos = null;
                $audiometria->m_cual = null;
            }

            if (isset($request->familiar_problemas_audicion)) {
                $audiometria->check_familiar_problemas = $request->familiar_problemas_audicion;
            } else {
                $audiometria->check_familiar_problemas = null;
            }
            $archivo = $request->file('archivo');

            $antiguo_documento = $audiometria->documento;
            if ($antiguo_documento) Storage::disk('audiometria')->delete($antiguo_documento);
            $audiometria->documento = $this->upload_archivo($archivo);
            $audiometria->firma=$request->sigImageData;



            $audiometria->save();
        




        return response()->json($audiometria, 200);
    }
    public function obten_audio($curp,$nim){
        return Audiometria::where('curp', $curp)
      ->where('nim', $nim)
      ->orderBy('created_at', 'desc')
      ->first();  
    }
    public function upload_archivo($archivo)
    {
        $archivo_nombre = '';
        if ($archivo) {
            $archivo_nombre = time() . $archivo->getClientOriginalName();
            Storage::disk('audiometria')->put($archivo_nombre, File::get($archivo));
        } else {
            $archivo_nombre = 'null';
        }
        return $archivo_nombre;
    }

    public function crear_audiometria_resultados($audiometria_id)
    {
        $array_hz = [125, 250, 500, 1000, 2000, 3000, 4000, 6000, 8000];
        $array_oidos = ["izquierdo", "derecho"];
        foreach ($array_oidos as $oido) {
            foreach ($array_hz as $hz) {
                $new_resultado = new AudiometriaResultado();
                $new_resultado->oido = $oido;
                $new_resultado->frecuencia = $hz;
                $new_resultado->desibelios = 0;
                $new_resultado->oido_v = $oido;
                $new_resultado->frecuencia_v = $hz;
                $new_resultado->desibelios_v = 0;
                $new_resultado->audiometria_id = $audiometria_id;
                $new_resultado->save();
            }
        }
    }

    public function getResultados($audiometria_id, $oido)
    {
        $audiometria = Audiometria::find($audiometria_id);
        if ($audiometria == null) {
            return response()->json("Error", 500);
        }

        $array = $audiometria->resultados->where('oido', $oido)->values();

        return response()->json($array, 200);
    }

    public function guardarResultado(Request $request)
    {
        // dd($request);
        $audiometria = Audiometria::find($request->audiometria_id);
        
        if ($audiometria == null) {
            return response()->json("Error", 500);
        }

        $oido = ($request->oido == "d" ? "derecho" : "izquierdo"); 
        $oido_v = ($request->oido_v == "d" ? "derecho" : "izquierdo");
        if($request->oido==null){
            $resultado = AudiometriaResultado::where('audiometria_id', $request->audiometria_id)->where('oido_v', $oido_v)->where('frecuencia_v', $request->hz_v)->first();

            if ($resultado == null) {
                return response()->json($request->oido, 500);
            }
            else{
                // $resultado = new AudiometriaResultado;
                $resultado->audiometria_id = $request->audiometria_id;
                $resultado->oido_v = $oido_v;
                $resultado->desibelios_v = $request->valor;
            }
        }
        else{
            $resultado = AudiometriaResultado::where('audiometria_id', $request->audiometria_id)->where('oido', $oido)->where('frecuencia', $request->hz)->first();

            if ($resultado == null) {
                return response()->json($request->oido, 500);
            }
            else{
                // $resultado = new AudiometriaResultado;
                $resultado->audiometria_id = $request->audiometria_id;
                $resultado->oido = $oido;
                $resultado->desibelios = $request->valor;
            }
        }
        

        $resultado->save();

        return response()->json($resultado, 200);
    }

    public function guardarFinal(Request $request)
    {
        $audiometria = Audiometria::find($request->audiometria_id);
        if ($audiometria == null) {
            return response()->json($request->audiometria_id, 500);
        }

        if (isset($request->interpretacion)) {
            $audiometria->interpretacion = $request->interpretacion;
        }
        if(isset($request->tipo_resultado)){
            $audiometria->tipo_resultado=$request->tipo_resultado;
        }
        if(isset($request->causa)){
            $audiometria->causa=$request->causa;
        }
        if(isset($request->otoscopia)){
            $audiometria->otoscopia=$request->otoscopia;
        }
        if(isset($request->IdMedico))
        {
            $audiometria->IdMedico=$request->IdMedico;
        }
        
        
        if (isset($request->oido_derecho_sel)) {
            $json_derecho_sel = array();
            foreach ($request->oido_derecho_sel as $value) {
                array_push($json_derecho_sel, $value);
            }
            $audiometria->oido_derecho_sel = implode(",", $json_derecho_sel);
        } else {
            $audiometria->oido_derecho_sel = null;
        }
        if (isset($request->oido_izquierdo_sel)) {
            $json_oido_izquierdo_sel = array();
            foreach ($request->oido_izquierdo_sel as $value) {
                array_push($json_oido_izquierdo_sel, $value);
            }
            $audiometria->oido_izquierdo_sel = implode(",", $json_oido_izquierdo_sel);
        } else {
            $audiometria->oido_izquierdo_sel = null;
        }
        


        $estudio_programado = EstudioProgramado::where('empleado_id', $request->empleado_id)
            ->whereIn('status', [0, 1])
            ->first();

        $empleado = Empleado::find($request->empleado_id);

        // dd($estudio_programado);
        // dd($empleado->expediente);
        // dd($audiometria->registroEntrada);


        $resultado = new Resultados();
        $resultado->comentarios = '';
        // $resultado->estudio_id = $audiometria->registroEntrada->estudios_id;
        $resultado->expediente_id = 6;
        // $resultado->categoria_id = $audiometria->registroEntrada->categoria_id;
        // $resultado->estudiosProgramados_id = $estudio_programado->id;
        // $resultado->empresa_id =$request->empleado_id;

        $resultado->medico_id = Session::get('IdEmpleado');
        $resultado->save();

        // $audiometria->registroEntrada->status = 2;
        // $audiometria->registroEntrada->save();

        $audiometria->resultado_id = $resultado->id;

        $audiometria->save();

        return response()->json($audiometria, 200);
    }

    public function guardarFirma(Request $request)
    {
        $user_htds = datosUserHtds::where('ID_empleado', Session::get('IdEmpleado'))->first();
        $user_htds->cedula = $request->cedula;
        $user_htds->nombre_completo = $request->nombre_completo;
        $user_htds->titulo_profesional = $request->titulo_profesional;
        $user_htds->save();

        if ($request->file('firma') != null) {
            $file = $request->file('firma');

            $file_name = time() . $file->getClientOriginalName();
            Storage::disk('datosHtds')->put($file_name, File::get($file));

            $antigua = $user_htds->firma;
            if ($antigua) Storage::disk('datosHtds')->delete($antigua);
            $user_htds->firma = $file_name;
            $user_htds->save();
        }
        return response()->json($user_htds, 200);
    }
}