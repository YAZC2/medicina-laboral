<?php

namespace App\Http\Controllers\Laboratorio\Espirometria;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;
use App\Estudios;
use App\Empresa;
use App\HistorialClinico;
use App\Espirometria;
use App\datosUserHtds;
use App\EstudioEspirometria;

use DB;

class EspirometriaController extends Controller
{
  public function espirometria($curp, $id_estu)
  {
    $paciente = Empleado::where('curp', $curp)->first();
    $empresa=Empresa::where('id',$paciente->empresa_id)->first();
    $estudio = EstudioProgramado::where('id', $id_estu)->first();
    $espirometria = EstudioEspirometria::where('curp', $curp)
      ->orWhere('nim', $estudio->folio)
      ->orderBy('created_at', 'desc')
      ->first();
    if ($espirometria == null) {
      $espirometria = new EstudioEspirometria();
      $espirometria->curp = $curp;
      $espirometria->save();
    }
    return view('Laboratorio/Medicina/espirometria/EstudioEspirometria')
      ->with('paciente', $paciente)
      ->with('estudio', $estudio)
      ->with('empresa', $empresa)
      ->with('espirometria', $espirometria);
  }
  public function save_espirometria(Request $request)
  {
    $empleado = Empleado::where("curp", $request->curp)->first();
    $id_empleado = $empleado->id;
    $espirometria = EstudioEspirometria::where('curp', $request->curp)
      ->orWhere('nim', $request->nim)
      ->orderBy('created_at', 'desc')
      ->first();
    if ($espirometria != null) {
      $antiguo_documento = $espirometria->documento;
    }
   
    $estudioesp = new EstudioEspirometria();
    $estudioesp->nim = $request->nim;
    $estudioesp->curp = $request->curp;
    $estudioesp->id_paciente = $id_empleado;
    $estudioesp->comentarios = $request->comentarios;
    $estudioesp->p_trabajo = $request->p_trabajo;
    $estudioesp->quimico = $request->quimico;
    $estudioesp->t_puesto = $request->t_puesto;
    $estudioesp->cirugia = $request->cirugia;
    $estudioesp->cirugia_ojos = $request->cirugia_ojos;
    $estudioesp->infarto = $request->infarto;
    $estudioesp->tuberculosis = $request->tuberculosis;
    $estudioesp->embarazada= $request->embarazada;
    $estudioesp->tos_sangre= $request->tos_sangre;
    $estudioesp->neumotorax= $request->neumotorax;
    $estudioesp->Traqueostomia= $request->Traqueostomia;
    $estudioesp->sonda= $request->sonda;
    $estudioesp->Aneurisma= $request->Aneurisma;
    $estudioesp->TROMBOEMBOLIA= $request->TROMBOEMBOLIA;
    $estudioesp->VOMITO= $request->VOMITO;
    $estudioesp->DILATADOR= $request->DILATADOR;
    $estudioesp->	ALIMENTOS= $request->ALIMENTOS;
    $estudioesp->	EJERCICIO= $request->EJERCICIO;
    $estudioesp->cuando_infarto=$request->cuando_infarto;
    $estudioesp->tipo_resultado=$request->tipo_resultado;
    $estudioesp->firma=$request->firma;
    $archivo = $request->file('archivo');


    $estudioesp->documento = $this->upload_archivo($archivo, $antiguo_documento);

    $estudioesp->save();
    return json_encode($estudioesp);
  }
  public function upload_archivo($archivo, $antiguo_documento)
  {
    $archivo_nombre = '';
    if ($archivo) {
      Storage::disk('espirometria')->delete($antiguo_documento);
      $archivo_nombre = time() . $archivo->getClientOriginalName();
      Storage::disk('espirometria')->put($archivo_nombre, File::get($archivo));
    } else {
      $archivo_nombre = $antiguo_documento;
    }
    return $archivo_nombre;
  }
  public function view_resultado($curp, $id_estu){
    $estudio = EstudioProgramado::where('id', $id_estu)->first();
    $paciente = Empleado::where('id', $estudio->empleado_id)->first();
    $espirometria = EstudioEspirometria::where('curp', $paciente->CURP)
      ->where('nim', $estudio->folio)
      ->orderBy('created_at', 'desc')
      ->first();
    
    if ($espirometria == null) {
      // $espirometria = new EstudioEspirometria();
      // $espirometria->curp = $curp;
      // $espirometria->save();
     $espirometria=null;
    }
    return view('Laboratorio/Medicina/espirometria/ResultadoEspirometria')
      ->with('paciente', $paciente)
      ->with('estudio', $estudio)
      ->with('espirometria', $espirometria);
  }
  public function index($id)
  {

    $medico = datosUserHtds::where('ID_empleado', Session::get('IdEmpleado'))->first();
    $ingreso = $this->ingreso($id);
    $paciente =  $this->empleado($ingreso->empleado_id);
    $toma = $this->toma($paciente->id);
    $espiro = $this->viewEspirometria($toma->folio);
    $empresa = $this->empresa($paciente->empresa_id);
    return view('Laboratorio/Medicina/espirometria/espirometria', compact('paciente', 'empresa', 'espiro', 'medico', 'ingreso'));
  }

  public function viewEspirometria($folio)
  {
    return Espirometria::where('nim', $folio)->first();
  }

  public function toma($empleadoId)
  {
    return EstudioProgramado::whereIn('status', [0, 1])
      ->where('empleado_id', $empleadoId)
      ->first();
  }

  public function empleado($id)
  {
    return Empleado::find($id);
  }
  public function ingreso($id)
  {
    try {
      $id = decrypt($id);
    } catch (\Throwable $th) {
      return abort(404);
    }
    return $ingreso = RegistroEntrada::find($id);
  }
  public function empresa($id)
  {
    return Empresa::find($id);
  }

  public function solicitud(Request $data)
  {
    DB::connection('has')->table('solicitud')->insert([
      'empresa' => $data->empresaId,
      'paciente_id' => $data->empleadoId,
      'area' => 'espirometria',
      'unidad' => 'movil',
      'medico_id' => Session::get('IdEmpleado')
    ]);
    return "true";
  }

  public function viewPdf($id)
  {
    $espiro = Espirometria::find($id);
    $medico = datosUserHtds::where('ID_empleado', Session::get('IdEmpleado'))->first();
    $pdf = \PDF::loadView('Laboratorio/Medicina/espirometria/espirometriaPdf', compact('espiro', 'medico'));
    return $pdf->stream();
  }

  public function fv_file(Request $data)
  {

    try {
      DB::beginTransaction();
      $espiro = Espirometria::find($data->id);
      $espiro->fv != null ? Storage::disk('espirometria')->delete($espiro->fv) : '';
      $file = $this->fileUpload($data->file('fv'));
      $espiro->fv = $file;
      $espiro->save();
      $data = array(
        'code' => 200,
        'data' => $espiro
      );
      DB::commit();
    } catch (\Throwable $th) {
      DB::rollback();
      $data = array(
        'code' => 500,
        'data' => $th
      );
    }
    return response()->json($data, $data['code']);
  }

  public function vt_file(Request $data)
  {
    try {
      DB::beginTransaction();
      $espiro = Espirometria::find($data->id);
      $espiro->vt != null ? Storage::disk('espirometria')->delete($espiro->vt) : '';
      $file = $this->fileUpload($data->file('vt'));
      $espiro->vt = $file;
      $espiro->save();
      $data = array(
        'code' => 200,
        'data' => $espiro
      );
      DB::commit();
    } catch (\Throwable $th) {
      DB::rollback();
      $data = array(
        'code' => 500,
        'data' => $th
      );
    }
    return response()->json($data, $data['code']);
  }

  public function fileUpload($file)
  {
    if ($file) {
      $nameFile = time() . '_' . $file->getClientOriginalName();
      Storage::disk('espirometria')->put($nameFile, File::get($file));
    } else {
      $nameFile = null;
    }
    return $nameFile;
  }

  public function resultEstudio(Request $data)
  {
    // Verificar si tiene sus datos en la de firma
    try {
      DB::beginTransaction();
      $ingreso = RegistroEntrada::find($data->ingreso);
      $ingreso->status = 2;
      $ingreso->save();

      $empleado = Empleado::find($ingreso->empleado_id);
      $expediente = $this->getExpediente($empleado->CURP);

      $toma = EstudioProgramado::where('empleado_id', $empleado->id)->whereIn('status', [1, 3])->first();
      $estudios = sizeof(json_decode($toma->estudios));

      $resultados = new Resultados();
      $resultados->estudio_id = $ingreso->estudios_id;
      $resultados->medico_id = Session::get('IdEmpleado');
      $resultados->expediente_id = $expediente->id;
      $resultados->categoria_id = $ingreso->categoria_id;
      $resultados->estudiosProgramados_id = $toma->id;
      $resultados->empresa_id = $empleado->empresa_id;
      $resultados->save();

      $espirometria =  Espirometria::find($data->espiroId);
      $espirometria->interpretacion = $data->interpretacion;
      $espirometria->resultado_id = $resultados->id;
      $espirometria->save();

      $tomaCount = Resultados::where('estudiosProgramados_id', $toma->id)->count();
      if ($tomaCount == $estudios) {
        $toma->status = 2;
        $toma->save();
      }

      $data = array(
        'code' => 200,
        'resultado' => $resultados
      );
      DB::commit();
    } catch (\Throwable $th) {
      DB::rollback();
      $data = array(
        'code' => 400,
        'error' => dd($th)
      );
    }

    return response()->json($data, $data['code']);
  }

  public function getExpediente($curp)
  {

    $expediente = Expediente::where('curp', $curp)->first();
    if (!$expediente) {
      $expediente = new Expediente();
      $expediente->curp = $empleado->CURP;
      $expediente->save();
    }
    return $expediente;
  }
}
