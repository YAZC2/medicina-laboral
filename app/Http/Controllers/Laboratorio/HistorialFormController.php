<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;

use App\Empleado;
use App\RegistroEntrada;
use App\EstudioProgramado;
use App\Estudios;
use App\Resultados;
use App\Archivo;
use App\HistorialClinico;
use App\Empresa;
use App\Expediente;

class HistorialFormController extends Controller
{

    public function permiso()
    {
        if (Session::get('permiso') != 'medico') {
            return abort(403);
        }
    }

    public function view($curp_encrypt)
    {
        try {
            $curp = decrypt($curp_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }
        $info = null;
        $empleado = $this->getEmpleado($curp);
        // $historial = $this->getHistorialesClinico($empleado->CURP);
        $historial = $this->getHistorialesClinico($curp);
        $estudios = $this->getEstudios($empleado->id);
        $empresa = Empresa::select('id', 'nombre')->where('giro_id', 4)->get();
        if ($historial) {

            $identificacion = json_decode($historial->identificacion, true);
            $FData = json_decode($historial->heredofamiliar, true);
            $Hfamiliar = json_decode($FData['data'], true);
            // return; FIXME: no se por que se coloco este return, lo comento para que pueda pasar y continuar probando.
            $noPatologico = json_decode($historial->noPatologico, true);
            $patologico = json_decode($historial->patologico, true);
            $genicoobstetrico = json_decode($historial->genicoobstetrico, true);
            $historialLaboral = json_decode($historial->historialLaboral, true);
            $historialfisico = json_decode($historial->examenFisico, true);
            $resultado = json_decode($historial->resultado, true);
            $admision_con = json_decode($historial->admision_contratista, true);
            $somnolencia = json_decode($historial->somnolencia, true);
            $cadenacustodia = json_decode($historial->cadena_custodia, true);
            $info = array(
                'identificacion' => $identificacion,
                'Heredofamiliar' => $Hfamiliar,
                'noPatologico' => $noPatologico,
                'patologico' => $patologico,
                'genicoobstetrico' => $genicoobstetrico,
                'historialLaboral' => $historialLaboral,
                'historialfisico' => $historialfisico,
                'resultado' => $resultado,
                'admision_con' => $admision_con,
                'cadenacustodia' => $cadenacustodia,
                'somnolencia' => $somnolencia

            );
        } else {
            $info = null;
        }

        if (!isset($estudios)) {
            $estudios = [];
        }
        $nombre_empresa = null;
        foreach ($empresa as $em) {
            $nombre_empresa = $em->nombre;
        }
        if ($nombre_empresa == 'Cemex' || $nombre_empresa == 'Comerdis' || $nombre_empresa == 'Empresa Demostracion') {
            return view('Laboratorio/Medicina/Estudio')->with([
                'paciente' => $empleado,
                'estudios' => $estudios,
                'historial' => $info,
            ])->with('empresa', $empresa);
        } else if ($nombre_empresa == 'Diagnose'  || $nombre_empresa == 'Fletera Nacional de Gases') {
            return view('Laboratorio/Medicina/RF0')->with([
                'paciente' => $empleado,
                'estudios' => $estudios,
                'historial' => $info,
            ])->with('empresa', $empresa);
        } else if ($nombre_empresa == 'Seguros Monterrey') {
            return view('Laboratorio/Medicina/EstudiosM')->with([
                'paciente' => $empleado,
                'estudios' => $estudios,
                'historial' => $info,
            ])->with('empresa', $empresa);
        } else if ($nombre_empresa == 'Pabsa') {
            return view('Laboratorio/Medicina/EstudiosP')->with([
                'paciente' => $empleado,
                'estudios' => $estudios,
                'historial' => $info,
            ])->with('empresa', $empresa);
        } else {
            return view('Laboratorio/Medicina/Estudio')->with([
                'paciente' => $empleado,
                'estudios' => $estudios,
                'historial' => $info,
            ])->with('empresa', $empresa);
        }
    }
    public function RecargarHistorial($curp_encrypt)
    {

        try {
            $curp = decrypt($curp_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }
        $info = null;
        try {
            $empleado = $this->getEmpleado($curp);
            $historial = $this->getHistorialesClinico($empleado->CURP);
            $estudios = $this->getEstudios($empleado->id);

            $identificacion = json_decode($historial->identificacion, true);
            $FData = json_decode($historial->heredofamiliar, true);
            $Hfamiliar = json_decode($FData['data'], true);
            $noPatologico = json_decode($historial->noPatologico, true);
            $patologico = json_decode($historial->patologico, true);
            $genicoobstetrico = json_decode($historial->genicoobstetrico, true);
            $historialLaboral = json_decode($historial->historialLaboral, true);
            $historialfisico = json_decode($historial->examenFisico, true);
            $resultado = json_decode($historial->resultado, true);
            $admision_con = json_decode($historial->admision_contratista, true);
            $cadenacustodia = json_decode($historial->cadena_custodia, true);
            $info = array(
                'identificacion' => $identificacion,
                'Heredofamiliar' => $Hfamiliar,
                'noPatologico' => $noPatologico,
                'patologico' => $patologico,
                'genicoobstetrico' => $genicoobstetrico,
                'historialLaboral' => $historialLaboral,
                'historialfisico' => $historialfisico,
                'resultado' => $resultado,
                'admision_con' => $admision_con,
                'cadenacustodia' => $cadenacustodia
            );
        } catch (\Exception $e) {
        }
        //  dd($info['historialLaboral']['genitourinario']);
        return view('Laboratorio/Medicina/Estudio')->with([
            'paciente' => $empleado,
            'estudios' => $estudios,
            'historial' => $info,
        ]);
    }
    public function getEmpleado($curp)
    {
        return Empleado::where('CURP', $curp)->first();
    }
    public function getHistorialesClinico($curp)
    {
        //   $expediente = Expediente::where('curp',$curp)->first();
        //   if ($expediente) {
        return HistorialClinico::where('curp', $curp)
            ->orderBy('created_at', 'desc')
            ->first();
        //   }else {
        //     return null;
        //   }

    }
    // public function getEstudios($id) {
    //     return  RegistroEntrada::select('e.id', 'e.nombre', 'registrosentradas.updated_at as fecha', 'c.nombre as categoria')
    //     ->where([
    //             'empleado_id' => $id,
    //             'status' => 2
    //         ])
    //     ->join('estudios as e', 'e.id','registrosentradas.estudios_id')
    //     ->join('categorias as c', 'c.id', 'e.categoria_id')
    //     ->get();
    // }
    public function getEstudios($id)
    {
        return  EstudioProgramado::select()
            ->where('empleado_id', $id)
            ->get();

        // ->whereIn('status', [1, 2])

        // $toma =  EstudioProgramado::select()
        // ->where('empleado_id', $id)
        // ->whereIn('status', [1, 2])
        // ->get();
        // $estudios = \json_decode($toma->estudios);
        // $estudios = Estudios::whereIn('id',$estudios)->get();
        // return $toma = array(
        // 'toma' => $toma,
        // 'estudios' => $estudios
        // );

        // return response()->json($toma, 200);

    }


    public function getIdProgramados($empleado_id)
    {
        return EstudioProgramado::select()
            ->where([
                ['historialClinico', null],
                ['empleado_id', $empleado_id]
            ])
            ->whereIn('status', [1, 2])
            ->first();
    }

    public function getIdResultado($empleado_id, $estudio_id)
    {
        $programado = $this->getIdProgramados($empleado_id);
        return Resultados::select('id')
            ->where([
                ['estudiosProgramados_id', $programado->id],
                ['estudio_id', $estudio_id]
            ])
            ->first();
    }
    public function getArchivos($empleado_id, $estudio_id)
    {
        $resultadoId = $this->getIdResultado($empleado_id, $estudio_id);
        $document = Archivo::where('resultado_id', $resultadoId->id)->get();
        return json_encode($document);
    }
    public function AdmisionContratista($historial, $paciente, $estudios)
    {
        return view('Laboratorio/Medicina/formatos/admisioncontratista')->with([
            'historial' => $historial,
            'paciente' => $paciente,
            'estudios' => $estudios
        ]);
    }
    public function historialForm(Request $request)
    {

        // $this->validate($request, [
        //     'fecha_estudio' => 'required|date',
        //     'estadoCivil' => 'required',
        //     'nacimiento' => 'required|date',
        //     'estado_salud' => 'required',
        //     'resultado_aptitud' => 'required',
        //     'pacienteId' => 'required',
        //     'generoPaciente' => 'required'
        // ]);
        $curp = $request->curp;
        try {
            $idPaciente = decrypt($request->get('pacienteId'));
        } catch (DecryptException $e) {
            return abort(404);
        }
        $cadenacustodia = null;
        $admision_contratista = null;
        $formulario = $request->formulario;
        $formulario_m = $request->formulario_m;
        if ($formulario_m == "examen_m") {
            $cadenacustodia = $this->cadenacustodia($request);
        } else if ($formulario == 'admision_contratista') {
            $admision_contratista = $this->admision_contratista($request);
        }

        $somnolencia = $this->somnolencia($request);
        $idheredofamiliares = $this->heredofamiliares($request);
        $no_patologicos = $this->noPatologicos($request);
        $patologico = $this->patologicos($request);
        $historial = $this->historia_laboral($request);
        $examen = $this->examen_fisico($request);
        $resultHistorial = $this->resultadosH($request);


        if ($request->get('generoPaciente') == 'FEMENINO' || $request->get('generoPaciente') == 'Femenino') {
            $genicoobstetrico = $this->genicoobstetricos($request);
            $historial = $this->identificacionController($request, $idPaciente, $idheredofamiliares, $no_patologicos, $patologico, $historial, $examen, $resultHistorial, $genicoobstetrico, $cadenacustodia, $curp, $admision_contratista, $formulario, $somnolencia);
        } else {
            $historial =  $this->identificacionController($request, $idPaciente, $idheredofamiliares, $no_patologicos, $patologico, $historial, $examen, $resultHistorial, null, $cadenacustodia, $curp, $admision_contratista, $formulario, $somnolencia);
        }

        $empleado = Empleado::find($idPaciente);

        return json_encode($empleado);
    }

    #agregar id de todos las categorias de
    #los historiales clinicos
    public function identificacionController($data, $idPaciente, $idheredofamiliares, $no_patologicos, $patologico, $historial, $examen, $resultHistorial, $genicoobstetrico = NUll, $cadenacustodia, $curp, $admision_contratista, $formulario, $somnolencia)
    {
        if (empty($cadenacustodia)) {
            $cadenacustodia = null;
        }
        if (empty($admision_contratista)) {
            $admision_contratista = null;
        }
        $identificacion =  json_encode([
            'fecha' => $data->get('fecha_estudio'),
            'numEmpleado' => $data->get('noEmpleado'),
            'departamento' => $data->get('departamento'),
            'estadoCivil' => $data->get('estadoCivil'),
            'escolaridad' => $data->get('escolaridad'),
            'domicilio' => $data->get('domicilio'),
            'lugarNacimiento' => $data->get('lugarnacimiento'),
            'ciudad' => $data->get('ciudad'),
            'municipio' => $data->get('municipio'),
            'estado' => $data->get('estado'),
            'nom_per_em' => $data->get('emergencia'),
            'parentesco' => $data->get('parentesco'),
            'telefono_1' => $data->get('telefonoParenteso'),
            'telefono_2' => $data->get('telefonoParenteso'),
            'domicilio_em' => $data->get('domicilioEm'),
            'lug_trabajo' => $data->get('lugtrabajo'),
            'nomina' => $data->get('nomina'),
            'nimss' => $data->get('nimss'),
            'supervisor' => $data->get('supervisor'),
            'profesion' => $data->get('profesion'),
            'fingreso' => $data->get('fingreso'),
            'telefono_pa' => $data->get('telefono_pa'),
            
        ]);
        // $hc = HistorialClinico::where('origen',1)
        // ->where('estado','proceso')
        // ->first();

        $hc = new HistorialClinico();

        // id de todos las secciones dentro del historial
        $hc->identificacion = $identificacion;
        $hc->estado = 'finalizado';
        $hc->heredofamiliar = $idheredofamiliares;
        $hc->noPatologico = $no_patologicos;
        $hc->patologico = $patologico;
        $hc->genicoobstetrico = $genicoobstetrico;
        $hc->historialLaboral = $historial;
        $hc->examenFisico = $examen;
        $hc->resultado = $resultHistorial;
        $hc->cadena_custodia = $cadenacustodia;
        $hc->admision_contratista = $admision_contratista;
        $hc->curp = $curp;
        $hc->somnolencia = $somnolencia;
        $hc->save();
        $idHistorial = $hc->id;
        $this->asignacion($idHistorial, $idPaciente);


        return $idHistorial;
    }
    public function somnolencia($data)
    {
        $sentado_leyendo = $data->get('sentado_leyendo');
        $tv = $data->get('tv');
        $teatro = $data->get('teatro');
        $auto_copiloto = $data->get('auto_copiloto');
        $recostado = $data->get('recostado');
        $conversando = $data->get('conversando');
        $sentado_comida = $data->get('sentado_comida');
        $sentado_comida = $data->get('sentado_comida');
        $trafico = $data->input('trafico');
        return  json_encode([
            'sentado_leyendo' => $sentado_leyendo,
            'tv' => $tv,
            'teatro' => $teatro,
            'auto_copiloto' => $auto_copiloto,
            'recostado' => $recostado,
            'conversando' => $conversando,
            'sentado_comida' => $sentado_comida,
            'trafico' => $trafico
        ]);
    }
    public function cadenacustodia($data)
    {

        $folio_web_flow = $data->input('folio_web_flow');
        $carta_compromiso = $data->input('carta_compromiso');
        $PLAN = $data->input('PLAN');
        $fumador = $data->input('fumador');
        $laboratorio = $data->input('laboratorio');
        $nombre = $data->input('nombre');
        $expedida_por = $data->input('expedida_por');
        $numero = $data->input('numero');
        $n_fecha = $data->input('n_fecha');
        $orina = $data->input('orina');
        $sangre = $data->input('sangre');
        $peso_cc = $data->input('peso_cc');
        $h_ayuno = $data->input('h_ayuno');
        $talla = $data->input('talla');
        $examen_medico = $data->input('examen_medico');
        $electrocardiagrama = $data->input('electrocardiagrama');
        $papanicolau = $data->input('papanicolau');
        $laboratorio = $data->input('laboratorio');
        $fecha = $data->input('fecha');
        $hora = $data->input('hora');
        $ayuno = $data->input('ayuno');
        $medico = $data->input('medico');
        $cedula_p = $data->input('cedula_p');
        $glucosa = $data->input('glucosa');
        $resultado = $data->input('resultado');
        $centrifugada = $data->input('centrifugada');

        return  json_encode([
            'folio_web_flow' => $folio_web_flow,
            'expedida_por' => $expedida_por,
            'carta_compromiso' => $carta_compromiso,
            'nombre' => $nombre,
            'expedida_por' => $expedida_por,
            'numero' => $numero,
            'n_fecha' => $n_fecha,
            'orina' => $orina,
            'sangre' => $sangre,
            'peso_cc' => $peso_cc,
            'h_ayuno' => $h_ayuno,
            'talla' => $talla,
            'examen_medico' => $examen_medico,
            'electrocardiagrama' => $electrocardiagrama,
            'papanicolau' => $papanicolau,
            'laboratorio' => $laboratorio,
            'fecha' => $fecha,
            'hora' => $hora,
            'ayuno' => $ayuno,
            'medico' => $medico,
            'cedula_p' => $cedula_p,
            'glucosa' => $glucosa,
            'resultado' => $resultado,
            'PLAN' => $PLAN,
            'fumador' => $fumador,
            'centrifugada' => $centrifugada,
            'laboratorio' => $laboratorio


        ]);
    }
    public function admision_contratista($data)
    {

        $pacienteId = $data->input('pacienteId');
        $generoPaciente = $data->input('generoPaciente');
        $curp = $data->input('curp');
        $vertigos = $data->input('vertigos');
        $vertigo_piso = $data->input('vertigo_piso');
        $escaleras = $data->input('escaleras');
        $altura = $data->input('altura');
        $membrana = $data->input('membrana');
        $p_talon = $data->input('p_talon');
        $linea = $data->input('linea');
        $t_altura = $data->input('t_altura');
        $e_cerrados = $data->input('e_cerrados');
        $fobias = $data->input('fobias');
        $e_estrechos = $data->input('e_estrechos');
        $e_confinados = $data->input('e_confinados');
        $c_vertebral = $data->input('c_vertebral');
        $f_vertebral = $data->input('f_vertebral');
        $d_espalda = $data->input('d_espalda');
        $d_ciatica = $data->input('d_ciatica');
        $entumen_pb = $data->input('entumen_pb');
        $limitacion_bpe = $data->input('limitacion_bpe');
        $m_inferior = $data->input('m_inferior');
        $fisiologicas = $data->input('fisiologicas');
        $punta_t = $data->input('punta_t');
        $apofisis = $data->input('apofisis');
        $sacroiliacas = $data->input('sacroiliacas');
        $m_is = $data->input('m_is');
        $Lasague = $data->input('Lasague');
        $reque25 = $data->input('25kg');
        $opresion = $data->input('opresion');
        $tosido = $data->input('tosido');
        $polvos_humos = $data->input('polvos_humos');
        $c_auditiva = $data->input('c_auditiva');
        $o_zumbidos = $data->input('o_zumbidos');
        $conversacion = $data->input('conversacion');
        $timpanica = $data->input('timpanica');
        $cerumen = $data->input('cerumen');
        $e_ruido = $data->input('e_ruido');
        $actividad_laboral = $data->input('actividad_laboral');
        $SATISFACTORIO = $data->input('SATISFACTORIO');
        $COVID_19 = $data->input(' COVID_19');
        $vulnerabilidad_covid = $data->input('vulnerabilidad_covid');
        $firma_medico = $data->input('firma_medico');
        $cedula = $data->input('cedula');
        $firma_evaluado = $data->input('firma_evaluado');
        $firma_evaluado1 = $data->input('firma_evaluado1');
        $lugar_fecha = $data->input('lugar_fecha');
        $bioSigData = $data->input('bioSigData');
        $sigImageData = $data->input('sigImageData');
        $factorescovid = array(
            'factorescovid' => $vulnerabilidad_covid
        );
        $factcovid = json_encode(array($factorescovid));


        return  json_encode([
            'pacienteId' => $pacienteId,
            'generoPaciente' => $generoPaciente,
            'curp' => $curp,
            'vertigos' => $vertigos,
            'vertigo_piso' => $vertigo_piso,
            'escaleras' => $escaleras,
            'altura' => $altura,
            'membrana' => $membrana,
            'p_talon' => $p_talon,
            'linea' => $linea,
            't_altura' => $t_altura,
            'e_cerrados' => $e_cerrados,
            'fobias' => $fobias,
            'e_estrechos' => $e_estrechos,
            'e_confinados' => $e_confinados,
            'c_vertebral' => $c_vertebral,
            'f_vertebral' => $f_vertebral,
            'd_espalda' => $d_espalda,
            'd_ciatica' => $d_ciatica,
            'entumen_pb' => $entumen_pb,
            'limitacion_bpe' => $limitacion_bpe,
            'm_inferior' => $m_inferior,
            'fisiologicas' => $fisiologicas,
            'punta_t' => $punta_t,
            'apofisis' => $apofisis,
            'sacroiliacas' => $sacroiliacas,
            'm_is' => $m_is,
            'Lasague' => $Lasague,
            'reque25' => $reque25,
            'opresion' => $opresion,
            'tosido' => $tosido,
            'polvos_humos' => $polvos_humos,
            'c_auditiva' => $c_auditiva,
            'o_zumbidos' => $o_zumbidos,
            'conversacion' => $conversacion,
            'timpanica' => $timpanica,
            'cerumen' => $cerumen,
            'e_ruido' => $e_ruido,
            'actividad_laboral' => $actividad_laboral,
            'SATISFACTORIO' => $SATISFACTORIO,
            'COVID_19' => $COVID_19,
            'vulnerabilidad_covid' => $vulnerabilidad_covid,
            'firma_medico' => $firma_medico,
            'cedula' => $cedula,
            'firma_evaluado' => $firma_evaluado,
            'firma_evaluado1' => $firma_evaluado1,
            'lugar_fecha' => $lugar_fecha,
            'factcovid' => $factcovid,
            'bioSigData' => $bioSigData,
            'sigImageData' => $sigImageData

        ]);
    }
    public function heredofamiliares($data)
    {
        $padre = array(
            'familiar' => 'Padre',
            'estado' => $data->get('padre_estado'),
            'enfermedad' => $data->get('select_padre')
        );

        $madre = array(
            'familiar' => 'Madre',
            'estado' => $data->get('madre_estado'),
            'enfermedad' => $data->get('select_madre')
        );

        $hermanos = array(
            'familiar' => 'Conyuge',
            'estado' => $data->get('conyuge_estado'),
            'enfermedad' => $data->get('select_conyuge')
        );

        $conyuge = [
            'familiar' => 'Hermanos(as)',
            'estado' => $data->get('hermano_estado'),
            'enfermedad' => $data->get('select_hermanos')
        ];

        $hijos = array(
            'familiar' => 'Hijos',
            'estado' => $data->get('hijos_estato'),
            'enfermedad' => $data->get('select_hijos')
        );

        $datos = json_encode(array($padre, $madre, $hermanos, $conyuge, $hijos));

        // $hf = new Heredofamiliar;
        // $hf->data = $datos;
        // $hf->save();

        return json_encode([
            'data' => $datos
        ]);
    }

    public function noPatologicos($data)
    {
        $tetano = $data->input('tetano');
        $rubela = $data->input('rubeola');
        $bcg = $data->input('bcg');
        $hepatitis = $data->input('hepatitis');
        $influnza = $data->input('influenza');
        $neumococica = $data->input('neumococica');
        $piso_cemento = $data->input('piso_cemento');
        $drenaje = $data->input('drenaje');
        $agua_potable = $data->input('agua_potable');
        $luz = $data->input('luz');
        $n_habitantes = $data->input('n_habitantes');
        $n_habitaciones = $data->input('n_habitaciones');
        $mascotas = $data->input('mascotas');
        $n_mascotas = $data->input('n_mascotas');
        $h_higienicos = $data->input('h_higienicos');
        $lavado_dientes = $data->input('lavado_dientes');
        $alimentacion_diaria = $data->input('alimentacion_diaria');
        $tiposangre = $data->input('tiposangre');
        $tatuajes = $data->input('tatuajes');
        $vacunas = [
            'tetano' => $tetano,
            'rubeola' => $rubela,
            'bcg' => $bcg,
            'hepatitis' => $hepatitis,
            'influenza' => $influnza,
            'neumococica' => $neumococica,
            'otros' => $data->input('otrasvacunas')
        ];

        if ($data->get('cigarro') == 'si') {
            $cigarro = [
                'Cigarro' => 'Si',
                'edad' => $data->input('cigarro_edad'),
                'frecuencia' => $data->input('cigarropromedio'),
                'noConsumir' => $data->input('dejarcigarro')
            ];
        } elseif ($data->get('cigarro') == 'no') {
            $cigarro = [
                'Cigarro' => 'No',
                'edad' => NULL,
                'frecuencia' => NULL,
                'noConsumir' => NULL
            ];
        }
        ////////////////////////////////////////////

        if ($data->get('alcohol') == 'si') {
            $alcohol = [
                'Alcohol' => 'Si',
                'edad' => $data->input('alcohol_edad'),
                'frecuencia' => $data->input('alcohol_frecuencia'),
            ];
        } elseif ($data->get('alcohol') == 'no') {
            $alcohol = [
                'Alcohol' => 'No',
                'edad' => NULL,
                'frecuencia' => NULL,
            ];
        }
        ////////////////////////////////////////////

        if ($data->get('drogas') == 'si') {
            $drogas = [
                'Drogas' => 'Si',
                'edad' => $data->input('drogas_edad'),
                'frecuencia' => $data->input('drogas_frecuencia'),
            ];
        } elseif ($data->get('drogas') == 'no') {
            $drogas = [
                'Drogas' => 'No',
                'edad' => NULL,
                'frecuencia' => NULL,
            ];
        }
        /////////////////////////////////////////

        if ($data->get('dieta') == 'si') {
            $dieta = [
                'Dieta' => 'Si',
                'descripcion' => $data->input('dieta_descripcion'),
            ];
        } elseif ($data->get('dieta') == 'no') {
            $dieta = [
                'Dieta' => 'No',
                'descripcion' => NULL,
            ];
        }
        /////////////////////////////////////////

        if ($data->get('deporte') == 'si') {
            $deporte = [
                'Deporte' => 'Si',
                'frecuencia' => $data->input('deporte_frecuencia'),
            ];
        } elseif ($data->get('deporte') == 'no') {
            $deporte = [
                'Deporte' => 'No',
                'frecuencia' => NULL,
            ];
        }

        // /////////////////////////////////////////
        return  json_encode([
            'vacunas' => $vacunas,
            'g_sanguineo' => $data->input('tiposangre'),
            'transfusion' => $data->get('transfusion'),
            'medica_ingeridos' => $data->get('mediIngeridos'),
            'cigarro' => $cigarro,
            'alcohol' => $alcohol,
            'drogas' => $drogas,
            'drogas_usadas' => $data->get('drogas_usadas'),
            'dieta' => $dieta,
            'deporte' => $deporte,
            'pasatiempo' => $data->get('pasatiempo'),
            'piso_cemento' => $piso_cemento,
            'drenaje' => $drenaje,
            'agua_potable' => $agua_potable,
            'luz' => $luz,
            'n_habitantes' => $n_habitantes,
            'n_habitaciones' => $n_habitaciones,
            'n_mascotas' => $n_mascotas,
            'mascotas' => $mascotas,
            'h_higienicos' => $h_higienicos,
            'lavado_dientes' => $lavado_dientes,
            'alimentacion_diaria' => $alimentacion_diaria,
            'tatuajes' => $tatuajes,
            'tiposangre' => $tiposangre
        ]);
    }

    public function patologicos($data)
    {
        $dental = [
            'Cirugía dental' => $data->get('c_dental'),
            'Descripcion' => $data->get('dental')
        ];

        $estomago = [
            'Cirugía de estomago/ úlceras gastricas' => $data->get('c_estomago'),
            'Descripcion' => $data->get('estomago')
        ];

        $cuello = [
            'Lesion en cuello/ espalda / rodillas, manos, codos' => $data->get('lesiones'),
            'Descripcion' => $data->get('cuello')
        ];

        $recto = [
            'Cirugía de colon/ recto' => $data->get('c_colon'),
            'Descripcion' => $data->get('colon')
        ];

        $intestinos = [
            'Cirugía de intestinos' => $data->get('c_intestino'),
            'Descripcion' => $data->get('intestino')
        ];

        $oofo = [
            'Ooforectomia/ Histerectomia' => $data->get('ooforectomia'),
            'Descripcion' => $data->get('Histerectomia')
        ];

        $vasectomia = [
            'Salpingoclasia/ Vasectomia' => $data->get('sal_vas'),
            'Descripcion' => $data->get('Vasectomia')
        ];

        $hernias = [
            'Cirugia de hernias' => $data->get('c_hernia'),
            'Descripcion' => $data->get('hernias')
        ];

        $tiroides = [
            'Cirugia de tiroides' => $data->get('c_tiroides'),
            'Descripcion' => $data->get('tiroides')
        ];

        $colecis = [
            'Colecistectomia' => $data->get('colecistectomia'),
            'Descripcion' => $data->get('Colecistectomia')
        ];

        $apendi = [
            'Apéndicectomia' => $data->get('apendicectomia'),
            'Descripcion' => $data->get('apendicectomias')
        ];

        $fracturas = [
            'Fracturas o esguinces' => $data->get('fracturas'),
            'Descripcion' => $data->get('esguinces')
        ];

        $cirugiasLesion = [
            $dental,
            $estomago,
            $cuello,
            $recto,
            $intestinos,
            $oofo,
            $vasectomia,
            $vasectomia,
            $hernias,
            $tiroides,
            $colecis,
            $apendi,
            $fracturas
        ];

        return json_encode([
            'enfermPadecidas' => $data->get('enfermedad_padecida'),
            'transtornos' => $data->get('transtornos_ep'),
            'alergiaMedica'  => $data->get('alergiasMedicas'),
            'alergiaPiel' => $data->get('alergia_piel'),
            'alergiaOtro' => $data->get('alergia_otro'),
            'tumorCancer' => $data->get('tumo_cancer'),
            'probVista' => $data->get('problema_vista'),
            'enfOido' => $data->get('enfer_oido'),
            'probCulumVert' => $data->get('columna_vertebral'),
            'huesoArticulacion' => $data->get('hueso_articulacion'),
            'otroProbMedico' => $data->get('otro_problema'),
            'cirugiasLesion' => $cirugiasLesion,
            'otra_cirugia' => $data->get('otra_cirugia'),
            'quirurgicos' => $data->get('quirurgicos'),
            'destipocirugia' => $data->get('destipocirugia'),
            'fechacirugia' => $data->get('fechacirugia'),
            'complicacionescirugia' => $data->get('complicacionescirugia'),
            'traumaticos' => $data->get('traumaticos'),
            'tipotraumaticos' => $data->get('tipotraumaticos'),
            'fechatraumaticos' => $data->get('fechatraumaticos'),
            'comptraumaticos' => $data->get('comptraumaticos'),
            'alergicos' => $data->get('alergicos'),
            'cualalergia' => $data->get('cualalergia'),
            'internamientos' => $data->get('internamientos'),
            'porquealergias' => $data->get('porquealergias'),
            'medicamento' => $data->get('medicamento')

        ]);
    }

    public function genicoobstetricos($data)
    {
        return json_encode([
            'inicioRegla' => $data->get('inicio_regla'),
            'fRegla' => $data->get('frecuencia_regla'),
            'duraRegla' => $data->get('duracion_regla'),
            'relacionSexInicio' => $data->get('inicio_re_sexual'),
            'metodoUsado' => $data->get('metodo_antic'),
            'ultimaMestruacion' => $data->get('ult_mestruacion'),
            'numEmbarazos' => $data->get('embarazos'),
            'numPartos' => $data->get('partos'),
            'numCesareas' => $data->get('cesareas'),
            'numAbortos' => $data->get('abortos'),
            'fecha_examenCancer' => $data->get('examen_matriz'),
            'fecha_examenMamas' => $data->get('examen_mamas'),
            'edadMenopausia' => $data->get('menopausia'),
            'papanicolaou' => $data->get('papanicolaou'),
            'MPF' => $data->get('MPF'),
            'embarazada' => $data->get('embarazada'),
            'do_menstruacion' => $data->get('do_menstruacion'),
            'enf_mama' => $data->get('enf_mama'),
            'enf_utero' => $data->get('enf_utero'),
            'MENARQUIA' => $data->get('MENARQUIA'),
            'ritmo_duracion' => $data->get('ritmo_duracion'),
            'DISMENORREA' => $data->get('DISMENORREA'),
            'G' => $data->get('G'),
            'P' => $data->get('P'),
            'A' => $data->get('A'),
            'C' => $data->get('C'),
            'FUP' => $data->get('FUP'),
            'IVSA' => $data->get('IVSA'),
            'CS' => $data->get('CS'),
            'FUM' => $data->get('FUM'),
            'MPF' => $data->get('MPF'),
            'DOC' => $data->get('DOC'),
            'GESTAC' => $data->get('GESTAC'),
            'FPP' => $data->get('FPP')
        ]);
    }

    public function historia_laboral($data)
    {


        $emple_uno = [
            'nombre' => $data->get('nombreEmpresa_actual'),
            'puesto' => $data->get('actual_puesto'),
            'actividad' => $data->get('actual_empleo_actividad'),
            'duracion' => $data->get('actual_empleo_duracion'),
            'seguridad' => $data->get('actual_equipoS'),
            'danos' => $data->get('actual_empleo_danos'),
            'tipo' => '1'
        ];

        $emple_dos = [
            'nombre' => $data->get('nomEmpresa_anterior'),
            'puesto' => $data->get('anterior_puesto'),
            'actividad' => $data->get('anterior_empleo_actividad'),
            'duracion' => $data->get('anterior_empleo_duracion'),
            'seguridad' => $data->get('anteriro_equipoS'),
            'danos' => $data->get('anterior_empleo_danos'),
            'tipo' => '2'
        ];

        $emple_tres = [
            'nombre' => $data->get('nomEmpresa_anterior_two'),
            'puesto' => $data->get('two_anterior_puesto'),
            'actividad' => $data->get('two_anterior_empleo_actividad'),
            'duracion' => $data->get('two_anterior_empleo_duracion'),
            'seguridad' => $data->get('two_anteriro_equipoS'),
            'danos' => $data->get('two_anterior_empleo_danos'),
            'tipo' => '3'
        ];

        $empleos = [
            $emple_uno,
            $emple_dos,
            $emple_tres
        ];

        return json_encode([
            'musculoesqueletico' => $data->get('musculoesqueletico'),
            'neurologico' =>  $data->get('neurologico_psicologico'),
            'gastroIntestinal' =>  $data->get('gastro_intestinal'),
            'genitourinario' =>  $data->get('genitourinario'),
            'cardiovasucular' =>  $data->get('cardiovascular'),
            'pulmunar' =>  $data->get('pulmunar'),
            'endocrino' =>  $data->get('endocrino'),
            'inmunologico' =>  $data->get('inmunologico'),
            'dermatologico' =>  $data->get('dermatologico'),
            'hematologico' => $data->get('hematologico'),
            'circulatorio' => $data->get('circulatorio'),
            'reproductor' => $data->get('reproductor'),
            'sintomasgenerales' => $data->get('sintomasgenerales'),
            'sentidos' => $data->get('sentidos'),
            'otrosproble' => $data->get('otrosproble'),
            'alergiaMedicamentos' => $data->get('reaccion_alergicas_medicamentos'),
            'problemaSalud' => $data->get('problema_salud'),
            'problemaMujer' => $data->get('padecimientos_mujer'),
            'abortos' => $data->get('abortos_ano'),
            'fechaParto' => $data->get('embarazada_parto'),
            'problemaHombre' => $data->get('padecimientos_hombre'),
            'padecimientoHombre' => $data->get('otros_padecimientos_hombre'),
            'trabajoRealizados' => $data->get('trabajos_realizados'),
            'otrosTrabajos' => $data->get('otrosTrabajos'),
            'trabajoMateriales' => $data->get('materiales_expuesto'),
            'otrasExposiciones' => $data->get('otras_exposiciones'),
            'empleos' => $empleos,
            'equipoSegActual' => $data->get('equipo_seguridad'),
            'examenVista' => $data->get('examen_vista'),
            'lentesContacto' => $data->get('uso_lentes_graduados'),
            'otras_exposiciones'  => $data->get('otrasexpPuesto'),
            'famMaterialPeligroso' => $data->get('ma_peligroso_fam'),
            'lugaresVividos' => $data->get('lugar_vivido'),
            'expMaterialPeligroso' => $data->get('exp_mat_peligrosos'),
            'edad_inicio_laboral' => $data->input('edad_inicio_laboral'),
            'trabajos_realizados' => $data->input('trabajos_realizados'),
            'tiempo_puesto' => $data->input('tiempo_puesto'),
            'horas_trabajo' => $data->input('horas_trabajo'),
            'grados_estudio' => $data->input('grados_estudio'),
            'accidentes_t' => $data->input('accidentes_t'),
            'enfermedades_t' => $data->input('enfermedades_t'),
            'trabajo_pie' => $data->input('trabajo_pie'),
            'trabajo_sentado' => $data->input('trabajo_sentado'),
            'trabajo_pie_fisico' => $data->input('trabajo_pie_fisico'),
            'trabajo_pie_caminando' => $data->input('trabajo_pie_caminando'),
            'herramientas_manuales' => $data->input('herramientas_manuales'),
            'maquinas' => $data->input('maquinas'),
            'ACUFENOS' => $data->input('ACUFENOS'),
            'PRURITO_OTICO' => $data->input('PRURITO_OTICO'),
            'TOS_PRODUCTIVA' => $data->input('TOS_PRODUCTIVA'),
            'OTALGIA' => $data->input('OTALGIA'),
            'DISNEA' => $data->input('DISNEA'),
            'PRURITO_OTICO' => $data->input('PRURITO_OTICO'),
            'PLENITUD_OTICA' => $data->input('PLENITUD_OTICA'),
            'tension_e' => $data->input('tension_e'),
            'proteccion_in' => $data->input('proteccion_in'),
            'adecuados_pro' => $data->input('adecuados_pro'),
            'extralaboral' => $data->input('extralaboral'),
            'epp' => $data->input('epp')

        ]);
    }

    public function examen_fisico($data)
    {

        return json_encode([
            'apariencia' => $data->get('apariencia'),
            'temperatura' => $data->get('temperatura'),
            'fc' => $data->get('fc'),
            'fr' => $data->get('fr'),
            'altura' => $data->get('altura'),
            'peso' => $data->get('peso'),
            'imc' => $data->get('imc'),
            'psBrazoDerecho' => $data->get('presion_derecho'),
            'psBrazoIzquierdo' => $data->get('presion_izquierdo'),
            'cabezaCueroOjos' => $data->get('observaciones_cabeza'),
            'oidoNarizBoca' => $data->get('observaciones_oidos'),
            'dientesFaringe' => $data->get('observaciones_dientes'),
            'cuello' => $data->get('observaciones_cuello'),
            'tiroides' => $data->get('observaciones_tiroides'),
            'nodoLinfatico' => $data->get('observaciones_nodo'),
            'toraxPulmones' => $data->get('observaciones_torax'),
            'pecho' => $data->get('observaciones_pecho'),
            'corazon' => $data->get('observaciones_corazon'),
            'abdomen' => $data->get('observaciones_abdomen'),
            'rectalDigital' => $data->get('observaciones_rectal'),
            'genitales' => $data->get('observaciones_genitales'),
            'columnaVertebral' => $data->get('observaciones_columna'),
            'piel' => $data->get('observaciones_piel'),
            'pulsoArterial' => $data->get('observaciones_pulso'),
            'extremidades' => $data->get('observaciones_extremidades'),
            'musculoesqueleto' => $data->get('observaciones_musculoesque'),
            'reflejosNeurologicos' => $data->get('observaciones_relejos'),
            'estadoMental' => $data->get('observaciones_mental'),
            'otrosComentarios' => $data->get('observaciones_comentarios'),
            'audiometria_hf' => $data->get('audiometria_hf'),
            'espirometria_hf' => $data->get('espirometria_hf'),
            'ra_torax'=>$data->get('ra_torax'),
            'ra_columna'=>$data->get('ra_columna'),
            'metabolico' => $data->get('metabolico'),
            'ojoIzquierdo' => $data->get('agudeza_izquierdo'),
            'ojoDerecho' => $data->get('agudeza_derecho'),
            'uso_epp' => $data->get('uso_epp'),
            'audiometria' => $data->get('audiometria'),
            'respiradores' => $data->get('respiradores_r'),
            'reentrenar' => $data->get('reentrenar'),
            'otrasRecomend' => $data->get('otra_recomendacion'),
            'recomEstiloVida' => $data->get('recomendaciones_vida'),
            'diagnosticoAudio' => $data->get('diagnostico_audiologico'),
            //EXPLORACIÓN FISICA//
            'puesto_solicitado' => $data->get('puesto_solicitado'),
            'Temperatura' => $data->get('Temperatura'),
            'Talla' => $data->get('Talla'),
            'Peso' => $data->get('Peso'),
            'IMC' => $data->get('IMC'),
            'p_abdominal' => $data->get('p_abdominal'),
            'p_toracico' => $data->get('p_toracico'),
            'TA' => $data->get('TA'),
            'FC' => $data->get('FC'),
            'FR' => $data->get('FR'),
            'SatO2' => $data->get('SatO2'),
            'SL' => $data->get('SL'),
            'CL' => $data->get('SL'),
            'SLI' => $data->get('SLI'),
            'CLI' => $data->get('CLI'),
            'TAHC' => $data->get('TAHC'),
            'FCHC' => $data->get('FCHC'),
            'FRHC' => $data->get('FRHC'),
            'THC' => $data->get('THC'),
            'PESO' => $data->get('PESO'),
            'TALLAHC' => $data->get('TALLAHC'),
            'IMChc' => $data->get('IMChc'),
            'ideal' => $data->get('ideal'),
            'CORPORAL' => $data->get('CORPORAL'),
            'ODSL' => $data->get('ODSL'),
            'OISL' => $data->get('OISL'),
            'ODCL' => $data->get('ODCL'),
            'OICL' => $data->get('OICL'),
            'DESTROSTIX' => $data->get('DESTROSTIX'),
            'HABITUS' => $data->get('HABITUS'),
            'CUELLO' => $data->get('CUELLO'),
            'TORAX' => $data->get('TORAX'),
            'ABDOMEN' => $data->get('ABDOMEN'),
            'PELVICOS' => $data->get('PELVICOS'),
            'EXTREMIDADES' => $data->get('EXTREMIDADES'),
            'cintura' => $data->get('cintura'),
            'vision_colores' => $data->get('vision_colores'),
            'Nariz' => $data->get('Nariz'),
            'agueza_aud_d' => $data->get('agueza_aud_d'),
            'con_auditivos_d' => $data->get('con_auditivos_d'),
            'mem_tim_d' => $data->get('mem_tim_d'),
            'agueza_aud_i' => $data->get('agueza_aud_i'),
            'con_auditivos_i' => $data->get('con_auditivos_i'),
            'mem_tim_i' => $data->get('mem_tim_i'),
            'cav_oral' => $data->get('cav_oral'),
            'orofaringe' => $data->get('orofaringe'),
            'Amigdalas' => $data->get('Amigdalas'),
            'Lengua_f002' => $data->get('Lengua_f002'),
            'Cuello' => $data->get('Cuello'),
            'cu_observaciones' => $data->get('cu_observaciones'),
            'Torax' => $data->get('Torax'),
            'to_observaciones' => $data->get('to_observaciones'),
            'ca_pulmonares' => $data->get('ca_pulmonares'),
            'ca_pulmonares_observaciones' => $data->get('ca_pulmonares_observaciones'),
            'mo_respiratorios' => $data->get('mo_respiratorios'),
            'mo_respiratorios_observaciones' => $data->get('mo_respiratorios_observaciones'),
            'ruicardi' => $data->get('ruicardi'),
            'ruicardi_observaciones' => $data->get('ruicardi_observaciones'),
            'Inspeccion' => $data->get('Inspeccion'),
            'Inspeccion_observaciones' => $data->get('Inspeccion_observaciones'),
            'R_peristalticos' => $data->get('R_peristalticos'),
            'R_peristalticos_observaciones' => $data->get('R_peristalticos_observaciones'),
            'd_palpitacion' => $data->get('d_palpitacion'),
            'palpitacion' => $data->get('palpitacion'),
            'diastasis' => $data->get('diastasis'),
            'Higado' => $data->get('Higado'),
            'Vibices' => $data->get('Vibices'),
            'Hernias' => $data->get('Hernias'),
            'Bazo' => $data->get('Bazo'),
            'B3n' => $data->get('B3n'),
            'con_auditivos_d' => $data->get('con_auditivos_d'),
            'Cicatriz' => $data->get('Cicatriz'),
            'Ganglios' => $data->get('Ganglios'),
            'Movimientos' => $data->get('Movimientos'),
            'Movimientos_observaciones' => $data->get('Movimientos_observaciones'),
            'Marcha' => $data->get('Marcha'),
            'Marcha_observaciones' => $data->get('Marcha_observaciones'),
            'acortamiento' => $data->get('acortamiento'),
            'Ulceras' => $data->get('Ulceras'),
            'Reflejo_patelar' => $data->get('Reflejo_patelar'),
            'Varices' => $data->get('Varices'),
            'Micosis' => $data->get('Micosis'),
            'Llenado_capilar' => $data->get('Llenado_capilar'),
            'Movimientos_to' => $data->get('Movimientos_to'),
            'Movimientos_to_observaciones' => $data->get('Movimientos_to_observaciones'),
            'lle_capilar_ca_to' => $data->get('lle_capilar_ca_to'),
            'lle_capilar_ca_to_observaciones' => $data->get('lle_capilar_ca_to_observaciones'),
            'Flogosis_t' => $data->get('Flogosis_t'),
            'Flogosis' => $data->get('Flogosis'),
            'Flogosis_observaciones' => $data->get('Flogosis_observaciones'),
            'Tumoracion' => $data->get('Tumoracion')

        ]);
    }

    public function resultadosH($data)
    {
        return json_encode([
            'trans_con' => $data->get('trans_con'),
            'incoherencia' => $data->get('incoherencia'),
            'trans_atencion' => $data->get('trans_atencion'),
            'estadoSalud' => $data->get('estado_salud'),
            'aptitud' => $data->get('resultado_aptitud'),
            'comentarios' => $data->get('comentarios_finales'),
            'bioSigData' => $data->get('bioSigData'),
            'sigImageData' => $data->get('sigImageData'),
            'motivo_consulta' => $data->get('motivo_consulta'),
            'peea' => $data->get('peea'),
            'estudios' => $data->get('estudios'),
            'plan_t' => $data->get('plan_t'),
            'proxima_cita' => $data->get('proxima_cita'),
            'diagnostico' => $data->get('diagnostico')
        ]);
    }

    public function asignacion($idHistorial, $idPaciente)
    {
        try {
            $estudioProgramado = EstudioProgramado::where([['empleado_id', $idPaciente], ['historialClinico', NULL]])->first();
            $estudioProgramado->historialClinico = $idHistorial;
            $estudioProgramado->save();
            return 'Exito';
        } catch (\Exception $e) {
            return 'Error';
        }
    }


    public function regresaformato(Request $request)
    {
        $formato = $request->formatos;
        return compact('formato');
    }
}
