<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultados extends Model {
    protected $table = 'resultados';

    public function archivo()
    {
        return $this->hasOne(Archivo::class, 'resultado_id','id');
    }

    public function estudio()
    {
        return $this->hasOne(Estudios::class, 'id', 'estudio_id');
    }

    public function expediente()
    {
        return $this->hasOne(Expediente::class, 'id', 'expediente_id');
    }

    public function audiometria()
    {
        return $this->hasOne(Audiometria::class, 'resultado_id', 'id');
    }

    public function espirometria()
    {
        return $this->hasOne(Espirometria::class, 'resultado_id', 'id');
    }

    public function medico()
    {
        return $this->hasOne(datosUserHtds::class, 'ID_empleado', 'medico_id');
    }
}
