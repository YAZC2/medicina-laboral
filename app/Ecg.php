<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ecg extends Model {
    protected $table = 'ecgs';

    public function ecg_series()
    {
        return $this->hasMany(EcgSerie::class, 'ecg_id', 'id');
    }
    public function ecg_medidas()
    {
        return $this->hasMany(EcgMedidas::class, 'ecg_id', 'id');
    }
    public function ecgInter()
    {
      return $this->hasMany(EcgInterpretacion::class,'ecg_id','id');
    }

}
