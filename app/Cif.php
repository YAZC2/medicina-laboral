<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cif extends Model {
    protected $table = 'cat_cif';
}
