<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SassOrden extends Model {
    protected $table = "orden_sass";

    public function programacionEstudio(){
        return $this->hasOne(EstudioProgramado::class, 'id', 'programacion');
    }

}
