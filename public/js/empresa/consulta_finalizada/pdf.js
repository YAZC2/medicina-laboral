$('#pdf_receta').on('click', function(arguments){
    id = $('#historial_id').val();
    url = '../Receta/'+id
    window.open( url, 'Print', 'left=200, top=150,bottom=100, width=950, height=500, toolbar=0, resizable=0,status=0');
});

$('#pdf_procedimientos').on('click', function(arguments){
    id = $('#historial_id').val();
    url = '../procedimientos/'+id
    window.open( url, 'Print', 'left=200, top=150,bottom=100, width=950, height=500, toolbar=0, resizable=0,status=0');
});

$('#pdf_tratamiento').on('click', function(arguments){
    id = $('#historial_id').val();
    url = '../tratamiento/'+id
    window.open( url, 'Print', 'left=200, top=150,bottom=100, width=950, height=500, toolbar=0, resizable=0,status=0');
});


$('#email_receta').on('click', function(arguments){
    swal({
      title: "Atención",
      text: "¿Seguro de enviar la receta medica por email?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
    })
    .then((isConfirm) => {
      if (isConfirm) {
        id = $('#historial_id').val();
        $.ajax({
        type:'get',
        dataType:'json',
        url:'../RecetaEmail/'+id,

        }).done(res=>{

            if(res == "Correcto")
            {
                swal("Enviado", "Se ha enviado el correo con la receta médica", "success");
            }else{
                swal("Error", "Algo ocurrio intentalo más tarde", "error");
            }

        }).fail(err=>{
            swal("Error", "Algo ocurrio intentalo más tarde", "error");
        })
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
    });
});

$('#email_procedimientos').on('click', function(arguments){
    swal({
      title: "Atención",
      text: "¿Seguro de enviar la el procedimiento médico por email?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
    })
    .then((isConfirm) => {
      if (isConfirm) {
        id = $('#historial_id').val();
        $.ajax({
        type:'get',
        dataType:'json',
        url:'../procedimientosEmail/'+id,

        }).done(res=>{

            if(res == "Correcto")
            {
                swal("Enviado", "Se ha enviado el correo con el procedimiento médico", "success");
            }else{
                swal("Error", "Algo ocurrio intentalo más tarde", "error");
            }

        }).fail(err=>{
            swal("Error", "Algo ocurrio intentalo más tarde", "error");
        })
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
    });
});

$('#email_tratamiento').on('click', function(arguments){
    swal({
      title: "Atención",
      text: "¿Seguro de enviar la el tratamiento médico por email?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
    })
    .then((isConfirm) => {
      if (isConfirm) {
        id = $('#historial_id').val();
        $.ajax({
        type:'get',
        dataType:'json',
        url:'../tratamientoEmail/'+id,

        }).done(res=>{

            if(res == "Correcto")
            {
                swal("Enviado", "Se ha enviado el correo con el tratamiento médico", "success");
            }else{
                swal("Error", "Algo ocurrio intentalo más tarde", "error");
            }

        }).fail(err=>{
            swal("Error", "Algo ocurrio intentalo más tarde", "error");
        })
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
    });
});

$('.alertCedula').on('click', function(arguments){
    swal('Configura tu cuenta','Para poder generar recetas médicas, es necesario completar algunos campos en la configuración de su cuenta','info')
});


$('.notasEmail').on('click', function(){
    $('#notas').modal('show');
})

$('.enviarEmailNota').on('click',function(){
    swal({
      title: "Atención",
      text: "¿Seguro que deseas enviar las notas por email?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
    })
    .then((isConfirm) => {
      if (isConfirm) {
        NotasEmail();
      } else {
          $('#notas').modal('hide');
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
    });
});

  function NotasEmail(){
    id = $('#historial_id').val();
    email = $('#notaEmailPatient').val();

    obj = new Object();
    obj.id = id;
    obj.email = email;

    $.ajax({
      type:'post',
      dataType:'json',
      url:'../NotasEmail',
      data:obj
    }).done(res=>{
      $('#notas').modal('hide');
      if(res == "Correcto")
      {
          swal("Enviado", "Se ha enviado el correo con la receta médica", "success");
      }else{
          swal("Error", "Algo ocurrio intentalo más tarde", "error");
      }

    }).fail(err=>{
        swal("Error", "Algo ocurrio intentalo más tarde", "error");
    })
  }
