$(document).ready(function() {
  sass = $('#idSass').val();
  examen = $('#idExamen').val();
  sass = sass.split('/');
  toma = sass[0];

  consecutivo = sass[1];
  data = new Object();
  data.toma = toma;
  data.consecutivo = consecutivo
  data.metodo = 'estudios';
  data.examen = examen;
  
  $.ajax({
    type:'post',
    data:data,
    dataType:'json',
    url:'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend:()=>{
        $('.loadResultados').show();
    }
  }).done(res=>{
    if (res.length>0) {
       result(res);
    }else {
      swal('Estudios Pendientes','Los resultados del examen aún no son liberados','info')
    }
    $('.loadResultados').hide();
  }).fail(err=>{

  })
});


function result(res) {
  dataSet = [];
  res.forEach((item, i) => {
    $('.seccion').text(item.seccion);
    dataSet.push([
      item.seccion,
      item.examen,
      item.abrev,
      item.resultado_capturado,
      // item.fecha_valida,
      item.metodo,
    ]);

  });

  $('#example').DataTable({
    data:dataSet,
    language: {
        "decimal": "",
        "emptyTable": "No hay informaci贸n",
        "info": "",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
  });
}

var pacientes = new List('pacientes', options);

/*$(document).ready(function() {
  sass = $('#idSass').val();
  examen = $('#idExamen').val();
  sass = sass.split('/');
  toma = sass[0];

  consecutivo = sass[1];
  data = new Object();
  data.toma = toma;
  data.consecutivo = consecutivo
  data.metodo = 'estudios';
  data.examen = examen;
  $.ajax({
    type:'post',
    data:data,
    dataType:'json',
    url:'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend:()=>{
        $('.loadResultados').show();
    }
  }).done(res=>{
    if (res.length>0) {
       result(res);
    }else {
      swal('Estudios Pendientes','Los resultados del examen aún no son liberados','info')
    }
    $('.loadResultados').hide();
  }).fail(err=>{

  })
});*/

// function obtener_tomas()
// {
//   let tomasempleado=[];
//   data = new Object();
//   data.nombre = "Aaron josue";
//   data.apellido = "Villegas Villatoro";
//   data.metodo = 'tomas_empleados';
//   $.ajax({
//     type:'post',
//     async: false,
//     data:data,
//     dataType:'json',
//     url:'https://asesores.ac-labs.com.mx/sass/index.php',
//     beforeSend:()=>{
//     }
//   }).done(res=>{
//       tomas=res.tomas;
//       tomas.forEach((item, i) => {
//         // console.log(item.toma);
//         tomasempleado.push(item.toma);
//       });

//   }).fail(err=>{
//   })
//   return tomasempleado;
// }
// var nombre='';
// var apellido='';
// var edad='';
// var sexo='';
// let estudiosempleado = [];
// var folio='';
// var options = {
//   valueNames: [ 'nombre'],
//   page: 6,
//   pagination: true,
//   pagination: {
//   innerWindow: 1,
//   left: 1,
//   right: 1,
//   paginationClass: "pagination",
//   item: "<li class='page-item'><a class='page-link page'></a></li>",
//   },
//   item: `<div class="col-lg-4 ml-md-auto mr-md-auto mt-2">
//           <div class="card">
//             <div class="text-center">
//               <img src="https://img.icons8.com/ios/38/000000/submit-resume.png"/>   
//             </div>  
//             <div class="">
//               <h6 class='primary nombre text-center'></h6>
//             </div>                        
//           </div>
//         </div>`
// };
function obtener_estudios() {
  nombre='';
  apellido='';
  edad='';
  estudiosempleado = [];
  folio='';
  id_paciente_sass='';
  data = new Object();
  data.id_sass = $("input[name=id_sass]").val();
  folio=data.id_sass;
  data.metodo = 'estudios_generales';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
    console.log("si trae");
    console.log(res);
    id_paciente_sass=res.id_paciente;
    console.log(res.nombre);
    nombre=res.nombre;
    console.log(res.apellidos);
    apellidos=res.apellidos;
    examenes = res.examenes;
    edad=res.edad;
    if(res.codigo_sexo==2){
      sexo='Femenino';
    }
    else{
      sexo='Masculino';
    }
    if(nombre!='' || apellidos!=''){
      console.log(examenes);
      $("#datos").empty();
      $("#datos").append(` 
        <div class="row p-1">
            <div class="col-md-6">
              <h6 class='font-weight-bold secondary'>Nombre:</h6>
              <small><p>${res.nombre} ${res.apellidos}</p></small>
              <h6 class="font-weight-bold secondary">Sexo:</h6>
              <p>${sexo}</p>
              <h6 class="font-weight-bold secondary">Edad:</h6>
              <p>${res.edad}</p>
            </div>
            <div class="col-md-6">
              
              <h6 class="font-weight-bold secondary">Curp:</h6>
              <input type="text" class="form-control" name="curp" id="curp">
              <h6 class="font-weight-bold secondary">Fecha de Nacimiento:</h6>
              <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" required>
              <button class="mt-2 btn btn-secondary btn-sm waves-effect waves-light text-height-1" onclick="agregarempleado();">
               Agregar
              </button>
            </div>
        </div>                      
        `);
      //   $(".list").empty();
      //   $("#busqueda").empty();
      //   $("#busqueda").append(`
      //   <input class="form-control search input-sm" placeholder="Buscar Estudio" />
      //    `);
      //   examenes.forEach((item, i) => {  
      //   var values = [{
      //    nombre: item.estudios.nombre,
      //   }];
      //   var userList = new List('estudios', options, values);
      //   estudiosempleado.push(item.estudios.id_examen);
      // });
      // $(".boton").append(`
     
      //  `);
       console.log("El arreglo:"+estudiosempleado)
    }
    else{
      $("#datos").empty();
      $("#datos").append(`<div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Error</h4>
      <div class="alert-body">
      No se encontró ningun paciente
      </div>
    </div>`)

    }
    
  }).fail(err => {
    $("#datos").empty();
    $("#datos").append(`<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Error</h4>
    <div class="alert-body">
    No se encontró ningun paciente
    </div>
  </div>`);
  })
  
}

// function programarestudiios(){
//   agregarempleado();
//   estudiosempleado.forEach(element => console.log(element));
//   console.log(folio);
//   console.log(nombre);
//   console.log(apellidos);
//   console.log(edad);
//   $.ajax({
// 		type:'post',
// 		dataType:'json',
// 		data:data,
// 		url:'../programarEstudiosEmpleado',
// 		beforeSend:()=>{
// 			$('#programarModal').modal('hide');
// 			$('#loading').modal('show');

// 		}
// 	}).done((res)=>{
// 		sass(res);
// 	}).fail(err=>{
// 			console.log(err);
// 			swal("Error", "Hubo algún problema, intentalo más tarde", "error");
// 	})
// }

function agregarempleado(){
  curp = document.getElementById('curp').value;
  id_empresa = document.getElementById('id_empresa').value;
  fecha_nacimiento=document.getElementById('fecha_nacimiento').value;
  $.ajax({
    type:'post',
    dataType:'json',
    url:'./addEmpleado',
    data:{clave:id_paciente_sass,id_empresa:id_empresa,folio:folio,nombre:nombre,sexo:sexo,curp:curp,apellidos:apellidos,edad:edad,fecha_nacimiento:fecha_nacimiento},
}).done( function(response){
  swal({
    position:'top-end',
    icon:'success',
    text:"El paciente ha sido registrado correctamente",
    title:'Paciente Registrado',
    showConfirmButton:true
  }).then(isConfirm=>{
   location.reload();
  })
}).fail(function(error) {
    var message;
    
   
    if(error.status == 404){
      message = "El paciente ya se encuentra registrado";
      swal('Error',message,'error')
    }
    else{
      message = "Revise que la fecha de nacimiento este correcta...";
      swal('Error',message,'error');
    }
    
   
}); 
}
// function programarestudiios(){
//   e.preventDefault();
//   var formData = new FormData();
//   var input_file = document.getElementById("logo");
//   formData.append('logo',input_file.files[0]);
//   formData.append('nombre',$("#nombre").val());
//   formData.append('direccion',$("#direccion").val());
//   alert(input_file);
//   $.ajax({
//       url:'add_empresa',
//       type:'post',
//       data:formData,
//       dataType:'json',
//       contentType:false,
//       processData:false,
//       beforeSend:()=>{
//       }
//   }).done(res=>{
//       swal({
//           position: 'top-end',
//           icon: 'success',
//           text: 'Los datos fueron almacenados de manera correcta',
//           title: 'Empresa Registrada',
//           showConfirmButton: true
//       }).then(isConfirm => {
//           location.reload();
//       });         
//   }).fail(err=>{
//       swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
//   });

// }
// $('#alta').on('submit',function(e) {
//   e.preventDefault();
//   $("#btn_enviar").prop("disabled", true);

//   $.ajax({
//       type:'post',
//       dataType:'json',
//       url:'altaEmpleados',
//       data:$('#alta').serialize()
//   }).done( function(response){
//     $("#btn_enviar").prop("disabled", false);
//     swal({
//       position:'top-end',
//       icon:'success',
//       text:'Los datos fueron almacenados de manera correcta',
//       title:'Paciente Registrado',
//       showConfirmButton:true
//     }).then(isConfirm=>{
//      location.reload();
//     })
//   }).fail(function(error) {
//       var message;
//       if(error.status == 422){
//           message = "Algo ha ocurrido, verifica que todos los campos sean correctos, y que haya seleccionado un grupo";
//           swal('Error',message,'error');
//       }
//       else if (error.status == 400){
//           message = "La curp ingresada ya se encuentra registrado, intenta con otra";
//           swal('Error',message,'error')
//       }
//       else if(error.status == 424){
//           message = "El correo ingresado ya se encuentra registrado, intenta con otra";
//           swal('Error',message,'error')
//       }
//       else if(error.status == 423){
//           message = "La clave ingresada ya se encuentra registrada, intenta con otra";
//           swal('Error',message,'error')
//       }
//       else if(error.status == 418){
//           message = "El grupo que ya existe. Por favor selecciónelo de la lista de grupos o elija otro nombre";
//           swal('Error',message,'error')
//       }
//       else{
//           message = "Algo ocurrió, inténtalo más tarde...";
//           swal('Error',message,'error')
//       }
//       $("#btn_enviar").prop("disabled", false);
//   });

// });



function result(res) {
  dataSet = [];
  res.forEach((item, i) => {
    $('.seccion').text(item.seccion);
    let metodo=decode_utf8(item.metodo);
    // let color=null;
    // if(item.semaforo=='rgb(0, 255, 0)'){
    //   color='<span class="badge badge-light-success">Normal</span>';

    // }
    // color,
    dataSet.push([
      
      item.seccion,
      item.examen,
      item.abrev,
      item.resultado_capturado,
     
      metodo,
      // item.fecha_valida,
      
    ]);
    console.log("Los resultados son:");
    console.log(item.seccion);
    console.log(item.examen);
    console.log(item.abrev);
    console.log(item.resultado_capturado);
    console.log(decode_utf8(item.metodo));
  });

  $('#example').DataTable({
    data:dataSet,
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
  });
  function decode_utf8(s) {
    return decodeURIComponent(escape(s));
}
}

