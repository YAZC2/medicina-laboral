
// ABRIR MODAL PARA CREAR NUEVO ANTECEDENTE
$("#btn_antecedenteFormModal_open").click(function(){
    $("#antecedenteFormModal").modal();
})

// ENVIO DE FORMULARIO DE NUEVO ANTECEDENTE
$("#form_add_antecedenteForm").submit(function(e){
    e.preventDefault();
    let form = $("#form_add_antecedenteForm").serialize();
    $.ajax({
        url:'add_antecedenteForm', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $("#btn_antecendenteForm_modal").attr('disabled',true);
            $("#btn_antecendenteForm_modal").find('span').show();
        }
    }).done(res=>{
        $("#antecedentes_accordion").append('<div class="collapse-border-item collapse-header card">'+
            '<div>'+
                '<div style="cursor: default !important;" class="card-header" id="heading'+res.id+'"'+
                    'aria-expanded="false" aria-controls="collapse'+res.id+'">'+
                    '<span class="lead collapse-title">'+res.nombre+'</span>'+
                    '<div>'+
                        '<button style="margin-right: 18px;" class="add_answer btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light" '+
                        'type="button" '+
                        'data-id="'+res.id+'">'+
                            '<i class="feather icon-plus"></i>'+
                        '</button>'+
                        '<button style="margin-right: 18px;" class="edit_form btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light" '+
                        'type="button" '+
                        'data-id="'+res.id+'">'+
                            '<i class="feather icon-edit"></i>'+
                        '</button>'+
                        '<button class="trash_form mr-3 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light" '+
                        'type="button" '+
                        'data-id="'+res.id+'">'+
                            '<i class="feather icon-trash"></i>'+
                        '</button>'+
                        '<div class="cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse'+res.id+'" aria-expanded="false" aria-controls="collapse'+res.id+'">'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div id="collapse'+res.id+'" class="collapse" aria-labelledby="heading'+res.id+'" data-parent="#antecedentes_accordion">'+
                '<div class="card-body">'+
                    '<h5>Aún no se han creado preguntas vinculadas</h5>'+
                '</div>'+
            '</div>'+
        '</div>');

        $("#antecedenteFormModal").modal('hide');
        $("#nombre_antecedente").val('');
        iniciarEventosClick();
        $("#btn_antecendenteForm_modal").attr('disabled',false);
        $("#btn_antecendenteForm_modal").find('span').hide();
        swal('Exito','Antecedente creado correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
});

$(".edit_form").click(function(){
    let id = $(this).data('id');
    let nombre = $("#heading"+id+" span");
    $("#nombre_antecedente_edit").val(nombre.text());
    $("#antecedente_form_edit_id").val(id);
    $("#antecedenteFormModalEdit").modal();
});

$("#form_edit_antecedenteForm").submit(function(e){
    e.preventDefault();
    let form = $("#form_edit_antecedenteForm").serialize();
    $.ajax({
        url:'edit_antecedenteForm', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $("#btn_antecendenteFormEdit_modal").find('span').show();
            $("#btn_antecendenteFormEdit_modal").attr('disabled',true);
        }
    }).done(res=>{
        let nombre = $("#nombre_antecedente_edit").val();
        let id = $("#antecedente_form_edit_id").val();
        $("#heading"+id+" span").text(nombre);
        $("#antecedenteFormModalEdit").modal('hide');
        iniciarEventosClick();

        $("#btn_antecendenteFormEdit_modal").find('span').hide();
        $("#btn_antecendenteFormEdit_modal").attr('disabled',false);

        swal('Exito','Antecedente actualizado correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
});

$(".trash_form").click(function(){
    let id = $(this).data('id');
    let nombre = $("#heading"+id+" span");
    $("#nombre_delete_form").text(nombre.text());
    $("#antecedente_form_delete_id").val(id);
    $("#antecedenteFormModalDelete").modal();
});

$("#form_delete_antecedenteForm").submit(function(e){
    e.preventDefault();
    let form = $("#form_delete_antecedenteForm").serialize();
    $.ajax({
        url:'delete_antecedenteForm', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $("#btn_antecedenteFormModal_delete").find('span').show();
            $("#btn_antecedenteFormModal_delete").attr('disabled',true);
        }
    }).done(res=>{
        $("#heading"+res).parent().parent().remove();
        $("#antecedenteFormModalDelete").modal('hide');
        $("#btn_antecedenteFormModal_delete").find('span').hide();
        $("#btn_antecedenteFormModal_delete").attr('disabled',false);
        swal('Exito','Antecedente eliminado correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
})

$(".add_answer").click(function(){
    let id = $(this).data('id');
    let nombre = $("#heading"+id+" span");
    $("#form_answer_nombre").text(nombre.text());
    $("#antecedente_form_id").val(id);

    $("#antecedenteAnswerModal").modal();
})

$("#answer_form_antecedente").submit(function(e){
    e.preventDefault();
    let form = $("#answer_form_antecedente").serialize();
    $.ajax({
        url:'add_antecedenteAnswer', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $(this).find('button').attr('disabled',true);
            $(this).find('button').find('span').show();
        }
    }).done(res=>{
        let antecedente_form_id = $("#antecedente_form_id").val();

        if($("#collapse"+antecedente_form_id+" .card-body h5").length == 0)
        {
            $("#collapse"+antecedente_form_id+" .card-body ul").append('<li class="list-group-item d-flex justify-content-between align-items-center" id="answer'+res.id+'">'+
                '<span>'+res.pregunta+'</span>'+
                '<div>'+
                    '<button data-id="'+res.id+'" class="edit_answer mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">'+
                        '<i class="feather icon-edit"></i>'+
                    '</button>'+
                    '<button data-id="'+res.id+'" class="trash_answer btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">'+
                        '<i class="feather icon-trash"></i>'+
                    '</button>'+
                '</div>'+
            '</li>');
        }else{
            $("#collapse"+antecedente_form_id+" .card-body").html('<ul class="list-group pl-1 pl-sm-3">'+
                    '<li class="list-group-item d-flex justify-content-between align-items-center" id="answer'+res.id+'">'+
                        '<span>'+res.pregunta+'</span>'+
                        '<div>'+
                            '<button data-id="'+res.id+'" class="edit_answer mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">'+
                                '<i class="feather icon-edit"></i>'+
                            '</button>'+
                            '<button data-id="'+res.id+'" class="trash_answer btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">'+
                                '<i class="feather icon-trash"></i>'+
                            '</button>'+
                        '</div>'+
                    '</li>'+
            '</ul>');
        }
        $("#antecedenteAnswerModal").modal('hide');
        $("#nombre_answer").val('');
        iniciarEventosClick();
        $(this).find('button').attr('disabled',false);
        $(this).find('button').find('span').hide();
        swal('Exito','Pregunta creada correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
});

$(".edit_answer").click(function(){
    let id = $(this).data('id');
    let nombre = $("#answer"+id+" span").text();
    $("#nombre_answer_edit").val(nombre);
    $("#answer_id").val(id);
    $("#antecedenteAnswerModalEdit").modal();
});

$("#answer_edit_form_antecedente").submit(function(e){
    e.preventDefault();
    let form = $("#answer_edit_form_antecedente").serialize();
    $.ajax({
        url:'edit_antecedenteAnswer', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $(this).find('button').attr('disabled',true);
            $(this).find('button').find('span').show();
        }
    }).done(res=>{
        $("#answer"+res.id+" span").text(res.pregunta);
        $("#antecedenteAnswerModalEdit").modal('hide');
        iniciarEventosClick();
        $(this).find('button').attr('disabled',false);
        $(this).find('button').find('span').hide();
        swal('Exito','Pregunta actualizada correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
})

$(".trash_answer").click(function(){
    let id = $(this).data('id');
    let nombre = $("#answer"+id+" span").text();

    $("#answer_delete_id").val(id);
    $("#nombre_delete_answer").text(nombre);
    $("#antecedenteAnswerModalDelete").modal();
});

$("#answer_delete_form_antecedente").submit(function(e){
    e.preventDefault();
    let form = $("#answer_delete_form_antecedente").serialize();
    $.ajax({
        url:'delete_antecedenteAnswer', // place your controller's method
        type:'post',
        data:form,
        dataType:'json', // text, json etc
        beforeSend:()=>{
            $("#btn_antecedenteAnswerModal_delete").find('span').show();
            $("#btn_antecedenteAnswerModal_delete").attr('disabled',true);
        }
    }).done(res=>{
        $("#answer"+res).remove();
        $("#antecedenteAnswerModalDelete").modal('hide');
        $("#btn_antecedenteAnswerModal_delete").find('span').hide();
        $("#btn_antecedenteAnswerModal_delete").attr('disabled',false);
        swal('Exito','Pregunta eliminada correctamente','success');
    }).fail(err=>{
        swal('Error','Hubo algún problema intentalo más tarde','error');
    });
})

function iniciarEventosClick() {
    $(".edit_form").click(function(){
        let id = $(this).data('id');
        let nombre = $("#heading"+id+" span");
        $("#nombre_antecedente_edit").val(nombre.text());
        $("#antecedente_form_edit_id").val(id);
        $("#antecedenteFormModalEdit").modal();
    });
    $(".trash_form").click(function(){
        let id = $(this).data('id');
        let nombre = $("#heading"+id+" span");
        $("#nombre_delete_form").text(nombre.text());
        $("#antecedente_form_delete_id").val(id);
        $("#antecedenteFormModalDelete").modal();
    });
    $(".add_answer").click(function(){
        let id = $(this).data('id');
        let nombre = $("#heading"+id+" span");
        $("#form_answer_nombre").text(nombre.text());
        $("#antecedente_form_id").val(id);

        $("#antecedenteAnswerModal").modal();
    })
    $(".edit_answer").click(function(){
        let id = $(this).data('id');
        let nombre = $("#answer"+id+" span").text();
        $("#nombre_answer_edit").val(nombre);
        $("#answer_id").val(id);
        $("#antecedenteAnswerModalEdit").modal();
    });
    $(".trash_answer").click(function(){
        let id = $(this).data('id');
        let nombre = $("#answer"+id+" span").text();

        $("#answer_delete_id").val(id);
        $("#nombre_delete_answer").text(nombre);
        $("#antecedenteAnswerModalDelete").modal();
    });
}



// dragula([document.querySelector(".basic-list-group")])
// .on('out', function (el,container) {
//     idForm = $(this)[0].containers[0].dataset.id
//     order(idForm);
//   }).on('drop', function(){
//     swal('Exito','Pregunta eliminada correctamente','success');
//   })
//
// function order(id) {
//   var elements = [];
//   $('.elements_question'+id).each(function(index) {
//     elements.push($(this).data('id'));
//   });
//   data = new Object();
//   data.answers = elements;
//   data.formId = id;
//   $.ajax({
//       url:'order', // place your controller's method
//       type:'post',
//       data:data,
//       dataType:'json', // text, json etc
//       beforeSend:()=>{
//       }
//   }).done(res=>{
//
//   }).fail(err=>{
//     //  swal('Error','Hubo algún problema intentalo más tarde','error');
//   });
// }
