// Inicio de List
var options = {
    valueNames: ['nombre','clave',  'detalle'],
    page: 5,
    pagination: true,
    pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item: "<li class='page-item'><a class='page-link page' ></a></li>",
    }
};

var consultas = new List('medicamentos', options);



// var options = {
//     valueNames: [ 'nombre',  ]
// };

// var hackerList = new List('medicamentos', options);

// var options = {
//     valueNames: [ 'name', 'born' ],
//     page: 3,
//     pagination: true,
//     pagination: {
//         innerWindow: 1,
//         left: 1,
//         right: 1,
//         paginationClass: "pagination",
//         item: "<li class='page-item'><a class='page-link page' ></a></li>",
//     }
// };
  
//   var userList = new List('users', options);





//Fin de List 

//Agregar Medicamento
$("#btnopenmedicamento").click(function () {
    $("#agregarmodal").modal();
})
$("#form_medicamentos").submit(function (e) {
    e.preventDefault();
    form = $("#form_medicamentos").serialize();
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'add_medicamento',
        data: form,
    }).done(function (response) {
        $("#btnopenmedicamento").prop("disabled", false);
        swal({
            position: 'top-end',
            icon: 'success',
            text: 'Los datos fueron almacenados de manera correcta',
            title: 'Medicamento Registrado',
            showConfirmButton: true
        }).then(isConfirm => {
            location.reload();
        })
    }).fail(function (error) {
        var message;
        message = "Algo ocurrió, inténtalo más tarde...";
        swal('Error', message, 'error')


        $("#btnopenmedicamento").prop("disabled", false);
    });
});
//Actualizar Medicamento
$(document).on('click', '.edit_med', function () {
    cargadatos($(this).data('id'));

});
function cargadatos(id) {
    $.ajax({
        type: "get",
        url: "getMedicamento/" + id,
        dataType: "json",
        beforeSend: () => {

        }
    }).done(res => {
        console.log(res.medic);
        document.getElementById("id").value = res.medic.id;
        document.getElementById("clave_e").value = res.medic.clave;
        document.getElementById("nombre_e").value = res.medic.nombre;
        document.getElementById("detalle_e").value = res.medic.detalle;
        $('#editar').modal('show');
    }).fail(err => {

        console.log(err);
        swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
    })
}
$('#form_edit_medicamentos').on('submit', function (e) {
    e.preventDefault();
    swal({
        title: "Modifiación",
        text: "¿Estas seguro de modificar los datos?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            modificacion();
        } else {
            swal("Cancelado", "La modificación fue cancelada", "error");
        }
    });

});
function modificacion() {
    $.ajax({
        type: 'POST',
        url: 'edit_medicamento',
        dataType: 'json',
        data: $('#form_edit_medicamentos').serialize(),
        beforeSend: () => {

        },
        success: function (response) {
            $('#btnedit').removeAttr('disabled');
            document.getElementById("clave_e").innerHTML = response.clave;
            document.getElementById("nombre_e").innerHTML = response.nombre;
            document.getElementById("detalle_e").innerHTML = response.detalle_e;
            $('#editar').modal('hide');
            swal({
                position: 'top-end',
                icon: 'success',
                text: 'Los datos se han actualizado correctamente',
                title: 'Medicamento Actualizado',
                showConfirmButton: true
            }).then(isConfirm => {
                location.reload();
            })

        }
    }).fail(function (error) {
        $('#btnedit').removeAttr('disabled');       
          if(error.status == 422) {
              swal('Error','Todos los campos son obligatorios. Revise que haya ingresado correctamente la información.','info');
          } else if (error.status == 400 && error.responseJSON.message == "Error en CURP") {
              swal('Error','La CURP ingresada ya se encuentra registrada, intente con otra','info');
          } else if (error.status == 400 && error.responseJSON.message == "Error en correo") {
              swal('Error','El correo ingresado ya se encuentra registrado, intente con otro','info');

          } else if (error.status == 400 && error.responseJSON.message == "Error en clave") {
              swal('Error','La clave del empleado ya se encuentra registrada, intente con otra','info');
          } else {
              swal('Error','Ocurrio algo inesperado, intentalo más tarde','error');
          }

    });
}
//Eliminar Medicamento
$(document).on('click', '.trash_med', function () {
    let id=$(this).data('id');
    swal({
        title: "Eliminación",
        text: "¿Estas seguro de eliminar los datos?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            eliminacion(id);
        } else {
            swal("Cancelado", "La operación ha sido cancelada", "error");
        }
    });

});

function eliminacion(id) {
    $.ajax({
        type: 'POST',
        url: 'delete_medicamento/'+id,
        dataType: 'json',
        data: $('#form_edit_medicamentos').serialize(),
        beforeSend: () => {
        },
        success: function (response) {          
            swal({
                position: 'top-end',
                icon: 'success',
                text: 'Los datos se han eliminado correctamente',
                title: 'Medicamento Eliminado',
                showConfirmButton: true
            }).then(isConfirm => {
                location.reload();
            })

        }
    }).fail(function (error) {
        $('#btnedit').removeAttr('disabled');
        if(error.status == 422) {
            swal('Error','Todos los campos son obligatorios. Revise que haya ingresado correctamente la información.','info');
        } else if (error.status == 400 && error.responseJSON.message == "Error en CURP") {
            swal('Error','La CURP ingresada ya se encuentra registrada, intente con otra','info');
        } else if (error.status == 400 && error.responseJSON.message == "Error en correo") {
            swal('Error','El correo ingresado ya se encuentra registrado, intente con otro','info');

        } else if (error.status == 400 && error.responseJSON.message == "Error en clave") {
            swal('Error','La clave del empleado ya se encuentra registrada, intente con otra','info');
        } else {
            swal('Error','Ocurrio algo inesperado, intentalo más tarde','error');
        }
    });
}
