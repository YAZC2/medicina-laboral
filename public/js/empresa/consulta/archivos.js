$("#document").on("submit", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("document"));
   //formData.append("dato", "valor");
   //formData.append(f.attr("name"), $(this)[0].files[0]);
   $.ajax({
     url: "../consultaDocument",
     type: "post",
     dataType: "json",
     data: formData,
     cache: false,
     contentType: false,
     processData: false,
     beforeSend:()=>{
       $('.doc').show();
     }
     }).done(function(res){
       $('#document')[0].reset();
       $('.doc').hide();
       toastr.success("El Archivo se almaceno correctamente", "Archivo Guardado", {
           positionClass: "toast-top-center",
           containerId: "toast-top-center"
      }).css({
          width: "700px",
          "max-width": "700px"
      })
      addElementDocument(res);
   }).fail(err=>{
     toastr.error("Error al subir el archivo, intentalo nuevamente.", "Error", {
         positionClass: "toast-top-center",
         containerId: "toast-top-center"
     }).css({
         width: "700px",
         "max-width": "700px"
     })
   });
});



$("#img").on("submit", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("img"));
   //formData.append("dato", "valor");
   //formData.append(f.attr("name"), $(this)[0].files[0]);
   $.ajax({
     url: "../consultaDocument",
     type: "post",
     dataType: "json",
     data: formData,
     cache: false,
     contentType: false,
     processData: false,
     beforeSend:()=>{
       $('.img').show();
     }
     }).done(function(res){
       $('#img')[0].reset();
      $('.img').hide();
       toastr.success("La imagen se almaceno correctamente", "Imagen Guardada", {
           positionClass: "toast-top-center",
           containerId: "toast-top-center"
      }).css({
          width: "700px",
          "max-width": "700px"
      });
      addElementImg(res);

   }).fail(err=>{
     toastr.error("Error al subir la imagen, intentalo nuevamente.", "Error", {
         positionClass: "toast-top-center",
         containerId: "toast-top-center"
     }).css({
         width: "700px",
         "max-width": "700px"
     })
   });
});


$(document).on('click','.delete', function(e){
  swal({
  title: "Atención",
  text: "¿Seguro que deseas eliminar el archivo?",
  icon: "warning",
  buttons: {
    cancel: {
        text: "No",
        value: null,
        visible: true,
        className: "",
        closeModal: false,
    },
    confirm: {
        text: "Si",
        value: true,
        visible: true,
        className: "",
        closeModal: false
    }
  }
  })
  .then((isConfirm) => {
  if (isConfirm) {
    deleteA($(this).data('key'));
  } else {
    swal("Cancelado", "La operación se cancelo correctamente", "error");
  }
  });
})



function deleteA(key) {
  $.ajax({
    url: "../deleteDocument",
    type: "post",
    dataType: "json",
    data: {id:key},
  }).done(function(res){
    console.log($(this).parent());
    $('#'+res.id).remove();
    swal("Eliminado", "La imagen se elimino correctamente", "success");
  }).fail(err=>{
    swal("Error", "Error al eliminar la imagen, intentalo nuevamente.", "error");
  });
}


function addElementDocument(res){
  if (res.extencion == "pdf")
    img = "<img src='https://img.icons8.com/fluent/48/000000/pdf.png'/>"
  else if(res.extencion == "xlsx"){
    img = "<img src='https://img.icons8.com/color/48/000000/xls.png'/>"
  }else if (res.extencion == "csv") {
    img = "<img src='https://img.icons8.com/color/48/000000/csv.png'/>"
  }else if (res.extencion == "docx" || res.extencion == "doc") {
    img = "<img src='https://img.icons8.com/color/48/000000/word.png'/>"
  }else if (res.extencion == "txt") {
    img = "<img src='https://img.icons8.com/color/48/000000/txt.png'/>"
  }

  $('.content_doc').append(`
    <div id="${res.id}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
      <div class="mr-50">
      ${img}
      </div>
      <div class="user-page-info">
        <h6 class="mb-0">${ res.nombre }</h6>
        <span class="font-small-2">hace un momento</span>
      </div>
      <a type="button" target="_blank" href="../storage/app/consultas/${res.storage}" class="btn btn-secondary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-eye"></i></i></a>
      <button type="button" data-key="${res.id}" class="delete btn btn-danger btn-icon ml-1 waves-effect waves-light"><i class="feather icon-trash"></i></i></button>
    </div>
  `);
}

https://expedienteclinico.humanly-sw.com/developing/storage/app/consultas/1602799858_descarga.png

function addElementImg(res) {
  if (res.extencion == "png")
    img = "<img src='https://img.icons8.com/officel/48/000000/png.png'/>"
  else if(res.extencion == "jpg"){
    img = "<img src='https://img.icons8.com/officel/48/000000/jpg.png'/>"
  }else {
    img = "<img src='https://img.icons8.com/officel/48/000000/jpg.png'/>"
  }

  $('.content_image').append(`
    <div id="${res.id}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
      <div class="mr-50">
      ${img}
      </div>
      <div class="user-page-info">
        <h6 class="mb-0">${ res.nombre }</h6>
        <span class="font-small-2">hace un momento</span>
      </div>
      <a type="button" target="_blank" href="../storage/app/consultas/${res.storage}" class="btn btn-secondary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-eye"></i></i></a>
      <button type="button" data-key="${res.id}" class="delete btn btn-danger btn-icon ml-1 waves-effect waves-light"><i class="feather icon-trash"></i></i></button>
    </div>
  `);

}
