$(document).ready(function () {

    $('.dibujaEstudios').empty();
    idtoma = $("#Idtoma").val();
    curp = $("#curp").val();

    estudio_laboratorio(idtoma);
    showEstudios(idtoma, curp);
    estudios_rx(idtoma);
});
//IMAGENOLOGIA
function estudios_rx(Idtoma) {
    $(".dibujaEstudios").empty();
    url = "https://humanly-sw.com/imagenologia/Api/V1_regresainterpretacion";
    console.log(url);
    nim = Idtoma;
    nim = nim.split('/');
    nim1 = nim[0];
    nim2 = nim[1];
    if (nim2.length != 4) {
        if (nim2.length == 1) {
            n = "000" + nim2;
        }
        else if (nim2.length == 2) {
            n = "00" + nim2;
        }
        else if (nim2.length == 3) {
            n = "0" + nim2;
        }
    }
    Idtoma = nim1 + "/" + n;
    console.log(Idtoma);
    $.ajax({
        type: 'post',
        data: {
            sass: Idtoma,
        },
        dataType: 'json',
        url: url,
        beforeSend: () => {
            $(".dibujaEstudios").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
        </div>`);
        }
    }).done(res => {
        console.log(res);
        $(".dibujaEstudios").empty();
        res.forEach((item, i) => {
            $('.dibujaEstudios').append(`
                <div class="col-md-4 mb-1">
                    <div class="text-center shadow bg-white p-1  rounded">
                        <h5 class='secondary'>${item.nombre}</h5>
                    
                        <hr>            
                        <a onclick="daicom('${Idtoma}','${item.id_estudio}')" class="btn-sm btn-primary btn text-white"><i class="feather icon-airplay"></i></a>
                        <hr>
                    
        
                    </div>
                </div>
            `);
        });
    }).fail(err => {
        console.log("No se ejecuto");
    });

}
function daicom(nim, codigo) {
    $('.contEstudios').empty();
    $(".contImagenologia").empty();
    showDaicoms(nim, codigo);
}
function showDaicoms(tomaId, codigo) {
    let Idtoma = tomaId.replace("-", "/");
    console.log(Idtoma);

    $.ajax({
        type: "POST",
        url: "https://humanly-sw.com/imagenologia/Api/V1_regresainterpretacion",
        dataType: "json",
        data: {
            sass: Idtoma,
            //sass: "101210401/0023"

        },
        beforeSend: () => {
            $("#estudios").empty();
            $("#contImagenologia").empty();
            $('#estudiosShow').modal('show');
        },
    })
        .done((res) => {
            das = JSON.stringify(res);
            if (das == "[]" || tomaId == null) {
                $(".contImagenologia").append(`
                    <div class='col-md-12'>
                        <h5 class="mb-0">
                            <div class="alert alert-primary alert_32">
                                <p class="text-center">
                                    !No existen tomas¡
                                </p>
                             </div>
        
                         </h5>
                    </div>
                `);
            }
            else {
                $(".form-check").hide();
                let i = 0;
                datos = JSON.stringify(res);
                arr1 = datos;
                j = 0;
                for (let item of res) {
                    arr = item.dicoms;
                    if (item.id_estudio == codigo) {
                        if (arr.length == 0) {
                            $("#estudios").append(` 
                                <div class="form-check form-check-inline">
                                     <div class="custom-control custom-radio">
                                         <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" value="" onclick='mostrar("${codigo}",this.value)'>
                                        <label class="custom-control-label secondary" for="customRadio1">Toma 1</label>
                                     </div> 
                                </div>                                 
                            `);
                        } else {

                            while (j <= arr.length - 1) {
                                var ruta1 =
                                    "https://imagenologia.dynpbx.mx/Imagenologia2/" +
                                    arr[j];

                                var ruta = ruta1.replace("\\", "/");

                                $("#estudios").append(` 
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio${j}" name="customRadio" class="custom-control-input" value="${ruta}" onclick='mostrar("${codigo}",this.value)'>
                                            <label class="custom-control-label secondary" for="customRadio${j}">Toma ${j}</label>
                                        </div> 
                                    </div>                                 
                                `);

                                j++;
                            }
                        }
                    }
                    i++;
                }
            }
        })
        .fail((err) => {
            $(".loadImagenologia").hide();
            console.log("Error");
            console.log(err);
        });
}
function mostrar(codigo, valor) {
    let sindaicom;
    let interurl;
    if (valor == null || valor == "") {
        sindaicom =
            "<div class='col-md-12'><h5 class=mb-0><div class='alert alert-primary alert_32'><p class='text-center'�0�3No existen tomas!</p></div></h5></div>";
    } else {
        sindaicom = "";
    }
    console.log("Example:" + sindaicom);
    k = 0;
    var d = JSON.parse(datos);

    var inter;
    var encointer;
    var clase;
    var d = JSON.parse(datos);
    console.log(d);
    for (let item of d) {
        interurl = item.interpretacion;
        if (item.id_estudio == codigo) {
            inter = item.interpretacion;
            interpreta = inter.length;
            if (interpreta < 200) {
                clase =
                    '<embed src="http://humanly-sw.com/imagenologia/uploads/interpretacionpdf/' +
                    atob(item.interpretacion) +
                    '" type="application/pdf" width="100%;" height="702px">';
                console.log(clase);
            } else {
                encointer = atob(item.interpretacion);
                clase = decode_utf8(encointer);

                clase = "<img src='https://has-humanly.com/pagina_has/images/cahitoredondo.svg' class='membretada'>" + clase + "<img src='https://has-humanly.com/pagina_has/images/logo_30.svg' class='imgleft'>" +
                    '<a href="https://humanly-sw.com/imagenologia/Pdf/handle_create_pdf/' + item.id_pdf + '" class="btn task-cat primario ml-2 mt-1 ">Descargar <img src="https://humanly-sw.com/imagenologiadev/uploads/assets/cloud_download.svg" alt="icono de nube"></a>';

            }
            console.log(inter.length);
            $(".contImagenologia").empty();

            $(".contImagenologia").append(`
            <div class='col-md-12'>
                 <div class="card">
                         <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                
                            </h5>
                         </div>
  
                 <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class='row'>
                      <div class='col-md-6'>
                        <div class="card-body">
                         
                         <div class="row" id="dicomview">                        
                            <div class="col-md-12">
                            ${sindaicom}
                                <div style="width:100%;height:700px;position:relative;color: white;display:inline-block;border-style: dashed; border-width: 1px; border-color:gray;"
                                    oncontextmenu="return false" class='disable-selection noIbar' unselectable='on' onselectstart='return false;'
                                        onmousedown='return false;'>
                                        <div id="dicomImage" style="width:100% ;height:700px;top:0px;left:0px; position:absolute"></div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
              
                     <div class='col-md-6'>
                        <div class="card-content">              
                        ${clase}   
                                   
                        </div>  
                                     
                     </div>
                     
                 </div>
                
              </div>`);


        }
        k++;
    }
    downloadAndView("dicomImage", valor);
    function decode_utf8(s) {
        return decodeURIComponent(escape(s));
    }
}
//LABORATORIO
$(document).on('click', '.showlaboratorio', function () {
    $('#estudios').empty();
    $(".contImagenologia").empty();
    $('#estudiosShow').modal('show');
    pdf_lab($(this).data('id_toma'), $(this).data('id_paciente'), $(this).data('contra'));
});
function estudio_laboratorio(Idtoma) {
    $("#tabla_dinamica").empty();
    nim = Idtoma;
    nim = nim.split('/');
    nim1 = nim[0];
    nim2 = nim[1];
    toma = nim1;
    consecutivo = nim2;
    metodo = 'recupera_empleado';
    data = new Object();
    data.metodo = metodo;
    data.toma = toma;
    data.consecutivo = consecutivo;
    $.ajax({
        type: 'post',
        data: data,
        dataType: 'json',
        url: 'https://asesores.ac-labs.com.mx/sass/index.php',
        beforeSend: () => {
            $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
        </div>`);
        }
    }).done(res => {

        res.pacientes.forEach((item, i) => {
            $('.dibujaEstudios').append(`<div class="col-md-4 mb-1">
                    <div class="text-center shadow bg-white p-1  rounded">
                        <h5 class="secondary">LABORATORIO</h5>
                    
                        <hr>            
                        <a data-id="${item.Nim}" data-id_paciente="${item.id_paciente}" data-id_toma="${item.id_toma}" data-contra="${item.Contrasena}" class="btn btn-primary btn-sm showlaboratorio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>  <hr>
                    
        
                    </div>
                </div>`);

        });


        nueva_tabla();


    }).fail(err => {

    })
}
function pdf_lab(idtoma, paciente, contrasena) {
    $('.contEstudios').empty();
    data = new Object();
    console.log(idtoma);
    console.log(paciente);
    console.log(contrasena);
    id_toma = idtoma;
    usuario = paciente;
    password = contrasena;
    console.log(contrasena);
    cliente = "ael";
    json_datos = `json={"id_toma":"${id_toma}","usuario":"${usuario}","password":"${password}","cliente":"ael"}`;

    url = `https://ael.sass.com.mx/ws/ws_resultados.php?${json_datos}`;
    console.log(url);
    var encontrado = '';
    datosem_la = [];
    $.ajax({
        type: 'get',
        data: data,
        dataType: 'json',
        url: url,
        beforeSend: () => {
            $(".loadEstudios").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
        </div>`);
        }
    }).done(res => {

        if (res.estatus.error == false) {

            pdf = res.PDF;
            $(".loadEstudios").empty();
            $('.contEstudios').append(`<div class="col-md-12"><embed
        src="data:application/pdf;base64,${pdf}"
        type="application/pdf" width="100%" height="500px"/></div>`);

        }
        else {
            $(".loadEstudios").empty();
            $('.contEstudios').append(`
        <div class="col-md-12">
        <div class="alert alert-warning" role="alert">
        <div class="alert-body">
         ${res.estatus.msg_error}
        </div>
      </div>
        </div>`);


        }





    }).fail(err => {
        console.log("No se ejecuto");
    });

    return encontrado;

}
function mandapdf(pdf) {

    $.ajax({
        type: 'post',
        data: { pdf: pdf },
        url: './labo_pdf',
        beforeSend: () => {
        }
    }).done(res => {



    }).fail(err => {
        console.log("No Enviado");
    });
}
//MEDICINA LABORAL
function showEstudios(tomaId, curp) {
    $.ajax({
        type: "get",

        url: "/obtenertomacorreo/" + tomaId,
        dataType: "json",
        beforeSend: () => {
            $('.loadEstudios').show();
        }
    }).done(res => {

        $('.loadEstudios').hide();
        toma = res.toma;
        estudios = res.estudios;
        estudios.forEach((item, i) => {


            if (item.nombre == 'ELECTROCARDIOGRAMA CON INTERPRETACIÓN') {
                e_clase = '<div class="btn-group" role="group" aria-label="Basic example"><a type="button" target="_blank" href="/trazo/' + curp + '/' + toma.id + '" class="btn-outline-secondary btn-sm btn-primary btn"><i class="feather icon-activity"></i></a></div>';
            }
            else if (item.nombre == 'ESPIROMETRIA') {

                e_clase = '<a type="button" target="_blank" href="/Resultado_Espirometria/' + curp + '/' + toma.id + '" class="btn-sm btn-primary btn" name="button"><i class="feather icon-cast"></i></a>';
            }
            else if (item.nombre == 'AUDIOMETRIA TONAL Y AEREA') {
                e_clase = '<a type="button" target="_blank" href="/c_Audiometria' + '/' + toma.id + '/' + curp + '" class="btn-sm btn-primary btn" name="button"><i class="feather icon-headphones"></i></a>';
            }
            else {
                e_clase = `<a type="button" target="_blank" href="/resultado_fisioterapia/${curp}/${toma.id}" class="btn-sm btn-primary btn" name="button">
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="16" height="16" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><path d="M95.92308,6.61538c-9.12199,0 -16.53846,7.41647 -16.53846,16.53846c0,9.12199 7.41647,16.53846 16.53846,16.53846c9.12199,0 16.53846,-7.41647 16.53846,-16.53846c0,-9.12199 -7.41647,-16.53846 -16.53846,-16.53846zM86,46.30769c-7.28726,0 -13.23077,5.94351 -13.23077,13.23077v52.92308h13.23077l8.68269,-9.30288l-8.68269,-5.99519c-2.79087,-2.22235 -3.07512,-5.73678 -2.6875,-9.50962c1.16286,1.9381 2.97176,3.61779 4.75481,4.96154l19.84615,11.99038c1.31791,0.67188 1.98978,0.62019 3.30769,0.62019c1.98978,0 4.47055,-1.31791 5.78846,-3.30769c1.98978,-2.63581 0.82692,-6.69291 -2.48077,-8.68269l-19.84615,-11.78365l-5.99519,-22.32692l10.54327,21.5l11.16346,5.375h8.68269v-17.77885l15.91827,17.77885h17.15865l-29.14904,-35.14423c-2.63581,-2.63581 -5.94351,-4.54808 -9.92308,-4.54808zM120.31731,92.61538c2.63582,3.30769 4.13462,9.25121 0.82692,13.23077c-1.98978,2.63582 -5.375,4.54808 -8.68269,4.54808c-3.97956,0 -13.23077,-4.54808 -13.23077,-4.54808l-12.19712,13.23077h-40.72596c-7.28726,0 -13.23077,5.94351 -13.23077,13.23077c0,7.28726 5.94351,13.23077 13.23077,13.23077h46.30769c3.97957,0 6.61538,-2.63581 6.61538,-6.61538c0,-3.97956 -2.63581,-6.61538 -6.61538,-6.61538l-39.69231,-3.92788l39.69231,0.62019c7.28726,0 13.23077,2.63582 13.23077,9.92308c0,2.63582 -0.7494,4.6256 -2.06731,6.61538h59.53846c4.6256,0 8.68269,-4.05709 8.68269,-8.68269c0,-4.6256 -3.23017,-7.80408 -7.85577,-8.47596l-55.40385,-3.10096l10.95673,-14.88462l29.14904,-1.86058c3.97957,0 7.85577,-4.08293 7.85577,-8.0625c0,-3.97956 -3.85036,-7.85577 -8.47596,-7.85577zM16.53846,105.84615c-9.12199,0 -16.53846,7.41647 -16.53846,16.53846c0,9.12199 7.41647,16.53846 16.53846,16.53846c9.12199,0 16.53846,-7.41647 16.53846,-16.53846c0,-9.12199 -7.41647,-16.53846 -16.53846,-16.53846zM6.61538,145.53846c-3.97956,0 -6.61538,3.30769 -6.61538,6.61538v13.23077h172v-13.23077h-138.92308l-4.54808,-4.54808c-1.31791,-1.31791 -2.76503,-2.06731 -4.75481,-2.06731z"></path></g></g></svg>
          </a>`;
            }
            if (item.nombre != 'VALORACIÓN MÉDICA') {
                $('.dibujaEstudios').append(`
              <div class="col-md-4 mb-1">
                  <div class="text-center shadow bg-white p-1  rounded">
                      <h5 class='secondary'>${item.nombre}</h5>
                      <hr>
                      ${e_clase}
  
                  </div>
              </div>
          `)
            }

        });

    }).fail(err => {
        $('.loadEstudios').hide();
        console.log(err);
        
    })
}



