$(".table_estudios").DataTable({
  responsive: !1,
  language: {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "",
    "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "_MENU_",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
      "first": "Primero",
      "last": "Ultimo",
      "next": "Siguiente",
      "previous": "Anterior"
    }
  }

});

$(document).on('click', '.showEstudio', function () {
  console.log();
  $('#estudiosShow').modal('show');
  showEstudios($(this).data('id'), $(this).data('curp'));
});
$(document).on('click', '#estudio_laboratorio', function () {
  $("#tabla_dinamica").empty(); 
  
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();

  data.metodo = 'empresa_la';
  data.codigo = '0253';
  data.empresa = 'CADENA COMERCIAL OXXO';
  data.fecha1='2021-11-01';
  data.fecha2='2021-11-30';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
  
    $(".cargar").empty();
    $('#tabla_dinamica').append(`
    <div class='row'>            
        <div class='col-4'>
            <label for="start">Fecha Inicial:</label>
            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <label for="start">Fecha Final:</label>
            <input type="date" id="fecha_final" name="fecha_final" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <a data-id="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%"><thead><tr>
      <th>Empleado</th><th>Toma</th><th>Acciones</th></tr></thead>
      <tbody></tbody></table>`);

    res.pacientes.forEach((item, i) => {
      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td><a data-id="${item.Nim}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a> 
                                         <a data-nim="${item.Nim}" class="btn btn-primary btn-sm send_email text-white mt-1">
                                         <i class="feather icon-mail"></i>
                                        </a>    
                                         </td>     
                                     </tr>`)
      console.log(" <td>" + item.nombre + "</td> ")
    });


    nueva_tabla();

  
  }).fail(err => {

  })
});
$(document).on('click', '#estudios_ml', function () {
  alert("estudios_ml");
 });
 $(document).on('click', '#imagenologia', function () {
  alert("imagenologia");
 });



function daicom(tomaid, id_estudio) {
  var bandera=$("#bandera").val();
  if(bandera!=2){
    url = "./dai/" + tomaid + "/" + id_estudio;
    window.location.href = url;
 
  }
  else{
    url = "./dai2/" + tomaid + "/" + id_estudio;
    window.location.href = url;
  }
 
}

function showEstudios(tomaId, curp) {
  $.ajax({
    type: "get",
    url: "./getEstudiosPacienteToma/" + tomaId,
    dataType: "json",
    beforeSend: () => {
      $('.loadEstudios').show();
    }
  }).done(res => {

    $('.loadEstudios').hide();
    $('.contEstudios').empty();
    toma = res.toma;
    // pdf_laboratorio(toma.folio);
    estudios = res.estudios;
    estudios.forEach((item, i) => {
     
      // if (item.categoria_id == 2) {
      //   e_clase = '<a onclick="daicom(' + toma.id + "," + item.id + ')" class="btn-sm btn-secondary btn text-white"><i class="feather icon-airplay"></i></a>';
      // }
      // else {
      //   e_clase = '<a type="button" target="_blank" href="./Resultados/' + toma.id + '/' + item.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-external-link"></i></a>';
      // }
      if (item.nombre == 'ELECTROCARDIOGRAMA CON INTERPRETACIÓN') {
        e_clase = '<div class="btn-group" role="group" aria-label="Basic example"><a type="button" target="_blank" href="./trazo/' + curp + '/' + toma.id + '" class="btn-outline-secondary btn-sm btn-secondary btn"><i class="feather icon-activity"></i></a></div>';
      }
      else if (item.nombre == 'ESPIROMETRIA') {

        e_clase = '<a type="button" target="_blank" href="./Resultado_Espirometria/' + curp + '/' + toma.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-cast"></i></a>';
      }
      else if (item.nombre == 'AUDIOMETRIA TONAL Y AEREA') {
        e_clase = '<a type="button" target="_blank" href="./resultadoAudiometria' + '/' + toma.id + '/' + curp + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-headphones"></i></a>';
      }
      else{
        e_clase = `<a type="button" target="_blank" href="./resultado_fisioterapia/${curp}/${toma.id}" class="btn-sm btn-secondary btn" name="button">
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="16" height="16" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><path d="M95.92308,6.61538c-9.12199,0 -16.53846,7.41647 -16.53846,16.53846c0,9.12199 7.41647,16.53846 16.53846,16.53846c9.12199,0 16.53846,-7.41647 16.53846,-16.53846c0,-9.12199 -7.41647,-16.53846 -16.53846,-16.53846zM86,46.30769c-7.28726,0 -13.23077,5.94351 -13.23077,13.23077v52.92308h13.23077l8.68269,-9.30288l-8.68269,-5.99519c-2.79087,-2.22235 -3.07512,-5.73678 -2.6875,-9.50962c1.16286,1.9381 2.97176,3.61779 4.75481,4.96154l19.84615,11.99038c1.31791,0.67188 1.98978,0.62019 3.30769,0.62019c1.98978,0 4.47055,-1.31791 5.78846,-3.30769c1.98978,-2.63581 0.82692,-6.69291 -2.48077,-8.68269l-19.84615,-11.78365l-5.99519,-22.32692l10.54327,21.5l11.16346,5.375h8.68269v-17.77885l15.91827,17.77885h17.15865l-29.14904,-35.14423c-2.63581,-2.63581 -5.94351,-4.54808 -9.92308,-4.54808zM120.31731,92.61538c2.63582,3.30769 4.13462,9.25121 0.82692,13.23077c-1.98978,2.63582 -5.375,4.54808 -8.68269,4.54808c-3.97956,0 -13.23077,-4.54808 -13.23077,-4.54808l-12.19712,13.23077h-40.72596c-7.28726,0 -13.23077,5.94351 -13.23077,13.23077c0,7.28726 5.94351,13.23077 13.23077,13.23077h46.30769c3.97957,0 6.61538,-2.63581 6.61538,-6.61538c0,-3.97956 -2.63581,-6.61538 -6.61538,-6.61538l-39.69231,-3.92788l39.69231,0.62019c7.28726,0 13.23077,2.63582 13.23077,9.92308c0,2.63582 -0.7494,4.6256 -2.06731,6.61538h59.53846c4.6256,0 8.68269,-4.05709 8.68269,-8.68269c0,-4.6256 -3.23017,-7.80408 -7.85577,-8.47596l-55.40385,-3.10096l10.95673,-14.88462l29.14904,-1.86058c3.97957,0 7.85577,-4.08293 7.85577,-8.0625c0,-3.97956 -3.85036,-7.85577 -8.47596,-7.85577zM16.53846,105.84615c-9.12199,0 -16.53846,7.41647 -16.53846,16.53846c0,9.12199 7.41647,16.53846 16.53846,16.53846c9.12199,0 16.53846,-7.41647 16.53846,-16.53846c0,-9.12199 -7.41647,-16.53846 -16.53846,-16.53846zM6.61538,145.53846c-3.97956,0 -6.61538,3.30769 -6.61538,6.61538v13.23077h172v-13.23077h-138.92308l-4.54808,-4.54808c-1.31791,-1.31791 -2.76503,-2.06731 -4.75481,-2.06731z"></path></g></g></svg>
        </a>`;
      }
      if(item.nombre != 'VALORACIÓN MÉDICA'){
        $('.contEstudios').append(`
            <div class="col-md-4 mb-1">
                <div class="text-center shadow bg-white p-1  rounded">
                    <h5 class='secondary'>${item.nombre}</h5>
                  
                    <hr>
                    <small class="d-block">${toma.created_at}</small>
                    <small class="d-block">${item.seccion == null ? '' : item.seccion}</small>             
                  
                    <hr>
                    ${e_clase}

                </div>
            </div>
        `)
      }

    });
    
  }).fail(err => {
    $('.loadEstudios').hide();
    console.log(err);
    swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
  })
}

function pdf_laboratorio(nim){
  data = new Object();
  id_toma = 1469580;
  usuario="1003561";
  password="YVN2UO";
  cliente = "ael";
  json_datos=`json={"id_toma":${id_toma},"usuario":"${usuario}","password":"${password}","cliente":"ael"}`;

  url=`https://ael.sass.com.mx/ws/ws_resultados.php?${json_datos}`;
  console.log(url);
  var encontrado= '';
  datosem_la=[];
  $.ajax({
    type: 'get',
    data: data,
    dataType: 'json',
    url: url,
    beforeSend: () => {
      
    }
  }).done(res => {
    console.log(res);
    pdf=res.PDF;
      $('.contEstudios').append(`
      <div class="col-md-4 mb-1">
          <div class="text-center shadow bg-white p-1  rounded">
              <h5 class='secondary'>LABORATORIO</h5>
              <hr>
              <button onclick="mandapdf('${pdf}')" type="button" class="btn btn-sm btn-secondary waves-effect waves-float waves-light"><i class="feather icon-external-link"></i></button>


          </div>
      </div>
  `) 
  
  }).fail(err => {
    console.log("No se ejecuto");
  });
  
  return encontrado;
 
}
function mandapdf(pdf){

  $.ajax({
    type: 'post',
    data: {pdf:pdf},
    url: './labo_pdf',
    beforeSend: () => {
    }
  }).done(res => {
    // console.log(res);
   
    let newWindow = open("./resultado_laboratorio", "_blank", "width=500,height=500");
    newWindow.focus();
    newWindow.onload = function() {
      let html = `${res}`;
      newWindow.document.body.insertAdjacentHTML("afterbegin", html);
    };
    
  }).fail(err => {
    console.log("No Enviado");
  });

}
function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}

$(document).on('click', '.buscar_fecha', function () {
  fecha_inicial = $("input[name=fecha_inicial]").val();
  fecha_final = $("input[name=fecha_final]").val();
  buscarestudio(fecha_inicial, fecha_final);

});
function buscarestudio(fecha_inicial, fecha_final) {
  $('#tabla_dinamica').empty();
  $.ajax({
    type: "get",
    url: "./Resultados1/" + fecha_inicial + "/" + fecha_final,
    dataType: "json",
    beforeSend: () => {
      $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
          <span class="sr-only">Loading...</span>
          </div>`);
    }
  }).done(res => {
    $(".cargar").empty();
    $('#tabla_dinamica').append('<table class="table_estudios1 table" style="width:100%"><thead><tr>' +
      '<th>Empleado</th><th>Toma</th><th>Fecha Inicial</th> <th>Fecha Final</th>  <th>Acciones</th></tr></thead>' +
      ' <tbody></tbody></table>');

    res.resultados.forEach((item, i) => {

      fecha = item.fecha_inicial + "T00:00:00";

      fechaf = item.fecha_final + "T00:00:00";
      if (item.folio == null) {
        folio_sass = ' ';
      }
      else {
        folio_sass = item.folio;
      }

      const monthNames = ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
        "Jul", "Ago", "Sep", "Oct", "Nov", "Dec"];

      const f = new Date(fecha);
      const fm = monthNames[f.getMonth()];
      fd = f.getDate();
      const fy = f.getFullYear();
      const ff = new Date(fechaf);
      const fmf = monthNames[ff.getMonth()];
      const fdf = ff.getDate();


      const fyf = ff.getFullYear();


      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido_paterno} ${item.apellido_materno}</td>
                                         <td>${folio_sass}</td>
                                         <td>${fm} / ${fd} / ${fy}</td>
                                         <td>${fmf} / ${fdf} / ${fyf}</td>
                                         <td><a data-id="${item.id}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         </td>     
                                     </tr>`)
      console.log(" <td>" + item.nombre + "</td> ")
    });


    nueva_tabla();


  }).fail(err => {

    console.log(err);
    console.log("./Resultados1/" + fecha_inicial + "/" + fecha_final);

  })
}

function nueva_tabla() {
  $(".table_estudios1").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }

  });

  function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

}
$(document).on('click', '.send_email', function () {

  $('#correo').modal('show');
    var nim_correo = document.getElementById("nim_correo");
    nim_correo.value = $(this).data('nim');
    var correo_envia = document.getElementById("email");
    correo_envia.value = '';

 
});

$('#enviar_resultado').on('submit', function (e) {
  e.preventDefault();
  var formData = new FormData();
  formData.append('email',$("#email").val());


  formData.append('nim',$("#nim_correo").val());
  $.ajax({
      url:'/correo_estudio',
      type:'post',
      data:formData,
      dataType:'json',
      contentType:false,
      processData:false,
      beforeSend:()=>{
      }
  }).done(res=>{
    console.log(res);
      swal({
          position: 'top-end',
          icon: 'success',
          text: 'Los datos fueron enviados de manera correcta',
          title: 'Datos Enviados correctamente',
          showConfirmButton: true
      }).then(isConfirm => {
          location.reload();
      });         
  }).fail(err=>{
      swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
  });
});