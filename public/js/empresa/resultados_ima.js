
$(".table_estudios").DataTable({
  responsive: !1,
  language: {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "",
    "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "_MENU_",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
      "first": "Primero",
      "last": "Ultimo",
      "next": "Siguiente",
      "previous": "Anterior"
    }
  }

});


$( document ).ready(function() {
  $("#tabla_dinamica").empty(); 
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  let codigo= $(`input[name=c_sass_empresa]`).val();
  let empresa= $(`input[name=n_sass_empresa]`).val();
  data.metodo = 'empresa_ima';
  data.codigo = codigo;
  data.empresa = empresa;
  data.fecha1='2022-01-01';
  data.fecha2='2022-12-30';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
  
    $(".cargar").empty();
    $('#tabla_dinamica').append(`
    <div class='row'>            
        <div class='col-4'>
            <label for="start">Fecha Inicial:</label>
            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <label for="start">Fecha Final:</label>
            <input type="date" id="fecha_final" name="fecha_final" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <a onclick="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%"><thead><tr>
      <th>Empleado</th><th>Toma</th><th>Acciones</th></tr></thead>
      <tbody></tbody></table>`);

    res.pacientes.forEach((item, i) => {
      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td><a data-id="${item.Nim}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         <a data-nim="${item.Nim}" class="btn btn-primary btn-sm send_email text-white mt-1">
                                          <i class="feather icon-mail"></i>
                                         </a> 
                                         </td>     
                                     </tr>`)
    
    });


    nueva_tabla();

  
  }).fail(err => {

  })
});

$(document).on('click', '.buscar_fecha', function () {
  fecha_inicial = $("input[name=fecha_inicial]").val();
  fecha_final = $("input[name=fecha_final]").val();
  estudio_laboratorio(fecha_inicial, fecha_final);

});
function estudio_laboratorio(fecha_inicial, fecha_final){
  $("#tabla_dinamica").empty(); 
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  let codigo= $(`input[name=c_sass_empresa]`).val();
  let empresa= $(`input[name=n_sass_empresa]`).val();
  data.metodo = 'empresa_ima';
  data.codigo = codigo;
  data.empresa = empresa;
  data.fecha1=fecha_inicial;
  data.fecha2=fecha_final;
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
      $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
    }
  }).done(res => {
  
    $(".cargar").empty();
    $('#tabla_dinamica').append(`
    <div class='row'>            
        <div class='col-4'>
            <label for="start">Fecha Inicial:</label>
            <input type="date" id="fecha_inicial" name="fecha_inicial" value="${fecha_inicial}" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <label for="start">Fecha Final:</label>
            <input type="date" id="fecha_final" name="fecha_final" value="${fecha_final}" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <a onclick="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%"><thead><tr>
      <th>Empleado</th><th>Toma</th><th>Acciones</th></tr></thead>
      <tbody></tbody></table>`);

    res.pacientes.forEach((item, i) => {
      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td><a data-id="${item.Nim}" data-id_paciente="${item.id_paciente}" data-id_toma="${item.id_toma}" data-contra="${item.Contrasena}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>   
                                         <a data-id="${item.Nim}" data-id_paciente="${item.id_paciente}" data-id_toma="${item.id_toma}" data-contra="${item.Contrasena}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a> 
                                         </td>     
                                     </tr>`)
    
    });


    nueva_tabla();

  
  }).fail(err => {

  })
}

function nueva_tabla() {
  $(".table_estudios1").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }

  });


  function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

}
$(document).on('click', '.showEstudio', function () {
  $('#estudiosShow').modal('show');
  
  $("#estudios").empty();
  pdf_laboratorio($(this).data('id'));
});

function pdf_laboratorio(Idtoma){
  $(".contImagenologia").empty();
 console.log(Idtoma)
  $('.contEstudios').empty();

  url="https://humanly-sw.com/imagenologia/Api/V1_regresainterpretacion";
  console.log(url);
  nim=Idtoma;
	nim = nim.split('/');				
  nim1 = nim[0];			
  nim2 = nim[1];
	if(nim2.length!=4){
      if(nim2.length==1){
        n="000"+nim2;
      }
      else if(nim2.length==2){
        n="00"+nim2;
      }
      else if(nim2.length==3){
        n="0"+nim2;
      }			
	}
  Idtoma = nim1+"/"+n;
    console.log(Idtoma);
  $.ajax({
    type: 'post',
    data: {
      sass: Idtoma,
    },
    dataType: 'json',
    url: url,
    beforeSend: () => {
      $(".loadEstudios").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
    }
  }).done(res => {
    console.log(res);
    $(".loadEstudios").empty();
    res.forEach((item, i) => {
    $('.contEstudios').append(`
      <div class="col-md-4 mb-1">
          <div class="text-center shadow bg-white p-1  rounded">
              <h5 class='secondary'>${item.nombre}</h5>
            
              <hr>            
              <a onclick="daicom('${Idtoma}','${item.id_estudio}')" class="btn-sm btn-secondary btn text-white"><i class="feather icon-airplay"></i></a>
              <hr>
          

          </div>
      </div>
    `);
  });
  
  }).fail(err => {
    console.log("No se ejecuto");
  });
 
}
function daicom(nim,codigo) {
    $('.contEstudios').empty();
    showDaicoms(nim,codigo);
}
function mandapdf(pdf){

  $.ajax({
    type: 'post',
    data: {pdf:pdf},
    url: './labo_pdf',
    beforeSend: () => {
    }
  }).done(res => {
   
   
    
  }).fail(err => {
    console.log("No Enviado");
  });
}
function showDaicoms(tomaId, codigo) {
  let Idtoma = tomaId.replace("-", "/");
  console.log(Idtoma);
  $.ajax({
      type: "POST",
      url: "https://humanly-sw.com/imagenologia/Api/V1_regresainterpretacion",
      dataType: "json",
      data: {
          sass: Idtoma,
          //sass: "101210401/0023"
         
      },
      beforeSend: () => { },
  })
      .done((res) => {
          das = JSON.stringify(res);
          if (das == "[]" || tomaId == null) {
              $(".contImagenologia").append(`
                  <div class='col-md-12'>
                      <h5 class="mb-0">
                          <div class="alert alert-primary alert_32">
                              <p class="text-center">
                                  !No existen tomas¡
                              </p>
                           </div>
      
                       </h5>
                  </div>
              `);
          }
          else {
              $(".form-check").hide();
              let i = 0;
              datos = JSON.stringify(res);
              arr1 = datos;
              j = 0;
              for (let item of res) {
                  arr = item.dicoms;
                  if (item.id_estudio == codigo) {
                      if (arr.length == 0) {
                          $("#estudios").append(` 
                              <div class="form-check form-check-inline">
                                   <div class="custom-control custom-radio">
                                       <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" value="" onclick='mostrar("${codigo}",this.value)'>
                                      <label class="custom-control-label secondary" for="customRadio1">Toma 1</label>
                                   </div> 
                              </div>                                 
                          `);
                      } else {
                          
                          while (j <= arr.length - 1) {
                              var ruta1 =
                                  "https://imagenologia.dynpbx.mx/Imagenologia2/" +
                                  arr[j];

                              var ruta = ruta1.replace("\\", "/");
                              
                              $("#estudios").append(` 
                                  <div class="form-check form-check-inline">
                                      <div class="custom-control custom-radio">
                                          <input type="radio" id="customRadio${j}" name="customRadio" class="custom-control-input" value="${ruta}" onclick='mostrar("${codigo}",this.value)'>
                                          <label class="custom-control-label secondary" for="customRadio${j}">Toma ${j}</label>
                                      </div> 
                                  </div>                                 
                              `);

                              j++;
                          }
                      }
                  }
                  i++;
              }
          }
      })
      .fail((err) => {
          $(".loadImagenologia").hide();
          console.log("Error");
          console.log(err);
      });
}

function mostrar(codigo, valor) {
  let sindaicom;
  let interurl;
  if (valor == null || valor == "") {
      sindaicom =
          "<div class='col-md-12'><h5 class=mb-0><div class='alert alert-primary alert_32'><p class='text-center'�0�3No existen tomas!</p></div></h5></div>";
  } else {
      sindaicom = "";
  }
  console.log("Example:" + sindaicom);
  k = 0;
  var d = JSON.parse(datos);
  
  var inter;
  var encointer;
  var clase;
  var d = JSON.parse(datos);
  console.log(d);
  for (let item of d) {
      interurl=item.interpretacion;
      if (item.id_estudio == codigo) {
          inter = item.interpretacion;
          interpreta = inter.length;
          if (interpreta < 200) {
              clase =
                  '<embed src="http://humanly-sw.com/imagenologia/uploads/interpretacionpdf/' +
                  atob(item.interpretacion) +
                  '" type="application/pdf" width="100%;" height="702px">';
              console.log(clase);
          } else {
              encointer = atob(item.interpretacion);
              clase = decode_utf8(encointer);
              
              clase="<img src='https://has-humanly.com/pagina_has/images/cahitoredondo.svg' class='membretada'>"+ clase+"<img src='https://has-humanly.com/pagina_has/images/logo_30.svg' class='imgleft'>"+
              '<a href="https://humanly-sw.com/imagenologia/Pdf/handle_create_pdf/'+item.id_pdf+'" class="btn task-cat primario ml-2 mt-1 ">Descargar <img src="https://humanly-sw.com/imagenologiadev/uploads/assets/cloud_download.svg" alt="icono de nube"></a>';      

          }
          console.log(inter.length);
          $(".contImagenologia").empty();
          
          $(".contImagenologia").append(`
          <div class='col-md-12'>
               <div class="card">
                       <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                              
                          </h5>
                       </div>

               <div id="collapseOne1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class='row'>
                    <div class='col-md-6'>
                      <div class="card-body">
                       
                       <div class="row" id="dicomview">                        
                          <div class="col-md-12">
                          ${sindaicom}
                              <div style="width:100%;height:700px;position:relative;color: white;display:inline-block;border-style: dashed; border-width: 1px; border-color:gray;"
                                  oncontextmenu="return false" class='disable-selection noIbar' unselectable='on' onselectstart='return false;'
                                      onmousedown='return false;'>
                                      <div id="dicomImage" style="width:100% ;height:700px;top:0px;left:0px; position:absolute"></div>
                                     
                              </div>
                              <button id="btnDescargar" class="btn btn-primary btn-sm">Descargar Imagen</button>
                          </div>
                         
                      </div>
                   
                  </div>
               </div>
              
                   <div class='col-md-6'>
                      <div class="card-content">              
                      ${clase}   
                                 
                      </div>  
                                   
                   </div>
                   
               </div>
              
            </div> `);
            

      }
      k++;
  }
  downloadAndView("dicomImage", valor);
  function decode_utf8(s) {
      return decodeURIComponent(escape(s));
  }
  const canvas = document.querySelector("canvas");
  $btnDescargar = document.querySelector("#btnDescargar");
  // var dataURL = canvas.toDataURL();
  // console.log(dataURL);
  // Listener del botón
  $btnDescargar.addEventListener("click", () => {
        // Crear un elemento <a>
        let enlace = document.createElement('a');
        // El título
        enlace.download = 'Radiografia.png';
        // Convertir la imagen a Base64 y ponerlo en el enlace
        enlace.href = canvas.toDataURL();
        // Hacer click en él
        enlace.click();
  });
}

$(document).on('click', '.send_email', function () {

  $('#correo').modal('show');
    var nim_correo = document.getElementById("nim_correo");
    nim_correo.value = $(this).data('nim');
    var correo_envia = document.getElementById("email");
    correo_envia.value = '';

 
});

$('#enviar_resultado').on('submit', function (e) {
  e.preventDefault();
  var formData = new FormData();
  formData.append('email',$("#email").val());


  formData.append('nim',$("#nim_correo").val());
  $.ajax({
      url:'/correo_estudio',
      type:'post',
      data:formData,
      dataType:'json',
      contentType:false,
      processData:false,
      beforeSend:()=>{
      }
  }).done(res=>{
      swal({
          position: 'top-end',
          icon: 'success',
          text: 'Los datos fueron enviados de manera correcta',
          title: 'Datos Enviados correctamente',
          showConfirmButton: true
      }).then(isConfirm => {
          location.reload();
      });         
  }).fail(err=>{
      swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
  });
});


