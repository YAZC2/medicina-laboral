$(".table_estudios").DataTable({
  responsive: !1,
  language: {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "",
    "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "_MENU_",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
      "first": "Primero",
      "last": "Ultimo",
      "next": "Siguiente",
      "previous": "Anterior"
    }
  }

});

$( document ).ready(function() {
  $("#tabla_dinamica").empty(); 
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  let codigo= $(`input[name=c_sass_empresa]`).val();
  let empresa= $(`input[name=n_sass_empresa]`).val();
  data.metodo = 'empresa_la';
  data.codigo = codigo;
  data.empresa = empresa;
  data.fecha1='2021-11-01';
  data.fecha2='2021-11-30';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
  
    $(".cargar").empty();
    $('#tabla_dinamica').append(`
    <div class='row'>            
        <div class='col-4'>
            <label for="start">Fecha Inicial:</label>
            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <label for="start">Fecha Final:</label>
            <input type="date" id="fecha_final" name="fecha_final" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <a onclick="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%"><thead><tr>
      <th>Empleado</th><th>Toma</th><th>Acciones</th></tr></thead>
      <tbody></tbody></table>`);

    res.pacientes.forEach((item, i) => {
      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td><a data-id="${item.Nim}" data-id_paciente="${item.id_paciente}" data-id_toma="${item.id_toma}" data-contra="${item.Contrasena}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         <a data-nim="${item.Nim}" class="btn btn-primary btn-sm send_email text-white mt-1">
                                         <i class="feather icon-mail"></i>
                                        </a> 
                                         </td>     
                                     </tr>`)
    
    });


    nueva_tabla();

  
  }).fail(err => {

  })
});

$(document).on('click', '.buscar_fecha', function () {
  fecha_inicial = $("input[name=fecha_inicial]").val();
  fecha_final = $("input[name=fecha_final]").val();
  estudio_laboratorio(fecha_inicial, fecha_final);

});
function estudio_laboratorio(fecha_inicial, fecha_final){
  $("#tabla_dinamica").empty(); 
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  let codigo= $(`input[name=c_sass_empresa]`).val();
  let empresa= $(`input[name=n_sass_empresa]`).val();
  data.metodo = 'empresa_la';
  data.codigo = codigo;
  data.empresa = empresa;
  data.fecha1=fecha_inicial;
  data.fecha2=fecha_final;
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
      $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
    }
  }).done(res => {
  
    $(".cargar").empty();
    $('#tabla_dinamica').append(`
    <div class='row'>            
        <div class='col-4'>
            <label for="start">Fecha Inicial:</label>
            <input type="date" id="fecha_inicial" name="fecha_inicial" value="${fecha_inicial}" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <label for="start">Fecha Final:</label>
            <input type="date" id="fecha_final" name="fecha_final" value="${fecha_final}" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <a onclick="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%"><thead><tr>
      <th>Empleado</th><th>Toma</th><th>Acciones</th></tr></thead>
      <tbody></tbody></table>`);

    res.pacientes.forEach((item, i) => {
      $('tbody').append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td><a data-id="${item.Nim}" data-id_paciente="${item.id_paciente}" data-id_toma="${item.id_toma}" data-contra="${item.Contrasena}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>    
                                         </td>     
                                     </tr>`)
    
    });


    nueva_tabla();

  
  }).fail(err => {

  })
}

function nueva_tabla() {
  $(".table_estudios1").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay informaci��n",
      "info": "",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }

  });


  function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

}
$(document).on('click', '.showEstudio', function () {
  $('#estudiosShow').modal('show');
  pdf_laboratorio($(this).data('id_toma'), $(this).data('id_paciente'), $(this).data('contra'));
});

function pdf_laboratorio(idtoma,paciente,contrasena){
  $('.contEstudios').empty();
  data = new Object();
  console.log(idtoma);
  console.log(paciente);
  console.log(contrasena);
  id_toma=idtoma;
  usuario=paciente;
  password=contrasena;
  console.log(contrasena);
  cliente = "ael";
  json_datos=`json={"id_toma":"${id_toma}","usuario":"${usuario}","password":"${password}","cliente":"ael"}`;
  
  url=`https://ael.sass.com.mx/ws/ws_resultados.php?${json_datos}`;
  console.log(url);
  var encontrado= '';
  datosem_la=[];
  $.ajax({
    type: 'get',
    data: data,
    dataType: 'json',
    url: url,
    beforeSend: () => {
      $(".loadEstudios").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
    }
  }).done(res => {
   
    if(res.estatus.error==false){
      
      pdf=res.PDF;
      $(".loadEstudios").empty();
      $('.contEstudios').append(`<div class="col-md-12"><embed
      src="data:application/pdf;base64,${pdf}"
      type="application/pdf" width="100%" height="500px"/></div>`);
 
    }
    else{
      $(".loadEstudios").empty();
      $('.contEstudios').append(`
      <div class="col-md-12">
      <div class="alert alert-warning" role="alert">
      <div class="alert-body">
       ${res.estatus.msg_error}
      </div>
    </div>
      </div>`);
      
      
    }
  
   
    
     
  
  }).fail(err => {
    console.log("No se ejecuto");
  });
  
  return encontrado;
 
}
function mandapdf(pdf){

  $.ajax({
    type: 'post',
    data: {pdf:pdf},
    url: './labo_pdf',
    beforeSend: () => {
    }
  }).done(res => {
    // console.log(res);
   
    let newWindow = open("./resultado_laboratorio", "_blank", "width=500,height=500");
    newWindow.focus();
    newWindow.onload = function() {
      let html = `${res}`;
      newWindow.document.body.insertAdjacentHTML("afterbegin", html);
    };
    
  }).fail(err => {
    console.log("No Enviado");
  });

}
$(document).on('click', '.send_email', function () {

  $('#correo').modal('show');
    var nim_correo = document.getElementById("nim_correo");
    nim_correo.value = $(this).data('nim');
    var correo_envia = document.getElementById("email");
    correo_envia.value = '';

 
});

$('#enviar_resultado').on('submit', function (e) {
  e.preventDefault();
  var formData = new FormData();
  formData.append('email',$("#email").val());


  formData.append('nim',$("#nim_correo").val());
  $.ajax({
      url:'/empresa_dev/correo_estudio',
      type:'post',
      data:formData,
      dataType:'json',
      contentType:false,
      processData:false,
      beforeSend:()=>{
      }
  }).done(res=>{
    console.log(res);
      swal({
          position: 'top-end',
          icon: 'success',
          text: 'Los datos fueron enviados de manera correcta',
          title: 'Datos Enviados correctamente',
          showConfirmButton: true
      }).then(isConfirm => {
          location.reload();
      });         
  }).fail(err=>{
      swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
  });
});