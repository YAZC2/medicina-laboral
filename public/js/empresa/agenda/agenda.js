var validacion_insert = $.trim($("#validacion_insert").data('validacion'));
var buttonCustom = '';
if(validacion_insert == "true")
{
    buttonCustom = {
        addNew: {
            text:"Nueva",
            click: function() {
                $(".modal-calendar").modal("show")
            }
        }
    }
}

$('#spinner').hide();
document.addEventListener("DOMContentLoaded", function() {
        a = "",
        t = "",
        o = "",
        d = document.getElementById("fc-default"),
        c = new FullCalendar.Calendar(d, {
            eventLimit: true, // for all non-TimeGrid views
            views: {
              timeGrid: {
                eventLimit: 6 // adjust to 6 only for timeGridWeek/timeGridDay
              }
            },
            initialView: 'timeGridDay',
          //  defaultView:'timeGridWeek',
            locale: 'es',
            height: 550,
            buttonText: {
              today:    'Hoy',
              month:    'Més',
              week:     'Semana',
              day:      'Día'
            },
            plugins: ["dayGrid", "timeGrid", "interaction"],
            customButtons: buttonCustom,
            header: {
                initialView: 'timeGridDay',
                left: "addNew",
                center: "dayGridMonth,timeGridWeek,timeGridDay",
                right: "prev,title,next",
                default:'timeGridWeek'
            },
            displayEventTime: !1,
            navLinks: !0,
            editable: !0,
            allDay: !0,
            navLinkDayClick: function(a) {
              //  $(".modal-calendar").modal("show")
            },
            dateClick: function(a) {
                crearNuevaCita(a);
            },
            eventClick: function(a) {
              showScheduler(a);
            }
        });
    c.render(),
    $("#basic-examples .fc-right").append(a),
    $(".modal-calendar .cal-submit-event").on("click", function() {
        $(".modal-calendar").modal("hide")
    }),$("#appointment").on("submit", function(e) {
        e.preventDefault();
        paciente = $('#patient').val();
        if (paciente == 'no') {
          swal('','Falta seleccionar al paciente','info');
        }else {
          cita = appointment();
        }
    }), $(".pickadate").pickadate({
        format: "yyyy-mm-dd",
        monthsFull:["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        weekdays: ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        weekdaysShort: ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
         today: 'Hoy',
         clear: 'Limpiar',
         close:'Cerrar',
    })
});

function crearNuevaCita(a) {
    if(validacion_insert == "false")
    {
        return false;
    }
    console.log(a.date.getTime());
    $('#getValueDateInitial').val(a.date);

    hour = a.date.getHours()
    min = a.date.getMinutes()
    day = a.date.getDate()
    month = a.date.getMonth() + 1
    year = a.date.getFullYear()

    hourInit = (hour < 10 ? "0" : "") + hour;
    minInit = (min < 10 ? "0" : "") + min;
    day = (day < 10 ? "0" : "") + day;
    month = (month < 10 ? "0" : "") + month;
    // 30 minutos despues de setear info inicial
    a.date.setMinutes(a.date.getMinutes() + 30);
    $('#getValueDateFinally').val(a.date);
    hour = a.date.getHours()
    min = a.date.getMinutes()

    hourEnd = (hour < 10 ? "0" : "") + hour;
    minEnd = (min < 10 ? "0" : "") + min;
    $(".modal-calendar #cal-fecha-date").val(year+'-'+month+'-'+day),
    $(".modal-calendar #cal-start-date").val(hourInit+':'+minInit),
    $(".modal-calendar #cal-end-date").val(hourEnd+':'+minEnd),
    $(".modal-calendar").modal("show");
}

function addCita(res,color = ''){

  patient = `${res.empleado.nombre} ${res.empleado.apellido_paterno} ${res.empleado.apellido_materno}`;
  c.addEvent({
      id: res.agenda.id,
      statu:res.agenda.statu,
      title:patient+', Motivo:'+res.agenda.reason,
      patient:patient,
      birthday:res.empleado.fecha_nacimiento,
      curp:res.empleado.CURP,
      email:res.empleado.email,
      clave:res.empleado.clave,
      start: `${res.agenda.date}T${res.agenda.start_time}`,
      end: `${res.agenda.date}T${res.agenda.end_time}`,
      description: res.agenda.reason,
      color:color,
      dataEventColor: o,
  })
}

function appointment(){
  $.ajax({
    type:'post',
    dataType:'json',
    //async:false,
    data:$('#appointment').serialize(),
    url:'newSchedule',
    beforeSend:()=>{
      $('#spinner').show();
      $('.cal-add-event').hide();
    }
  }).done(res=>{

    $('#spinner').hide();
    $('.cal-add-event').show();
    addCita(res);
    emailSend(res);
    notification();
    $('#patient').select2("val", "");
    $("#patient").val('').change();
    $('#appointment')[0].reset();
    $(".modal-calendar").modal("hide");
  }).fail(err=>{
    $('#spinner').hide();
    swal("Error", "Intentalo más tarde", "error");
  })
}

// NOTE: Seleccion de los empleados
$('.select2').select2();


function emailSend(res){
  if (res.agenda.patient_remember == 1) {
    data = new Object();
    data.date = res.agenda.date;
    data.startTime = res.agenda.start_time;
    data.endTime = res.agenda.end_time;
    data.reason = res.agenda.reason;
    data.empleado = `${res.empleado.nombre} ${res.empleado.apellido_paterno}`
    data.email = res.empleado.email;
    $.ajax({
      type:'post',
      dataType:'json',
    //  async:false,
      data:data,
      url:'https://humanly.javierviteramirez.com/Agenda/public/hasScheduler',
    }).done(res=>{
    }).fail(err=>{

    })
  }
}


$(document).ready(function() {
  $.ajax({
    type:'get',
    dataType:'json',
    //async:false,
    url:'getSchedule'
  }).done(res=>{
    res.forEach((schedule, i) => {
      color = schedule.agenda.statu == 1 ? '#002b46' : '#f15a24';
      addCita(schedule,color);
    });
    $('#patient').val('');
    $('#appointment')[0].reset();
  }).fail(err=>{
    swal("Error", "Intentalo más tarde", "error");
  })

});


function showScheduler(a) {
    data = a.event.extendedProps;
    if (data.statu == 2) {
      detailSchedule(a);
      return
    }

    $('.title_modal').text(data.patient)
    $('.nacimiento').text(data.birthday)
    $('.clave').text(data.clave)
    $('.curp').text(data.curp)
    $('.email').text(data.email)
    $('.date').val(moment(a.event.start).format("YYYY-MM-DD"))
    $(".startTime").val(moment(a.event.start).format("hh:mm")),
    $(".endTime").val(moment(a.event.end).format("hh:mm")),
    $(".consultation").val(a.event.extendedProps.description);
    $('.date_id').val(a.event.id)
    $(".modal-schedule").modal("show")
}

$('#Uappointment').on('submit', function(e){
  e.preventDefault();
  swal({
    title: "Atención",
    text: "Seguro que deseas actualizar la cita",
    icon: "warning",
    buttons: {
      cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
      },
      confirm: {
          text: "Confirmar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
      }
    }
  })
  .then((isConfirm) => {
    if (isConfirm) {
      Uappointment();
    } else {
        swal("Cancelado", "la operación fue cancelada", "error");
    }
  });
});

function Uappointment(){
  $.ajax({
    type:'put',
    dataType:'json',
    //async:false,
    data:$('#Uappointment').serialize(),
    url:'updateSchedule'
  }).done(res=>{
    c.getEventById(res.agenda.id).remove();
    notification();
    addCita(res);
    $('.modal-schedule').modal('hide');
    swal("Reagendado", "la cita se reagendo de manera exitosa", "success");
  }).fail(err=>{
    swal("Error", "Intentalo más tarde", "error");
  })
}

$('.deleteSchedule').on('click', (arguments) => {
  swal({
    title: "Atención",
    text: "Seguro que deseas eliminar la cita",
    icon: "warning",
    buttons: {
      cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
      },
      confirm: {
          text: "Confirmar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
      }
    }
  })
  .then((isConfirm) => {
      if (isConfirm) {
        Dappointment();
      } else {
          swal("Cancelado", "la operación fue cancelada", "error");
      }
  });
})

function Dappointment(){
  id = $('.date_id').val();
  $.ajax({
    type:'get',
    dataType:'json',
    url:'deleteSchedule/'+id
  }).done(res=>{
    c.getEventById(res.id).remove()
    $('.modal-schedule').modal('hide');
    swal("Eliminado", "la cita se ha eliminado de manera exitosa", "success");
    notification();
  }).fail(err=>{
    swal("Error", "Intentalo más tarde", "error");
  })
}


$('.formConsulta').on('click', function(e){
  e.preventDefault();
  swal({
    title: "Atención",
    text: "Seguro que desea iniciar la consulta",
    icon: "warning",
    buttons: {
      cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
      },
      confirm: {
          text: "Confirmar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
      }
    }
  })
  .then((isConfirm) => {
    if (isConfirm) {
      agendaConsulta($(this).serialize());
    } else {
        swal("Cancelado", "la operación fue cancelada", "error");
    }
  });

});

function agendaConsulta(data) {
  console.log(data);
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'consultasAgenda'
  }).done(res=>{
      console.log(res);
    location.href="consultas/"+res.encrypt;

  }).fail(err=>{
    swal("Error", "Intentalo más tarde", "error");
  })
}


function detailSchedule(a){


  data = a.event.extendedProps;

  $('.d_patient').html(
    `${data.patient}
    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
    `);
  $('.d-nacimiento').text(data.birthday)
  $('.d-clave').text(data.clave)
  $('.d-curp').text(data.curp)
  $('.d-email').text(data.email)
  $('.d-fecha').text(moment(a.event.start).format("YYYY-MM-DD"))
  $(".d-inicial").text(moment(a.event.start).format("hh:mm")),
  $(".d-final").text(moment(a.event.end).format("hh:mm")),
  $(".d-motivo").text(a.event.extendedProps.description);

  //$(".modal-schedule").modal("show")

  $('.detailSchedule').modal('show');
}
