$(document).ready(function () {
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: 'consultasStats/2022'
    }).done(res => {
        data = arrayData(res)
        chart(data);
    })
    nopatologicos();
    enfermedades();
    // audio();
    // espirometrias();
    // electrocardiogra();
    estudios();
});
function tabla(){
    Highcharts.chart('container', {
        series: [{
          type: "treemap",
          layoutAlgorithm: 'stripes',
          alternateStartingDirection: true,
          levels: [{
            level: 1,
            layoutAlgorithm: 'sliceAndDice',
            dataLabels: {
              enabled: true,
              align: 'left',
              verticalAlign: 'top',
              style: {
                fontSize: '15px',
                fontWeight: 'bold'
              }
            }
          }],
          data: [{
            id: 'A',
            name: 'Apples',
            color: "#EC2500"
          }, {
            id: 'B',
            name: 'Bananas',
            color: "#ECE100"
          }, {
            id: 'O',
            name: 'Oranges',
            color: '#EC9800'
          }, {
            name: 'Anne',
            parent: 'A',
            value: 5
          }, {
            name: 'Rick',
            parent: 'A',
            value: 3
          }, {
            name: 'Peter',
            parent: 'A',
            value: 4
          }, {
            name: 'Anne',
            parent: 'B',
            value: 4
          }, {
            name: 'Rick',
            parent: 'B',
            value: 10
          }, {
            name: 'Peter',
            parent: 'B',
            value: 1
          }, {
            name: 'Anne',
            parent: 'O',
            value: 1
          }, {
            name: 'Rick',
            parent: 'O',
            value: 3
          }, {
            name: 'Peter',
            parent: 'O',
            value: 3
          }, {
            name: 'Susanne',
            parent: 'Kiwi',
            value: 2,
            color: '#9EDE00'
          }]
        }],
        title: {
          text: 'Fruit consumption'
        }
      });
}
function arrayData(res) {
    var stats = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    res.forEach((item, i) => {
        month = item.month - 1;
        stats.splice(month, 1, item.stats);
    });
    return stats;
}

function chart(data) {
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Consultas Por Mes'
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"]
        },
        yAxis: {
            title: {
                text: 'Cantidad'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: "2022",
            data: data
        }]
    });

}
// function datos(estudio,tipo_resultado) {   
    
//     $.ajax({
//     type: "get",
//     url: "./personas/"+estudio/tipo_resultado,
//     dataType: "json",
//     beforeSend: () => {
//         $("#tabla"+estudio).empty();  
//         $("#"+estudio).addClass('animate__animated animate__backOutRight');  
        
                
//         }
//     }).done(res => {
//         $("#"+id).removeClass('animate__animated animate__backOutRight'); 
//         $("#"+id).hide();           
//         $('#tabla'+id).show(); 
          
//         console.log(res);     
//         // $('#tabla'+id).append(`

//         // <table class="animate__animated animate__lightSpeedInLeft datatables-ajax table no-footer" id='tablaes${id}'style="width:100%">
//         //         <thead>
//         //             <tr>
//         //                 <th>Nombre</th>
//         //                 <th>Apellido Paterno</th>
//         //                 <th>Apellido Materno</th>
//         //                 <th>Curp</th>
//         //                 <th>Ver</th>
//         //             </tr>
//         //         </thead>
//         //         <tbody id='${id}'>
//         //         </tbody>
               
//         //     </table>
//         //     <button class='btn btn-primary btn-sm waves-effect waves-light' id='atr' onclick="atras('${id}')"><img src="http://img.icons8.com/material-outlined/16/ffffff/back--v1.png"/></button>`);
//         //    res.datos.forEach((item, i) => {
//         //     var idbody="tablaes"+id+' tbody';                  
//         //     $("#"+idbody).append(`
//         //                             <tr>
//         //                             <td>${item.nombre}</td>
//         //                              <td>${item.apellido_paterno}</td>    
//         //                              <td>${item.apellido_materno}</td> 
//         //                              <td>${item.CURP}</td>                   
//         //                              <td>
//         //                              <a href="./empleados/${item.CURP}" class="action-view actions_button">
//         //                                 <i class="feather icon-eye"></i>
//         //                              </a>    
//         //                              </td>     
//         //                             </tr>
//         //                         `);
//         //         });  
//         //         nueva(id);  
            
            
            
//     }).fail(err => {
//        console.log(err);
       
        
//    })
// }
// function nueva(id){
//     $("#tablaes"+id).DataTable({
//      responsive: !1,
//     language: {
//     "decimal": "",
//     "emptyTable": "No hay informaci贸n",
//      "info": "",
//      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
//      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
//     "infoPostFix": "",
//      "thousands": ",",
//     "lengthMenu": "_MENU_",
//     "loadingRecords": "Cargando...",
//     "processing": "Procesando...",
//     "search": "Buscar",
//     "zeroRecords": "Sin resultados encontrados",
//     "paginate": {
//     "first": "Primero",
//     "last": "Ultimo",
//      "next": "Siguiente",
//      "previous": "Anterior"
//   }
// }


$(document).ready(function () {
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: 'DiagnosticosIndicador'
    }).done(res => {
        let nombre = [];
        let count = []
        res.forEach((item) => {
            nombre.push(item.nombre);
            count.push(item.stats)
        })
        diagnosticos(nombre, count)
    })
})

function diagnosticos(nombres, count) {
    $('#bar-chart').empty();
    var e = "#002b46"
    var t = [e, "#28C76F", "#EA5455", "#FF9F43", "#00cfe8"];
    var a = !1;
    var i = {
        chart: {
            height: 350,
            type: "bar"
        },
        colors: t,
        plotOptions: {
            bar: {
                horizontal: !0
            }
        },
        dataLabels: {
            enabled: !1
        },
        series: [{
            data: count
        }],
        xaxis: {
            categories: nombres,
            tickAmount: 5
        },
        yaxis: {
            opposite: a
        }
    };
    new ApexCharts(document.querySelector("#bar-chart"), i).render();
}

$('#diagnosticos').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: "post",
        url: "DiagnosticosDate",
        data: $('#diagnosticos').serialize(),
        dataType: "json",
    }).done(res => {
        let nombre = [];
        let count = []
        res.forEach((item) => {
            nombre.push(item.nombre);
            count.push(item.stats)
        })
        diagnosticos(nombre, count)
    }).fail(err => {
        console.log(err);
    });

})
$('#estudios_rea').on('submit', function (e) {
    e.preventDefault();
    nombre = '';
    apellido = '';
    edad = '';
    fechaalta = '';
    estudiosempleado = [];
    folio = '';
    data = new Object();
    let fecha_inicial = $(`input[name=finicial]`).val();
    let fecha_final = $(`input[name=ffinal]`).val();
    let empresa = $(`input[name=nombre_sass]`).val();
    let codigo = $(`input[name=codigo_sass]`).val();
    let estudio = $(`#estudio`).val();
    data.metodo = 'empresa_la';
    data.codigo = codigo;
    data.empresa = empresa;
    data.fecha1 = fecha_inicial;
    data.fecha2 = fecha_final;
    $.ajax({
        type: 'post',
        data: data,
        dataType: 'json',
        url: 'https://asesores.ac-labs.com.mx/sass/index.php',
        beforeSend: () => {

        }
    }).done(res => {
        res.pacientes.forEach((item, i) => {
            obtenresultado(item.Nim,estudio);
        });
        



    }).fail(err => {

    })


})
function obtenresultado(nim,examen){
    
    data = new Object();
    data.metodo = 'estudios';
    nim = nim.split('/');				
    toma = nim[0];			
    consecutivo = nim[1];
    data.toma = toma;
    data.consecutivo = consecutivo;
    data.examen=examen;
    $.ajax({
        type: 'post',
        data: data,
        dataType: 'json',
        url: 'https://asesores.ac-labs.com.mx/sass/index.php',
        beforeSend: () => {

        }
    }).done(res => {
        console.log(res);
        // res.pacientes.forEach((item, i) => {
        //     console.log(item.Nim);
        //     console.log(estudio);
        // });



    }).fail(err => {

    })

}
