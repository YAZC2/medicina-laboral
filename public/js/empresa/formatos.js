$(".table_estudios").DataTable({
  responsive: !1,
  language: {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "",
    "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "_MENU_",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
      "first": "Primero",
      "last": "Ultimo",
      "next": "Siguiente",
      "previous": "Anterior"
    }
  }
  
});
// function cambiar_estatus(id,status){
//   data=new Object();
//   data.id=id;
//   if(status=='0')
//   {
//     status_n='1';
//   }
//   else{
//     status_n='0';
//   }
//   data.status=status_n;
//   $.ajax({
//     type: 'post',
//     data: data,
//     url: 'actualiza_estatus',
//     beforeSend: () => {
//     }
//   }).done(res => {
//    console.log(res);
//     // $("#Estatus").empty();
//     // $('#Estatus').append(`<span class="badge rounded-pill bg-success verde">Des</span>`);

    

  
//   }).fail(err => {

//   })

// }

function cambiar_estatus(id) {
  let status= $(`input[name=Estatus_Ac_${id}]`).val();
  
  if(status=='1'){
    status_n='0';
  }
  else{
    status_n='1';
  }
  data= new Object();
  data.id=id;
  data.status_n=status_n;
  $.ajax({
    type: 'post',
    dataType: 'json',
    url: 'actualiza_estatus',
    data: data,
  }).done(function (response) {
    // alert("'"+'Estatus_Ac_'+id+"'");
    console.log(response.status);
    document.getElementById(`Estatus_Ac_${id}`).value=response.status;
    
   
      if(response.status=='1'){
        $('#Estatus_'+id).empty();
      $('#Estatus_'+id).append(`<span class="badge rounded-pill bg-success verde">Habilitado</span>`);
      }
      else{
        $('#Estatus_'+id).empty();
        $('#Estatus_'+id).append(`<span class="badge rounded-pill bg-danger">Deshabilitado</span>`);
      }
   
  }).fail(function (error) {
  


  });

}

