"use strict";
$(".data-list-view").DataTable({
    responsive: !1,
    dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    },

    order: [
        [1, "asc"]
    ],

    buttons: [
  ],
    initComplete: function(t, e) {
        $(".dt-buttons .btn").removeClass("btn-secondary")
    }
}), 0 < $(".data-items").length && new PerfectScrollbar(".data-items", {
    wheelPropagation: !1
}), $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
    $(".add-new-data").removeClass("show"), $(".overlay-bg").removeClass("show"), $("#data-name, #data-price").val(""), $("#data-category, #data-status").prop("selectedIndex", 0)
})


$('#microbiologia').on('submit', (e) => {
  e.preventDefault();
  $.ajax({
    type:'post',
    dataType:'json',
    data:$('#microbiologia').serialize(),
    url:'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend:()=>{
      $('.loading').show()
    }
  }).done(res=>{
    var dataSet = [];
    res.forEach((item, i) => {
      dataSet.push([
        item.nombre_completo,
        item.seccion,
        item.examen,
        item.metodo,
        item.origen_nombre,
        item.createAt,
        `<a onclick="resultados('${item.id_toma}')" class="action-view actions_button"><i class="feather icon-eye"></i></a>`
      ]);
    });

    $('.microbiologiaTable').dataTable().fnDestroy();
    dataTable(dataSet);
    $('.loading').hide()
  }).fail(err=>{
    $('.loading').hide()
    alert('Hubo un error intentalo más tarde');
    console.log(err);
  })
})


function dataTable(dataSet) {
  $(".data-list-view").DataTable({
     data: dataSet,
      responsive: !1,
      dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "_MENU_",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      },

      order: [
          [1, "asc"]
      ],

      buttons: [
    ],
      initComplete: function(t, e) {
          $(".dt-buttons .btn").removeClass("btn-secondary")
      }
  })
}


function resultados(id_toma) {
    $('.loadSass').show();
    $('.result_examen').empty();
    $('#modal').modal('show');

    $.ajax({
      type:'post',
      dataType:'json',
      data:{id_toma:id_toma,metodo:'microbiologiaToma'},
      url:'https://asesores.ac-labs.com.mx/sass/index.php',
    }).done(res=>{
      res.forEach((item, i) => {
        $('.result_examen').append(`
          <tr>
            <th scope="row">${item.examen}</th>
            <td>${item.desc_resultado}</td>
            <td>${item.resultado_capturado}</td>
            <td>${item.unidad}</td>
          </tr>
        `)

      });
        $('.loadSass').hide();
    }).fail(err=>{
      console.log(err);
    })
}
