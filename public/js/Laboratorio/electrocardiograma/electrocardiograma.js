// $("#formelectro").submit(function(e){
//     e.preventDefault();
//     let formulario = $(this).serialize();
//     var formData = new FormData();
//     var input_file = document.getElementById("archivo");
//     console.log(input_file.files[0]);
   
//     alert(formulario);
//     $.ajax({
//         type: "POST",
//         url: "../save_electro",
//         data: formData,
//         dataType: "json",
//     }).done(res=>{
//         swal({
//             position: 'top-end',
//             icon: 'success',
//             title: "Guardado",
//             text:"Electrocardiograma guardada correctamente",
//             showConfirmButton: true
//           }).then(isConfirm => {
            
//             window.location.reload();
//           });
        
//     }).fail(err=>{
//         console.log(err);
//         swal("Error","Ocurrio un error en el servidor, intentelo más tarde.","error");
//     })
// });

$("#formelectro").submit(function(e){
    e.preventDefault();
    var formData = new FormData();
    var input_file = document.getElementById("archivo");
    var interpretacionpdf = document.getElementById("interpretacionpdf");
    console.log(interpretacionpdf.files[0]);
    formData.append('archivo',input_file.files[0]);
    formData.append('archivo1',input_file.files[1]);
    formData.append('interpretacionpdf',interpretacionpdf.files[0]);
    formData.append('curp',$("#curp").val());
    formData.append('nim',$("#nim").val());
    formData.append('tipo_resultado',$('input:radio[name=tipo_resultado]:checked').val());
    formData.append('id_paciente',$("#id_paciente").val());
    formData.append('frecuencia',$("#frecuencia").val());
    formData.append('eje_p',$("#eje_p").val());
    formData.append('ejeqrs',$("#ejeqrs").val());
    formData.append('ejeqrs',$("#ejeqrs").val());
    formData.append('ejet',$("#ejet").val());     
    formData.append('rr',$("#rr").val());  
    formData.append('pr',$("#pr").val());  
    formData.append('pr',$("#pr").val());  
    formData.append('qrs',$("#qrs").val()); 
    formData.append('qtm',$("#qtm").val()); 
    formData.append('qtc',$("#qtc").val()); 
    formData.append('observaciones',$("#observaciones").val()); 
    formData.append('conclusiones',$("#conclusiones").val()); 
    formData.append('notas',$("#notas").val());   
    $.ajax({
        url:'../save_electro',
        type:'post',
        data:formData,
        dataType:'json',
        contentType:false,
        processData:false,
        beforeSend:()=>{
        }
    }).done(res=>{
        swal({
            position: 'top-end',
            icon: 'success',
            title: "Guardado",
            text:"Electrocardiograma guardada correctamente",
            showConfirmButton: true
          }).then(isConfirm => {
            
            window.location.reload();
          });         
    }).fail(err=>{
        swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
    });
});


