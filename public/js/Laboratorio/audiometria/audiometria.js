$(function() {
    $('#button1').addClass('d-none');
    $('#button2').addClass('d-none');
    $('#boton').hide();
});

// function guardar(idExp) {
// 	console.log('Guarda la firma');
// 	//Imagen cnv
// 	var firma = document.getElementById("cnv");
// 	var imagenFirm = firma.toDataURL("image/png");
// 	var stringFirm = document.FORM1.bioSigData.value;
// 	var stringFirm64 = document.FORM1.sigImageData.value;

// 	// Envío la petición XHR al servidor con los datos de la imagen
// 	$.ajax({
// 		url: '/TalentoHumano/registroEmpleados/updateFirma',
// 		method: 'post',
// 		dataType: 'json',
// 		data: {
// 			idExp,
// 			stringFirm64
// 		},
// 		beforeSend: function() {
// 			$('#myModal').modal('hide');
// 			$('#loadModal').modal('show');
// 		},
// 		success: function(data) {
// 			$('#loadModal').modal('hide');
// 			swal({
// 					title: data.titulo,
// 					text: data.texto,
// 					icon: data.alerta,
// 					buttons: {
// 						confirm: {
// 							text: "OK",
// 							value: true,
// 							visible: true,
// 							className: "",
// 							closeModal: false
// 						}
// 					}
// 				})
// 				.then((isConfirm) => {
// 					location.reload();
// 				});
// 		},
// 		error: (err) => {
// 			console.log(err);
// 			swal("Error", "No se completo el guardado de los datos", "error");
// 			$('#loadModal').modal('hide');
// 		}
// 	});
// }

var tmr;

var resetIsSupported = false;
var SigWeb_1_6_4_0_IsInstalled = false; //SigWeb 1.6.4.0 and above add the Reset() and GetSigWebVersion functions
var SigWeb_1_7_0_0_IsInstalled = false; //SigWeb 1.7.0.0 and above add the GetDaysUntilCertificateExpires() function

window.onload = function() {

    var sigWebVer = "";
    try {
        sigWebVer = GetDaysUntilCertificateExpires();
    } catch (err) {
        console.log("Unable to get SigWeb Version: " + err.message)
    }

    if (sigWebVer != "") {
        try {
            SigWeb_1_7_0_0_IsInstalled = isSigWeb_1_7_0_0_Installed(sigWebVer);
        } catch (err) {
            console.log(err.message)
        };
        //if SigWeb 1.7.0.0 is installed, then enable corresponding functionality
        if (SigWeb_1_7_0_0_IsInstalled) {

            resetIsSupported = true;
            try {
                var daysUntilCertExpires = GetDaysUntilCertificateExpires();
                document.getElementById("daysUntilExpElement").innerHTML = "SigWeb Certificate expires in " + daysUntilCertExpires + " days.";
            } catch (err) {
                console.log(err.message)
            };
            var note = document.getElementById("sigWebVrsnNote");
            note.innerHTML = "SigWeb 1.7.0 installed";
        } else {
            try {
                SigWeb_1_6_4_0_IsInstalled = isSigWeb_1_6_4_0_Installed(sigWebVer);
                //if SigWeb 1.6.4.0 is installed, then enable corresponding functionality
            } catch (err) {
                console.log(err.message)
            };
            if (SigWeb_1_6_4_0_IsInstalled) {
                resetIsSupported = true;
                var sigweb_link = document.createElement("a");
                sigweb_link.href = "https://www.topazsystems.com/software/sigweb.exe";
                sigweb_link.innerHTML = "https://www.topazsystems.com/software/sigweb.exe";

                var note = document.getElementById("sigWebVrsnNote");
                note.innerHTML = "SigWeb 1.6.4 is installed. Install the newer version of SigWeb from the following link: ";
                note.appendChild(sigweb_link);
            } else {
                var sigweb_link = document.createElement("a");
                sigweb_link.href = "https://www.topazsystems.com/software/sigweb.exe";
                sigweb_link.innerHTML = "https://www.topazsystems.com/software/sigweb.exe";

                var note = document.getElementById("sigWebVrsnNote");
                note.innerHTML = "A newer version of SigWeb is available. Please uninstall the currently installed version of SigWeb and then install the new version of SigWeb from the following link: ";
                note.appendChild(sigweb_link);
            }
        }
    } else {
        //Older version of SigWeb installed that does not support retrieving the version of SigWeb (Version 1.6.0.2 and older)
        var sigweb_link = document.createElement("a");
        sigweb_link.href = "https://www.topazsystems.com/software/sigweb.exe";
        sigweb_link.innerHTML = "https://www.topazsystems.com/software/sigweb.exe";

        var note = document.getElementById("sigWebVrsnNote");
        note.innerHTML = "A newer version of SigWeb is available. Please uninstall the currently installed version of SigWeb and then install the new version of SigWeb from the following link: ";
        note.appendChild(sigweb_link);
    }

}

function isSigWeb_1_6_4_0_Installed(sigWebVer) {
    var minSigWebVersionResetSupport = "1.6.4.0";

    if (isOlderSigWebVersionInstalled(minSigWebVersionResetSupport, sigWebVer)) {
        console.log("SigWeb version 1.6.4.0 or higher not installed.");
        return false;
    }
    return true;
}

function isSigWeb_1_7_0_0_Installed(sigWebVer) {
    var minSigWebVersionGetDaysUntilCertificateExpiresSupport = "1.7.0.0";

    if (isOlderSigWebVersionInstalled(minSigWebVersionGetDaysUntilCertificateExpiresSupport, sigWebVer)) {
        console.log("SigWeb version 1.7.0.0 or higher not installed.");
        return false;
    }
    return true;
}

function isOlderSigWebVersionInstalled(cmprVer, sigWebVer) {
    return isOlderVersion(cmprVer, sigWebVer);
}

function isOlderVersion(oldVer, newVer) {
    const oldParts = oldVer.split('.')
    const newParts = newVer.split('.')
    for (var i = 0; i < newParts.length; i++) {
        const a = parseInt(newParts[i]) || 0
        const b = parseInt(oldParts[i]) || 0
        if (a < b) return true
        if (a > b) return false
    }
    return false;
}

function onSign() {
    if (IsSigWebInstalled()) {
        var ctx = document.getElementById('cnv').getContext('2d');
        SetDisplayXSize(500);
        SetDisplayYSize(100);
        SetTabletState(0, tmr);
        SetJustifyMode(0);
        ClearTablet();
        if (tmr == null) {
            tmr = SetTabletState(1, ctx, 50);
        } else {
            SetTabletState(0, tmr);
            tmr = null;
            tmr = SetTabletState(1, ctx, 50);
        }

        $('#button1').removeClass('d-none');
        $('#button2').removeClass('d-none');
        $('#button3').hide();
    } else {
        alert("Unable to communicate with SigWeb. Please confirm that SigWeb is installed and running on this PC.");
    }
}

function onClear() {
    ClearTablet();
}

function onDone() {
    if (NumberOfTabletPoints() == 0) {
        alert("Por favor firme antes de continuar");
    } else {
        $('#boton').show();
        $('#button1').hide();
        $('#button2').hide();
        $('#mensajitoirma').text('Firma agregada correctamente');

        SetTabletState(0, tmr);
        //RETURN TOPAZ-FORMAT SIGSTRING
        SetSigCompressionMode(1);
        document.FORM1.bioSigData.value = GetSigString();
        //document.FORM1.sigStringData.value = GetSigString();
        //this returns the signature in Topaz's own format, with biometric information

        //RETURN BMP BYTE ARRAY CONVERTED TO BASE64 STRING
        SetImageXSize(500);
        SetImageYSize(100);
        SetImagePenWidth(5);
        GetSigImageB64(SigImageCallback);

    }
}

function SigImageCallback(str) {
    document.FORM1.sigImageData.value = str;
}

function endDemo() {
    ClearTablet();
    SetTabletState(0, tmr);
}

function close() {
    if (resetIsSupported) {
        Reset();
    } else {
        endDemo();
    }
}

//Perform the following actions on
//	1. Browser Closure
//	2. Tab Closure
//	3. Tab Refresh
window.onbeforeunload = function(evt) {
    close();
    clearInterval(tmr);
    evt.preventDefault(); //For Firefox, needed for browser closure
};
// $(".audiometria").steps({
//     headerTag: "h6",
//     bodyTag: "fieldset",
//     transitionEffect: "fade",
//     enableAllSteps: true,
//     titleTemplate: '<span class="step">#index#</span> #title#',
//     labels: {
//       finish: 'Guardar',
//       previous: 'Anterior',
//       next: 'Siguiente'
//     },
//     onFinished: function (event, currentIndex) {
//       swal({
//         title: "Confirmación de envio",
//         text: "¿Seguro que desea terminar el historial clinico?",
//         icon: "warning",
//         buttons: {
//           cancel: {
//             text: "Cancelar",
//             value: null,
//             visible: true,
//             className: "",
//             closeModal: false,
//           },
//           confirm: {
//             text: "Guardar",
//             value: true,
//             visible: true,
//             className: "",
//             closeModal: false
//           }
//         }
//       }).then(isConfirm => {
//         if (isConfirm) {
//           $('#formaudiometria').submit();
//         } else {
//           swal("Operacion Cancelada", "Puede continuar...", "error");
//         }
//       });
  
//     }
  
//   });
//   $("#formaudiometria").on('submit', (e) => {
//     e.preventDefault();
//     varaible = $("#formaudiometria").serialize();
//     console.log(varaible);
//     $.ajax({
//       type: 'POST',
//       dataType: 'json',
//       data: varaible,
//       url: '../historialForm'
//     }).done((res) => {
//       console.log(res);
//       $('#wizard').find('a[href="#finish"]').remove();
//       swal({
//         position: 'top-end',
//         icon: 'success',
//         title: varaible,
//         showConfirmButton: true
//       }).then(isConfirm => {
//         // window.location.href = '../Pacientes-Medico/'+res.empresa_id;
//         window.location.reload();
//       });
//     }).fail((err) => {
//       if (err.responseText.indexOf('estado_salud') > -1) {
//         tpl = ` <div class="alert alert-danger" id="AlertaResultado" >
//                     Faltan campos por llenar
//                 </div>`;
//         $('#AlertaResultado').html(tpl);
//         $(".alert").delay(4000).slideUp(300, function () {
//           $(this).alert('close');
//         });
//         $('#estado_salud').css({
//           'color': 'blank',
//           'background': '#ff000060'
//         });
//       }
  
//       if (err.status == 0) {
//         swal("Se ha registrado correctamente");
//       } else {
//         swal("Algo salió mal");
//       }
//     });
   
//   });
function estudio(){
    
    if($('input:radio[name=tipo_resultado]:checked').val()=='ANORMAL'){
        $("#mostrarcausa").removeClass('d-none');
    }
    else{
        $("#mostrarcausa").addClass('d-none');
    } 

}
$(document).ready(function() {
    estudio();
    $('.molestia_oido').select2({
        placeholder: "Molestia en algun oido"
    });
    $('.oido_derecho_sel').select2({
        placeholder: "Interpretación"
    });
    $('.oido_izquierdo_sel').select2({
        placeholder: "Interpretación"
    });

    $("#explosion_cercana").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_explosion_oido").show();
            $("#explosion_oido").attr('required',true);
        }else{
            $("#div_explosion_oido").hide();
            $("#explosion_oido").attr('required',false);
        }
    });

    $("#audifonos_musica").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_audifonos_musica").show();
            $("#audifonos_musica_volumen").attr('required',true);
            $("#audifonos_musica_no_dias").attr('required',true);
            $("#audifonos_musica_anios").attr('required',true);
        }else{
            $("#div_audifonos_musica").hide();
            $("#audifonos_musica_volumen").attr('required',false);
            $("#audifonos_musica_no_dias").attr('required',false);
            $("#audifonos_musica_anios").attr('required',false);
        }
    });

    $("#acude_eventos").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_acude_eventos").show();
            $("#acude_eventos_frecuencia").attr('required',true);
        }else{
            $("#div_acude_eventos").hide();
            $("#acude_eventos_frecuencia").attr('required',false);
        }
    });

    $("#practica_actividades").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_practica_actividades").show();
            $("#practica_actividades_frecuencia").attr('required',true);
        }else{
            $("#div_practica_actividades").hide();
            $("#practica_actividades_frecuencia").attr('required',false);
        }
    });

    $("#medicamentos_frecuentes").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_medicamentos_frecuentes").show();
            $("#medicamentos_frecuentes_frecuencia").attr('required',true);
        }else{
            $("#div_medicamentos_frecuentes").hide();
            $("#medicamentos_frecuentes_frecuencia").attr('required',false);
        }
    });
  

    $("#form_carta_consentimiento").submit(function(e){
        e.preventDefault();
        let formulario = $(this).serialize();
     console.log(formulario);
        $.ajax({
            type: "POST",
            url: "../save_audiometria",
            data: formulario,
            dataType: "json",
        }).done(res=>{
            swal({
                position: 'top-end',
                icon: 'success',
                title: "Guardado",
                text:"Audiometría guardada correctamente",
                showConfirmButton: true
              }).then(isConfirm => {
                
                window.location.reload();
              });
            
        }).fail(err=>{
            console.log(err);
            swal("Error","Ocurrio un error en el servidor, intentelo más tarde.","error");
        })
    });

    // $("#form_carta_consentimiento").submit(function(e){
    //     e.preventDefault();
    //     var formData = new FormData();
    //     var input_file = document.getElementById("archivo");
    //     formData.append('archivo',input_file.files[0]);
    //     formData.append('curp',$("#curp").val());
    //     formData.append('registro_id',$("#registro_id").val());   
    //     formData.append('puesto_trabajo',$("#puesto_trabajo").val());
    //     formData.append('proteccion_auditiva',$("#proteccion_auditiva").val());
    //     formData.append('tce',$("#tce").val());
    //     formData.append('molestia_oido',$("#molestia_oido").val());
    //     formData.append('tiempo_trabajando_exposion_ruido',$("#tiempo_trabajando_exposion_ruido").val());
    //     formData.append('tiempo_puesto',$("#tiempo_puesto").val());
    //     formData.append('explosion_cercana',$("#explosion_cercana").val());
    //     formData.append('explosion_oido',$("#explosion_oido").val());
    //     formData.append('audifonos_musica',$("#audifonos_musica").val());
    //     formData.append('audifonos_musica_volumen',$("#audifonos_musica_volumen").val());
    //     formData.append('audifonos_musica_no_dias',$("#audifonos_musica_no_dias").val());
    //     formData.append('audifonos_musica_anios',$("#audifonos_musica_anios").val());
    //     formData.append('gripa_infancia',$("#gripa_infancia").val());
    //     formData.append('gripa_actualidad',$("#gripa_actualidad").val());
    //     formData.append('infeccion_oidos',$("#infeccion_oidos").val());
    //     formData.append('diabetes',$("#diabetes").val());
    //     formData.append('hipertension',$("#hipertension").val());
    //     formData.append('paralisis_facial',$("#paralisis_facial").val());
    //     formData.append('acude_eventos',$("#acude_eventos").val());
    //     formData.append('acude_eventos_frecuencia',$("#acude_eventos_frecuencia").val());
    //     formData.append('varicela',$("#varicela").val());
    //     formData.append('practica_actividades',$("#practica_actividades").val());
    //     formData.append('practica_actividades_frecuencia',$("#practica_actividades_frecuencia").val());
    //     formData.append('medicamentos_frecuentes',$("#medicamentos_frecuentes").val());
    //     formData.append('medicamentos_frecuentes_cuales',$("#medicamentos_frecuentes_cuales").val());
    //     formData.append('familiar_problemas_audicion',$("#familiar_problemas_audicion").val());
	// 	$.ajax({
    //         url:'../save_audiometria',
    //         type:'post',
    //         data:formData,
    //         dataType:'json',
    //         contentType:false,
    //         processData:false,
    //         beforeSend:()=>{
    //         }
	// 	}).done(res=>{
    //         swal("Exito","La información se ha actualizado correctamente.","success"); 
    //         location.reload();          
	// 	}).fail(err=>{
    //         swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
	// 	});
    // })

    $(".input_decibeles").TouchSpin({
        min: -10,
        max: 120,
        step: 5,
        boostat: 5,
        maxboostedstep: 10,
    });

    $(".input_decibeles").change(function(){
        if($(this).val() == ""){
            $(this).val(0);
        }
    });
    

    var array_oido_izquierdo = [0,0,0,0,0,0,0,0,0];
    var array_oido_derecho = [0,0,0,0,0,0,0,0,0];
    var array_oido_izquierdo_v = [0,0,0,0,0,0,0,0];
    var array_oido_derecho_v = [0,0,0,0,0,0,0,0];
    var audiometria_id = $("#audiometria_id").val();

    $.ajax({
        type: "GET",
        url: "../getResultadosAudiometria/"+audiometria_id+"/izquierdo",
        dataType: "json",
    }).done(res=>{
        array_oido_izquierdo = [];
        array_oido_izquierdo_v = [];
        res.forEach(element => {
            array_oido_izquierdo.push(parseInt(element.desibelios));
            $("#oido_izquierdo_"+element.frecuencia).val(element.desibelios);
            array_oido_izquierdo_v.push(parseInt(element.desibelios_v));
            $("#Grafica_VO #oido_izquierdo_"+element.frecuencia_v).val(element.desibelios_v);
          
        });
        initGrafica();
        initGrafica2();
    }).fail(err=>{
        console.log(err);
    });

    $.ajax({
        type: "GET",
        url: "../getResultadosAudiometria/"+audiometria_id+"/derecho",
        dataType: "json",
    }).done(res=>{
        array_oido_derecho = [];
        array_oido_derecho_v = [];
        res.forEach(element => {
            array_oido_derecho.push(parseInt(element.desibelios));
            $("#oido_derecho_"+element.frecuencia).val(element.desibelios)
            array_oido_derecho_v.push(parseInt(element.desibelios_v));
            $("#Grafica_VO #oido_derecho_"+element.frecuencia_v).val(element.desibelios_v)
        });
        initGrafica();
        initGrafica2();
    }).fail(err=>{
        console.log(err);
    });

    function initGrafica() {
        let series = [
            {
                name: "Oído izquierdo",
                data:array_oido_izquierdo,
            },
            {
                name: "Oído derecho",
                data:array_oido_derecho
            }
        ];
        chart.updateSeries(series,true);
    }
    function initGrafica2() {
        let series = [
            {
                name: "Oído izquierdo VO",
                data:array_oido_izquierdo_v,
            },
            {
                name: "Oído derecho VO",
                data:array_oido_derecho_v
            }
        ];
        chart2.updateSeries(series,true);
    }

    var options = {
        series: [
            {
                name: "Oído izquierdo",
                data:array_oido_izquierdo,
            },
            {
                name: "Oído derecho",
                data:array_oido_derecho
            }
        ],
        chart: {
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            }
        },
        colors:["#001eff", "#fc0000"],
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight',
            colors:["#001eff","#fc0000"],
            width: 3,
        },
        legend:{
            markers: {
                width: 12,
                height: 12,
                strokeWidth: 1,
                strokeColor:["#001eff","#fc0000"] ,
                fillColor: ["#001eff","#fc0000"],
                radius: 12,
                offsetX: 0,
                offsetY: 0
            },
        },
        title: {
            text: $("#tituloGeneral").text(),
            align: 'left'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 1
            },
            column: {
                colors: ['transparent','#f3f3f3'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        xaxis: {
            categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
            position: "top",
            offsetX: 0,
            offsetY: -0,
            labels:{
                show:true,
                hideOverlappingLabels:true,
                // offsetY:-15,
            }
        },
        yaxis:{
            reversed: true,
            tickAmount:10,
            forceNiceScale:true,
            min: -10,
            max: 120,
        }
    };
    var options1 = {
        series: [
            
            {
                name: "Oído izquierdo V",
                data:array_oido_izquierdo_v,
            },
            {
                name: "Oído derecho V",
                data:array_oido_derecho_v
            }
        ],
        chart: {
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            }
        },
        colors:['#CB3234','#3B83BD'],
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight',
            colors:['#CB3234','#3B83BD'],
            width: 3,
        },
        legend:{
            markers: {
                width: 12,
                height: 12,
                strokeWidth: 1,
                strokeColor:['#CB3234','#3B83BD'] ,
                fillColor: ['#CB3234','#3B83BD'],
                radius: 12,
                offsetX: 0,
                offsetY: 0
            },
        },
        title: {
            text: $("#tituloGeneral").text(),
            align: 'left'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 1
            },
            column: {
                colors: ['transparent','#f3f3f3'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        xaxis: {
            categories: ['125','250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
            position: "top",
            offsetX: 0,
            offsetY: -0,
            labels:{
                show:true,
                hideOverlappingLabels:true,
                // offsetY:-15,
            }
        },
        yaxis:{
            reversed: true,
            tickAmount:10,
            forceNiceScale:true,
            min: -10,
            max: 120,
        }
    };

    var chart = new ApexCharts(document.querySelector("#line-chart2"), options);
    var chart2 = new ApexCharts(document.querySelector("#line-chartVO"), options1);
    chart.render();
    chart2.render();

    $("#gra .input_decibeles").change(function(){
        
        let oido = $(this).data('oido');
        let hz = $(this).data('hz');
        let valor = $(this).val();
       
        

        data = {
            "oido": oido,
            "hz":hz,
            "valor":valor,
            "audiometria_id": audiometria_id
        }
        console.log(data);
        $.ajax({
            type: "POST",
            url: "../guardarResultado",
            data: data,
            dataType: "json",
        }).done(res=>{
            console.log(res);
        }).fail(err=>{
            console.log(err);
        });


     if(oido == "i")
        {
            updateArray(array_oido_izquierdo,hz,valor);
        }
        else {
            updateArray(array_oido_derecho,hz,valor);
        }


        
        
        let series = [
            {
                name: "Oído izquierdo",
                data:array_oido_izquierdo,
            },
            {
                name: "Oído derecho",
                data:array_oido_derecho
            }
        ];
        chart.updateSeries(series,true);
    });
    $("#Grafica_VO .input_decibeles").change(function(){
      
        let oido = $(this).data('oido');
        let hz = $(this).data('hz');
        let valor = $(this).val();
       
        

        data = {
            "oido_v": oido,
            "hz_v":hz,
            "valor":valor,
            "audiometria_id": audiometria_id
        }
        console.log(data);
        $.ajax({
            type: "POST",
            url: "../guardarResultado",
            data: data,
            dataType: "json",
        }).done(res=>{
            console.log(res);
        }).fail(err=>{
            console.log(err);
        });


     if(oido == "i")
        {
            updateArray(array_oido_izquierdo_v,hz,valor);
        }
        else {
            updateArray(array_oido_derecho_v,hz,valor);
        }


        
        
        let series = [
            {
                name: "Oído izquierdo VO",
                data:array_oido_izquierdo_v,
            },
            {
                name: "Oído derecho VO",
                data:array_oido_derecho_v
            }
        ];
        chart2.updateSeries(series,true);
    });
    
   
    // $("#decibel2").click(function(){

        
    //     let oido_v = $(this).data('oido_v');
    //     let hz_v = $(this).data('hz_v');
        

    //     data = {
    //         "valor":valor,
    //         "oido_v": oido_v,
    //         "hz_v":hz_v,
    //         "audiometria_id": audiometria_id
    //     }
    //     console.log(data);
    //     $.ajax({
    //         type: "POST",
    //         url: "../guardarResultado",
    //         data: data,
    //         dataType: "json",
    //     }).done(res=>{
    //         console.log(res);
    //     }).fail(err=>{
    //         console.log(err);
    //     });
    //     if(oido_v!='undefined'){
    //         if(oido_v == "i")
    //         {
    //             updateArray(array_oido_izquierdo_v,hz_v,valor);
    //         }
    //         else {
    //             updateArray(array_oido_derecho_v,hz_v,valor);
    //         }
    //     }
        
        
    //     let series = [
    //         {
    //             name: "Oído izquierdo VO",
    //             data:array_oido_izquierdo_v,
    //         },
    //         {
    //             name: "Oído derecho VO",
    //             data:array_oido_derecho_v
    //         }
    //     ];
    //     chart.updateSeries(series,true);
    // });
    
    function updateArray(array_elementos,elemento,valor) {
        switch (elemento) {
            case 125:
                array_elementos[0] = valor;
                break;
            case 250:
                array_elementos[1] = valor;
                break;
            case 500:
                array_elementos[2] = valor;
                break;
            case 1000:
                array_elementos[3] = valor;
                break;
            case 2000:
                array_elementos[4] = valor;
                break;
            case 3000:
                array_elementos[5] = valor;
                break;
            case 4000:
                array_elementos[6] = valor;
                break;
            case 6000:
                array_elementos[7] = valor;
                break;
            case 8000:
                array_elementos[8] = valor;
                break;
            default:
                break;
        }
    }
    
    $("#formFinal").submit(function(e){
       
        e.preventDefault();
        swal({
            title: "Atención",
            text: "Estas a punto de terminar, ¿Estás seguro de continuar?",
            icon: "warning",
            buttons: {
                cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                },
                confirm: {
                        text: "Confirmar",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                }
            }
        })
        .then((isConfirm) => {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: "../guardarFinal",
                    data: $(this).serialize(),
                    dataType: "json",
                }).done(res=>{
                    swal("Confirmado","La operación se realizo con éxito","success");
                    // $(location).attr('href',$("#linkPaciente").attr("href"));
                }).fail(err=>{
                    console.log(err);
                    swal("Error","La operación no se pudo completar, intentelo más tarde","error");
                });

            } else {
                swal("Cancelado", "la operación fue cancelada", "error");
            }
        });
    });


    $("#formDatos").submit(function(e){
        e.preventDefault();
        var formData = new FormData();
        var input_file = document.getElementById("img_firma");
        formData.append('firma',input_file.files[0]);
        formData.append('cedula',$("#cedula").val());
        formData.append('titulo_profesional',$("#titulo_profesional").val());
        formData.append('nombre_completo',$("#nombre_completo").val());
		$.ajax({
            url:'../guardarFirma',
            type:'post',
            data:formData,
            dataType:'json',
            contentType:false,
            processData:false,
            beforeSend:()=>{

            }
		}).done(res=>{
            swal("Exito","La información se ha actualizado correctamente.","success");
            $("#divBtnModal").html('<p>Puedes actualizar tus datos <button type="button" id="btnModal" class="btn btn-sm btn-outline-adn"><i class="feather icon-upload"></i> Actualizar</button></p>')
            $("#btnFormFinal").attr("disabled",false);
            $("#helpDatos").remove();
            $("#modal").modal('hide');
		}).fail(err=>{
            swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
		});
    })

    $("#btnModal").click(function(){
        $("#modal").modal();
    });
    $(".anormal").click(function(){
        $("#Grafica_VO").show();
        $("#VO").show();
        
       
    });
    
    var toolbarOptions = [
		[{
			'list': 'ordered'
		}, {
			'list': 'bullet'
		}],
		[{
			'direction': 'rtl'
		}],
		// [{
		//   'size': ['small', false, 'large', 'huge']
		// }],
		[{
			'color': []
		}, {
			'background': []
		}],
		[{
			'font': []
		}],
		[{
			'align': []
		}],
		['clean']
	];

    var quill = new Quill('#editor', {
        modules: {
            toolbar: toolbarOptions
            },
        theme: 'snow',
        placeholder: 'Escribe tu nota...'
    });

    quill.on('text-change', function() {
        html = $('#editor').children('.ql-editor').html();
        $("#interpretacion_input").val(html);
    });
});

