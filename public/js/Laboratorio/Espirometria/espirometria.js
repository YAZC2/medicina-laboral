// function initToma() {
//     obj = new Object();
//     obj.empresaId = $('.empresaId').val();
//     obj.empleadoId = $('.empleadoId').val();
//     $.ajax({
//         type:'post',
//         dataType:'json',
//         data:obj,
//         url:'../solicitud',
//     }).done(res=>{
//         console.log(res);
//     }).fail(err=>{

//     })
// }

// $(window).ready(()=>{
//     for (var i = 1; i <= 3; i++) {
//         var data = $('#maneobra_'+i).val();
//         var fvc = $('#fvc_'+i).val();
//         electrocardiograma(data,i,fvc);
//     }
// })

// function electrocardiograma(res,number,fvc) {

//   $('#content').append(
//     `
//     <div class="col-md-4">
//     <div id="ecg${number}" style="height: 400px; width: 100%;"></div>
//     </div>
//     `
//   )

//   data = res.split(' ');
//   dataEcg = [];
//   data.forEach((item, i) => {
//     dataEcg.push(parseInt(item) * 0.001);
//   });

//   console.log(dataEcg);

//   var xAxisStripLinesArray = [];
//   var yAxisStripLinesArray = [];
//   var dps = [];
//   var dataPointsArray =dataEcg;

//   var chart = new CanvasJS.Chart(`ecg${number}`,{
//       title:{
//         text:"Maneobra "+number,
//       },
//       axisY:{
//         stripLines:yAxisStripLinesArray,
//         gridThickness: 1,
//         gridColor:"#002b46",
//         lineColor:"#002b46",
//         tickColor:"#002b46",
//         labelFontColor:"#002b46",
//       },
//       axisX:{
//         stripLines:xAxisStripLinesArray,
//         gridThickness: 1,
//         gridColor:"#002b46",
//         lineColor:"#002b46",
//         tickColor:"#002b46",
//         labelFontColor:"#002b46",
//       },
//       credits: {
//         enabled: false
//     },
//       data: [
//           {
//             type: "spline",
//             color:"black",
//             dataPoints: dps
//           }
//       ]
//   });

//   var volumen = fvc/dataEcg.length;
//   var volumenIncrement = volumen;
//   for(var i=0; i<dataEcg.length;i++){
//     dps.push({y: dataPointsArray[i],x: volumenIncrement});
//     volumenIncrement += volumen;
//   }

//   chart.render();

// }

//////////////////////////////////////////////////////////////////////
// var quill = new Quill('#editor', {
//     modules: {
//         toolbar: toolbarOptions
//         },
//     theme: 'snow',
//     placeholder: 'Escribe tu nota...'
// });

// quill.on('text-change', function() {
//     html = $('#editor').children('.ql-editor').html();
//     $("#interpretacion_input").val(html);
// });
var tmr;

var resetIsSupported = false;
var SigWeb_1_6_4_0_IsInstalled = false; //SigWeb 1.6.4.0 and above add the Reset() and GetSigWebVersion functions
var SigWeb_1_7_0_0_IsInstalled = false; //SigWeb 1.7.0.0 and above add the GetDaysUntilCertificateExpires() function

window.onload = function() {

    var sigWebVer = "";
    try {
        sigWebVer = GetDaysUntilCertificateExpires();
    } catch (err) {
        console.log("Unable to get SigWeb Version: " + err.message)
    }

    if (sigWebVer != "") {
        try {
            SigWeb_1_7_0_0_IsInstalled = isSigWeb_1_7_0_0_Installed(sigWebVer);
        } catch (err) {
            console.log(err.message)
        };
        //if SigWeb 1.7.0.0 is installed, then enable corresponding functionality
        if (SigWeb_1_7_0_0_IsInstalled) {

            resetIsSupported = true;
            try {
                var daysUntilCertExpires = GetDaysUntilCertificateExpires();
                document.getElementById("daysUntilExpElement").innerHTML = "SigWeb Certificate expires in " + daysUntilCertExpires + " days.";
            } catch (err) {
                console.log(err.message)
            };
            var note = document.getElementById("sigWebVrsnNote");
            note.innerHTML = "SigWeb 1.7.0 installed";
        } else {
            try {
                SigWeb_1_6_4_0_IsInstalled = isSigWeb_1_6_4_0_Installed(sigWebVer);
                //if SigWeb 1.6.4.0 is installed, then enable corresponding functionality
            } catch (err) {
                console.log(err.message)
            };
            if (SigWeb_1_6_4_0_IsInstalled) {
                resetIsSupported = true;
                var sigweb_link = document.createElement("a");
                sigweb_link.href = "https://www.topazsystems.com/software/sigweb.exe";
                sigweb_link.innerHTML = "https://www.topazsystems.com/software/sigweb.exe";

                var note = document.getElementById("sigWebVrsnNote");
                note.innerHTML = "SigWeb 1.6.4 is installed. Install the newer version of SigWeb from the following link: ";
                note.appendChild(sigweb_link);
            } else {
                var sigweb_link = document.createElement("a");
                sigweb_link.href = "https://www.topazsystems.com/software/sigweb.exe";
                sigweb_link.innerHTML = "https://www.topazsystems.com/software/sigweb.exe";

                var note = document.getElementById("sigWebVrsnNote");
                note.innerHTML = "A newer version of SigWeb is available. Please uninstall the currently installed version of SigWeb and then install the new version of SigWeb from the following link: ";
                note.appendChild(sigweb_link);
            }
        }
    } else {
        //Older version of SigWeb installed that does not support retrieving the version of SigWeb (Version 1.6.0.2 and older)
        var sigweb_link = document.createElement("a");
        sigweb_link.href = "https://www.topazsystems.com/software/sigweb.exe";
        sigweb_link.innerHTML = "https://www.topazsystems.com/software/sigweb.exe";

        var note = document.getElementById("sigWebVrsnNote");
        note.innerHTML = "A newer version of SigWeb is available. Please uninstall the currently installed version of SigWeb and then install the new version of SigWeb from the following link: ";
        note.appendChild(sigweb_link);
    }

}

function isSigWeb_1_6_4_0_Installed(sigWebVer) {
    var minSigWebVersionResetSupport = "1.6.4.0";

    if (isOlderSigWebVersionInstalled(minSigWebVersionResetSupport, sigWebVer)) {
        console.log("SigWeb version 1.6.4.0 or higher not installed.");
        return false;
    }
    return true;
}

function isSigWeb_1_7_0_0_Installed(sigWebVer) {
    var minSigWebVersionGetDaysUntilCertificateExpiresSupport = "1.7.0.0";

    if (isOlderSigWebVersionInstalled(minSigWebVersionGetDaysUntilCertificateExpiresSupport, sigWebVer)) {
        console.log("SigWeb version 1.7.0.0 or higher not installed.");
        return false;
    }
    return true;
}

function isOlderSigWebVersionInstalled(cmprVer, sigWebVer) {
    return isOlderVersion(cmprVer, sigWebVer);
}

function isOlderVersion(oldVer, newVer) {
    const oldParts = oldVer.split('.')
    const newParts = newVer.split('.')
    for (var i = 0; i < newParts.length; i++) {
        const a = parseInt(newParts[i]) || 0
        const b = parseInt(oldParts[i]) || 0
        if (a < b) return true
        if (a > b) return false
    }
    return false;
}

function onSign() {
    if (IsSigWebInstalled()) {
        var ctx = document.getElementById('cnv').getContext('2d');
        SetDisplayXSize(500);
        SetDisplayYSize(100);
        SetTabletState(0, tmr);
        SetJustifyMode(0);
        ClearTablet();
        if (tmr == null) {
            tmr = SetTabletState(1, ctx, 50);
        } else {
            SetTabletState(0, tmr);
            tmr = null;
            tmr = SetTabletState(1, ctx, 50);
        }

        $('#button1').removeClass('d-none');
        $('#button2').removeClass('d-none');
        $('#button3').hide();
    } else {
        alert("Unable to communicate with SigWeb. Please confirm that SigWeb is installed and running on this PC.");
    }
}

function onClear() {
    ClearTablet();
}

function onDone() {
    if (NumberOfTabletPoints() == 0) {
        alert("Por favor firme antes de continuar");
    } else {
        $('#boton').show();
        $('#button1').hide();
        $('#button2').hide();
        $('#mensajitoirma').text('Firma agregada correctamente');

        SetTabletState(0, tmr);
        //RETURN TOPAZ-FORMAT SIGSTRING
        SetSigCompressionMode(1);
        document.FORM1.bioSigData.value = GetSigString();
        //document.FORM1.sigStringData.value = GetSigString();
        //this returns the signature in Topaz's own format, with biometric information

        //RETURN BMP BYTE ARRAY CONVERTED TO BASE64 STRING
        SetImageXSize(500);
        SetImageYSize(100);
        SetImagePenWidth(5);
        GetSigImageB64(SigImageCallback);

    }
}

function SigImageCallback(str) {
    document.FORM1.sigImageData.value = str;
}

function endDemo() {
    ClearTablet();
    SetTabletState(0, tmr);
}

function close() {
    if (resetIsSupported) {
        Reset();
    } else {
        endDemo();
    }
}

//Perform the following actions on
//	1. Browser Closure
//	2. Tab Closure
//	3. Tab Refresh
window.onbeforeunload = function(evt) {
    close();
    clearInterval(tmr);
    evt.preventDefault(); //For Firefox, needed for browser closure
};
$("#form_espirometria").submit(function(e){
        e.preventDefault();
        let formulario = $(this).serialize();
        var formData = new FormData();
        var input_file = document.getElementById("archivo");
        console.log(input_file.files[0]);
        quimico=$('input[name="quimico"]:checked').val();
        cirugia=$('input[name="cirugia"]:checked').val();
        cirugia_ojos=$('input[name="cirugia_ojos"]:checked').val();
        infarto=$('input[name="infarto"]:checked').val();
        tuberculosis=$('input[name="tuberculosis"]:checked').val();
        embarazada=$('input[name="embarazada"]:checked').val();
        tos_sangre=$('input[name="tos_sangre"]:checked').val();
        neumotorax=$('input[name="neumotorax"]:checked').val();
        Traqueostomia=$('input[name="Traqueostomia"]:checked').val();
        sonda=$('input[name="sonda"]:checked').val();
        Aneurisma=$('input[name="Aneurisma"]:checked').val();
        TROMBOEMBOLIA=$('input[name="TROMBOEMBOLIA"]:checked').val();
        VOMITO=$('input[name="VOMITO"]:checked').val();
        DILATADOR=$('input[name="DILATADOR"]:checked').val();
        ALIMENTOS=$('input[name="ALIMENTOS"]:checked').val();
        EJERCICIO=$('input[name="EJERCICIO"]:checked').val();
        firma=$("#img").val();
       
        formData.append('curp',$("#curp").val());
        formData.append('tipo_resultado',$('input[name="tipo_resultado"]:checked').val());
        formData.append('firma',firma);
        formData.append('nim',$("#nim").val());
        formData.append('p_trabajo',$("#p_trabajo").val()); 
        formData.append('quimico',quimico);
        formData.append('t_puesto',$("#t_puesto").val()); 
        formData.append('cirugia',cirugia); 
        formData.append('cirugia_ojos',cirugia_ojos); 
        formData.append('infarto',infarto); 
        formData.append('cuando_infarto',$("#cuando_infarto").val()); 
        formData.append('tuberculosis',tuberculosis); 
        formData.append('embarazada',embarazada); 
        formData.append('tos_sangre',tos_sangre); 
        formData.append('neumotorax',neumotorax); 
        formData.append('Traqueostomia',Traqueostomia); 
        formData.append('sonda',sonda); 
        formData.append('Aneurisma',Aneurisma); 
        formData.append('TROMBOEMBOLIA',TROMBOEMBOLIA); 
        formData.append('VOMITO',VOMITO); 
        formData.append('DILATADOR',DILATADOR); 
        formData.append('ALIMENTOS',ALIMENTOS); 
        formData.append('EJERCICIO',EJERCICIO);
        formData.append('archivo',input_file.files[0]);
        formData.append('comentarios',$("#comentarios").val());  
		$.ajax({
            url:'../guarda_espirometria',
            type:'post',
            data:formData,
            dataType:'json',
            contentType:false,
            processData:false,
            beforeSend:()=>{
            }
		}).done(res=>{
            swal({
                position: 'top-end',
                icon: 'success',
                title: "Guardado",
                text:"Espirometría guardada correctamente",
                showConfirmButton: true
              }).then(isConfirm => {
                
                window.location.reload();
              });         
		}).fail(err=>{
            swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
		});
    });
function espirometria(){
    alert("sdsasad");
}

$(document).on('change', '#fv_input', function(){

    if ($(this).val()) {
        var formData = new FormData(document.getElementById("fv"));
        $.ajax({
            url: "../fv_file",
            type: "post",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend:()=>{
                $('.fv-load').show();

            }
        }).done(function(res){
            $('.fv-load').hide();

            $('.fv_img').attr('src','../storage/app/espirometria/'+res.data.fv);
        }).fail(err=>{
            swal('Error','Hubo algún problema al subir el archivo, intentalo más tarde','error')
        });
    }
});

$(document).on('change', '#vt_input', function(){
    console.log($(this).val());
    if ($(this).val()) {
        var formData = new FormData(document.getElementById("vt"));

        $.ajax({
            url: "../vt_file",
            type: "post",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend:()=>{
                $('.vt-load').show();
            }
        }).done(function(res){
            $('.vt-load').hide();
            $('.vt_img').attr('src','../storage/app/espirometria/'+res.data.vt);

        }).fail(err=>{
            swal('Error','Hubo algún problema al subir el archivo, intentalo más tarde','error')
        })
    }

});



$('.updateMedico').click(function (e) {
    $('#datosMedico').modal('show');
});


$('#updateFormData').on('submit',function(e){

    e.preventDefault();
    var formData = new FormData(document.getElementById("updateFormData"));
    $.ajax({
        type: "post",
        url: "../Audiometria/guardarFirma",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        beforeSend:()=>{
          $('.loadFormUpdate').show();
          $('#updateFormData').hide();

        }
    }).done(res=>{

        $('.firmaMedico').attr('src','../storage/app/datosHtds/'+res.firma);
        $('.titulo').text(res.titulo_profesional);
        $('.nombre').text(res.nombre_completo);
        $('.cedula').text(res.cedula);
        $('#datosMedico').modal('hide');
        $('.loadFormUpdate').hide();
        $('#updateFormData').show();
        swal('Modificado','Los datos se modificaron correctamente','success')

    }).fail(err=>{
        $('.loadFormUpdate').hide();
        $('#updateFormData').show();
        swal('Error','Hubo algún problema, intentalo más tarde','error');
    });

})



$('#saveForm').on('submit',function(e){
    e.preventDefault();
    swal({
        title: "Atención",
        text: "Estas a punto de terminar, ¿Estás seguro de continuar?",
        icon: "warning",
        buttons: {
            cancel: {
                    text: "Cancelar",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
            },
            confirm: {
                    text: "Confirmar",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
            }
        }
    })
    .then((isConfirm) => {
        if (isConfirm) {
            saveEspirometria();
        } else {
            swal("Cancelado", "la operación fue cancelada", "error");
        }
    });
})





function saveEspirometria() {
    let interpretacion = $('#interpretacion').val();
    let medico = $('#medicoToken').val();

    if (interpretacion == ''){
        swal('Espera...','Falta por interpretar el estudio.','info');
        return
    }else if(medico == ""){
        swal('Espera...','Falta la firma de medico.','info');
        return
    }
    data = new Object();
    data.ingreso = $('#ingreso').val();
    data.interpretacion = interpretacion;
    data.espiroId = $('#espirometria').val();

    $.ajax({
        type: "post",
        url: "../save_espirometria",
        data: data,
        dataType: "json",
        beforeSend:()=>{
          $('.loadFormUpdate').show();
          $('#updateFormData').hide();

        }
    }).done(res=>{

        swal({
			position:'top-end',
			icon:'success',
			text:'El estudio fue terminado correctamente',
			showConfirmButton:true
		}).then(isConfirm=>{
			location.href = "../Medicina-Pacientes";
		})

    }).fail(err=>{

        swal('Error','Hubo algún problema, intentalo más tarde','error');
    });
}
