$(".table_estudios").DataTable({
    responsive: !1,
    language: {
        decimal: "",
        emptyTable: "No hay información",
        info: "",
        infoEmpty: "Mostrando 0 de 0 de 0 Entradas",
        infoFiltered: "(Filtrado de _MAX_ total entradas)",
        infoPostFix: "",
        thousands: ",",
        lengthMenu: "_MENU_",
        loadingRecords: "Cargando...",
        processing: "Procesando...",
        search: "Buscar",
        zeroRecords: "Sin resultados encontrados",
        paginate: {
            first: "Primero",
            last: "Ultimo",
            next: "Siguiente",
            previous: "Anterior",
        },
    },
});

$(document).ready(function () {
    $("#tabla_dinamica").empty();
    nombre = "";
    apellido = "";
    edad = "";
    fechaalta = "";
    estudiosempleado = [];
    folio = "";
    data = new Object();
    let codigo = $(`input[name=c_sass_empresa]`).val();
    let empresa = $(`input[name=n_sass_empresa]`).val();
    data.metodo = "empresa_ima";
    data.codigo = codigo;
    data.empresa = empresa;
    data.fecha1 = "2022-01-01";
    data.fecha2 = "2022-12-30";
    $.ajax({
        type: "post",
        data: data,
        dataType: "json",
        url: "https://asesores.ac-labs.com.mx/sass/index.php",
        beforeSend: () => {},
    })
        .done((res) => {
            console.log(res);
            $(".cargar").empty();
            $("#tabla_dinamica").append(`
    <div class='row'>            
        <div class='col-4'>
            <label for="start">Fecha Inicial:</label>
            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <label for="start">Fecha Final:</label>
            <input type="date" id="fecha_final" name="fecha_final" value="2021-07-22" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
        </div>
        <div class='col-4'>
            <a onclick="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
        </div>
    </div>`);

            $("#tabla_dinamica")
                .append(`<table class="table_estudios1 table" style="width:100%">
    <thead>
      <tr>
        <th>Empleado</th>
        <th>Toma</th>
        <th>Nivel de riesgo</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody></tbody>
    </table>`);

            res.pacientes.forEach((item, i) => {
                $("tbody").append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td class="text-center">
                                            <span class="badge rounded-pill bg-warning">Medio</span>
                                         </td>
                                         <td>
                                            <a data-id="${item.Nim}" data-nombre="${item.nombre}" data-apellido="${item.apellido}" data-id_paciente="${item.id_paciente}" class="MostrarEstudio btn btn-icon rounded-circle btn-primary btn-sm waves-effect waves-light mt-25 text-white mt-1">
                                                <i class="feather float-right icon-plus"></i>
                                            </a> 
                                            
                                         </td>     
                                     </tr>`);
            });

            nueva_tabla();
        })
        .fail((err) => {});
});

$(document).on("click", ".buscar_fecha", function () {
    fecha_inicial = $("input[name=fecha_inicial]").val();
    fecha_final = $("input[name=fecha_final]").val();
    estudio_laboratorio(fecha_inicial, fecha_final);
});
function estudio_laboratorio(fecha_inicial, fecha_final) {
    $("#tabla_dinamica").empty();
    nombre = "";
    apellido = "";
    edad = "";
    fechaalta = "";
    estudiosempleado = [];
    folio = "";
    data = new Object();
    let codigo = $(`input[name=c_sass_empresa]`).val();
    let empresa = $(`input[name=n_sass_empresa]`).val();
    data.metodo = "empresa_ima";
    data.codigo = codigo;
    data.empresa = empresa;
    data.fecha1 = fecha_inicial;
    data.fecha2 = fecha_final;
    $.ajax({
        type: "post",
        data: data,
        dataType: "json",
        url: "https://asesores.ac-labs.com.mx/sass/index.php",
        beforeSend: () => {
            $(".cargar")
                .append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
        },
    })
        .done((res) => {
            $(".cargar").empty();
            $("#tabla_dinamica").append(`
              <div class='row'>            
                  <div class='col-4'>
                      <label for="start">Fecha Inicial:</label>
                      <input type="date" id="fecha_inicial" name="fecha_inicial" value="${fecha_inicial}" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
                  </div>
                  <div class='col-4'>
                      <label for="start">Fecha Final:</label>
                      <input type="date" id="fecha_final" name="fecha_final" value="${fecha_final}" min="2020-01-01" max="2023-12-31" class='form-control pickadate-disable picker__input picker__input--active'>
                  </div>
                  <div class='col-4'>
                      <a onclick="" class="btn btn-secondary  buscar_fecha text-white mt-2"> Buscar</a>
                  </div>
              </div>`);

              $("#tabla_dinamica").append(`<table class="table_estudios1 table" style="width:100%">
                  <thead>
                    <tr>
                      <th>Empleado</th>
                      <th>Toma</th>
                      <th>Nivel de Riesgo</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>`);

            res.pacientes.forEach((item, i) => {
                $("tbody").append(`<tr><td>${item.nombre} ${item.apellido}</td>
                                         <td>${item.Nim}</td>
                                         <td>
                                            <span class="badge rounded-pill bg-warning">Medio</span>
                                         </td>
                                         <td>
                                         <a data-id="${item.Nim}" data-id_paciente="${item.id_paciente}" data-id_toma="${item.id_toma}" data-contra="${item.Contrasena}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                            <i class="feather icon-eye"></i>
                                         </a>
                                         </td>     
                                     </tr>`);
            });

            nueva_tabla();
        })
        .fail((err) => {});
}

function nueva_tabla() {
    $(".table_estudios1").DataTable({
        responsive: !1,
        language: {
            decimal: "",
            emptyTable: "No hay información",
            info: "",
            infoEmpty: "Mostrando 0 de 0 de 0 Entradas",
            infoFiltered: "(Filtrado de _MAX_ total entradas)",
            infoPostFix: "",
            thousands: ",",
            lengthMenu: "_MENU_",
            loadingRecords: "Cargando...",
            processing: "Procesando...",
            search: "Buscar",
            zeroRecords: "Sin resultados encontrados",
            paginate: {
                first: "Primero",
                last: "Ultimo",
                next: "Siguiente",
                previous: "Anterior",
            },
        },
    });

    function sumarDias(fecha, dias) {
        fecha.setDate(fecha.getDate() + dias);
        return fecha;
    }
}
$(document).on("click", ".MostrarEstudio", function () {
    $(".modal-title").empty();
    $(".modal-title").append(`RIESGO CV-${$(this).data('nombre')} ${$(this).data('apellido')}`);
    // $("#edad").append(`Edad: ${$(this).data('edad')}`);
    $("#idpaciente").append(`<b>ID Paciente:</b> ${$(this).data('id_paciente')}`);
    // $(this).data('id')
    
    $("#Vista_Modal").modal("show");
});

