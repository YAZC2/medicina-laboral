@extends('layouts.app')

@section('styles')
<link href="../../resources/sass/fontawesome/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../resources/sass/css/styles.css">
@endsection

@section('content')

<div class="bienvenidos_device"></div>

<div class="col-md-5">
    <div class="content">
        <div class="logo">
            <img src="../../resources/sass/images/logotipo.png" alt="Diagnosticos Clinicos">
        </div>
        <div class="formulario">

            <h2 class="title">Reestablecer contraseña</h2>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @else
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="input-group">
                        <label for="email" class="col-md-4 col-form-label text-md-left">Correo electrónico</label>

                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <button type="submit" class="btn btn_custom">
                        Enviar enlace
                    </button>

                </form>

                <div class="btn-rst-pwrd">
                    <a class="btn btn-link" style="display:block"  href="../../public/" >
                        Regresar a inicio de sesión
                    </a>
                </div>
            @endif

        </div>
    </div>
</div>

<div class="col-md-7 bienvenidos"></div>

@endsection
