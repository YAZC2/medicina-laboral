<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Audiometría de {{ucfirst($empleado->nombre) .' '. ucfirst($empleado->apellido_paterno).' '. ucfirst($empleado->apellido_materno)}}</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        body{
        }
        .titulo{
            font-size: 20px;
            text-align: center;
        }
        .header-info{
            font-size: 12px;
            padding: 0px;
            margin: 0px;
        }
        th{
            font-size: 12px;
            background-color: #f15a29;
            color: #fff;
            border: #cececece 1px solid;
            height: 60px;
        }
        .trCabecera td{
            height: 40px;
            font-size: 11px;
            border: 1px solid #cecece;
            align-items: center;
            vertical-align: middle;
        }
        .letras td{
            font-size: 10px;
            height: 30px;
            padding: 10px 2px;
            border: 1px solid #cecece;
            align-items: center;
            vertical-align: middle;
        }
        .interpretacion p{
            font-size: 12px;
            padding: 0px;
            margin: 0px;
        }
        .interpretacion span{
            font-size: 10px;
        }
        .interpretacion hr{
            margin: 2px 0px;
        }
        .antecedentes p{
            font-size: 12px;
            padding: 0px;
            margin: 0px;
        }
        .antecedentes hr{
            margin: 2px 0px;
        }
        .antecedentes ul>li{
            font-size: 10px;
        }
        .saltoPagina{
            page-break-after: always;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-12 titulo">
            EXAMEN AUDIOMETRICO
        </div>
    </div>
    <div style="height: 15px"></div>
    <div class="row">
        <div class="col-12 col-md-6">
            <p class="header-info">Nombre del paciente: {!! $empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno !!}</p>
            <p class="header-info">Edad: {{ \Carbon::parse($empleado->fecha_nacimiento)->age}} años</p>
            <p class="header-info">NIM: {!! $audiometria->nim !!}</p>
        </div>
        <div class="col-12 col-md-6 text-right">
            <p class="header-info">Fecha: {!! $audiometria->updated_at->format('d/M/Y') !!}</p>
            <p class="header-info">Empresa: {!! $empleado->empresa->nombre !!}</p>
            <p class="header-info">Puesto de trabajo: {!! $audiometria->puesto_trabajo_solicitado !!}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <img src="{!! $audiometria->imagen !!}" class="w-100" alt="">
        </div>
    </div>
    <div style="height: 50px"></div>
    <div class="row">
        <div class="col-12">
            <table width="100%" class="text-center">
                <thead>
                    <tr>
                        <th>EVALUACION Y SIGNIFICADO DEL INDICE SAL</th>
                        <th>(INDICE DE FLETCHER MODIFICADO POR BERRUECO)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="70%">
                            <table width="100%">
                                <tr class="trCabecera">
                                    <td>GRADO</td>
                                    <td>SAL (dB)</td>
                                    <td>NOMBRE DE <br> LA CLASE</td>
                                    <td>CARACTERISTICAS</td>
                                </tr>
                                <tr class="letras">
                                    <td>A</td>
                                    <td>-16</td>
                                    <td>Normal</td>
                                    <td>Los dos oídos están dentro de los limites normales.</td>
                                </tr>
                                <tr class="letras">
                                    <td>B</td>
                                    <td>16-30</td>
                                    <td>Casi normal</td>
                                    <td>Tiene dificultades en conversaciones en voz baja nada más.</td>
                                </tr>
                                <tr class="letras">
                                    <td>C</td>
                                    <td>31 - 45</td>
                                    <td>Ligero empeoramiento</td>
                                    <td>Tiene dificultades en una conversación normal, pero no si se levanta la voz.</td>
                                </tr>
                                <tr class="letras">
                                    <td>D</td>
                                    <td>46 - 60</td>
                                    <td>Serio empeoramiento</td>
                                    <td>Tiene dificultades incluso cuando se levanta la voz.</td>
                                </tr>
                                <tr class="letras">
                                    <td>E</td>
                                    <td>61 - 90</td>
                                    <td>Grave empeoramiento</td>
                                    <td>Sólo puede oír una conversación amplificada.</td>
                                </tr>
                                <tr class="letras">
                                    <td>F</td>
                                    <td>91 o +</td>
                                    <td>Profundo empeoramiento</td>
                                    <td>No puede entender ni una conversación amplificada.</td>
                                </tr>
                                <tr class="letras">
                                    <td>G</td>
                                    <td colspan="2">Sordera total ambos oidos</td>
                                    <td>No puede oír sonido alguno.</td>
                                </tr>
                            </table>
                        </td>
                        <td width="30%">
                            <table width="100%">
                                <tr class="trCabecera">
                                    <td colspan="2">DSHL*</td>
                                    <td>% Pérdida</td>
                                </tr>
                                <tr class="letras">
                                    <td>Oído derecho</td>
                                    <td>{!! $dshl_d !!} dB</td>
                                    <td>{!! $dshl_d / 4 * 0.8 !!} %</td>
                                </tr>
                                <tr class="letras">
                                    <td>Oído izquierdo</td>
                                    <td>{!! $dshl_i !!} dB</td>
                                    <td>{!! $dshl_i / 4 * 0.8 !!} %</td>
                                </tr>
                                <tr class="letras">
                                    <td colspan="3">*DSHL:Suma de pérdidas a 500,1000, 2000 y 4000Hz</td>
                                </tr>
                                <tr class="letras">
                                    <td colspan="3">ÍNDICE SAL </td>
                                </tr>
                                <tr class="letras">
                                    <td>Oído derecho</td>
                                    <td>{!! $indice_sal_d !!} dB</td>
                                    <td>
                                        @if($indice_sal_d >= 91)
                                        F
                                        @elseif($indice_sal_d > 60 && $indice_sal_d < 91)
                                        E
                                        @elseif($indice_sal_d > 45 && $indice_sal_d < 61)
                                        D
                                        @elseif($indice_sal_d > 30 && $indice_sal_d < 46)
                                        C
                                        @elseif($indice_sal_d > 15 && $indice_sal_d < 31)
                                        B
                                        @else
                                        A
                                        @endif
                                    </td>
                                </tr>
                                <tr class="letras">
                                    <td>Oído izquierdo</td>
                                    <td>{!! $indice_sal_i !!} dB</td>
                                    <td>
                                        @if($indice_sal_i >= 91)
                                        F
                                        @elseif($indice_sal_i > 60 && $indice_sal_i < 91)
                                        E
                                        @elseif($indice_sal_i > 45 && $indice_sal_i < 61)
                                        D
                                        @elseif($indice_sal_i > 30 && $indice_sal_i < 46)
                                        C
                                        @elseif($indice_sal_i > 15 && $indice_sal_i < 31)
                                        B
                                        @else
                                        A
                                        @endif
                                    </td>
                                </tr>
                                <tr class="letras">
                                    <td colspan="3">--</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="saltoPagina"></div>
    <div class="row">
        <div class="col-12 antecedentes">
            <p>Antecedentes</p>
            <hr>
            <ul>
                @if($audiometria->check_proteccion_auditiva == "on")
                <li>uso de protección auditiva actualmente</li>
                @endif
                @if($audiometria->check_tce == "on")
                <li>refiere de traumatismo craneoencefálico</li>
                @endif
                @if($audiometria->tiempo_trabajando_exposicion != null)
                <li>refiere exposició a ruido laboral durante {!! $audiometria->tiempo_trabajando_exposicion !!}</li>
                @endif
                @if($audiometria->tiempo_puesto != null)
                <li>antigüedad de {!! $audiometria->tiempo_puesto !!} en el empleo actual</li>
                @endif
                @if($audiometria->check_explosion_cercana == "on")
                <li>ha estado en una explosión cercana, oido afectado: {!! $audiometria->ec_oido !!}</li>
                @endif
                @if($audiometria->check_audifonos_musica == "on")
                <li>escucha música {!! $audiometria->am_no_dias_semana !!} día(s) a la semana, en un volumen de {!! $audiometria->am_volumen !!} durante {!! $audiometria->am_cuantos_anios  !!} año(s)</li>
                @endif
                @if($audiometria->check_gripa_infancia == "on")
                <li>refiere gripa frecuente en infancia</li>
                @endif
                @if($audiometria->check_gripa_actualidad == "on")
                <li>refiere gripa en la actualidad</li>
                @endif
                @if($audiometria->check_infeccion_oidos == "on")
                <li>refiere infección de oídos</li>
                @endif
                @if($audiometria->check_hipertension == "on")
                <li>refiere tener hipertensión</li>
                @endif
                @if($audiometria->check_paralisis_facial == "on")
                <li>refiere tener paralisis facial</li>
                @endif
                @if($audiometria->check_diabetes == "on")
                <li>refiere tener diabetes</li>
                @endif
                @if($audiometria->check_eventos == "on")
                <li>refiere acudir a fiestas, antros, bailes con la frecuencia de {!! $audiometria->e_frecuencia !!}</li>
                @endif
                @if($audiometria->check_varicela == "on")
                <li>refiere tener o haber tenido varicela</li>
                @endif
                @if($audiometria->check_practicas == "on")
                <li>refiere practicar natación, aviación o uso de motocicleta con la frecuencia de {!! $audiometria->p_frecuencia !!}</li>
                @endif
                @if($audiometria->check_medicamentos == "on")
                <li>refiere tomar {!! $audiometria->m_cual !!} frecuentemente</li>
                @endif
                @if($audiometria->check_familiar_problemas == "on")
                <li>refiere tener algún familiar con problemas de audición</li>
                @endif
            </ul>
        </div>
    </div>
    <div style="height: 25px"></div>
    <div class="row">
        <div class="col-12 interpretacion">
            <p>Interpretación</p>
            <hr>
            <span>{!! $audiometria->interpretacion !!}</span>
        </div>
    </div>
    <div style="height: 25px"></div>
    <div class="row">
        <div class="col-12 text-center">
            <hr>
            <img src="{!! asset("storage/app/datosHtds/".$user_htds->firma) !!}" style="width: 200px;">
            <br>
            Dra. Fatima Andrea Mora Sánchez
            <br>
            Cedula Profesional {!! $user_htds->cedula !!}
        </div>
    </div>
</body>
</html>
