<!doctype html>
<html lang="en-us">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .alert-primary{
             background-color: #002b46!important;
            color: #fff;
         }
         .h5{
             color:#002b46;
             font-family:"Segoe UI', Tahoma, Geneva, Verdana, sans-serif";
         }
    </style>
</head>
<body>
   
    <p><b>Nombre:</b> {{ $empleado->nombre }} {{ $empleado->apellido_parteno }} {{ $empleado->apellido_materno }} </p>
    <div class="alert m-0 p-0 alert-primary text-center">
        No cuenta con información
    </div>
</body>
</html>
