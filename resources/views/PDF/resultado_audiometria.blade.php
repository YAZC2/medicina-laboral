@extends('layouts.VuexyLaboratorio')

@section('title', 'Audiometría')

@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css"
    href="{!! asset('public/vuexy/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') !!}">
<link rel="stylesheet" type="text/css"
    href="{!! asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/css/daicoms/daicoms.css') !!}">
@endsection

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{ route('empleados') }}">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Audiometria
            {{ $empleado->nombre . ' ' . $empleado->apellido_paterno . ' ' . $empleado->apellido_materno }}</li>
    </ol>
</nav>
{{-- <div class="canvas_div_pdf"> --}}
    <div class="row" id="pruebaRow" style="font-size:16px;">
        <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-content">
                    <div class="row mt-1">
                        <div class="col-4">
                        </div>
                        <div class="col-4">
                        </div>
                        <div class="col-2">
                        </div>
                        <div class="col-2">
                            <button class="btn btn-sm" id="bim" onclick="printDiv('imprimir')"><img
                                    src="//img.icons8.com/office/36/000000/print.png" /></button>

                            {{-- <a id="crearPdf" href="{!! url("crearPDF/audiometria/".$audiometria->id."/".$audiometria->curp) !!}" class="btn
                                btn-primary btn-sm float-right btn_export_pdf text-white">Crear PDF</a> --}}
                        </div>
                    </div>
    
                    <div class="card-body pl-3 pr-3" id="imprimir">
                        <div class="col-md-12 row pt-1">
                            <div class="col-md-3"> <img
                                    src="https://has-humanly.com/pagina_has/images/cahitoredondo.svg" class="float-left"
                                    width="200;"></div>
                            <div class="col-md-6">
                                <p class="text-center"><b>ASESORES ESPECIALIZADOS EN LABORATORIOS MATRIZ</b> <br>
                                    21 PONIENTE No. 3711, COL BELISARIO DOMINGUEZ C.P. 72180, PUEBLA, PUE<br>
                                    TELS. (222)2966608, 2966609, 2966610 y 2224540933</p>
                            </div>
                            <div class="col-md-3"><img src="https://has-humanly.com/pagina_has/images/logo_30.svg"
                                    class="float-right" width="200;"></div>
                        </div>

                        @if ($audiometria != null)
                        <h5 class="text-center primary" id="pr1">EXAMEN AUDIOMETRICO</h5>
                        <div class="row">
                            <div class="col-4 col-md-4">
                                <p class="card-subtitle"><b>Nombre del paciente:</b> {{ $empleado->nombre }}
                                    {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}</p>
                                <p class="card-subtitle pt-1"><b>Empresa:</b> {{ $empresa->nombre }}</p>
                            </div>

                            <div class="col-4 col-md-4">
                                <p class="card-subtitle"><b>Código:</b> {{ $audiometria->nim }}</p>
                                <p class="card-subtitle pt-1"><b>Edad:</b> {{
                                    \Carbon::parse($empleado->fecha_nacimiento)->age }}</p>
                            </div>
                            <div class="col-4 col-md-4">
                                <p class="card-subtitle"><b>Puesto de trabajo:</b>
                                    {{ $audiometria->puesto_trabajo_solicitado }}</p>
                                <p class="card-subtitle pt-1"><b>Fecha:</b> {{ $empleado->created_at }}</p>
                            </div>
                            @if(!empty($audiometria->oido_derecho_sel) || !empty($audiometria->interpretacion))

                            <div class="col-md-6">
                                {{-- <div id="line-chart2"></div> --}}
                                <div id="AVA"></div>
                                <input type="hidden" name="audiometria_id" id="audiometria_id"
                                    value="{{ $audiometria->id }}">
                            </div>
                            <div class="col-md-6" id="VO">
                                {{-- <div id="line-chart3"></div> --}}
                                <div id="AVO"></div>
                            </div>
                        </div>
                        <hr>
                        <div id="pruebaNueva">
                            <div class="row" style="font-size:14px;">
                                <div class="col-md-9">
                                    <table class="table table-bordered table-sm table-responsive" style="">
                                        <thead class="">
                                            <th colspan="4" class="bg-primary text-white" style="text-align: center;">
                                                EVALUACIÓN Y SIGNIFICADO DEL INDICE</th>
                                        </thead>
                                        <thead class="primary" style="text-align: center;">
                                            <th style="text-align: center;">GRADO</th>
                                            <th style="text-align: center;">SAL(dB)</th>
                                            <th style="text-align: center;">NOMBRE DE LA CLASE</th>
                                            <th style="text-align: center;">CARACTERISTICAS</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>A</td>
                                                <td>-16</td>
                                                <td>Normal</td>
                                                <td>Los dos oídos están dentro de los limites normales.</td>
                                            </tr>
                                            <tr>
                                                <td>B</td>
                                                <td>16-30</td>
                                                <td>Casi Normal</td>
                                                <td>Tiene dificultades en conversaciones en voz baja nada más.</td>
                                            </tr>
                                            <tr>
                                                <td>C</td>
                                                <td>31-45</td>
                                                <td>Ligero empeoramiento</td>
                                                <td>Tiene dificultades en una conversación normal, pero no si se levanta
                                                    la voz.</td>
                                            </tr>
                                            <tr>
                                                <td>D</td>
                                                <td>46-60</td>
                                                <td>Serio empeoramiento</td>
                                                <td>Tiene dificultades incluso cuando se levanta la voz.</td>
                                            </tr>
                                            <tr>
                                                <td>E</td>
                                                <td>61-90</td>
                                                <td>Grave empeoramiento</td>
                                                <td>Sólo puede oír una conversación amplificada.</td>
                                            </tr>
                                            <tr>
                                                <td>F</td>
                                                <td>91 o +</td>
                                                <td>Profundo empeoramiento</td>
                                                <td>No puede entender ni una conversación amplificada.</td>
                                            </tr>
                                            <tr>
                                                <td>G</td>
                                                <td></td>
                                                <td>Sordera total ambos oidos</td>
                                                <td>No puede oír sonido alguno</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <div class="row text-center col-md-12">
                                        <p class="small text-center" style="text-align: center;">
                                            (INDICE DE FLETCHER MODIFICADO POR BERRUECO)
                                        </p>
                                    </div>
                                    <table class="table table-bordered table-sm">
                                        <thead class="">
                                            <th></th>
                                            <th> <b style="font-size:16px;">DSHL*</b></th>
                                            <th><b style="font-size:16px;">% Pérdida</b></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>OD</td>
                                                <td> {!! $dshl_d !!} dB</td>
                                                <td> {!! ($dshl_d / 4) * 0.8 !!} %</td>
                                            </tr>
                                            <tr>
                                                <td>OI</td>
                                                <td> {!! $dshl_i !!} dB</td>
                                                <td> {!! ($dshl_i / 4) * 0.8 !!} %</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row text-center">
                                        <div class="col-12 mb-1">
                                            <span class="small">*DSHL:Suma de pérdidas a 500,1000, 2000 y
                                                4000Hz</span>
                                        </div>
                                    </div>
                                    <table class="table table-bordered table-sm">
                                        <div class="row text-center col-md-12">
                                            <p class="text-center col-md-12">
                                                <b>ÍNDICE SAL</b>
                                            </p>
                                        </div>
                                        <tbody>
                                            <tr>
                                                <td>OD</td>
                                                <td> {!! $indice_sal_d !!} dB</td>
                                                <td>
                                                    @if ($indice_sal_d >= 91)
                                                    F
                                                    @elseif($indice_sal_d > 60 && $indice_sal_d < 91) E
                                                        @elseif($indice_sal_d> 45 && $indice_sal_d < 61) D
                                                            @elseif($indice_sal_d> 30 && $indice_sal_d < 46) C
                                                                @elseif($indice_sal_d> 15 && $indice_sal_d < 31) B @else
                                                                    A @endif </td>
                                            </tr>
                                            <tr>
                                                <td>OI</td>
                                                <td> {!! $indice_sal_i !!} dB</td>
                                                <td>
                                                    @if ($indice_sal_i >= 91)
                                                    F
                                                    @elseif($indice_sal_i > 60 && $indice_sal_i < 91) E
                                                        @elseif($indice_sal_i> 45 && $indice_sal_i < 61) D
                                                            @elseif($indice_sal_i> 30 && $indice_sal_i < 46) C
                                                                @elseif($indice_sal_i> 15 && $indice_sal_i < 31) B @else
                                                                    A @endif </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <b>Sintomas:</b>
                                    {{ $audiometria->sintomas }}
                                    <b>Otoscopia:</b>
                                    {{ $audiometria->otoscopia }}
                                </div>
                            </div>
                            <div class="row text-justify">
                                <div class="col-9">
                                    <b>Antecedentes:</b>
                                    @if ($audiometria->check_proteccion_auditiva == 'on')
                                    Uso de protección auditiva actualmente,
                                    @endif
                                    @if ($audiometria->check_tce == 'on')
                                    Refiere de traumatismo craneoencefálico,
                                    @endif
                                    @if ($audiometria->tiempo_trabajando_exposicion != null)
                                    Refiere exposició a ruido laboral durante {!!
                                    $audiometria->tiempo_trabajando_exposicion !!},
                                    @endif
                                    @if ($audiometria->tiempo_puesto != null)
                                    Antigüedad de {!! $audiometria->tiempo_puesto !!} en el empleo actual,
                                    @endif
                                    @if ($audiometria->check_explosion_cercana == 'on')
                                    Ha estado en una explosión cercana, oido afectado:
                                    {!! $audiometria->ec_oido !!},
                                    @endif
                                    @if ($audiometria->check_audifonos_musica == 'on')
                                    Escucha música {!! $audiometria->am_no_dias_semana !!} día(s) a la semana, en un
                                    volumen de {!! $audiometria->am_volumen !!} durante {!!
                                    $audiometria->am_cuantos_anios !!}
                                    año(s),
                                    @endif
                                    @if ($audiometria->check_gripa_infancia == 'on')
                                    Refiere gripa frecuente en infancia,
                                    @endif
                                    @if ($audiometria->check_gripa_actualidad == 'on')
                                    Refiere gripa en la actualidad,
                                    @endif
                                    @if ($audiometria->check_infeccion_oidos == 'on')
                                    Refiere infección de oídos,
                                    @endif
                                    @if ($audiometria->check_hipertension == 'on')
                                    Refiere tener hipertensión,
                                    @endif
                                    @if ($audiometria->check_paralisis_facial == 'on')
                                    Refiere tener paralisis facial,
                                    @endif
                                    @if ($audiometria->check_diabetes == 'on')
                                    Refiere tener diabetes,
                                    @endif
                                    @if ($audiometria->check_eventos == 'on')
                                    Refiere acudir a fiestas, antros, bailes con la frecuencia de
                                    {!! $audiometria->e_frecuencia !!},
                                    @endif
                                    @if ($audiometria->check_varicela == 'on')
                                    Refiere tener o haber tenido varicela   
                                    @endif
                                    @if ($audiometria->check_practicas == 'on')
                                    Refiere practicar natación, aviación o uso de motocicleta con la
                                    frecuencia de {!! $audiometria->p_frecuencia !!},
                                    @endif
                                    @if ($audiometria->check_medicamentos == 'on')
                                    Refiere tomar {!! $audiometria->m_cual !!} frecuentemente,
                                    @endif
                                    @if ($audiometria->check_familiar_problemas == 'on')
                                    Refiere tener algún familiar con problemas de audición
                                    @endif.
                                    <br>
                                    @if(!empty($audiometria->oido_derecho_sel || $audiometria->interpretacion))
                                    <b>Interpretación:</b><br>
                                    @php
                                    $odio_derechito = explode(',', $audiometria->oido_derecho_sel);
                                    echo "OD(Oído Derecho):";
                                    foreach($odio_derechito as $od){
                                        if($od=='audiometria_normal'){
                                            echo 'Audiometría Normal a Tonos Puros';
                                        }
                                        else if($od=='audiometria_limites_normales'){
                                            echo 'Audiometría Dentro de Límites Normales a Tonos Puros';

                                        }
                                        else if($od=='hipoacusia_leve'){
                                            echo 'Hipoacusia Leve de Conducción';
                                        }
                                        else if($od=='hipoacusia_moderada'){
                                            echo 'Hipoacusia Moderada de Conducción';
                                        }
                                        else if($od=='hipoacusia_severa'){
                                            echo 'Hipoacusia Severa de Conducción';
                                        }
                                        else if($od=='hipoacusia_profunda'){
                                            echo 'Hipoacusia Profunda de Conducción';
                                        }
                                        else if($od=='hipoacusia_leve_mixta'){
                                            echo 'Hipoacusia Leve Mixta';
                                        }
                                        else if($od=='hipoacusia_moderada_mixta'){
                                            echo 'Hipoacusia Moderada Mixta';
                                        }
                                        else if($od=='hipoacusia_severa_mixta'){
                                            echo 'Hipoacusia Severa Mixta';
                                        }
                                        else if($od=='hipoacusia_profunda_mixta'){
                                            echo 'Hipoacusia Profunda Mixta';
                                        }
                                        else if($od=='hipoacusia_leve_Neurosensorial'){
                                            echo 'Hipoacusia Leve Neurosensorial';
                                        }
                                        else if($od=='hipoacusia_moderada_Neurosensorial'){
                                            echo 'Hipoacusia Moderada Neurosensorial';
                                        }
                                        else if($od=='hipoacusia_severa_Neurosensorial'){
                                            echo 'Hipoacusia Severa Neurosensorial';
                                        }
                                        else if($od=='hipoacusia_profunda_Neurosensorial'){
                                            echo 'Hipoacusia Profunda Neurosensorial';
                                        }
                                        else if($od=='cofosis'){
                                            echo "Cofosis (Deficiencia Auditiva Total)";

                                        }
                       
                                      echo ',';
                                    }
                                    echo "</br>";
                                    $odio_izquierdo = explode(',', $audiometria->oido_izquierdo_sel);
                                    echo "OI(Oído Izquierdo):";
                                    foreach($odio_izquierdo as $od){
                                        if($od=='audiometria_normal'){
                                            echo 'Audiometría Normal a Tonos Puros';
                                        }
                                        else if($od=='audiometria_limites_normales'){
                                            echo 'Audiometría Dentro de Límites Normales a Tonos Puros';

                                        }
                                        else if($od=='hipoacusia_leve'){
                                            echo 'Hipoacusia Leve de Conducción';
                                        }
                                        else if($od=='hipoacusia_moderada'){
                                            echo 'Hipoacusia Moderada de Conducción';
                                        }
                                        else if($od=='hipoacusia_severa'){
                                            echo 'Hipoacusia Severa de Conducción';
                                        }
                                        else if($od=='hipoacusia_profunda'){
                                            echo 'Hipoacusia Profunda de Conducción';
                                        }
                                        else if($od=='hipoacusia_leve_mixta'){
                                            echo 'Hipoacusia Leve Mixta';
                                        }
                                        else if($od=='hipoacusia_moderada_mixta'){
                                            echo 'Hipoacusia Moderada Mixta';
                                        }
                                        else if($od=='hipoacusia_severa_mixta'){
                                            echo 'Hipoacusia Severa Mixta';
                                        }
                                        else if($od=='hipoacusia_profunda_mixta'){
                                            echo 'Hipoacusia Profunda Mixta';
                                        }
                                        else if($od=='hipoacusia_leve_Neurosensorial'){
                                            echo 'Hipoacusia Leve Neurosensorial';
                                        }
                                        else if($od=='hipoacusia_moderada_Neurosensorial'){
                                            echo 'Hipoacusia Moderada Neurosensorial';
                                        }
                                        else if($od=='hipoacusia_severa_Neurosensorial'){
                                            echo 'Hipoacusia Severa Neurosensorial';
                                        }
                                        else if($od=='hipoacusia_profunda_Neurosensorial'){
                                            echo 'Hipoacusia Profunda Neurosensorial';
                                        }
                                        else if($od=='cofosis'){
                                            echo "Cofosis (Deficiencia Auditiva Total)";

                                        }
                       
                                      echo ',';
                                    }
                                    @endphp
                                    @endif
                                    
                                  
                                  @if(!empty($audiometria->interpretacion))  
                                    <br><b>Comentarios:</b>
                                        {!! $audiometria->interpretacion !!}
                                    </div>
                                  @endif
                                <div class="col-3 text-center align-self-center">
                                    <img src="{!! asset('storage/app/datosHtds/' . $user_htds->firma) !!}"
                                        style="width: 200px;">
                                    <br>
                                    {!! $user_htds->titulo_profesional !!}. {!! $user_htds->nombre_completo !!}
                                    <br>
                                    Cedula Profesional {!! $user_htds->cedula !!}
                                </div>

                            </div>
                        </div>
                        @else
                        <div class="col-md-12 mt-3">
                            <div class="alert m-0 p-0 alert-primary text-center">
                                Aún no se sube la interpretación y el resultado gráfico
                            </div>
                        </div>
                        @endif
                        @else
                        {{-- <div class="row">
                            <div class="alert alert-danger text-center col-md-12" role="alert">
                                No Cuenta con ningún estudio realizado de Audiometria
                            </div>
                        </div> --}}
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
    {{--
</div> --}}
<div id="container"></div>
@endsection

@section('page_vendor_js')
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
@endsection

@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
{{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script>
    //     function imprSelec() {
        //         var boton = document.getElementById("bim");
        //         boton.style.display = 'none';
        //         $('.bim').hide();
        //         window.print();
        // }
        function printDiv(nombreDiv) {
            var contenido = document.getElementById(nombreDiv).innerHTML;
            var contenidoOriginal = document.body.innerHTML;

            document.body.innerHTML = contenido;

            window.print();

            document.body.innerHTML = contenidoOriginal;
        }
        var chart = null;
        $(document).ready(function() {


            var array_oido_izquierdo = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            var array_oido_derecho = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            var array_oido_izquierdo_v = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            var array_oido_derecho_v = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            var audiometria_id = $("#audiometria_id").val();
            var estatus=0;
            $.ajax({
                type: "GET",
                url: "../../Audiometria/getResultadosAudiometria/" + audiometria_id + "/izquierdo",
                dataType: "json",
            }).done(res => {
                array_oido_izquierdo = [];
                array_oido_izquierdo_v = [];
                res.forEach(element => {

                    array_oido_izquierdo.push(parseInt(element.desibelios));
                    $("#oido_izquierdo_" + element.frecuencia).val(element.desibelios)
                    array_oido_izquierdo_v.push(parseInt(element.desibelios_v));
                    $("#VO #oido_izquierdo_" + element.frecuencia_v).val(element.desibelios_v)
                });
                // initGrafica();
                // initGrafica2();
                graf();
            }).fail(err => {
                console.log(err);
            });

            $.ajax({
                type: "GET",
                url: "../../Audiometria/getResultadosAudiometria/" + audiometria_id + "/derecho",
                dataType: "json",
            }).done(res => {
                array_oido_derecho = [];
                array_oido_derecho_v = [];
                res.forEach(element => {
                    array_oido_derecho.push(parseInt(element.desibelios));
                    $("#oido_derecho_" + element.frecuencia).val(element.desibelios)
                    array_oido_derecho_v.push(parseInt(element.desibelios_v));
                    if(element.desibelios_v!='0'){
                        estatus=1;
                    }
                    $("#oido_derecho_" + element.frecuencia_v).val(element.desibelios_v)
                });
                // initGrafica();
                // initGrafica2();
                graf(estatus);
            }).fail(err => {
                console.log(err);
            });

            // function initGrafica() {
            //     let series = [{
            //             name: "Oído izquierdo",
            //             data: array_oido_izquierdo,
            //         },
            //         {
            //             name: "Oído derecho",
            //             data: array_oido_derecho
            //         }
            //     ];
            //     chart.updateSeries(series, true);
            // }

            // function initGrafica2() {
            //     let series = [{
            //             name: "Oído izquierdo VO",
            //             data: array_oido_izquierdo_v,
            //         },
            //         {
            //             name: "Oído derecho VO",
            //             data: array_oido_derecho_v
            //         }
            //     ];
            //     chart2.updateSeries(series, true);
            // }
            // var options = {
            //     series: [{
            //             name: "Oído izquierdo",
            //             data: array_oido_izquierdo,
            //         },
            //         {
            //             name: "Oído derecho",
            //             data: array_oido_derecho
            //         }
            //     ],
            //     chart: {
            //         height: 400,
            //         width: 550,
            //         type: 'line',
            //         zoom: {
            //             enabled: false
            //         },
            //         toolbar: {
            //             show: false
            //         }
            //     },
            //     colors: ["#001eff", "#fc0000"],
            //     dataLabels: {
            //         enabled: false
            //     },
            //     stroke: {
            //         curve: 'straight',
            //         colors: ["#001eff", "#fc0000"],
            //         width: 3,
            //     },
            //     legend: {
            //         markers: {
            //             width: 12,
            //             height: 12,
            //             strokeWidth: 1,
            //             strokeColor: ["#001eff", "#fc0000"],
            //             fillColor: ["#001eff", "#fc0000"],
            //             radius: 12,
            //             offsetX: 0,
            //             offsetY: 0
            //         },
            //     },
            //     grid: {
            //         row: {
            //             colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            //             opacity: 1
            //         },
            //         column: {
            //             colors: ['transparent', '#f3f3f3'], // takes an array which will be repeated on columns
            //             opacity: 0.5
            //         },
            //     },
            //     xaxis: {
            //         categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
            //         position: "top",
            //         offsetX: 0,
            //         offsetY: -0,
            //         labels: {
            //             show: true,
            //             hideOverlappingLabels: true,
            //             // offsetY:-15,
            //         }
            //     },
            //     yaxis: {
            //         reversed: true,
            //         tickAmount: 10,
            //         forceNiceScale: true,
            //         min: -10,
            //         max: 120,
            //     }
            // };
            // var options1 = {
            //     series: [{
            //             name: "Oído izquierdo VO",
            //             data: array_oido_izquierdo_v
            //         },
            //         {
            //             name: "Oído derecho VO",
            //             data: array_oido_derecho_v
            //         }
            //     ],
            //     chart: {
            //         height: 400,
            //         width: 550,
            //         type: 'line',
            //         zoom: {
            //             enabled: false
            //         },
            //         toolbar: {
            //             show: false
            //         }
            //     },
            //     colors: ["#001eff", "#fc0000"],
            //     dataLabels: {
            //         enabled: false
            //     },
            //     stroke: {
            //         curve: 'straight',
            //         colors: ["#001eff", "#fc0000"],
            //         width: 3,
            //     },
            //     legend: {
            //         markers: {
            //             width: 12,
            //             height: 12,
            //             strokeWidth: 1,
            //             strokeColor: ["#001eff", "#fc0000"],
            //             fillColor: ["#001eff", "#fc0000"],
            //             radius: 12,
            //             offsetX: 0,
            //             offsetY: 0
            //         },
            //     },
            //     grid: {
            //         row: {
            //             colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            //             opacity: 1
            //         },
            //         column: {
            //             colors: ['transparent', '#f3f3f3'], // takes an array which will be repeated on columns
            //             opacity: 0.5
            //         },
            //     },
            //     xaxis: {
            //         categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
            //         position: "top",
            //         offsetX: 0,
            //         offsetY: -0,
            //         labels: {
            //             show: true,
            //             hideOverlappingLabels: true,
            //             // offsetY:-15,
            //         }
            //     },
            //     yaxis: {
            //         reversed: true,
            //         tickAmount: 10,
            //         forceNiceScale: true,
            //         min: -10,
            //         max: 120,
            //     }
            // };

            // chart = new ApexCharts($("#line-chart2")[0], options);
            // chart.render();
            // chart2 = new ApexCharts($("#line-chart3")[0], options1);
            // chart2.render();

            $("#crearPdf").click(function(e) {
                e.preventDefault();
                var img_temp = "";
                var dataURL = chart.dataURI().then(({
                    imgURI,
                    blob
                }) => {
                    $.ajax({
                        type: "POST",
                        url: "../Audiometria/guardarFinal",
                        dataType: "json",
                        data: {
                            audiometria_id: audiometria_id,
                            imagen: imgURI
                        }
                    }).done(res => {
                        url = $(this).attr("href");
                        window.open(url, '_blank');
                        return false;
                    }).fail(err => {
                        console.log(err);
                    });
                });
            })
            function graf(esta){
                Highcharts.SVGRenderer.prototype.symbols.cross = function (x, y, w, h) {
                    return ['M', x, y, 'L', x + w, y + h, 'M', x + w, y, 'L', x, y + h, 'z'];
                };
                if (Highcharts.VMLRenderer) {
                    Highcharts.VMLRenderer.prototype.symbols.cross = Highcharts.SVGRenderer.prototype.symbols.cross;
                }
                Highcharts.setOptions({
                colors: ['#ff0000', '#0000FF']
                });

                Highcharts.chart('AVA', {

                    title: {
                        text: 'AUDIOMETRÍA VÍA AÉREA'
                    },

                    xAxis: {
                        categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
                        position: "top",
                        offsetX: 0,
                        offsetY: -0,
                                
                    },
                    
                    yAxis: {
                        max: 120,
                        min: -10,
                        reversed: true,
                        tickInterval: 10,
                    },

                    series: [{
                        name: "Oído derecho VA",
                        data: array_oido_derecho,
                        marker: {
                            symbol: 'url(http://has-humanly.com/cemex/storage/images/circulo.png)',
                            width: 20,
                            height: 20
                        }
                    },{
                        name: "Oído izquierdo VA",
                        data: array_oido_izquierdo,
                        marker: {
                            symbol: 'url(http://has-humanly.com/cemex/storage/images/equis.png)',
                            width: 20,
                            height: 20
                        }
                    }],

                    credits: {
                        enabled: false
                    },

                    subtitle: {
                        text: '',
                        verticalAlign: 'bottom',
                        align: 'right',
                        style: {
                            fontSize: '10px'
                        }
                    }
                });
                if(esta=='1'){
                Highcharts.chart('AVO', {

                        title: {
                            text: 'AUDIOMETRÍA VÍA ÓSEA'
                        },

                        xAxis: {
                            categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
                            position: "top",
                            offsetX: 0,
                            offsetY: -0,
                                    
                        },

                        yAxis: {
                            max: 120,
                            min: -10,
                            reversed: true,
                            tickInterval: 10,
                        },

                        series: [{
                            name: "Oído derecho VO",
                            data: array_oido_derecho_v,
                            dashStyle: 'Dash',
                            marker: {
                                symbol: 'url(http://has-humanly.com/cemex/storage/images/menorque.png)',
                                width: 25,
                                height: 25
                                
                            }
                        },{
                            name: "Oído izquierdo VO",
                            data: array_oido_izquierdo_v,
                            marker: {
                                symbol: 'url(http://has-humanly.com/cemex/storage/images/mayorque.png)',
                                width: 25,
                                height: 25
                            },
                            dashStyle: 'Dash',
                        }],

                        credits: {
                            enabled: false
                        },

                        subtitle: {
                            text: '',
                            verticalAlign: 'bottom',
                            align: 'right',
                            style: {
                                fontSize: '10px'
                            }
                        }
                });
                }
            }
        });



       
</script>
@endsection