<!DOCTYPE html>
<html>

<head>

    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .letra {
            font-size: 12px !important;
        }

        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }

        .list_none {
            list-style: none;
        }
    </style>
</head>

<body>
    <div class="content letra">
        <div class="text-center mb-1">
            <h4 class="font-weight-normal" style="font-size:20px">HISTORIA CLÍNICA</h4>
        </div>
        <div id="identificacion">

            <div class="p-0 text-center fondo_titulos">
                <b>FICHA DE IDENTIFICACIÓN</b>
            </div>


            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>FECHA (Día/Mes/Año): {{ \Carbon::parse($identificacion->fecha)->format('d/m/Y') }}</td>
                        <td>N° de Empleado: {{ $identificacion->numEmpleado }}{{ $empleado->curp }}</td>
                        <td>Departamento: {{ $identificacion->departamento }}</td>
                        <td>Edad: {{ \Carbon::parse($empleado->fecha_nacimiento)->age }}</td>
                        <td>SEXO {{ $empleado->genero }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">APELLIDO PATERNO: {{ $empleado->apellido_paterno }}</td>
                        <td colspan="1">APELLIDO MATERNO: {{ $empleado->apellido_materno }}</td>
                        <td colspan="2">NOMBRE (S): {{ $empleado->nombre }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">ESTADO CIVIL: {{ $identificacion->estadoCivil }}</td>
                        <td colspan="2">ESCOLARIDAD: {{ $identificacion->escolaridad }}</td>
                    </tr>
                    <tr>
                        <td colspan="6">DOMICILIO: {{ $identificacion->domicilio }}</td>
                    </tr>
                    <tr>
                        <td colspan="2"> <span class="">LUGAR DE NACIMIENTO:
                                {{ $identificacion->lugarNacimiento }}</span> </td>
                        <td colspan="3">FECHA DE NACIMIENTO: <br>{{ \Carbon::parse($empleado->fecha_nacimiento
                            )->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td>CIUDAD Ó EJIDO: {{ $identificacion->ciudad }}</td>
                        <td>MUNICIPIO: {{ $identificacion->municipio }}</td>
                        <td colspan="3">ESTADO: {{ $identificacion->estado }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">EN CASO DE EMERGENCIA LLAMAR A: {{ $identificacion->nom_per_em }}</td>
                        <td colspan="2">PARENTESCO: {{ $identificacion->parentesco }}</td>
                        <td colspan="2">TELEFONO: {{ $identificacion->telefono_1 }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">DOMICILIO: {{ $identificacion->domicilio_em }}</td>
                        <td colspan="2">LUGAR DE TRABAJO:</td>
                        <td>TELEFONO: {{ $identificacion->telefono_2 }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="heredofamiliares">
            <div class="text-center fondo_titulos">
                ANTECEDENTES HEREDOFAMILIARES
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <th class="text-center fondo_titulos">FAMILIAR</th>
                        <th class="text-center fondo_titulos">ESTADO</th>
                        <th class="text-center fondo_titulos">ENFERMEDADES</th>
                    </tr>
                    @php
                    $heredofamilia = json_decode($heredofamiliar->data);
                    @endphp
                    @foreach ($heredofamilia as $familiar)
                    <tr>
                        <td>{{ $familiar->familiar }}</td>
                        <td>{{ $familiar->estado }}</td>
                        <td>
                            @if ($familiar->enfermedad != null)
                            @foreach ($familiar->enfermedad as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @else
                            Ninguno
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="nopatologico">
            @php
            $vacunas = $nopatologico->vacunas;
            @endphp
            <div class="text-center fondo_titulos">
                ANTECEDENTES PERSONALES NO PATOLÓGICOS
            </div>

            <table class="table table-sm table-bordered">
                <tr>
                    <th colspan="4" style="">PREGUNTAS</th>
                    <th>Si</th>
                    <th>No</th>
                </tr>
                <tr>
                    <td colspan="4">¿CUENTA CON PISO DE CEMENTO?</td>
                    <td>
                        @if($nopatologico->piso_cemento=="si")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                    <td>
                        @if($nopatologico->piso_cemento=="no")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4">¿SU CASA CUENTA CON DRENAJE?</td>
                    <td>
                        @if($nopatologico->drenaje=="si")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                    <td>
                        @if($nopatologico->drenaje=="no")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4">¿SU CASA CUENTA CON AGUA POTABLE?</td>
                    <td>
                        @if($nopatologico->agua_potable=="si")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                    <td>
                        @if($nopatologico->agua_potable=="no")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4">¿SU CASA CUENTA CON LUZ ELÉTRICA?</td>
                    <td>
                        @if($nopatologico->luz=="si")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                    <td>
                        @if($nopatologico->luz=="no")
                        <p class="text-center">X</p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4">NÚMERO DE HABITANTES</td>
                    <td colspan="2">
                        {{$nopatologico->n_habitantes}}
                    </td>

                </tr>
                <tr>
                    <td colspan="4">NÚMERO DE HABITACIONES</td>
                    <td colspan="2">
                        {{$nopatologico->n_habitaciones}}
                    </td>

                </tr>
                <tr>
                    <td colspan="4">NÚMERO DE MASCOTAS</td>
                    <td colspan="2">
                        {{$nopatologico->n_mascotas}}
                    </td>

                </tr>
                <tr>
                    <td colspan="4">¿CUÁLES SON SUS MASCOTAS?</td>
                    <td colspan="2">
                        {{$nopatologico->mascotas}}
                    </td>

                </tr>


            </table>
            <div style="page-break-after:always;"></div>
            <div class="text-center fondo_titulos">
                HÁBITOS HIGIÉNICO-DIETÉTICOS
            </div>
            <table class="table table-sm table-bordered">
                <tr>
                    <td colspan="4">BAÑO</td>
                    <td colspan="2">
                        {{$nopatologico->h_higienicos}}
                    </td>

                </tr>
                <tr>
                    <td colspan="4">LAVADO DE DIENTES</td>
                    <td colspan="2">
                        {{$nopatologico->lavado_dientes}}
                    </td>

                </tr>
                <tr>
                    <td colspan="4">ALIMENTACIÓN DIARIA</td>
                    <td colspan="2">
                        {{$nopatologico->alimentacion_diaria}}
                    </td>

                </tr>
            </table>
            <div class="text-center fondo_titulos">
                USO DEL CIGARRO
            </div>
            @php
            $cigarro = $nopatologico->cigarro;
            $alcohol = $nopatologico->alcohol;
            $drogas = $nopatologico->drogas;
            $du = $nopatologico->drogas_usadas;
            $deporte = $nopatologico->deporte;
            $dieta = $nopatologico->dieta;

            @endphp
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A) ¿FUMA USTED? {{ $cigarro->Cigarro }}</td>
                        <td>B) ¿DESDE QUE EDAD?<br> {{ $cigarro->edad }}</td>
                        <td>C) ¿NUMERO PROMEDIO DE CIGARROS QUE FUMA?<br> {{ $cigarro->frecuencia }}</td>
                        <td>D) ¿A QUE EDAD DEJÓ DE FUMAR?<br> {{ $cigarro->noConsumir }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="text-center fondo_titulos">
                USO DEL ALCOHOL
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A) ¿TOMA BEBIDAS ALCOHÓLICAS?{{ $alcohol->Alcohol }}</td>
                        <td>B) ¿DESDE QUE EDAD? <br> {{ $alcohol->edad }}</td>
                        <td colspan="2">C) ¿CON QUE FRECUENCIA Y CANTIDAD? <br> {{ $alcohol->frecuencia }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="text-center fondo_titulos">
                USO DE DROGAS
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A) ¿ALGUNA VEZ USÓ DROGAS? {{ $drogas->Drogas }}</td>
                        <td>B) ¿DESDE QUE EDAD? {{ $drogas->edad }}</td>
                        <td colspan="2">C) ¿CON QUE FRECUENCIA Y CANTIDAD? {{ $drogas->frecuencia }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">MENCIONE QUE DROGAS HA USADO:
                            @if ($du != null)
                            @foreach ($du as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @else
                            Ninguna
                            @endif
                        </td>

                        <td>
                            ¿SE LE HA INDICADO ALGUNA DIETA? {{ $dieta->Dieta . ' ' . $dieta->descripcion }}
                        </td>
                        <td>
                            ¿PRACTICA USTED ALGUN DEPORTE? {{ $deporte->Deporte }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">ESPECIFIQUE CUAL Y FRECUENCIA: {{ $deporte->frecuencia }}</td>
                        <td colspan="2">INDIQUE CUÁL ES SU PASATIEMPO FAVORITO: {{ $nopatologico->pasatiempo }}</td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div id="patologico">
            @php
            $enfermedades = $patologico->enfermPadecidas;
            $cl = $patologico->cirugiasLesion;
            @endphp
            @if($admision_contratistas!=null && $formato=='admision_contratista')
            <div class="text-center" style="background-color:#000 !important;color:#fff;">
                ANTECEDENTES PATOLÓGICOS PERSONALES
            </div>
            @else
            <div class="text-center fondo_titulos">
                ANTECEDENTES PATOLÓGICOS PERSONALES
            </div>
            @endif

            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>ENFERMEDADES PADECIDAS:</td>
                        <td colspan="2">
                            @if ($enfermedades == null)
                            Ninguna
                            @else
                            @foreach ($enfermedades as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="fondo_titulos text-center letra">DESCRIPCIÓN</th>
                        <th scope="col" class="fondo_titulos text-center letra">CUÁL</th>
                        <th scope="col" class="fondo_titulos text-center letra">SI</th>
                        <th scope="col" class="fondo_titulos text-center letra">NO</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>TRANSTORNOS EMOCIONALES O PSIQUIATRICOS, EJEMPLOS ANSIDEDAD, DEPRESIÓN</td>
                        <td>{{ $patologico->transtornos }}</td>
                        <td>@if(!empty($patologico->transtornos))
                            X
                            @endif</td>
                        <td>@if(empty($patologico->transtornos))
                            X
                            @endif</td>
                    </tr>
                    <tr>
                        <td>ALERGIAS A MEDICAMENTOS: </td>
                        <td>{{ $patologico->alergiaMedica }}</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>ALERGIAS EN LA PIEL O SENSIBILIDAD:</td>
                        <td> {{ $patologico->alergiaPiel }}</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>OTRO TIPO DE ALERGIAS: </td>
                        <td> {{ $patologico->alergiaOtro }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>TUMORES O CANCER: </td>
                        <td> {{ $patologico->tumorCancer }}</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td>PROBLEMAS DE LA VISTA: </td>
                        <td> {{ $patologico->probVista }}</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>

                        <td>ENFERMEDAD DEL OIDO:</td>
                        <td> {{ $patologico->enfOido }}</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>

                        <td>PROBLEMA EN COLUMNA VERTEBRAL: </td>
                        <td> {{ $patologico->probCulumVert }}</td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>

                        <td>HUESOS Y ARTICULACIONES: </td>
                        <td> {{ $patologico->huesoArticulacion }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3">Otros problemas médicos no enlistados:</td>
                        <td colspan="3"> {{ $patologico->otroProbMedico }}</td>


                    </tr>
                </tbody>
            </table>
            {{-- <p class="text-center"><b>INDIQUE SI HA TENIDO ALGUNA DE LAS LESIONES O CIRUGIAS SIGUIENTES:</b></p>
            <hr>
            <table class="table table-sm table-bordered">
                <tbody>
                    @foreach ($cl as $json)
                    <tr>
                        @foreach ($json as $key => $value)
                        <td>{{ strtoupper($key) }}</td>
                        <td>{{ strtoupper($value) }}</td>
                        @endforeach
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="1">OTRAS CIRUGÍAS INCLUYENDO ACCIDENTES AUTOMOVILÍSTICOS O DEL TRABAJO:</td>
                        <td colspan="3">{{ $patologico->otra_cirugia }}</td>
                    </tr>
                </tbody>
            </table> --}}
        </div>
        @if ($empleado->genero == 'FEMENINO' || $empleado->genero == 'Femenino')
        <div id="genicoobstetrico">
            <div class="text-center fondo_titulos">
                ANTECEDENTES GINECOOBSTETRICOS
            </div>


            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>¿A QUÉ EDAD INICIÓ SU REGLA?</td>
                        <td>{{ $genicoobstetrico->inicioRegla }}</td>
                        <td>¿FRECUENCIA DE REGLA?</td>
                        <td>{{ $genicoobstetrico->fRegla }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNTOS DÍAS DURA SU REGLA?</td>
                        <td>{{ $genicoobstetrico->duraRegla }}</td>
                        <td>¿EDAD QUE INICIÓ SUS RELACIONES SEXUALES?</td>
                        <td>{{ $genicoobstetrico->relacionSexInicio }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁL MÉTODO DE PLANIFICACIÓN HA USADO?</td>
                        <td>{{ $genicoobstetrico->metodoUsado }}</td>
                        <td>¿CUÁL FUE LA FECHA DE SU ÚLTIMA MENSTRUACION?</td>
                        <td>{{ $genicoobstetrico->ultimaMestruacion }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNTOS EMBARAZOS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numEmbarazos }}</td>
                        <td>¿CUANTOS PARTOS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numPartos }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNTAS CESÁREAS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numCesareas }}</td>
                        <td>¿CUÁNTOS ABORTOS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numAbortos }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNDO SE REALIZÓ EL ÚLTIMO EXÁMEN DEL CÁNCER DE MATRIZ?</td>
                        <td>{{ $genicoobstetrico->fecha_examenCancer }}</td>
                        <td> ¿CUÁNDO SE REALIZÓ EL ÚLTIMO EXÁMEN DE MAMAS?</td>
                        <td>{{ $genicoobstetrico->fecha_examenMamas }}</td>
                    </tr>
                    <tr>
                        <td>¿A QUÉ EDAD TUVO SU MENOPAUSIA?</td>
                        <td class="border-right">{{ $genicoobstetrico->edadMenopausia }}</td>
                        <td> FECHA DE ÚLTIMO PAPANICOLAOU</td>
                        <td>{{ $genicoobstetrico->papanicolaou }}</td>

                    </tr>
                    <tr>
                        <td>MPF</td>
                        <td class="border-right">{{ $genicoobstetrico->MPF }}</td>
                        <td> ¿ESTÁ EMBARAZADA?</td>
                        <td>{{ $genicoobstetrico->embarazada }}</td>

                    </tr>
                    <tr>
                        <td>¿DOLOR DURANTE LA MENSTRUACIÓN?</td>
                        <td class="border-right">{{ $genicoobstetrico->do_menstruacion }}</td>
                        <td>ENFERMEDAD DE MAMA</td>
                        <td>{{ $genicoobstetrico->enf_mama }}</td>

                    </tr>
                    <tr>
                        <td>ENFERMEDAD DE UTERO</td>
                        <td class="border-right">{{ $genicoobstetrico->enf_utero }}</td>
                    </tr>

                </tbody>
            </table>
        </div>
        @endif
        @if($formato!='admision_contratista')
        <div id="historialaboral">
            @php
            $hl = $historiaLaboral->trabajoRealizados;
            $tm = $historiaLaboral->trabajoMateriales;
            $empleos = $historiaLaboral->empleos;
            $equipo = $historiaLaboral->equipoSegActual;
            $vista = $historiaLaboral->examenVista;
            $lentes = $historiaLaboral->lentesContacto;
            $lugares = $historiaLaboral->lugaresVividos;
            @endphp
            <div class="text-center fondo_titulos">
                ANTECEDENTES LABORALES
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>
                            EDAD INICIO LABORAL:
                        </td>
                        <td>
                            {{ $historiaLaboral->edad_inicio_laboral }} Años
                        </td>

                    </tr>
                    <tr>
                        <td>
                            TIEMPO EN EL PUESTO:
                        </td>
                        <td>
                            {{ $historiaLaboral->tiempo_puesto }}
                        </td>
                        <td>
                            HORAS DEL TRABAJO AL DIA:
                        </td>
                        <td>
                            {{ $historiaLaboral->horas_trabajo }}
                        </td>


                    </tr>
                    <tr>
                        <td>
                            GRADO DE ESTUDIO:
                        </td>
                        <td colspan="4">
                            {{ $historiaLaboral->grados_estudio }}

                        </td>
                    </tr>
                    <tr>
                        <td>
                            ÁREA DE TRABAJO:
                        </td>
                        <td>
                            @if ($hl == null)
                            Ninguno
                            @else
                            @foreach ($hl as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @endif
                        </td>
                        <td>OTROS TRABAJADOS REALIZADOS:</td>
                        <td>{{ $historiaLaboral->otrosTrabajos }}</td>
                    </tr>
                    <tr>
                        <td>ACCIDENTES EN EL TRABAJO:</td>
                        <td>{{$historiaLaboral->accidentes_t}}</td>
                        <td>ENFERMEDADES EN EL TRABAJO:</td>
                        <td>{{$historiaLaboral->enfermedades_t}}</td>
                    </tr>
                    <tr>
                        <td>SE HA EXPUESTO O HA TRABAJADO CON ALGUNO DE ESTOS MATERIALES:</td>
                        <td>
                            @if ($tm == null)
                            Ninguno
                            @else
                            @foreach ($tm as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @endif
                        </td>
                        <td>Otras exposiciones:</td>
                        <td>{{ $historiaLaboral->otrasExposiciones }}</td </tr>
                </tbody>
            </table> @foreach ($empleos as
            $empleo) <div class="text-center fondo_titulos">
                {{-- @if ($empleo->tipo == 1)
                EMPLEO ACTUAL
                @elseif ($empleo->tipo==2)
                EMPLEO ANTERIOR
                @elseif ($empleo->tipo==3)
                EMPLEO ANTERIOR
                @endif --}}
                </tr>
                </tbody>
            </div>



            @endforeach
            <table class="table table-sm table-bordered">

                <tbody>
                    <tr>
                        <td>¿REALIZA SU TRABAJO NORMALMENTE DE PIE?: {{ $historiaLaboral->trabajo_pie }}</td>
                        <td>¿REALIZA SU TRABAJO GENERALMENTE SENTADO?: {{ $historiaLaboral->trabajo_sentado }}</td>
                    </tr>
                    <tr>
                        <td>¿REALIZA SU TRABAJO NORMALMENTE DE PIE CON ESFUERZO FÍSICO?: {{
                            $historiaLaboral->trabajo_pie_fisico }}</td>
                        <td>¿REALIZA SU TRABAJO GENERALMENTE DE PIE CAMINANDO?: {{
                            $historiaLaboral->trabajo_pie_caminando }}</td>
                    </tr>
                    <tr>
                        <td>¿USA HERRAMIENTAS MANUALES?: {{ $historiaLaboral->herramientas_manuales }}</td>
                        <td>¿UTILIZA MAQUINAS?: {{ $historiaLaboral->maquinas }}</td>
                    </tr>

                </tbody>

            </table>
            <div class="text-center fondo_titulos">
                SINTOMAS LABORALES
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>ACUFENOS: {{ $historiaLaboral->ACUFENOS }}</td>
                        <td>OTALGIA: {{ $historiaLaboral->OTALGIA }}</td>
                    </tr>
                    <tr>
                        <td>PRURITO OTICO: {{ $historiaLaboral->PRURITO_OTICO }}</td>
                        <td>PLENITUD OTICA: {{ $historiaLaboral->PLENITUD_OTICA }}</td>
                    </tr>
                    <tr>
                        <td>TOS PRODUCTIVA : {{ $historiaLaboral->TOS_PRODUCTIVA }}</td>
                        <td>DISNEA: {{ $historiaLaboral->DISNEA }}</td>
                    </tr>
                    <tr>
                        <td>¿SU TRABAJO LE OCASIONA TENSIÓN EMOCIONAL? : {{ $historiaLaboral->tension_e }}</td>
                        <td>¿USA EQUIPO DE PROTECCIÓN INDIVIDUAL? : {{ $historiaLaboral->proteccion_in }}</td>
                    </tr>
                    <tr>
                        <td>¿CUENTA CON MEDIOS ADECUADOS DE PROTECCIÓN? : {{ $historiaLaboral->adecuados_pro }}</td>
                        <td>¿EN QUÉ DEDICA SU TIEMPO EXTRA LABORAL? : {{ $historiaLaboral->extralaboral }}</td>
                    </tr>


                </tbody>
            </table>


        </div>
        @endif
        <div class="text-center fondo_titulos">
            EXPLORACIÓN FÍSICA
        </div>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>TEMPERATURA: {{ $examen->Temperatura }}</td>
                    <td>TALLA: {{ $examen->Talla }}</td>
                </tr>
                <tr>
                    <td>PESO: {{ $examen->Peso }}</td>
                    <td>IMC: {{ $examen->IMC }}</td>
                </tr>
                <tr>
                    <td>PERÍMETRO ABDOMINAL: {{ $examen->p_abdominal }}</td>
                    <td>PERÍMETRO TORÁCICO: {{ $examen->p_toracico }}</td>
                </tr>
                <tr>
                    <td>PERÍMETRO ABDOMINAL: {{ $examen->p_abdominal }}</td>
                    <td>PERÍMETRO TORÁCICO: {{ $examen->p_toracico }}</td>
                </tr>
            </tbody>
        </table>
        <p class="text-center">SGINOS VITALES</p>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>T.A.: {{ $examen->TA }}</td>
                    <td>F.C.: {{ $examen->FC }}</td>
                </tr>
                <tr>
                    <td>F.R.: {{ $examen->FR }}</td>
                    <td>SAT O2: {{ $examen->SatO2 }}</td>
                </tr>
            </tbody>
        </table>
        <p class="text-center">INSPECCIÓN GENERAL</p>
        <p class="text-center">AGUDEZA VISUAL</p>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <th>
                        OJO DERECHO
                    </th>
                    <th>
                        OJO IZQUIERDO
                    </th>
                </tr>

                <tr>
                    <td>SIN LENTES: {{ $examen->SL }}</td>
                    <td>SIN LENTES: {{ $examen->SLI }}</td>
                </tr>
                <tr>
                    <td>CON LENTES: {{ $examen->CL }}</td>
                    <td>CON LENTES: {{ $examen->CLI }}</td>
                </tr>
                <tr>
                    <td colspan="6">VISIÓN DE COLORES: {{ $examen->vision_colores }}</td>

                </tr>
                <tr>
                    <td colspan="6">NARIZ: {{ $examen->Nariz }}</td>
                </tr>
            </tbody>
        </table>

        <p class="text-center">OÍDOS</p>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <th>
                        OÍDO DERECHO
                    </th>
                    <th>
                        OÍDO IZQUIERDO
                    </th>
                </tr>

                <tr>
                    <td>AGUDEZA AUDITIVA: {{ $examen->agueza_aud_d }}</td>
                    <td>AGUDEZA AUDITIVA: {{ $examen->agueza_aud_i }}</td>
                </tr>
                <tr>
                    <td>CONDUCTOS AUDITIVOS: {{ $examen->con_auditivos_d }}</td>
                    <td>CONDUCTOS AUDITIVOS: {{ $examen->con_auditivos_i }}</td>
                </tr>
                <tr>
                    <td>MEMBRANA TIMPÁNICA: {{ $examen->mem_tim_d }}</td>
                    <td>MEMBRANA TIMPÁNICA: {{ $examen->mem_tim_i }}</td>
                </tr>

            </tbody>
        </table>
        <hr>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>CAVIDAD ORAL: {{ $examen->cav_oral }}</td>
                    <td>ORAFARINGE: {{ $examen->orofaringe }}</td>
                </tr>
                <tr>
                    <td>AMIGDALAS: {{ $examen->Amigdalas }}</td>
                    <td>LENGUA: {{ $examen->Lengua_f002 }}</td>
                </tr>
                <tr>
                    <td>CUELLO: {{ $examen->Cuello }}</td>
                    <td>OBSERVACIONES CUELLO: {{ $examen->cu_observaciones }}</td>
                </tr>
                <tr>
                    <td>CAMPOS PULMONARES: {{ $examen->ca_pulmonares }}</td>
                    <td>OBSERVACIONES CAMPOS PULMONARES: {{ $examen->ca_pulmonares_observaciones }}</td>
                </tr>
                <tr>
                    <td>MOVIMIENTOS RESPIRATORIOS: {{ $examen->mo_respiratorios }}</td>
                    <td>OBSERVACIONES MOVIMIENTOS RESPIRATORIOS: {{ $examen->mo_respiratorios_observaciones }}</td>
                </tr>
                <tr>
                    <td>RUIDOS CARDIACOS: {{ $examen->ruicardi_observaciones }}</td>
                    <td>OBSERVACIONES RUIDOS CARDIACOS: {{ $examen->ruicardi_observaciones }}</td>
                </tr>


            </tbody>
        </table>
        <p class="text-center">ABDOMEN</p>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>INSPECCIÓN: {{ $examen->Inspeccion }}</td>
                    <td>OBSERVACIONES: {{ $examen->Inspeccion_observaciones }}</td>
                </tr>
                <tr>
                    <td>RUIDOS PERISTÁLTICOS: {{ $examen->R_peristalticos }}</td>
                    <td>OBSERVACIONES: {{ $examen->R_peristalticos_observaciones }}</td>
                </tr>
                <tr>
                    <td>DOLOR A LA PALPACIÓN: {{ $examen->d_palpitacion }}</td>
                    <td>PALPACIÓN: {{ $examen->palpitacion }}</td>
                </tr>
                <tr>
                    <td>DIÁSTASIS DE RECTOS: {{ $examen->diastasis }}</td>
                    <td>HÍGADO: {{ $examen->Higado }}</td>
                </tr>
                <tr>
                    <td>VIBICES: {{ $examen->Vibices }}</td>
                    <td>HERNIAS: {{ $examen->Hernias }}</td>
                </tr>
                <tr>
                    <td>BAZO: {{ $examen->Bazo }}</td>
                    <td>TUMORACIÓN: {{ $examen->Tumoracion }}</td>
                </tr>
                <tr>
                    <td>CICATRIZ: {{ $examen->Cicatriz }}</td>
                    <td>GANGLIOS: {{ $examen->Ganglios }}</td>
                </tr>

            </tbody>
        </table>
        <p class="text-center">MOVIMIENTOS PÉLVICOS</p>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>MOVIMIENTOS: {{ $examen->Movimientos }}</td>
                    <td>OBSERVACIONES: {{ $examen->Movimientos_observaciones }}</td>
                </tr>
                <tr>
                    9
                    <td>MARCHA: {{ $examen->Marcha }}</td>
                    <td>OBSERVACIONES: {{ $examen->Marcha_observaciones }}</td>
                </tr>
                <tr>
                    <td>¿HAY ACORTAMIENTO?: {{ $examen->acortamiento }}</td>
                    <td>FLOGOSIS: {{ $examen->Flogosis }}</td>
                </tr>
                <tr>
                    <td>ULCERAS: {{ $examen->Ulceras }}</td>
                    <td>REFLEJO PATELAR: {{ $examen->Reflejo_patelar }}</td>
                </tr>
                <tr>
                    <td>VARICES: {{ $examen->Varices }}</td>
                    <td>MICOSIS: {{ $examen->Micosis }}</td>
                </tr>
                <tr>
                    <td>LLENADO CAPILAR: {{ $examen->Llenado_capilar }}</td>
                    <td></td>
                </tr>

            </tbody>
        </table>
        <p class="text-center">MIEMBROS TORÁCICOS</p>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>MOVIMIENTOS: {{ $examen->Movimientos_to_observaciones }}</td>
                    <td>OBSERVACIONES: {{ $examen->Movimientos_to_observaciones }}</td>
                </tr>
                <tr>
                    <td>LLENADO CAPILAR: {{ $examen->lle_capilar_ca_to }}</td>
                    <td>OBSERVACIONES: {{ $examen->Marcha_observaciones }}</td>
                </tr>
                <tr>
                    <td>FLOGOSIS: {{ $examen->Flogosis_t }}</td>
                    <td>OBSERVACIONES: {{ $examen->Flogosis_observaciones }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="page-break-after:always;"></div>
    <div class="row">
        <div class="col-md-3">
         
        </div>
        <div class="col-md-3">
            <h5 class="text-center"><b>DICTAMEN DE APTITUD</b></h5>
        </div>
        <div class="col-md-3">
            <img width="135" height="50" class="" src="https://has-humanly.com/empresa_dev/storage/firmas/asesor.png"
            style="float: right !important;">
        </div>

    </div>
     
    <br>
    <br>
    <br>
    <p class="text-center">Posterior a realización de examen médico de <br>
        <b>{{ $empleado->apellido_paterno }} {{ $empleado->apellido_paterno }} {{ $empleado->nombre }}</b><br>
        {{ $empleado->sexo }} de {{ $empleado->edad }} años de edad, se dictamina:
    </p>
    <p><b>RESULTADOS DE APTITUD:</b></p>
    <ul class="list_none bold">

        <li>1. Cumple Requerimientos Médicos (Satisfactorio): @if($resultados->aptitud=='Cumple')<img
                src="https://img.icons8.com/material-sharp/24/000000/x-coordinate.png" />@endif</li>
        <li>2. Cumple Requerimientos Médicos Condicionado (Satisfactorio Condicionado):</li>
        <li>3. No Cumple Requerimientos Médicos (No satisfactorio): @if($resultados->aptitud=='No')<img
                src="https://img.icons8.com/material-sharp/24/000000/x-coordinate.png" />@endif </li>
    </ul>
    <br><br>
    <table>
        <tbody>
            <tr>
                <td class="text-center pl-5 pr-5"></td>
                <td class="text-center pl-5 pr-5"></td>
                <td class="text-center pl-5 pr-5"><img width="170" width="70"
                        src="https://has-humanly.com/empresa_dev/storage/firmas/dr_jaeson.png"><br>Dr. Jaeson Israel
                    Velasco Orea</td>
            </tr>

            <tr>
                <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px rgb(255, 255, 255);"></td>
                <td class="text-center pl-5 pr-5"></td>
                <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
            </tr>
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center">NOMBRE Y FIRMA DEL MÉDICO</td>
            </tr>
            <tr>
                <td class="text-center" style="font-size:9px !important;"></td>
                <td class="text-center"></td>
                <td class="text-center">CEDULA PROF. 8149200</td>
            </tr>
        </tbody>
    </table>
    {{-- <table class="table table-sm table-bordered">
        <tbody>
            <tr>
                <td>¿OBSERVÓ ALGÚN TRANSTORNO DE LA CONDUCTA? {{ $resultados->trans_con }}</td>
                <td>¿OBSERVÓ ALGUNA INCOHERENCIA?{{ $resultados->incoherencia }}</td>
                <td>¿OBSERVÓ ALGÚN TRANSTORNO DE LA ATENCIÓN? {{ $resultados->trans_atencion }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table table-sm table-bordered">
        <tbody>
            <tr>
                <td>ESTADO DE SALUD:</td>
                <td>{{ $resultados->estadoSalud }}</td>
            </tr>
            <tr>
                <td>RESULTADOS DE APTITUD:</td>
                <td>{{ $resultados->aptitud }}</td>
            </tr>
            <tr>
                <td>COMENTARIOS: </td>
                <td>{{ $resultados->comentarios }}</td>
            </tr>
        </tbody>
    </table> --}}
    {{-- <footer>
        <p>Avenida 21 Poniente No. 3711Colonia Belisario Domínguez Puebla Puebla<br>
            Tel: 222 2966608 al 13
        </p>
    </footer> --}}
</body>
</html>