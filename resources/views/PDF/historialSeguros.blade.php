<!DOCTYPE html>
<html>

<head>

    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .letra {
            font-size: 12px !important;
        }

        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }

        .list_none {
            list-style: none;
        }

        .texto_vertical {
            width: 1px;
            transform: rotate(-90deg);
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
        }
    </style>
</head>

<body>

    <div class="content letra">
        <table class="">
            <tbody>
                <tr>
                    <td>
                        <h4 class="font-weight-normal text-left"
                            style="font-size:30px !important;color:#52555b !important;">Examen Médico</h4>
                    </td>
                    <td>
                        <h4 class="font-weight-normal text-rigth pt-3"
                            style="font-size:12px !important;border-bottom: 1px solid #000;">Nº. Folio(s)<span></span>
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="font-weight-normal text-left"
                            style="font-size:14px !important;color:#52555b !important;"><b>Indicaciones:</b></h4>
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px !important;">
                        <ul class="list_none" style="text-align: justify !important;">
                            <li> 1. Este formato deberá ser llenado a una sola tinta, con letra legible y deberá tener
                                firma autógrafa del médico examinador.</li>
                            <li>2. Es necesario llenar el formato en su totalidad y proporcionar información completa y
                                detallada. No será válido con tachaduras o
                                enmendaduras. En caso de error, deberá cancelar con paréntesis y línea, solicitando
                                firma del examinado adyacente a la corrección.</li>
                            <li>3. Los resultados serán generados y administrados por Seguros Monterrey New York Life,
                                los cuales son exclusivos para los
                                trámites de suscripción.</li>
                        </ul>
                    </td>

                </tr>
            </tbody>
        </table>

        <div id="identificacion">
            <table class="table table-sm table-bordered" width="100%">
                <tbody>
                    <tr>
                        <td colspan="3">1. Nombre completo de la persona por asegurar<br>
                            {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }} {{ $empleado->nombre }}
                        </td>
                        <td colspan="1">2. Edad {{ $identificacion->numEmpleado }}{{ $empleado->curp }}</td>
                        <td colspan="2">3.Sexo {{ $identificacion->departamento }}<br>
                            <label for="cbox1">Mujer</label>
                            <input type="checkbox" id="cbox1" value="second_checkbox" checked>
                            <label for="cbox2">Hombre</label>
                            <input type="checkbox" id="cbox2" value="second_checkbox">
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">4. Fecha de nacimiento<br>
                            {{ $empleado->fecha_nacimiento }}
                        </td>
                        <td colspan="3">5. Ocupación (detallar) {{ $identificacion->numEmpleado }}{{ $empleado->curp }}
                        </td>
                        <td colspan="1">6. Nacionalidad {{ $identificacion->departamento }}
                        </td>

                    </tr>
                </tbody>
            </table>
            <table class="table table-sm table-bordered" width="100%">
                <tbody>
                <tr>
                    <td colspan="8"> 7. ¿En su familia hay o ha habido alguna de las siguientes enfermedades? </td>
                 </tr>
                 <tr>
                     <td class="text-center fondo_titulos" colspan="8">
                         ANTECEDENTES HEREDOFAMILIARES
                     </td>

                 </tr>
                 <tr>
                     <th></th>
                     <th>Si</th>
                     <th>No</th>
                     <th>Parentesco/especificar</th>
                     <th></th>
                     <th>Si</th>
                     <th>No</th>
                     <th>Parentesco/especificar</th>
                 </tr>
                 <tr>
                     <td>Cáncer</td>
                     <td><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><input type="checkbox" id="cancer_no" value="no"></td>
                     <td>
                         <p>Madre</p>
                     </td>
                     <td>Hipertensión Arterial</td>
                     <td><input type="checkbox" id="hipertension_si" value="si"></td>
                     <td><input type="checkbox" id="hipertension_no" value="no"></td>
                     <td>
                         <p>Padre</p>
                     </td>
                 </tr>
                 <tr>
                     <td>Diabetes</td>
                     <td><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><input type="checkbox" id="cancer_no" value="no"></td>
                     <td>
                         <p>Madre</p>
                     </td>
                     <td>Renales</td>
                     <td><input type="checkbox" id="hipertension_si" value="si"></td>
                     <td><input type="checkbox" id="hipertension_no" value="no"></td>
                     <td>
                         <p>Padre</p>
                     </td>
                 </tr>
                 <tr>
                     <td>Cardíacas</td>
                     <td><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><input type="checkbox" id="cancer_no" value="no"></td>
                     <td>
                         <p>Madre</p>
                     </td>
                     <td>Tiroideas</td>
                     <td><input type="checkbox" id="hipertension_si" value="si"></td>
                     <td><input type="checkbox" id="hipertension_no" value="no"></td>
                     <td>
                         <p>Padre</p>
                     </td>
                 </tr>
                 <tr>
                     <td>Neurológicas</td>
                     <td><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><input type="checkbox" id="cancer_no" value="no"></td>
                     <td>
                         <p>Madre</p>
                     </td>
                     <td>Mentales</td>
                     <td><input type="checkbox" id="hipertension_si" value="si"></td>
                     <td><input type="checkbox" id="hipertension_no" value="no"></td>
                     <td>
                         <p>Padre</p>
                     </td>
                 </tr>
                 <tr>
                     <td>Reumáticas</td>
                     <td><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><input type="checkbox" id="cancer_no" value="no"></td>
                     <td>
                         <p>Madre</p>
                     </td>
                     <td>Otras</td>
                     <td><input type="checkbox" id="hipertension_si" value="si"></td>
                     <td><input type="checkbox" id="hipertension_no" value="no"></td>
                     <td>
                         <p>Padre</p>
                     </td>
                 </tr>
                 
                </tbody>
            </table>
            <table width="100%" class="table table-sm table-bordered">
                <tr>
                    <td colspan="8" class="mt-0">8. Historia familiar directa</td>
                </tr>
                <tr>
                    <th colspan="2">Parentesco</th>
                    <th colspan="1">Edad</th>
                    <th colspan="2">Estado de salud</th>
                    <th colspan="1">Edad a<br> su muerte</th>
                    <th colspan="2">Causa de muerte</th>
                </tr>
                <tbody>
                    
                    <tr>
                        <td colspan="2">Padre</td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Madre</td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Hermanos</td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Cónyuge</td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Hijos</td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                        <td colspan="1"></td>
                        <td colspan="2"></td>
                    </tr>
                </tbody>
                
                
            </table>
            <table class="table table-sm table-bordered" width="100%">
                <tr>
                    <td class="text-center fondo_titulos" colspan="8">
                       Hábitos
                    </td>

                </tr>
                <tbody>
                
                 <tr>
                     <td>9. ¿Fuma actualmente?</td>
                     <td><label for="">Si</label><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><label for="">No</label><input type="checkbox" id="cancer_no" value="no"></td>
                     <td>11. ¿Ingiere actualmente bebidas alcohólicas?</td>
                     <td><label for="">Si</label><input type="checkbox" id="cancer_si" value="si"></td>
                     <td><label for="">No</label><input type="checkbox" id="cancer_no" value="no"></td>
                     
                 </tr>
                 <tr>
                    <td>10. ¿Ha fumado alguna vez?</td>
                    <td><label for="">Si</label><input type="checkbox" id="cancer_si" value="si"></td>
                    <td><label for="">No</label><input type="checkbox" id="cancer_no" value="no"></td>
                    <td>12. ¿Ha ingerido bebidas alcohólicas?</td>
                    <td><label for="">Si</label><input type="checkbox" id="cancer_si" value="si"></td>
                    <td><label for="">No</label><input type="checkbox" id="cancer_no" value="no"></td>
                    
                 </tr>
                 <tr>
                    <td>a. ¿En qué año empezó a fumar?</td>
                    <td colspan="2"></td>
                    <td>a. Clase</td>
                    <td colspan="3"></td>
                 </tr>
                 <tr>
                    <td>b. ¿Número de cigarrillos diarios?</td>
                    <td colspan="2"></td>
                    <td>b. Cantidad y frecuencia </td>
                    <td colspan="3"></td>
                 </tr>
                 <tr>
                    <td>c. ¿En qué año dejó de fumar?</td>
                    <td colspan="2"></td>
                    <td>c. Si dejó de beber, señale razón y fecha</td>
                    <td colspan="3"></td>
                 </tr>
                 <tr>
                    <td>d. ¿Por qué razón?</td>
                    <td colspan="2"></td>
                    <td>d. ¿Cuánto bebía antes?</td>
                    <td colspan="3"></td>
                 </tr>
                 <tr>
                    <td colspan="5">13. a. ¿Ha consumido alguna droga (marihuana, heroína, cocaína, anfetaminas, LSD, barbitúricos, entre otros) o anabólicos?</td>
                    <td colspan="2"><label for="">Si</label><input type="checkbox" id="cancer_si" value="si"></td>
                    <td colspan="2"><label for="">No</label><input type="checkbox" id="cancer_no" value="no"></td>
                  
                 </tr>
                 <tr>
                    <td colspan="4">b. Si es afirmativo, señalar fecha de inicio, tipo de droga, frecuencia y última utilización</td>
                    <td colspan="5"></td>
                    
                  
                 </tr>
                </tbody>
            </table>
            <table class="table table-sm table-bordered" width="100%">
                <tbody>
                    <tr>
                        <td  >14. ¿Practica regularmente algún tipo de ejercicio o deporte extremo?</td>
                        <td colspan="2"><label for="">Si</label><input type="checkbox" id="cancer_si" value="si"></td>
                        <td colspan="2"><label for="">No</label><input type="checkbox" id="cancer_no" value="no"></td>
                    </tr>
                    <tr>
                        <td colspan="6">En caso afirmativo, detallar tipo, frecuencia, duración, fecha de inicio, nivel amateur o profesional, si compite o ha sufrido accidentes: </td>
                        <td colspan="3"></td>
                      
                    </tr>
                    <tr>
                        <td colspan="9">Ante cualquier respuesta afirmativa, detallar en la parte inferior: fecha de diagnóstico, cuadro clínico, resultados de estudios de 
                            laboratorio y gabinete, tratamiento, evolución, secuelas, médico tratante, hospital y estado actual. </td>
                    </tr>
                </tbody>
            </table>
            
            <div class="row">
            <table class="table table-sm table-bordered" width="50%">
               
                <tbody>
                    <tr>
                        <td colspan="3">16. Padece o ha padecido de:</td>
                        <td colspan="1"><label for="">Si</label></td>
                        <td colspan="1"><label for="">No</label></td>
                        
                    </tr>
                    <tr>
                        <td colspan="3">a. ¿Algún soplo en el corazón? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                        
                    </tr>
                    <tr>
                        <td colspan="3">b. ¿Dolor en el pecho o en el tórax? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                       
                    </tr>
                    <tr>
                        <td colspan="3">c. ¿Infarto en el corazón? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                       
                    </tr>
                    <tr>
                        <td colspan="3">d. ¿Arritmias? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                       
                    </tr>
                    <tr>
                        <td colspan="3">e. ¿Falta de aire (disnea)? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                        
                    </tr>
                    <tr>
                        <td colspan="3">f. ¿Hipertensión arterial? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                     
                    </tr>
                    <tr>
                        <td colspan="3">g. ¿Alguna otra enfermedad del corazón? *</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                       
                    </tr>
                    <tr>
                        <td colspan="3">h. ¿Elevación de colesterol o triglicéridos?</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                        
                    </tr>
                    <tr>
                        <td colspan="3">i. ¿Enf. circulatorias (várices/hemorroides/trombosis)?</td>
                        <td colspan="1"><label for=""></label></td>
                        <td colspan="1"><label for=""></label></td>
                        
                    </tr>
                </tbody>
            </table>
            <table class="table table-sm table-bordered" width="50%">
               
                <tbody>
                    <tr>
                        
                        <td colspan="2">16.1. ¿Diabetes?*</td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">16.2. ¿Enf. de tiroides?</td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">16.3 ¿Enf. o accidente osteoarticular/ rodilla/
                            columna/etc.? </td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">16.3 ¿Enf. o accidente osteoarticular/ rodilla/
                            columna/etc.? </td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">16.4. ¿Cáncer/tumores?</td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                       
                        <td colspan="2">16.5. ¿Alergias a medicamentos/alimentos/etc.?</td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">16.6 ¿Otras enf. (reuma/derma/inmuno/etc.?</td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">16.7. ¿Alguna intervención quirúrgica?</td>
                        <td colspan="1">x</td>
                        <td colspan="1"></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">17. En caso de ser mujer, padece o ha padecido de:</td>
                        <td colspan="1"><label for="">Si</label></td>
                        <td colspan="1"><label for="">No</label></td>
                    </tr>
                    <tr>
                        
                        <td colspan="2">a. ¿Enfermedades de la glándula mamaria?</td>
                        <td colspan="1"><label for="">Si</label></td>
                        <td colspan="1"><label for="">No</label></td>
                    </tr>
                </tbody>
            </table>
        </div>
            
        </div>
</body>
<script src="{!! asset('js/bootstrap.min.js') !!}"></script>

</html>