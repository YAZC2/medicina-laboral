<!DOCTYPE html>
<html>

<head>
    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
         header {
            position: fixed;
            position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;
                font-size: 10px !important;
            }
        .letra {
            font-size: 10px !important;
            text-transform: uppercase;
            color: #2271B3;
            font-weight: bold;
        }

        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }

        .list_none {
            list-style: none;
        }

        .cuadrado {
            width: 60px !important;
            height: 20px !important;
            border: 1px solid #000;
        }

        td {
            border-bottom: 1px solid #2271B3;
            font-size: 11px;
        }

        .boderblanco {
            border-bottom: 1px solid #fff;
        }
        @page  {  margin: 80px 50px; }
        #header { position: fixed; left: 0px; right: 0px; height: 150px; float: right; }

    </style>
</head>


<header>
    {{-- <table>
        <tbody>
            <tr>
                <td><img class="" style=""src="https://has-humanly.com/pagina_has/images/logo_30.svg" width="120px" height="40px"/></td>
                <td> <h4 class="font-weight-normal" style="font-size:18px !important;color:#000;">
                        <b>HISTORIA CLÍNICA</b>
                    </h4>
                    <h4 class="font-weight-normal" style="font-size:18px !important;color:#2271B3;">
                        CENTRO DE SALUD CEMEX
                    </h4>
                </td>
                <td><img class="" style=""src="https://has-humanly.com/cemex/storage/app/empresas/1200px-Cemex_logo.svg.png" width="120px" height="40px"/></td>
               
            </tr>
        </tbody>
    </table> --}}
    <div class="col-md-12 row">
        <div class="col-md-3">
            <img src="https://has-humanly.com/pagina_has/images/logo_30.svg" 
             width="120;">
        </div>
        <div class="col-md-6 text-center">
           <b>ASESORES ESPECIALIZADOS EN LABORATORIOS MATRIZ</b> <br>
                21 PONIENTE No. 3711, COL BELISARIO DOMINGUEZ C.P. 72180, PUEBLA, PUE<br>
                TELS. (222)2966608, 2966609, 2966610 y 2224540933
        </div>
        <div class="col-md-3">
            <img src="https://has-humanly.com/cemex/storage/app/empresas/1200px-Cemex_logo.svg.png" 
            class="float-right" width="110;"></div>
    </div>
    
</header>
<body class="">
    <div class="content letra">
        
        <div class="text-center mt-2">
            <h4 class="font-weight-normal" style="font-size:18px !important;color:#000;"><b>HISTORIA CLÍNICA</b>
            </h4>
            <h4 class="font-weight-normal" style="font-size:18px !important;color:#2271B3;">
                CENTRO DE SALUD CEMEX</h4>

        </div>
        <table class="table table-sm letra">
            <tbody style="font-size:12px;">
                <tr>
                    <td colspan=""></td>
                    <td colspan=""></td>
                    <td colspan="">FECHA:</td>
                    <td colspan="">{{ \Carbon::parse($identificacion->fecha)->format('d/m/Y') }}</td>
                </tr>
                <tr>
                    <td style="color:#0000ff;font-size:14px !important;"><b>1. DATOS GENERALES:</b></td>
                    <td></td>
                    <td></td>
                    <td class=""></td>
                </tr>
                <tr>
                    <td>NOMBRE:{{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }} {{ $empleado->nombre }}</td>
                    <td>EDAD:{{ \Carbon::parse($empleado->fecha_nacimiento)->age }}</td>
                    <td>SEXO:{{ $empleado->genero }}</td>
                    <td class="">NOMINA:@if(!empty($identificacion->nomina)){{$identificacion->nomina}}@endif</td>
                </tr>
                <tr>
                    <td>DEPARTAMENTO:{{ $identificacion->departamento }}</td>
                    <td></td>
                    <td>DIRECCIÓN:{{ $identificacion->domicilio }}</td>
                    <td class=""></td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td></td>
                    <td>PISO:</td>
                    <td class="">EXT:</td>
                </tr> -->
                <tr>
                    <td>TELEFONO: @if(!empty($identificacion->telefono_pa)){{ $identificacion->telefono_pa }}@endif</td>
                    <td></td>
                    <td>ESTADO CIVIL:{{ $identificacion->estadoCivil }}</td>
                    <td class="">N° AFILIACION IMSS:@if(!empty($identificacion->nimss)){{$identificacion->nimss}}@endif</td>
                </tr>
                <tr>
                    <td>FECHA Y LUGAR DE NACIMIENTO:{{ $empleado->fecha_nacimiento }}/{{ $identificacion->lugarNacimiento }}</td>
                    <td></td>
                    <td>FECHA INGRESO:</td>
                    <td class="">@if(!empty($identificacion->fingreso)){{ $identificacion->fingreso }}@endif</td>
                </tr>
                <tr>
                    <td>SUPERVISOR:@if(!empty($identificacion->supervisor)){{$identificacion->supervisor}}@endif</td>
                    <td></td>
                    <td>PROFESION:@if(!empty($identificacion->profesion)){{$identificacion->profesion}}@endif</td>
                    <td class=""></td>
                </tr>
                <tr>
                    <td colspan="4" style="color:#0000ff;font-size:14px !important;"><b>2. ANTECEDENTES HEREDOFAMILIARES:</b></td>

                </tr>
                <tr>
                    <td colspan="4" class="boderblanco">ASCENDENTES:</td>
                </tr>
                @php
                $heredofamilia = json_decode($heredofamiliar->data);
                @endphp
                @foreach ($heredofamilia as $familiar)
                @if($familiar->familiar=='Padre' or $familiar->familiar=='Madre')
                <tr>
                    <td>{{ $familiar->familiar }}</td>
                    <td>{{ $familiar->estado }}</td>
                    <td colspan="2">
                        @if ($familiar->enfermedad != null)
                        @foreach ($familiar->enfermedad as $value)
                        {{$value}}
                        @endforeach
                        @else
                        Ninguno
                        @endif

                    </td>
                </tr>
                @endif
                @endforeach
                <tr>
                    <td class="boderblanco" colspan="4">COLATERALES:</td>
                </tr>
                @foreach ($heredofamilia as $familiar)
                @if($familiar->familiar=='Conyuge' or $familiar->familiar=='Hermanos(as)')
                <tr>
                    <td>{{ $familiar->familiar }}</td>
                    <td>{{ $familiar->estado }}</td>
                    <td colspan="2">
                        @if ($familiar->enfermedad != null)
                        @foreach ($familiar->enfermedad as $value)
                        {{$value}}
                        @endforeach
                        @else
                        Ninguno
                        @endif

                    </td>
                </tr>
                @endif
                @endforeach
                <tr>
                    <td colspan="4" class="boderblanco">DESCENDENTES:</td>
                </tr>
                @foreach ($heredofamilia as $familiar)
                @if($familiar->familiar=='Hijos')
                <tr>
                    <td>{{ $familiar->familiar }}</td>
                    <td>{{ $familiar->estado }}</td>
                    <td colspan="2">
                        @if ($familiar->enfermedad != null)
                        @foreach ($familiar->enfermedad as $value)
                        {{$value}}
                        @endforeach
                        @else
                        Ninguno
                        @endif

                    </td>
                </tr>
                @endif
                @endforeach
                <tr>
                    <td colspan="4" style="color:#0000ff;font-size:14px !important;"><b>3. ANTECEDENTES PERSONALES NO PATOLÓGICOS:</b></td>

                </tr>
                <tr>
                    @php

                    $cigarro = $nopatologico->cigarro;
                    $alcohol = $nopatologico->alcohol;
                    $drogas = $nopatologico->drogas;
                    $du = $nopatologico->drogas_usadas;
                    $deporte = $nopatologico->deporte;
                    $dieta = $nopatologico->dieta;
                    if(!empty($nopatologico->tatuajes)){
                    $tatuajes=$nopatologico->tatuajes;
                    }else{
                    $tatuajes='';
                    }
                    if(!empty($nopatologico->tiposangre)){
                    $tiposangre=$nopatologico->tiposangre;
                    }
                    else{
                    $tiposangre='';
                    }


                    @endphp
                    <td>TABAQUISMO: {{$cigarro->Cigarro }}</td>
                    <td>¿DESDE QUE EDAD?: {{ $cigarro->edad }}</td>
                    <td>¿NUMERO PROMEDIO DE CIGARROS QUE FUMA?:{{ $cigarro->frecuencia }}</td>
                    <td>¿A QUE EDAD DEJÓ DE FUMAR?:{{ $cigarro->noConsumir }}</td>
                </tr>
                <tr>
                    <td>ALCOHOLISMO:{{ $alcohol->Alcohol }}</td>
                    <td>¿DESDE QUE EDAD?:{{ $alcohol->edad }}</td>
                    <td>¿CON QUE FRECUENCIA Y CANTIDAD?:{{ $alcohol->frecuencia }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>TOXICOMANÍAS:{{ $drogas->Drogas }}<br>
                        @if ($du != null)
                        @foreach ($du as $value)
                        {{ $value . ', ' }}
                        @endforeach
                        @else
                        Ninguna
                        @endif
                    </td>
                    <td></td>
                    <td>TATUAJES:@if(!empty($tatuajes)){{$tatuajes}}@endif</td>
                    <td></td>
                </tr>
                <tr>
                    <td>TIPO DE SANGRE:{{$tiposangre}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td colspan="4" style="color:#0000ff;font-size:14px !important;"><b>4. ANTECEDENTES PERSONALES PATOLÓGICOS:</b></td>

                </tr>
                <tr>
                    <td colspan="4">MEDICOS:
                        @php
                        $enfermedades = $patologico->enfermPadecidas;
                        $cl = $patologico->cirugiasLesion;
                        @endphp
                        @if ($enfermedades == null)
                        Ninguna
                        @else
                        @foreach ($enfermedades as $value)
                        {{ $value . ', ' }}
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="border-bottom: 1px solid #fff;font-size:11px;" colspan="4">
                        QUIRÚRGICOS: @if(!empty($patologico->quirurgicos)){{$patologico->quirurgicos}}@endif,
                        @if(!empty($patologico->quirurgicos)) 
                            @if($patologico->quirurgicos=='si')
                        Tipo de cirugia:{{$patologico->destipocirugia}},
                        Fecha:{{$patologico->fechacirugia}},
                        Complicaciones:{{$patologico->complicacionescirugia}}
                            @endif 
                        @endif
                    </td>
                    <!-- @foreach ($cl as $json)
                <tr>
                    @foreach ($json as $key => $value)
                    <td style="border-bottom: 1px solid #fff;font-size:11px;">{{ strtoupper($key) }}</td>
                    <td style="border-bottom: 1px solid #fff;font-size:11px;">{{ strtoupper($value) }}</td>
                    @endforeach
                </tr>
                @endforeach -->
                </tr>
                <tr>
                    <td colspan="4">TRAUMÁTICOS: @if(!empty($patologico->traumaticos)){{$patologico->traumaticos }}@endif,
                        @if(!empty($patologico->traumaticos)) 
                            @if($patologico->traumaticos=='si')
                            Tipo de traumatismo:{{$patologico->tipotraumaticos}},
                            Fecha:{{$patologico->fechatraumaticos}},
                            Complicaciones:{{$patologico->comptraumaticos}}
                            @endif 
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4">ALÉRGICOS:  @if(!empty($patologico->alergicos)){{$patologico->alergicos}}@endif,
                        @if(!empty($patologico->alergicos)) 
                            @if($patologico->alergicos=='si')
                            ¿Cuáles?:{{$patologico->cualalergia}}
                            @endif 
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4">INTERNAMIENTOS: @if(!empty($patologico->internamientos)){{$patologico->internamientos}}@endif,
                        @if(!empty($patologico->internamientos)) 
                            @if($patologico->internamientos=='si')
                            ¿Por qué?:{{$patologico->porquealergias}}
                            @endif
                        @endif
                       
                    </td>
                </tr>
                </tr>
                <tr>
                    <td>MEDIC. QUE CONSUME:</td>
                    <td>@if(!empty($patologico->medicamento)){{$patologico->medicamento}}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
               
                @if ($empleado->genero == 'FEMENINO' || $empleado->genero == 'Femenino')
                <tr>
                    <td colspan="4" style="color:#0000ff;font-size:14px !important;"><b>5.ANTECEDENTES GINECO-OBSTÉTRICOS:</b></td>

                </tr>
                <tr>
                    <td>
                        MENARQUIA:@if(!empty($genicoobstetrico->MENARQUIA)){{ $genicoobstetrico->MENARQUIA }}@endif
                    <td>RITMO Y DURACIÓN:@if(!empty($genicoobstetrico->ritmo_duracion)){{ $genicoobstetrico->ritmo_duracion }}@endif</td>
                    <td>DISMENORREA: @if(!empty($genicoobstetrico->DISMENORREA)){{ $genicoobstetrico->DISMENORREA }}@endif</td>
                    <td> G: @if(!empty($genicoobstetrico->G)){{ $genicoobstetrico->G }}@endif P: @if(!empty($genicoobstetrico->P)){{ $genicoobstetrico->P }}@endif A: @if(!empty($genicoobstetrico->A)){{ $genicoobstetrico->A }}@endif C: @if(!empty($genicoobstetrico->C)){{ $genicoobstetrico->C }}@endif</td>

                </tr>
                <tr>
                    <td>FUP/C: @if(!empty($genicoobstetrico->FUP)){{ $genicoobstetrico->FUP }}@endif </td>
                    <td>IVSA: @if(!empty($genicoobstetrico->IVSA)){{ $genicoobstetrico->IVSA }}@endif </td>
                    <td>N° C.S:@if(!empty($genicoobstetrico->CS)){{ $genicoobstetrico->CS }}@endif</td>
                    <td>FUM:@if(!empty($genicoobstetrico->FUM)){{ $genicoobstetrico->FUM }}@endif</td>
                </tr>
                <tr>
                    <td>MPF: @if(!empty($genicoobstetrico->MPF)){{ $genicoobstetrico->MPF }}@endif</td>
                    <td>DOC:@if(!empty($genicoobstetrico->DOC)){{ $genicoobstetrico->DOC }}@endif</td>
                    <td>EDA GESTA:@if(!empty($genicoobstetrico->GESTAC)){{ $genicoobstetrico->GESTAC }}@endif</td>
                    <td>FPP:@if(!empty($genicoobstetrico->FPP)){{ $genicoobstetrico->FPP }}@endif</td>
                </tr>
                <tr>
                    <td colspan="2">Enfermedad de mama: @if(!empty($genicoobstetrico->enf_mama)){{ $genicoobstetrico->enf_mama }}@endif</td>
                    <td colspan="2">Enfermedad de útero:@if(!empty($genicoobstetrico->enf_utero)){{ $genicoobstetrico->enf_utero }}@endif</td>
                </tr>
                @endif

                <tr>
                    <td style="color:#0000ff;font-size:14px !important;"><b>6. MOTIVO DE CONSULTA:</b></td>
                    <td>@if(!empty($resultados->motivo_consulta)){{$resultados->motivo_consulta}}@endif</td>
                    <td></td>
                    <td class=""></td>
                </tr>
                <tr>
                    <td style="color:#0000ff;font-size:14px !important;"><b>7. P E E A:</b></td>
                    <td>@if(!empty($resultados->peea)){{$resultados->peea}}@endif</td>
                    <td></td>
                    <td class=""></td>
                </tr>
             
               
                <tr class="">
                    <td colspan="4" style="color:#0000ff;font-size:14px !important;border-bottom:1px solid #fff !important;"><b>8.-INTERROGATORIO POR APARATOS Y SISTEMAS:</b></td>

                </tr>
                <tr>
                    <td colspan="4"><b>SINTOMAS GENERALES:</b>@if(!empty($historiaLaboral->sintomasgenerales)){{$historiaLaboral->sintomasgenerales}}@endif</td>
                </tr>
                <tr>
                    <td colspan="4"><b>DIGESTIVO:</b>@if ($historiaLaboral->gastroIntestinal == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->gastroIntestinal as $value)
                        @if($value=='PT')
                        Problemas al tragar,
                        @elseif($value=='DAI')
                        Dolor abdominal/Indigestion cronica,
                        @elseif($value=='CE')
                        Cambio en sus evacuaciones,
                        @elseif($value=='DP')
                        Diarrea persistente,
                        @elseif($value=='ENS')
                        Evacuaciones negras o sangre,
                        @else
                        Vomitos repetitivos
                        @endif
                        @endforeach
                        @endif</td>
                </tr>
                <tr>
                    <td colspan="4"><b>RESPIRATORIO:</b> @if ($historiaLaboral->pulmunar == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->pulmunar as $value)
                        @if($value=='FA')
                        Le falta el aire,
                        @elseif($value=='SP')
                        Silbidos en el pecho,
                        @else
                        Tos persistente,
                        @endif
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><b>CIRCULATORIO:{{ $historiaLaboral->circulatorio }}</b>
                        @if (!empty($historiaLaboral->circulatorio))
                            @if ($historiaLaboral->circulatorio == null)
                            Ninguno
                            @else
                            @foreach ($historiaLaboral->circulatorio as $value)
                            @if($value=='ER')
                            Enfermedad de Raynaud,
                            @elseif($value=='AC')
                            Ataque al corazón,
                            @endif
                            @endforeach
                            @endif
                        @else
                            Ninguno
                        @endif
                        </td>
                </tr>
                <tr>
                    <td colspan="4"><b>MUSCULOESQUELETICO:</b> @if ($historiaLaboral->musculoesqueletico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->musculoesqueletico as $value)
                        @if($value=='DA')
                        Dolor Articulaciones,
                        @elseif($value=='PEC')
                        Problemas espalda/cuello,
                        @else
                        Cansancio excesivo,
                        @endif
                        @endforeach
                        @endif</td>
                </tr>
                <tr>
                    <td colspan="4"><b>NERVIOSO:</b>
                        @if ($historiaLaboral->neurologico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->neurologico as $value)

                        @if($value=='DCFI')
                        Dolores de la cabeza frecuentes o intensos,
                        @elseif($value=='DFD')
                        Dificultad para dormir,
                        @elseif($value=='VM')
                        Vertigo o mareos,
                        @elseif($value=='PM(C)')
                        Problemas con la memoria(Concentración),
                        @elseif($value=='Tem')
                        Temblor,
                        @elseif($value=='Ner')
                        Nerviosismo,
                        @elseif($value=='Dep')
                        Depresión,
                        @elseif($value=='AC')
                        Ataques o Convulsiones,
                        @elseif($value=='PAPC')
                        Paralisis de alguna parte del cuerpo,
                        @elseif($value=='PET')
                        Problemas de estres en el trabajo,
                        @else
                        Problemas en tu vida familiar,
                        @endif
                        @endforeach
                        @endif</td>
                </tr>
                <tr>
                    <td colspan="4"><b>URINARIO:</b>
                        @if ($historiaLaboral->genitourinario == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->genitourinario as $value)
                        @if($value=='PNOMU')
                        Por la noche orina más de una vez,
                        @elseif($value=='OSPR')
                        Orina con sangre/Piedras en el riñon,
                        @else
                        Problemas al orinar,
                        @endif
                        @endforeach
                        @endif</td>
                </tr>
                <tr>
                    <td colspan="4"><b>REPRODUCTOR:</b>
                        @if(!empty($historiaLaboral->reproductor))
                            @if ($historiaLaboral->reproductor == null)
                            Ninguno
                            @else
                            @foreach ($historiaLaboral->reproductor as $value)
                            @if($value=='IO')
                            Infección de orina,
                            @elseif($value=='VPH')
                            Virus del Papiloma Humano,
                            @endif
                            @endforeach
                            @endif
                        @else
                            Ninguno
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><b>SENTIDOS:@if(!empty($historiaLaboral->sentidos)){{$historiaLaboral->sentidos}} @else
                        Ninguno
                        @endif</b></td>
                </tr>
                <tr>
                    <td colspan="4"><b>PIEL Y FANERAS:</b>
                        @if ($historiaLaboral->dermatologico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->dermatologico as $value)
                        Problemas en la piel
                        @endforeach
                        @endif</td>
                </tr>
                <tr>
                    <td colspan="4"><b>OTROS:</b>@if(!empty($historiaLaboral->otrosproble)){{$historiaLaboral->otrosproble}} @else
                        Ninguno
                        @endif</td>
                </tr>
                <tr>
                    <td style="color:#0000ff;font-size:14px !important;"><b>9.-EXPLORACIÓN FÍSICA:</b></td>
                    <td></td>
                    <td></td>
                    <td class=""></td>
                </tr>
                <tr>
                    <td><b>SIGNOS VITALES:</b></td>
                    <td></td>
                    <td></td>
                    <td class=""></td>
                </tr>
                <tr>
                    <td><b>T/A: @if(!empty($examen->TAHC)){{ $examen->TAHC }}@endif mmhg.</b></td>
                    <td>FC: @if(!empty($examen->FCHC)){{ $examen->FCHC }} @endif FR:</td>
                    <td>°T: @if(!empty($examen->THC)){{ $examen->THC }} °C PESO: {{ $examen->PESO }}@endif kgs </td>
                    <td>TALLA: @if(!empty($examen->TALLAHC)){{ $examen->TALLAHC }}@endif mts.</td>
                </tr>
                <tr>
                    <td><b>IMC:  @if(!empty($examen->IMChc)){{ $examen->IMChc }}@endif kg/m2</b></td>
                    <td>PESO IDEAL: @if(!empty($examen->ideal)){{ $examen->ideal }}@endif kgs</td>
                    <td>CINTURA: @if(!empty($examen->cintura)){{ $examen->cintura }}@endif </td>
                    <td>GRASA CORPORAL @if(!empty($examen->CORPORAL)){{ $examen->CORPORAL }}@endif</td>
                </tr>
                <tr>
                    <td><b>AGUDEZA VISUAL SIN LENTES</b></td>
                    <td>SIN LENTES: </td>
                    <td>OJO DER: @if(!empty($examen->ODSL)){{ $examen->ODSL }}@endif</td>
                    <td>OJO IZQ.:@if(!empty($examen->OISL)){{ $examen->OISL }}@endif</td>
                </tr>
                <tr>
                    <td><b></b></td>
                    <td>CON LENTES:</td>
                    <td>OJO DER:@if(!empty($examen->ODCL)){{ $examen->ODCL }}@endif</td>
                    <td>OJO IZQ.:@if(!empty($examen->OICL)){{ $examen->OICL }}@endif</td>
                </tr>
                <tr>
                    <td><b>DESTROSTIX</b></td>
                    <td>@if(!empty($examen->OICL)){{ $examen->OICL }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>HABITUS EXTERIOR:</b></td>
                    <td>@if(!empty($examen->HABITUS)){{ $examen->HABITUS }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>CABEZA Y CUELLO:</b></td>
                    <td>@if(!empty($examen->CUELLO)){{ $examen->CUELLO }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>TORAX:</b></td>
                    <td>@if(!empty($examen->TORAX)){{ $examen->TORAX }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>ABDOMEN:</b></td>
                    <td>@if(!empty($examen->ABDOMEN)){{ $examen->ABDOMEN }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>MIEM.PELVICOS:</b></td>
                    <td>@if(!empty($examen->PELVICOS)){{ $examen->PELVICOS }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>EXTREMIDADES:</b></td>
                    <td>@if(!empty($examen->EXTREMIDADES)){{ $examen->EXTREMIDADES }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <div style="page-break-after:always;"></div><br>
        <table class="table table-sm letra">
            <tbody style="font-size:12px;">

                <tr>
                    <td style="color:#0000ff;font-size:14px !important;"><b>10.DIAGNOSTICO:</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">@if(!empty($resultados->diagnostico)){{ $resultados->diagnostico }}@endif</td>
                </tr>
                <tr>
                    <td><b>ESTUDIOS DE LABORATORIO:</b></td>
                    <td>@if(!empty($resultados->estudios)){{ $resultados->estudios }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>PLAN TERAPÉUTICO:</b></td>
                    <td>@if(!empty($resultados->plan_t)){{ $resultados->plan_t }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>PRÓXIMA CITA:</b></td>
                    <td>@if(!empty($resultados->proxima_cita)){{ $resultados->proxima_cita }}@endif</td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>


        <table class="pl-5 pr-5">

            <tbody>
                <tr>
                    <td class="text-center pl-5 pr-5" style="border-bottom: 1px solid #fff;font-size:11px;"><img width="150" height="50" src="https://has-humanly.com/empresa_dev/storage/firmas/dr_jaeson.png"><br>Velasco Orea Jaeson Israel</td>
                    <td class="text-center pl-5 pr-5" style="border-bottom: 1px solid #fff;font-size:11px;"></td>
                    <td class="text-center pl-5 pr-5" style="border-bottom: 1px solid #fff;font-size:11px;"><img width="210" height="50" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAABkCAYAAABwx8J9AAAAAXNSR0IArs4c6QAAFedJREFUeF7tnV2oFVUbx5d9E0FJQUVFnVDMKMyISgrUiyjpIiMoK8ikDxULMwizsuwLjaj0qg5dmEFUxIudizBvMiGqU9JJCE5BoXZjd9ZFVBSdl996efa7zpyZ2Xv2fOw9M/8Fm7P1zKxZ67fW4T/Ps571rFlTU1NLnHN8VERABESgQ+D2229f8sEHHyy2/9i8efN/nnvuue+ESAREYDgJzJqamtrinHtmOJunVomACAyCwLfffusWLlzorrjiCv9566233M6dO9299947iObomSIgAj0QkKD3AEmXiEDbCNx6663u448/docOHXLff/+9W7p0qQS9bZNA/a0dAQl67YZMDRaBcgls2bLFPfvss+6RRx5xr732mvv0008l6OUiV+0iUAgBCXohGFWJCDSDgIk5bvZ9+/a5M844Q4LejKFVL1pAQILegkFWF5tJAMs5b1my5P/xsHFiTv2y0PNS1v0iUA0BCXo1nPUUEeibwJdffunXsQ8fPuz279/vf/IpqmCNn3feee6jjz5yc+bMcV9//bW3zK1I0IsirXpEoFwCEvRy+ap2EeiZAML5yy+/ePEeHx/3os33aLnoooscn8WLOzvKen5G9EJeEIho//XXX6f9yqLbFyxY4P9/w4YNCorrm7JuFIFqCEjQq+Gsp4iAJ4BoI56I6JEjR7xoxwkq14bCbd9DF3lRSHmJoP65c+e62267zXsB4tp0+eWXu7vuusvddNNNfiubigiIwHARkKAP13ioNQ0hgCDy6cVNfu2113oX9zXXXNMR8TKEOwnt9u3bvQX+7rvvuhUrVnQus5eNF1980R04cGDa7bTvmWeecVW2syFTQ90QgdIISNBLQ6uK20YAAd+1a5f78MMPZ6xxh9b2Oeec4y655BL/4fugy/z5873XgD3np5xyyozmnHvuuf6FY3Jy0nsY9u7d69544w1/j4R90KOn54vA/wlI0DUbRCAHAQRubGxsmogj3suXL3esP/O9mxWLy3tQws7LB0lkbM95FAUBeYsWLfLWOFHwVhBzLPsdO3Z4YUfw6Sfr+vyUSz7HpNKtItAnAQl6n+B0W7sIdHOhm4ivXLkyk5gR9IaFHHV3V0H3zz//9Oldca1jnce9VHSLcEfMSQuL1c611EkhRSypYlVEQASqIyBBr461nlQzAojVe++9563QaLR56ELHGu/XIu0mmGUi27Ztm9u0aZPPBoeFHldI/7ps2bKeI9yx6Mkyx30S9TJHT3WLwEwCEnTNChGIEECURkdHvZhjceJOJljMgta6udCzAEVQEdaqDz7BzT8yMuLX8b/44ovYtXP6Ye1j/Zxreykws1zwBPzRt17v7aV+XSMCIhBPQIKumSECzvl14Kg1jhitXr3ai3lcsFhecOZup56qBR3BZf2c9K5pLyiIPn1H0LMURH3t2rXeHc/9rME//vjjWarQtSIgAhkJSNAzAtPlzSKQZI2vX7++dKvS3O1VC7odjcqLCmv3ScWuiwbEZZkBuN5XrVrlE+a8/vrrbs2aNVlu17UiIAIZCEjQM8DSpc0ggPWINY5bHUGnlG2Nx5EblKCbdU4gHLEAScX2p09MTPQdI0DdeD8I/KMkbY1rxsxSL0RgsAQk6IPlr6dXSAAXNwFuiLlttcJKrcIaj+umBZxVaaFjKbOvnEC+3bt3p9JH+HnpOHbsWO5RspeDtAC83A9RBSLQcgIS9JZPgDZ0H9cxkdesGQ/KGo/jbJZylYJORjjEFTFH1NPK7Nmz/fp6N+HvZQ7hFWE9XlZ6L7R0jQj0R0CC3h833VUDAqGQE5jF+i1BbsMQcW0BcWx3o51VBMWZqFrWt7QhxItx5513pm5pyzoFbJtct0C8rPXqehEQgf8RkKBrJjSOAMKFJUp6UhPyjRs3DiwbWxxgWz9nrzaR4FUIeq9ub9zyJJyBI9HtRWWxs61sSUlsGjcR1SERqJiABL1i4HpcuQSwfLEssXo5FQyhzCJIVaVhtfXzKgUdkYbP0aNHp513Hh0R+GGhF/mSQdpYlj2SUsyWOytUuwi0g4AEvR3j3IpeIkLsfSbgbevWrZn3PZvVXEUaVtqJB4GtXHwvUjzTPALdBNUY8DK0Z8+eQuaNiTnLC7jbcfmriIAIFE9Agl48U9U4AAImGljjBHGxDS1LqTrJi51ghqAvXbq0dEG3FwiywqWxseuyZIZL4ywxzzILda0I5CMgQc/HT3cPAYEiRKPKLWThCWZEkVch6LxAEE/A+nVaCY9KzTu0RYxL3jbofhFoEwEJeptGu4F9LUo0qtxCFh7IQmKXsgXdnkfqVZYikopdlycznNVd1Lg0cMqqSyJQGgEJemloVXHZBIoSDXO3s52N71WtZ/OcKgS9Snd7uMNAa+Zl/wWofhGYTkCCrhlRSwLmti5CNMwytQNY1q1b51555RUfFEZwWNGlagu9Knd7dIcBwYUKgCt69qg+EUgmIEHX7KglgSKDt8L183vuuce9/fbbnklZlnqVgt6ruz1c18fzkbXk3WGQ9Xm6XgREYCYBCbpmRS0JFBm8Fa6fP/roo+7VV19tjKDbskS36PZ+k76E2fj63WFQywmoRovAEBKQoA/hoKhJ6QTyWpNh7XZEqKVgvfLKK90333zTGEFftGiRjwtIO2DFeHbbox7lZvnxLRsfwXRyseuvVwQGR0CCPjj2enKfBEKXNZnW8hSzTHGvkyGNUnZ+9apc7iTY4YAVGNG/pJJl+SIuP/6wpdXNMx90rwjUmYAEvc6j19K2myDmPYrTotuxTG+55Ra/fYxSdjrWqgSdHPGrVq1y3TLfdVu+4MWANfJdu3b58+OHNT9+S/8c1G0R6BCQoGsy1I4AW6OwPMl4RirRfksorKz/Llu2rFGCjpgj6rjbk1zhacsXHDf7/vvvezGnwIiXHc6Pz5IfP8v40B7Gl/biKVERARHonYAEvXdWunKICNgBIhw00q+4WHQ77ujx8XGfW71JFjovPbatL2noosFwuNRHR0f92fEcVENZsWKFu+OOO7qen55levAyRf14SWB/+PBh/z0s3TwLWZ6na0WgDQQk6G0Y5Qb2sYjzukMxs+NCcS+XfWBKFS73XgIHbcnhgQcecJdeeqkXchNVkuxgiS9fvjzzCxMMeTGg0FfK/v37p/07OiVJsMNn8eLF/lfm1leQXQP/eNWl0ghI0EtDq4rLJIBbdmRkxD+C/OSWFKbXZ4aR3bZ+ftppp7nzzz/fC3qZ6VirEHTbrjYxMZHoun766afd888/30GGeGKNr169OvYec4djTfOhHDx40J9uF4p30hjgSeFFgZ/z5s3zAs6HfPYqIiAC+QlI0PMzVA0DIrB9+3a3YcOGrlHccc0LI7t37NjRcbez9eqvv/5y27Ztq3ViGbarIbosSYTFXOp4OEyI2ar32GOPeTGnmCucF48ffvjBW+1mcXcTa36PaJ999tleuE3A+akiAiJQLgEJerl8VXuJBLDSzW3ebWtWtBlhZDffTzrpJPfzzz/7M9QR8yJSyiZ1fdOmTZ0XhjJyuUe3q1mUOi8uoUud9v3000/u5Zdf9iKOBY5wm9Bb+02YzR1OMCIeET5Zj6ktcTqoahFoPQEJeuunQL0BhKKOuBDg1s0aDNeXydWONbty5Uq/LavsLWu2bm0vDAho0e59Xkpeeuklt2bNGh8xjjXOz14KDOGHS5zvtFPr2L2Q0zUiMHgCEvTBj4FakJMAYoULnS1aWI24zRG1pBK3hm0pX4sQdESbNsVtu4omxek3SY65xW09m6CzcG07zTUOI64966yz3H333efbiYhrm1jOiajbRWDABCToAx4APb44AmxDY+81YpdmrYfb1czljZXMnva8Ee5mgdOruG1X/Qg6HgXqRYR7Fe4TTjjB3X///X67WegaD1Pd0l9Z38XNP9UkAoMmIEEf9Ajo+YUSYP2XQLk0az3croZQIuann366Yy09b4S7vSwQYEZdUcEM18/xBoQCj4VMe9L2ZoeweBnh5SV0p1922WXuu+++c08++aR74YUXZrDt9xCWQgdJlYmACJRCQIJeClZVOmgCSdZ69CCS8OhUXPVsocqzpm0nt8UdvWrWOyKP2PNvLG7bqx1lFu7N5juWNvdwKEpYcJcTC8B2M+IACOqbnJycEUtg1nmWQ1gGPY56vgiIQO8EJOi9s9KVNSMQZ60fOXLEb1EzwbPta3QNEc4TdW57vy3gDevZsq0hxGNjY44XiLjCcwnMQ5z5btHj3Mc9pGDlZcQKz+B6hDwMAmRvPsJP/6JF1nnNJrCaKwIZCUjQMwLT5fUjEFrrs2bNcqeeeqp76qmnvDWOyNmWtTyCbmIOHQLycIOzTz6ukH0NMUa4EeNoghzau3fvXp9+1RK4ROuJ8wAkZYcLT0iTdV6/+asWi0CvBCTovZLSdbUmgLX+xBNPeFc3AWP//PNPpz9z5sxxP/74Y6qFnha5Hop5FJJZ2mZdx+1vtwQ5/A4Bt33giD3if+GFF/oofixvXkIQ/K1bt86I5Lf1+dDdbm52S6XKfVmz6tV64NV4EWgRAQl6iwa77V0NXc4I58MPP+wOHDjQwcLaNlYzIhgKZhi5vnnzZnfcccelZlBDmIkuZx84dVrgGw8yyzrJlY4LnVS0/KQtFGs3Qo0Ln33zlGgUfZy7PbzX6mv7PFD/RaCpBCToTR1Z9WsagfDsc85Rp1i2OILhOL0tGjWOGGNhI6JpqU8RcOrgJQFXuwm5NSAUdCzscL84Wdi4H6ubyHhEOixm/ZurPHy5CN3u9gzawT1ys+sPQATaR0CC3r4xb2WPo/u/7bQ2BNCyxSGufNLE+8QTT3Q333yzv47gurQUsbjO7UzxMBjOXOlY4uaS5yx2rrFIewaJdfRoGlpzq0e3xZnbfvfu3R0xl5u9lVNdnW4xAQl6iwe/TV03QWfLF3vO2afNISzhWnrIg7Vr3OaI7+eff+7WrVvnf42g//33351LsbgR5jDLGi8E0aj0OFd6+DwLaIuOSRgxb1vS4l4iLFofoedlhWv27NmT+ejTNs0J9VUEmkZAgt60EVV/ZhDATU2SlXfeeWfa74h2v/rqq/0Z3HbCGhdEBTMU2+uvv9599tln/hqsdF4U0vKkY4FjdVsgGm78pBSrWNnhwShssbvxxhv9ywGWflzbrEOsq6cF3mlaiIAINJ+ABL35Y9zKHiLiWKqIoZ0wxpY1ksZ88sknXsTDhC5xgWshOHN12//xIjB79mw3Pj4+Iyr95JNP9v+fJPZY9evXr09Mu8oLwujoaEfEeRkgqxzR7tH1edpjB7zwMlDmKXGtnEjqtAjUiIAEvUaDpaamE4gTcQLdrrvuOm+dE1jGNQjtoUOHprmjuwl60ta0BQsWeJENo9KjrvRQ2H/77TefljZ6RGm0Z6GII+Z4A8KCiOOCD/eq4w3Aza787PpLEYF2EpCgt3PcG9FrE2dLn2pZ2RBxS96CxWpbtwgYI/gsLrmKCTrWs0WsHzx40Fu/uLLNrY6w3nDDDQ5rf+7cue6hhx7KLKCIeZqom5hHRdysceIAzAVvyWmS9qY3YqDVCREQgZ4ISNB7wqSLhoFAkoDTNoQYNzpCHq5Rh9vV2C6G8IXWuQk2LwW46OOKCSzZ3SwqfRA8zEtg7SF3O30NI9zpv4oIiEA7CUjQ2znuteg1VrGlQeVnmAbVBJyffJKKWd5Y5Qjf3Xff7a666qrOoSih6xuhjAa4DUu0eJgnPhq9bhHucQey1GKg1UgREIFCCEjQC8GoSooiYBnU2IMd7t1GWFmnJuo7TcCj7XjzzTfdgw8+6I4//nj377//uqmpqc4l1EldF1xwgbv44ou9dR9Gmts+7kGtSUfXyZMC3ohw59o//vijqGFQPSIgAjUkIEGv4aA1sclhZjP6h4iagPMzbj05jkOaW57McLjNcc2feeaZ7vfff+8kb6Eugs/4vT0/aXtZGfzpv3kLolHuFhPA2n7cywXR9lwzMTFRRtNUpwiIQE0ISNBrMlBNbWYo5P2sVacJOFvLvvrqK4+Oum3tPBqxjnBbzvWQswl82t7xPOMSF6lu9UXXyZOeQyAgLypxaWPztE33ioAI1I+ABL1+Y1brFscJsLm2N27cGGuJE2WO9cpPtn0hhGE+dAMSXVcP947jTmfvd5hOlQxv9mx+ksrVrGSLnLe684p7LxY4Gezs5SNuq1rcwLMsQeR+3OlrtZ4oarwIiEBmAhL0zMh0Qz8Eoi516kCAWRMPxQvR5tpwy1j0eYgv0ea4mUm2EhcYFx5iEg126yX5Cm5vE/iouHdLDBO2d2xsLPZc814t8G6sFeHejZB+LwLtISBBb89YD6yniOvChQt9BDm50cmRboFt/C5JvE24Ee958+Z5ATf3eLfOWHQ7dXAPLwl4APoJdAvFvdfEMGH7bA28Hwu8Wz85JY7tdkePHu05zqBbnfq9CIhAPQlI0Os5brVqddLBI2EnQvEm+xoijPj3W8zdbtZ5XDKZfuvulhgmrDe6L77fZybdN3/+fL9MgKCriIAItJuABL3d419Z76MHj9iDEe284h3tRBj01uQjRBFyItx5aSALnooIiEC7CUjQ2z3+jet9mJOdzu3cudOv0TexWF/ZzsZLjIoIiEC7CUjQ2z3+jeo9ke8jIyOdPvUS/FZnAOaJ2LdvX6ZkO3Xus9ouAiKQTECCrtnRKAK49gmy4/CTJlvnDBpHwRKfcOzYsc55640aTHVGBEQgEwEJeiZcurgOBMwV3XRBZ/3cvBB1GBe1UQREoFwCEvRy+ar2ARCwqPomCzrb8NgKqPXzAUwwPVIEhpSABH1IB0bNykeApDCkQx3UwSr5Wt/97rZ4IbqT0BUiIAJGQIKuuSACNSQgQa/hoKnJIlAyAQl6yYBVvQiUQcByuDd5WaEMbqpTBJpMQILe5NFV3xpLYO3atT7X/OTkZK6Meo0FpI6JQAsJSNBbOOjqcv0JcGQq8QEIuooIiIAIQECCrnkgAjUjYFH8inCv2cCpuSJQMgEJesmAVb0IFE1AAXFFE1V9ItAMAhL0ZoyjetEiAhL0Fg22uioCGQhI0DPA0qUiMAwEJOjDMApqgwgMHwEJ+vCNiVokAqkE2pAJT1NABEQgOwEJenZmukMEBk6g6ZnwBg5YDRCBGhKQoNdw0NRkERABERABEYgSkKBrToiACIiACIhAAwhI0BswiOqCCIiACIiACEjQNQdEQAREQAREoAEEJOgNGER1QQREQAREQAQk6JoDIiACIiACItAAAhL0BgyiuiACIiACIiACEnTNAREQAREQARFoAAEJegMGUV0QAREQAREQAQm65oAIiIAIiIAINICABL0Bg6guiIAIiIAIiIAEXXNABERABERABBpAQILegEFUF0RABERABERAgq45IAIiIAIiIAINICBBb8AgqgsiIAIiIAIiIEHXHBABERABERCBBhCQoDdgENUFERABERABEZCgaw6IgAiIgAiIQAMI/BeJnPEZPHxwiQAAAABJRU5ErkJggg==">{{ strtoupper($empleado->apellido_paterno) }} {{ strtoupper($empleado->apellido_materno) }} {{ strtoupper($empleado->nombre) }}</td>


                </tr>

                <tr>
                    <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
                    <td class="text-center pl-5 pr-5" style=" border-bottom: 1px solid #fff;font-size:11px;"></td>
                    <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
                </tr>
                <tr>
                    <td class="text-center" style=" border-bottom: 1px solid #fff;font-size:11px;">MEDICO<br>
                        CEDULA PROF.</td>
                    <td class="text-center" style=" border-bottom: 1px solid #fff;font-size:11px;"></td>
                    <td class="text-center" style=" border-bottom: 1px solid #fff;font-size:11px;">FIRMA DEL PACIENTE</td>
                </tr>
                <tr>
                    <td class="text-center" style="font-size:9px !important;border-bottom: 1px solid #fff;font-size:11px;">8149200</td>
                    <td class="text-center" style=" border-bottom: 1px solid #fff;font-size:11px;"></td>
                    <td class="text-center" style=" border-bottom: 1px solid #fff;font-size:11px;"></td>
                </tr>
            </tbody>
        </table>

    </div>
</body>

</html>
