<!DOCTYPE html>
<html>

<head>
    <title>GONIOMETRÍA</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .letra {
            font-size: 12px !important;
        }

        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }

        .list_none {
            list-style: none;
        }

        .table-bordered {
            border: 1px solid #000000 !important;
        }

        .table-bordered td,
        .table-bordered th {
            border: 1px solid #000000 !important;
        }

        .table td,
        .table th {
            padding: 0rem !important;
            padding-left: 10px;
            height: 1px !important;
        }

    </style>
</head>
<body>
    @php
        $fecha = date('Y-m-d H:i:s');
    @endphp
    <div class="row">
        <div class="col-md-3">
            <img width="135" height="50" class=""
                src="https://has-humanly.com/empresa_dev/storage/firmas/asesor.png" style="">
        </div>
        <div class="col-md-3 text-center">
            <h5><b>GONIOMETRÍA</b></h5>
        </div>
        <div class="col-md-3 text-center">

        </div>
    </div>
    <table class="table table-bordered table-sm letra ml-5 mr-5" style='margin-top:-35px;'>
        <tr>
            <td colspan="2">NOMBRE:</td>

            <td colspan="2">{{ $empleado->nombre }} {{ $empleado->apellido_paterno }}
                {{ $empleado->apellido_materno }}</td>

            <td colspan="2">FECHA:{{ $fecha }}</td>
        </tr>
        <tr>
            <td colspan="2">EDAD:{{ $empleado->edad }}</td>

            <td colspan="2">NÚMERO DE REVISIÓN:{{ $fisioterapia->num_revision }}</td>

            <td colspan="2">NIM:{{ $fisioterapia->nim }}</td>
        </tr>
    </table>
    <table class="table table-bordered table-sm letra ml-5 mr-5">
        <tr>
            <td colspan="1" rowspan="1"><b>MIEMBRO
                    SUPERIOR:</b></td>
            <td colspan="1" rowspan="1"><b>Valores
                    naturales(rom)
                </b></td>
            <td colspan="2"><b>RETEST {{ \Carbon::parse($fisioterapia->updated_at)->format('d/m/Y') }}:</b></td>
            <td colspan="2"><b>TEST @if ($fisioterapia_an != null){{ \Carbon::parse($fisioterapia_an->updated_at)->format('d/m/Y') }}@endif :</b></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Izquierdo</td>
            <td>Derecho</td>
            <td>Izquierdo</td>
            <td>Derecho</td>
        </tr>
        <tr>
            <td><b>Hombro</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Flexión</td>
            <td>180°</td>
            <td>{{ $fisioterapia->s_h_flexion_i }}°</td>
            <td>{{ $fisioterapia->s_h_flexion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_flexion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_flexion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Extensión</td>
            <td>60°</td>
            <td>{{ $fisioterapia->s_h_extension_i }}°</td>
            <td>{{ $fisioterapia->s_h_extension_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_extension_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_extension_d }}@endif°</td>
        </tr>
        <tr>
            <td>Abducción</td>
            <td>180°</td>
            <td>{{ $fisioterapia->s_h_abduccion_i }}°</td>
            <td>{{ $fisioterapia->s_h_abduccion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_abduccion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_abduccion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Rot. Externa</td>
            <td>90°</td>
            <td>{{ $fisioterapia->s_h_r_externa_i }}°</td>
            <td>{{ $fisioterapia->s_h_r_externa_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_r_externa_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_r_externa_d }}@endif°</td>
        </tr>
        <tr>
            <td>Rot. interna</td>
            <td>70°</td>
            <td>{{ $fisioterapia->s_h_r_interna_i }}°</td>
            <td>{{ $fisioterapia->s_h_r_interna_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_r_interna_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_h_r_interna_d }}@endif°</td>
        </tr>
        <tr>
            <td><b>Codo</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Flexión</td>
            <td>150°</td>
            <td>{{ $fisioterapia->s_c_flexion_i }}°</td>
            <td>{{ $fisioterapia->s_c_flexion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_c_flexion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_c_flexion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Pronación</td>
            <td>80°</td>
            <td>{{ $fisioterapia->s_c_pronacion_i }}°</td>
            <td>{{ $fisioterapia->s_c_pronacion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_c_pronacion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_c_pronacion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Supinación</td>
            <td>80°</td>
            <td>{{ $fisioterapia->s_c_supinacion_i }}°</td>
            <td>{{ $fisioterapia->s_c_supinacion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_c_supinacion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_c_supinacion_d }}@endif°</td>
        </tr>
        <tr>
            <td><b>Muñeca</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Flexión</td>
            <td>80°</td>
            <td>{{ $fisioterapia->s_m_flexion_i }}°</td>
            <td>{{ $fisioterapia->s_m_flexion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_flexion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_flexion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Extensión</td>
            <td>70°</td>
            <td>{{ $fisioterapia->s_m_extension_i }}°</td>
            <td>{{ $fisioterapia->s_m_extension_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_extension_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_extension_d }}@endif°</td>
        </tr>
        <tr>
            <td>Desv radial</td>
            <td>20°</td>
            <td>{{ $fisioterapia->s_m_des_radial_i }}°</td>
            <td>{{ $fisioterapia->s_m_des_radial_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_des_radial_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_des_radial_d }}@endif°</td>
        </tr>
        <tr>
            <td>Desv cubital</td>
            <td>30°</td>
            <td>{{ $fisioterapia->s_m_des_cubital_i }}°</td>
            <td>{{ $fisioterapia->s_m_des_cubital_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_des_cubital_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->s_m_des_cubital_d }}@endif°</td>
        </tr>
    </table>
    <table class="table table-bordered table-sm letra ml-5 mr-5">
        <tr>
            <td colspan="1" rowspan="1"><b>MIEMBRO
                    INFERIOR:</b></td>

            <td colspan="1" rowspan="1"><b>Valores
                    naturales(rom)
                </b></td>

            <td colspan="2"><b>RETEST {{ \Carbon::parse($fisioterapia->updated_at)->format('d/m/Y') }}:</b></td>
            <td colspan="2"><b>TEST @if ($fisioterapia_an != null){{ \Carbon::parse($fisioterapia_an->updated_at)->format('d/m/Y') }}@endif</b></td>
        </tr>
        <tr>
            <td></td>

            <td></td>

            <td>Izquierdo</td>
            <td>Derecho</td>
            <td>Izquierdo</td>
            <td>Derecho</td>
        </tr>
        <tr>
            <td><b>Cadera</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Flexión</td>
            <td>120°</td>
            <td>{{ $fisioterapia->i_ca_flexion_i }}°</td>
            <td>{{ $fisioterapia->i_ca_flexion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_flexion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_flexion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Extensión</td>
            <td>30°</td>
            <td>{{ $fisioterapia->i_ca_extension_i }}°</td>
            <td>{{ $fisioterapia->i_ca_extension_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_extension_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_extension_d }}@endif°</td>
        </tr>
        <tr>
            <td>Abducción</td>
            <td>45°</td>
            <td>{{ $fisioterapia->i_ca_abduccion_i }}°</td>
            <td>{{ $fisioterapia->i_ca_abduccion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_abduccion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_abduccion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Aducción</td>
            <td>30°</td>
            <td>{{ $fisioterapia->i_ca_aduccion_i }}°</td>
            <td>{{ $fisioterapia->i_ca_aduccion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_aduccion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_aduccion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Rot. Externa</td>
            <td>45°</td>
            <td>{{ $fisioterapia->i_ca_r_externa_i }}°</td>
            <td>{{ $fisioterapia->i_ca_r_externa_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_r_externa_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_r_externa_d }}@endif°</td>
        </tr>
        <tr>
            <td>Rot. interna</td>
            <td>45°</td>
            <td>{{ $fisioterapia->i_ca_r_interna_i }}°</td>
            <td>{{ $fisioterapia->i_ca_r_interna_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_r_interna_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_ca_r_interna_d }}@endif°</td>
        </tr>
        <tr>
            <td><b>Rodilla</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Flexión</td>
            <td>130°</td>
            <td>{{ $fisioterapia->i_r_flexion_i }}°</td>
            <td>{{ $fisioterapia->i_r_flexion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_r_flexion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_r_flexion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Extensión</td>
            <td>0°</td>
            <td>{{ $fisioterapia->i_r_extension_i }}°</td>
            <td>{{ $fisioterapia->i_r_extension_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_r_extension_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_r_extension_d }}@endif°</td>
        </tr>
        <tr>
            <td><b>Tobillo</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Flexión</td>
            <td>40°</td>
            <td>{{ $fisioterapia->i_t_flexion_i }}°</td>
            <td>{{ $fisioterapia->i_t_flexion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_flexion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_flexion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Extensión</td>
            <td>20°</td>
            <td>{{ $fisioterapia->i_t_extension_i }}°</td>
            <td>{{ $fisioterapia->i_t_extension_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_extension_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_extension_d }}@endif°</td>
        </tr>
        <tr>
            <td>Inversión</td>
            <td>30°</td>
            <td>{{ $fisioterapia->i_t_inversion_i }}°</td>
            <td>{{ $fisioterapia->i_t_inversion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_inversion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_inversion_d }}@endif°</td>
        </tr>
        <tr>
            <td>Eversión</td>
            <td>20°</td>
            <td>{{ $fisioterapia->i_t_eversion_i }}°</td>
            <td>{{ $fisioterapia->i_t_eversion_d }}°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_eversion_i }}@endif°</td>
            <td>@if ($fisioterapia_an != null){{ $fisioterapia_an->i_t_eversion_d }}@endif°</td>
        </tr>
    </table>
    @if($act_vida_cotidina != null || $levantar_objeto != null || $caminar != null || $sentarse != null || $pararse != null || $dormir != null || $actividad_sexual != null || $actividad_social != null || $viajar != null)
    <div style="page-break-after:always;"></div>
    @endif
    <table class="table table-sm letra ml-5 mr-5" style="border-bottom:solid 1px #fff !important;">
        <tr>
            <td colspan="6" style="border-bottom:solid 1px #fff !important;border-top:solid 1px #fff !important;">
                <b>RESUMEN DE VALORACIÓN:</b></td>
        </tr>
        <tr style="border-bottom:solid 1px #fff !important;">
            <td colspan="6" style="border-bottom:solid 1px #fff !important;">
                <p>{{ $fisioterapia->resumen_valoracion }} </p>
                @if ($intensidad_dolor != null)
                    <p>INTENSIDAD DEL DOLOR: {{ $intensidad_dolor }} </p>
                @endif
                @if ($act_vida_cotidina != null)
                    <p>ACTIVIDADES DE LA VIDA COTIDIANA(Lavarse,Vestirse,Etc): {{ $act_vida_cotidina }} </p>
                @endif
                @if ($levantar_objeto != null)
                    <p>LEVANTAR OBJETOS: {{ $levantar_objeto }} </p>
                @endif
                @if ($caminar != null)
                    <p>CAMINAR: {{ $caminar }} </p>
                @endif
                @if ($sentarse != null)
                    <p>SENTARSE: {{ $sentarse }} </p>
                @endif
                @if ($pararse != null)
                    <p>PARARSE: {{ $pararse }} </p>
                @endif
                @if ($dormir != null)
                    <p>DORMIR: {{ $dormir }} </p>
                @endif
                @if ($actividad_sexual != null)
                    <p>ACTIVIDAD SEXUAL: {{ $actividad_sexual }} </p>
                @endif
                @if ($actividad_social != null)
                    <p>ACTIVIDADES SOCIALES(Fiestas,Deportes,etc): {{ $actividad_social }} </p>
                @endif
                @if ($viajar != null)
                    <p>VIAJAR: {{ $viajar }} </p>
                @endif
            </td>
        </tr>
        {{-- <tr>
            <td colspan="2">

            </td>
            <td  colspan="2" style="border-top:1px solid #000;text-align: center;">
                <p>LFT. (RESPONSABLE)
                céd. Prof.</p>
            </td>
            <td colspan="2">
                
            </td>
        </tr> --}}
        <tr style="border-bottom:solid 1px #fff !important;">
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
            <td class="text-center" colspan="2"><img width="95" width="35"
                    src="https://has-humanly.com/empresa_dev/storage/firmas/firma_yarik.png"></td>
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
        </tr>
        <tr style="border-bottom:solid 1px #fff !important;">
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
            <td class="text-center" colspan="2" style="border-bottom:solid 1px #000;"></td>
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
            <td class="text-center" style="border-bottom:solid 1px #fff !important;"></td>
        </tr>
        <tr>
            <td class="text-center" style="border-top:solid 1px #fff !important;"></td>
            <td class="text-center" style="border-top:solid 1px #fff !important;"></td>
            <td class="text-center" colspan="2" style="border-top:solid 1px #fff !important;">LFT. Ana Yarik Murrieta
                Juárez<br>
                Céd. Prof. 12131945 </td>
            <td class="text-center" style="border-top:solid 1px #fff !important;"></td>
            <td class="text-center" style="border-top:solid 1px #fff !important;"></td>
        </tr>
    </table>
</body>
{{-- <script src="{!! asset('js/bootstrap.min.js') !!}"></script> --}}
</html>
