<!DOCTYPE html>
<html>

<head>

    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .letra {
            font-size: 10px !important;
            text-transform: uppercase;
        }
        header {
            position: fixed;
            position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;
                font-size: 10px !important;
            }
        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }

        .list_none {
            list-style: none;
        }
        .cuadrado {
            width: 60px !important;       
            height: 20px !important;     
            border: 1px solid #000; 
        }
        @page  { margin: 80px 50px; }
        #header { position: fixed; left: 0px; right: 0px;top: 5px;float: right; }

    </style>
</head>
<header>
    {{-- <table>
        <tbody>
            <tr>
                <td><img class="" style=""src="https://has-humanly.com/pagina_has/images/logo_30.svg" width="120px" height="40px"/></td>
                <td> <h4 class="font-weight-normal" style="font-size:18px !important;color:#000;">
                        <b>HISTORIA CLÍNICA</b>
                    </h4>
                    <h4 class="font-weight-normal" style="font-size:18px !important;color:#2271B3;">
                        CENTRO DE SALUD CEMEX
                    </h4>
                </td>
                <td><img class="" style=""src="https://has-humanly.com/cemex/storage/app/empresas/1200px-Cemex_logo.svg.png" width="120px" height="40px"/></td>
               
            </tr>
        </tbody>
    </table> --}}
    <div class="col-md-12 row">
        <div class="col-md-3">
            <img src="https://has-humanly.com/pagina_has/images/logo_30.svg" 
             width="120;">
        </div>
        <div class="col-md-6 text-center">
           <b>ASESORES ESPECIALIZADOS EN LABORATORIOS MATRIZ</b> <br>
                21 PONIENTE No. 3711, COL BELISARIO DOMINGUEZ C.P. 72180, PUEBLA, PUE<br>
                TELS. (222)2966608, 2966609, 2966610 y 2224540933
               
        </div>
        <div class="col-md-3">
            <img src="" 
            class="float-right" width="110;"></div>
    </div>
    
</header>
<body class="">
    <div class="content letra">
        {{-- <img class="left" style="float: right;"src="https://has-humanly.com/cemex/storage/app/empresas/1200px-Cemex_logo.svg.png" width="70px" height="30px"/> --}}
        <br><div class="text-center">
            @if($admision_contratistas!=null && $formato=='admision_contratista')
            <h4 class="font-weight-normal" style="font-size:20px">ADMISIÓN CONTRATISTA</h4>
            @else
            <h4 class="font-weight-normal" style="font-size:20px">EXAMEN MÉDICO DE PREADMISIÓN</h4>
            @endif

        </div>
        <div id="identificacion">
            @if($admision_contratistas!=null && $formato=='admision_contratista')
            <div class="p-0 text-center" style="background-color:#000 !important;color:#fff !important;">
                <b>IDENTIFICACION </b>
            </div>
            @else
            <div class="p-0 text-center fondo_titulos">
                <b>IDENTIFICACION </b>
            </div>
            @endif

            <table class="table table-sm table-bordered">
             
                <tbody>
                    <tr>
                        <td>FECHA (Día/Mes/Año): {{ \Carbon::parse($identificacion->fecha)->format('d/m/Y') }}</td>
                        <td>N° de Empleado: {{ $identificacion->numEmpleado }}{{ $empleado->curp }}</td>
                        <td>Departamento: {{ $identificacion->departamento }}</td>
                        <td>Edad: {{ \Carbon::parse($empleado->fecha_nacimiento)->age }}</td>
                        <td>SEXO {{ $empleado->genero }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">APELLIDO PATERNO: {{ $empleado->apellido_paterno }}</td>
                        <td colspan="1">APELLIDO MATERNO: {{ $empleado->apellido_materno }}</td>
                        <td colspan="2">NOMBRE (S): {{ $empleado->nombre }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">ESTADO CIVIL: {{ $identificacion->estadoCivil }}</td>
                        <td colspan="3">ESCOLARIDAD: {{ $identificacion->escolaridad }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">DOMICILIO: {{ $identificacion->domicilio }}</td>
                        <td colspan="3">ESTADO: {{ $identificacion->estado }}</td>
                    </tr>
                    <tr>
                        <td colspan="2"> <span class="">LUGAR DE NACIMIENTO:
                                {{ $identificacion->lugarNacimiento }}</span> </td>
                        <td colspan="3">FECHA DE NACIMIENTO: {{ $empleado->fecha_nacimiento }}</td>
                    </tr>
                    {{-- <tr>
                        <td>CIUDAD Ó EJIDO: {{ $identificacion->ciudad }}</td>
                        <td>MUNICIPIO: {{ $identificacion->municipio }}</td>
                       
                    </tr> --}}
                    <tr>
                        <td colspan="2">EN CASO DE EMERGENCIA LLAMAR A: {{ $identificacion->nom_per_em }}</td>
                        <td colspan="2">PARENTESCO: {{ $identificacion->parentesco }}</td>
                        <td colspan="2">TELEFONO: {{ $identificacion->telefono_1 }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">DOMICILIO: {{ $identificacion->domicilio_em }}</td>
                        <td colspan="2">LUGAR DE TRABAJO:</td>
                        <td>TELEFONO: {{ $identificacion->telefono_2 }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="heredofamiliares">
            @if($admision_contratistas!=null && $formato=='admision_contratista')
            <div class="text-center" style="background-color:#000 !important;color:#fff !important;">
                ANTECEDENTES HEREDOFAMILIARES
            </div>

            @else
            <div class="text-center fondo_titulos" role="alert">
                ANTECEDENTES HEREDOFAMILIARES
            </div>
            @endif
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>
                            <ul class="list_none">
                                <li>1. HEPATITIS</li>
                                <li>2. CANCER</li>
                                <li>3. ASMA</li>
                                <li>4. COLESTEROL ALTO</li>
                                <li>5. EVC</li>
                                <li>6. DIABETES</li>
                                <li>7. OTROS</li>
                            </ul>
                        </td>
                        <td>
                            <ul class="list_none">
                                <li>8. OBESIDAD</li>
                                <li>9. SIDA</li>
                                <li>10. TUBERCULOSIS</li>
                                <li>11. SANGRADO</li>
                                <li>12. ANEMIA</li>
                                <li>13. ALCOHOLISMO</li>

                            </ul>
                        </td>
                        <td>
                            <ul class="list_none">
                                <li>14. ALTA PRESIÓN SANGUINEA</li>
                                <li>15. ATAQUE AL CORAZÓN ANTES DE LOS 55 AÑOS</li>
                                <li>16. DEFECTOS CONGÉNITOS</li>
                                <li>17. PROBLEMAS EMOCIONALES/PSICOLÓGICOS</li>
                            </ul>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="5" style="font-size:7.5px !important;">POR FAVOR INDIQUE SI SU FAMILIA (PADRE,
                            MADRE, HERMANOS (AS) ) TUVO ALGUNA DE LAS ENFERMEDADES ARRIBA MENCIONADAS, Coloca el numero
                            en el renglón de enfermedad
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <th class="text-center fondo_titulos">Familiar</th>
                        <th class="text-center fondo_titulos">Estado</th>
                        <th class="text-center fondo_titulos">Enfermedades</th>
                    </tr>
                    @php
                    $heredofamilia = json_decode($heredofamiliar->data);
                    @endphp
                    @foreach ($heredofamilia as $familiar)
                    <tr>
                        <td>{{ $familiar->familiar }}</td>
                        <td>{{ $familiar->estado }}</td>
                        <td>
                            @if ($familiar->enfermedad != null)
                            @foreach ($familiar->enfermedad as $value)
                            @if($value=='Hepatitis')
                            {{ 1 . ', ' }}
                            @elseif($value=='Cancer')
                            {{ 2 . ', ' }}
                            @elseif($value=='Asma')
                            {{ 3 . ', ' }}
                            @elseif($value=='Colesterol Alto')
                            {{ 4 . ', ' }}
                            @elseif($value=='Evc')
                            {{ 5 . ', ' }}
                            @elseif($value=='Diabetes')
                            {{ 6 . ', ' }}
                            @elseif($value=='Otros')
                            {{ 7 . ', ' }}
                            @elseif($value=='Obesidad')
                            {{ 8 . ', ' }}
                            @elseif($value=='Sida')
                            {{ 9 . ', ' }}
                            @elseif($value=='Tuberculosis')
                            {{ 10 . ', ' }}
                            @elseif($value=='Sangrado')
                            {{ 11 . ', ' }}
                            @elseif($value=='Anemia')
                            {{ 12 . ', ' }}
                            @elseif($value=='Alcoholismo')
                            {{ 13 . ', ' }}
                            @elseif($value=='Alta presión sanguinea')
                            {{ 14 . ', ' }}
                            @elseif($value=='Ataque al corazón antes de los 55 años')
                            {{ 15 . ', ' }}
                            @elseif($value=='Defectos Congénitos')
                            {{ 16 . ', ' }}
                            @elseif($value=='Problemas Emocionales/Psicológicos')
                            {{ 17 . ', ' }}
                            @endif
                            @endforeach
                            @else
                            Ninguno
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="nopatologico">
            @php
            $vacunas = $nopatologico->vacunas;
            @endphp
            @if($admision_contratistas!=null && $formato=='admision_contratista')
            <div class="text-center" style="background-color:#000 !important;color:#fff;">
                ANTECEDENTES PERSONALES NO PATOLÓGICOS
            </div>
            @else
            <div class="text-center fondo_titulos">
                ANTECEDENTES PERSONALES NO PATOLÓGICOS
            </div>
            @endif
           
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <th colspan="6">
                           Vacunas Recibidas
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">Vacuna</th>
                        <th class="text-center">Realizado</th>
                        <th class="text-center">Comentario</th>
                        <th class="text-center">Vacuna</th>
                        <th class="text-center">Realizado</th>
                        <th class="text-center">Comentario</th>
                    </tr>
                    <tr>
                        @foreach ($vacunas->tetano as $value)
                        @if ($value == 'No' || $value == 'no')
                        <td>Tetano</td>
                        <td>No</td>
                        @else
                        @if ($value == 'Tetano')
                        <td>{{ $value }}</td>
                        <td>Si</td>
                        @endif
                        @if ($value != 'no' && $value != 'Tetano' && $value != null)
                        <td>{{ $value }}</td>
                        @endif
                        @if ($value == null)
                        <td>Ninguno</td>
                        @endif
                        @endif

                        @endforeach

                        @foreach ($vacunas->rubeola as $value)
                        @if ($value == 'no')
                        <td>Rubeola</td>
                        <td>No</td>
                        @else
                        @if ($value == 'Rubeola')
                        <td>{{ $value }}</td>
                        <td>Si</td>
                        @endif
                        @if ($value != 'no' && $value != 'Rubeola' && $value != null)
                        <td>{{ $value }}</td>
                        @endif
                        @if ($value == null)
                        <td>Ninguno</td>
                        @endif
                        @endif
                        @endforeach
                    </tr>
                    <tr>
                        @foreach ($vacunas->hepatitis as $value)
                        @if ($value == 'no')
                        <td>Hepatitis</td>
                        <td>No</td>
                        @else
                        @if ($value == 'Hepatitis')
                        <td>{{ $value }}</td>
                        <td>Si</td>
                        @endif
                        @if ($value != 'no' && $value != 'Hepatitis' && $value != null)
                        <td>{{ $value }}</td>
                        @endif
                        @if ($value == null)
                        <td>Ninguno</td>
                        @endif
                        @endif
                        @endforeach
                        @foreach ($vacunas->bcg as $value)
                        @if ($value == 'no')
                        <td>(BCG)</td>
                        <td>No</td>
                        @else
                        @if ($value == '(BCG)')
                        <td>{{ $value }}</td>
                        <td>Si</td>
                        @endif
                        @if ($value != 'no' && $value != '(BCG)' && $value != null)
                        <td>{{ $value }}</td>
                        @endif
                        @if ($value == null)
                        <td>Ninguno</td>
                        @endif
                        @endif
                        @endforeach
                    </tr>
                    <tr>
                        @foreach ($vacunas->influenza as $value)
                        @if ($value == 'no')
                        <td>Influenza</td>
                        <td>No</td>
                        @else
                        @if ($value == 'Influenza')
                        <td>{{ $value }}</td>
                        <td>Si</td>
                        @endif
                        @if ($value != 'no' && $value != 'Influenza' && $value != null)
                        <td>{{ $value }}</td>
                        @endif
                        @if ($value == null)
                        <td>Ninguno</td>
                        @endif
                        @endif
                        @endforeach
                        @foreach ($vacunas->neumococica as $value)
                        @if ($value == 'no')
                        <td>Neumococica</td>
                        <td>No</td>
                        @else
                        @if ($value == 'Neumococica')
                        <td>{{ $value }}</td>
                        <td>Si</td>
                        @endif
                        @if ($value != 'no' && $value != 'Neumococica' && $value != null)
                        <td>{{ $value }}</td>
                        @endif
                        @if ($value == null)
                        <td>Ninguno</td>
                        @endif
                        @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td colspan="2">Otras Vacunas:</td>
                        <td colspan="3">{{ $vacunas->otros }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">Grupo Sanguinio: <br> {{ $nopatologico->g_sanguineo }}</td>
                        <td colspan="3">¿Alguna vez ha recibido usted alguna transfusión sanguinea?
                            {{ $nopatologico->transfusion }}</td>
                    </tr>
                    <tr>
                        <td colspan="6">Mencione los medicamentos que haya tomado en las últimas dos semanas:
                            <br>{{ $nopatologico->medica_ingeridos }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="page-break-after:always;"></div>
            <div class="text-center fondo_titulos">
                USO DEL CIGARRO
            </div>
            @php
            $cigarro = $nopatologico->cigarro;
            $alcohol = $nopatologico->alcohol;
            $drogas = $nopatologico->drogas;
            $du = $nopatologico->drogas_usadas;
            $deporte = $nopatologico->deporte;
            $dieta = $nopatologico->dieta;

            @endphp
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A) ¿FUMA USTED? {{ $cigarro->Cigarro }}</td>
                        <td>B) ¿DESDE QUE EDAD?<br> {{ $cigarro->edad }}</td>
                        <td>C) ¿NUMERO PROMEDIO DE CIGARROS QUE FUMA?<br> {{ $cigarro->frecuencia }}</td>
                        <td>D) ¿A QUE EDAD DEJÓ DE FUMAR?<br> {{ $cigarro->noConsumir }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="text-center fondo_titulos">
                USO DEL ALCOHOL
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A) ¿TOMA BEBIDAS ALCOHÓLICAS?{{ $alcohol->Alcohol }}</td>
                        <td>B) ¿DESDE QUE EDAD? <br> {{ $alcohol->edad }}</td>
                        <td colspan="2">C) ¿CON QUE FRECUENCIA Y CANTIDAD? <br> {{ $alcohol->frecuencia }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="text-center fondo_titulos">
                USO DE DROGAS
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>A) ¿ALGUNA VEZ USÓ DROGAS? {{ $drogas->Drogas }}</td>
                        <td>B) ¿DESDE QUE EDAD? {{ $drogas->edad }}</td>
                        <td colspan="2">C) ¿CON QUE FRECUENCIA Y CANTIDAD? {{ $drogas->frecuencia }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">MENCIONE QUE DROGAS HA USADO:
                            @if ($du != null)
                            @foreach ($du as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @else
                            Ninguna
                            @endif
                        </td>

                        <td>
                            ¿SE LE HA INDICADO ALGUNA DIETA? {{ $dieta->Dieta . ' ' . $dieta->descripcion }}
                        </td>
                        <td>
                            ¿PRACTICA USTED ALGUN DEPORTE? {{ $deporte->Deporte }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">ESPECIFIQUE CUAL Y FRECUENCIA: {{ $deporte->frecuencia }}</td>
                        <td colspan="2">INDIQUE CUÁL ES SU PASATIEMPO FAVORITO: {{ $nopatologico->pasatiempo }}</td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div id="patologico">
            @php
            $enfermedades = $patologico->enfermPadecidas;
            $cl = $patologico->cirugiasLesion;
            @endphp
            @if($admision_contratistas!=null && $formato=='admision_contratista')
            <div class="text-center" style="background-color:#000 !important;color:#fff;">
                ANTECEDENTES PATOLÓGICOS PERSONALES
            </div>
            @else
            <div class="text-center fondo_titulos">
                ANTECEDENTES PATOLÓGICOS PERSONALES
            </div>
            @endif

            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>ENFERMEDADES PADECIDAS:</td>
                        <td colspan="2">
                            @if ($enfermedades == null)
                            Ninguna
                            @else
                            @foreach ($enfermedades as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="fondo_titulos text-center letra">DESCRIPCIÓN</th>
                        <th scope="col" class="fondo_titulos text-center letra">CUÁL</th>
                        <th scope="col" class="fondo_titulos text-center letra">SI</th>
                        <th scope="col" class="fondo_titulos text-center letra">NO</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>TRANSTORNOS EMOCIONALES O PSIQUIATRICOS(ANSIDEDAD, DEPRESIÓN)</td>
                        <td>{{ $patologico->transtornos }}</td>
                        <td>
                            @if($patologico->transtornos!='no' and $patologico->transtornos!=null)
                           X
                            @endif
                        </td>
                        <td>
                            @if($patologico->transtornos=='no' || $patologico->transtornos=='' || $patologico->transtornos==null)
                            X
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>ALERGIAS A MEDICAMENTOS: </td>
                        <td>{{ $patologico->alergiaMedica }}</td>
                        <td>
                            @if($patologico->alergiaMedica!='no' and $patologico->alergiaMedica!=null)
                            X
                            @endif
                         </td>
                        <td>
                            @if($patologico->alergiaMedica=='no' || $patologico->alergiaMedica=='' || $patologico->alergiaMedica==null)
                            X
                            @endif
                        </td>

                    </tr>
                    <tr>
                        <td>ALERGIAS EN LA PIEL O SENSIBILIDAD:</td>
                        <td> {{ $patologico->alergiaPiel }}</td>
                        <td>
                            @if($patologico->alergiaPiel!='no' and $patologico->alergiaPiel!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->alergiaPiel=='no' || $patologico->alergiaPiel=='' || $patologico->alergiaPiel==null)
                            X
                            @endif
                        </td>

                    </tr>
                    <tr>
                        <td>OTRO TIPO DE ALERGIAS: </td>
                        <td> {{ $patologico->alergiaOtro }}</td>
                        <td>
                            @if($patologico->alergiaOtro!='no' and $patologico->alergiaOtro!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->alergiaOtro=='no' || $patologico->alergiaOtro=='' || $patologico->alergiaOtro==null)
                            X
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>TUMORES O CANCER: </td>
                        <td> {{ $patologico->tumorCancer }}</td>
                        <td>
                            @if($patologico->tumorCancer!='no' and $patologico->tumorCancer!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->tumorCancer=='no' || $patologico->tumorCancer=='' || $patologico->tumorCancer==null)
                            X
                            @endif
                        </td>

                    </tr>
                    <tr>
                        <td>PROBLEMAS DE LA VISTA: </td>
                        <td> {{ $patologico->probVista }}</td>
                        <td>
                            @if($patologico->probVista!='no' and $patologico->probVista!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->probVista=='no' || $patologico->probVista==null || $patologico->probVista=='')
                            X
                            @endif
                        </td>

                    </tr>
                    <tr>

                        <td>ENFERMEDAD DEL OIDO:</td>
                        <td> {{ $patologico->enfOido }}</td>
                        <td>
                            @if($patologico->enfOido!='no' and $patologico->enfOido!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->enfOido=='no' || $patologico->enfOido==null || $patologico->enfOido=='')
                            X
                            @endif
                        </td>

                    </tr>
                    <tr>

                        <td>PROBLEMA EN COLUMNA VERTEBRAL: </td>
                        <td> {{ $patologico->probCulumVert }}</td>
                        <td>
                            @if($patologico->probCulumVert!='no' and $patologico->probCulumVert!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->probCulumVert=='no' || $patologico->probCulumVert==null || $patologico->probCulumVert=='')
                            X
                            @endif
                        </td>

                    </tr>
                    <tr>

                        <td>HUESOS Y ARTICULACIONES: </td>
                        <td> {{ $patologico->huesoArticulacion }}</td>
                        <td>
                            @if($patologico->huesoArticulacion!='no' and $patologico->huesoArticulacion!=null)
                            X
                            @endif
                        </td>
                        <td>
                            @if($patologico->huesoArticulacion=='no' || $patologico->huesoArticulacion==null || $patologico->huesoArticulacion=='')
                            X
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">Otros problemas médicos no enlistados:{{ $patologico->otroProbMedico }}</td>
                        


                    </tr>
                </tbody>
            </table>
            <p class="text-center"><b>INDIQUE SI HA TENIDO ALGUNA DE LAS LESIONES O CIRUGIAS SIGUIENTES:</b></p>
            <hr>
            <table class="table table-sm table-bordered">
                <tbody>
                    @foreach ($cl as $json)
                    <tr>
                        @foreach ($json as $key => $value)
                        <td>{{ strtoupper($key) }}</td>
                        <td>{{ strtoupper($value) }}</td>
                        @endforeach
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="1">OTRAS CIRUGÍAS INCLUYENDO ACCIDENTES AUTOMOVILÍSTICOS O DEL TRABAJO:</td>
                        <td colspan="3">{{ $patologico->otra_cirugia }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        @if ($empleado->genero == 'FEMENINO' || $empleado->genero == 'Femenino')
        <div id="genicoobstetrico">
            <div class="text-center fondo_titulos">
                ANTECEDENTES GINECOOBSTETRICOS
            </div>


            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>¿A QUÉ EDAD INICIÓ SU REGLA?</td>
                        <td>{{ $genicoobstetrico->inicioRegla }}</td>
                        <td>¿FRECUENCIA DE REGLA?</td>
                        <td>{{ $genicoobstetrico->fRegla }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNTOS DÍAS DURA SU REGLA?</td>
                        <td>{{ $genicoobstetrico->duraRegla }}</td>
                        <td>¿EDAD QUE INICIÓ SUS RELACIONES SEXUALES?</td>
                        <td>{{ $genicoobstetrico->relacionSexInicio }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁL MÉTODO DE PLANIFICACIÓN HA USADO?</td>
                        <td>{{ $genicoobstetrico->metodoUsado }}</td>
                        <td>¿CUÁL FUE LA FECHA DE SU ÚLTIMA MENSTRUACION?</td>
                        <td>{{ $genicoobstetrico->ultimaMestruacion }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNTOS EMBARAZOS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numEmbarazos }}</td>
                        <td>¿CUANTOS PARTOS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numPartos }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNTAS CESÁREAS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numCesareas }}</td>
                        <td>¿CUÁNTOS ABORTOS HA TENIDO?</td>
                        <td>{{ $genicoobstetrico->numAbortos }}</td>
                    </tr>
                    <tr>
                        <td>¿CUÁNDO SE REALIZÓ EL ÚLTIMO EXÁMEN DEL CÁNCER DE MATRIZ?</td>
                        <td>{{ $genicoobstetrico->fecha_examenCancer }}</td>
                        <td> ¿CUÁNDO SE REALIZÓ EL ÚLTIMO EXÁMEN DE MAMAS?</td>
                        <td>{{ $genicoobstetrico->fecha_examenMamas }}</td>
                    </tr>
                    <tr>
                        <td>¿A QUÉ EDAD TUVO SU MENOPAUSIA?</td>
                        <td class="border-right">{{ $genicoobstetrico->edadMenopausia }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        @endif
        @if($formato!='admision_contratista')
        <div id="historialaboral">
            @php
            $hl = $historiaLaboral->trabajoRealizados;
            $tm = $historiaLaboral->trabajoMateriales;
            $empleos = $historiaLaboral->empleos;
            $equipo = $historiaLaboral->equipoSegActual;
            $vista = $historiaLaboral->examenVista;
            $lentes = $historiaLaboral->lentesContacto;
            $lugares = $historiaLaboral->lugaresVividos;
            @endphp
            <div class="text-center fondo_titulos">
                HISTORIAL LABORAL
            </div>
            <table class="table table-sm table-bordered">
                <tbody>
                    <tr>
                        <td>
                            1.- HA REALIZADO UNO DE LOS SIGUIENTES TRABAJOS:
                        </td>
                        <td>
                            @if ($hl == null)
                            Ninguno
                            @else
                            @foreach ($hl as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @endif
                        </td>
                        {{-- <td>Otros Trabajos Realizados:</td>
                        <td>{{ $historiaLaboral->otrosTrabajos }}</td> --}}
                    </tr>
                    <tr>
                        <td>2.- ¿SE HA EXPUESTO O HA TRABAJADO CON ALGUNO DE ESTOS MATERIALES?</td>
                        <td>
                            @if ($tm == null)
                            Ninguno
                            @else
                            @foreach ($tm as $value)
                            {{ $value . ', ' }}
                            @endforeach
                            @endif
                        </td>
                        <td>Otras exposiciones:</td>
                        <td>{{ $historiaLaboral->otrasExposiciones }}</td </tr> </tbody> </table> @foreach ($empleos as
                            $empleo) <div class="text-center fondo_titulos">
                        @if ($empleo->tipo == 1)
                        EMPLEO ACTUAL
                        @elseif ($empleo->tipo==2)
                        EMPLEO ANTERIOR
                        @elseif ($empleo->tipo==3)
                        EMPLEO ANTERIOR
                        @endif
        </div>
        <hr>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>NOMBRE DE LA EMPRESA: {{ $empleo->nombre }}</td>
                    <td>PUESTO: {{ $empleo->puesto }}</td>
                </tr>
                <tr>
                    <td>DESCRIPCIÓN DE SU ACTIVIDAD:{{ $empleo->actividad }}</td>
                    <td>DURACIÓN DEL EMPLEO: {{ $empleo->duracion }}</td>
                </tr>
                <tr>
                    <td colspan="2">EQUIPO DE SEGURIDAD USADO: {{ $empleo->seguridad }}</td>
                </tr>
                <tr>
                    <td colspan="2">HA SUFRIDO ENFERMEDADES RELACIONADAS AL TRABAJO:
                        {{ $empleo->danos }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">HA SUFRIDO ACCIDENTE DE TRABAJO QUE GENERARA INCAPACIDAD O SECUELAS
                        {{ $empleo->danos }}
                    </td>
                </tr>
            </tbody>
        </table>
        @endforeach
        <div style="page-break-after:always;"></div>
        <div class="text-center fondo_titulos">
            EQUIPO DE SEGURIDAD USADO EN EL PUESTO ACTUAL
        </div>

        <hr>
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td class="border-0 text-uppercase">¿Tiene alguna limitante para utilizar EPP(Equipo de Protección Personal)?</td>
                    <td class="border-0">{{ $historiaLaboral->epp }}</td>
                    <td class="border-0"></td>
                    <td class="border-0"></td>
                </tr>
                <tr>
                    <td>EQUIPO:</td>
                    <td colspan="3">
                        @if ($equipo == null)
                        Ninguno
                        @else
                        @foreach ($equipo as $value)
                            @if($value=='R')
                            Respiradores,
                            @elseif($value=='CSC')
                            Calzado de seguridad con/sin casquillo,
                            @elseif($value=='MP')
                            Mascarillas para polvo,
                            @elseif($value=='TA')
                            Tapones auditivos,
                            @elseif($value=='C')
                            Cascos,
                            @elseif($value=='Res')
                            Respirador,
                            @elseif($value=='LS')
                            Lentes de seguridad con o sin aumento,
                            @elseif($value=='LimEPP')
                            Limitante para utilizar EPP(Equipo de protección personal),
                            @endif
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>¿USA LENTES GRADUADOS? {{ $vista[0] }}</td>
                    <td>FECHA DEL ÚLTIMO EXÁMEN VISUAL: {{ $vista[1] }}</td>
                    @if (count($lentes) == 1)
                    <td>¿USA LENTES DE CONTACTO? {{ $lentes[0] }}</td>
                    <td>¿DE QUE TIPO?</td>
                    @else
                    <td>¿USA LENTES DE CONTACTO? {{ $lentes[0] }}</td>
                    <td>¿DE QUE TIPO? {{ $lentes[1] }}</td>
                    @endif
                </tr>
                <tr>
                    <td colspan="2">OTRAS EXPOSICIONES: {{ $historiaLaboral->otras_exposiciones }}</td>
                    <td colspan="2">¿ALGUIEN DE SU FAMILIA TRABAJA CON MATERIALES PELIGROSOS? (ASBESTOS, PLOMO, ETC.)
                        {{ $historiaLaboral->famMaterialPeligroso }}</td>
                </tr>
                <tr>
                    <td colspan="2">ALGUNA VEZ HA VIVIDO CERCA DE:FABRICAS, BASURERO, MINA, OTRO LUGAR QUE GENERE
                        RESIDUOS PELIGROSOS
                        @if ($historiaLaboral->lugaresVividos == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->lugaresVividos as $value)
                        {{ $value . ', ' }}
                        @endforeach

                        @endif
                    </td>
                    <td colspan="2">ALGUNA OTRA EXPOSICIÓN A MATERIALES PELIGROSOS:
                        {{ $historiaLaboral->expMaterialPeligroso }}
                    </td>
                </tr>
            </tbody>
        </table>

        <div class="text-center fondo_titulos">
            INTERROGATORIO POR APARATOS Y SISTEMAS
        </div>
        <hr>
        <table class="table table-sm table-bordered">
            <thead>
                <tr class="text-center">
                    <td colspan="4">¿DURANTE EL AÑO PASADO TUVO USTED ALGUNO DE LOS SIGUIENTES SÍNTOMAS?</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>Síntomas</td>
                    <td></td>
                    <td>Síntomas</td>
                </tr>
                <tr>
                    <td>NEUROLÓGICO / PSICOLÓGICO</td>
                    <td>
                        @if ($historiaLaboral->neurologico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->neurologico as $value)
                       
                        @if($value=='DCFI')
                        Dolores de la cabeza frecuentes o intensos, 
                        @elseif($value=='DFD')
                       Dificultad para dormir,
                        @elseif($value=='VM')
                         Vertigo o mareos,
                        @elseif($value=='PM(C)')
                        Problemas con la memoria(Concentración), 
                        @elseif($value=='Tem')
                        Temblor, 
                        @elseif($value=='Ner')
                        Nerviosismo, 
                        @elseif($value=='Dep')
                       Depresión, 
                        @elseif($value=='AC')
                       Ataques o Convulsiones, 
                        @elseif($value=='PAPC')
                        Paralisis de alguna parte del cuerpo, 
                        @elseif($value=='PET')
                        Problemas de estres en el trabajo, 
                        @else
                        Problemas en tu vida familiar,
                        @endif
                        @endforeach
                        @endif
                    </td>
                    <td>CARDIOVASCULAR</td>
                    <td>
                        @if ($historiaLaboral->cardiovasucular == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->cardiovasucular as $value)
                        @if($value=='DP')
                        Dolor de pecho, 
                        @elseif($value=='Pal')
                        Palpitaciones,
                        @elseif($value=='HT')
                        Hinchazón de tobillos,
                        @else
                        Calambre en las piernas al caminar
                        @endif
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>GASTRO INTESTINAL</td>
                    <td>
                        @if ($historiaLaboral->gastroIntestinal == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->gastroIntestinal as $value)
                        @if($value=='PT')
                        Problemas al tragar, 
                        @elseif($value=='DAI')
                        Dolor abdominal/Indigestion cronica,
                        @elseif($value=='CE')
                        Cambio en sus evacuaciones,
                        @elseif($value=='DP')
                        Diarrea persistente,
                        @elseif($value=='ENS')
                        Evacuaciones negras o sangre,
                        @else
                        Vomitos repetitivos
                        @endif
                        @endforeach
                        @endif
                    </td>
                    <td>PULMONAR</td>
                    <td>
                        @if ($historiaLaboral->pulmunar == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->pulmunar as $value)
                        @if($value=='FA')
                        Le falta el aire, 
                        @elseif($value=='SP')
                        Silbidos en el pecho,
                        @else
                        Tos persistente,
                        @endif
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>GENITOURINARIO</td>
                    <td>
                        @if ($historiaLaboral->genitourinario == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->genitourinario as $value)
                        @if($value=='PNOMU')
                        Por la noche orina más de una vez, 
                        @elseif($value=='OSPR')
                        Orina con sangre/Piedras en el riñon,
                        @else
                        Problemas al orinar,
                        @endif
                        @endforeach
                        @endif
                    </td>
                    <td>ENDOCRINO</td>
                    <td>
                        @if ($historiaLaboral->endocrino == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->endocrino as $value)
                        Aumento o perdida de peso de mas de 5kgs
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>MUSCOLOESQUELETICO</td>
                    <td>
                        @if ($historiaLaboral->musculoesqueletico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->musculoesqueletico as $value)
                        @if($value=='DA')
                        Dolor Articulaciones, 
                        @elseif($value=='PEC')
                        Problemas espalda/cuello,
                        @else
                        Cansancio excesivo,
                        @endif
                        @endforeach
                        @endif
                    </td>
                    <td>INMUNOLÓGICO</td>
                    <td>
                        @if ($historiaLaboral->inmunologico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->inmunologico as $value)
                        @if($value=='GC')
                        Ganglo en cuello, 
                        @elseif($value=='AI')
                        Axila o ingle,
                        @endif
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>DERMATOLÓGICO</td>
                    <td>
                        @if ($historiaLaboral->dermatologico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->dermatologico as $value)
                        Problemas en la piel
                        @endforeach
                        @endif
                    </td>
                    <td>HEMATOLÓGICO</td>
                    <td>
                        @if ($historiaLaboral->hematologico == null)
                        Ninguno
                        @else
                        @foreach ($historiaLaboral->hematologico as $value)
                        Sangrado poco comunes
                        @endforeach
                        @endif
                    </td>
                </tr>
                {{-- <tr class="text-center">
                        <td colspan="4">Alergias</td>
                    </tr>
                    <tr>
                        <td colspan="2">Reacción alergica a medicamentos:
                            @if ($historiaLaboral->alergiaMedicamentos == 'no')
                                No
                            @else
                                Si
                            @endif
                        </td>
                        <td colspan="2">¿Algún problema de salud en este momento?
                            {{ $historiaLaboral->problemaSalud }}</td>
                </tr> --}}
                <tr class="text-center">
                    <td colspan="4"><b>REPRODUCTIVO</b></td>
                </tr>

                @if ($empleado->genero == 'FEMENINO' || $empleado->genero == 'Femenino')
                <tr>
                    <td>SE HA PRESENTADO LO SIGUIENTE:</td>
                    <td colspan="3">
                        @if ($historiaLaboral->problemaMujer == null)
                        NINGUN PROBLEMA PRESENTADO
                        @else
                        @foreach ($historiaLaboral->problemaMujer as $value)
                        {{ $value . ' ' }}
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2">¿Ha tenido abortos?¿En que año? {{ $historiaLaboral->abortos }}</td>
                    <td colspan="2">Si esta embarazada cual es la fecha probable de parto:
                        {{ $historiaLaboral->fechaParto }}</td>
                </tr>
                @else
                <tr>
                    <td>SE HA PRESENTADO LO SIGUIENTE:</td>
                    <td>
                        @if ($historiaLaboral->problemaHombre == null)
                        NINGUN PROBLEMA PRESENTADO
                        @else
                        @foreach ($historiaLaboral->problemaHombre as $value)
                        {{ $value . ' ' }}
                        @endforeach
                        @endif
                    </td>
                    <td colspan="2">OTROS PADECIMIENTOS: {{ $historiaLaboral->padecimientoHombre }}</td>
                </tr>
                @endif

            </tbody>
        </table>
    </div>
    @endif

    <div id="examenFisico">
        @if($admision_contratistas!=null && $formato=='admision_contratista')
        <div class="text-center" style="background-color:#000 !important;color:#fff !important;">
            EXAMEN FÍSICO
        </div>
        @else
        <div class="text-center fondo_color">
            EXAMEN FÍSICO
        </div>
        @endif
        <table class="table table-sm table-bordered">
            <tr>
                <td colspan="2">APARIENCIA GENERAL: {{ $examen->apariencia }}</td>
                <td>TEMPERATURA: {{ $examen->temperatura }}</td>
                <td>FC: {{ $examen->fc }}</td>
                <td>FR: {{ $examen->fr }}</td>
            </tr>
            <tr>
                <td>ALTURA: {{ $examen->altura }}</td>
                <td>PESO: {{ $examen->peso }}</td>
                <td>IMC: {{ $examen->imc }}</td>
                <td>PRESIÓN SANGUÍNEA EN REPOSO(BRAZO DERECHO):{{ $examen->psBrazoDerecho }}</td>
                <td>BRAZO IZQUIERDO: {{ $examen->psBrazoDerecho }}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <th>SIN DATOS PATOLOGICOS</th>
                <th>OBSERVACIONES DETECTADAS </th>
                <th colspan="2">INTERPRETACION DE ESTUDIOS DE GABINETE REALIZADOS
                    POR EL PERSONAL MÉDICO</th>
            </tr>
            <tr>
                <td colspan="2">CABEZA, CUERO CABELLUDO / OJOS</td>
                <td class="text-center">
                    @if(empty($examen->cabezaCueroOjos))
                    X
                    @endif
                </td>
                <td>
                    {{ $examen->cabezaCueroOjos }}
                    </td>
                <td colspan="2">AUDIOMETRIA: @if(isset($examen->audiometria_hf)){{ $examen->audiometria_hf }}@endif</td>
            </tr>
            <tr>
                <td colspan="2"> OIDOS / NARIZ / BOCA</td>
                <td class="text-center">
                    @if(empty($examen->oidoNarizBoca))
                    X
                    @endif
                </td>
                <td>{{ $examen->oidoNarizBoca }}</td>
                <td colspan="2">ESPIROMETRIA: @if(isset($examen->espirometria_hf)){{ $examen->espirometria_hf }}@endif</td>
            </tr>
            <tr>
                <td colspan="2"> DIENTES / FARINGE</td>
                <td class="text-center">
                    @if(empty($examen->dientesFaringe))
                    X
                    @endif
                </td>
                <td>{{ $examen->dientesFaringe }}</td>
                <td colspan="2">RADIOGRAFIA TORAX: @if(isset($examen->ra_torax)){{ $examen->ra_torax }}@endif</td>
            </tr>
            <tr>
                <td colspan="2">CUELLO</td>
                <td class="text-center">
                    @if(empty($examen->cuello))
                    X
                    @endif
                </td>
                <td>{{ $examen->cuello }}</td>
                <td colspan="2">RADIOGRAFIA COLUMNA: @if(isset($examen->ra_columna)){{ $examen->ra_columna }}@endif</td>
            </tr>
            <tr>
                <td colspan="2">TIROIDES</td>
                <td class="text-center">
                    @if(empty($examen->tiroides))
                    X
                    @endif
                </td>
                <td>{{ $examen->tiroides }}</td>
                <td colspan="2">METABOLICO: @if(isset($examen->metabolico)){{ $examen->metabolico }}@endif</td>
            </tr>
            <tr>
                <td colspan="2">NODO LINFÁTICO</td>
                <td class="text-center">
                    @if(empty($examen->tiroides))
                    X
                    @endif
                </td>
                <td></td>
                <td colspan="2"></td>
            </tr>

            {{-- <tr>
                    <td colspan="2">Torak y Pulmones</td>
                    <td>{{ $examen->toraxPulmones }}</td>
            <td>Pecho</td>
            <td>{{ $examen->pecho }}</td>
            </tr> --}}
            <tr>
                <td colspan="2">CORAZÓN</td>
                <td class="text-center">
                    @if(empty($examen->corazon))
                    X
                    @endif
                </td>
                <td>{{ $examen->corazon }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">ABDOMEN</td>
                <td class="text-center">
                    @if(empty($examen->abdomen))
                    X
                    @endif
                </td>
                <td>{{ $examen->abdomen }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">GENITALES</td>
                <td class="text-center">
                    @if(empty($examen->genitales))
                    X
                    @endif
                </td>
                <td>{{ $examen->genitales }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">EXTREMIDADES</td>
                <td class="text-center">
                    @if(empty($examen->extremidades))
                    X
                    @endif
                </td>
                <td>{{ $examen->extremidades }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">MUSCULOESQUELETO</td>
                <td class="text-center">
                    @if(empty($examen->musculoesqueleto))
                    X
                    @endif
                </td>
                <td>{{ $examen->musculoesqueleto }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">REFLEJOS Y NEUROLÓGICOS</td>
                <td class="text-center">
                    @if(empty($examen->reflejosNeurologicos))
                    X
                    @endif
                </td>
                <td>{{ $examen->reflejosNeurologicos }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">ESTADO MENTAL</td>
                <td class="text-center">
                    @if(empty($examen->estadoMental))
                    X
                    @endif
                </td>
                <td>{{ $examen->estadoMental }}</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">OTROS COMENTARIOS</td>
                <td class="text-center">
                    @if(empty($examen->otrosComentarios))
                    X
                    @endif
                </td>
                <td>{{ $examen->otrosComentarios }}</td>
                <td colspan="2"></td>
            </tr>

            <tr class="text-center">
                <td colspan="6">AGUDEZA VISUAL</td>
            </tr>
            <tr>
                <td colspan="1">OJO IZQUIERDO</td>
                <td colspan="2">{{ $examen->ojoIzquierdo }}</td>
                <td colspan="1">OJO DERECHO</td>
                <td colspan="2"> {{ $examen->ojoDerecho }}</td>
            </tr>
            <tr class="text-center">
                <td colspan="6">RECOMENDACIONES DE ACUERDO A RESULTADOS</td>
            </tr>
            <tr>
                <td colspan="3">USO DE EPP:</td>
                <td>{{ $examen->uso_epp }}</td>
                <td>AUDIOMETRIA</td>
                <td>{{ $examen->audiometria }}</td>
            </tr>
            <tr>
                <td colspan="3">RESPIRADORES</td>
                <td>{{ $examen->respiradores }}</td>
                <td>REENTRENAR EN:</td>
                <td>{{ $examen->reentrenar }}</td>
            </tr>
            <tr>
                <td colspan="3">OTROS:</td>
                <td colspan="3">{{ $examen->otrasRecomend }}</td>
            </tr>
            <tr class="text-center">
                <td colspan="6">RECOMENDACIONES DE ESTILO DE VIDA</td>
            </tr>
            <tr>

                <td colspan="6">
                    @if ($examen->recomEstiloVida == null)
                    SIN RECOMENDACIONES
                    @else
                    @foreach ($examen->recomEstiloVida as $value)
                        @if($value=='Df')
                            Dejar de fumar,
                            @elseif($value=='RA')
                            Reducir consume de alcohol,
                            @elseif($value=='BP')
                            Bajar de peso,
                            @elseif($value=='TRC')
                            Tratamineto para reducir de colesterol,
                            @elseif($value=='ME')
                            Manejo de estres,
                            @elseif($value=='EPP')
                            Examen del pecho personal,
                            @elseif($value=='ETP')
                            Examen Testicular Personal,
                            @elseif($value=='RE')
                            Rutina de ejercicio,
                            @elseif($value=='PRE')
                            Programa regular de ejercicios,
                            @else
                            Otros,
                        @endif
                    <!-- {{ $value . ', ' }} -->
                    @endforeach
                    @endif
                </td>
            </tr>
            <tr class="text-center">
                    <td colspan="5">Diagnostico Audiológico</td>
                </tr>
                <tr>
                    <td colspan="5">{{ $examen->diagnosticoAudio }}</td>
            </tr>
        </table>
    </div>
  
    {{-- <div class="somnolencia">
        <p class="text-center">ESCALA DE SOMNOLENCIA DE EPWORTH</p>
      
        <p>Escala de sueño de Epworth</p>
        <p><b>PREGUNTA</b>¿Con que frecuencia se queda Ud. dormido en las siguientes situaciones?.
            Incluso si no ha realizado recientemente alguna de las actividades mencionadas a continuación,trate de imaginar en que medida le afectarían.</p>
        <p>Utilice la siguiente escala y elija la cifra adecuada par cada situación:</p>
        <ul>
          <li>0 = Nunca se ha dormido</li>
          <li>1 = Escasa posibilidad de dormirse</li>
          <li>2 = Moderada posibilidad de dormirse</li>
          <li>3 = Elevada posibilidad de dormirse</li>
        </ul>
        <table class="table table-sm table-bordered">
            <tr>
                <th colspan="4">Situación</th>
                <th>Puntuación</th>
            </tr>
            <tr>
                <td colspan="4">Sentado y leyendo</td>
                <td>
                    @if($somnolencia->sentado_leyendo!=6)
                        {{ $somnolencia->sentado_leyendo }}
                    @else
                        N/A
                    @endif
                </td>

            </tr>
            <tr>
                <td colspan="4">Viendo la T.V.</td>
                <td>
                    @if($somnolencia->tv!=6)
                    {{ $somnolencia->tv}}
                    @else
                        N/A
                    @endif
                </td>
               
            </tr>
            <tr>
                <td colspan="4">Sentado,inactivo en un espectaculo(teatro)</td>
                <td>
                    @if($somnolencia->teatro!=6)
                    {{ $somnolencia->teatro}}
                    @else
                        N/A
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">En auto,como copiloto de un viaje de una hora</td>
                <td>
                    @if($somnolencia->auto_copiloto!=6)
                    {{ $somnolencia->auto_copiloto}}
                    @else
                        N/A
                    @endif
                 </td>
               
            </tr>
            <tr>
                <td colspan="4">Recostado a media tarde</td>
                <td>
                    @if($somnolencia->recostado!=6)
                    {{ $somnolencia->recostado}}
                    @else
                        N/A
                    @endif
                 </td>
               
            </tr>
            <tr>
                <td colspan="4">Sentado y conversando con alguien</td>
                <td>
                    @if($somnolencia->conversando!=6)
                    {{ $somnolencia->conversando}}
                    @else
                        N/A
                    @endif
                 </td>
               
            </tr>
            <tr>
                <td colspan="4">Sentado despues de la comida(sin toma alcohol)</td>
                <td>
                    @if($somnolencia->sentado_comida!=6)
                    {{ $somnolencia->sentado_comida}}
                    @else
                        N/A
                    @endif
                 </td>
               
            </tr>
            <tr>
                <td colspan="4">En su auto cuando se para durante algunos minutos debido al trafico</td>
                <td>
                    @if($somnolencia->trafico!=6)
                    {{ $somnolencia->trafico}}
                    @else
                        N/A
                    @endif
                 </td>
            </tr>
            <tr>
                <td colspan="4">Puntuación total(max 24)</td>
                <td>
                    @if($somnolencia->trafico!=6 || $somnolencia->tv!=6)
                    {{$somnolencia->sentado_leyendo+$somnolencia->tv+$somnolencia->teatro+$somnolencia->auto_copiloto+$somnolencia->recostado+$somnolencia->conversando+$somnolencia->sentado_comida+$somnolencia->trafico}}
                    @else
                        N/A
                    @endif
                    
                 </td>
            </tr>
        </table>
        <table class="table table-sm table-bordered">
            <tr>
                <td colspan="4">1-6 Puntos:Sueño Normal</td>
            </tr>
            <tr>
                <td colspan="4">7-8 Puntos:Somnolencia Media</td>
            </tr>
            <tr>
                <td colspan="4">9-24 Puntos:Somnolencia Anómala(posiblemente patológica)</td>
            </tr>
        </table>
        
    </div> --}}
 
   
    @if($admision_contratistas!=null && $formato=='admision_contratista')
    <div class="admision_contratista">
        <p class="text-center"><b>INTERROGATORIO Y EXPLORACIÓN MÉDICA</b></p>


        <p>Dirigido a identificar factores de Riesgo para realizar Trabajos</p>
        <p class="text-center">TRABAJOS EN ALTURAS</p>
        <table class="table table-sm table-bordered">
            <tr>
                <th colspan="4" style="background-color:#000;color:#fff !important;">Preguntas</th>
                <th>Si</th>
                <th>No</th>
            </tr>
            <tr>
                <td colspan="4">¿Alguna vez ha presentado mareos o vértigos</td>
                <td>
                    @if($admision_contratistas->vertigos=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->vertigos=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* …cuando está a nivel de piso?</td>
                <td>
                    @if($admision_contratistas->vertigo_piso=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->vertigo_piso=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* …cuando se encuentra parado en una altura mayor a 2 m.?</td>
                <td>
                    @if($admision_contratistas->altura=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->altura=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Ha presentado lesiones en oídos que le hayan roto la membrana timpánica?</td>
                <td>
                    @if($admision_contratistas->membrana=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->membrana=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <h5>** Exploración médica</h5>
            <tr>
                <td colspan="4">* Revisión de marcha punta - talón </td>
                <td>
                    @if($admision_contratistas->p_talon=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->p_talon=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Revisión equilibrio dedo nariz con ojos cerrados</td>
                <td>
                    @if($admision_contratistas->e_cerrados=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->e_cerrados=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Caminar por línea </td>
                <td>
                    @if($admision_contratistas->linea=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->linea=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="fondo_titulos" colspan="4">Cumple con los requerimientos médicos para trabajos en alturas (
                    SATISFACTORIO)</td>
                <td>
                    @if($admision_contratistas->t_altura=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->t_altura=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
        </table>
        <p class="text-center">TRABAJOS EN ESPACIOS CONFINADOS</p>
        <table class="table table-sm table-bordered">
            <tr>
                <th colspan="4" style="background-color:#000;color:#fff !important;">Preguntas</th>
                <th>Si</th>
                <th>No</th>
            </tr>
            <tr>
                <td colspan="4">* Cuando se encuentra en espacios cerrados, ¿se desespera?</td>
                <td>
                    @if($admision_contratistas->e_cerrados=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->e_cerrados=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Tiene miedos (fobias) para trabajar en lugares cerrados, estrechos, con poca
                    iluminación y poco aire?</td>
                <td>
                    @if($admision_contratistas->fobias=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->fobias=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Cuando se encuentra en espacios estrechos, ¿siente que no puede respirar bien?</td>
                <td>
                    @if($admision_contratistas->e_estrechos=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->e_estrechos=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="fondo_titulos" colspan="4">Cumple con los requerimientos médicos para trabajos en espacios
                    confinados ( SATISFACTORIO)</td>
                <td>
                    @if($admision_contratistas->e_confinados=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->e_confinados=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
        </table>
        <p class="text-center">TRABAJOS DE LEVANTAMIENTO Y MOVIMIENTO DE CARGAS</p>
        <table class="table table-sm table-bordered">
            <tr>
                <th colspan="4" style="background-color:#000;color:#fff !important;">Preguntas</th>
                <th>Si</th>
                <th>No</th>
            </tr>
            <tr>
                <td colspan="4">* ¿Ha tenido cirugías en su espalda (columna vertebral)?</td>
                <td>
                    @if($admision_contratistas->c_vertebral=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->c_vertebral=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Se ha fracturado alguna vertebra?</td>
                <td>
                    @if($admision_contratistas->f_vertebral=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->f_vertebral=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">*¿Ha padecido o padece de dolores de espalda?</td>
                <td>
                    @if($admision_contratistas->d_espalda=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->d_espalda=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">*¿Ha tenido dolores de la ciática? </td>
                <td>
                    @if($admision_contratistas->d_ciatica=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->d_ciatica=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Se le entumen sus brazos o piernas sin razón aparente? </td>
                <td>
                    @if($admision_contratistas->entumen_pb=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->entumen_pb=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Tiene alguna limitación de movimiento de brazos, piernas o espalda? </td>
                <td>
                    @if($admision_contratistas->limitacion_bpe=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->limitacion_bpe=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">*¿Tiene algún miembro inferior (pierna) más corto que el otro? </td>
                <td>
                    @if($admision_contratistas->m_inferior=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->m_inferior=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <p>** Exploración médica</p>
            <tr>
                <td colspan="4">* Revisión de curvaturas fisiológicas de columna</td>
                <td>
                    @if($admision_contratistas->fisiologicas=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->fisiologicas=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Revisión de asimetría muscular de miembros inferiores, superiores y de columna</td>
                <td>
                    @if($admision_contratistas->fisiologicas=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">* Revisión de alineación de caderas</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">* Marcha punta - talón</td>
                <td>
                    @if($admision_contratistas->punta_t=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->punta_t=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Palpación de apófisis espinosas (cervicales, dorsales y lumbares)</td>
                <td>
                    @if($admision_contratistas->apofisis=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->apofisis=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Revisión (presión) de articulaciones sacroilíacas</td>
                <td>
                    @if($admision_contratistas->sacroiliacas=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->sacroiliacas=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Revisión completa de arcos de movimiento de columna, miembros inferiores y superiores.
                </td>
                <td>
                    @if($admision_contratistas->m_is=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->m_is=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Revisión de signo Lasague y Lasague en dos tiempos.</td>
                <td>
                    @if($admision_contratistas->Lasague=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->Lasague=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="fondo_titulos" colspan="4">Cumple con los requerimientos médicos para trabajos donde tenga
                    que levantar o empujar más de 25 kgs. ( SATISFACTORIO)</td>
                <td>
                    @if($admision_contratistas->t_altura=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->t_altura=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
        </table>
        <p class="text-center">FUNCIÓN PULMONAR</p>
        <table class="table table-sm table-bordered">
            <tr>
                <th colspan="4" style="background-color:#000;color:#fff !important;">Preguntas</th>
                <th>Si</th>
                <th>No</th>
            </tr>
            <tr>
                <td colspan="4">* ¿Le ha faltado respiración o ha sentido opresión en el pecho?</td>
                <td>
                    @if($admision_contratistas->opresion=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->opresion=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Ha tosido por las mañanas en los últimos 12 meses?</td>
                <td>
                    @if($admision_contratistas->tosido=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->tosido=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="fondo_titulos" colspan="4">Cumple con los requerimientos médicos para trabajos donde esté
                    expuesto a polvos o humos</td>
                <td>
                    @if($admision_contratistas->t_altura=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->t_altura=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
        </table>
        <p class="text-center">AUDITIVA</p>
        <table class="table table-sm table-bordered">
            <tr>
                <th colspan="4" style="background-color:#000;color:#fff !important;">Preguntas</th>
                <th>Si</th>
                <th>No</th>
            </tr>
            <tr>
                <td colspan="4">* ¿Ha notado recientemente cambios en su capacidad auditiva?</td>
                <td>
                    @if($admision_contratistas->c_auditiva=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->c_auditiva=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Ha notado zumbidos o pitidos en sus oídos?</td>
                <td>
                    @if($admision_contratistas->o_zumbidos=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->o_zumbidos=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* ¿Le resulta difícil participar en una conversación?</td>
                <td>
                    @if($admision_contratistas->conversacion=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->conversacion=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            ** Exploración médica
            <tr>
                <td colspan="4">* Revisión de conducto auditivo, buscando perforación timpánica</td>
                <td>
                    @if($admision_contratistas->timpanica=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->timpanica=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="4">* Revisión de conducto auditivo, buscando tapón de cerumen o infección.</td>
                <td>
                    @if($admision_contratistas->cerumen=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->cerumen=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="fondo_titulos" colspan="4">Cumple con los requerimientos médicos para trabajos con exposición
                    a ruido ( SATISFACTORIO)</td>
                <td>
                    @if($admision_contratistas->e_ruido=="si")
                    <p class="text-center">X</p>
                    @endif
                </td>
                <td>
                    @if($admision_contratistas->e_ruido=="no")
                    <p class="text-center">X</p>
                    @endif
                </td>
            </tr>
        </table>
    </div>
    @endif
    <div style="page-break-after:always;"></div>
    <p class="text-center" style="font-size:16px !important;"><b>DICTAMEN DE APTITUD</b></p>
    <p class="text-center">Posterior a realización de examen médico de  {{ strtoupper($empleado->apellido_paterno) }} {{ strtoupper($empleado->apellido_materno) }} {{ strtoupper($empleado->nombre) }}<br>
        {{ $empleado->genero }} de {{ \Carbon::parse($empleado->fecha_nacimiento)->age }} años de edad, se dictamina:
        </p>
    <div id="Resultados">
        {{-- <div class="alert alert-secondary text-center" role="alert">
                RESULTADOS
            </div> --}}
         
        @if($admision_contratistas!=null && $formato=='admision_contratista')

        <p class="text-center"><b>RESULTADOS DE APTITUD PARA REALIZAR LA ACTIVIDAD LABORAL</b></p>
        <p>Determinar si el trabajador es medicamente apto o no apto para realizar la actividad de trabajo, en función
            de los resultados en la exploración física- médica, antecedentes médicos así como el interrogatorio de los
            factores de riesgos.</p>
        <p>Preguntar al trabajador, e indicar en este apartado, la actividad laboral que
            realizará:{{ $admision_contratistas->actividad_laboral  }}</p>
        <ul>
            @if($admision_contratistas->SATISFACTORIO=='SATISFACTORIO')
            <li class="list_none">CUMPLE CON LOS REQUERIMIENTOS MEDICOS (SATISFACTORIO</li>
            @elseif($admision_contratistas->SATISFACTORIO=='SATISFACTORIO_CONDICIONADO')
            <li class="list_none">CUMPLE CON LOS REQUERIMIENTOS MEDICOS CONDICIONADO (SANTISFACTORIO CONDICIONADO)</li>
            @else
            <li class="list_none"> NO CUMPLE CON LOS REQUERIMIENTOS MEDICOS (NO SATISFACTORIO)</li>
            @endif



        </ul>
        <p>En caso de ser satisfactorio condicionado o no satisfactorio, describir la causa/diagnóstico: </p>
        @else
        {{-- <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>ESTADO DE SALUD:</td>
                    <td>{{ $resultados->estadoSalud }}</td>
                </tr>
                <tr>
                    <td>RESULTADOS DE APTITUD:</td>
                    <td>{{ $resultados->aptitud }}</td>
                </tr>
                <tr>
                    <td>COMENTARIOS: </td>
                    <td>{{ $resultados->comentarios }}</td>
                </tr>
            </tbody>
        </table> --}}
        <p>ESTADOS DE SALUD:<span>{{ $resultados->estadoSalud }}</span></p>
    <p>RESULTADOS DE APTITUD: </p>
    <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <td>1. Cumple Requerimientos Médicos (Satisfactorio):</td>
                    <td class="text-center">@if($resultados->aptitud=='Cumple')
                        X
                        @endif</td>
                </tr>
                <tr>
                    <td>2. Cumple Requerimientos Médicos Condicionado (Satisfactorio Condicionado):</td>
                   
                    <td class="text-center">@if($resultados->aptitud=='Condicionado')
                        X
                        @endif</td>
                </tr>
                <tr>
                    <td>3. No Cumple Requerimientos Médicos (No satisfactorio): </td>
                    <td class="text-center">@if($resultados->aptitud=='No')
                        X
                        @endif</td>
                </tr>
            </tbody>
        </table>
 
    <p>COMENTARIOS:<span>{{ $resultados->comentarios }}</span></P>
        @endif



    </div>
    <div class="mt-0" width="100%">
    </div>
    @if($admision_contratistas!=null && $formato=='admision_contratista')
    <p>Nombre completo y firma del médico:<br><img width="150" height="50" src="https://has-humanly.com/empresa_dev/storage/firmas/dr_jaeson.png"><br>Velasco Orea Jaeson Israel
        <hr>
    </p>
    <p>Cédula profesional:8149200
        <hr>
    </p>
    {{-- <p>Firma del evaluado:
        <hr>
    </p> --}}
    <p class="text-center">CONSENTIMIENTO SOBRE MANEJO DE INFORMACIÓN </p>
    <ul>
        <li>Autorizo que la información generada pueda ser capturada, almacenada y resumida en medios electrónicos con
            el propósito de que se utilice para fines estadísticos, así como la definición de campañas de salud y
            bienestar en beneficio mío y de otros empleados.</li>
        <li>Estoy de acuerdo que los datos recabados en una consulta médica, campañas de salud o los proporcionados por
            mí de forma voluntaria al personal médico gestionado por CEMEX, se manejarán de forma confidencial. </li>
        <li>En caso de solicitarse cualquier dato adicional por el área de Recursos Humanos de CEMEX para completar el
            expediente, doy mi consentimiento, estando consciente y de acuerdo con el resguardo de dicha documentación,
            ya sea en físico o digital.</li>
    </ul>
    <p>Nombre y firma del evaluado:<br><img width="210" height="50" class="mt-2" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAABkCAYAAABwx8J9AAAAAXNSR0IArs4c6QAAFedJREFUeF7tnV2oFVUbx5d9E0FJQUVFnVDMKMyISgrUiyjpIiMoK8ikDxULMwizsuwLjaj0qg5dmEFUxIudizBvMiGqU9JJCE5BoXZjd9ZFVBSdl996efa7zpyZ2Xv2fOw9M/8Fm7P1zKxZ67fW4T/Ps571rFlTU1NLnHN8VERABESgQ+D2229f8sEHHyy2/9i8efN/nnvuue+ESAREYDgJzJqamtrinHtmOJunVomACAyCwLfffusWLlzorrjiCv9566233M6dO9299947iObomSIgAj0QkKD3AEmXiEDbCNx6663u448/docOHXLff/+9W7p0qQS9bZNA/a0dAQl67YZMDRaBcgls2bLFPfvss+6RRx5xr732mvv0008l6OUiV+0iUAgBCXohGFWJCDSDgIk5bvZ9+/a5M844Q4LejKFVL1pAQILegkFWF5tJAMs5b1my5P/xsHFiTv2y0PNS1v0iUA0BCXo1nPUUEeibwJdffunXsQ8fPuz279/vf/IpqmCNn3feee6jjz5yc+bMcV9//bW3zK1I0IsirXpEoFwCEvRy+ap2EeiZAML5yy+/ePEeHx/3os33aLnoooscn8WLOzvKen5G9EJeEIho//XXX6f9yqLbFyxY4P9/w4YNCorrm7JuFIFqCEjQq+Gsp4iAJ4BoI56I6JEjR7xoxwkq14bCbd9DF3lRSHmJoP65c+e62267zXsB4tp0+eWXu7vuusvddNNNfiubigiIwHARkKAP13ioNQ0hgCDy6cVNfu2113oX9zXXXNMR8TKEOwnt9u3bvQX+7rvvuhUrVnQus5eNF1980R04cGDa7bTvmWeecVW2syFTQ90QgdIISNBLQ6uK20YAAd+1a5f78MMPZ6xxh9b2Oeec4y655BL/4fugy/z5873XgD3np5xyyozmnHvuuf6FY3Jy0nsY9u7d69544w1/j4R90KOn54vA/wlI0DUbRCAHAQRubGxsmogj3suXL3esP/O9mxWLy3tQws7LB0lkbM95FAUBeYsWLfLWOFHwVhBzLPsdO3Z4YUfw6Sfr+vyUSz7HpNKtItAnAQl6n+B0W7sIdHOhm4ivXLkyk5gR9IaFHHV3V0H3zz//9Oldca1jnce9VHSLcEfMSQuL1c611EkhRSypYlVEQASqIyBBr461nlQzAojVe++9563QaLR56ELHGu/XIu0mmGUi27Ztm9u0aZPPBoeFHldI/7ps2bKeI9yx6Mkyx30S9TJHT3WLwEwCEnTNChGIEECURkdHvZhjceJOJljMgta6udCzAEVQEdaqDz7BzT8yMuLX8b/44ovYtXP6Ye1j/Zxreykws1zwBPzRt17v7aV+XSMCIhBPQIKumSECzvl14Kg1jhitXr3ai3lcsFhecOZup56qBR3BZf2c9K5pLyiIPn1H0LMURH3t2rXeHc/9rME//vjjWarQtSIgAhkJSNAzAtPlzSKQZI2vX7++dKvS3O1VC7odjcqLCmv3ScWuiwbEZZkBuN5XrVrlE+a8/vrrbs2aNVlu17UiIAIZCEjQM8DSpc0ggPWINY5bHUGnlG2Nx5EblKCbdU4gHLEAScX2p09MTPQdI0DdeD8I/KMkbY1rxsxSL0RgsAQk6IPlr6dXSAAXNwFuiLlttcJKrcIaj+umBZxVaaFjKbOvnEC+3bt3p9JH+HnpOHbsWO5RspeDtAC83A9RBSLQcgIS9JZPgDZ0H9cxkdesGQ/KGo/jbJZylYJORjjEFTFH1NPK7Nmz/fp6N+HvZQ7hFWE9XlZ6L7R0jQj0R0CC3h833VUDAqGQE5jF+i1BbsMQcW0BcWx3o51VBMWZqFrWt7QhxItx5513pm5pyzoFbJtct0C8rPXqehEQgf8RkKBrJjSOAMKFJUp6UhPyjRs3DiwbWxxgWz9nrzaR4FUIeq9ub9zyJJyBI9HtRWWxs61sSUlsGjcR1SERqJiABL1i4HpcuQSwfLEssXo5FQyhzCJIVaVhtfXzKgUdkYbP0aNHp513Hh0R+GGhF/mSQdpYlj2SUsyWOytUuwi0g4AEvR3j3IpeIkLsfSbgbevWrZn3PZvVXEUaVtqJB4GtXHwvUjzTPALdBNUY8DK0Z8+eQuaNiTnLC7jbcfmriIAIFE9Agl48U9U4AAImGljjBHGxDS1LqTrJi51ghqAvXbq0dEG3FwiywqWxseuyZIZL4ywxzzILda0I5CMgQc/HT3cPAYEiRKPKLWThCWZEkVch6LxAEE/A+nVaCY9KzTu0RYxL3jbofhFoEwEJeptGu4F9LUo0qtxCFh7IQmKXsgXdnkfqVZYikopdlycznNVd1Lg0cMqqSyJQGgEJemloVXHZBIoSDXO3s52N71WtZ/OcKgS9Snd7uMNAa+Zl/wWofhGYTkCCrhlRSwLmti5CNMwytQNY1q1b51555RUfFEZwWNGlagu9Knd7dIcBwYUKgCt69qg+EUgmIEHX7KglgSKDt8L183vuuce9/fbbnklZlnqVgt6ruz1c18fzkbXk3WGQ9Xm6XgREYCYBCbpmRS0JFBm8Fa6fP/roo+7VV19tjKDbskS36PZ+k76E2fj63WFQywmoRovAEBKQoA/hoKhJ6QTyWpNh7XZEqKVgvfLKK90333zTGEFftGiRjwtIO2DFeHbbox7lZvnxLRsfwXRyseuvVwQGR0CCPjj2enKfBEKXNZnW8hSzTHGvkyGNUnZ+9apc7iTY4YAVGNG/pJJl+SIuP/6wpdXNMx90rwjUmYAEvc6j19K2myDmPYrTotuxTG+55Ra/fYxSdjrWqgSdHPGrVq1y3TLfdVu+4MWANfJdu3b58+OHNT9+S/8c1G0R6BCQoGsy1I4AW6OwPMl4RirRfksorKz/Llu2rFGCjpgj6rjbk1zhacsXHDf7/vvvezGnwIiXHc6Pz5IfP8v40B7Gl/biKVERARHonYAEvXdWunKICNgBIhw00q+4WHQ77ujx8XGfW71JFjovPbatL2noosFwuNRHR0f92fEcVENZsWKFu+OOO7qen55levAyRf14SWB/+PBh/z0s3TwLWZ6na0WgDQQk6G0Y5Qb2sYjzukMxs+NCcS+XfWBKFS73XgIHbcnhgQcecJdeeqkXchNVkuxgiS9fvjzzCxMMeTGg0FfK/v37p/07OiVJsMNn8eLF/lfm1leQXQP/eNWl0ghI0EtDq4rLJIBbdmRkxD+C/OSWFKbXZ4aR3bZ+ftppp7nzzz/fC3qZ6VirEHTbrjYxMZHoun766afd888/30GGeGKNr169OvYec4djTfOhHDx40J9uF4p30hjgSeFFgZ/z5s3zAs6HfPYqIiAC+QlI0PMzVA0DIrB9+3a3YcOGrlHccc0LI7t37NjRcbez9eqvv/5y27Ztq3ViGbarIbosSYTFXOp4OEyI2ar32GOPeTGnmCucF48ffvjBW+1mcXcTa36PaJ999tleuE3A+akiAiJQLgEJerl8VXuJBLDSzW3ebWtWtBlhZDffTzrpJPfzzz/7M9QR8yJSyiZ1fdOmTZ0XhjJyuUe3q1mUOi8uoUud9v3000/u5Zdf9iKOBY5wm9Bb+02YzR1OMCIeET5Zj6ktcTqoahFoPQEJeuunQL0BhKKOuBDg1s0aDNeXydWONbty5Uq/LavsLWu2bm0vDAho0e59Xkpeeuklt2bNGh8xjjXOz14KDOGHS5zvtFPr2L2Q0zUiMHgCEvTBj4FakJMAYoULnS1aWI24zRG1pBK3hm0pX4sQdESbNsVtu4omxek3SY65xW09m6CzcG07zTUOI64966yz3H333efbiYhrm1jOiajbRWDABCToAx4APb44AmxDY+81YpdmrYfb1czljZXMnva8Ee5mgdOruG1X/Qg6HgXqRYR7Fe4TTjjB3X///X67WegaD1Pd0l9Z38XNP9UkAoMmIEEf9Ajo+YUSYP2XQLk0az3croZQIuann366Yy09b4S7vSwQYEZdUcEM18/xBoQCj4VMe9L2ZoeweBnh5SV0p1922WXuu+++c08++aR74YUXZrDt9xCWQgdJlYmACJRCQIJeClZVOmgCSdZ69CCS8OhUXPVsocqzpm0nt8UdvWrWOyKP2PNvLG7bqx1lFu7N5juWNvdwKEpYcJcTC8B2M+IACOqbnJycEUtg1nmWQ1gGPY56vgiIQO8EJOi9s9KVNSMQZ60fOXLEb1EzwbPta3QNEc4TdW57vy3gDevZsq0hxGNjY44XiLjCcwnMQ5z5btHj3Mc9pGDlZcQKz+B6hDwMAmRvPsJP/6JF1nnNJrCaKwIZCUjQMwLT5fUjEFrrs2bNcqeeeqp76qmnvDWOyNmWtTyCbmIOHQLycIOzTz6ukH0NMUa4EeNoghzau3fvXp9+1RK4ROuJ8wAkZYcLT0iTdV6/+asWi0CvBCTovZLSdbUmgLX+xBNPeFc3AWP//PNPpz9z5sxxP/74Y6qFnha5Hop5FJJZ2mZdx+1vtwQ5/A4Bt33giD3if+GFF/oofixvXkIQ/K1bt86I5Lf1+dDdbm52S6XKfVmz6tV64NV4EWgRAQl6iwa77V0NXc4I58MPP+wOHDjQwcLaNlYzIhgKZhi5vnnzZnfcccelZlBDmIkuZx84dVrgGw8yyzrJlY4LnVS0/KQtFGs3Qo0Ln33zlGgUfZy7PbzX6mv7PFD/RaCpBCToTR1Z9WsagfDsc85Rp1i2OILhOL0tGjWOGGNhI6JpqU8RcOrgJQFXuwm5NSAUdCzscL84Wdi4H6ubyHhEOixm/ZurPHy5CN3u9gzawT1ys+sPQATaR0CC3r4xb2WPo/u/7bQ2BNCyxSGufNLE+8QTT3Q333yzv47gurQUsbjO7UzxMBjOXOlY4uaS5yx2rrFIewaJdfRoGlpzq0e3xZnbfvfu3R0xl5u9lVNdnW4xAQl6iwe/TV03QWfLF3vO2afNISzhWnrIg7Vr3OaI7+eff+7WrVvnf42g//33351LsbgR5jDLGi8E0aj0OFd6+DwLaIuOSRgxb1vS4l4iLFofoedlhWv27NmT+ejTNs0J9VUEmkZAgt60EVV/ZhDATU2SlXfeeWfa74h2v/rqq/0Z3HbCGhdEBTMU2+uvv9599tln/hqsdF4U0vKkY4FjdVsgGm78pBSrWNnhwShssbvxxhv9ywGWflzbrEOsq6cF3mlaiIAINJ+ABL35Y9zKHiLiWKqIoZ0wxpY1ksZ88sknXsTDhC5xgWshOHN12//xIjB79mw3Pj4+Iyr95JNP9v+fJPZY9evXr09Mu8oLwujoaEfEeRkgqxzR7tH1edpjB7zwMlDmKXGtnEjqtAjUiIAEvUaDpaamE4gTcQLdrrvuOm+dE1jGNQjtoUOHprmjuwl60ta0BQsWeJENo9KjrvRQ2H/77TefljZ6RGm0Z6GII+Z4A8KCiOOCD/eq4w3Aza787PpLEYF2EpCgt3PcG9FrE2dLn2pZ2RBxS96CxWpbtwgYI/gsLrmKCTrWs0WsHzx40Fu/uLLNrY6w3nDDDQ5rf+7cue6hhx7KLKCIeZqom5hHRdysceIAzAVvyWmS9qY3YqDVCREQgZ4ISNB7wqSLhoFAkoDTNoQYNzpCHq5Rh9vV2C6G8IXWuQk2LwW46OOKCSzZ3SwqfRA8zEtg7SF3O30NI9zpv4oIiEA7CUjQ2znuteg1VrGlQeVnmAbVBJyffJKKWd5Y5Qjf3Xff7a666qrOoSih6xuhjAa4DUu0eJgnPhq9bhHucQey1GKg1UgREIFCCEjQC8GoSooiYBnU2IMd7t1GWFmnJuo7TcCj7XjzzTfdgw8+6I4//nj377//uqmpqc4l1EldF1xwgbv44ou9dR9Gmts+7kGtSUfXyZMC3ohw59o//vijqGFQPSIgAjUkIEGv4aA1sclhZjP6h4iagPMzbj05jkOaW57McLjNcc2feeaZ7vfff+8kb6Eugs/4vT0/aXtZGfzpv3kLolHuFhPA2n7cywXR9lwzMTFRRtNUpwiIQE0ISNBrMlBNbWYo5P2sVacJOFvLvvrqK4+Oum3tPBqxjnBbzvWQswl82t7xPOMSF6lu9UXXyZOeQyAgLypxaWPztE33ioAI1I+ABL1+Y1brFscJsLm2N27cGGuJE2WO9cpPtn0hhGE+dAMSXVcP947jTmfvd5hOlQxv9mx+ksrVrGSLnLe684p7LxY4Gezs5SNuq1rcwLMsQeR+3OlrtZ4oarwIiEBmAhL0zMh0Qz8Eoi516kCAWRMPxQvR5tpwy1j0eYgv0ea4mUm2EhcYFx5iEg126yX5Cm5vE/iouHdLDBO2d2xsLPZc814t8G6sFeHejZB+LwLtISBBb89YD6yniOvChQt9BDm50cmRboFt/C5JvE24Ee958+Z5ATf3eLfOWHQ7dXAPLwl4APoJdAvFvdfEMGH7bA28Hwu8Wz85JY7tdkePHu05zqBbnfq9CIhAPQlI0Os5brVqddLBI2EnQvEm+xoijPj3W8zdbtZ5XDKZfuvulhgmrDe6L77fZybdN3/+fL9MgKCriIAItJuABL3d419Z76MHj9iDEe284h3tRBj01uQjRBFyItx5aSALnooIiEC7CUjQ2z3+jet9mJOdzu3cudOv0TexWF/ZzsZLjIoIiEC7CUjQ2z3+jeo9ke8jIyOdPvUS/FZnAOaJ2LdvX6ZkO3Xus9ouAiKQTECCrtnRKAK49gmy4/CTJlvnDBpHwRKfcOzYsc55640aTHVGBEQgEwEJeiZcurgOBMwV3XRBZ/3cvBB1GBe1UQREoFwCEvRy+ar2ARCwqPomCzrb8NgKqPXzAUwwPVIEhpSABH1IB0bNykeApDCkQx3UwSr5Wt/97rZ4IbqT0BUiIAJGQIKuuSACNSQgQa/hoKnJIlAyAQl6yYBVvQiUQcByuDd5WaEMbqpTBJpMQILe5NFV3xpLYO3atT7X/OTkZK6Meo0FpI6JQAsJSNBbOOjqcv0JcGQq8QEIuooIiIAIQECCrnkgAjUjYFH8inCv2cCpuSJQMgEJesmAVb0IFE1AAXFFE1V9ItAMAhL0ZoyjetEiAhL0Fg22uioCGQhI0DPA0qUiMAwEJOjDMApqgwgMHwEJ+vCNiVokAqkE2pAJT1NABEQgOwEJenZmukMEBk6g6ZnwBg5YDRCBGhKQoNdw0NRkERABERABEYgSkKBrToiACIiACIhAAwhI0BswiOqCCIiACIiACEjQNQdEQAREQAREoAEEJOgNGER1QQREQAREQAQk6JoDIiACIiACItAAAhL0BgyiuiACIiACIiACEnTNAREQAREQARFoAAEJegMGUV0QAREQAREQAQm65oAIiIAIiIAINICABL0Bg6guiIAIiIAIiIAEXXNABERABERABBpAQILegEFUF0RABERABERAgq45IAIiIAIiIAINICBBb8AgqgsiIAIiIAIiIEHXHBABERABERCBBhCQoDdgENUFERABERABEZCgaw6IgAiIgAiIQAMI/BeJnPEZPHxwiQAAAABJRU5ErkJggg=="><br>{{ strtoupper($empleado->apellido_paterno) }} {{ strtoupper($empleado->apellido_materno) }} {{ strtoupper($empleado->nombre) }}
        <hr>
    </p>
    <p>Lugar y fecha: {{ $admision_contratistas->lugar_fecha }}
        <hr>
    </p>
    @else
    <table class="pl-5 pr-5">

        <tbody>
            <tr>
                <td class="text-center pl-5 pr-5"><img width="210" height="50" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAABkCAYAAABwx8J9AAAAAXNSR0IArs4c6QAAFedJREFUeF7tnV2oFVUbx5d9E0FJQUVFnVDMKMyISgrUiyjpIiMoK8ikDxULMwizsuwLjaj0qg5dmEFUxIudizBvMiGqU9JJCE5BoXZjd9ZFVBSdl996efa7zpyZ2Xv2fOw9M/8Fm7P1zKxZ67fW4T/Ps571rFlTU1NLnHN8VERABESgQ+D2229f8sEHHyy2/9i8efN/nnvuue+ESAREYDgJzJqamtrinHtmOJunVomACAyCwLfffusWLlzorrjiCv9566233M6dO9299947iObomSIgAj0QkKD3AEmXiEDbCNx6663u448/docOHXLff/+9W7p0qQS9bZNA/a0dAQl67YZMDRaBcgls2bLFPfvss+6RRx5xr732mvv0008l6OUiV+0iUAgBCXohGFWJCDSDgIk5bvZ9+/a5M844Q4LejKFVL1pAQILegkFWF5tJAMs5b1my5P/xsHFiTv2y0PNS1v0iUA0BCXo1nPUUEeibwJdffunXsQ8fPuz279/vf/IpqmCNn3feee6jjz5yc+bMcV9//bW3zK1I0IsirXpEoFwCEvRy+ap2EeiZAML5yy+/ePEeHx/3os33aLnoooscn8WLOzvKen5G9EJeEIho//XXX6f9yqLbFyxY4P9/w4YNCorrm7JuFIFqCEjQq+Gsp4iAJ4BoI56I6JEjR7xoxwkq14bCbd9DF3lRSHmJoP65c+e62267zXsB4tp0+eWXu7vuusvddNNNfiubigiIwHARkKAP13ioNQ0hgCDy6cVNfu2113oX9zXXXNMR8TKEOwnt9u3bvQX+7rvvuhUrVnQus5eNF1980R04cGDa7bTvmWeecVW2syFTQ90QgdIISNBLQ6uK20YAAd+1a5f78MMPZ6xxh9b2Oeec4y655BL/4fugy/z5873XgD3np5xyyozmnHvuuf6FY3Jy0nsY9u7d69544w1/j4R90KOn54vA/wlI0DUbRCAHAQRubGxsmogj3suXL3esP/O9mxWLy3tQws7LB0lkbM95FAUBeYsWLfLWOFHwVhBzLPsdO3Z4YUfw6Sfr+vyUSz7HpNKtItAnAQl6n+B0W7sIdHOhm4ivXLkyk5gR9IaFHHV3V0H3zz//9Oldca1jnce9VHSLcEfMSQuL1c611EkhRSypYlVEQASqIyBBr461nlQzAojVe++9563QaLR56ELHGu/XIu0mmGUi27Ztm9u0aZPPBoeFHldI/7ps2bKeI9yx6Mkyx30S9TJHT3WLwEwCEnTNChGIEECURkdHvZhjceJOJljMgta6udCzAEVQEdaqDz7BzT8yMuLX8b/44ovYtXP6Ye1j/Zxreykws1zwBPzRt17v7aV+XSMCIhBPQIKumSECzvl14Kg1jhitXr3ai3lcsFhecOZup56qBR3BZf2c9K5pLyiIPn1H0LMURH3t2rXeHc/9rME//vjjWarQtSIgAhkJSNAzAtPlzSKQZI2vX7++dKvS3O1VC7odjcqLCmv3ScWuiwbEZZkBuN5XrVrlE+a8/vrrbs2aNVlu17UiIAIZCEjQM8DSpc0ggPWINY5bHUGnlG2Nx5EblKCbdU4gHLEAScX2p09MTPQdI0DdeD8I/KMkbY1rxsxSL0RgsAQk6IPlr6dXSAAXNwFuiLlttcJKrcIaj+umBZxVaaFjKbOvnEC+3bt3p9JH+HnpOHbsWO5RspeDtAC83A9RBSLQcgIS9JZPgDZ0H9cxkdesGQ/KGo/jbJZylYJORjjEFTFH1NPK7Nmz/fp6N+HvZQ7hFWE9XlZ6L7R0jQj0R0CC3h833VUDAqGQE5jF+i1BbsMQcW0BcWx3o51VBMWZqFrWt7QhxItx5513pm5pyzoFbJtct0C8rPXqehEQgf8RkKBrJjSOAMKFJUp6UhPyjRs3DiwbWxxgWz9nrzaR4FUIeq9ub9zyJJyBI9HtRWWxs61sSUlsGjcR1SERqJiABL1i4HpcuQSwfLEssXo5FQyhzCJIVaVhtfXzKgUdkYbP0aNHp513Hh0R+GGhF/mSQdpYlj2SUsyWOytUuwi0g4AEvR3j3IpeIkLsfSbgbevWrZn3PZvVXEUaVtqJB4GtXHwvUjzTPALdBNUY8DK0Z8+eQuaNiTnLC7jbcfmriIAIFE9Agl48U9U4AAImGljjBHGxDS1LqTrJi51ghqAvXbq0dEG3FwiywqWxseuyZIZL4ywxzzILda0I5CMgQc/HT3cPAYEiRKPKLWThCWZEkVch6LxAEE/A+nVaCY9KzTu0RYxL3jbofhFoEwEJeptGu4F9LUo0qtxCFh7IQmKXsgXdnkfqVZYikopdlycznNVd1Lg0cMqqSyJQGgEJemloVXHZBIoSDXO3s52N71WtZ/OcKgS9Snd7uMNAa+Zl/wWofhGYTkCCrhlRSwLmti5CNMwytQNY1q1b51555RUfFEZwWNGlagu9Knd7dIcBwYUKgCt69qg+EUgmIEHX7KglgSKDt8L183vuuce9/fbbnklZlnqVgt6ruz1c18fzkbXk3WGQ9Xm6XgREYCYBCbpmRS0JFBm8Fa6fP/roo+7VV19tjKDbskS36PZ+k76E2fj63WFQywmoRovAEBKQoA/hoKhJ6QTyWpNh7XZEqKVgvfLKK90333zTGEFftGiRjwtIO2DFeHbbox7lZvnxLRsfwXRyseuvVwQGR0CCPjj2enKfBEKXNZnW8hSzTHGvkyGNUnZ+9apc7iTY4YAVGNG/pJJl+SIuP/6wpdXNMx90rwjUmYAEvc6j19K2myDmPYrTotuxTG+55Ra/fYxSdjrWqgSdHPGrVq1y3TLfdVu+4MWANfJdu3b58+OHNT9+S/8c1G0R6BCQoGsy1I4AW6OwPMl4RirRfksorKz/Llu2rFGCjpgj6rjbk1zhacsXHDf7/vvvezGnwIiXHc6Pz5IfP8v40B7Gl/biKVERARHonYAEvXdWunKICNgBIhw00q+4WHQ77ujx8XGfW71JFjovPbatL2noosFwuNRHR0f92fEcVENZsWKFu+OOO7qen55levAyRf14SWB/+PBh/z0s3TwLWZ6na0WgDQQk6G0Y5Qb2sYjzukMxs+NCcS+XfWBKFS73XgIHbcnhgQcecJdeeqkXchNVkuxgiS9fvjzzCxMMeTGg0FfK/v37p/07OiVJsMNn8eLF/lfm1leQXQP/eNWl0ghI0EtDq4rLJIBbdmRkxD+C/OSWFKbXZ4aR3bZ+ftppp7nzzz/fC3qZ6VirEHTbrjYxMZHoun766afd888/30GGeGKNr169OvYec4djTfOhHDx40J9uF4p30hjgSeFFgZ/z5s3zAs6HfPYqIiAC+QlI0PMzVA0DIrB9+3a3YcOGrlHccc0LI7t37NjRcbez9eqvv/5y27Ztq3ViGbarIbosSYTFXOp4OEyI2ar32GOPeTGnmCucF48ffvjBW+1mcXcTa36PaJ999tleuE3A+akiAiJQLgEJerl8VXuJBLDSzW3ebWtWtBlhZDffTzrpJPfzzz/7M9QR8yJSyiZ1fdOmTZ0XhjJyuUe3q1mUOi8uoUud9v3000/u5Zdf9iKOBY5wm9Bb+02YzR1OMCIeET5Zj6ktcTqoahFoPQEJeuunQL0BhKKOuBDg1s0aDNeXydWONbty5Uq/LavsLWu2bm0vDAho0e59Xkpeeuklt2bNGh8xjjXOz14KDOGHS5zvtFPr2L2Q0zUiMHgCEvTBj4FakJMAYoULnS1aWI24zRG1pBK3hm0pX4sQdESbNsVtu4omxek3SY65xW09m6CzcG07zTUOI64966yz3H333efbiYhrm1jOiajbRWDABCToAx4APb44AmxDY+81YpdmrYfb1czljZXMnva8Ee5mgdOruG1X/Qg6HgXqRYR7Fe4TTjjB3X///X67WegaD1Pd0l9Z38XNP9UkAoMmIEEf9Ajo+YUSYP2XQLk0az3croZQIuann366Yy09b4S7vSwQYEZdUcEM18/xBoQCj4VMe9L2ZoeweBnh5SV0p1922WXuu+++c08++aR74YUXZrDt9xCWQgdJlYmACJRCQIJeClZVOmgCSdZ69CCS8OhUXPVsocqzpm0nt8UdvWrWOyKP2PNvLG7bqx1lFu7N5juWNvdwKEpYcJcTC8B2M+IACOqbnJycEUtg1nmWQ1gGPY56vgiIQO8EJOi9s9KVNSMQZ60fOXLEb1EzwbPta3QNEc4TdW57vy3gDevZsq0hxGNjY44XiLjCcwnMQ5z5btHj3Mc9pGDlZcQKz+B6hDwMAmRvPsJP/6JF1nnNJrCaKwIZCUjQMwLT5fUjEFrrs2bNcqeeeqp76qmnvDWOyNmWtTyCbmIOHQLycIOzTz6ukH0NMUa4EeNoghzau3fvXp9+1RK4ROuJ8wAkZYcLT0iTdV6/+asWi0CvBCTovZLSdbUmgLX+xBNPeFc3AWP//PNPpz9z5sxxP/74Y6qFnha5Hop5FJJZ2mZdx+1vtwQ5/A4Bt33giD3if+GFF/oofixvXkIQ/K1bt86I5Lf1+dDdbm52S6XKfVmz6tV64NV4EWgRAQl6iwa77V0NXc4I58MPP+wOHDjQwcLaNlYzIhgKZhi5vnnzZnfcccelZlBDmIkuZx84dVrgGw8yyzrJlY4LnVS0/KQtFGs3Qo0Ln33zlGgUfZy7PbzX6mv7PFD/RaCpBCToTR1Z9WsagfDsc85Rp1i2OILhOL0tGjWOGGNhI6JpqU8RcOrgJQFXuwm5NSAUdCzscL84Wdi4H6ubyHhEOixm/ZurPHy5CN3u9gzawT1ys+sPQATaR0CC3r4xb2WPo/u/7bQ2BNCyxSGufNLE+8QTT3Q333yzv47gurQUsbjO7UzxMBjOXOlY4uaS5yx2rrFIewaJdfRoGlpzq0e3xZnbfvfu3R0xl5u9lVNdnW4xAQl6iwe/TV03QWfLF3vO2afNISzhWnrIg7Vr3OaI7+eff+7WrVvnf42g//33351LsbgR5jDLGi8E0aj0OFd6+DwLaIuOSRgxb1vS4l4iLFofoedlhWv27NmT+ejTNs0J9VUEmkZAgt60EVV/ZhDATU2SlXfeeWfa74h2v/rqq/0Z3HbCGhdEBTMU2+uvv9599tln/hqsdF4U0vKkY4FjdVsgGm78pBSrWNnhwShssbvxxhv9ywGWflzbrEOsq6cF3mlaiIAINJ+ABL35Y9zKHiLiWKqIoZ0wxpY1ksZ88sknXsTDhC5xgWshOHN12//xIjB79mw3Pj4+Iyr95JNP9v+fJPZY9evXr09Mu8oLwujoaEfEeRkgqxzR7tH1edpjB7zwMlDmKXGtnEjqtAjUiIAEvUaDpaamE4gTcQLdrrvuOm+dE1jGNQjtoUOHprmjuwl60ta0BQsWeJENo9KjrvRQ2H/77TefljZ6RGm0Z6GII+Z4A8KCiOOCD/eq4w3Aza787PpLEYF2EpCgt3PcG9FrE2dLn2pZ2RBxS96CxWpbtwgYI/gsLrmKCTrWs0WsHzx40Fu/uLLNrY6w3nDDDQ5rf+7cue6hhx7KLKCIeZqom5hHRdysceIAzAVvyWmS9qY3YqDVCREQgZ4ISNB7wqSLhoFAkoDTNoQYNzpCHq5Rh9vV2C6G8IXWuQk2LwW46OOKCSzZ3SwqfRA8zEtg7SF3O30NI9zpv4oIiEA7CUjQ2znuteg1VrGlQeVnmAbVBJyffJKKWd5Y5Qjf3Xff7a666qrOoSih6xuhjAa4DUu0eJgnPhq9bhHucQey1GKg1UgREIFCCEjQC8GoSooiYBnU2IMd7t1GWFmnJuo7TcCj7XjzzTfdgw8+6I4//nj377//uqmpqc4l1EldF1xwgbv44ou9dR9Gmts+7kGtSUfXyZMC3ohw59o//vijqGFQPSIgAjUkIEGv4aA1sclhZjP6h4iagPMzbj05jkOaW57McLjNcc2feeaZ7vfff+8kb6Eugs/4vT0/aXtZGfzpv3kLolHuFhPA2n7cywXR9lwzMTFRRtNUpwiIQE0ISNBrMlBNbWYo5P2sVacJOFvLvvrqK4+Oum3tPBqxjnBbzvWQswl82t7xPOMSF6lu9UXXyZOeQyAgLypxaWPztE33ioAI1I+ABL1+Y1brFscJsLm2N27cGGuJE2WO9cpPtn0hhGE+dAMSXVcP947jTmfvd5hOlQxv9mx+ksrVrGSLnLe684p7LxY4Gezs5SNuq1rcwLMsQeR+3OlrtZ4oarwIiEBmAhL0zMh0Qz8Eoi516kCAWRMPxQvR5tpwy1j0eYgv0ea4mUm2EhcYFx5iEg126yX5Cm5vE/iouHdLDBO2d2xsLPZc814t8G6sFeHejZB+LwLtISBBb89YD6yniOvChQt9BDm50cmRboFt/C5JvE24Ee958+Z5ATf3eLfOWHQ7dXAPLwl4APoJdAvFvdfEMGH7bA28Hwu8Wz85JY7tdkePHu05zqBbnfq9CIhAPQlI0Os5brVqddLBI2EnQvEm+xoijPj3W8zdbtZ5XDKZfuvulhgmrDe6L77fZybdN3/+fL9MgKCriIAItJuABL3d419Z76MHj9iDEe284h3tRBj01uQjRBFyItx5aSALnooIiEC7CUjQ2z3+jet9mJOdzu3cudOv0TexWF/ZzsZLjIoIiEC7CUjQ2z3+jeo9ke8jIyOdPvUS/FZnAOaJ2LdvX6ZkO3Xus9ouAiKQTECCrtnRKAK49gmy4/CTJlvnDBpHwRKfcOzYsc55640aTHVGBEQgEwEJeiZcurgOBMwV3XRBZ/3cvBB1GBe1UQREoFwCEvRy+ar2ARCwqPomCzrb8NgKqPXzAUwwPVIEhpSABH1IB0bNykeApDCkQx3UwSr5Wt/97rZ4IbqT0BUiIAJGQIKuuSACNSQgQa/hoKnJIlAyAQl6yYBVvQiUQcByuDd5WaEMbqpTBJpMQILe5NFV3xpLYO3atT7X/OTkZK6Meo0FpI6JQAsJSNBbOOjqcv0JcGQq8QEIuooIiIAIQECCrnkgAjUjYFH8inCv2cCpuSJQMgEJesmAVb0IFE1AAXFFE1V9ItAMAhL0ZoyjetEiAhL0Fg22uioCGQhI0DPA0qUiMAwEJOjDMApqgwgMHwEJ+vCNiVokAqkE2pAJT1NABEQgOwEJenZmukMEBk6g6ZnwBg5YDRCBGhKQoNdw0NRkERABERABEYgSkKBrToiACIiACIhAAwhI0BswiOqCCIiACIiACEjQNQdEQAREQAREoAEEJOgNGER1QQREQAREQAQk6JoDIiACIiACItAAAhL0BgyiuiACIiACIiACEnTNAREQAREQARFoAAEJegMGUV0QAREQAREQAQm65oAIiIAIiIAINICABL0Bg6guiIAIiIAIiIAEXXNABERABERABBpAQILegEFUF0RABERABERAgq45IAIiIAIiIAINICBBb8AgqgsiIAIiIAIiIEHXHBABERABERCBBhCQoDdgENUFERABERABEZCgaw6IgAiIgAiIQAMI/BeJnPEZPHxwiQAAAABJRU5ErkJggg==">{{ strtoupper($empleado->apellido_paterno) }} {{ strtoupper($empleado->apellido_materno) }} {{ strtoupper($empleado->nombre) }}</td>
                <td class="text-center pl-5 pr-5"></td>
                <td class="text-center pl-5 pr-5"><img width="150" height="50" src="https://has-humanly.com/empresa_dev/storage/firmas/dr_jaeson.png"><br>Velasco Orea Jaeson Israel</td>
            </tr>
           
            <tr>
                <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
                <td class="text-center pl-5 pr-5"></td>
                <td class="text-center pl-5 pr-5" style="border-bottom:solid 1px #000;"></td>
            </tr>
            <tr>
                <td class="text-center">NOMBRE Y FIRMA DEL CANDIDATO</td>
                <td class="text-center"></td>
                <td class="text-center">NOMBRE Y FIRMA DEL MÉDICO</td>
            </tr>
            <tr>
                <td class="text-center" style="font-size:9px !important;">Doy consentimiento para realización de
                    exámenes y <br>
                    laboratorios que la empresa considere necesarios para mi contratación.<br>
                    La información proporcionada es de manera voluntaria y en apego a la verdad</td>
                <td class="text-center"></td>
                <td class="text-center">CEDULA PROFESIONAL<br>8149200</td>
            </tr>
        </tbody>
    </table>
    @endif
    </div>
</body>

</html>
