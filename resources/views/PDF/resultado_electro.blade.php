<!DOCTYPE html>
<html>
<head>
    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <style>
        .letra {
            font-size: 12px !important;
        }

        .fondo_titulos {
            background-color: #636363 !important;
            color: #fff !important;
        }

        .list_none {
            list-style: none;
        }
    </style>
</head>
<body>
    <table class="table" style=" border: 2px solid #e34e51;">
        <tbody>
            <tr>
                <td> <img width="" width="" class=""
                        src="https://has-humanly.com/empresa_dev/storage/firmas/galicia.png" style=""></td>
                <td>
                    <b style="font-size:17.4px;text-align:center;color:#233e5e;">Electrodiagnóstico Integral</b>
                    <p style="font-size:14px;text-align:center;color:#54708b;">Dr. Abraham Galicia Reyes<br>Informe de
                        Estudio Electrocardiográfico
                    </p>

                </td>
                <td> <img width="" width="" class="" src="https://has-humanly.com/empresa_dev/storage/firmas/asesor.png"
                        style=""></td>
            </tr>
        </tbody>
    </table>
    <p style="font-weight:bold;font-size:16px;" class="ml-5 mr-5">Ficha de Identidad</p>
    <hr style="background-color:#e34e51;height:1px;" class="ml-5 mr-5">
    <table class="table-sm ml-5 mr-5 letra">
        <tbody>
            <tr>
                <td>Nombre</td>

                <td>{{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}</td>

            </tr>
            <tr>
                <td>Edad</td>
                <td>{{ $empleado->edad }} años</td>

            </tr>
            <tr>
                <td>Origen</td>
                <td>Matriz areamedica@laboratorioasesores.com </td>

            </tr>
            <tr>
                <td>Motivo del Estudio</td>
                <td>Check up empresarial</td>

            </tr>
        </tbody>
    </table>
    @if(!empty($electrocardiograma->frecuencia))
    <p style="font-weight:bold;font-size:16px;" class="ml-5 mr-5">Parámetros</p>
    <hr style="background-color:#e34e51;height:1px;" class="ml-5 mr-5">
    <table class="ml-5 mr-5 letra">
        <tbody>
            <tr>
                <td>Ritmo</td>
                <td>Sinusal</td>
            </tr>
            <tr>
                <td>Frecuencia&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; </td>
                <td>{{ $electrocardiograma->frecuencia }}</td>
            </tr>
            <tr>
                <td>Eje de P&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; </td>
                <td>{{ $electrocardiograma->eje_p }}</td>
            </tr>
            <tr>
                <td>Eje de QRS&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; </td>
                <td>{{ $electrocardiograma->ejeqrs }}</td>
            </tr>
            <tr>
                <td>Eje de T&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; </td>
                <td>{{ $electrocardiograma->ejet }}</td>
            </tr>
        </tbody>
    </table>
    <p style="font-weight:bold;font-size:16px;" class="ml-5 mr-5">Intervalos</p>
    <hr style="background-color:#e34e51;height:1px;" class="ml-5 mr-5">
    <table class="ml-5 mr-5 letra">
        <tbody>
            <tr>
                <td>R-R&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>

                <td>{{ $electrocardiograma->rr }}</td>
            </tr>
            <tr>
                <td>P-R&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                <td>{{ $electrocardiograma->pr }}</td>
            </tr>
            <tr>
                <td>QRS&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                <td>{{ $electrocardiograma->qrs }}</td>
            </tr>
            <tr>
                <td>QTm&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                <td>{{ $electrocardiograma->qtm }}</td>
            </tr>
            <tr>
                <td>QTc&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>
                <td>{{ $electrocardiograma->qtc }}</td>
            </tr>
        </tbody>
    </table>
    <p style="font-weight:bold;font-size:16px;" class="ml-5 mr-5 mt-1">Observaciones</p>
    <hr style="background-color:#e34e51;height:1px;" class="ml-5 mr-5">
    <table class="ml-5 mr-5 letra">
        <tbody>
            <tr>
                <td>{{ $electrocardiograma->observaciones }}</td>
            </tr>
        </tbody>
    </table>
    <table class="ml-5 mr-5 letra mt-0" style="border-bottom:0px !important;" width=100%>
        <tbody>
            <tr>
                <td style="border-bottom:0px !important;font-size:16px !important;text-align:center;">
                    <b>Conclusiones</b></td>
                <td style="border-bottom:0px !important;font-size:16px !important;text-align:center;"><b>Notas</b> </td>
                <td style="border-bottom:0px !important;font-size:16px !important;text-align:center;"><b>Atentamente</b>
                </td>
            </tr>
            <tr>
                <td style="border: 2px solid #e34e51;">{{ $electrocardiograma->conclusiones }} </td>
                <td style="border: 2px solid #e34e51;">{{ $electrocardiograma->Notas }} </td>
                <td class="text-center"><img width="" width="" class=""
                        src="https://has-humanly.com/empresa_dev/storage/firmas/firma_galicia.png" style=""><br><b>Dr.
                        Abraham Galicia Reyes</b><br>
                    Cardiólogo, Electrofisiólogo<br>
                    CP: 1659107 / CE: 3624212</td>
            </tr>
        </tbody>
    </table>
    @endif
</body>
<footer>
    {{-- <a class="mr-50" target="_blank"
        href="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->documento) !!}">
        <img src="https://img.icons8.com/color/48/000000/download-from-cloud.png" />
    </a> --}}
</footer>
{{-- <script src="{!! asset('js/bootstrap.min.js') !!}"></script> --}}

</html>