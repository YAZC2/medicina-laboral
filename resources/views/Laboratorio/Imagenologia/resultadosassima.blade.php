<html>
    <head>
        <title>Resultados Laboratorio</title>
    </head>
    <body>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Humanly Software">
    <title>IMAGENOLOGIA</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://has.humanly-sw.com/public/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/vendors/css/vendors.min.css ">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <link rel="stylesheet" type="text/css" href=" https://has-humanly.com/empresa_dev/public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href=" https://has-humanly.com/empresa_dev/public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css">

    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.min.css ">
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/core/colors/palette-gradient.min.css ">
    <link rel="stylesheet" type="text/css" href=" https://has-humanly.com/empresa_dev/public/css/daicoms/daicoms.css">
    <link rel="stylesheet" type="text/css" href=" https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href=" https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/pages/data-list-view.min.css">
    <link rel="stylesheet" href="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/css/quill.bubble.min.css">
    <link rel="stylesheet" href="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/css/quill.snow.min.css">
    <link rel="stylesheet" href="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/css/index.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa_dev/public/vuexy/assets/css/style.css">
    <link rel="stylesheet" href="https://has-humanly.com/empresa_dev/public/css/empresa/pacientes.css">
<link rel="stylesheet" type="text/css" href=" https://has-humanly.com/empresa_dev/public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css">

    <!-- END: Custom CSS-->


<!-- END: Head-->

<!-- BEGIN: Body-->



    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow bg-secondary">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav bookmark-icons">
                            <!-- li.nav-item.mobile-menu.d-xl-none.mr-auto-->
                            <!--   a.nav-link.nav-menu-main.menu-toggle.hidden-xs(href='#')-->
                            <!--     i.ficon.feather.icon-menu-->
                            

                            <li class="nav-item d-none d-lg-block text-white">
                                                                Cemex
                                                            </li>

                            
                        </ul>
                        
                        
                        
                        
                    </div>
                    <ul class="pacie mt-2">
                        



                    </ul>
                    <ul class="nav navbar-nav float-right">

                        

                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>

                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                <i class=" ficon feather icon-info "></i>
                                <!--<span class="badge badge-pill badge-danger badge-up">5</span>-->
                            </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <h3 class="white">Nuevas</h3>
                                        <span class="notification-title">
                                            Funcionalidades
                                        </span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list"><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i class="feather icon-plus-square font-medium-5 primary"></i></div>
                                            <div class="media-body">
                                                <h6 class="primary media-heading">Consultas</h6><small class="notification-text">lleva el control de tus consultas.</small>
                                            </div><small>
                                                
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-clipboard font-medium-5 success"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="success media-heading red darken-1">Receta Médicas</h6><small class="notification-text">
                                                    Receta Electrónica configurable.
                                                </small>
                                            </div>
                                            <small>
                                                
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-book font-medium-5 danger"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="danger media-heading yellow darken-3">
                                                    Expediente Clínico Electrónico
                                                </h6>
                                                <small class="notification-text">
                                                    Lleva el control de todos tus pacientes.</small>
                                            </div>
                                            <small>
                                                
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-bar-chart-2 font-medium-5 info"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="info media-heading">
                                                    Estadísticas clínicas de tu práctica médica
                                                </h6><small class="notification-text">
                                                    Conoce con datos y estadísticas el desempeño real.
                                                </small>
                                            </div>
                                            <small>
                                                
                                            </small>
                                        </div>
                                    </a><a class="d-flex justify-content-between" href="javascript:void(0)">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left">
                                                <i class="feather icon-users font-medium-5 warning"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="warning media-heading">
                                                    Pasa más timpo con tu paciente
                                                </h6><small class="notification-text">
                                                    <ul>
                                                        <li>Ten toda la información de tus pacientes en una sola
                                                            pantalla.</li>
                                                        <li>Notas de consulta.</li>
                                                        <li>Gráficas de signos vitales.</li>
                                                        <li>Estudios de laboratorio y radiografías en la nube.</li>
                                                        <li>Detección de pacientes duplicados.</li>
                                                        <li>Encuentra fácilmente a tus pacientes.</li>
                                                    </ul>
                                                </small>
                                            </div>
                                            <small>
                                                
                                            </small>
                                        </div>
                                    </a></li>
                                <li class="dropdown-menu-footer" data-toggle="modal" data-target="#xlarge">
                                    <a class="dropdown-item p-1 text-center" href="javascript:void(0)">
                                        Soporte
                                    </a>
                                </li>
                            </ul>
                        </li>



                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                <i class="ficon feather icon-bell"></i>
                                <span class="badge badge-pill badge-danger notificationCount badge-up"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header m-0 p-2">
                                        <span class="notification-title">
                                            Notificaciones
                                        </span>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list notificationList">


                                </li>
                            </ul>
                        </li>



                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">Jaeson Israel Velasco</span><span class="user-status">SUPERVISOR DE SERVICIO MEDICO</span></div><span><img id="logo_principal_layaout" class="round" src="https://has-humanly.com/empresa_dev/storage/app/empresas/1637883175cemex.jpg" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                
                                

                                <a class="dropdown-item" href="https://has-humanly.com/empresa_dev/Configuracion">
                                    <i class="feather icon-settings"></i>
                                    Configuración
                                </a>
                                                                    <a class="dropdown-item" href="https://has-humanly.com/empresa_dev/configuracion-usuarios">
                                        <i class="feather icon-users"></i> Equipo
                                    </a>
                                                                                                    <a class="dropdown-item" href="https://has-humanly.com/empresa_dev/Accesos">
                                        <i class="feather icon-lock"></i> Accesos
                                    </a>
                                
                                <div class="dropdown-divider"></div>
                                <form action="https://has-humanly.com/empresa_dev/logout" method="post">
                                    <input type="hidden" name="_token" value="J4kAOVmRwqpxSDu9BGk6Hm1DT1mXsf7myxp6rPLK">                                    <button type="submit" class="dropdown-item" href="auth-login.html">
                                        <i class="feather icon-power"></i>
                                        Cerrar Sesión
                                    </button>
                                </form>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="https://has-humanly.com/empresa_dev/Administrador">
                        <img src="https://has-humanly.com/empresa_dev/public/logo.png" width="45" alt="">
                        <h2 class="brand-text secondary mb-0">HAS</h2>
                    </a>
                </li>
                <li class="nav-item nav-toggle toolbar_toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                        <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon secondary" data-ticon="icon-disc"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                                    <li class=" nav-item ">
                        <a href="https://has-humanly.com/empresa_dev/Empleados">
                            <i class="feather icon-users"></i>
                            <span class="menu-title" data-i18n="Documentation">Pacientes</span>
                        </a>
                    </li>
                
                                    <li class=" nav-item  ">
                        <a href="https://has-humanly.com/empresa_dev/Grupos">
                            <i class="feather icon-grid"></i>
                            <span class="menu-title" data-i18n="Documentation">Grupos</span>
                        </a>
                    </li>
                
                               
                    <li class=" nav-item"><a href="#">
                        <i class="feather icon-bar-chart-2"></i>
                        <span class="menu-title" data-i18n="Ecommerce">Estadísticas</span></a>
                        <ul class="menu-content">
                            <li class="">
                                <a href="https://has-humanly.com/empresa_dev/indicadoresConsultas"><i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="Details">Consultorio</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                                
                                    <li class="nav-item"><a href="#">
                        <i class="feather icon-clipboard"></i>
                            <span class="menu-title" data-i18n="Ecommerce">Resultados</span></a>
                        <ul class="menu-content">
                            <li class="">
                                <a href="https://has-humanly.com/empresa_dev/Resultados"><i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="Details">Medicina Laboral</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="https://has-humanly.com/empresa_dev/ResultadosIma"><i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="Details">Imagenologia</span>
                                </a>
                            </li> 
                            <li class="">
                                <a href="https://has-humanly.com/empresa_dev/ResultadosLa"><i class="feather icon-circle"></i>
                                    <span class="menu-item" data-i18n="Details">Laboratorio</span>
                                </a>
                            </li>          
                        </ul>
                    </li>
                                
                                    <li class=" nav-item  ">
                        <a href="https://has-humanly.com/empresa_dev/Agenda">
                            <i class="feather icon-calendar"></i>
                            <span class="menu-title" data-i18n="Documentation">Agenda</span>
                        </a>
                    </li>
                

                


                <li class=" nav-item  ">
                    <a href="https://has-humanly.com/empresa_dev/Configuracion">
                        <i class="feather icon-settings"></i>
                        <span class="menu-title" data-i18n="Documentation">Configuraciones</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">

            <div class="content-body">
                <nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-chevron">
      <li class="breadcrumb-item"><a href="">Empleados</a></li>
      
      <li class="breadcrumb-item"><a href=""></a></li>
      <li class="breadcrumb-item active" aria-current="page"></li>
    </ol>
  </nav>
<input type="text" class="form-control" value="101211229/0337" id="toma" name="toma">
<input type="text" class="form-control" value="80050" id="estudio" name="estudio">

<div class="card">
    <div class="card-header bg-secondary text-center">
        <h6 class="card-title text-center text-white pb-2">IMAGENOLOGÍA</h6>
    </div>
    <div class="card-body">
        <div class="col-md-12">
       
            <div id="estudios">
            
            </div>
        </div>
       
        <div class="row contImagenologia">
        
       
        </div>
        
    </div>
</div>

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light ">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT © 2020<a class="text-bold-800 grey darken-2">Health Administration Solution.</a>Todos los derechos
                reservados.</span><span class="float-md-right d-none d-md-block">Humanly Software</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>

    <div class="modal-size-xl mr-1 mb-1 d-inline-block">
        <div class="modal fade text-left" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-center">
                        <h4 class="modal-title col-md-12" id="myModalLabel16">Ayuda / Soporte</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body ">
                        <div class="row text-center">
                            <div class="col-md-12">
                                <h2 class="primary text-center">Health Administration Solution</h2>
                                <h5 class="secondary"> Estamos aquí para ayudarte Contactanos via:</h5>
                                <img src="https://has-humanly.com/empresa_dev/public/img/contacto.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




<!-- BEGIN: Vendor JS-->
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
window.cornerstoneWADOImageLoader || document.write(
    '<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')
</script>
<script src=" https://has-humanly.com/empresa_dev/public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js   "></script>
<script src="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/js/quill.min.js"></script>
<script src="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/js/index.js"></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/cornerstone.min.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/cornerstoneMath.min.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/cornerstoneTools.min.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/dicomParser.min.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/initializeWebWorkers.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/uids.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/openJPEG-FixedMemory.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/librerias-dicom/logica.js   "></script>
<script src=" https://has-humanly.com/empresa_dev/public/js/empresa/empleado/obtenerdaicom.js   "></script>

<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/core/app-menu.min.js"></script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/core/app.min.js"></script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/scripts/components.min.js"></script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/scripts/customizer.min.js"></script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/scripts/footer.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>





        
    
</body></html>
