<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Prueba de inicio de sesión</title>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Health Administration Solution</title>
        <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
        <link rel="shortcut icon" type="image/x-icon" href="https://has.humanly-sw.com/public/logo.png">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
    
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/vendors/css/vendors.min.css ">
            <!-- END: Vendor CSS-->
    
        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="https://has-humanly.com/empresa/public/vuexy/app-assets/css/themes/semi-dark-layout.min.css">

    
      </head>
</head>
<body>
    <div class="container">
          {{-- <button onclick="sesionRecepcion()">Recepción</button>
    <button onclick="sesionTecnicoRadiologo()">Técnico radiólogo</button>
    <button onclick="sesionMedicoRadiologo()">Médico radiólogo</button> --}}
    <h5 class="text-center mt-5">Medicina Laboral</h5>
    <center>
        <div class="col-md-4">
            <button class="btn btn-primary btn-block float-right btn-inline waves-effect waves-light" onclick="sesionMedicina()">Iniciar Sesión</button>
        </div>
    </center>
        
  
   
    
    {{-- <button onclick="sesionLaboratorio()">Laboratorio</button> --}}
    </div>
  

    <!--Scripts-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('js/Laboratorio/audiometria/enlaceaudiometria.js') !!}"></script>
    
</body>

</html>
