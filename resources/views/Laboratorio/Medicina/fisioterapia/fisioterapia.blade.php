@extends('layouts.VuexyLaboratorio')

@section('title')

@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/vendors/css/forms/selects/selectize.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/vendors/css/forms/selects/selectize.default.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/vendors/css/forms/selects/select2.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/vendors/css/extensions/sweetalert.css')!!}">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/css/plugins/animate/animate.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/css/plugins/forms/selectize/selectize.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/css/plugins/forms/checkboxes-radios.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/css/plugins/forms/wizard.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/css/plugins/pickers/daterange/daterange.css')!!}">
<link rel="stylesheet" type="text/css" href="{!! asset('app-assets/css/fisioterapiacaritas.css')!!}">
@endsection
@section('css_custom')
<style>
  .app-content .wizard.wizard-circle>.steps>ul>li:before,
  .app-content .wizard.wizard-circle>.steps>ul>li:after {
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.done .step {
    border-color: #f26b3e;
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.current .step {
    color: #f26b3e;
    border-color: #f26b3e;
  }

  .app-content .wizard>.actions>ul>li>a {
    background-color: #f26b3e;
    border-radius: .4285rem;
  }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Fisioterapia de {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }} {{ $empleado->nombre }}
        </li>
    </ol>

</nav>
<div class="card" id="estudios_pro">
    <div class="card-content collapse show" style="">
        <div class="card-header bg-secondary">
            <h5 class="text-white">Fisioterapia</h5>
        </div>
        <div class="card-body">
         
          <form id="form" method="post" class="number-tab-steps wizard-circle">
                @csrf
                <input type="hidden" name="nim" value="{{$fisioterapia->nim}}"/>
                <input type="hidden" name="curp" value="{{$fisioterapia->curp}}"/>
                <h6>Test de Evaluación funcional de oswestry</h6>
                @include('Laboratorio.Medicina.includes.oswestry')
                <h6>Cuestionario para determinar dolor lumbar inespecifico</h6>
                @include('Laboratorio.Medicina.includes.dolorlumbar')
                <h6>Goniometría</h6>
                @include('Laboratorio.Medicina.includes.Goniometria')
             
               
              </form>
        </div>
    </div>
</div>



@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! asset('app-assets/vendors/js/extensions/sweetalert.min.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/menu/jquery.mmenu.all.min.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/extensions/jquery.steps.min.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/forms/select/selectize.min.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/pickers/daterange/daterangepicker.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')!!}"></script>
<script src="{!! asset('app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js')!!}"></script>

{{-- checkbox --}}
<script src="{!! asset('app-assets/vendors/js/menu/jquery.mmenu.all.min.js')!!}"></script>
<script src="{!! asset('app-assets/vendors/js/forms/icheck/icheck.min.js')!!}"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<!-- BEGIN PAGE LEVEL JS-->

{{-- checkbox --}}
<script src="{!! asset('cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js')!!}"></script>
<script src="{!! asset('app-assets/js/scripts/forms/checkbox-radio.js')!!}"></script>

<script src="{!! asset('resources/js/Laboratorio/fisioterapia.js') !!}"></script>

<script src="{!! asset('app-assets/js/scripts/forms/select/form-selectize.js')!!}"></script>

<!-- END PAGE LEVEL JS-->
@endsection