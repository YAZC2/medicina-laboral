@extends('layouts.VuexyLaboratorio')

@section('title')
Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
{{-- Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
  .app-content .wizard.wizard-circle>.steps>ul>li:before,
  .app-content .wizard.wizard-circle>.steps>ul>li:after {
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.done .step {
    border-color: #f26b3e;
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.current .step {
    color: #f26b3e;
    border-color: #f26b3e;
  }

  .app-content .wizard>.actions>ul>li>a {
    background-color: #f26b3e;
    border-radius: .4285rem;
  }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb ml-1">
    <li class="breadcrumb-item"><a href="{{route('historial_empresas')}}">Pacientes</a></li>
    <li class="breadcrumb-item active" aria-current="page">Historial Clinico de
      {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
  </ol>
</nav>


<div class="">
  <div class=" content-body">
    <section id="number-tabs">

      <div class="card">
        <div class="card-content collapse show">
          <div class="card-header">
            <input type="hidden" value="{{$paciente->empresa_id}}" id="empresa_id">
            <h5 class="card-title">Formatos pabsa</h5>
          </div>
          <div class="card-body">

            <select class="form-control" id="exampleFormControlSelect1" name="formatos"
              onChange="formatoOnChange(this)">
              <option value="">Selecciona un formato</option>
              <option value="REG001">REG 001</option>
            </select>
          </div>
        </div>
        <div class="col-12">
          <div class="card" id="formato" style="display:none;">
            <div class="card-header bg-secondary">

              <h4 class="card-title text-white mb-1" id="titulo">Examen Médico</h4>
              <a class="float-right text-white mb-1" id="doc_pdf" href="" target="_blank">
                <i class="feather icon-printer "></i>
              </a>
              @if(count($errors) > 0)
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Error en la validación</strong> <br>
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
            </div>

            <div class="card-content collapse show">
              <div class="card-body">
                <form id="form" method="post" class="number-tab-steps wizard-circle">

                  @csrf
                  <input type="hidden" name="formulario" id="formulario" value="">
                  <input type="hidden" name="pacienteId" value="{{encrypt($paciente->id)}}">
                  <input type="hidden" id="generoPaciente" name="generoPaciente" value="{{$paciente->genero}}">
                  <input type="hidden" id="curp" name="curp" value="{{$paciente->CURP}}">

                  <!-- Step 1 -->
                  <h6>Identificacón</h6>
                  @include('Laboratorio.Medicina.includes.identificacion')
                  <!-- Step 2 -->
                  <h6>Heredofamiliares</h6>
                  @include('Laboratorio.Medicina.includes.heredofamiliares')
                  <!-- Step 3 -->
                  <h6>Antecedentes personales no patológicos</h6>
                  @include('Laboratorio.Medicina.includes.no_patologicos')
                  <!-- Step 4 -->
                  <h6>Antecedentes personales patológicos</h6>
                  @include('Laboratorio.Medicina.includes.patologicos')
                  <!-- Step 5 (damas) -->
                  @if ($paciente->genero=='Femenino'||$paciente->genero=='FEMENINO')
                  <h6>Antecedentes Ginecoobstetricos</h6>
                  @include('Laboratorio.Medicina.includes.ginecoobstetricos')
                  @endif
                  <!-- Step 6 -->
                  <h6>Antecedentes Laborales</h6>
                  @include('Laboratorio.Medicina.includes.historia_laboral')

                  <!-- Step 7 -->
                  <h6>Exploración Física</h6>
                  @include('Laboratorio.Medicina.includes.exploracion_fisica')

                  <h6>Resultados de Aptitud</h6>
                  @include('Laboratorio.Medicina.includes.resultados')
                </form>
              </div>
            </div>
          </div>
          <div class="card" id="fisioterapia" style="display:none;">
            <div class="card-header bg-secondary">

              <h4 class="card-title text-white mb-1" id="titulo">FISIOTERAPIA</h4>
              <a class="float-right text-white mb-1" id="doc_pdf" href="" target="_blank">
                <i class="feather icon-printer "></i>
              </a>
              @if(count($errors) > 0)
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Error en la validación</strong> <br>
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
            </div>

            {{-- <div class="card-content collapse show">
              <div class="card-body">
                <form id="form" method="post" class="number-tab-steps wizard-circle">

                  @csrf
                  <input type="hidden" name="formulario" id="formulario" value="">
                  <input type="hidden" name="pacienteId" value="{{encrypt($paciente->id)}}">
                  <input type="hidden" id="generoPaciente" name="generoPaciente" value="{{$paciente->genero}}">
                  <input type="hidden" id="curp" name="curp" value="{{$paciente->CURP}}">


                  <h6>Test de Evaluación funcional de oswestry</h6>
                  @include('Laboratorio.Medicina.includes.oswestry')
                  <h6>Cuestionario para determinar dolor lumbar indesoecifico</h6>
                  @include('Laboratorio.Medicina.includes.dolorlumbar')
                  <h6>Goniometría</h6>
                  @include('Laboratorio.Medicina.includes.Goniometria')
                </form>
              </div>
            </div> --}}
          </div>

        </div>
      </div>
      <div class="card" id="estudios_pro">
        <div class="card-content collapse show" style="">
          <div class="card-header">
            <h5 class="primary">Estudios Programados</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-8 mb-1">
                <input type="text" class="form-control search input-sm" placeholder="Buscar Nim">
              </div>
              <div class='col-md-4'>
                <button id="btnopenmedicamento" type="button" onclick='modal_estudio();'
                  class="btn btn-secondary waves-effect waves-float waves-light">Programar Estudio</button>
              </div>
              <div class="row col-md-12 list">

                @forelse ($estudios as $estudio)
                <div class="col-lg-3 col-sm-6 rounded">
                  <div class="card" style="box-shadow: 0 4px 24px 0 rgb(34 41 47 / 30%) !important;">
                    <div class="card-header">
                      <div class="row">
                        <a data-id="{{ $estudio->id }}" class="badge avatar bg-danger float-left elimar_estudio"><i
                            class="feather icon-trash-2 text-white"></i></a>
                        <a data-id="{{ $estudio->id }}" data-nim="{{ $estudio->folio }}"
                          class="badge avatar bg-warning float-left editar_estudio"><i
                            class="feather icon-edit text-white"></i></a>

                      </div>
                   
                      <span class="float-right primary"><b>{{ $estudio->fecha_inicial }}</b></span>
                    </div>
                    
                    <div class="card-body d-flex align-items-center justify-content-between">

                      <div>
                        <h3 class="fw-bolder mb-75 secondary folio">{{ $estudio->folio }}</h3>
                        

                      </div>

                      <div class="avatar bg-primary">
                        <span class="avatar-content">


                          <a data-id="{{ $estudio->id }}" class="showEstudio">
                            <i class="feather icon-layers"></i>


                          </a>

                        </span>

                      </div>
                    </div>
                  </div>
                </div>
                @empty
                <div class="alert col-md-12">
                  <div class="alert alert-danger text-center">
                    Ningún Nim registrado
                  </div>
                </div>
                @endforelse



              </div>
              <div class="col-md-12 mt-1">
                <div class="d-flex align-items-center justify-content-center">
                  <div id="anterior"></div>
                  <ul class="pagination justify-content-center m-0"></ul>
                  <div id="siguiente"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

  </div>
  </section>
</div>
</div>
{{-- fin de formulario --}}

<!-- Modal -->
<div class="modal animated bounceInDown text-left" id="bounceInDown" tabindex="-1" role="dialog"
  aria-labelledby="myModalLabel47" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel47">Resultados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-2">
            <div class="col-md-12" id="iframes">
              <a href="#" id="toolIframe">

              </a>
            </div>
          </div>
          <div class="col-md-10" id="contentIframe">
            <iframe width="" height="" class="col-12" style="display:none; height: 67vh;" id="iframeBg"></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade text-left" id="estudiosShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
  aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

    <div class="modal-content">
      <div class="modal-header bg-primary ">
        <h4 class="modal-title float-left">Estudios Programados</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body mt-2">

        <div class="col-md-12 text-center loadEstudios" style="display:none">
          <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>


        <div class="row contEstudios">

        </div>



      </div>
    </div>

  </div>
</div>

<div class="modal fade text-left" id="newestudio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
  aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

    <div class="modal-content">
      <div class="modal-header bg-primary ">
        <h4 class="modal-title float-left">Programar Nuevos Estudios </h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <input type="hidden" value="{{$paciente->id}}" id="id_paciente">
      <input type="hidden" value="{{$paciente->empresa_id}}" id="empresa_id">
      <div class="modal-body mt-2">
        <div class="row">
          <div class="col-md-8 mb-1">
            <input type="text" class="form-control search input-sm" name="nim_sass" placeholder="Buscar Estudios">
          </div>
          <div class='col-md-4'>
            <button id="btnagregarempleado" type="button" class="btn btn-primary waves-effect waves-float waves-light"
              onclick="obtener_estudios()">Buscar</button>
          </div>
        </div>

        <div class="row" id="estudiosass">
        </div>
        <div id="datos" class="col-md-12"></div>
      </div>
    </div>

  </div>
</div>
<div class="modal fade text-left" id="edit_estudio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

    <div class="modal-content">
      <div class="modal-header bg-primary ">
        <h4 class="modal-title float-left">Actualizar Estudio </h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <input type="hidden" value="{{$paciente->id}}" id="id_paciente">
      <input type="hidden" value="{{$paciente->empresa_id}}" id="empresa_id">
      <div class="modal-body mt-2">
        <div class="row" id="actualiza_es"></div>
        <div id="Actualiza_es" class="col-md-12"></div>
      </div>
    </div>

  </div>
</div>
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="../app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<!-- BEGIN PAGE LEVEL JS-->
{{-- <script src="app-assets/js/scripts/forms/wizard-steps.js"></script> --}}
{{-- checkbox --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>
<script src="../app-assets/js/scripts/forms/checkbox-radio.js"></script>
<script src="../resources/js/Laboratorio/MedicinaPaciente.js"></script>
<script src="../resources/js/Laboratorio/estudioslaboratorio.js"></script>
<script src="../app-assets/js/scripts/forms/select/form-selectize.js"></script>

<!-- END PAGE LEVEL JS-->
@endsection