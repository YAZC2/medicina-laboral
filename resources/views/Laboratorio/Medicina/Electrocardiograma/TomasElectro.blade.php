@extends('layouts.VuexyLaboratorio')

@section('title')
{{-- Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
    .app-content .wizard.wizard-circle>.steps>ul>li:before,
    .app-content .wizard.wizard-circle>.steps>ul>li:after {
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.done .step {
        border-color: #f26b3e;
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.current .step {
        color: #f26b3e;
        border-color: #f26b3e;
    }

    .app-content .wizard>.actions>ul>li>a {
        background-color: #f26b3e;
        border-radius: .4285rem;
    }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{ route('empleados') }}">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Tomas Electrocardiograma
            {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
    </ol>

</nav>
@if(!empty($electrocardiograma->documento1))
    @if($electrocardiograma->documento1!=null)
                
    <div class="card" id="estudios_pro">
        <div class="card-content collapse show" style="">
            <div class="card-header bg-secondary">
                <h5 class="text-white text-center">Electrocardiograma-Imagen 1</h5>
            </div>
            <div class="card-body">
            
                    <embed src="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->documento1) !!}" type="application/pdf" width="100%;" height="700px">
                
            
                                
                            
            </div>
        </div>
    </div>
    @endif
@endif
@if(!empty($electrocardiograma->documento2))
    @if($electrocardiograma->documento2!=null)
    <div class="card" id="estudios_pro">
        <div class="card-content collapse show" style="">
            <div class="card-header bg-secondary">
                <h5 class="text-white text-center">Electrocardiograma-Imagen 2</h5>
            </div>
            <div class="card-body">
                    <embed src="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->documento2) !!}" type="application/pdf" width="100%;" height="700px">
            </div>
        </div>
    </div>
    @endif
@endif
@if(empty($electrocardiograma->documento1) && empty($electrocardiograma->documento2))
    <div class="card" id="estudios_pro">
        <div class="card-content collapse show" style="">
            <div class="card-header bg-secondary">
                <h5 class="text-white text-center">Electrocardiograma</h5>
            </div>
            <div class="card-body">
                <div class="alert alert-primary">
                    <h5 class="primary text-center">No se ha subido ninguna toma de Electrocardiograma</h5>
                </div>
            </div>
        </div>
    </div>
@endif
@if(!empty($electrocardiograma->interpretacionpdf))
<div class="card" id="estudios_pro">
    <div class="card-content collapse show" style="">
        <div class="card-header bg-secondary">
            <h5 class="text-white text-center">Interpretaci贸n pdf</h5>
        </div>
        <div class="card-body">
                <embed src="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->interpretacionpdf) !!}" type="application/pdf" width="100%;" height="700px">
        </div>
    </div>
</div>
@else
<div class="card" id="estudios_pro">
    <div class="card-content collapse show" style="">
        <div class="card-body">
            <div class="alert alert-primary">
                <h5 class="primary text-center">No hay ninguna interpretaci贸n</h5>
            </div>
        </div>
    </div>
</div>
@endif



@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>

<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
{{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
<script src="{!! asset('js/Laboratorio/electrocardiograma/electrocardiograma.js') !!}"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
@endsection