@extends('layouts.VuexyLaboratorio')

@section('title')
{{-- Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
    .app-content .wizard.wizard-circle>.steps>ul>li:before,
    .app-content .wizard.wizard-circle>.steps>ul>li:after {
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.done .step {
        border-color: #f26b3e;
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.current .step {
        color: #f26b3e;
        border-color: #f26b3e;
    }

    .app-content .wizard>.actions>ul>li>a {
        background-color: #f26b3e;
        border-radius: .4285rem;
    }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Electrocardiograma
            {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
    </ol>

</nav>
<div class="card" id="estudios_pro">
    <div class="card-content collapse show" style="">
        <div class="card-header bg-secondary">
            <h5 class="text-white">Estudio Electrocardiograma</h5>
        </div>
        <div class="card-body">
            {{-- <form method="post" id="form_electro">
                <input type="hidden" value="{{ $paciente->CURP }}" name="curp" id="curp">

                <div class="row">

                    <div class="form-label-group col-md-8">
                        <div class="row ml-0">
                            <div class='col-md'>
                                <input type="file" accept="application/pdf" class="custom-file-input" name="archivo"
                                    id='archivo'>
                                <label for="archivo" class='custom-file-label'>Spacelabs Healthcare Sentinel</label>
                            </div>

                            <div class='col-md-3'>
                                <a class="mr-50" target="_blank" href="{!! asset('storage/app/espirometria/' ) !!}">
                                    <img src="https://img.icons8.com/color/48/000000/download-from-cloud.png" />
                                </a>

                            </div>

                        </div>
                    </div>
                    <div class="form-label-group col-md-8">
                        <label for="">Nombre:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-label-group col-md-12">
                        <p>Ficha de Identidad</p>
                        <hr>
                        <label for="">Nombre:</label>
                        <label for="nombre">{{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' .
                            $paciente->apellido_materno }}</p></label>
                        <input type="text" class="form-group" name="nombre" value="">


                    </div>
                </div>
                <button class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Guardar</button>
            </form> --}}

            <form method="post" id="formelectro">
                <div class="row">
                    <input type="hidden" value="{{ $paciente->CURP }}" name="curp" id="curp">
                    <input type="hidden" value="{{ $paciente->id }}" name="id_paciente" id="id_paciente">
                    <input type="hidden" value="{{ $estudio->folio }}" name="nim" id="nim">
                   
                    <div class="form-label-group col-md-12">
                        <div class="row ml-0">
                            <div class='col-md-6'>
                                <input type="file" accept="application/pdf" class="custom-file-input" name="archivo"
                                    id='archivo' multiple>
                                <label for="archivo" class='custom-file-label'>Spacelabs Healthcare Sentinel</label>
                            </div>
                            
                            <div class='col-md-3'>
                                @if($electrocardiograma->documento1!=null)
                                <a class="mr-50" target="_blank" href="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->documento1) !!}">
                                    <img src="https://img.icons8.com/color/48/000000/download-from-cloud.png" />
                                </a>
                                @endif
                                @if($electrocardiograma->documento2!=null)
                                <a class="mr-50" target="_blank" href="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->documento2) !!}">
                                    <img src="https://img.icons8.com/color/48/000000/download-from-cloud.png" />
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="row ml-0">
                            <div class='col-md-6'>
                                <input type="file" accept="application/pdf" class="custom-file-input" name="interpretacionpdf"
                                    id='interpretacionpdf' multiple>
                                <label for="interpretacionpdf" class='custom-file-label'>Interpretación pdf</label>
                            </div>
                            
                            <div class='col-md-3'>
                                @if($electrocardiograma->interpretacionpdf!=null)
                                <a class="mr-50" target="_blank" href="{!! asset('storage/app/electrocardiograma/' . $electrocardiograma->interpretacionpdf) !!}">
                                    <img src="https://img.icons8.com/color/48/000000/download-from-cloud.png" />
                                </a>
                                @endif
                               
                            </div>
                           
                            
                            
                        </div>

                    </div>
                    <div class="col-md-12">
                        <h6 class="primary">Ficha de Identidad</h6>
                        <hr class="bg-primary">
                    </div>
                    <div class="col-md-3">
                        <label for="GET-name"><b>Nombre:</b></label>
                        <label for="GET-name"> {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' .
                            $paciente->apellido_materno }}</label>
                    </div>
                    <div class="col-md-3">
                        <label for="GET-name"><b>Edad:</b></label>
                        <label for="GET-name">{{ $paciente->CURP }}</label>
                    </div>
                    <div class="col-md-3">
                        <label for="GET-name"><b>Origen:</b></label>
                        <label for="GET-name">Matriz</label><br>
                        <label for="GET-name"><b>Correo:</b></label>
                        <label for="GET-name">areamedica@laboratorioasesores.com</label>
                        
                    </div>
                    <div class="col-3">
                        <fieldset class="form-group">
                            <label class="d-block">ESTUDIO:</label>
                           
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary quimico" name="tipo_resultado"
                                @if($electrocardiograma->tipo_resultado =='NORMAL' && !empty($electrocardiograma->tipo_resultado)) checked @endif value="NORMAL" id="normal" onchange="estudio();">
                                <label class="custom-control-label" for="normal">NORMAL</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tipo_resultado"
                                @if($electrocardiograma->tipo_resultado =='ANORMAL' && !empty($electrocardiograma->tipo_resultado)) checked @endif value="ANORMAL" id="anormal" onchange="estudio();">
                                <label class="custom-control-label" for="anormal">ANORMAL</label>
                            </div>
                           
                        </fieldset>
                    </div>
                    {{-- <div class="col-md-12 mt-2">
                        <h6 class="primary">Parámetros</h6>
                        <hr class="bg-primary">
                    </div>
                    <div class="col-md-3 align-items-start">
                        <label for="GET-name"><b>Frecuencia:</b></label>
                        <input type="text" value="{{ $electrocardiograma->frecuencia }}" name="frecuencia" id="frecuencia" class="form-control">
                    </div>
                    <div class="col-md-3 align-items-start">
                        <label for="GET-name"><b>Eje de P:</b></label>
                        <input type="text" value="{{ $electrocardiograma->eje_p }}" name="eje_p" id="eje_p" class="form-control">
                    </div>
                    <div class="col-md-3 align-items-start">
                        <label for="GET-name"><b>Eje de QRS :</b></label>
                        <input type="text" value="{{ $electrocardiograma->ejeqrs }}" class="form-control" name="ejeqrs" id="ejeqrs">
                    </div>
                    <div class="col-md-3 align-items-start">
                        <label for="GET-name"><b>Eje de T :</b></label>
                        <input type="text" value="{{ $electrocardiograma->ejet }}" class="form-control" name="ejet" id="ejet">
                    </div>
                    <div class="col-md-12 mt-2">
                        <h6 class="primary">Intervalos</h6>
                        <hr class="bg-primary">
                    </div>
                    <div class="col-md-3 align-items-start">
                        <label for="GET-name"><b>R-R:</b></label>
                        <input type="text" value="{{ $electrocardiograma->rr }}" class="form-control" name="rr" id="rr">
                    </div>
                    <div class="col-md-3 align-items-start">
                        <label for="GET-name"><b>P-R:</b></label>
                        <input type="text" value="{{ $electrocardiograma->pr }}" class="form-control" name="pr" id="pr">
                    </div>
                    <div class="col-md-2 align-items-start">
                        <label for="GET-name"><b>QRS :</b></label>
                        <input type="text" value="{{ $electrocardiograma->qrs }}" class="form-control" name="qrs" id="qrs">
                    </div>
                    <div class="col-md-2 align-items-start">
                        <label for="GET-name"><b>QTm :</b></label>
                        <input type="text" value="{{ $electrocardiograma->qtm }}" class="form-control" name="qtm" id="qtm">
                    </div>
                    <div class="col-md-2 align-items-start">
                        <label for="GET-name"><b>QTc :</b></label>
                        <input type="text" value="{{ $electrocardiograma->qtc }}" class="form-control" name="qtc" id="qtc">
                    </div> --}}
                    <div class="col-md-3 mt-2">
                        <textarea class="form-control" rows="10" cols="50" placeholder="Observaciones"
                            name="observaciones" id="observaciones">{{ $electrocardiograma->observaciones }}</textarea>

                    </div>
                    <div class="col-md-3 mt-2">
                        <textarea class="form-control" rows="10" cols="50" placeholder="Conclusiones"
                            name="conclusiones" id="conclusiones">{{ $electrocardiograma->conclusiones }}</textarea>

                    </div>
                    <div class="col-md-3 mt-2">
                        <textarea class="form-control" rows="10" cols="50" placeholder="Notas"
                            name="notas" id="notas">{{ $electrocardiograma->notas }}</textarea>

                    </div>
                    
                    <div class="col-md-12">
                        <button class="btn btn-sm btn-primary mt-2 waves-effect waves-light">Guardar</button>

                    </div>

                </div>
        </div>

        </form>



    </div>
</div>
</div>




@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>

<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
{{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
<script src="{!! asset('js/Laboratorio/electrocardiograma/electrocardiograma.js') !!}"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
@endsection