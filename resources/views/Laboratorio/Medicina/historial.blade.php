{{-- @extends('layouts.administradorApp') --}}
@extends('layouts.VuexyLaboratorio')
@section('title','Pacientes')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">
{{-- @dd(Session::all()) --}}
@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
@endsection
@section('content')
    <!-- DataTable starts -->
    <section id="data-list-view" class="data-list-view-header">
        <!-- DataTable starts -->
     
        <div class='col-md-4'>
            <button id="btnagregarempleado" type="button"
              class="btn btn-primary waves-effect waves-float waves-light">Agregar Paciente</button>
              @foreach ($empresas as $nombreempresa)
              
            <input type="hidden" class="form-control" value="{{ $nombreempresa->id }}" id='id_empresa' />
            @endforeach
          </div>
        <div class="table-responsive">
          <table class="table data-list-view">
            <thead>
              <tr>
                <th>Clave</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Edad</th>
                <th>Curp</th>
                
                <th>Ver</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($empleados as $value)
                <tr class="mb-1">
    
                  <td>{{ $value->clave }}</td>
                  <td>{{ $value->nombre }}</td>
                  <td>{{ $value->apellido_paterno }}</td>
                  <td>{{ $value->apellido_materno }}</td>
                  <td>{{ \Carbon::parse($value->fecha_nacimiento)->age }}</td>
                  <td>{{ $value->CURP }}</td>
                  
                  <td>
                    <a href="./Historial-Clinico/{{encrypt($value->CURP)}}" class="action-view actions_button">
                        <i class="feather icon-eye"></i>
                    </a>
                  </td>
                  <td>
                    
    
                    <button class="btn" onclick="cargadatos({{ $value->id }})"><i class="feather icon-edit"></i></button>
                 
                  </td>
                  <td>
                    <button class="btn" onclick="elimarem({{ $value->id }})"><i class="feather icon-trash"></i></button>
                 
                  {{-- <span class="action-delete actions_button" data-id="{{ $value->id }}"></span> --}}
                
                  </td>
                </tr>
              @endforeach
    
            </tbody>
          </table>
        </div>
       
      </section>

<div id="agregarmodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="bg-primary modal-header">
          <h4 class="modal-title float-left">Buscar Paciente</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <section id="basic-datatable">
            <div class="row">
              <div class="col-12">
                <div class="card-content">
                  <div class="card-body card-dashboard">
                    <div class='row'>
                      <div class='col-md-8'>
                        <input id='id_sass' name='id_sass' class="form-control input col-md-12" type="text"
                          placeholder="Buscar nim..." tabindex="-1" data-search="search" value=''>
                      </div>
                      <div class='col-md-4'>
                        <button type="button" class="btn btn-primary waves-effect waves-float waves-light"
                          id="busca_empleado" onclick="obtener_estudios()">Buscar</button>
                      </div>
                    </div>
                    <div class='row'>
                      <div class='col-md-12 mt-2'>
                        <div id='datos'>
                        </div>
                        {{-- <div id="estudios">
                          <div class='col-md-12' id='busqueda'>
                          </div>
                          <div class="row list"></div>
                          <div class="col-md-12 mt-1">
                            <div class="d-flex align-items-center justify-content-center">
                              <div id="anterior"></div>
                              <ul class="pagination justify-content-center m-0"></ul>
                              <div id="siguiente"></div>
                            </div>
                          </div>
  
                        </div> --}}
                        <div class='boton col-md-12'>
  
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
  
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
</div>
<div id="editar" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="bg-primary modal-header">
              <h4 class="modal-title float-left">Editar Paciente</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
              <form method="post" id="form_e_edit">
                  <div class="form-row">
                      <input type="hidden" name="id_e" class="form-control" id="id_em" value='' required>

                      <div class="form-group col-md-12">
                          <label for="nombre">Nombre:</label>
                          <input type="text" name="nombre_em" class="form-control" id="nombre_em" value='' required>
                      </div>
                      <div class="form-group col-md-12">
                        <label for="nombre">Apellido Paterno:</label>
                        <input type="text" name="apellido_p_em" class="form-control" id="apellido_p_em" value='' required>
                      </div>
                      <div class="form-group col-md-12">
                        <label for="nombre">Apellido Materno:</label>
                        <input type="text" name="apellido_m_em" class="form-control" id="apellido_m_em" value='' required>
                      </div>
                      <div class="form-group col-md-12">
                        <label for="nombre">Edad:</label>
                        <input type="date" name="edad_em" class="form-control" id="edad_em" value='' required>
                      </div>
                      <div class="form-group col-md-12">
                          <label for="clave">Curp:</label>
                          <input type="text" name="curp_em" class="form-control" id="curp_em" value=''>
                      </div>
                      <div class="form-group col-md-12">
                        <label for="clave">Genero:</label>
                        <select class="form-control" id="genero" name="genero" onchange="">
                        </select>
                      </div>
                     

                  </div>
                  <div class="float-right">
                      <button type="submit" class="btn btn-sm btn-primary" id="btnedit">
                          <span class="spinner-border  spinner-border-sm" role="status" aria-hidden="true"
                              style="display: none;"></span>
                          Actualizar
                      </button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>



    @endsection
    @section('page_js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
    
    @endsection
    
@section('page_js')

@endsection
@section('js_custom')
  <script src="{!! asset('public/js/empresa/paciente.js') !!}"></script>
  
  <script src=" {!! asset('resources/js/Laboratorio/Medicina.js') !!} "></script>
  <script src="{!! asset('public/js/empresa/empleado/resultados.js') !!}" charset="utf-8"></script>
  

@endsection
