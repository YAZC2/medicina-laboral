@extends('layouts.VuexyLaboratorio')
@section('title','Configuración Formularios')
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')
<style>
    .verde {
        background-color: #20aa5e !important;
    }
</style>

@endsection
@section('css_custom')

@endsection
@section('content')

<section id="basic-datatable">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header bg-secondary">
                    <h4 class="card-title text-white">Formatos</h4>
                </div>

                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class='col-md-12'>
                            <table class="table_estudios table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Titulo</th>
                                        <th class="text-center">Estatus</th>
                                        <th class="text-center">Habilitar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($formatos as $row)
                                    @if($row->status=='1')
                                    @php
                                    $clase="badge rounded-pill bg-success verde";
                                    $estatus='Habilitado';
                                    @endphp
                                    @else
                                    @php
                                    $clase="badge rounded-pill bg-danger";
                                    $estatus='Deshabilitado';
                                    @endphp
                                    @endif
                                    <tr>
                                        <input type="hidden" value="{{ $row->status }}" id="Estatus_Ac_{{ $row->id }}"
                                            name="Estatus_Ac_{{ $row->id }}" />
                                        <td class="text-center">{{$row->titulo}}</td>
                                        <td class="text-center" id="Estatus_{{ $row->id }}"><span
                                                class="{{ $clase }}">{{ $estatus }}</span></td>
                                        <td class="text-center">
                                            <div class="custom-control custom-switch custom-switch-primary mb-1"
                                                onchange="cambiar_estatus('{{$row->id}}')">
                                                <input type="checkbox" {{ $row->status == '1' ? 'checked' : '' }}
                                                class="custom-control-input" name="{{ $row->prefijo }}"
                                                id="{{ $row->prefijo }}">
                                                <label class="custom-control-label" for="{{ $row->prefijo }}">
                                                    <span class="switch-text-left">Si</span>
                                                    <span class="switch-text-right">No</span>
                                                </label>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection


@section('page_vendor_js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   ">
</script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}">
</script>

@endsection

@section('page_js')
<script src="{!! asset('public/js/empresa/formatos.js') !!}">
</script>
@endsection
@section('js_custom')


@endsection