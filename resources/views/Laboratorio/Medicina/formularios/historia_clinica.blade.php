@extends('layouts.VuexyLaboratorio')

@section('title')
Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
@endsection

@section('begin_vendor_css')
    <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.default.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/select2.min.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/extensions/sweetalert.css") !!}">
    <!-- END VENDOR CSS-->
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/animate/animate.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/selectize/selectize.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/checkboxes-radios.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/wizard.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/pickers/daterange/daterange.css") !!}">
@endsection
@section('css_custom')
    <style>
        .app-content .wizard.wizard-circle > .steps > ul > li:before, .app-content .wizard.wizard-circle > .steps > ul > li:after{
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.done .step{
            border-color: #f26b3e;
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.current .step{
            color: #f26b3e;
            border-color: #f26b3e;
        }
        .app-content .wizard > .actions > ul > li > a{
            background-color: #f26b3e;
            border-radius: .4285rem;
        }
    </style>
@endsection
{{-- BEGIN body html --}}
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item"><a href="{{ url('Medicina-Pacientes') }}">Pacientes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</li>
        </ol>
    </nav>
    {{-- inicio de formulario --}}
    <div class="">
        <div class="content-body"><!-- Form wizard with number tabs section start -->
            <section id="number-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h4 class="card-title text-white">Examen Médico</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="number-tab-steps wizard-circle">
                                        <h6>Generales</h6>
                                        <fieldset>
                                            <div id="generales">
                                                <form method="POST" id="form_0" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="fecha">Fecha:</label>
                                                            <input readonly type="text" class="form-control" name="fecha" id="fecha" value="{!! now()->format('d/M/Y') !!}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="tipo_examen">Tipo de examen:</label>
                                                            <input type="text" class="form-control" name="tipo_examen" id="tipo_examen" value="Ingreso" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="folio">Folio:</label>
                                                            <input type="text" class="form-control" name="folio" id="folio" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->folio !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="nombre">Nombre:</label>
                                                            <input type="text" class="form-control" name="nombre" id="nombre" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->nombre !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="apellido_paterno">Apellido paterno:</label>
                                                            <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->apellido_paterno !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="apellido_materno">Apellido materno:</label>
                                                            <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->apellido_materno !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="sexo">Sexo:</label>
                                                            <select class="form-control" name="sexo" id="sexo">
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->sexo == "femenino" ? "selected":"") !!} value="femenino">Femenino</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->sexo == "masculino" ? "selected":"") !!} value="masculino">Masculino</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="edad">Edad:</label>
                                                            <input type="text" class="form-control" name="edad" id="edad" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->edad !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="estado_civil">Estado civil:</label>
                                                            <select name="estado_civil" id="estado_civil" class="form-control">
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->estado_civil == "soltero" ? "selected":"") !!} value="soltero">Soltera(o)</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->estado_civil == "casado" ? "selected":"") !!} value="casado">Casada(o)</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->estado_civil == "divorciado" ? "selected":"") !!} value="divorciado">Divorsiada(o)</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->estado_civil == "viudo" ? "selected":"") !!} value="viudo">Viuda(o)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="fecha_nacimiento">Fecha de nacimiento:</label>
                                                            <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->fecha_nacimiento !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="telefono">Teléfono:</label>
                                                            <input type="text" class="form-control" name="telefono" id="telefono" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->telefono !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="escolaridad">Escolaridad:</label>
                                                            <select name="escolaridad" id="escolaridad" class="form-control">
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->escolaridad == "sin_escolaridad" ? "selected":"") !!} value="sin_escolaridad">Sin escolaridad</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->escolaridad == "primaria" ? "selected":"") !!} value="primaria">Primaria</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->escolaridad == "secundaria" ? "selected":"") !!} value="secundaria">Secundaria</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->escolaridad == "bachiller" ? "selected":"") !!} value="bachiller">Bachiller</option>
                                                                <option {!! is_null($historial_clinico->generales_n) ?'': ($historial_clinico->generales_n->escolaridad == "licenciatura" ? "selected":"") !!} value="licenciatura">Licenciatura</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="lugar_nacimiento">Lugar de nacimiento:</label>
                                                            <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->lugar_nacimiento !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="grupo_rh">Grupo y Rh:</label>
                                                            <input type="text" class="form-control" name="grupo_rh" id="grupo_rh" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->grupo_rh !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="no_imss">No. IMSS:</label>
                                                            <input type="text" class="form-control" name="no_imss" id="no_imss" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->no_imss !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="domicilio">Domicilio:</label>
                                                            <input type="text" class="form-control" name="domicilio" id="domicilio" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->domicilio !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="fecha_ingreso">Fecha de ingreso:</label>
                                                            <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso" value="{!! is_null($historial_clinico->generales_n) ?'': $historial_clinico->generales_n->fecha_ingreso !!}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Antecedentes</h6>
                                        <fieldset>
                                            <div id="antecedentes_laborales">
                                                <form method="POST" id="form_1" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="descripcion_al">Antecedente laborales:</label>
                                                            <textarea class="form-control" name="descripcion_al" id="descripcion_al" rows="2">{!! is_null($historial_clinico->antecedentes_n) ?'': $historial_clinico->antecedentes_n->descripcion_al !!}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="descripcion_ahf">Antecedentes heredo familiares:</label>
                                                            <textarea class="form-control" name="descripcion_ahf" id="descripcion_ahf" rows="2">{!! is_null($historial_clinico->antecedentes_n) ?'': $historial_clinico->antecedentes_n->descripcion_ahf !!}</textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Antecedentes personales patologicos</h6>
                                        <fieldset>
                                            <div id="antecedentes_personales_patologicos">
                                                <form method="POST" id="form_2" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="tabaquismo">Tabaquismo:</label>
                                                            <input type="text" class="form-control" id="tabaquismo" name="tabaquismo" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->tabaquismo !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="alcoholismo">Alcoholismo:</label>
                                                            <input type="text" class="form-control" id="alcoholismo" name="alcoholismo" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->alcoholismo !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="drogas">Drogas:</label>
                                                            <input type="text" class="form-control" id="drogas" name="drogas" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->drogas !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="vacunas">Vacunas:</label>
                                                            <input type="text" class="form-control" id="vacunas" name="vacunas" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->vacunas !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="ejercicio">Ejercicio:</label>
                                                            <input type="text" class="form-control" id="ejercicio" name="ejercicio" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->ejercicio !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="padecimiento_actual">Padecimiento actual:</label>
                                                            <input type="text" class="form-control" id="padecimiento_actual" name="padecimiento_actual" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->padecimiento_actual !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="fecha_ultima_consulta">Fecha última consulta médico:</label>
                                                            <input type="date" class="form-control" id="fecha_ultima_consulta" name="fecha_ultima_consulta" value="{!! is_null($historial_clinico->antecedentes_personales_patologicos_n) ?'': $historial_clinico->antecedentes_personales_patologicos_n->fecha_ultima_consulta !!}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Exploracion fisica</h6>
                                        <fieldset>
                                            <div id="exploracion_fisica">
                                                <form method="POST" id="form_3" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="peso">Peso:</label>
                                                            <input type="text" class="form-control" id="peso" name="peso" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->peso !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="talla">Talla:</label>
                                                            <input type="text" class="form-control" id="talla" name="talla" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->talla !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="imc">IMC:</label>
                                                            <input type="number" class="form-control" id="imc" name="imc" readonly value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->imc !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="per_abdom">Per. Abdom.:</label>
                                                            <input type="number" class="form-control" id="per_abdom" name="per_abdom" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->per_abdom !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="peso_max">Peso Max. Recom.:</label>
                                                            <input type="text" class="form-control" id="peso_max" name="peso_max" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->peso_max !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="kg_mas">Kg de más:</label>
                                                            <input type="text" class="form-control" id="kg_mas" name="kg_mas" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->kg_mas !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="presion_arterial">Presión arterial:</label>
                                                            <fieldset>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="presion_arterial_1" id="presion_arterial_1" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->presion_arterial_1 !!}">
                                                                    <input class="form-control text-center" type="text" readonly value="/">
                                                                    <input type="text" class="form-control" name="presion_arterial_2" id="presion_arterial_2" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->presion_arterial_2 !!}">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="ta_media">TA media:</label>
                                                            <input type="number" class="form-control" id="ta_media" name="ta_media" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->ta_media !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label for="frec_card">Frec. Card.:</label>
                                                            <fieldset>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" id="frec_card" name="frec_card" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->frec_card !!}">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">bpm</span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="flexibilidad">Flexibilidad :</label>
                                                            <input type="number" class="form-control" id="flexibilidad" name="flexibilidad" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->flexibilidad !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="apariencia_exterior">Apariencia exterior :</label>
                                                            <input type="text" class="form-control" id="apariencia_exterior" name="apariencia_exterior" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->apariencia_exterior !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="temp">Temp :</label>
                                                            <fieldset>
                                                                <div class="input-group">
                                                                    <input type="number" class="form-control" id="temp" name="temp" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->temp !!}">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">C°</span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="f_resp">F. Resp. :</label>
                                                            <input type="text" class="form-control" id="f_resp" name="f_resp" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->f_resp !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="cabeza">Cabeza :</label>
                                                            <input type="text" class="form-control" id="cabeza" name="cabeza" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->cabeza !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="ojos">Ojos :</label>
                                                            <input type="text" class="form-control" id="ojos" name="ojos" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->ojos !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="reflejos">Reflejos :</label>
                                                            <input type="text" class="form-control" id="reflejos" name="reflejos" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->reflejos !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="pterigiones">Pterigiones :</label>
                                                            <input type="text" class="form-control" id="pterigiones" name="pterigiones" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->pterigiones !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="fondo_ojos">Fondo de ojos :</label>
                                                            <input type="text" class="form-control" id="fondo_ojos" name="fondo_ojos" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->fondo_ojos !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="av_izquierdo">AV izquierdo :</label>
                                                            <input type="text" class="form-control" id="av_izquierdo" name="av_izquierdo" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->av_izquierdo !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="av_derecho">AV derecho :</label>
                                                            <input type="text" class="form-control" id="av_derecho" name="av_derecho" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->av_derecho !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="vision_cromatica">Visión cromática :</label>
                                                            <input type="text" class="form-control" id="vision_cromatica" name="vision_cromatica" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->vision_cromatica !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label for="presbicia">Presbicia :</label>
                                                            <input type="text" class="form-control" id="presbicia" name="presbicia" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->presbicia !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="oidos">Oidos :</label>
                                                            <input type="text" class="form-control" id="oidos" name="oidos" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->oidos !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="nariz">Nariz :</label>
                                                            <input type="text" class="form-control" id="nariz" name="nariz" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->nariz !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="faringe_lengua">Faringe y lengua :</label>
                                                            <input type="text" class="form-control" id="faringe_lengua" name="faringe_lengua" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->faringe_lengua !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="dental">Dental :</label>
                                                            <input type="text" class="form-control" id="dental" name="dental" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->dental !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="cuello">Cuello :</label>
                                                            <input type="text" class="form-control" id="cuello" name="cuello" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->vision_cromatica !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="torax">Torax :</label>
                                                            <input type="text" class="form-control" id="torax" name="torax" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->torax !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="corazon">Corazón :</label>
                                                            <input type="text" class="form-control" id="corazon" name="corazon" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->corazon !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="pulmones">Pulmones :</label>
                                                            <input type="text" class="form-control" id="pulmones" name="pulmones" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->pulmones !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="torax_posterior_columna">Torax posterior y columna :</label>
                                                            <input type="text" class="form-control" id="torax_posterior_columna" name="torax_posterior_columna" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->torax_posterior_columna !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="ingles_genitales">Ingles y genitales :</label>
                                                            <input type="text" class="form-control" id="ingles_genitales" name="ingles_genitales" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->ingles_genitales !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="extremidades_superiores">Extremidades superiores :</label>
                                                            <input type="text" class="form-control" id="extremidades_superiores" name="extremidades_superiores" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->extremidades_superiores !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="extremidades_inferiores">Extremidades inferiores :</label>
                                                            <input type="text" class="form-control" id="extremidades_inferiores" name="extremidades_inferiores" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->extremidades_inferiores !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="piel_faneras">Piel y faneras :</label>
                                                            <input type="text" class="form-control" id="piel_faneras" name="piel_faneras" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->piel_faneras !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="laboratorios">Laboratorios :</label>
                                                            <input type="text" class="form-control" id="laboratorios" name="laboratorios" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->laboratorios !!}">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="glucosa">Glucosa :</label>
                                                            <input type="text" class="form-control" id="glucosa" name="glucosa" value="{!! is_null($historial_clinico->exploracion_fisica_n) ?'': $historial_clinico->exploracion_fisica_n->glucosa !!}">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                        <h6>Conclusiones</h6>
                                        <fieldset>
                                            <div id="conclusion">
                                                <form method="POST" id="form_4" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="conclusion">Conclusión:</label>
                                                            <textarea class="form-control" name="conclusion" id="conclusion" cols="30" rows="10">{!! is_null($historial_clinico->conclusiones_n) ?'': $historial_clinico->conclusiones_n->conclusion !!}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="recomendaciones">Recomendaciones:</label>
                                                            <textarea class="form-control" name="recomendaciones" id="recomendaciones" cols="30" rows="10">{!! is_null($historial_clinico->conclusiones_n) ?'': $historial_clinico->conclusiones_n->recomendaciones !!}</textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    {{-- fin de formulario --}}
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! url("app-assets/vendors/js/extensions/sweetalert.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/extensions/jquery.steps.min.js") !!}"></script>
{{-- <script src="{!! url("app-assets/vendors/js/forms/select/selectize.min.js") !!}"></script> --}}
<script src="{!! url("app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/pickers/daterange/daterangepicker.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/validation/jquery.validate.min.js") !!}"></script>
<script src="{!! url("app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js") !!}"></script>

{{-- checkbox --}}
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/icheck/icheck.min.js") !!}"></script>
<!-- END PAGE VENDOR JS-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

@endsection


@section('js_custom')
<script src="{!! url("app-assets/js/scripts/forms/checkbox-radio.js") !!}"></script>
<script>
    $(".number-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        enableAllSteps: true,
        titleTemplate: '<span class="step">#index#</span> #title#',
        cssClass: 'wizard',
        labels: {
            finish: 'Guardar',
            previous: 'Anterior',
            next: 'Siguiente'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: $("#form_"+currentIndex).serialize(),
                url: '../../save_formulario_2/'+currentIndex
            }).done((res) => {
                console.log(res);
            }).fail((err) => {
                console.log(err);
            });
            return true;
        },
        onFinished: function(event, currentIndex) {
            swal({
                title: "¿Seguro que desea terminar el historial clinico?",
                text: "Esta acción es irreversible",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Si, terminar",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
                }).then(isConfirm => {
                if (isConfirm) {
                    let data = {
                        "historial_clinico_id":$("input[name='historial_clinico_id']").val()
                    }
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        url: '../../save_formulario_2/finalizado'
                    }).done((res) => {
                        console.log(res);
                        location.reload();
                    }).fail((err) => {
                        console.log(err);
                    });
                    swal("Exito", "Guardado con exito", "success");
                } else {
                    swal("Operacion Cancelada", "Puede continuar...", "error");
                }
            });
        }
    });

    $("#telefono").mask("(000)-000-00-00", {placeholder: "(000)-00-00-00"});
    $("#edad").mask("00");
    $("#no_imss").mask("00000000000");

    $("#talla").mask("0.00 mt");

    // $("#presion_arterial").mask("0.00 mt");


    $("#peso").blur(function(){
        let a = $(this).val();
        $(this).val(numeral(a).format('0.00')+" kg");

        let peso = $(this).val().replace("kg","");
        let talla = $("#talla").val().replace("mt","");
        if(peso != "" && talla != ""){
            calcularIMC(talla,peso);
        }
    });

    $("#talla").blur(function(){
        let a = $(this).val();
        $(this).val(numeral(a).format('0.00')+" mt");

        let talla = $(this).val().replace("mt","");
        let peso = $("#peso").val().replace("kg","");
        if(peso != "" && talla != ""){
            calcularIMC(talla,peso);
        }
    });

    $("#peso_max").blur(function(){
        let a = $(this).val();
        $(this).val(numeral(a).format('0.00')+" kg");
    });

    $("#kg_mas").blur(function(){
        let a = $(this).val();
        $(this).val(numeral(a).format('0.00')+" kg");
    });

    function calcularIMC(talla,peso) {
        let resultado = peso / (talla ^2);
        $("#imc").val(resultado.toFixed(1));
    }
</script>
@endsection
