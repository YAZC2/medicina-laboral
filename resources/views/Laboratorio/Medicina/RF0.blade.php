@extends('layouts.VuexyLaboratorio')

@section('title')
Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
{{-- Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
  .app-content .wizard.wizard-circle>.steps>ul>li:before,
  .app-content .wizard.wizard-circle>.steps>ul>li:after {
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.done .step {
    border-color: #f26b3e;
    background-color: #f26b3e;
  }

  .app-content .wizard>.steps>ul>li.current .step {
    color: #f26b3e;
    border-color: #f26b3e;
  }

  .app-content .wizard>.actions>ul>li>a {
    background-color: #f26b3e;
    border-radius: .4285rem;
  }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb ml-1">
    <li class="breadcrumb-item"><a href="{{ url('Medicina-Pacientes') }}">Pacientes</a></li>
    <li class="breadcrumb-item active" aria-current="page">Historial Clinico de
      {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
  </ol>
</nav>


<div class="">
  <div class=" content-body">
    <section id="number-tabs">
   
      <div class="card">
        <div class="card-content collapse show">
          <div class="card-header">
               <input type="text" value="{{$paciente->empresa_id}}" id="empresa_id">
            <h5 class="card-title">Formatos</h5>
          </div>
          <div class="card-body">

            <select class="form-control" id="exampleFormControlSelect1" name="formatos" onChange="formatoOnChange(this)">
              <option value="">Selecciona un formato</option>
              <option value="REG001">REG 001</option>
            </select>
          </div>
        </div>
        <div class="col-12">
          <div class="card" id="formato" style="display:none;">
            <div class="card-header bg-secondary">
              
                <h4 class="card-title text-white mb-1" id="titulo">Examen Médico</h4>
                <a class="float-right text-white mb-1" id="doc_pdf" href="" target="_blank">
                  <i class="feather icon-printer "></i>
                </a>
                @if(count($errors) > 0)
                 <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Error en la validación</strong> <br>
                  <ul>
                   @foreach($errors->all() as $error)
                   <li>{{ $error }}</li>
                   @endforeach
                  </ul>
                 </div>
                @endif
            </div>
          
            <div class="card-content collapse show">
                <div class="card-body">
                    <form id="form" method="post" class="number-tab-steps wizard-circle">
                    
                      @csrf
                      <input type="hidden" name="formulario" id="formulario" value="">
                      <input type="hidden" name="pacienteId" value="{{encrypt($paciente->id)}}">
                      <input type="hidden" id="generoPaciente" name="generoPaciente" value="{{$paciente->genero}}">
                      <input type="hidden" id="curp" name="curp" value="{{$paciente->CURP}}">
                      
                         <!-- Step 1 -->
                        <h6>Identificacón</h6>
                        @include('Laboratorio.Medicina.includes.identificacion')
                        <!-- Step 2 -->
                        <h6>Heredofamiliares</h6>
                        @include('Laboratorio.Medicina.includes.heredofamiliares')
                         <!-- Step 3 -->
                          <h6>Antecedentes personales no patológicos</h6>
                        @include('Laboratorio.Medicina.includes.no_patologicos')
                        <!-- Step 4 -->
                        <h6>Antecedentes personales patológicos</h6>
                        @include('Laboratorio.Medicina.includes.patologicos')
                        <!-- Step 5 (damas) -->
                        @if ($paciente->genero=='Femenino'||$paciente->genero=='FEMENINO')
                          <h6>Antecedentes Ginecoobstetricos</h6>
                        @include('Laboratorio.Medicina.includes.ginecoobstetricos')
                        @endif
                        <!-- Step 6 -->
                        <h6>Antecedentes Laborales</h6>
                        @include('Laboratorio.Medicina.includes.historia_laboral')
                        
                         <!-- Step 7 -->
                        <h6>Exploración Física</h6>
                        @include('Laboratorio.Medicina.includes.exploracion_fisica')
                        
                       <h6>Resultados de Aptitud</h6>
                        @include('Laboratorio.Medicina.includes.resultados')
                    </form>
                </div>
            </div>
        </div>
          
        </div>
      </div>

     
    
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-secondary">
              <h4 class="card-title pb-2" style="color:#fff !important;">Estudios Programados</h4>
            </div>
            <div class="card-content collapse show" style="">
              <div class="col-md-4">
                @foreach ($estudios as $estudio)
                <div class="text-center shadow bg-white p-1  rounded">
                  <h5>{{ $estudio->folio }}</h5>
                  <hr>
                  <small class="d-block">{{ $estudio->nombre }}</small>
                  <small class="d-block">{{ $estudio->nombre }}</small>
                  <hr>
                  <a data-id="{{$estudio->id}}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                    <i class="feather icon-eye"></i>
                 </a> 
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>

  </div>
  </section>
</div>
</div>
{{-- fin de formulario --}}

<!-- Modal -->
<div class="modal animated bounceInDown text-left" id="bounceInDown" tabindex="-1" role="dialog"
  aria-labelledby="myModalLabel47" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel47">Resultados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-2">
            <div class="col-md-12" id="iframes">
              <a href="#" id="toolIframe">

              </a>
            </div>
          </div>
          <div class="col-md-10" id="contentIframe">
            <iframe width="" height="" class="col-12" style="display:none; height: 67vh;" id="iframeBg"></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade text-left" id="estudiosShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    
        <div class="modal-content">
        <div class="modal-header bg-primary ">
                <h4 class="modal-title float-left">Estudios Programados</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body mt-2">
                
                <div class="col-md-12 text-center loadEstudios" style="display:none">
                    <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>


                <div class="row contEstudios">

                </div>


            </div>
        </div>
    
    </div>
</div>
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="../app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<!-- BEGIN PAGE LEVEL JS-->
{{-- <script src="app-assets/js/scripts/forms/wizard-steps.js"></script> --}}
{{-- checkbox --}}
<script src="../app-assets/js/scripts/forms/checkbox-radio.js"></script>
<script src="../resources/js/Laboratorio/MedicinaPaciente.js"></script>
<script src="../app-assets/js/scripts/forms/select/form-selectize.js"></script>
<!-- END PAGE LEVEL JS-->
@endsection