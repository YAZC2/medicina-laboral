@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaPaci.css">
@endsection

@section('content')
<div class="title-content">
   <h1 class="title">Pacientes Ingresados</h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <br>
    </div>
    <div class="panel-body">
        <div class="row content_card" id="admin_list">
            <form id="search" method="post" class="col-md-12 mb-3">
                @csrf
                <div class="row">
                    <div class="col-12 input-group input-focus">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-white" ><i class="fa fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control search border-left-0 col-12 input_buscador" name="search"  placeholder="Busqueda de pacientes">
                    </div>
                </div>
            </form>
            <div class="col-12">
                <div class="turnt content_card row list">
                @forelse ($resultados as $estudio)
                    <div class="col-md-4 list_custom" id="{{$estudio->expediente->curp}}">
                        @if ($estudio->estudio_id == 1382)
                            <a target="_blank" class="card" href="{!! url('espirometria-view/'.$estudio->espirometria->id) !!}">
                        @elseif ($estudio->estudio_id == 1383)
                            <a target="_blank" class="card" href="{!! url('resultadoAudiometria/'.$estudio->audiometria->id) !!}">
                        @else
                            <a class="card" onclick="Modal('{!! $estudio->archivo->archivo !!}','{!! $estudio->archivo->tipo !!}','{{$estudio->estudio->nombre}}')">
                        @endif
                        <div class="card-header">
                            <h6 class="card-title font-weight-normal search_estudio">Estudio: {{$estudio->estudio->nombre}}</h6>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <p class="small search_fecha">Fecha: {!! $estudio->created_at->format('d/M/Y h:i a') !!}</p>
                                @if ($estudio->medico == null)
                                <p class="small search_medico">Medico: S/A</p>
                                @else
                                <p class="small search_medico">Medico: {!! $estudio->medico->titulo_profesional.'. '.$estudio->medico->nombre_completo !!}</p>
                                @endif
                            </div>
                        </div>
                        </a>
                    </div>
                @empty
                    <p>Aún no hay pacientes</p>
                @endforelse
                </div>
            </div>
            <div class="col-12 mb-2 mt-1">
                <div class="row">
                    <div class="col-12">
                        <h1 id="msg-result" class="d-none">0 resultados</h1>
                        <div class="d-flex align-items-center justify-content-center">
                            <div id="anterior"></div>
                            <ul class="pagination justify-content-center m-0"></ul>
                            <div id="siguiente"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="estudios" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title float-left text-white" id="estudio_title"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="loader-wrapper mx-auto" style="display:none" id="load">
                    <div class="loader-container">
                        <div class="ball-clip-rotate-multiple loader-success">
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div id="resultado_contenido"></div>
                </div>
                <div class="col-12 pt-2">
                    <button type="button" data-dismiss="modal" class="mx-auto btn  btn-secondary btn-sm">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_custom')
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>
<script src="{!! asset('/js/Laboratorio/historial_pacientes/paciente_estudios.js') !!}"></script>
@endsection
