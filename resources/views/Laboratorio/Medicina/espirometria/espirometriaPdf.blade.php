<meta charset="UTF-8">
<title>{{ $espiro->nim }}</title>
{{-- <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}"> --}}
<style media="screen">
body {
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: #212529;
  text-align: left;
  background-color: #fff;
}

p{
    font-size: .9rem;
    margin: 0%;
    padding: 0%;
    letter-spacing: 1px;
}

td{
    padding: 0 ;
    margin: 0;
    font-size: .8rem;
    margin: 0%;
    padding: 0%;
    letter-spacing: 1px;
}
tr{
    padding: 0 ;
    margin: 0;
}
.text-center{
    text-align: center !important;
}
.mt-1{
    margin-top:
}

.m-0 {
  margin: 0 !important;
}

.mt-0,
.my-0 {
  margin-top: 0 !important;
}

.mr-0,
.mx-0 {
  margin-right: 0 !important;
}

.mb-0,
.my-0 {
  margin-bottom: 0 !important;
}

.ml-0,
.mx-0 {
  margin-left: 0 !important;
}

.m-1 {
  margin: 0.25rem !important;
}

.mt-1,
.my-1 {
  margin-top: 0.25rem !important;
}

.mr-1,
.mx-1 {
  margin-right: 0.25rem !important;
}

.mb-1,
.my-1 {
  margin-bottom: 0.25rem !important;
}

.ml-1,
.mx-1 {
  margin-left: 0.25rem !important;
}

.m-2 {
  margin: 0.5rem !important;
}

.mt-2,
.my-2 {
  margin-top: 0.5rem !important;
}

.mr-2,
.mx-2 {
  margin-right: 0.5rem !important;
}

.mb-2,
.my-2 {
  margin-bottom: 0.5rem !important;
}

.ml-2,
.mx-2 {
  margin-left: 0.5rem !important;
}

.m-3 {
  margin: 1rem !important;
}

.mt-3,
.my-3 {
  margin-top: 1rem !important;
}

.mr-3,
.mx-3 {
  margin-right: 1rem !important;
}

.mb-3,
.my-3 {
  margin-bottom: 1rem !important;
}

.ml-3,
.mx-3 {
  margin-left: 1rem !important;
}

.m-4 {
  margin: 1.5rem !important;
}

.mt-4,
.my-4 {
  margin-top: 1.5rem !important;
}

.mr-4,
.mx-4 {
  margin-right: 1.5rem !important;
}

.mb-4,
.my-4 {
  margin-bottom: 1.5rem !important;
}

.ml-4,
.mx-4 {
  margin-left: 1.5rem !important;
}

.m-5 {
  margin: 3rem !important;
}

.mt-5,
.my-5 {
  margin-top: 3rem !important;
}

.mr-5,
.mx-5 {
  margin-right: 3rem !important;
}

.mb-5,
.my-5 {
  margin-bottom: 3rem !important;
}

.ml-5,
.mx-5 {
  margin-left: 3rem !important;
}

.p-0 {
  padding: 0 !important;
}

.pt-0,
.py-0 {
  padding-top: 0 !important;
}

.pr-0,
.px-0 {
  padding-right: 0 !important;
}

.pb-0,
.py-0 {
  padding-bottom: 0 !important;
}

.pl-0,
.px-0 {
  padding-left: 0 !important;
}

.p-1 {
  padding: 0.25rem !important;
}

.pt-1,
.py-1 {
  padding-top: 0.25rem !important;
}

.pr-1,
.px-1 {
  padding-right: 0.25rem !important;
}

.pb-1,
.py-1 {
  padding-bottom: 0.25rem !important;
}

.pl-1,
.px-1 {
  padding-left: 0.25rem !important;
}

.p-2 {
  padding: 0.5rem !important;
}

.pt-2,
.py-2 {
  padding-top: 0.5rem !important;
}

.pr-2,
.px-2 {
  padding-right: 0.5rem !important;
}

.pb-2,
.py-2 {
  padding-bottom: 0.5rem !important;
}

.pl-2,
.px-2 {
  padding-left: 0.5rem !important;
}

.p-3 {
  padding: 1rem !important;
}

.pt-3,
.py-3 {
  padding-top: 1rem !important;
}

.pr-3,
.px-3 {
  padding-right: 1rem !important;
}

.pb-3,
.py-3 {
  padding-bottom: 1rem !important;
}

.pl-3,
.px-3 {
  padding-left: 1rem !important;
}

.p-4 {
  padding: 1.5rem !important;
}

.pt-4,
.py-4 {
  padding-top: 1.5rem !important;
}

.pr-4,
.px-4 {
  padding-right: 1.5rem !important;
}

.pb-4,
.py-4 {
  padding-bottom: 1.5rem !important;
}

.pl-4,
.px-4 {
  padding-left: 1.5rem !important;
}

.p-5 {
  padding: 3rem !important;
}

.pt-5,
.py-5 {
  padding-top: 3rem !important;
}

.pr-5,
.px-5 {
  padding-right: 3rem !important;
}

.pb-5,
.py-5 {
  padding-bottom: 3rem !important;
}

.pl-5,
.px-5 {
  padding-left: 3rem !important;
}

.m-n1 {
  margin: -0.25rem !important;
}

.mt-n1,
.my-n1 {
  margin-top: -0.25rem !important;
}

.mr-n1,
.mx-n1 {
  margin-right: -0.25rem !important;
}

.mb-n1,
.my-n1 {
  margin-bottom: -0.25rem !important;
}

.ml-n1,
.mx-n1 {
  margin-left: -0.25rem !important;
}

.m-n2 {
  margin: -0.5rem !important;
}

.mt-n2,
.my-n2 {
  margin-top: -0.5rem !important;
}

.mr-n2,
.mx-n2 {
  margin-right: -0.5rem !important;
}

.mb-n2,
.my-n2 {
  margin-bottom: -0.5rem !important;
}

.ml-n2,
.mx-n2 {
  margin-left: -0.5rem !important;
}

.m-n3 {
  margin: -1rem !important;
}

.mt-n3,
.my-n3 {
  margin-top: -1rem !important;
}

.mr-n3,
.mx-n3 {
  margin-right: -1rem !important;
}

.mb-n3,
.my-n3 {
  margin-bottom: -1rem !important;
}

.ml-n3,
.mx-n3 {
  margin-left: -1rem !important;
}

.m-n4 {
  margin: -1.5rem !important;
}

.mt-n4,
.my-n4 {
  margin-top: -1.5rem !important;
}

.mr-n4,
.mx-n4 {
  margin-right: -1.5rem !important;
}

.mb-n4,
.my-n4 {
  margin-bottom: -1.5rem !important;
}

.ml-n4,
.mx-n4 {
  margin-left: -1.5rem !important;
}

.m-n5 {
  margin: -3rem !important;
}

.mt-n5,
.my-n5 {
  margin-top: -3rem !important;
}

.mr-n5,
.mx-n5 {
  margin-right: -3rem !important;
}

.mb-n5,
.my-n5 {
  margin-bottom: -3rem !important;
}

.ml-n5,
.mx-n5 {
  margin-left: -3rem !important;
}

.m-auto {
  margin: auto !important;
}

.mt-auto,
.my-auto {
  margin-top: auto !important;
}

.mr-auto,
.mx-auto {
  margin-right: auto !important;
}

.mb-auto,
.my-auto {
  margin-bottom: auto !important;
}

.ml-auto,
.mx-auto {
  margin-left: auto !important;
}
.text-right {
  text-align: right !important;
}
.text-left {
  text-align: left !important;
}
</style>

<div>
    <img width="20%" src="{!! asset("public/img/asesoress.jpg") !!}" alt="" style="margin: 0%; padding:0% ">
    <table width="100%">
        <tr>
            <td width="100%" class="text-center"  >
                <h4 style="margin: 0%; padding:0%;">REPORTE DE ESPIROMETRIA</h4>
            </td>
        </tr>
    </table>
    <table width="100%" >
        <tbody>
            <tr>
                <td>
                   <p>Referencia : {{ $espiro->nim }}</p>
                </td>
                <td>
                    <p>
                        Hora:
                     @php
                        $split = explode(" ",$espiro->tomaFecha);
                     @endphp
                        {{$split[1]}}
                    </p>

                </td>
                <td>
                    <p>
                        Fecha:
                        @php
                            $split = explode(" ",$espiro->tomaFecha);
                        @endphp
                        {{$split[0]}}
                    </p>

                </td>
            </tr>
            <tr>
                <td><p>Nombre:  {{ $espiro->nombre }} {{ $espiro->apellidos }}</p></td>
                <td>
                    <p>Sexo:
                    @if ($espiro->sexo == 'M')
                        Masculino
                        @else
                        Femenino
                    @endif
                    </p>
                    </td>
            </tr>
            <tr>

                <td><p>Edad: {{  $espiro->edad  }}</p></td>
                <td><p>Talla: {{ $espiro->talla }}</p></td>
                <td><p>Peso: {{ $espiro->peso }}</p></td>
                <td><p>Temp(°C):  {{ $espiro->temperatura }}</p></td>
            </tr>

            <tr>

                <td><p>Press(mmHg) {{ $espiro->presion }}</p></td>
                <td><p>Humedad: {{ $espiro->humedad }}  </p></td>
                <td><p>Fuma: </p></td>
                <td><p>
                    IMC:
                    @php
                        $altura = ($espiro->talla/100)*2;
                        $peso = $espiro->peso;
                    @endphp
                    {{ round($peso/$altura,1) }}
                </p></td>
            </tr>


        </tbody>

    </table>





    <div>
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="text-center" style="margin: 0%;padding:0%; margin-top:15px; margin-bottom:10px;">INFORME DE FVC  </h5>

                </div>
                <div class="card-content collapse show">
                  <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-sm" width="100%">
                            <thead class="bg-secondary text-white">
                                <tr>
                                    <th class="text-left">Parametro</th>
                                    <th class="text-left">Unidad</th>
                                    <th class="text-left">M1</th>
                                    <th class="text-left">%REF</th>
                                    <th class="text-left">M2</th>
                                    <th class="text-left">%REF</th>
                                    <th class="text-left">M3</th>
                                    <th class="text-left">%REF</th>
                                    <th class="text-left">REF</th>
                                    <th class="text-left">LNN</th>
                                </tr>
                              </thead>
                              <tbody>
                               @foreach ($espiro->resultados as  $value)

                                  <tr>
                                   <td>{{ $value->title }}</td>
                                   <td>{{ $value->units }}</td>
                                   <td>{{ $value->data }}</td>

                                   @if ($value->title == "Mejor Fvc")
                                      <td>{{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                      @elseif ($value->title == "Mejor Fev1")
                                        <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                    @elseif ($value->title == "MFEV1/MFVC")
                                        <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                   @endif

                                   <td>{{ $value->data }}</td>

                                  @if ($value->title == "Mejor Fvc")
                                     <td>{{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                     @elseif ($value->title == "Mejor Fev1")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                   @elseif ($value->title == "MFEV1/MFVC")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                  @endif
                                   <td>{{ $value->data }}</td>

                                  @if ($value->title == "Mejor Fvc")
                                     <td>{{round(($value->data*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                     @elseif ($value->title == "Mejor Fev1")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                   @elseif ($value->title == "MFEV1/MFVC")
                                       <td>{{round(($value->data*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                  @endif

                                   @if ($value->title == "Mejor Fvc")
                                      <td>{{ $espiro->ref->where('title','RFVC')->first()->data }}</td>
                                      @elseif ($value->title == "Mejor Fev1")
                                        <td>{{ $espiro->ref->where('title','RFEV1')->first()->data }}</td>
                                    @elseif ($value->title == "MFEV1/MFVC")
                                        <td>{{ $espiro->ref->where('title','RFEV1-FVC')->first()->data }}</td>
                                   @endif

                                   @if ($value->title == "Mejor Fvc")
                                      <td>{{ $espiro->lln->where('title','RFVC LLN')->first()->data }}</td>
                                      @elseif ($value->title == "Mejor Fev1")
                                        <td>{{ $espiro->lln->where('title','RFEV1 LLN')->first()->data }}</td>
                                    @elseif ($value->title == "MFEV1/MFVC")
                                        <td>{{ $espiro->lln->where('title','RFEV1-FVC LLN')->first()->data }}</td>
                                   @endif
                                  </tr>

                               @endforeach
                               @for ($i = 1; $i <= 17; $i++)
                                    @if ($i == 1)
                                        <tr>
                                            <td>FVC</td>
                                            <td>l</td>
                                            <td> {{$fvc1 = $espiro->maneobras->where('title','FVC')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{round(($fvc1*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                            <td> {{$fvc1 = $espiro->maneobras->where('title','FVC')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{round(($fvc1*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                            <td> {{$fvc1 = $espiro->maneobras->where('title','FVC')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{round(($fvc1*100)/$espiro->ref->where('title','RFVC')->first()->data) }}</td>
                                            <td> {{$espiro->ref->where('title','RFVC')->first()->data }} </td>
                                            <td>{{$espiro->lln->where('title','RFVC LLN')->first()->data }}</td>
                                        </tr>
                                    {{-- @elseif ($i == 2)
                                        <tr>
                                            <td>FEV0.5</td>
                                            <td>l</td>
                                            <td> {{$fvc1 = $espiro->maneobras->where('title','FEV0.5')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{$fvc1 = $espiro->maneobras->where('title','FEV0.5')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{$fvc1 = $espiro->maneobras->where('title','FEV0.5')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td></td>
                                        </tr> --}}
                                    @elseif ($i == 3)
                                        <tr>
                                            <td>FEV1</td>
                                            <td>l</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RFEV1')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEV1 LLN')->first()->data }} </td>
                                        </tr>
                                    @elseif ($i == 4)
                                        <tr>
                                            <td>FEV1/FVC</td>
                                            <td>%</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FVC')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FVC')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FVC')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-FVC')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RFEV1-FVC')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEV1-FVC LLN')->first()->data }} </td>
                                        </tr>
                                    @elseif ($i == 5)
                                        <tr>
                                            <td>PEF</td>
                                            <td>l/s</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','PEF')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RPEF')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','PEF')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RPEF')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','PEF')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RPEF')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RPEF')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RPEF LLN')->first()->data }} </td>
                                        </tr>
                                    @elseif ($i == 6)
                                        <tr>
                                            <td>FEF50%</td>
                                            <td>l/s</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEF50%')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEF50')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEF50%')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEF50')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEF50%')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEF50')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RFEF50')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEF50 LLN')->first()->data }} </td>
                                        </tr>
                                    @elseif ($i == 7)
                                        <tr>
                                            <td>FEF25%-75%</td>
                                            <td>l/s</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEF25%-FEF75%')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEF25-75')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEF25%-FEF75%')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEF25-75')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEF25%-FEF75%')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEF25-75')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RFEF25-75')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEF25-75 LLN')->first()->data }} </td>
                                        </tr>
                                    {{-- @elseif ($i == 8)
                                        <tr>
                                            <td>FEF50%/FIF50%</td>
                                            <td></td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> {{ $espiro->ref->where('title','RFEF50-FIF50')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEF50-FIF50 LLN')->first()->data }} </td>
                                        </tr> --}}
                                    {{-- @elseif ($i == 9)
                                        <tr>
                                            <td>MTT</td>
                                            <td>s</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','MTT')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','MTT')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','MTT')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr> --}}
                                    @elseif ($i == 10)
                                        <tr>
                                            <td>FEV1/FEV0.5</td>
                                            <td></td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FEV0.5')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-FEV0.5')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FEV0.5')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-FEV0.5')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FEV0.5')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-FEV0.5')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RFEV1-FEV0.5')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEV1-FEV0.5 LLN')->first()->data }} </td>
                                        </tr>
                                    @elseif ($i == 11)
                                        <tr>
                                            <td>FEV1/PEF</td>
                                            <td>%</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/PEF')->where('maneobra',3)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-PEF')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/PEF')->where('maneobra',2)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-PEF')->first()->data) }}</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/PEF')->where('maneobra',1)->first()->data}}</td>
                                            <td> {{ round(($fvc1*100)/$espiro->ref->where('title','RFEV1-PEF')->first()->data) }}</td>
                                            <td> {{ $espiro->ref->where('title','RFEV1-PEF')->first()->data }} </td>
                                            <td> {{ $espiro->lln->where('title','RFEV1-PEF LLN')->first()->data }} </td>
                                        </tr>
                                    {{-- @elseif ($i == 12)
                                        <tr>
                                            <td>FIF50%</td>
                                            <td>l/s</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr> --}}
                                    @elseif ($i == 13)
                                        <tr>
                                            <td>Vext</td>
                                            <td>l</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','Vext.')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','Vext.')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','Vext.')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>
                                    {{-- @elseif ($i == 14)
                                        <tr>
                                            <td>MVV Ind</td>
                                            <td>l/min</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','MVV Ind')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','MVV Ind')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','MVV Ind')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr> --}}
                                    @elseif ($i == 15)
                                        <tr>
                                            <td>FEV6</td>
                                            <td>l</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV6')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV6')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV6')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>
                                    @elseif ($i == 16)
                                        <tr>
                                            <td>FEV1/FEV6</td>
                                            <td>%</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FEV6')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FEV6')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV1/FEV6')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>
                                    {{-- @elseif ($i == 17)
                                        <tr>
                                            <td>FEV0.75/FVC</td>
                                            <td>%</td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV0.75/FVC')->where('maneobra',3)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV0.75/FVC')->where('maneobra',2)->first()->data}}</td>
                                            <td> </td>
                                            <td> {{ $fvc1 = $espiro->maneobras->where('title','FEV0.75/FVC')->where('maneobra',1)->first()->data}}</td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr> --}}

                                    @endif


                               @endfor
                               <tr>
                                   <td colspan="10">Repetibilidad ATS/ERS: FVC: {{ $espiro->repetibilidad_fvc }}, FEV1: {{ $espiro->repetibilidad_fev1 }}</td>
                               </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>


    <table width="100%" class="text-center">
        <td width="50%"> <img style="width: 17rem; height:17rem" src="{!! asset('storage/app/espirometria/'.$espiro->fv) !!}" alt=""> </td>
        <td width="50%"> <img style="width: 17rem; height:17rem" src="{!! asset('storage/app/espirometria/'.$espiro->vt) !!}" alt=""> </td>
    </table>
    <p class="text-center">
        {{ $espiro->interpretacion }}
    </p>
    <table width="100%" class="text-center mt-3">
        <tr>
            <td width="50%">
                21 Poniente 3711
                Col. Belisario Domínguez
            </td>

            <td width="50%" class="text-right">
                <img style="width: 100px" src="{{ asset('storage/app/datosHtds/'.$medico->firma) }}" alt="">
                <br>
                 {{ $medico->titulo_profesional }} {{ $medico->nombre_completo }}
                <br>
                Cedula Profesional {{ $medico->cedula }}
            </td>
        </tr>
    </table>

</div>

<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
