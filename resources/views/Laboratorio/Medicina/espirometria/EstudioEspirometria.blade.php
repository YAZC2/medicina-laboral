@extends('layouts.VuexyLaboratorio')

@section('title')
{{-- Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
    .app-content .wizard.wizard-circle>.steps>ul>li:before,
    .app-content .wizard.wizard-circle>.steps>ul>li:after {
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.done .step {
        border-color: #f26b3e;
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.current .step {
        color: #f26b3e;
        border-color: #f26b3e;
    }

    .app-content .wizard>.actions>ul>li>a {
        background-color: #f26b3e;
        border-radius: .4285rem;
    }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{route('historial_empresas')}}">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Espirometria
            {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
    </ol>

</nav>
<div class="card" id="estudios_pro">
    <div class="card-content collapse show" style="">
        <div class="card-header bg-secondary">
            <h5 class="text-white">Estudio Espirometria</h5>
        </div>
        <div class="card-body">
            <form method="post" id="form_espirometria" name="FORM1">
                <input type="hidden" value="{{ $paciente->CURP }}" name="curp" id="curp">
                <input type="hidden" value="{{ $estudio->folio }}" name="nim" id="nim">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <h5 class="card-subtitle">Fecha: {!! now()->format('d/M/Y') !!}</h5>
                        </fieldset>
                        <fieldset class="form-group">
                            <h5 class="card-subtitle">Empresa: {!! $empresa->nombre !!}</h5>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="p_trabajo">Puesto de trabajo solicitado:</label>
                            <input type="text" name="p_trabajo" id="p_trabajo" value="{{ $espirometria->p_trabajo }}" class="form-control">
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="form-group">
                            <h5 class="card-subtitle">Nim:{{ $estudio->folio }}</h5>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="d-block">¿Ha estado expuesto a agentes quimico o polvos?</label>
                           @if($espirometria->quimico=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary quimico" name="quimico"
                                    checked value="si" id="quimico_si">
                                <label class="custom-control-label" for="quimico_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="quimico"
                                    value="no" id="quimico_no">
                                <label class="custom-control-label" for="quimico_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary quimico" name="quimico"
                                     value="si" id="quimico_si">
                                <label class="custom-control-label" for="quimico_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="quimico"
                                checked value="no" id="quimico_no">
                                <label class="custom-control-label" for="quimico_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="t_puesto">Tiempo en el puesto:</label>
                            <input type="text" name="t_puesto" id="t_puesto" value="{{ $espirometria->t_puesto }}" class="form-control">
                        </fieldset>
                    </div>
                </div>
                
                <div class="row">
                    <div class="form-label-group col-md-4">
                        <fieldset class="form-group">
                            <h5>Criterios de exclusión</h5>
                            <label class="d-block">¿Ha tenido alguna cirugia en los últimos 6 meses?</label>
                            @if($espirometria->cirugia=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia"
                                    checked value="si" id="cirugia_si">
                                <label class="custom-control-label" for="cirugia_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia"
                                    value="no" id="cirugia_no">
                                <label class="custom-control-label" for="cirugia_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia"
                                     value="si" id="cirugia_si">
                                <label class="custom-control-label" for="cirugia_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia"
                                checked value="no" id="cirugia_no">
                                <label class="custom-control-label" for="cirugia_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-4">
                        <fieldset class="form-group">
                            <label class="d-block">¿Ha tenido desprendimiento de retina o alguna cirugia de ojos?</label>
                            @if($espirometria->cirugia_ojos=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia_ojos"
                                    checked value="si" id="cirugia_ojos_si">
                                <label class="custom-control-label" for="cirugia_ojos_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia_ojos"
                                    value="no" id="cirugia_ojos_no">
                                <label class="custom-control-label" for="cirugia_ojos_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia_ojos"
                                     value="si" id="cirugia_ojos_si">
                                <label class="custom-control-label" for="cirugia_ojos_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="cirugia_ojos"
                                checked value="no" id="cirugia_ojos_no">
                                <label class="custom-control-label" for="cirugia_ojos_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-4">
                        <div class="row">
                               <fieldset class="form-group">
                                    <label class="d-block">¿Ha tenido algun infarto o problema del corazón?</label>
                                    @if($espirometria->cirugia_ojos=='si')
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="infarto"
                                            checked value="si" id="infarto_si">
                                        <label class="custom-control-label" for="infarto_si">si</label>
                                    </div>
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="infarto"
                                            value="no" id="infarto_no">
                                        <label class="custom-control-label" for="infarto_no">no</label>
                                    </div>
                                    @else
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="infarto"
                                             value="si" id="infarto_si">
                                        <label class="custom-control-label" for="infarto_si">si</label>
                                    </div>
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="infarto"
                                        checked value="no" id="infarto_no">
                                        <label class="custom-control-label" for="infarto_no">no</label>
                                    </div>
                                    @endif
                                </fieldset>
                                <fieldset>
                                    <input type="text" name="cuando_infarto" id="cuando_infarto" placeholder="¿Cuándo?" value="{{ $espirometria->cuando_infarto }}" class="form-control">
                                </fieldset>
                            
                    </div>
                    </div>
                    <div class="form-label-group col-md-4">
                        <fieldset class="form-group">
                            <label class="d-block">¿Tiene o ha tenido Tuberculosis?</label>
                            @if($espirometria->cirugia_ojos=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tuberculosis"
                                    checked value="si" id="tuberculosis_si">
                                <label class="custom-control-label" for="tuberculosis_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tuberculosis"
                                    value="no" id="tuberculosis_no">
                                <label class="custom-control-label" for="tuberculosis_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tuberculosis"
                                     value="si" id="tuberculosis_si">
                                <label class="custom-control-label" for="tuberculosis_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tuberculosis"
                                checked value="no" id="tuberculosis_no">
                                <label class="custom-control-label" for="tuberculosis_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">¿Está embarazada?</label>
                            @if($espirometria->embarazada=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="embarazada"
                                    checked value="si" id="embarazada_si">
                                <label class="custom-control-label" for="embarazada_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="embarazada"
                                    value="no" id="embarazada_no">
                                <label class="custom-control-label" for="embarazada_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="embarazada"
                                     value="si" id="embarazada_si">
                                <label class="custom-control-label" for="embarazada_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="embarazada"
                                checked value="no" id="embarazada_no">
                                <label class="custom-control-label" for="embarazada_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">¿Tiene tos con sangre?</label>
                            @if($espirometria->tos_sangre=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tos_sangre"
                                    checked value="si" id="tos_sangre_si">
                                <label class="custom-control-label" for="tos_sangre_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tos_sangre"
                                    value="no" id="tos_sangre_no">
                                <label class="custom-control-label" for="tos_sangre_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tos_sangre"
                                     value="si" id="tos_sangre_si">
                                <label class="custom-control-label" for="tos_sangre_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tos_sangre"
                                checked value="no" id="tos_sangre_no">
                                <label class="custom-control-label" for="tos_sangre_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">¿Neumotórax?</label>
                            @if($espirometria->neumotorax=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="neumotorax"
                                    checked value="si" id="neumotorax_si">
                                <label class="custom-control-label" for="neumotorax_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="neumotorax"
                                    value="no" id="neumotorax_no">
                                <label class="custom-control-label" for="neumotorax_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="neumotorax"
                                     value="si" id="neumotorax_si">
                                <label class="custom-control-label" for="neumotorax_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="neumotorax"
                                checked  value="no" id="neumotorax_no">
                                <label class="custom-control-label" for="neumotorax_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">¿Traqueostomia?</label>
                            @if($espirometria->neumotorax=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Traqueostomia"
                                    checked value="si" id="Traqueostomia_si">
                                <label class="custom-control-label" for="Traqueostomia_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Traqueostomia"
                                    value="no" id="Traqueostomia_no">
                                <label class="custom-control-label" for="Traqueostomia_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Traqueostomia"
                                     value="si" id="Traqueostomia_si">
                                <label class="custom-control-label" for="Traqueostomia_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Traqueostomia"
                                checked value="no" id="Traqueostomia_no">
                                <label class="custom-control-label" for="Traqueostomia_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">SONDA PLEURAL</label>
                            @if($espirometria->neumotorax=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="sonda"
                                    checked value="si" id="sonda_si">
                                <label class="custom-control-label" for="sonda_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="sonda"
                                    value="no" id="sonda_no">
                                <label class="custom-control-label" for="sonda_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="sonda"
                                     value="si" id="sonda_si">
                                <label class="custom-control-label" for="sonda_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="sonda"
                                checked value="no" id="sonda_no">
                                <label class="custom-control-label" for="sonda_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">ANEURISMA</label>
                            @if($espirometria->neumotorax=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Aneurisma"
                                    checked value="si" id="Aneurisma_si">
                                <label class="custom-control-label" for="Aneurisma_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Aneurisma"
                                    value="no" id="Aneurisma_no">
                                <label class="custom-control-label" for="Aneurisma_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Aneurisma"
                                     value="si" id="Aneurisma_si">
                                <label class="custom-control-label" for="Aneurisma_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="Aneurisma"
                                checked value="no" id="Aneurisma_no">
                                <label class="custom-control-label" for="Aneurisma_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">TROMBOEMBOLIA</label>
                            @if($espirometria->TROMBOEMBOLIA=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="TROMBOEMBOLIA"
                                    checked value="si" id="TROMBOEMBOLIA_si">
                                <label class="custom-control-label" for="TROMBOEMBOLIA_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="TROMBOEMBOLIA"
                                    value="no" id="TROMBOEMBOLIA_no">
                                <label class="custom-control-label" for="TROMBOEMBOLIA_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="TROMBOEMBOLIA"
                                     value="si" id="TROMBOEMBOLIA_si">
                                <label class="custom-control-label" for="TROMBOEMBOLIA_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="TROMBOEMBOLIA"
                                checked value="no" id="TROMBOEMBOLIA_no">
                                <label class="custom-control-label" for="TROMBOEMBOLIA_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-3">
                        <fieldset class="form-group">
                            <label class="d-block">NAÚSEAS O VOMITO</label>
                            @if($espirometria->VOMITO=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="VOMITO"
                                    checked value="si" id="VOMITO_si">
                                <label class="custom-control-label" for="VOMITO_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="VOMITO"
                                    value="no" id="VOMITO_no">
                                <label class="custom-control-label" for="VOMITO_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="VOMITO"
                                     value="si" id="VOMITO_si">
                                <label class="custom-control-label" for="VOMITO_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="VOMITO"
                                checked value="no" id="VOMITO_no">
                                <label class="custom-control-label" for="VOMITO_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-4">
                        <fieldset class="form-group">
                            <label class="d-block">HA USADO ALGUN BRONCODILATADOR EN LAS ULTIMAS 3 HRS</label>
                            @if($espirometria->DILATADOR=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="DILATADOR"
                                    checked value="si" id="DILATADOR_si">
                                <label class="custom-control-label" for="DILATADOR_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="DILATADOR"
                                    value="no" id="DILATADOR_no">
                                <label class="custom-control-label" for="DILATADOR_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="DILATADOR"
                                     value="si" id="DILATADOR_si">
                                <label class="custom-control-label" for="DILATADOR_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="DILATADOR"
                                checked value="no" id="DILATADOR_no">
                                <label class="custom-control-label" for="DILATADOR_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-4">
                        <fieldset class="form-group">
                            <label class="d-block">HA CONSUMIDO ALIMENTOS O BEBIDAS EN LAS ULTIMAS 3 HRS</label>
                            @if($espirometria->ALIMENTOS=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="ALIMENTOS"
                                    checked value="si" id="ALIMENTOS_si">
                                <label class="custom-control-label" for="ALIMENTOS_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="ALIMENTOS"
                                    value="no" id="ALIMENTOS_no">
                                <label class="custom-control-label" for="ALIMENTOS_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="ALIMENTOS"
                                     value="si" id="ALIMENTOS_si">
                                <label class="custom-control-label" for="ALIMENTOS_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="ALIMENTOS"
                                checked value="no" id="ALIMENTOS_no">
                                <label class="custom-control-label" for="ALIMENTOS_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="form-label-group col-md-4">
                        <fieldset class="form-group">
                            <label class="d-block">HA REALIZADO EJERCICIO EN LA ÚLTIMA HORA</label>
                            @if($espirometria->EJERCICIO=='si')
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="EJERCICIO"
                                    checked value="si" id="EJERCICIO_si">
                                <label class="custom-control-label" for="EJERCICIO_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="EJERCICIO"
                                    value="no" id="EJERCICIO_no">
                                <label class="custom-control-label" for="EJERCICIO_no">no</label>
                            </div>
                            @else
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="EJERCICIO"
                                     value="si" id="EJERCICIO_si">
                                <label class="custom-control-label" for="EJERCICIO_si">si</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="EJERCICIO"
                                checked value="no" id="EJERCICIO_no">
                                <label class="custom-control-label" for="EJERCICIO_no">no</label>
                            </div>
                            @endif
                        </fieldset>
                    </div>
                    <div class="col-3">
                        <fieldset class="form-group">
                            <label class="d-block">ESTUDIO:</label>
                           
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary quimico" name="tipo_resultado"
                                @if($espirometria->tipo_resultado =='NORMAL' && !empty($espirometria->tipo_resultado)) checked @endif value="NORMAL" id="normal" onchange="estudio();">
                                <label class="custom-control-label" for="normal">NORMAL</label>
                            </div>
                            <div class="d-inline-block custom-control custom-radio mr-1">
                                <input type="radio" class="custom-control-input bg-secondary" name="tipo_resultado"
                                @if($espirometria->tipo_resultado =='ANORMAL' && !empty($espirometria->tipo_resultado)) checked @endif value="ANORMAL" id="anormal" onchange="estudio();">
                                <label class="custom-control-label" for="anormal">ANORMAL</label>
                            </div>
                           
                        </fieldset>
                    </div>
                   
                        <div class="col-md-12">
                            <div class="row">
                                <label for="firma_evaluado1" class="pl-2 text-center col-md-12 mt-1">FIRMA DEL EVALUADO</label>
                                @if(empty($espirometria->firma))

                                <div class="col-md-12">
                                    <div class="form-group row last">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <canvas id="cnv" name="firma_evaluado" width="500" height="100" value="sdasf"></canvas>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <br>
                                            <a id="button3" name="ClearBtn" class="btn btn-primary btn-xs text-white" onclick="javascript:onSign()">Firmar</a>
                                            <a id="button1" name="ClearBtn" class="btn btn-primary btn-xs text-white" onclick="javascript:onClear()">Limpiar</a>
                                            <a id="button2" name="DoneBtn" class="btn btn-warning btn-xs text-white" onclick="javascript:onDone()">Confirmar</a>
                                            <span id="mensajitoirma" class="primary"></span>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="col-md-12 text-center">
                                    <img src="data:image/png;base64,{{ $espirometria->firma }}">
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="bioSigData" value="">
                            <br>
                            <br>
                            <input type="hidden" name="sigImageData" id="img" value="@if(!empty($espirometria->firma)){{ $espirometria->firma }}@endif">
                            {{-- @if(!empty($historial['resultado']['sigImageData']))
                                                <label>Firma del evaluado anterior:</label>
                                                <img src="data:image/png;base64,{{ $historial['resultado']['sigImageData'] }}">
                            @endif --}}
                        </div>
                    
                </div>
                <div class="row">
                    <div class="form-label-group col-md-8">
                        <div class="row ml-0">
                            <div class='col-md'>
                                <input type="file" accept="application/pdf" class="custom-file-input" name="archivo"
                                    id='archivo'>
                                <label for="archivo" class='custom-file-label'>Subir Archivo Espirometria</label>
                            </div>
                            @if($espirometria->documento!=null)
                            <div class='col-md-3'>
                                <a class="mr-50" target="_blank"
                                    href="{!! asset('storage/app/espirometria/' . $espirometria->documento) !!}">
                                    <img src="https://img.icons8.com/color/48/000000/download-from-cloud.png" />
                                </a>

                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-label-group col-md-8">
                        <textarea class="form-control" name="comentarios" id="comentarios" rows="10" cols="50"
                            placeholder="Escribe un comentario">{{ $espirometria->comentarios }}
                        </textarea>


                    </div>
                </div>
                <button class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Guardar</button>
            </form>




        </div>
    </div>
</div>




@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>

<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
{{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
<script src="{!! asset('js/Laboratorio/Espirometria/espirometria.js') !!}"></script>
<script src="{!! asset('js/Laboratorio/audiometria/libreriafirma.js') !!}"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
@endsection