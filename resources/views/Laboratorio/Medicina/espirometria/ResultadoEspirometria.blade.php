@extends('layouts.VuexyLaboratorio')

@section('title')
{{-- Paciente: {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}
Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->

<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')
<style>
    .app-content .wizard.wizard-circle>.steps>ul>li:before,
    .app-content .wizard.wizard-circle>.steps>ul>li:after {
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.done .step {
        border-color: #f26b3e;
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.current .step {
        color: #f26b3e;
        border-color: #f26b3e;
    }

    .app-content .wizard>.actions>ul>li>a {
        background-color: #f26b3e;
        border-radius: .4285rem;
    }
</style>
@endsection
{{-- BEGIN body html --}}
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{ route('empleados') }}">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Espirometría
            {{ $paciente->nombre . ' ' . $paciente->apellido_paterno . ' ' . $paciente->apellido_materno }}</li>
    </ol>

</nav>
<div class="card" id="estudios_pro">
    <div class="card-content collapse show" style="">

        <div class="card-body">
            <div class="container row">
                <div class="col-md-12">
                    <img src="https://has-humanly.com/pagina_has/images/cahitoredondo.svg" class="float-left" width="200;">
                    <img src="https://has-humanly.com/pagina_has/images/logo_30.svg" class="float-right" width="200;">
                    <p class="text-center"><b>ASESORES ESPECIALIZADOS EN LABORATORIOS MATRIZ</b> <br>
                        21 PONIENTE No. 3711, COL BELISARIO DOMINGUEZ C.P. 72180, PUEBLA, PUE<br>
                        TELS. (222)2966608, 2966609, 2966610 y 2224540933</p>
                </div>
                @if( $espirometria!=null)
                <div class="col-md-4">
                    <p class="secondary"><b class="primary">Puesto de trabajo solicitado:</b> {{ $espirometria->p_trabajo }}</p>
                    
                </div>
                <div class="col-md-4">
                  
                    <p class="secondary"><b class="primary">Tiempo en el puesto:</b> {{ $espirometria->t_puesto }}</p>
                </div>
                <div class="col-md-4">
                    <p class="secondary"><b class="primary">Nim:</b> {{ $espirometria->nim }}</p>

                </div>
                <div class="col-md-12 mb-2">
                    <p class="secondary"><b class="primary">Interpretación:</b> {{ $espirometria->comentarios }}</p>
                </div><br>
            </div>

            @if($espirometria->documento!=null)
            <div class="col-md-12" style="z-index:100;background-image: url('https://has-humanly.com/pagina_has/images/cahitoredondo.svg');">
                <embed src="{!! asset('storage/app/espirometria/' . $espirometria->documento) !!}"
                    type="application/pdf" width="100%;" height="700px">
            </div>
            @else
            <div class="alert alert-danger" role="alert">
                No se ha subido ningún documento
            </div>
            @endif
            @else
            <div class="alert alert-danger" role="alert">
                No se ha realizado el estudio de espirometria
            </div>
            @endif








        </div>
    </div>
</div>




@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
<script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

{{-- checkbox --}}
<script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>

<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
{{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
<script src="{!! asset('js/Laboratorio/Espirometria/espirometria.js') !!}"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
@endsection