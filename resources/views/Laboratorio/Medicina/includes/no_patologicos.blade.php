<fieldset>
  <div id="noPatologico">
    <hr>
    <p class="text-center">Vacunas Recibidas</p>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label>Tetanos: </label>
          @if (isset($historial) && $historial['noPatologico']['vacunas']['tetano'][0]!='No')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="tetano[]" value="Tetano" id="tetano_si">
            <label class="custom-control-label" for="tetano_si" onclick="showControll('tetano','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="tetano[]" value="No" id="tetano_no">
            <label class="custom-control-label" for="tetano_no" onclick="hideControll('tetano','show')">no</label>
          </div>
          <div class="tetano" style="display:none">
            <input type="text" class="form-control tetano" name="tetano[]" placeholder="Nota" value="{{$historial['noPatologico']['vacunas']['tetano'][1]}}">
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="tetano[]" value="Tetano" id="tetano_si">
            <label class="custom-control-label" for="tetano_si" onclick="showControll('tetano','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="tetano[]" value="No" id="tetano_no">
            <label class="custom-control-label" for="tetano_no" onclick="hideControll('tetano','show')">no</label>
          </div>
          <div class="tetano" style="display:none">
            <input type="text" class="form-control tetano" name="tetano[]" placeholder="Nota">
          </div>
          @endif
        </div>

      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Rubeloa: </label>
          @if (isset($historial) && $historial['noPatologico']['vacunas']['rubeola'][0]!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="rubeola[]" value="Rubeola" id="rubeola_si">
            <label class="custom-control-label" for="rubeola_si" onclick="showControll('rubeola','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="rubeola[]" value="no" id="rubeola_no">
            <label class="custom-control-label" for="rubeola_no" onclick="hideControll('rubeola','show')">no</label>
          </div>
          <div class="rubeola" style="display:none">
            <input type="text" class="form-control rubeola" name="rubeola[]" placeholder="Nota" value="{{$historial['noPatologico']['vacunas']['rubeola'][1]}}">
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="rubeola[]" value="Rubeola" id="rubeola_si">
            <label class="custom-control-label" for="rubeola_si" onclick="showControll('rubeola','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="rubeola[]" value="no" id="rubeola_no">
            <label class="custom-control-label" for="rubeola_no" onclick="hideControll('rubeola','show')">no</label>
          </div>
          <div class="rubeola" style="display:none">
            <input type="text" class="form-control rubeola" name="rubeola[]" placeholder="Nota">
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>(BCG): </label>
          @if (isset($historial) && $historial['noPatologico']['vacunas']['bcg'][0]!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="bcg[]" value="(BCG)" id="bcg_si">
            <label class="custom-control-label" for="bcg_si" onclick="showControll('bcg','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="bcg[]" value="no" id="bcg_no">
            <label class="custom-control-label" for="bcg_no" onclick="hideControll('bcg','show')">no</label>
          </div>
          <div class="bcg" style="display:none">
            <input type="text" class="form-control bcg" name="bcg[]" placeholder="Nota" value="{{$historial['noPatologico']['vacunas']['bcg'][1]}}">
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="bcg[]" value="(BCG)" id="bcg_si">
            <label class="custom-control-label" for="bcg_si" onclick="showControll('bcg','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="bcg[]" value="no" id="bcg_no">
            <label class="custom-control-label" for="bcg_no" onclick="hideControll('bcg','show')">no</label>
          </div>
          <div class="bcg" style="display:none">
            <input type="text" class="form-control bcg" name="bcg[]" placeholder="Nota">
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Hepatitis: </label>
          @if (isset($historial) && $historial['noPatologico']['vacunas']['hepatitis'][0]!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="hepatitis[]" value="Hepatitis" id="hepatitis_si">
            <label class="custom-control-label" for="hepatitis_si" onclick="showControll('hepatitis','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="hepatitis[]" value="no" id="hepatitis_no">
            <label class="custom-control-label" for="hepatitis_no" onclick="hideControll('hepatitis','show')">no</label>
          </div>
          <div class="hepatitis" style="display:none">
            <input type="text" class="form-control hepatitis" name="hepatitis[]" placeholder="Nota" value="{{$historial['noPatologico']['vacunas']['hepatitis'][1]}}">
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="hepatitis[]" value="Hepatitis" id="hepatitis_si">
            <label class="custom-control-label" for="hepatitis_si" onclick="showControll('hepatitis','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="hepatitis[]" value="no" id="hepatitis_no">
            <label class="custom-control-label" for="hepatitis_no" onclick="hideControll('hepatitis','show')">no</label>
          </div>
          <div class="hepatitis" style="display:none">
            <input type="text" class="form-control hepatitis" name="hepatitis[]" placeholder="Nota">
          </div>
          @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Influenza: </label>
          @if (isset($historial) && $historial['noPatologico']['vacunas']['influenza'][0]!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="influenza[]" value="Influenza" id="influenza_si">
            <label class="custom-control-label" for="influenza_si" onclick="showControll('influenza','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="influenza[]" value="no" id="influenza_no">
            <label class="custom-control-label" for="influenza_no" onclick="hideControll('influenza','show')">no</label>
          </div>
          <div class="influenza" style="display:none">
            <input type="text" class="form-control influenza" name="influenza[]" placeholder="Nota" value="{{$historial['noPatologico']['vacunas']['influenza'][1]}}">
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="influenza[]" value="Influenza" id="influenza_si">
            <label class="custom-control-label" for="influenza_si" onclick="showControll('influenza','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="influenza[]" value="no" id="influenza_no">
            <label class="custom-control-label" for="influenza_no" onclick="hideControll('influenza','show')">no</label>
          </div>
          <div class="influenza" style="display:none">
            <input type="text" class="form-control influenza" name="influenza[]" placeholder="Nota">
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Neumococica: </label>
          @if (isset($historial) && $historial['noPatologico']['vacunas']['neumococica'][0]!='no')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="neumococica[]" value="Neumococica" id="neumococica_si">
            <label class="custom-control-label" for="neumococica_si" onclick="showControll('neumococica','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="neumococica[]" value="no" id="neumococica_no">
            <label class="custom-control-label" for="neumococica_no" onclick="hideControll('neumococica','show')">no</label>
          </div>
          <div class="neumococica" style="display:none">
            <input type="text" class="form-control neumococica" name="neumococica[]" placeholder="Nota" value="{{$historial['noPatologico']['vacunas']['neumococica'][1]}}">
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="neumococica[]" value="Neumococica" id="neumococica_si">
            <label class="custom-control-label" for="neumococica_si" onclick="showControll('neumococica','show')">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="neumococica[]" value="no" id="neumococica_no">
            <label class="custom-control-label" for="neumococica_no" onclick="hideControll('neumococica','show')">no</label>
          </div>
          <div class="neumococica" style="display:none">
            <input type="text" class="form-control neumococica" name="neumococica[]" placeholder="Nota">
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        @if (isset($historial) && !empty ($historial['noPatologico']['vacunas']['otros']))
        <div class="form-group">
          <label for="otrasvacunas">Otras Vacunas :</label>
          <input type="text" class="form-control" name="otrasvacunas" id="otrasvacunas" value="{{$historial['noPatologico']['vacunas']['otros']}}">
        </div>
        @else
        <div class="form-group">
          <label for="otrasvacunas">Otras Vacunas :</label>
          <input type="text" class="form-control" name="otrasvacunas" id="otrasvacunas">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        @if (isset($historial) && !empty($historial['noPatologico']['g_sanguineo']))
        <div class="form-group">
          <label for="tiposangre">Grupo Sanguinio :</label>
          <input type="text" class="form-control" name="tiposangre" id="tiposangre" value="{{$historial['noPatologico']['g_sanguineo']}}">
        </div>
        @else
        <div class="form-group">
          <label for="tiposangre">Grupo Sanguinio :</label>
          <input type="text" class="form-control" name="tiposangre" id="tiposangre">
        </div>
        @endif
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="d-block">¿Alguna vez ha recibido usted alguna transfusión sanguinea? </label>
          @if (isset($historial) && $historial['noPatologico']['transfusion']!='no')
          <div class="d-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="transfusion" value="si" id="transfusion_si">
            <label class="custom-control-label" for="transfusion_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="transfusion" value="no" id="transfusion_no">
            <label class="custom-control-label" for="transfusion_no">no</label>
          </div>
          @else
          <div class="d-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondarys" name="transfusion" value="si" id="transfusion_si">
            <label class="custom-control-label" for="transfusion_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="transfusion" value="no" id="transfusion_no">
            <label class="custom-control-label" for="transfusion_no">no</label>
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="medicamentos_ingeridos">Mencione los medicamentos que haya tomado en las últimas dos semanas</label>
          @if (isset($historial) && !empty($historial['noPatologico']['medica_ingeridos']))
          <input type="text" class="form-control" id="medicamentos_ingeridos" name="mediIngeridos" placeholder="" value="{{$historial['noPatologico']['medica_ingeridos']}}">
          @else
          <input type="text" class="form-control" id="medicamentos_ingeridos" name="mediIngeridos" placeholder="">

          @endif
        </fieldset>
      </div>
    </div>
   
    
  </div>
  <div class="row" id="no_patologicos_reg0001">
    <label class="col-md-12">Su casa cuenta con:</label>
    <div class="col-md-3">
        <label class="d-block">Piso de cemento</label>
        @if (isset($historial) && $historial['noPatologico']['piso_cemento']=='si')
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked
                name="piso_cemento" value="si" id="piso_cemento_si">
            <label class="custom-control-label" for="piso_cemento_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="piso_cemento"
                value="no" id="piso_cemento_no">
            <label class="custom-control-label" for="piso_cemento_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" 
              name="piso_cemento" value="si" id="piso_cemento_si">
          <label class="custom-control-label" for="piso_cemento_si">si</label>
      </div>
      <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="piso_cemento" checked
              value="no" id="piso_cemento_no">
          <label class="custom-control-label" for="piso_cemento_no">no</label>
      </div>
        @endif
    </div>
    <div class="col-md-3">
        <label class="d-block">Drenaje</label>
        @if (isset($historial) && $historial['noPatologico']['drenaje']=='si')
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked
                name="drenaje" value="si" id="drenaje_si">
            <label class="custom-control-label" for="drenaje_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="drenaje"
                value="no" id="drenaje_no">
            <label class="custom-control-label" for="drenaje_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" 
              name="drenaje" value="si" id="drenaje_si">
          <label class="custom-control-label" for="drenaje_si">si</label>
      </div>
      <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="drenaje" checked
              value="no" id="drenaje_no">
          <label class="custom-control-label" for="drenaje_no">no</label>
      </div>
        @endif
    </div>
    <div class="col-md-3">
        <label class="d-block">Agua potable</label>
        @if (isset($historial) && $historial['noPatologico']['agua_potable']=='si')
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked
                name="agua_potable" value="si" id="agua_potable_si">
            <label class="custom-control-label" for="agua_potable_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="agua_potable"
                value="no" id="agua_potable_no">
            <label class="custom-control-label" for="agua_potable_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" 
              name="agua_potable" value="si" id="agua_potable_si">
          <label class="custom-control-label" for="agua_potable_si">si</label>
      </div>
      <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="agua_potable" checked
              value="no" id="agua_potable_no">
          <label class="custom-control-label" for="agua_potable_no">no</label>
      </div>
        @endif
    </div>
    <div class="col-md-3">
        <label class="d-block">Luz eléctrica</label>
        @if (isset($historial) && $historial['noPatologico']['luz']=='si')
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked
                name="luz" value="si" id="luz_si">
            <label class="custom-control-label" for="agua_potable_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="luz"
                value="no" id="luz_no">
            <label class="custom-control-label" for="luz_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" 
              name="luz" value="si" id="luz_si">
          <label class="custom-control-label" for="agua_potable_si">si</label>
      </div>
      <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="luz" checked
              value="no" id="luz_no">
          <label class="custom-control-label" for="luz_no">no</label>
      </div>
        @endif
    </div>
    <div class="col-md-3 mt-1">
            <label for="n_habitantes">Número de habitantes:</label>
            <input type="number" class="form-control" name="n_habitantes" id="n_habitantes" value="{{$historial['noPatologico']['n_habitantes']}}">
    </div>
    <div class="col-md-3 mt-1">
        <label for="n_habitaciones">Número de habitaciones:</label>
        <input type="number" class="form-control" name="n_habitaciones" id="n_habitaciones" value="{{$historial['noPatologico']['n_habitaciones']}}">
    </div>
    <div class="col-md-3 mt-1">
        <label for="mascotas">Número de mascota:</label>
        <input type="number" class="form-control" name="n_mascotas" id="n_mascotas" value="{{$historial['noPatologico']['n_mascotas']}}">
    </div>
    <div class="col-md-3 mt-1">
        <label for="mascotas">¿Cuáles mascotas?:</label>
        <input type="text" class="form-control" name="mascotas" id="mascotas" value="{{$historial['noPatologico']['mascotas']}}" placeholder="Gatos,Perros...">
    </div>
    <h5 class="text-center mt-1 col-md-12">Hábitos higiénico - dietéticos</h5>
    <div class="col-md-6 mt-1">
        <label for="h_higienicos">Baño:</label>
        <input type="text" class="form-control" name="h_higienicos" id="h_higienicos" value="{{$historial['noPatologico']['h_higienicos']}}" placeholder="3 veces por semana">
    </div>
    <div class="col-md-6 mt-1">
        <label for="lavado_dientes">Lavado de dientes:</label>
        <input type="text" class="form-control" name="lavado_dientes" id="lavado_dientes" value="{{$historial['noPatologico']['lavado_dientes']}}" placeholder="1 vez por dia">
    </div>
    <div class="col-md-6 mt-1">
        <label for="alimentacion_diaria">Alimentación diaria:</label>
        <input type="text" class="form-control" name="alimentacion_diaria" id="alimentacion_diaria" value="{{$historial['noPatologico']['alimentacion_diaria']}}" placeholder="3 veces por dia">
    </div>
    
    
</div>
  <p class="text-center">Uso del Cigarro</p>
  <hr>
  <div class="row">
    <div class="col-md-3">
      <fieldset class="form-group">
        <label class="d-block">¿Fuma Ustes? </label>
        @if (isset($historial) && $historial['noPatologico']['cigarro']['Cigarro']!='No')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="cigarro" checked onclick="showControll('cigarro','disabled')" value="si" id="cigarro_si">
          <label class="custom-control-label" for="cigarro_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="cigarro" onclick="hideControll('cigarro','disabled')" value="no" id="cigarro_no">
          <label class="custom-control-label" for="cigarro_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="cigarro" onclick="showControll('cigarro','disabled')" value="si" id="cigarro_si">
          <label class="custom-control-label" for="cigarro_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="cigarro" checked onclick="hideControll('cigarro','disabled')" value="no" id="cigarro_no">
          <label class="custom-control-label" for="cigarro_no">no</label>
        </div>
        @endif
      </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label for="cigarro_edad">¿Desde que edad?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['cigarro']['edad']))
        <input type="text" class="form-control cigarro" disabled id="cigarro_edad" name="cigarro_edad" placeholder="" value="{{$historial['noPatologico']['cigarro']['edad']}}">
        @else
        <input type="text" class="form-control cigarro" disabled id="cigarro_edad" name="cigarro_edad" placeholder="">

        @endif
      </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label for="cigarropromedio">¿Número promedio de cigarro que fuma?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['cigarro']['frecuencia']))
        <input type="text" class="form-control cigarro" disabled id="cigarropromedio" name="cigarropromedio" placeholder="" value="{{$historial['noPatologico']['cigarro']['frecuencia']}}">
        @else
        <input type="text" class="form-control cigarro" disabled id="cigarropromedio" name="cigarropromedio" placeholder="">

        @endif
      </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label for="dejarcigarro">¿A que edad dejo de fumar?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['cigarro']['noConsumir']))
        <input type="text" class="form-control cigarro" disabled id="dejarcigarro" name="dejarcigarro" placeholder="" value="{{$historial['noPatologico']['cigarro']['noConsumir']}}">
        @else
        <input type="text" class="form-control cigarro" disabled id="dejarcigarro" name="dejarcigarro" placeholder="">
        @endif
      </fieldset>
    </div>
  </div>
  <p class="text-center">Uso de Alcohol</p>
  <hr>
  <div class="row">
    <div class="col-md-3">
      <fieldset class="form-group">
        <label class="d-block">¿Toma bebidas alcoholicas?</label>
        @if (isset($historial) && $historial['noPatologico']['alcohol']['Alcohol']!='No')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="alcohol" value="si" id="alcohol_si">
          <label class="custom-control-label" for="alcohol_si" onclick="showControll('alcohol','disabled')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="alcohol" value="no" id="alcohol_no">
          <label class="custom-control-label" for="alcohol_no" onclick="hideControll('alcohol','disabled')">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="alcohol" value="si" id="alcohol_si">
          <label class="custom-control-label" for="alcohol_si" onclick="showControll('alcohol','disabled')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="alcohol" value="no" id="alcohol_no">
          <label class="custom-control-label" for="alcohol_no" onclick="hideControll('alcohol','disabled')">no</label>
        </div>
        @endif
      </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label for="alcohol_edad">¿Desde que edad?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['alcohol']['edad']))
        <input type="text" class="form-control alcohol" disabled id="alcohol_edad" name="alcohol_edad" placeholder="" value="{{$historial['noPatologico']['alcohol']['edad']}}">
        @else
        <input type="text" class="form-control alcohol" disabled id="alcohol_edad" name="alcohol_edad" placeholder="">
        @endif
      </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="alcohol_frecuencia">¿Con que frecuencia y cantidad?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['alcohol']['frecuencia']))
        <input type="text" class="form-control alcohol" disabled id="alcohol_frecuencia" name="alcohol_frecuencia" placeholder="" value="{{$historial['noPatologico']['alcohol']['frecuencia']}}">
        @else
        <input type="text" class="form-control alcohol" disabled id="alcohol_frecuencia" name="alcohol_frecuencia" placeholder="">
        @endif
      </fieldset>
    </div>
  </div>
  <p class="text-center">Uso de Drogas</p>
  <hr>
  <div class="row">
    <div class="col-md-3">
      <fieldset class="form-group">
        <label class="d-block">¿Alguna vez usó drogas?</label>
        @if (isset($historial) && $historial['noPatologico']['drogas']['Drogas']!='No')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="drogas" value="si" id="dogras_si">
          <label class="custom-control-label" for="dogras_si" onclick="showControll('drogas','disabled')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="drogas" value="no" id="dogras_no">
          <label class="custom-control-label" for="dogras_no" onclick="hideControll('drogas','disabled')">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="drogas" value="si" id="dogras_si">
          <label class="custom-control-label" for="dogras_si" onclick="showControll('drogas','disabled')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="drogas" value="no" id="dogras_no">
          <label class="custom-control-label" for="dogras_no" onclick="hideControll('drogas','disabled')">no</label>
        </div>
        @endif
      </fieldset>
    </div>
    <div class="col-md-3">
      <fieldset class="form-group">
        <label for="alcohol_edad">¿Desde que edad?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['drogas']['edad']))
        <input type="text" class="form-control drogas" disabled id="drogas_edad" name="drogas_edad" placeholder="" value="{{$historial['noPatologico']['drogas']['edad']}}">
        @else
        <input type="text" class="form-control drogas" disabled id="drogas_edad" name="drogas_edad" placeholder="">
        @endif
      </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label for="alcohol_frecuencia">¿Con que frecuencia y cantidad?</label>
        @if (isset($historial) && !empty($historial['noPatologico']['drogas']['frecuencia']))
        <input type="text" class="form-control drogas" disabled id="drogas_frecuencia" name="drogas_frecuencia" placeholder="" value="{{$historial['noPatologico']['drogas']['frecuencia']}}">
        @else
        <input type="text" class="form-control drogas" disabled id="drogas_frecuencia" name="drogas_frecuencia" placeholder="">
        @endif
      </fieldset>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group drogas">
        <label>Mencione que drogas ha usado</label>
        <select class="selectize-multiple" class=" form-control" name="drogas_usadas[]" placeholder="Drogas" multiple>
          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Resistol', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Resistol">Resistol</option>
          @else
          <option value="Resistol">Resistol</option>
          @endif

          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Sarolo', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Sarolo">Sarolo</option>
          @else
          <option value="Sarolo">Sarolo</option>
          @endif

          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Marihuana', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Marihuana">Marihuana</option>
          @else
          <option value="Marihuana">Marihuana</option>
          @endif

          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Cocaina', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Cocaina">Cocaina</option>
          @else
          <option value="Cocaina">Cocaina</option>
          @endif

          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Pastillas', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Pastillas">Pastillas</option>
          @else
          <option value="Pastillas">Pastillas</option>
          @endif

          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Thinner', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Thinner">Thinner</option>
          @else
          <option value="Thinner">Thinner</option>
          @endif

          @if (isset($historial) && !empty($historial['noPatologico']['drogas_usadas']) && in_array('Otras', $historial['noPatologico']['drogas_usadas']))
          <option selected value="Otras">Otras</option>
          @else
          <option value="Otras">Otras</option>
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group" id="noPatologicodieta">
        <label class="d-block">¿Se le ha indicado alguna dieta?</label>
        @if (isset($historial) && $historial['noPatologico']['dieta']['Dieta']!='No')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="dieta" value="si" id="dieta_si">
          <label class="custom-control-label" for="dieta_si" onclick="showControll('dieta','show')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="dieta" value="no" id="dieta_no">
          <label class="custom-control-label" for="dieta_no" onclick="hideControll('dieta','show')">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="dieta" value="si" id="dieta_si">
          <label class="custom-control-label" for="dieta_si" onclick="showControll('dieta','show')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="dieta" value="no" id="dieta_no">
          <label class="custom-control-label" for="dieta_no" onclick="hideControll('dieta','show')">no</label>
        </div>
        @endif
      </div>
      <div class="form-group dieta" style="display:none">
        @if (isset($historial) && !empty($historial['noPatologico']['dieta']['descripcion']))
        <input type="text" class="form-control dieta" name="dieta_descripcion" placeholder="Descripción" value="{{$historial['noPatologico']['dieta']['descripcion']}}">
        @else
        <input type="text" class="form-control dieta" name="dieta_descripcion" placeholder="Descripción">

        @endif
      </div>
    </div>
    <div class="col-md-3" id="noPatologicodeporte">
      <div class="form-group">
        <label class="d-block">¿Practica algun deporte?</label>
        @if (isset($historial) && $historial['noPatologico']['deporte']['Deporte']!='No')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="deporte" value="si" id="deporte_si">
          <label class="custom-control-label" for="deporte_si" onclick="showControll('deporte_frecuencia','show')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="deporte" value="no" id="deporte_no">
          <label class="custom-control-label" for="deporte_no" onclick="hideControll('deporte_frecuencia','show')">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="deporte" value="si" id="deporte_si">
          <label class="custom-control-label" for="deporte_si" onclick="showControll('deporte_frecuencia','show')">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="deporte" value="no" id="deporte_no">
          <label class="custom-control-label" for="deporte_no" onclick="hideControll('deporte_frecuencia','show')">no</label>
        </div>
        @endif
      </div>
      <div class="form-group deporte_frecuencia" style="display:none">
        @if (isset($historial) && !empty($historial['noPatologico']['deporte']['frecuencia']))
        <input type="text" class="form-control deporte_frecuencia" name="deporte_frecuencia" placeholder="Especifique cual y frecuencia" value="{{$historial['noPatologico']['deporte']['frecuencia']}}">
        @else
        <input type="text" class="form-control deporte_frecuencia" name="deporte_frecuencia" placeholder="Especifique cual y frecuencia">

        @endif
      </div>

    </div>
      <div class="col-md-12" id="noPatologicopasa">
        <fieldset class="form-group">
          <label for="pasatiempo">Indique cuál es su pasatiempo favorito:</label>
          @if (isset($historial) && !empty($historial['noPatologico']['pasatiempo']))
          <input type="text" class="form-control" name="pasatiempo" id="pasatiempo" placeholder="" value="{{$historial['noPatologico']['pasatiempo']}}">
          @else
          <input type="text" class="form-control" name="pasatiempo" id="pasatiempo" placeholder="">
          @endif
        </fieldset>
      </div>
  </div>
  <div class="row" id="patologicoshc">
    <fieldset class="form-group col-md-6">
      <label for="tiposangre">Tipo de Sangre:</label>
      @if (isset($historial) && !empty($historial['noPatologico']['tiposangre']))
      <input type="text" class="form-control" name="tiposangre" id="tiposangre" placeholder="" value="{{$historial['noPatologico']['tiposangre']}}">
      @else
      <input type="text" class="form-control" name="tiposangre" id="tiposangre" placeholder="">
      @endif
    </fieldset>
    <fieldset class="form-group col-md-6">
      <label for="tatuajes">Tatuajes:</label>
      @if (isset($historial) && !empty($historial['noPatologico']['tatuajes']))
      <input type="text" class="form-control" name="tatuajes" id="tatuajes" placeholder="" value="{{$historial['noPatologico']['tatuajes']}}">
      @else
      <input type="text" class="form-control" name="tatuajes" id="tatuajes" placeholder="">
      @endif
    </fieldset>
  </div>
  </fieldset>

<script>
  TetanosSelecionado();

  function TetanosSelecionado() {
    if (document.getElementById('tetano_si').checked) {
      showControll('tetano', 'show');
    }
    if (document.getElementById('rubeola_si').checked) {
      showControll('rubeola', 'show');
    }
    if (document.getElementById('bcg_si').checked) {
      showControll('bcg', 'show');
    }
    if (document.getElementById('hepatitis_si').checked) {
      showControll('hepatitis', 'show');
    }
    if (document.getElementById('influenza_si').checked) {
      showControll('influenza', 'show');
    }
    if (document.getElementById('neumococica_si').checked) {
      showControll('neumococica', 'show');
    }
    if (document.getElementById('cigarro_si').checked) {
      showControll('cigarro', 'disabled');
    }
    if (document.getElementById('alcohol_si').checked) {
      showControll('alcohol', 'disabled')
    }
    if (document.getElementById('dogras_si').checked) {
      showControll('drogas', 'disabled')
    }
    if (document.getElementById('dieta_si').checked) {
      showControll('dieta', 'show')
    }
    if (document.getElementById('deporte_si').checked) {
      showControll('deporte_frecuencia', 'show')
    }

  }
</script>
