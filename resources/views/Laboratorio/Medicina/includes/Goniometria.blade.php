<fieldset>
    <div class="col-md-12">
        <h5 class="text-center secondary">MIEMBRO SUPERIOR</h5>
    </div>
    <hr>
    <div class="col-md-12">
        <h5 class="text-center secondary">Hombro</h5>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="secondary">Izquierdo:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_flexion_i" placeholder="Valores naturales(rom)" name="s_h_flexion_i" value="{{ $fisioterapia['s_h_flexion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_extension_i" placeholder="Valores naturales(rom)" name="s_h_extension_i" value="{{ $fisioterapia['s_h_extension_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Abducción</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_abduccion_i" placeholder="Valores naturales(rom)" name="s_h_abduccion_i" value="{{ $fisioterapia['s_h_abduccion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. Externa </p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_r_externa_i" placeholder="Valores naturales(rom)" name="s_h_r_externa_i" value="{{ $fisioterapia['s_h_r_externa_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. interna </p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_r_interna_i" placeholder="Valores naturales(rom)" name="s_h_r_interna_i" value="{{ $fisioterapia['s_h_r_interna_i'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="secondary">Derecho:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_flexion_d" placeholder="Valores naturales(rom)" name="s_h_flexion_d" value="{{ $fisioterapia['s_h_flexion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_extension_d" placeholder="Valores naturales(rom)" name="s_h_extension_d" value="{{ $fisioterapia['s_h_extension_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Abducción</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_abduccion_d" placeholder="Valores naturales(rom)" name="s_h_abduccion_d" value="{{ $fisioterapia['s_h_abduccion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. Externa </p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_r_externa_d" placeholder="Valores naturales(rom)" name="s_h_r_externa_d" value="{{ $fisioterapia['s_h_r_externa_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. interna </p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_h_r_interna_d" placeholder="Valores naturales(rom)" name="s_h_r_interna_d" value="{{ $fisioterapia['s_h_r_interna_d'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center secondary">Codo</h5>
        </div>
        <div class="col-md-12">
            <h6 class="secondary">Izquierdo:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_c_flexion_i" placeholder="Valores naturales(rom)" name="s_c_flexion_i" value="{{ $fisioterapia['s_c_flexion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Pronación</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_c_pronacion_i" placeholder="Valores naturales(rom)" name="s_c_pronacion_i" value="{{ $fisioterapia['s_c_pronacion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Supinación</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_c_supinacion_i" placeholder="Valores naturales(rom)" name="s_c_supinacion_i" value="{{ $fisioterapia['s_c_supinacion_i'] }}">
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <h6 class="secondary">Derecho:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_c_flexion_d" placeholder="Valores naturales(rom)" name="s_c_flexion_d" value="{{ $fisioterapia['s_c_flexion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Pronación</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_c_pronacion_d" placeholder="Valores naturales(rom)" name="s_c_pronacion_d" value="{{ $fisioterapia['s_c_pronacion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Supinación</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_c_supinacion_d" placeholder="Supinación" name="s_c_supinacion_d" value="{{ $fisioterapia['s_c_supinacion_d'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center secondary">Muñeca </h5>
        </div>
        <div class="col-md-12">
            <h6 class="secondary">Izquierda:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_flexion_i" placeholder="Valores naturales(rom)" name="s_m_flexion_i" value="{{ $fisioterapia['s_m_flexion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_extension_i" placeholder="Valores naturales(rom)" name="s_m_extension_i" value="{{ $fisioterapia['s_m_extension_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Desv radial</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_des_radial_i" placeholder="Valores naturales(rom)" name="s_m_des_radial_i" value="{{ $fisioterapia['s_m_des_radial_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Desv cubital</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_des_cubital_i" placeholder="Valores naturales(rom)" name="s_m_des_cubital_i" value="{{ $fisioterapia['s_m_des_cubital_i'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="secondary">Derecha:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_flexion_d" placeholder="Valores naturales(rom)" name="s_m_flexion_d" value="{{ $fisioterapia['s_m_flexion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_extension_d" placeholder="Valores naturales(rom)" name="s_m_extension_d" value="{{ $fisioterapia['s_m_extension_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Desv radial</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_des_radial_d" placeholder="Valores naturales(rom)" name="s_m_des_radial_d" value="{{ $fisioterapia['s_m_des_radial_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Desv cubital</p>
            <div class="form-group">
                <input type="text" class="form-control" id="s_m_des_cubital_d" placeholder="Valores naturales(rom)" name="s_m_des_cubital_d" value="{{ $fisioterapia['s_m_des_cubital_d'] }}">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <h5 class="text-center secondary">MIEMBRO INFERIOR</h5>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center secondary">Cadera</h5>
        </div>
        <div class="col-md-12">
            <h5 class="secondary">Izquierda:</h5>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_flexion_i" placeholder="Valores naturales(rom)" name="i_ca_flexion_i" value="{{ $fisioterapia['i_ca_flexion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_extension_i" placeholder="Valores naturales(rom)" name="i_ca_extension_i" value="{{ $fisioterapia['i_ca_extension_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Abducción</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_abduccion_i" placeholder="Valores naturales(rom)" name="i_ca_abduccion_i" value="{{ $fisioterapia['i_ca_abduccion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Aducción</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_aduccion_i" placeholder="Valores naturales(rom)" name="i_ca_aduccion_i" value="{{ $fisioterapia['i_ca_aduccion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. Externa </p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_r_externa_i" placeholder="Valores naturales(rom)" name="i_ca_r_externa_i" value="{{ $fisioterapia['i_ca_r_externa_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. interna </p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_r_interna_i" placeholder="Valores naturales(rom)" name="i_ca_r_interna_i" value="{{ $fisioterapia['i_ca_r_interna_i'] }}">
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <h5 class="secondary">Derecha:</h5>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_flexion_d" placeholder="Valores naturales(rom)" name="i_ca_flexion_d" value="{{ $fisioterapia['i_ca_flexion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_extension_d" placeholder="Valores naturales(rom)" name="i_ca_extension_d" value="{{ $fisioterapia['i_ca_extension_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Abducción</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_abduccion_d" placeholder="Valores naturales(rom)" name="i_ca_abduccion_d" value="{{ $fisioterapia['i_ca_abduccion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Aducción</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_aduccion_d" placeholder="Valores naturales(rom)" name="i_ca_aduccion_d" value="{{ $fisioterapia['i_ca_aduccion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. Externa </p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_r_externa_d" placeholder="Valores naturales(rom)" name="i_ca_r_externa_d" value="{{ $fisioterapia['i_ca_r_externa_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Rot. interna </p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_ca_r_interna_d" placeholder="Valores naturales(rom)" name="i_ca_r_interna_d" value="{{ $fisioterapia['i_ca_r_interna_d'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center secondary">Rodilla</h5>
        </div>
        <div class="col-md-12">
            <h6 class="secondary">Izquierda:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_r_flexion_i" placeholder="Valores naturales(rom)" name="i_r_flexion_i" value="{{ $fisioterapia['i_r_flexion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_r_extension_i" placeholder="Valores naturales(rom)" name="i_r_extension_i" value="{{ $fisioterapia['i_r_extension_i'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="secondary">Derecha:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_r_flexion_d" placeholder="Valores naturales(rom)" name="i_r_flexion_d" value="{{ $fisioterapia['i_r_flexion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_r_extension_d" placeholder="Valores naturales(rom)" name="i_r_extension_d" value="{{ $fisioterapia['i_r_extension_d'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center secondary">Tobillo</h5>
        </div>
        <div class="col-md-12">
            <h6 class="secondary">Izquierda:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_flexion_i" placeholder="Valores naturales(rom)" name="i_t_flexion_i" value="{{ $fisioterapia['i_t_flexion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_extension_i" placeholder="Valores naturales(rom)" name="i_t_extension_i" value="{{ $fisioterapia['i_t_extension_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Inversión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_inversion_i" placeholder="Valores naturales(rom)" name="i_t_inversion_i" value="{{ $fisioterapia['i_t_inversion_i'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Eversión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_eversion_i" placeholder="Valores naturales(rom)" name="i_t_eversion_i" value="{{ $fisioterapia['i_t_eversion_i'] }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="secondary">Derecha:</h6>
        </div>
        <div class="col-md-3">
            <p>Flexión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_flexion_d" placeholder="Valores naturales(rom)" name="i_t_flexion_d" value="{{ $fisioterapia['i_t_flexion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Extensión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_extension_d" placeholder="Valores naturales(rom)" name="i_t_extension_d" value="{{ $fisioterapia['i_t_extension_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Inversión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_inversion_d" placeholder="Valores naturales(rom)" name="i_t_inversion_d" value="{{ $fisioterapia['i_t_inversion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Eversión</p>
            <div class="form-group">
                <input type="text" class="form-control" id="i_t_eversion_d" placeholder="Valores naturales(rom)" name="i_t_eversion_d" value="{{ $fisioterapia['i_t_eversion_d'] }}">
            </div>
        </div>
        <div class="col-md-3">
            <p>Numero de revisión:</p>
            <div class="form-group">
                <input type="text" class="form-control" id="num_revision" placeholder="" name="num_revision" value="{{ $fisioterapia['num_revision'] }}">
            </div>
        </div>
        <div class="col-md-8">
            <p>RESUMEN VALORACIÓN:</p>
            <div class="form-group">
                <textarea type="text" class="form-control" name="resumen_valoracion">{{ $fisioterapia['resumen_valoracion'] }}</textarea>

            </div>
        </div>
    </div>
</fieldset>
