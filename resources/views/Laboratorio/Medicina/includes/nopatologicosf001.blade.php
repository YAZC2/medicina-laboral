<fieldset>
   
    <div class="row" id="no_patologicos_reg0001">
        <label>Su casa cuenta con:</label>
        <div class="col-md-3">
            <label class="d-block">Piso de cemento</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="piso_cemento" value="si" id="piso_cemento_si">
                <label class="custom-control-label" for="piso_cemento_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="piso_cemento"
                    value="no" id="piso_cemento_no">
                <label class="custom-control-label" for="piso_cemento_no">no</label>
            </div>
        </div>
        <div class="col-md-3">
            <label class="d-block">Drenaje</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="drenaje" value="si" id="drenaje_si">
                <label class="custom-control-label" for="drenaje_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="drenaje"
                    value="no" id="drenaje_no">
                <label class="custom-control-label" for="drenaje_no">no</label>
            </div>
        </div>
        <div class="col-md-3">
            <label class="d-block">Agua potable</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="agua_potable" value="si" id="agua_potable_si">
                <label class="custom-control-label" for="agua_potable_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="agua_potable"
                    value="no" id="agua_potable_no">
                <label class="custom-control-label" for="agua_potable_no">no</label>
            </div>
        </div>
        <div class="col-md-3">
            <label class="d-block">Luz eléctrica</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="luz" value="si" id="luz_si">
                <label class="custom-control-label" for="agua_potable_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="luz"
                    value="no" id="luz_no">
                <label class="custom-control-label" for="luz_no">no</label>
            </div>
        </div>
        <div class="col-md-3 mt-1">
                <label for="n_habitantes">Número de habitantes:</label>
                <input type="number" class="form-control" name="n_habitantes" id="n_habitantes" value="">
        </div>
        <div class="col-md-3 mt-1">
            <label for="n_habitaciones">Número de habitaciones:</label>
            <input type="number" class="form-control" name="n_habitaciones" id="n_habitaciones" value="">
        </div>
        <div class="col-md-3 mt-1">
            <label for="mascotas">Número de mascota:</label>
            <input type="number" class="form-control" name="mascotas" id="mascotas" value="">
        </div>
        <div class="col-md-3 mt-1">
            <label for="mascotas">¿Cuáles mascotas?:</label>
            <input type="text" class="form-control" name="mascotas" id="mascotas" value="" placeholder="Gatos,Perros...">
        </div>
        <h5 class="text-center mt-1 col-md-12">Hábitos higiénico - dietéticos</h5>
        <div class="col-md-6 mt-1">
            <label for="h_higienicos">Baño:</label>
            <input type="text" class="form-control" name="h_higienicos" id="h_higienicos" value="" placeholder="">
        </div>
        <div class="col-md-6 mt-1">
            <label for="lavado_dientes">Lavado de dientes:</label>
            <input type="text" class="form-control" name="lavado_dientes" id="lavado_dientes" value="" placeholder="">
        </div>
        <div class="col-md-6 mt-1">
            <label for="alimentacion_diaria">Alimentación diaria:</label>
            <input type="text" class="form-control" name="alimentacion_diaria" id="alimentacion_diaria" value="" placeholder="">
        </div>
        <div class="col-md-6 mt-1">
            <label for="alimentacion_diaria">Alimentación diaria:</label>
            <input type="text" class="form-control" name="alimentacion_diaria" id="alimentacion_diaria" value="" placeholder="">
        </div>
        <div class="col-md-3 mt-1">
            <label class="d-block">Fuma:</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="fuma" value="si" id="fuma_si">
                <label class="custom-control-label" for="fuma_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="fuma"
                    value="no" id="fuma_no">
                <label class="custom-control-label" for="fuma_no">no</label>
            </div>
        </div>
        <div class="col-md-3 mt-1">
            <label class="d-block">¿Consume alcohol?</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="alcohol" value="si" id="alcohol_si">
                <label class="custom-control-label" for="alcohol_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="alcohol"
                    value="no" id="alcohol_no">
                <label class="custom-control-label" for="alcohol_no">no</label>
            </div>
            <br>
            <label for="regularidad_alcohol">¿Con qué regularidad?</label>
            <input type="text" class="form-control" name="regularidad_alcohol" id="regularidad_alcohol" value="" placeholder="">
        </div>

        <div class="col-md-3 mt-1">
            <label class="d-block">¿Consume medicamentos?</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="con_medicamentos" value="si" id="con_medicamentos_si">
                <label class="custom-control-label" for="con_medicamentos_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="con_medicamentos"
                    value="no" id="con_medicamentos_no">
                <label class="custom-control-label" for="con_medicamentos_no">no</label>
            </div>
            <br>
            <label for="medicamentos">¿Cuáles?</label>
            <input type="text" class="form-control" name="medicamentos" id="medicamentos" value="" placeholder="">
        </div>
  
        <div class="col-md-3 mt-1">
            <label class="d-block">¿Consume drogas?</label>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" checked
                    name="con_drogas" value="si" id="con_drogas_si">
                <label class="custom-control-label" for="con_drogas_si">si</label>
            </div>
            <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-secondary" name="con_drogas"
                    value="no" id="con_drogas_no">
                <label class="custom-control-label" for="con_drogas_no">no</label>
            </div>
            <br>
            <label for="drogas">¿Cuáles?</label>
            <input type="text" class="form-control" name="drogas" id="drogas" value="" placeholder="">
        </div>
    </div>
    <br>
    
</fieldset>