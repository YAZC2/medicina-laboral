<fieldset>
    <div id="HLaboral">
        <div class="row">

            <div class="col-md-6">
                <label>
                    Ha realizado unos de los siguientes trabajos:
                </label>
                <select class="selectize-multiple" class="form-control" name="trabajos_realizados[]" placeholder="Trabajos" multiple>

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Granjero',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Granjero">Granjero</option>
                    @else
                    <option value="Granjero">Granjero</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Colocando
                    aisalantes', $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Colocando aisalantes">Colocando aisalantes</option>
                    @else
                    <option value="Colocando aisalantes">Colocando aisalantes</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Textiles',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Textiles">Textiles</option>
                    @else
                    <option value="Textiles">Textiles</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Canteras',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Canteras">Canteras</option>
                    @else
                    <option value="Canteras">Canteras</option>|
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Militares',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Militares">Militares</option>
                    @else
                    <option value="Militares">Militares</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Minas',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Minas">Minas</option>
                    @else
                    <option value="Minas">Minas</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Fundación',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Fundación">Fundación</option>
                    @else
                    <option value="Fundación">Fundación</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) &&
                    in_array('Petroquimicos', $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Petroquimicos">Petroquimicos</option>
                    @else
                    <option value="Petroquimicos">Petroquimicos</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) &&
                    in_array('Construcción', $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Construcción">Construcción</option>
                    @else
                    <option value="Construcción">Construcción</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Mecanicos',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Mecanicos">Mecanicos</option>
                    @else
                    <option value="Mecanicos">Mecanicos</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Leñador',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Leñador">Leñador</option>
                    @else
                    <option value="Leñador">Leñador</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Embarques',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Embarques">Embarques</option>
                    @else
                    <option value="Embarques">Embarques</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Pulidor de
                    arena', $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Pulidor de arena">Pulidor de arena</option>
                    @else
                    <option value="Pulidor de arena">Pulidor de arena</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Pintor',
                    $historial['historialLaboral']['trabajoRealizados']) )
                    <option selected value="Pintor">Pintor</option>
                    @else
                    <option value="Pintor">Pintor</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Manuf. de
                    papel', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Manuf. de papel">Manuf. de papel</option>
                    @else
                    <option value="Manuf. de papel">Manuf. de papel</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) &&
                    in_array('Desengrasador', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Desengrasador">Desengrasador</option>
                    @else
                    <option value="Desengrasador">Desengrasador</option>
                    @endif
                </select>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="otros_trabajos_">Otros Trabajos</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['otrosTrabajos']))
                    <input type="text" class="form-control" id="otros_trabajos_" name="otrosTrabajos" placeholder="" value="{{$historial['historialLaboral']['otrosTrabajos']}}">
                    @else
                    <input type="text" class="form-control" id="otros_trabajos_" name="otrosTrabajos" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div id="hl_f001">
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="otras_exposiciones">Edad de inicio:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['otrasExposiciones']))
                        <input type="number" class="form-control" id="edad_inicio_laboral" name="edad_inicio_laboral" value="{{$historial['historialLaboral']['edad_inicio_laboral']}}">
                        @else
                        <input type="number" class="form-control" id="edad_inicio_laboral" name="edad_inicio_laboral">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="otras_exposiciones">Tiempo en el puesto:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['tiempo_puesto']))
                        <input type="number" class="form-control" id="tiempo_puesto" name="tiempo_puesto" value="{{$historial['historialLaboral']['tiempo_puesto']}}">
                        @else
                        <input type="number" class="form-control" id="tiempo_puesto" name="tiempo_puesto">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="horas_trabajo">Horas de trabajo al día:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['horas_trabajo']))
                        <input type="number" class="form-control" id="horas_trabajo" name="horas_trabajo" value="{{$historial['historialLaboral']['horas_trabajo']}}">
                        @else
                        <input type="number" class="form-control" id="horas_trabajo" name="horas_trabajo">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="grados_estudio">Grado de estudios:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['grados_estudio']))
                        <input type="text" class="form-control" id="grados_estudio" name="grados_estudio" value="{{$historial['historialLaboral']['grados_estudio']}}">
                        @else
                        <input type="text" class="form-control" id="grados_estudio" name="grados_estudio">
                        @endif
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>
                    ¿Se ha expuesto o ha trabajado con alguno de estos materiales?
                </label>
                <select class="selectize-multiple" class="form-control" name="materiales_expuesto[]" placeholder="Materiales" multiple>
                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Berilio',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Berilio">Berilio</option>
                    @else
                    <option value="Berilio">Berilio</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Humo de
                    soldadura', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Humo de soldadura">Humo de soldadura</option>
                    @else
                    <option value="Humo de soldadura">Humo de soldadura</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) &&
                    in_array('Plastico/Hule', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Plastico/Hule">Plastico/Hule</option>
                    @else
                    <option value="Plastico/Hule">Plastico/Hule</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Ruido',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Ruido">Ruido</option>
                    @else
                    <option value="Ruido">Ruido</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Asbesto',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Asbesto">Asbesto</option>
                    @else
                    <option value="Asbesto">Asbesto</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Dioxido de
                    azufre', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Dioxido de azufre">Dioxido de azufre</option>
                    @else
                    <option value="Dioxido de azufre">Dioxido de azufre</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Radiación',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Radiación">Radiación</option>
                    @else
                    <option value="Radiación">Radiación</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Plomo',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Plomo">Plomo</option>
                    @else
                    <option value="Plomo">Plomo</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) &&
                    in_array('Caustico(Acido,Base)', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Caustico(Acido,Base)">Caustico(Acido,Base)</option>
                    @else
                    <option value="Caustico(Acido,Base)">Caustico(Acido,Base)</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Silice',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Silice">Silice</option>
                    @else
                    <option value="Silice">Silice</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Gases de
                    cloro', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Gases de cloro">Gases de cloro</option>
                    @else
                    <option value="Gases de cloro">Gases de cloro</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Traunma
                    fis. Repetido', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Traunma fis. Repetido">Traunma fis. Repetido</option>
                    @else
                    <option value="Traunma fis. Repetido">Traunma fis. Repetido</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Arsenico',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Arsenico">Arsenico</option>
                    @else
                    <option value="Arsenico">Arsenico</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Talco',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Talco">Talco</option>
                    @else
                    <option value="Talco">Talco</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Gases/Humo
                    irritante', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Gases/Humo irritante">Gases/Humo irritante</option>
                    @else
                    <option value="Gases/Humo irritante">Gases/Humo irritante</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Fluoruro',
                    $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Fluoruro">Fluoruro</option>
                    @else
                    <option value="Fluoruro">Fluoruro</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) &&
                    in_array('Insecticidas/Otros', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Insecticidas/Otros">Insecticidas/Otros</option>
                    @else
                    <option value="Insecticidas/Otros">Insecticidas/Otros</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Otros
                    materiales', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Otros materiales">Otros materiales</option>
                    @else
                    <option value="Otros materiales">Otros materiales</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Otros
                    aceites', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Otros aceites">Otros aceites</option>
                    @else
                    <option value="Otros aceites">Otros aceites</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Otros
                    polvos', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Otros polvos">Otros polvos</option>
                    @else
                    <option value="Otros polvos">Otros polvos</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Otros
                    solventes', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Otros solventes">Otros solventes</option>
                    @else
                    <option value="Otros solventes">Otros solventes</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Otras
                    fibras sinteticas', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Otras fibras sinteticas">Otras fibras sinteticas</option>
                    @else
                    <option value="Otras fibras sinteticas">Otras fibras sinteticas</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoMateriales']) && in_array('Otro
                    argon', $historial['historialLaboral']['trabajoMateriales']) )
                    <option selected value="Otro argon">Otro argon</option>
                    @else
                    <option value="Otro argon">Otro argon</option>
                    @endif
                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Vibraciones
                    fuertes', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Desengrasador">Vibraciones fuertes</option>
                    @else
                    <option value="Desengrasador">Vibraciones fuertes</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Temperatura
                    elevada', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Temperatura elevada">Temperatura elevada</option>
                    @else
                    <option value="Temperatura elevada">Temperatura elevada</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Temperatura
                    abatida', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Temperatura abatida">Temperatura abatida</option>
                    @else
                    <option value="Temperatura abatida">Temperatura abatida</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Humedad
                    Elevada', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Humedad Elevada">Humedad elevada</option>
                    @else
                    <option value="Humedad Elevada">Temperatura abatida</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Olores
                    Desagradables', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Olores Desagradables">Olores desagradables</option>
                    @else
                    <option value="Olores Desagradables">Olores desagradables</option>
                    @endif

                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Iluminación
                    insuficiente', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Iluminación insuficiente">Iluminación insuficiente</option>
                    @else
                    <option value="Iluminación insuficiente">Iluminación insuficiente</option>
                    @endif
                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Ventilación
                    insuficiente', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Ventilación insuficiente">Ventilación insuficiente</option>
                    @else
                    <option value="Ventilación insuficiente">Ventilación insuficiente</option>
                    @endif
                    @if (isset($historial) && !empty($historial['historialLaboral']['trabajoRealizados']) && in_array('Sustancias
                    químicas', $historial['historialLaboral']['trabajoRealizados']) )
                    <option value="Sustancias químicas">Sustancias químicas</option>
                    @else
                    <option value="Sustancias químicas">Sustancias químicas</option>
                    @endif

                </select>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset class="form-group">
                    <label for="otras_exposiciones">Otras exposiciones</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['otrasExposiciones']))
                    <input type="text" class="form-control" id="otras_exposiciones" name="otras_exposiciones" value="{{$historial['historialLaboral']['otrasExposiciones']}}">
                    @else
                    <input type="text" class="form-control" id="otras_exposiciones" name="otras_exposiciones">
                    @endif
                </fieldset>
            </div>
        </div>
        <div class="row" id="accidentes" style="display:none;">
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="accidentes_t">Accidentes de trabajo:</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['accidentes_t']))
                    <input type="text" class="form-control" id="accidentes_t" name="accidentes_t" value="{{$historial['historialLaboral']['accidentes_t']}}">
                    @else
                    <input type="text" class="form-control" id="accidentes_t" name="accidentes_t">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="enfermedades_t">Enfermedades de trabajo:</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['enfermedades_t']))
                    <input type="text" class="form-control" id="enfermedades_t" name="enfermedades_t" value="{{$historial['historialLaboral']['enfermedades_t']}}">
                    @else
                    <input type="text" class="form-control" id="enfermedades_t" name="enfermedades_t">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">¿Realiza su trabajo generalmente de pie?</label>
                    @if ($historial['historialLaboral']['trabajo_pie']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie" checked value="si" id="trabajo_pie_si">
                        <label class="custom-control-label" for="trabajo_pie_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie" value="no" id="trabajo_pie_no">
                        <label class="custom-control-label" for="trabajo_pie_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie" value="si" id="trabajo_pie_si">
                        <label class="custom-control-label" for="trabajo_pie_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="trabajo_pie" value="no" id="trabajo_pie_no">
                        <label class="custom-control-label" for="trabajo_pie_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">¿Realiza su trabajo generalmente sentado?</label>
                    @if ($historial['historialLaboral']['trabajo_sentado']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_sentado" checked value="si" id="trabajo_sentado_si">
                        <label class="custom-control-label" for="trabajo_sentado_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_sentado" value="no" id="trabajo_sentado_no">
                        <label class="custom-control-label" for="trabajo_sentado_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_sentado" value="si" id="trabajo_sentado_si">
                        <label class="custom-control-label" for="trabajo_sentado_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="trabajo_sentado" value="no" id="trabajo_sentado_no">
                        <label class="custom-control-label" for="trabajo_sentado_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">¿Realiza su trabajo generalmente de pie con esfuerzo físico?</label>
                    @if ($historial['historialLaboral']['trabajo_pie_fisico']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie_fisico" checked value="si" id="trabajo_pie_fisico_si">
                        <label class="custom-control-label" for="trabajo_pie_fisico_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie_fisico" value="no" id="trabajo_pie_fisico_no">
                        <label class="custom-control-label" for="trabajo_pie_fisico_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie_fisico" value="si" id="trabajo_pie_fisico_si">
                        <label class="custom-control-label" for="trabajo_pie_fisico_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="trabajo_pie_fisico" value="no" id="trabajo_pie_fisico_no">
                        <label class="custom-control-label" for="trabajo_pie_fisico_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">¿Realiza su trabajo generalmente de pie caminando?</label>
                    @if ($historial['historialLaboral']['trabajo_pie_caminando']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie_caminando" checked value="si" id="trabajo_pie_caminando_si">
                        <label class="custom-control-label" for="trabajo_pie_caminando_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie_caminando" value="no" id="trabajo_pie_caminando_no">
                        <label class="custom-control-label" for="trabajo_pie_caminando_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="trabajo_pie_caminando" value="si" id="trabajo_pie_caminando_si">
                        <label class="custom-control-label" for="trabajo_pie_caminando_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="trabajo_pie_caminando" value="no" id="trabajo_pie_caminando_no">
                        <label class="custom-control-label" for="trabajo_pie_caminando_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">Usa herramientas manuales</label>
                    @if ($historial['historialLaboral']['herramientas_manuales']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="herramientas_manuales" checked value="si" id="herramientas_manuales_si">
                        <label class="custom-control-label" for="herramientas_manuales_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="herramientas_manuales" value="no" id="herramientas_manuales_no">
                        <label class="custom-control-label" for="herramientas_manuales_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="herramientas_manuales" value="si" id="herramientas_manuales_si">
                        <label class="custom-control-label" for="herramientas_manuales_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="herramientas_manuales" value="no" id="herramientas_manuales_no">
                        <label class="custom-control-label" for="herramientas_manuales_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">Utiliza máquinas</label>
                    @if ($historial['historialLaboral']['maquinas']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="maquinas" checked value="si" id="maquinas_si">
                        <label class="custom-control-label" for="maquinas_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="maquinas" value="no" id="maquinas_no">
                        <label class="custom-control-label" for="maquinas_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="maquinas" value="si" id="maquinas_si">
                        <label class="custom-control-label" for="maquinas_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="maquinas" value="no" id="maquinas_no">
                        <label class="custom-control-label" for="maquinas_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <hr>
            <div class="col-md-12">
                <p class="text-center">SINTOMAS LABORALES</p>
            </div>

            <hr>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">ACUFENOS</label>
                    @if ($historial['historialLaboral']['ACUFENOS']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="ACUFENOS" checked value="si" id="ACUFENOS_si">
                        <label class="custom-control-label" for="ACUFENOS_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="ACUFENOS" value="no" id="ACUFENOS_no">
                        <label class="custom-control-label" for="ACUFENOS_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="ACUFENOS" value="si" id="ACUFENOS_si">
                        <label class="custom-control-label" for="ACUFENOS_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="ACUFENOS" value="no" id="ACUFENOS_no">
                        <label class="custom-control-label" for="ACUFENOS_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">OTALGIA</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['OTALGIA']) &&
                    $historial['historialLaboral']['OTALGIA']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="OTALGIA" checked value="si" id="OTALGIA_si">
                        <label class="custom-control-label" for="OTALGIA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="OTALGIA" value="no" id="OTALGIA_no">
                        <label class="custom-control-label" for="OTALGIA_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="OTALGIA" value="si" id="OTALGIA_si">
                        <label class="custom-control-label" for="OTALGIA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="OTALGIA" value="no" id="OTALGIA_no">
                        <label class="custom-control-label" for="OTALGIA_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">PRURITO OTICO</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['PRURITO_OTICO']) &&
                    $historial['historialLaboral']['PRURITO_OTICO']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="PRURITO_OTICO" checked value="si" id="PRURITO_OTICO_si">
                        <label class="custom-control-label" for="OPRURITO_OTICO_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="PRURITO_OTICO" value="no" id="PRURITO_OTICO_no">
                        <label class="custom-control-label" for="PRURITO_OTICO_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="PRURITO_OTICO" value="si" id="PRURITO_OTICO_si">
                        <label class="custom-control-label" for="PRURITO_OTICO_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="PRURITO_OTICO" value="no" id="PRURITO_OTICO_no">
                        <label class="custom-control-label" for="PRURITO_OTICO_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>

            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">PLENITUD OTICA</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['PLENITUD_OTICA']) &&
                    $historial['historialLaboral']['PLENITUD_OTICA']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="PLENITUD_OTICA" checked value="si" id="PLENITUD_OTICA_si">
                        <label class="custom-control-label" for="PLENITUD_OTICA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="PLENITUD_OTICA" value="no" id="PLENITUD_OTICA_no">
                        <label class="custom-control-label" for="PLENITUD_OTICA_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="PLENITUD_OTICA" value="si" id="PLENITUD_OTICA_si">
                        <label class="custom-control-label" for="PLENITUD_OTICA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="PLENITUD_OTICA" value="no" id="PLENITUD_OTICA_no">
                        <label class="custom-control-label" for="PLENITUD_OTICA_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">TOS PRODUCTIVA </label>
                    @if ($historial['historialLaboral']['TOS_PRODUCTIVA']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="TOS_PRODUCTIVA" checked value="si" id="TOS_PRODUCTIVA_si">
                        <label class="custom-control-label" for="TOS_PRODUCTIVA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="TOS_PRODUCTIVA" value="no" id="TOS_PRODUCTIVA_no">
                        <label class="custom-control-label" for="TOS_PRODUCTIVA_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="TOS_PRODUCTIVA" value="si" id="TOS_PRODUCTIVA_si">
                        <label class="custom-control-label" for="TOS_PRODUCTIVA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="TOS_PRODUCTIVA" value="no" id="TOS_PRODUCTIVA_no">
                        <label class="custom-control-label" for="TOS_PRODUCTIVA_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label class="d-block">DISNEA</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['DISNEA']) &&
                    $historial['historialLaboral']['DISNEA']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="DISNEA" checked value="si" id="DISNEA_si">
                        <label class="custom-control-label" for="DISNEA">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="DISNEA" value="no" id="DISNEA_no">
                        <label class="custom-control-label" for="DISNEA_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="DISNEA" value="si" id="DISNEA_si">
                        <label class="custom-control-label" for="DISNEA_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="DISNEA" value="no" id="DISNEA_no">
                        <label class="custom-control-label" for="DISNEA_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <hr>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="d-block">¿Su trabajo le ocasiona tesión emocional?</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['tension_e']) &&
                    $historial['historialLaboral']['tension_e']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="tension_e" checked value="si" id="tension_e_si">
                        <label class="custom-control-label" for="tension_e_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="tension_e" value="no" id="tension_e_no">
                        <label class="custom-control-label" for="tension_e_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="tension_e" value="si" id="tension_e_si">
                        <label class="custom-control-label" for="tension_e_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="tension_e" value="no" id="tension_e_no">
                        <label class="custom-control-label" for="tension_e_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="d-block">¿Usa equipo de protección individual?</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['proteccion_in']) &&
                    $historial['historialLaboral']['proteccion_in']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="proteccion_in" checked value="si" id="proteccion_in_si">
                        <label class="custom-control-label" for="proteccion_in_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="proteccion_in" value="no" id="proteccion_in_no">
                        <label class="custom-control-label" for="proteccion_in_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="proteccion_in" value="si" id="proteccion_in_si">
                        <label class="custom-control-label" for="proteccion_in_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="proteccion_in" value="no" id="proteccion_in_no">
                        <label class="custom-control-label" for="proteccion_in_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="d-block">¿Cuenta con medios adecuados de protección?</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['adecuados_pro']) &&
                    $historial['historialLaboral']['adecuados_pro']=='si')
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="adecuados_pro" checked value="si" id="adecuados_pro_si">
                        <label class="custom-control-label" for="adecuados_pro_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="adecuados_pro" value="no" id="adecuados_pro_no">
                        <label class="custom-control-label" for="adecuados_pro_no">no</label>
                    </div>
                    @else
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" name="adecuados_pro" value="si" id="adecuados_pro_si">
                        <label class="custom-control-label" for="adecuados_pro_si">si</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                        <input type="radio" class="custom-control-input bg-secondary" checked name="adecuados_pro" value="no" id="adecuados_pro_no">
                        <label class="custom-control-label" for="adecuados_pro_no">no</label>
                    </div>
                    @endif

                </fieldset>
            </div>
            <div class="col-md-12">
                <fieldset class="form-group">
                    <label for="extralaboral">¿En qué dedica su tiempo extralaboral?</label>
                    @if (isset($historial) && !empty($historial['historialLaboral']['extralaboral']))
                    <input type="text" class="form-control" id="extralaboral" name="extralaboral" value="{{$historial['historialLaboral']['extralaboral']}}" placeholder="Hobby/Deporte/Descanso/Actividades laborales con remuneración extra">
                    @else
                    <input type="text" class="form-control" id="extralaboral" name="extralaboral" placeholder="Hobby/Deporte/Descanso/Actividades laborales con remuneración extra">
                    @endif
                </fieldset>
            </div>
        </div>
        <div class="" id="laboral_examen_medico">
            <hr>
            <p class="text-center">Empleo Actual</p>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="nomEmpresa_actual">Nombre de la empresa:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="nomEmpresa_actual" name="nombreEmpresa_actual" placeholder="" value="{{$historial['historialLaboral']['empleos'][0]['nombre']}}">
                        @else
                        <input type="text" class="form-control" id="nomEmpresa_actual" name="nombreEmpresa_actual" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="actul_puesto">Puesto:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="actul_puesto" name="actual_puesto" placeholder="" value="{{$historial['historialLaboral']['empleos'][0]['puesto']}}">
                        @else
                        <input type="text" class="form-control" id="actul_puesto" name="actual_puesto" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="actual_empleo_actividad">Descripción de su actividad:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="actual_empleo_actividad" name="actual_empleo_actividad" placeholder="" value="{{$historial['historialLaboral']['empleos'][0]['actividad']}}">
                        @else
                        <input type="text" class="form-control" id="actual_empleo_actividad" name="actual_empleo_actividad" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="actual_empleo_duracion">Duración del empleo:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="actual_empleo_duracion" name="actual_empleo_duracion" placeholder="" value="{{$historial['historialLaboral']['empleos'][0]['duracion']}}">
                        @else
                        <input type="text" class="form-control" id="actual_empleo_duracion" name="actual_empleo_duracion" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="actual_equipoS">Equipo de seguridad usado:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="actual_equipoS" name="actual_equipoS" placeholder="" value="{{$historial['historialLaboral']['empleos'][0]['seguridad']}}">
                        @else
                        <input type="text" class="form-control" id="actual_equipoS" name="actual_equipoS" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="actual_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="actual_empleo_danos" name="actual_empleo_danos" placeholder="" value="{{$historial['historialLaboral']['empleos'][0]['danos']}}">
                        @else
                        <input type="text" class="form-control" id="actual_empleo_danos" name="actual_empleo_danos" placeholder="">
                        @endif
                    </fieldset>
                </div>
            </div>
            <hr>
            <p class="text-center">Empleo Anterior</p>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="nomEmpresa_anterior">Nombre de la empresa:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="nomEmpresa_anterior" name="nomEmpresa_anterior" placeholder="" value="{{$historial['historialLaboral']['empleos'][1]['nombre']}}">
                        @else
                        <input type="text" class="form-control" id="nomEmpresa_anterior" name="nomEmpresa_anterior" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="anterior_puesto">Puesto:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="anterior_puesto" name="anterior_puesto" placeholder="" value="{{$historial['historialLaboral']['empleos'][1]['puesto']}}">
                        @else
                        <input type="text" class="form-control" id="anterior_puesto" name="anterior_puesto" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="anterior_empleo_actividad">Descripción de su actividad:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="anterior_empleo_actividad" name="anterior_empleo_actividad" placeholder="" value="{{$historial['historialLaboral']['empleos'][1]['actividad']}}">
                        @else
                        <input type="text" class="form-control" id="anterior_empleo_actividad" name="anterior_empleo_actividad" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="anterior_empleo_duracion">Duración del empleo:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="anterior_empleo_duracion" name="anterior_empleo_duracion" placeholder="" value="{{$historial['historialLaboral']['empleos'][1]['duracion']}}">
                        @else
                        <input type="text" class="form-control" id="anterior_empleo_duracion" name="anterior_empleo_duracion" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="anteriro_equipoS">Equipo de seguridad usado:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="anteriro_equipoS" name="anteriro_equipoS" placeholder="" value="{{$historial['historialLaboral']['empleos'][1]['seguridad']}}">
                        @else
                        <input type="text" class="form-control" id="anteriro_equipoS" name="anteriro_equipoS" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="anterior_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="anterior_empleo_danos" name="anterior_empleo_danos" placeholder="" value="{{$historial['historialLaboral']['empleos'][1]['danos']}}">
                        @else
                        <input type="text" class="form-control" id="anterior_empleo_danos" name="anterior_empleo_danos" placeholder="">
                        @endif
                    </fieldset>
                </div>
            </div>

            <hr>
            <p class="text-center">Empleo Anterior</p>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="nomEmpresa_anterior_two">Nombre de la empresa:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="nomEmpresa_anterior_two" name="nomEmpresa_anterior_two" placeholder="" value="{{$historial['historialLaboral']['empleos'][2]['nombre']}}">
                        @else
                        <input type="text" class="form-control" id="nomEmpresa_anterior_two" name="nomEmpresa_anterior_two" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="two_anterior_puesto">Puesto:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="two_anterior_puesto" name="two_anterior_puesto" placeholder="" value="{{$historial['historialLaboral']['empleos'][2]['puesto']}}">
                        @else
                        <input type="text" class="form-control" id="two_anterior_puesto" name="two_anterior_puesto" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="two_anterior_empleo_actividad">Descripción de su actividad:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="two_anterior_empleo_actividad" name="two_anterior_empleo_actividad" placeholder="" value="{{$historial['historialLaboral']['empleos'][2]['actividad']}}">
                        @else
                        <input type="text" class="form-control" id="two_anterior_empleo_actividad" name="two_anterior_empleo_actividad" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="two_anterior_empleo_duracion">Duración del empleo:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="two_anterior_empleo_duracion" name="two_anterior_empleo_duracion" placeholder="" value="{{$historial['historialLaboral']['empleos'][2]['duracion']}}">
                        @else
                        <input type="text" class="form-control" id="two_anterior_empleo_duracion" name="two_anterior_empleo_duracion" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="two_anteriro_equipoS">Equipo de seguridad usado:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="two_anteriro_equipoS" name="two_anteriro_equipoS" placeholder="" value="{{$historial['historialLaboral']['empleos'][2]['seguridad']}}">
                        @else
                        <input type="text" class="form-control" id="two_anteriro_equipoS" name="two_anteriro_equipoS" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="two_anterior_empleo_danos">Posibles daños a la salud relacionados con el trabajo</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['empleos']))
                        <input type="text" class="form-control" id="two_anterior_empleo_danos" name="two_anterior_empleo_danos" placeholder="" value="{{$historial['historialLaboral']['empleos'][2]['danos']}}">
                        @else
                        <input type="text" class="form-control" id="two_anterior_empleo_danos" name="two_anterior_empleo_danos" placeholder="">
                        @endif
                    </fieldset>
                </div>
            </div>
            <hr>
            @php
            $EquipoS = array(
            'R' => 'Respiradores',
            'CSC' => 'Calzado de seguridad con/sin casquillos',
            'MP' => 'Mascarillas para polvo',
            'TA' => 'Tapones auditivos',
            'C' => 'Cascos',
            'Res' => 'Respirador',
            'LS' => 'Lentes de seguridad con o sin aumento',
            'LimEPP' => 'Limitante para utilizar EPP(Equipo de protección personal)',
            );
            @endphp
            <p class="text-center">Equipo de seguridad usando en el puesto actual</p>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <select class="selectize-multiple" class=" form-control" name="equipo_seguridad[]" placeholder="Equipo de seguridad" multiple>

                            @if (isset($historial) && !empty($historial['historialLaboral']['equipoSegActual']))
                            @foreach ($historial['historialLaboral']['equipoSegActual'] as $key => $recomendacionesBD)
                            ///
                            @foreach ($EquipoS as $keyBD => $value)
                            @if ($keyBD == $recomendacionesBD)
                            <option selected value={{$keyBD}}>{{$value}}</option>
                            @unset($EquipoS[$keyBD]);
                            @endif
                            @endforeach
                            ////
                            @endforeach
                            @foreach ($EquipoS as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @else
                            @foreach ($EquipoS as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @endif

                        </select>
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group">
                        <label class="d-block">¿Usa lentes graduados?</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['examenVista'][0]) &&
                        $historial['historialLaboral']['examenVista'][0]=='si')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="examen_vista[]" checked value="si" id="lentes_si">
                            <label class="custom-control-label" for="lentes_si">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="examen_vista[]" value="no" id="lentes_no">
                            <label class="custom-control-label" for="lentes_no">no</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="examen_vista[]" value="si" id="lentes_si">
                            <label class="custom-control-label" for="lentes_si">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="examen_vista[]" value="no" id="lentes_no">
                            <label class="custom-control-label" for="lentes_no">no</label>
                        </div>
                        @endif

                    </fieldset>
                    <fieldset class="form-group lentes">
                        <label for="examen_vista">Fecha de último examen</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['examenVista'][1]))
                        <input type="date" class="form-control lentes" id="examen_vista" name="examen_vista[]" placeholder="" value="{{$historial['historialLaboral']['examenVista'][1]}}">
                        @else
                        <input type="date" class="form-control lentes" id="examen_vista" name="examen_vista[]" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset class="form-group">
                        <label class="d-block">¿Usa lentes de contacto?</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['lentesContacto'][0]) &&
                        $historial['historialLaboral']['lentesContacto'][0]=='si')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="uso_lentes_graduados[]" value="si" id="lentes_graduados_si">
                            <label class="custom-control-label" for="lentes_graduados_si" onclick="showControll('graduados','show')">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="uso_lentes_graduados[]" value="no" id="lentes_graduados_no">
                            <label class="custom-control-label" for="lentes_graduados_no" onclick="hideControll('graduados','show')">no</label>
                        </div>

                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="uso_lentes_graduados[]" value="si" id="lentes_graduados_si">
                            <label class="custom-control-label" for="lentes_graduados_si" onclick="showControll('graduados','show')">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="uso_lentes_graduados[]" value="no" id="lentes_graduados_no">
                            <label class="custom-control-label" for="lentes_graduados_no" onclick="hideControll('graduados','show')">no</label>
                        </div>

                        @endif
                    </fieldset>
                    <fieldset class="form-group graduados" style="display:none">
                        <label for="examen_vista">¿De que tipo?</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['lentesContacto'][1]))
                        <input type="text" class="form-control graduados" id="examen_vista" name="uso_lentes_graduados[]" placeholder="" value="{{$historial['historialLaboral']['lentesContacto'][0]}}">
                        @else
                        <input type="text" class="form-control graduados" id="examen_vista" name="uso_lentes_graduados[]" placeholder="">
                        @endif
                    </fieldset>
                    <fieldset class="form-group">
                        <label class="d-block">¿Tiene alguna limitante para utilizar EPP(Equipo de Protección Personal)?</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['epp']) &&
                        $historial['historialLaboral']['epp']=='si')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="epp" value="si" id="epp_si">
                            <label class="custom-control-label" for="epp_si" onclick="">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="epp" value="no" id="epp_no">
                            <label class="custom-control-label" for="epp_no" onclick="">no</label>
                        </div>

                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="epp" value="si" id="epp_si">
                            <label class="custom-control-label" for="epp_si" onclick="">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="epp" value="no" id="epp_no">
                            <label class="custom-control-label" for="epp_no" onclick="">no</label>
                        </div>

                        @endif
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label for="">Otras exposiciones:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['otras_exposiciones']))
                        <input type="text" class="form-control" id="" name="otrasexpPuesto" placeholder="" value="{{$historial['historialLaboral']['otras_exposiciones']}}">
                        @else
                        <input type="text" class="form-control" id="" name="otrasexpPuesto" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="d-block">¿Alguien de su familia trabaja con materiales peligrosos? (Asbesto, Plomo,
                            Etc.)</label>

                        @if (isset($historial) && !empty($historial['historialLaboral']['famMaterialPeligroso']=='si'))
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="ma_peligroso_fam" value="si" id="ma_fam_peligroso_si">
                            <label class="custom-control-label" for="ma_fam_peligroso_si">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="ma_peligroso_fam" value="no" id="ma_fam_peligroso_no">
                            <label class="custom-control-label" for="ma_fam_peligroso_no">no</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="ma_peligroso_fam" value="si" id="ma_fam_peligroso_si">
                            <label class="custom-control-label" for="ma_fam_peligroso_si">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="ma_peligroso_fam" value="no" id="ma_fam_peligroso_no">
                            <label class="custom-control-label" for="ma_fam_peligroso_no">no</label>
                        </div>
                        @endif
                    </fieldset>
                </div>
                @php
                $VividoCerca = array(
                'Fábricas' => 'Fábricas',
                'Basurero' => 'Basurero',
                'Mina' => 'Mina',
                'Otro' => 'Otro lugar que genere residuos peligrosos',
                );
                @endphp
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="d-block">Alguna vez ha vivido cerca de:</label>
                        <select class="selectize-multiple" class=" form-control" name="lugar_vivido[]" placeholder="Lugares donde has vivido" multiple>

                            @if (isset($historial) && !empty($historial['historialLaboral']['lugaresVividos']))
                            @foreach ($historial['historialLaboral']['lugaresVividos'] as $key => $recomendacionesBD)
                            ///
                            @foreach ($VividoCerca as $keyBD => $value)
                            @if ($keyBD == $recomendacionesBD)
                            <option selected value={{$keyBD}}>{{$value}}</option>
                            @unset($VividoCerca[$keyBD]);
                            @endif
                            @endforeach
                            ////
                            @endforeach
                            @foreach ($VividoCerca as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @else
                            @foreach ($VividoCerca as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @endif

                        </select>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="exp_mat_peligrosos">Alguna otra exposición a materiales peligrosos:</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['expMaterialPeligroso']))
                        <input type="text" class="form-control" id="exp_mat_peligrosos" name="exp_mat_peligrosos" placeholder="" value="{{$historial['historialLaboral']['expMaterialPeligroso']}}">
                        @else
                        <input type="text" class="form-control" id="exp_mat_peligrosos" name="exp_mat_peligrosos" placeholder="">
                        @endif
                    </fieldset>
                </div>
            </div>
            <hr>
            <p class="text-center">Alergias</p>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label class="d-block">Reacción alergica a medicamentos</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['alergiaMedicamentos'])=="si")
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="reaccion_alergicas_medicamentos" value="si" id="reac_alergica_si">
                            <label class="custom-control-label" for="reac_alergica_si">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="reaccion_alergicas_medicamentos" value="no" id="reac_alergica_no">
                            <label class="custom-control-label" for="reac_alergica_no">no</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="reaccion_alergicas_medicamentos" value="si" id="reac_alergica_si">
                            <label class="custom-control-label" for="reac_alergica_si">si</label>
                        </div>
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" checked name="reaccion_alergicas_medicamentos" value="no" id="reac_alergica_no">
                            <label class="custom-control-label" for="reac_alergica_no">no</label>
                        </div>
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="problema_salud">¿Algún problema de salud en este momento?</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['problemaSalud']))
                        <input type="text" class="form-control" id="problema_salud" name="problema_salud" placeholder="" value="{{$historial['historialLaboral']['problemaSalud']}}">
                        @else
                        <input type="text" class="form-control" id="problema_salud" name="problema_salud" placeholder="">
                        @endif
                    </fieldset>
                </div>
            </div>
            <hr>
            <p class="text-center">Reproductivo</p>
            <hr>
            @php
            $problemaMujerA = array(
            'DA' => 'Dolor Articulaciones',
            'PEC' => 'Problemas espalda/cuello',
            'B' => 'Bochorno',
            'OP' => 'Otros problemas',
            'HI' => '¿Han tratado usted y su esposo de tener hijos por más de un año sin exito?',
            'Boc' => 'Bochorno',

            );
            @endphp
            <div class="row">
                @if ($paciente->genero=='Femenino'||$paciente->genero=='FEMENINO')
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label class="">Mencione si ha presentado lo siguiente</label>
                        <select class="selectize-multiple" class=" form-control" name="padecimientos_mujer[]" placeholder="Padecimientos" multiple>

                            @if (isset($historial) && !empty($historial['historialLaboral']['problemaMujer']))
                            @foreach ($historial['historialLaboral']['problemaMujer'] as $key => $recomendacionesBD)
                            ///
                            @foreach ($problemaMujerA as $keyBD => $value)
                            @if ($keyBD == $recomendacionesBD)
                            <option selected value={{$keyBD}}>{{$value}}</option>
                            @unset($problemaMujerA[$keyBD]);
                            @endif
                            @endforeach
                            ////
                            @endforeach
                            @foreach ($problemaMujerA as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @else
                            @foreach ($problemaMujerA as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @endif

                        </select>
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label for="abortos_ano">¿Han tenido, abortos?¿En que año?</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['abortos']))
                        <input type="date" class="form-control" id="abortos_ano" name="abortos_ano" placeholder="" value="{{$historial['historialLaboral']['abortos']}}">
                        @else
                        <input type="date" class="form-control" id="abortos_ano" name="abortos_ano" placeholder="">
                        @endif
                    </fieldset>
                </div>
                <div class="col-md-4">
                    <fieldset class="form-group">
                        <label for="embarazada_parto">Si esta embarazada, cual es la fecha probable de parto</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['fechaParto']))
                        <input type="date" class="form-control" id="embarazada_parto" name="embarazada_parto" placeholder="" value="{{$historial['historialLaboral']['fechaParto']}}">
                        @else
                        <input type="date" class="form-control" id="embarazada_parto" name="embarazada_parto" placeholder="">
                        @endif
                    </fieldset>
                </div>
                @endif
                @php
                $HIHA = array(
                'HIH' => '¿Han tratado de tener hijos por más de un año sin exito?',
                );
                @endphp
                @if ($paciente->genero=='Masculino'||$paciente->genero=='MASCULINO')
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label class="">Mencione si ha presentado lo siguiente</label>
                        <select class="selectize-multiple" class=" form-control" name="padecimientos_hombre[]" placeholder="Padecimientos" multiple>

                            @if (isset($historial) && !empty($historial['historialLaboral']['problemaHombre']))
                            @foreach ($historial['historialLaboral']['problemaHombre'] as $key => $recomendacionesBD)
                            ///
                            @foreach ($HIHA as $keyBD => $value)
                            @if ($keyBD == $recomendacionesBD)
                            <option selected value={{$keyBD}}>{{$value}}</option>
                            @unset($HIHA[$keyBD]);
                            @endif
                            @endforeach
                            ////
                            @endforeach
                            @foreach ($HIHA as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @else
                            @foreach ($HIHA as $key => $value)
                            <option value={{$key}}>{{$value}}</option>
                            @endforeach
                            @endif

                        </select>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="otros_padecimientos_hombre">Otros padecimientos</label>
                        @if (isset($historial) && !empty($historial['historialLaboral']['padecimientoHombre']))
                        <input type="text" class="form-control" id="otros_padecimientos_hombre" name="otros_padecimientos_hombre" value="{{$historial['historialLaboral']['padecimientoHombre']}}">
                        @else
                        <input type="text" class="form-control" id="otros_padecimientos_hombre" name="otros_padecimientos_hombre">
                        @endif
                    </fieldset>
                </div>
                @endif
            </div>

        </div>

    </div>
    <div id="HCemex">
        <hr>
        <p class="text-center">Interrogatorio por aparatos y sistemas</p>
        <hr>
        
        @php
        $neurologicoP = array(
        'DCFI' => 'Dolores de la cabeza frecuentes o intensos',
        'DFD' => 'Dificultad para dormir',
        'VM' => 'Vertigo o mareos',
        'PM(C)' => 'Problemas con la memoria(Concentración)',
        'Tem' => 'Temblor',
        'Ner' => 'Nerviosismo',
        'Dep' => 'Depresión',
        'AC' => 'Ataques o Convulsiones',
        'PAPC' => 'Paralisis de alguna parte del cuerpo',
        'PET' => 'Problemas de estres en el trabajo',
        'PVF' => 'Problemas en tu vida familiar',
        );
        @endphp
        <p class="text-center">¿Durante el año pasado tuvo usted alguno de los siguientes sínotmas?</p>
        <div class="row">
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Sintomas generales:</label>
                    <input type="text" name="sintomasgenerales" class="form-control" value="@if(!empty($historial['historialLaboral']['sintomasgenerales'])){{$historial['historialLaboral']['sintomasgenerales']}}@endif" placeholder="Escriba los Sintomas generales"/>
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Neurológico/Psicológico/NERVIOSO</label>
                    <select class="selectize-multiple" class="form-control" name="neurologico_psicologico[]" placeholder="Neurológico/Psicológico" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['neurologico']))
                        @foreach ($historial['historialLaboral']['neurologico'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($neurologicoP as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($neurologicoP[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($neurologicoP as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($neurologicoP as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $gastro = array(
            'PT' => 'Problemas al tragar',
            'DAI' => 'Dolor abdominal/Indigestion cronica',
            'CE' => 'Cambio en sus evacuaciones',
            'DP' => 'Diarrea persistente',
            'ENS' => 'Evacuaciones negras o sangre',
            'VR' => 'Vomitos repetitivos',
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Gastro Intestinal/DIGESTIVO</label>
                    <select class="selectize-multiple" class=" form-control" name="gastro_intestinal[]" placeholder="Gastro Intestinal" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['gastroIntestinal']))
                        @foreach ($historial['historialLaboral']['gastroIntestinal'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($gastro as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($gastro[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($gastro as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($gastro as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $genitourinarioA = array(
            'PNOMU' => 'Por la noche orina más de una vez',
            'OSPR' => 'Orina con sangre/Piedras en el riñon',
            'PO' => 'Problemas al orinar'
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Genitourinario</label>
                    <select class="selectize-multiple" class=" form-control" name="genitourinario[]" placeholder="Genitourinario" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['genitourinario']))
                        @foreach ($historial['historialLaboral']['genitourinario'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($genitourinarioA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($genitourinarioA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($genitourinarioA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($genitourinarioA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $cardiovasucularA = array(
            'DP' => 'Dolor de pecho',
            'Pal' => 'Palpitaciones',
            'HT' => 'Hinchazón de tobillos',
            'CPC' => 'Calambre en las piernas al caminar',
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Cardiovascular</label>
                    <select class="selectize-multiple" class=" form-control" name="cardiovascular[]" placeholder="Cardiovascular" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['cardiovasucular']))
                        @foreach ($historial['historialLaboral']['cardiovasucular'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($cardiovasucularA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($cardiovasucularA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($cardiovasucularA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($cardiovasucularA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $PulmunarA = array(
            'FA' => 'Le falta el aire',
            'SP' => 'Silbidos en el pecho',
            'TP' => 'Tos persistente'
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Pulmunar/RESPIRATORIO</label>
                    <select class="selectize-multiple" class=" form-control" name="pulmunar[]" placeholder="Pulmunar" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['pulmunar']))
                        @foreach ($historial['historialLaboral']['pulmunar'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($PulmunarA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($PulmunarA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($PulmunarA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($PulmunarA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $Endocrinoa = array(
            'AP' => 'Aumento o perdida de peso de mas de 5kgs',
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Endocrino</label>
                    <select class="selectize-multiple" class=" form-control" name="endocrino[]" placeholder="Endocrino" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['endocrino']))
                        @foreach ($historial['historialLaboral']['endocrino'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($Endocrinoa as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($Endocrinoa[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($Endocrinoa as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($Endocrinoa as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>

            @php
            $MusculoesqueleticoA = array(
            'DA' => 'Dolor Articulaciones',
            'PEC' => 'Problemas espalda/cuello',
            'CE' => 'Cansancio excesivo',
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Musculoesqueletico</label>
                    <select class="selectize-multiple" class=" form-control" name="musculoesqueletico[]" placeholder="Musculoesqueletico" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['musculoesqueletico']))
                        @foreach ($historial['historialLaboral']['musculoesqueletico'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($MusculoesqueleticoA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($MusculoesqueleticoA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($MusculoesqueleticoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($MusculoesqueleticoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $InmunológicoA = array(
            'GC' => 'Ganglo en cuello',
            'AI' => 'Axila o ingle',
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Inmunológico</label>
                    <select class="selectize-multiple" class=" form-control" name="inmunologico[]" placeholder="Inmunológico" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['inmunologico']))
                        @foreach ($historial['historialLaboral']['inmunologico'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($InmunológicoA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($InmunológicoA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($InmunológicoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($InmunológicoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $DermatológicoA = array(
            'PP' => 'Problemas en la piel'
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Dermatológico</label>
                    <select class="selectize-multiple" class=" form-control" name="dermatologico[]" placeholder="Dermatológico" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['dermatologico']))
                        @foreach ($historial['historialLaboral']['dermatologico'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($DermatológicoA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($DermatológicoA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($DermatológicoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($DermatológicoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $HematológicoA = array(
            'SC' => 'Sangrado poco comunes'
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">Hematológico</label>
                    <select class="selectize-multiple" class=" form-control" name="hematologico[]" placeholder="Hematológico" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['hematologico']))
                        @foreach ($historial['historialLaboral']['hematologico'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($HematológicoA as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($HematológicoA[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($HematológicoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($HematológicoA as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $circulatorio = array(
            'ER' => 'Enfermedad de Raynaud',
            'AC'=>'Ataque al corazón'
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">CIRCULATORIO:</label>
                    <select class="selectize-multiple" class=" form-control" name="circulatorio[]" placeholder="Circulatorio" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['circulatorio']))
                        @foreach ($historial['historialLaboral']['circulatorio'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($circulatorio as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($circulatorio[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($circulatorio as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($circulatorio as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            @php
            $reproductor = array(
            'IO' => 'Infección de orina',
            'VPH'=>'Virus del Papiloma Humano'
            );
            @endphp
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">REPRODUCTOR:</label>
                    <select class="selectize-multiple" class=" form-control" name="reproductor[]" placeholder="Reproductor" multiple>

                        @if (isset($historial) && !empty($historial['historialLaboral']['reproductor']))
                        @foreach ($historial['historialLaboral']['reproductor'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($reproductor as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{$keyBD}}>{{$value}}</option>
                        @unset($reproductor[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($reproductor as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @else
                        @foreach ($reproductor as $key => $value)
                        <option value={{$key}}>{{$value}}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">SENTIDOS:</label>
                    <input type="text" name="sentidos" class="form-control" value="@if(!empty($historial['historialLaboral']['sentidos'])){{$historial['historialLaboral']['sentidos']}}@endif" placeholder="Sentidos"/>
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label class="">OTROS:</label>
                    <input type="text" name="otrosproble" class="form-control" value="@if(!empty($historial['historialLaboral']['otrosproble'])){{$historial['historialLaboral']['otrosproble']}}@endif" placeholder="Otros"/>
                </fieldset>
            </div>
        </div>
        <hr>
    </div>
</fieldset>
<script type="text/javascript">
    if (document.getElementById('lentes_graduados_si').checked) {
        showControll('graduados', 'show');
    }

</script>
