<fieldset>
  <div id="gine" style="display:none;">
    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="inicio_regla">¿A qué edad ínicio su regla?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['inicioRegla']))
          <input type="text" class="form-control" id="inicio_regla" name="inicio_regla" placeholder="" value="{{$historial['genicoobstetrico']['inicioRegla']}}">
          @else
          <input type="text" class="form-control" id="inicio_regla" name="inicio_regla" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="frecuencia_regla">¿Frecuencia de regla?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['fRegla']))
          <input type="text" class="form-control" id="frecuencia_regla" name="frecuencia_regla" placeholder="" value="{{$historial['genicoobstetrico']['fRegla']}}">
          @else
          <input type="text" class="form-control" id="frecuencia_regla" name="frecuencia_regla" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="duracion_regla">¿Cuántos días dura su regla?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['duraRegla']))
          <input type="text" class="form-control" id="duracion_regla" name="duracion_regla" placeholder="" value="{{$historial['genicoobstetrico']['duraRegla']}}">
          @else
          <input type="text" class="form-control" id="duracion_regla" name="duracion_regla" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="inicio_re_sexual">¿Edad en que inicío sus relaciones sexuales?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['relacionSexInicio']))
          <input type="text" class="form-control" id="" name="inicio_re_sexual" placeholder="" value="{{$historial['genicoobstetrico']['relacionSexInicio']}}">
          @else
          <input type="text" class="form-control" id="" name="inicio_re_sexual" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="metodo_antic">¿Cuál metodo de planifcación ha usado?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['metodoUsado']))
          <input type="text" class="form-control" id="metodo_antic" name="metodo_antic" placeholder="" value="{{$historial['genicoobstetrico']['metodoUsado']}}">
          @else
          <input type="text" class="form-control" id="metodo_antic" name="metodo_antic" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="ult_mestruacion">¿Cuál fue la fecha de su ultima mestruación?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['ultimaMestruacion']))
          <input type="text" class="form-control" name="ult_mestruacion" id="ult_mestruacion" placeholder="" value="{{$historial['genicoobstetrico']['ultimaMestruacion']}}">
          @else
          <input type="text" class="form-control" name="ult_mestruacion" id="ult_mestruacion" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="embarazos">¿Cuántos embarazos ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numEmbarazos']))
          <input type="text" class="form-control" id="embarazos" name="embarazos" placeholder="" value="{{$historial['genicoobstetrico']['numEmbarazos']}}">
          @else
          <input type="text" class="form-control" id="embarazos" name="embarazos" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="partos">¿Cuántos partos ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numPartos']))
          <input type="text" class="form-control" id="partos" name="partos" placeholder="" value="{{$historial['genicoobstetrico']['numPartos']}}">
          @else
          <input type="text" class="form-control" id="partos" name="partos" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="cesareas">¿Cuántas cesáreas ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numCesareas']))
          <input type="text" class="form-control" id="cesareas" name="cesareas" placeholder="" value="{{$historial['genicoobstetrico']['numCesareas']}}">
          @else
          <input type="text" class="form-control" id="cesareas" name="cesareas" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="abortos">¿Cuántos abortos ha tenido?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['numAbortos']))
          <input type="text" class="form-control" id="abortos" name="abortos" placeholder="" value="{{$historial['genicoobstetrico']['numAbortos']}}">
          @else
          <input type="text" class="form-control" id="abortos" name="abortos" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="examen_matriz">¿Cuándo se realizó el último exámen del cáncer de matriz?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['fecha_examenCancer']))
          <input type="text" class="form-control" id="examen_matriz" name="examen_matriz" placeholder="" value="{{$historial['genicoobstetrico']['fecha_examenCancer']}}">
          @else
          <input type="text" class="form-control" id="examen_matriz" name="examen_matriz" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="examen_mamas">¿Cuándo se realizó el último exámen de mamas?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['fecha_examenMamas']))
          <input type="text" class="form-control" id="examen_mamas" name="examen_mamas" placeholder="" value="{{$historial['genicoobstetrico']['fecha_examenMamas']}}">
          @else
          <input type="text" class="form-control" id="examen_mamas" name="examen_mamas" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="menopausia">¿A qué edad tuvo su menopausia?</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['edadMenopausia']))
          <input type="text" class="form-control" id="menopausia" name="menopausia" placeholder="" value="{{$historial['genicoobstetrico']['edadMenopausia']}}">
          @else
          <input type="text" class="form-control" id="menopausia" name="menopausia" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="menopausia">Fecha de último papanicolaou</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['papanicolaou']))
          <input type="text" class="form-control" id="papanicolaou" name="papanicolaou" placeholder="" value="{{$historial['genicoobstetrico']['edadMenopausia']}}">
          @else
          <input type="text" class="form-control" id="papanicolaou" name="papanicolaou" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="menopausia">MPF</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['MPF']))
          <input type="text" class="form-control" id="MPF" name="MPF" placeholder="" value="{{$historial['genicoobstetrico']['edadMenopausia']}}">
          @else
          <input type="text" class="form-control" id="MPF" name="MPF" placeholder="">
          @endif
        </fieldset>
      </div>

      <div class="col-md-6">
        <fieldset class="form-group">
          <label class="d-block">¿Está embarazada?</label>
          @if (empty($historial['genicoobstetrico']['embarazada'])=='si')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="embarazada" value="si" id="embarazada_si">
            <label class="custom-control-label" for="embarazada_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="embarazada" value="no" id="embarazada_no">
            <label class="custom-control-label" for="embarazada_no">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="embarazada" value="si" id="embarazada_si">
            <label class="custom-control-label" for="embarazada_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="embarazada" checked value="no" id="embarazada_no">
            <label class="custom-control-label" for="embarazada_no">no</label>
          </div>
          @endif

        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label class="d-block">Dolor durante menstruación:</label>
          @if (empty($historial['genicoobstetrico']['do_menstruacion'])=='si')
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" checked name="do_menstruacion" value="si" id="do_menstruacion_si">
            <label class="custom-control-label" for="do_menstruacion_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="do_menstruacion" value="no" id="do_menstruacion_no">
            <label class="custom-control-label" for="do_menstruacion_no">no</label>
          </div>
          @else
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="do_menstruacion" value="si" id="do_menstruacion_si">
            <label class="custom-control-label" for="do_menstruacion_si">si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-secondary" name="do_menstruacion" checked value="no" id="do_menstruacion_no">
            <label class="custom-control-label" for="do_menstruacion_no">no</label>
          </div>
          @endif
        </fieldset>
      </div>
    </div>

  </div>
  <div class="" id="gine_historial_clinica" style="display:none;">
    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="MENARQUIA">MENARQUIA</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['MENARQUIA']))
          <input type="text" class="form-control" id="MENARQUIA" name="MENARQUIA" placeholder="" value="{{$historial['genicoobstetrico']['MENARQUIA']}}">
          @else
          <input type="text" class="form-control" id="MENARQUIA" name="MENARQUIA" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="ritmo_duracion">RITMO Y DURACIÓN:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['ritmo_duracion']))
          <input type="text" class="form-control" id="ritmo_duracion" name="ritmo_duracion" placeholder="" value="{{$historial['genicoobstetrico']['ritmo_duracion']}}">
          @else
          <input type="text" class="form-control" id="ritmo_duracion" name="ritmo_duracion" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="DISMENORREA">DISMENORREA:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['DISMENORREA']))
          <input type="text" class="form-control" id="DISMENORREA" name="DISMENORREA" placeholder="" value="{{$historial['genicoobstetrico']['DISMENORREA']}}">
          @else
          <input type="text" class="form-control" id="DISMENORREA" name="DISMENORREA" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-1">
        <fieldset class="form-group">
          <label for="G">G:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['G']))
          <input type="text" class="form-control" id="G" name="G" placeholder="" value="{{$historial['genicoobstetrico']['G']}}">
          @else
          <input type="text" class="form-control" id="G" name="G" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-1">
        <fieldset class="form-group">
          <label for="P">P:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['P']))
          <input type="text" class="form-control" id="P" name="P" placeholder="" value="{{$historial['genicoobstetrico']['P']}}">
          @else
          <input type="text" class="form-control" id="P" name="P" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-1">
        <fieldset class="form-group">
          <label for="A">A:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['A']))
          <input type="text" class="form-control" id="A" name="A" placeholder="" value="{{$historial['genicoobstetrico']['A']}}">
          @else
          <input type="text" class="form-control" id="A" name="A" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-1">
        <fieldset class="form-group">
          <label for="C">C:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['C']))
          <input type="text" class="form-control" id="C" name="C" placeholder="" value="{{$historial['genicoobstetrico']['C']}}">
          @else
          <input type="text" class="form-control" id="C" name="C" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="FUP">FUP/C:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['FUP']))
          <input type="text" class="form-control" id="FUP" name="FUP" placeholder="" value="{{$historial['genicoobstetrico']['FUP']}}">
          @else
          <input type="text" class="form-control" id="FUP" name="FUP" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="IVSA">IVSA:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['IVSA']))
          <input type="text" class="form-control" id="IVSA" name="IVSA" placeholder="" value="{{$historial['genicoobstetrico']['IVSA']}}">
          @else
          <input type="text" class="form-control" id="IVSA" name="IVSA" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="CS">N° C.S:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['CS']))
          <input type="text" class="form-control" id="CS" name="CS" placeholder="" value="{{$historial['genicoobstetrico']['CS']}}">
          @else
          <input type="text" class="form-control" id="CS" name="CS" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="FUM">FUM:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['FUM']))
          <input type="text" class="form-control" id="FUM" name="FUM" placeholder="" value="{{$historial['genicoobstetrico']['FUM']}}">
          @else
          <input type="text" class="form-control" id="FUM" name="FUM" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="MPF">MPF:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['MPF']))
          <input type="text" class="form-control" id="MPF" name="MPF" placeholder="" value="{{$historial['genicoobstetrico']['MPF']}}">
          @else
          <input type="text" class="form-control" id="MPF" name="MPF" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="DOC">DOC:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['DOC']))
          <input type="text" class="form-control" id="DOC" name="DOC" placeholder="" value="{{$historial['genicoobstetrico']['DOC']}}">
          @else
          <input type="text" class="form-control" id="DOC" name="DOC" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="GESTAC">EDA GESTAC:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['GESTAC']))
          <input type="text" class="form-control" id="GESTAC" name="GESTAC" placeholder="" value="{{$historial['genicoobstetrico']['GESTAC']}}">
          @else
          <input type="text" class="form-control" id="GESTAC" name="GESTAC" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="FPP">FPP:</label>
          @if (isset($historial) && !empty($historial['genicoobstetrico']['FPP']))
          <input type="text" class="form-control" id="FPP" name="FPP" placeholder="" value="{{$historial['genicoobstetrico']['FPP']}}">
          @else
          <input type="text" class="form-control" id="FPP" name="FPP" placeholder="">
          @endif
        </fieldset>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <fieldset class="form-group">
        <label class="d-block">Enfermedad de mama</label>
        @if (empty($historial['genicoobstetrico']['enf_mama'])=='si')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="enf_mama" value="si" id="enf_mama_si">
          <label class="custom-control-label" for="enf_mama_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="enf_mama" value="no" id="enf_mama_no">
          <label class="custom-control-label" for="enf_mama_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="enf_mama" value="si" id="enf_mama_si">
          <label class="custom-control-label" for="enf_mama_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="enf_mama" checked value="no" id="enf_mama_no">
          <label class="custom-control-label" for="enf_mama_no">no</label>
        </div>
        @endif
      </fieldset>
    </div>
    <div class="col-md-6">
      <fieldset class="form-group">
        <label class="d-block">Enfermedad de útero</label>
        @if (empty($historial['genicoobstetrico']['enf_utero'])=='si')
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" checked name="enf_utero" value="si" id="enf_utero_si">
          <label class="custom-control-label" for="enf_utero_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="enf_utero" value="no" id="enf_utero_no">
          <label class="custom-control-label" for="enf_utero_no">no</label>
        </div>
        @else
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="enf_utero" value="si" id="enf_utero_si">
          <label class="custom-control-label" for="enf_utero_si">si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-secondary" name="enf_utero" checked value="no" id="enf_utero_no">
          <label class="custom-control-label" for="enf_utero_no">no</label>
        </div>
        @endif
      </fieldset>
    </div>
  </div>

</fieldset>