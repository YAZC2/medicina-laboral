<fieldset>
    <div class="row">
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="puesto_solicitado">Puesto solicitado:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['puesto_solicitado']))
                <input type="text" class="form-control" id="puesto_solicitado" name="puesto_solicitado" placeholder=""
                    value="{{ $historial['historialfisico']['puesto_solicitado'] }}">
                @else
                <input type="text" class="form-control" id="puesto_solicitado" name="puesto_solicitado" placeholder="">
                @endif
            </fieldset>
        </div>
        {{-- <div class="col-md-3">
            <fieldset class="form-group">
                <label for="Somatometria">Somatometría:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Somatometria']))
                <input type="text" class="form-control" id="Somatometria" name="Somatometria" placeholder=""
                    value="{{ $historial['historialfisico']['Somatometria'] }}">
                @else
                <input type="text" class="form-control" id="Somatometria" name="Somatometria" placeholder="">
                @endif
            </fieldset>
        </div> --}}
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="Temperatura">Temperatura:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Temperatura']))
                <input type="text" class="form-control" id="Temperatura" name="Temperatura" placeholder=""
                    value="{{ $historial['historialfisico']['Temperatura'] }}">
                @else
                <input type="text" class="form-control" id="Temperatura" name="Temperatura" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="Temperatura">Talla:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Talla']))
                <input type="text" class="form-control" id="Talla" name="Talla" placeholder=""
                    value="{{ $historial['historialfisico']['Talla'] }}">
                @else
                <input type="text" class="form-control" id="Talla" name="Talla" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="Temperatura">Peso:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Peso']))
                <input type="text" class="form-control" id="Peso" name="Peso" placeholder=""
                    value="{{ $historial['historialfisico']['Peso'] }}">
                @else
                <input type="text" class="form-control" id="Peso" name="Peso" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="IMC">IMC:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['IMC']))
                <input type="text" class="form-control" id="IMC" name="IMC" placeholder=""
                    value="{{ $historial['historialfisico']['IMC'] }}">
                @else
                <input type="text" class="form-control" id="IMC" name="IMC" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="p_abdominal">Perímetro abdominal:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['p_abdominal']))
                <input type="text" class="form-control" id="p_abdominal" name="p_abdominal" placeholder=""
                    value="{{ $historial['historialfisico']['p_abdominal'] }}">
                @else
                <input type="text" class="form-control" id="p_abdominal" name="p_abdominal" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="p_toracico">Perímetro torácico:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['p_toracico']))
                <input type="text" class="form-control" id="p_toracico" name="p_toracico" placeholder=""
                    value="{{ $historial['historialfisico']['p_toracico'] }}">
                @else
                <input type="text" class="form-control" id="p_toracico" name="p_toracico" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-12">
            <h5>Signos Vitales</h5>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="TA">T.A.:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['TA']))
                <input type="text" class="form-control" id="TA" name="TA" placeholder=""
                    value="{{ $historial['historialfisico']['TA'] }}">
                @else
                <input type="text" class="form-control" id="TA" name="TA" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="FC">F.C.:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['FC']))
                <input type="text" class="form-control" id="FC" name="FC" placeholder=""
                    value="{{ $historial['historialfisico']['FC'] }}">
                @else
                <input type="text" class="form-control" id="FC" name="FC" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="FR">F.R.:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['FR']))
                <input type="text" class="form-control" id="FR" name="FR" placeholder=""
                    value="{{ $historial['historialfisico']['FR'] }}">
                @else
                <input type="text" class="form-control" id="FR" name="FR" placeholder="">
                @endif
            </fieldset>
        </div>

        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="SatO2">Sat O2 :</label>
                @if (isset($historial) && !empty($historial['historialfisico']['SatO2']))
                <input type="text" class="form-control" id="SatO2" name="SatO2" placeholder=""
                    value="{{ $historial['historialfisico']['SatO2'] }}">
                @else
                <input type="text" class="form-control" id="SatO2" name="SatO2" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-12">
            <h5>Inspección General</h5>
        </div>
        <div class="col-md-12">
            <p>Agudeza Visual</p>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="">Ojo Derecho:</label><br>
                <label for="SL">Sin Lentes</label>
                @if (isset($historial) && !empty($historial['historialfisico']['SL']))
                <input type="text" class="form-control" id="SL" name="SL" placeholder=""
                    value="{{ $historial['historialfisico']['SL'] }}">
                @else
                <input type="text" class="form-control" id="SL" name="SL" placeholder="">
                @endif
                <label for="CL">Con Lentes</label>
                @if (isset($historial) && !empty($historial['historialfisico']['CL']))
                <input type="text" class="form-control" id="CL" name="CL" placeholder=""
                    value="{{ $historial['historialfisico']['CL'] }}">
                @else
                <input type="text" class="form-control" id="CL" name="CL" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="OI">Ojo Izquiero:</label><br>
                <label for="SLI">Sin Lentes</label>
                @if (isset($historial) && !empty($historial['historialfisico']['SLI']))
                <input type="text" class="form-control" id="SLI" name="SLI" placeholder=""
                    value="{{ $historial['historialfisico']['SLI'] }}">
                @else
                <input type="text" class="form-control" id="SLI" name="SLI" placeholder="">
                @endif
                <label for="CLI">Con Lentes</label>
                @if (isset($historial) && !empty($historial['historialfisico']['CLI']))
                <input type="text" class="form-control" id="CLI" name="CLI" placeholder=""
                    value="{{ $historial['historialfisico']['CLI'] }}">
                @else
                <input type="text" class="form-control" id="CLI" name="CLI" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="vision_colores">Visión a los colores:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['vision_colores']))
                <input type="text" class="form-control" id="vision_colores" name="vision_colores" placeholder=""
                    value="{{ $historial['historialfisico']['vision_colores'] }}">
                @else
                <input type="text" class="form-control" id="vision_colores" name="vision_colores" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="Nariz">Nariz:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Nariz']))
                <input type="text" class="form-control" id="Nariz" name="Nariz" placeholder="Normal/Anormal"
                    value="{{ $historial['historialfisico']['Nariz'] }}">
                @else
                <input type="text" class="form-control" id="Nariz" name="Nariz" placeholder="Normal/Anormal">
                @endif
            </fieldset>
        </div>
        <div class="col-md-12">
            <fieldset class="form-group">
                <h5 class="text-center">Oídos</h5>
            </fieldset>
        </div>
        <div class="col-md-12">
            <fieldset class="form-group">
                <h5 class="">
                    <h6>Oído Derecho:</h6>
                </h5>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="agueza_aud_d">Agudeza auditiva:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['agueza_aud_d']))
                <input type="text" class="form-control" id="agueza_aud_d" name="agueza_aud_d"
                    placeholder="Normal/Anormal" value="{{ $historial['historialfisico']['agueza_aud_d'] }}">
                @else
                <input type="text" class="form-control" id="agueza_aud_d" name="agueza_aud_d"
                    placeholder="Normal/Anormal">
                @endif
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="con_auditivos_d">Conductos auditivos:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['con_auditivos_d']))
                <input type="text" class="form-control" id="con_auditivos_d" name="con_auditivos"
                    placeholder="PERMEABLE" value="{{ $historial['historialfisico']['con_auditivos_d'] }}">
                @else
                <input type="text" class="form-control" id="con_auditivos_d" name="con_auditivos_d"
                    placeholder="PERMEABLE">
                @endif
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="mem_tim_d">Membrana timpánica:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['mem_tim_d']))
                <input type="text" class="form-control" id="mem_tim_d" name="mem_tim_d" placeholder="INTEGRA"
                    value="{{ $historial['historialfisico']['mem_tim_d'] }}">
                @else
                <input type="text" class="form-control" id="mem_tim_d" name="mem_tim_d" placeholder="INTEGRA">
                @endif
            </fieldset>
        </div>
        <div class="col-md-12">
            <fieldset class="form-group">
                <h5 class="">
                    <h6>Oído Izquierda:</h6>
                </h5>
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="agueza_aud_i">Agudeza auditiva:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['agueza_aud_i']))
                <input type="text" class="form-control" id="agueza_aud_i" name="agueza_aud_i"
                    placeholder="Normal/Anormal" value="{{ $historial['historialfisico']['agueza_aud_i'] }}">
                @else
                <input type="text" class="form-control" id="agueza_aud_i" name="agueza_aud_i"
                    placeholder="Normal/Anormal">
                @endif
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="con_auditivos_i">Conductos auditivos:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['con_auditivos_i']))
                <input type="text" class="form-control" id="con_auditivos_i" name="con_auditivos_i"
                    placeholder="PERMEABLE" value="{{ $historial['historialfisico']['con_auditivos_i'] }}">
                @else
                <input type="text" class="form-control" id="con_auditivos_i" name="con_auditivos_i"
                    placeholder="PERMEABLE">
                @endif
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="mem_tim_i">Membrana timpánica:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['mem_tim_i']))
                <input type="text" class="form-control" id="mem_tim_i" name="mem_tim_i" placeholder="INTEGRA"
                    value="{{ $historial['historialfisico']['mem_tim_i'] }}">
                @else
                <input type="text" class="form-control" id="mem_tim_i" name="mem_tim_i" placeholder="INTEGRA">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="cav_oral">Cavidad oral:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['cav_oral']))
                <input type="text" class="form-control" id="cav_oral" name="cav_oral" placeholder=""
                    value="{{ $historial['historialfisico']['cav_oral'] }}">
                @else
                <input type="text" class="form-control" id="cav_oral" name="cav_oral" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="orofaringe">Orofaringe:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['orofaringe']))
                <input type="text" class="form-control" id="orofaringe" name="orofaringe" placeholder=""
                    value="{{ $historial['historialfisico']['orofaringe'] }}">
                @else
                <input type="text" class="form-control" id="orofaringe" name="orofaringe" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="Amigdalas">Amigdalas:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Amigdalas']))
                <input type="text" class="form-control" id="Amigdalas" name="Amigdalas" placeholder=""
                    value="{{ $historial['historialfisico']['Amigdalas'] }}">
                @else
                <input type="text" class="form-control" id="Amigdalas" name="Amigdalas" placeholder="">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="Lengua_f002">Lengua:</label>
                @if (isset($historial) && !empty($historial['historialfisico']['Lengua_f002']))
                <input type="text" class="form-control" id="Lengua_f002" name="Lengua_f002" placeholder=""
                    value="{{ $historial['historialfisico']['Lengua_f002'] }}">
                @else
                <input type="text" class="form-control" id="Lengua_f002" name="Lengua_f002" placeholder="">
                @endif
            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Cuello:</label>
                @if ($historial['historialfisico']['Cuello']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Cuello" value="Normal"
                        id="Cuello_normal">
                    <label class="custom-control-label" for="Cuello_normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Cuello" value="Anormal"
                        id="Cuello_Anormal">
                    <label class="custom-control-label" for="Cuello_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Cuello" value="Normal"
                        id="Cuello_normal">
                    <label class="custom-control-label" for="Cuello_normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Cuello" value="Anormal"
                        id="Cuello_Anormal">
                    <label class="custom-control-label" for="Cuello_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['cu_observaciones']))
                <input type="text" class="form-control" id="cu_observaciones" name="cu_observaciones"
                    placeholder="Observaciones" value="{{ $historial['historialfisico']['cu_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="cu_observaciones" name="cu_observaciones"
                    placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Tórax:</label>
                @if ($historial['historialfisico']['Torax']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Torax" value="Normal"
                        id="Torax_Normal">
                    <label class="custom-control-label" for="Torax_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Torax" value="Anormal"
                        id="Torax_Anormal">
                    <label class="custom-control-label" for="Torax_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Torax" value="Normal"
                        id="Torax_Normal">
                    <label class="custom-control-label" for="Torax_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Torax" value="Anormal"
                        id="Torax_Anormal">
                    <label class="custom-control-label" for="Torax_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['to_observaciones']))
                <input type="text" class="form-control" id="to_observaciones" name="to_observaciones"
                    placeholder="Observaciones" value="{{ $historial['historialfisico']['to_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="to_observaciones" name="to_observaciones"
                    placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Campos pulmonares:</label>
                @if ($historial['historialfisico']['ca_pulmonares']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="ca_pulmonares"
                        value="Normal" id="ca_pulmonares_Normal">
                    <label class="custom-control-label" for="ca_pulmonares_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="ca_pulmonares" value="Anormal"
                        id="ca_pulmonares_Anormal">
                    <label class="custom-control-label" for="ca_pulmonares_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary"  name="ca_pulmonares"
                        value="Normal" id="ca_pulmonares_Normal">
                    <label class="custom-control-label" for="ca_pulmonares_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="ca_pulmonares" value="Anormal"
                        id="ca_pulmonares_Anormal">
                    <label class="custom-control-label" for="ca_pulmonares_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['ca_pulmonares_observaciones']))
                <input type="text" class="form-control" id="ca_pulmonares_observaciones"
                    name="ca_pulmonares_observaciones" placeholder="Observaciones"
                    value="{{ $historial['historialfisico']['ca_pulmonares_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="ca_pulmonares_observaciones"
                    name="ca_pulmonares_observaciones" placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Movimientos respiratorios:</label>
                @if ($historial['historialfisico']['mo_respiratorios']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="mo_respiratorios"
                        value="Normal" id="mo_respiratorios_Normal">
                    <label class="custom-control-label" for="mo_respiratorios_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="mo_respiratorios"
                        value="Anormal" id="mo_respiratorios_Anormal">
                    <label class="custom-control-label" for="mo_respiratorios_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary"  name="mo_respiratorios"
                        value="Normal" id="mo_respiratorios_Normal">
                    <label class="custom-control-label" for="mo_respiratorios_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="mo_respiratorios"
                        value="Anormal" id="mo_respiratorios_Anormal">
                    <label class="custom-control-label" for="mo_respiratorios_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['mo_respiratorios_observaciones']))
                <input type="text" class="form-control" id="mo_respiratorios_observaciones"
                    name="mo_respiratorios_observaciones" placeholder="Observaciones"
                    value="{{ $historial['historialfisico']['mo_respiratorios_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="mo_respiratorios_observaciones"
                    name="mo_respiratorios_observaciones" placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Ruidos cardiacos:</label>
                @if ($historial['historialfisico']['ruicardi']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="ruicardi" value="Normal"
                        id="ruicardi_Normal">
                    <label class="custom-control-label" for="ruicardi_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="ruicardi" value="Anormal"
                        id="ruicardi_Anormal">
                    <label class="custom-control-label" for="ruicardi_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary"  name="ruicardi" value="Normal"
                        id="ruicardi_Normal">
                    <label class="custom-control-label" for="ruicardi_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="ruicardi" value="Anormal"
                        id="ruicardi_Anormal">
                    <label class="custom-control-label" for="ruicardi_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['ruicardi_observaciones']))
                <input type="text" class="form-control" id="ruicardi_observaciones" name="ruicardi_observaciones"
                    placeholder="Observaciones" value="{{ $historial['historialfisico']['ruicardi_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="ruicardi_observaciones" name="ruicardi_observaciones"
                    placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-12">
            <h5 class="text-center">Abdomen</h5>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Inspección:</label>
                @if ($historial['historialfisico']['Inspeccion']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Inspeccion"
                        value="Normal" id="Inspeccion_Normal">
                    <label class="custom-control-label" for="Inspeccion_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Inspeccion" value="Anormal"
                        id="Inspeccion_Anormal">
                    <label class="custom-control-label" for="Inspeccion_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Inspeccion"
                        value="Normal" id="Inspeccion_Normal">
                    <label class="custom-control-label" for="Inspeccion_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Inspeccion" value="Anormal"
                        id="Inspeccion_Anormal">
                    <label class="custom-control-label" for="Inspeccion_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['Inspeccion_observaciones']))
                <input type="text" class="form-control" id="Inspeccion_observaciones" name="Inspeccion_observaciones"
                    placeholder="Observaciones" value="{{ $historial['historialfisico']['Inspeccion_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="Inspeccion_observaciones" name="Inspeccion_observaciones"
                    placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Ruidos peristálticos:</label>
                @if ($historial['historialfisico']['R_peristalticos']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="R_peristalticos"
                        value="Normal" id="R_peristalticos_Normal">
                    <label class="custom-control-label" for="R_peristalticos_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="R_peristalticos" value="Anormal"
                        id="R_peristalticos_Anormal">
                    <label class="custom-control-label" for="R_peristalticos_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="R_peristalticos"
                        value="Normal" id="R_peristalticos_Normal">
                    <label class="custom-control-label" for="R_peristalticos_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="R_peristalticos" value="Anormal"
                        id="R_peristalticos_Anormal">
                    <label class="custom-control-label" for="R_peristalticos_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['R_peristalticos_observaciones']))
                <input type="text" class="form-control" id="R_peristalticos_observaciones"
                    name="R_peristalticos_observaciones" placeholder="Observaciones"
                    value="{{ $historial['historialfisico']['R_peristalticos_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="R_peristalticos_observaciones"
                    name="R_peristalticos_observaciones" placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Dolor a la palpación:</label>
                @if ($historial['historialfisico']['d_palpitacion']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="d_palpitacion"
                        value="Si" id="d_palpitacion_Si">
                    <label class="custom-control-label" for="d_palpitacion_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="d_palpitacion" value="No"
                        id="d_palpitacion_No">
                    <label class="custom-control-label" for="d_palpitacion_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="d_palpitacion"
                        value="Si" id="d_palpitacion_Si">
                    <label class="custom-control-label" for="d_palpitacion_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="d_palpitacion" value="No"
                        id="d_palpitacion_No">
                    <label class="custom-control-label" for="d_palpitacion_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Palpación:</label>
                @if ($historial['historialfisico']['palpitacion']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="palpitacion" value="Si"
                        id="palpitacion_Si">
                    <label class="custom-control-label" for="palpitacion_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="palpitacion" value="No"
                        id="palpitacion_No">
                    <label class="custom-control-label" for="palpitacion_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="palpitacion" value="Si"
                        id="palpitacion_Si">
                    <label class="custom-control-label" for="palpitacion_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="palpitacion" value="No"
                        id="palpitacion_No">
                    <label class="custom-control-label" for="palpitacion_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Diástasis de rectos:</label>
                @if ($historial['historialfisico']['diastasis']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="diastasis" value="Si"
                        id="diastasis_Si">
                    <label class="custom-control-label" for="diastasis_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="diastasis" value="No"
                        id="diastasis_No">
                    <label class="custom-control-label" for="diastasis_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="diastasis" value="Si"
                        id="diastasis_Si">
                    <label class="custom-control-label" for="diastasis_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="diastasis" value="No"
                        id="diastasis_No">
                    <label class="custom-control-label" for="diastasis_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Hígado:</label>
                @if ($historial['historialfisico']['Higado']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Higado" value="Si"
                        id="Higado_Si">
                    <label class="custom-control-label" for="Higado_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Higado" value="No"
                        id="Higado_No">
                    <label class="custom-control-label" for="Higado_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Higado" value="Si"
                        id="Higado_Si">
                    <label class="custom-control-label" for="Higado_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Higado" value="No"
                        id="Higado_No">
                    <label class="custom-control-label" for="Higado_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Vibices:</label>
                @if ($historial['historialfisico']['Vibices']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Vibices" value="Si"
                        id="Vibices_Si">
                    <label class="custom-control-label" for="Vibices_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Vibices" value="No"
                        id="Vibices_No">
                    <label class="custom-control-label" for="Vibices_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Vibices" value="Si"
                        id="Vibices_Si">
                    <label class="custom-control-label" for="Vibices_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Vibices" value="No"
                        id="Vibices_No">
                    <label class="custom-control-label" for="Vibices_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Hernias:</label>
                @if ($historial['historialfisico']['Hernias']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Hernias" value="Si"
                        id="Hernias_Si">
                    <label class="custom-control-label" for="Hernias_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Hernias" value="No"
                        id="Hernias_No">
                    <label class="custom-control-label" for="Hernias_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Hernias" value="Si"
                        id="Hernias_Si">
                    <label class="custom-control-label" for="Hernias_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Hernias" value="No"
                        id="Hernias_No">
                    <label class="custom-control-label" for="Hernias_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Bazo:</label>
                @if ($historial['historialfisico']['Bazo']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Bazo" value="Si"
                        id="Bazo_Si">
                    <label class="custom-control-label" for="Bazo_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Bazo" value="No" id="Bazo_No">
                    <label class="custom-control-label" for="Bazo_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Bazo" value="Si"
                        id="Bazo_Si">
                    <label class="custom-control-label" for="Bazo_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Bazo" value="No" id="Bazo_No">
                    <label class="custom-control-label" for="Bazo_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Tumoración:</label>
                @if ($historial['historialfisico']['Tumoracion']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Tumoracion" value="Si"
                        id="Tumoracion_Si">
                    <label class="custom-control-label" for="Tumoracion_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Tumoracion" value="No"
                        id="Tumoracion_No">
                    <label class="custom-control-label" for="Tumoracion_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Tumoracion" value="Si"
                        id="Tumoracion_Si">
                    <label class="custom-control-label" for="Tumoracion_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Tumoracion" value="No"
                        id="Tumoracion_No">
                    <label class="custom-control-label" for="Tumoracion_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Cicatriz:</label>
                @if ($historial['historialfisico']['Cicatriz']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Cicatriz" value="Si"
                        id="Cicatriz_Si">
                    <label class="custom-control-label" for="Cicatriz_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Cicatriz" value="No"
                        id="Cicatriz_No">
                    <label class="custom-control-label" for="Cicatriz_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Cicatriz" value="Si"
                        id="Cicatriz_Si">
                    <label class="custom-control-label" for="Cicatriz_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Cicatriz" value="No"
                        id="Cicatriz_No">
                    <label class="custom-control-label" for="Cicatriz_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Ganglios:</label>
                @if ($historial['historialfisico']['Ganglios']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Ganglios" value="Si"
                        id="Ganglios_Si">
                    <label class="custom-control-label" for="Ganglios_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Ganglios" value="No"
                        id="Ganglios_No">
                    <label class="custom-control-label" for="Ganglios_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Ganglios" value="Si"
                        id="Ganglios_Si">
                    <label class="custom-control-label" for="Ganglios_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Ganglios" value="No"
                        id="Ganglios_No">
                    <label class="custom-control-label" for="Ganglios_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-12">
            <h5>Movimientos Pélvicos</h5>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Movimientos:</label>
                @if ($historial['historialfisico']['Movimientos']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Movimientos"
                        value="Normal" id="Movimientos_Normal">
                    <label class="custom-control-label" for="Movimientos_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Movimientos" value="Anormal"
                        id="Movimientos_Anormal">
                    <label class="custom-control-label" for="Movimientos_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Movimientos"
                        value="Normal" id="Movimientos_Normal">
                    <label class="custom-control-label" for="Movimientos_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Movimientos" value="Anormal"
                        id="Movimientos_Anormal">
                    <label class="custom-control-label" for="Movimientos_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['Movimientos_observaciones']))
                <input type="text" class="form-control" id="Movimientos_observaciones" name="Movimientos_observaciones"
                    placeholder="Observaciones"
                    value="{{ $historial['historialfisico']['Movimientos_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="Movimientos_observaciones" name="Movimientos_observaciones"
                    placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label class="d-block">Marcha:</label>
                @if ($historial['historialfisico']['Marcha']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Marcha" value="Normal"
                        id="Marcha_Normal">
                    <label class="custom-control-label" for="Marcha_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Marcha" value="Anormal"
                        id="Marcha_Anormal">
                    <label class="custom-control-label" for="Marcha_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Marcha" value="Normal"
                        id="Marcha_Normal">
                    <label class="custom-control-label" for="Marcha_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Marcha" value="Anormal"
                        id="Marcha_Anormal">
                    <label class="custom-control-label" for="Marcha_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['Marcha_observaciones']))
                <input type="text" class="form-control" id="Marcha_observaciones" name="Marcha_observaciones"
                    placeholder="Observaciones" value="{{ $historial['historialfisico']['Marcha_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="Marcha_observaciones" name="Marcha_observaciones"
                    placeholder="Observaciones">
                @endif

            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">¿Hay acortamiento?</label>
                @if ($historial['historialfisico']['acortamiento']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="acortamiento" value="Si"
                        id="acortamiento_Si">
                    <label class="custom-control-label" for="acortamiento_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="acortamiento" value="No"
                        id="acortamiento_No">
                    <label class="custom-control-label" for="acortamiento_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="acortamiento" value="Si"
                        id="acortamiento_Si">
                    <label class="custom-control-label" for="acortamiento_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="acortamiento" value="No"
                        id="acortamiento_No">
                    <label class="custom-control-label" for="acortamiento_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">Flogosis</label>
                @if ($historial['historialfisico']['Flogosis']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Flogosis" value="Si"
                        id="Flogosis_Si">
                    <label class="custom-control-label" for="Flogosis_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Flogosis" value="No"
                        id="Flogosis_No">
                    <label class="custom-control-label" for="Flogosis_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Flogosis" value="Si"
                        id="Flogosis_Si">
                    <label class="custom-control-label" for="Flogosis_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Flogosis" value="No"
                        id="Flogosis_No">
                    <label class="custom-control-label" for="Flogosis_No">No</label>
                </div>
                @endif
            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">Ulceras</label>
                @if ($historial['historialfisico']['Ulceras']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Ulceras" value="Si"
                        id="Ulceras_Si">
                    <label class="custom-control-label" for="Ulceras_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Ulceras" value="No"
                        id="Ulceras_No">
                    <label class="custom-control-label" for="Ulceras_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Ulceras" value="Si"
                        id="Ulceras_Si">
                    <label class="custom-control-label" for="Ulceras_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Ulceras" value="No"
                        id="Ulceras_No">
                    <label class="custom-control-label" for="Ulceras_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">Reflejo patelar</label>
                @if ($historial['historialfisico']['Reflejo_patelar']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Reflejo_patelar"
                        value="Si" id="Reflejo_patelar_Si">
                    <label class="custom-control-label" for="Reflejo_patelar_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Reflejo_patelar" value="No"
                        id="Reflejo_patelar_No">
                    <label class="custom-control-label" for="Reflejo_patelar_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Reflejo_patelar"
                        value="Si" id="Reflejo_patelar_Si">
                    <label class="custom-control-label" for="Reflejo_patelar_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Reflejo_patelar" value="No"
                        id="Reflejo_patelar_No">
                    <label class="custom-control-label" for="Reflejo_patelar_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">Varices</label>
                @if ($historial['historialfisico']['Varices']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Varices" value="Si"
                        id="Varices_Si">
                    <label class="custom-control-label" for="Varices_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Varices" value="No"
                        id="Varices_No">
                    <label class="custom-control-label" for="Varices_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Varices" value="Si"
                        id="Varices_Si">
                    <label class="custom-control-label" for="Varices_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Varices" value="No"
                        id="Varices_No">
                    <label class="custom-control-label" for="Varices_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">Micosis</label>
                @if ($historial['historialfisico']['Micosis']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Micosis" value="Si"
                        id="Micosis_Si">
                    <label class="custom-control-label" for="Micosis_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Micosis" value="No"
                        id="Micosis_No">
                    <label class="custom-control-label" for="Micosis_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Micosis" value="Si"
                        id="Micosis_Si">
                    <label class="custom-control-label" for="Micosis_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Micosis" value="No"
                        id="Micosis_No">
                    <label class="custom-control-label" for="Micosis_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-3">
            <fieldset class="form-group">
                <label class="d-block">Llenado capilar</label>
                @if ($historial['historialfisico']['Llenado_capilar']=='Si')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Llenado_capilar"
                        value="Si" id="Llenado_capilar_Si">
                    <label class="custom-control-label" for="Llenado_capilar_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Llenado_capilar" value="No"
                        id="Llenado_capilar_No">
                    <label class="custom-control-label" for="Llenado_capilar_Si_No">No</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Llenado_capilar"
                        value="Si" id="Llenado_capilar_Si">
                    <label class="custom-control-label" for="Llenado_capilar_Si">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Llenado_capilar" value="No"
                        id="Llenado_capilar_No">
                    <label class="custom-control-label" for="Llenado_capilar_Si_No">No</label>
                </div>
                @endif

            </fieldset>
        </div>
        <div class="col-md-12">
            <h5 class="text-center">Miembros torácicos</h5>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label class="d-block">Movimientos:</label>
                @if ($historial['historialfisico']['Movimientos_to']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Movimientos_to"
                        value="Normal" id="Movimientos_to_Normal">
                    <label class="custom-control-label" for="Movimientos_to_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Movimientos_to" value="Anormal"
                        id="Movimientos_to_Anormal">
                    <label class="custom-control-label" for="Movimientos_to_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Movimientos_to"
                        value="Normal" id="Movimientos_to_Normal">
                    <label class="custom-control-label" for="Movimientos_to_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Movimientos_to" value="Anormal"
                        id="Movimientos_to_Anormal">
                    <label class="custom-control-label" for="Movimientos_to_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['Movimientos_to_observaciones']))
                <input type="text" class="form-control" id="Movimientos_to_observaciones"
                    name="Movimientos_to_observaciones" placeholder="Observaciones"
                    value="{{ $historial['historialfisico']['Movimientos_to_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="Movimientos_to_observaciones"
                    name="Movimientos_to_observaciones" placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label class="d-block">Llenado capilar:</label>
                @if ($historial['historialfisico']['lle_capilar_ca_to']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="lle_capilar_ca_to"
                        value="Normal" id="lle_capilar_ca_to_Normal">
                    <label class="custom-control-label" for="lle_capilar_ca_to_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="lle_capilar_ca_to"
                        value="Anormal" id="lle_capilar_ca_to_Anormal">
                    <label class="custom-control-label" for="lle_capilar_ca_to_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="lle_capilar_ca_to"
                        value="Normal" id="lle_capilar_ca_to_Normal">
                    <label class="custom-control-label" for="lle_capilar_ca_to_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="lle_capilar_ca_to"
                        value="Anormal" id="lle_capilar_ca_to_Anormal">
                    <label class="custom-control-label" for="lle_capilar_ca_to_Anormal">Anormal</label>
                </div>
                @endif
                @if (isset($historial) && !empty($historial['historialfisico']['lle_capilar_ca_to_observaciones']))
                <input type="text" class="form-control" id="lle_capilar_ca_to_observaciones"
                    name="lle_capilar_ca_to_observaciones" placeholder="Observaciones"
                    value="{{ $historial['historialfisico']['lle_capilar_ca_to_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="lle_capilar_ca_to_observaciones"
                    name="lle_capilar_ca_to_observaciones" placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label class="d-block">Flogosis:</label>
                @if ($historial['historialfisico']['Flogosis_t']=='Normal')
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Flogosis_t" value="Normal"
                        id="Flogosis_t_Normal">
                    <label class="custom-control-label" for="Flogosis_t_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="Flogosis_t" value="Anormal"
                        id="Flogosis_t_Anormal">
                    <label class="custom-control-label" for="Flogosis_t_Anormal">Anormal</label>
                </div>
                @else
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary"  name="Flogosis_t" value="Normal"
                        id="Flogosis_t_Normal">
                    <label class="custom-control-label" for="Flogosis_t_Normal">Normal</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked name="Flogosis_t" value="Anormal"
                        id="Flogosis_t_Anormal">
                    <label class="custom-control-label" for="Flogosis_t_Anormal">Anormal</label>
                </div>
                @endif
                @if ($historial['historialfisico']['Flogosis_observaciones'])
                <input type="text" class="form-control" id="Flogosis_observaciones" name="Flogosis_observaciones"
                    placeholder="Observaciones" value="{{ $historial['historialfisico']['Flogosis_observaciones'] }}">
                @else
                <input type="text" class="form-control" id="Flogosis_observaciones" name="Flogosis_observaciones"
                    placeholder="Observaciones">
                @endif
            </fieldset>
        </div>
    </div>
</fieldset>