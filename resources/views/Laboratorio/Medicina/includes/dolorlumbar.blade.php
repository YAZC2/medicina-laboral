<fieldset>
    <div id="somnolencia">
        <div class="col-md-12">
            {{-- <h5 class="text-center">Test de Evaluación funcional de oswestry</h5> --}}
      
            <div class="row">
                <div class="col-md-6">
                    <p>1.-En el último mes ha sentido dolor en la parte baja de la espalda(zona lumbar) con una duración
                        de un día o más?</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['dolor_espalda_baja'] == 'si')
                       <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" class="custom-control-input bg-secondary" value="si" checked name="dolor_espalda_baja" id="dolor_espalda_baja_si">
                            <label class="custom-control-label" for="dolor_espalda_baja_si">Si</label>
                        </div>
                  
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio"  class="custom-control-input bg-secondary" name="dolor_espalda_baja"
                                value="si" id="dolor_espalda_baja_si">
                            <label class="custom-control-label" for="dolor_espalda_baja_si">Si</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['dolor_espalda_baja'] == 'no')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="dolor_espalda_baja"
                                value="no" id="dolor_espalda_baja_no">
                            <label class="custom-control-label" for="dolor_espalda_baja_no">No</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio"  class="custom-control-input bg-secondary" name="dolor_espalda_baja"
                                value="no" id="dolor_espalda_baja_no">
                            <label class="custom-control-label" for="dolor_espalda_baja_no">No</label>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <p>2.-¿En el último mes ha experimentado dolor en el área sombreada, con una duración de un día o
                        más?</p>
                    <div class="form-group">
                        <div class='row'>
                            <div class="col-md-6">
                                <img src="https://has-humanly.com/img/munequito.png" alt="">
                            </div>
                            <div class="col-md-6">
                                @if (isset($fisioterapia) && $fisioterapia['dolor_area_sombreada'] == 'si')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked name="dolor_area_sombreada" id="dolor_area_sombreada_si">
                                    <label class="custom-control-label" for="dolor_area_sombreada_si">Si</label>
                                </div>
                        
                                @else
                                <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio"  class="custom-control-input bg-secondary" name="dolor_area_sombreada"
                                        value="si" id="dolor_area_sombreada_si">
                                    <label class="custom-control-label" for="dolor_area_sombreada_si">Si</label>
                                </div>
                                @endif
        
                                @if (isset($fisioterapia) && $fisioterapia['dolor_area_sombreada'] == 'no')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" checked class="custom-control-input bg-secondary" name="dolor_area_sombreada"
                                        value="no" id="dolor_area_sombreada_no">
                                    <label class="custom-control-label" for="dolor_area_sombreada_no">No</label>
                                </div>
                                @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio"  class="custom-control-input bg-secondary" name="dolor_area_sombreada"
                                        value="no" id="dolor_area_sombreada_no">
                                    <label class="custom-control-label" for="dolor_area_sombreada_no">No</label>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="primary">
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app">1.-En el último mes, cuántos dias ha experimentado dolor en la parte baja de la
                            espalda(Zona lumbar)?</label>
                        <input type="text" class="form-control" name="dias_parte_baja" id="dias_parte_baja" value="{{ $fisioterapia['dias_parte_baja']}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <p>2.-Indique sobre la escala de abajo qué tan fuerte fue el dolor, el día que más le dolió del mes
                        pasado?</p>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        @if (isset($fisioterapia) && $fisioterapia['dia_mas_dolio'] == 'super-happy')
                        <div class="d-inline-block custom-control custom-radio col-md-2">
                            <label for="super-happy2">
                                <input type="radio" checked id="super-happy2" class="super-happy" name="dia_mas_dolio" value="super-happy" data-valnumerico="10">
                                <svg viewBox="0 0 24 24"><path d="M12,17.5C14.33,17.5 16.3,16.04 17.11,14H6.89C7.69,16.04 9.67,17.5 12,17.5M8.5,11A1.5,1.5 0 0,0 10,9.5A1.5,1.5 0 0,0 8.5,8A1.5,1.5 0 0,0 7,9.5A1.5,1.5 0 0,0 8.5,11M15.5,11A1.5,1.5 0 0,0 17,9.5A1.5,1.5 0 0,0 15.5,8A1.5,1.5 0 0,0 14,9.5A1.5,1.5 0 0,0 15.5,11M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"></path> </svg>
                                <br>
                                0 Sin dolor
                            </label> 
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio col-md-2">
                            <label for="super-happy2">
                                <input type="radio" id="super-happy2" class="super-happy" name="dia_mas_dolio" value="super-happy" data-valnumerico="10">
                                <svg viewBox="0 0 24 24"><path d="M12,17.5C14.33,17.5 16.3,16.04 17.11,14H6.89C7.69,16.04 9.67,17.5 12,17.5M8.5,11A1.5,1.5 0 0,0 10,9.5A1.5,1.5 0 0,0 8.5,8A1.5,1.5 0 0,0 7,9.5A1.5,1.5 0 0,0 8.5,11M15.5,11A1.5,1.5 0 0,0 17,9.5A1.5,1.5 0 0,0 15.5,8A1.5,1.5 0 0,0 14,9.5A1.5,1.5 0 0,0 15.5,11M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"></path> </svg>
                                <br>
                                0 Sin dolor
                            </label> 
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['dia_mas_dolio'] == 'happy')
                            <div class="d-inline-block custom-control custom-radio col-md-2">
                                <label for="happy2">
                                    <input type="radio" checked id="happy2" class="happy" name="dia_mas_dolio" value="happy" data-valnumerico="10">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                        <path
                                            d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8C16.3,8 17,8.7 17,9.5M12,17.23C10.25,17.23 8.71,16.5 7.81,15.42L9.23,14C9.68,14.72 10.75,15.23 12,15.23C13.25,15.23 14.32,14.72 14.77,14L16.19,15.42C15.29,16.5 13.75,17.23 12,17.23Z">
                                        </path>
                                    </svg><br>
                                    2 Duele un poco
                                </label>
                            </div>
                        @else
                            <div class="d-inline-block custom-control custom-radio col-md-2">
                                <label for="happy2">
                                    <input type="radio" id="happy2" class="happy" name="dia_mas_dolio" value="happy" data-valnumerico="10">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                        <path
                                            d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8C16.3,8 17,8.7 17,9.5M12,17.23C10.25,17.23 8.71,16.5 7.81,15.42L9.23,14C9.68,14.72 10.75,15.23 12,15.23C13.25,15.23 14.32,14.72 14.77,14L16.19,15.42C15.29,16.5 13.75,17.23 12,17.23Z">
                                        </path>
                                    </svg><br>
                                    2 Duele un poco
                                </label>
                            </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['dia_mas_dolio'] == 'neutral')
                        <div class="d-inline-block custom-control custom-radio col-md-2">
                            <label for="neutral2">
                                <input type="radio" checked id="neutral2" class="neutral" name="dia_mas_dolio" value="neutral" data-valnumerico="10">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                    <path
                                        d="M8.5,11A1.5,1.5 0 0,1 7,9.5A1.5,1.5 0 0,1 8.5,8A1.5,1.5 0 0,1 10,9.5A1.5,1.5 0 0,1 8.5,11M15.5,11A1.5,1.5 0 0,1 14,9.5A1.5,1.5 0 0,1 15.5,8A1.5,1.5 0 0,1 17,9.5A1.5,1.5 0 0,1 15.5,11M12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22C6.47,22 2,17.5 2,12A10,10 0 0,1 12,2M9,14H15A1,1 0 0,1 16,15A1,1 0 0,1 15,16H9A1,1 0 0,1 8,15A1,1 0 0,1 9,14Z">
                                    </path>
                                </svg><br>
                                4 Duele un poco más
                            </label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio col-md-2">
                            <label for="neutral2">
                                <input type="radio" id="neutral2" class="neutral" name="dia_mas_dolio" value="neutral" data-valnumerico="10">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                    <path
                                        d="M8.5,11A1.5,1.5 0 0,1 7,9.5A1.5,1.5 0 0,1 8.5,8A1.5,1.5 0 0,1 10,9.5A1.5,1.5 0 0,1 8.5,11M15.5,11A1.5,1.5 0 0,1 14,9.5A1.5,1.5 0 0,1 15.5,8A1.5,1.5 0 0,1 17,9.5A1.5,1.5 0 0,1 15.5,11M12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22C6.47,22 2,17.5 2,12A10,10 0 0,1 12,2M9,14H15A1,1 0 0,1 16,15A1,1 0 0,1 15,16H9A1,1 0 0,1 8,15A1,1 0 0,1 9,14Z">
                                    </path>
                                </svg><br>
                                4 Duele un poco más
                            </label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['dia_mas_dolio'] == 'sad')
                            <div class="d-inline-block custom-control custom-radio col-md-2">
                                <label for="sad2">
                                    <input type="radio" id="sad2" checked class="sad" name="dia_mas_dolio" value="sad" data-valnumerico="10">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                    <path
                                        d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M15.5,8C16.3,8 17,8.7 17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M12,14C13.75,14 15.29,14.72 16.19,15.81L14.77,17.23C14.32,16.5 13.25,16 12,16C10.75,16 9.68,16.5 9.23,17.23L7.81,15.81C8.71,14.72 10.25,14 12,14Z">
                                    </path>
                                </svg>
                                    <br>
                                    6 Duele aún más
                                </label>
                            </div>
                        @else
                            <div class="d-inline-block custom-control custom-radio col-md-2">
                                <label for="sad2">
                                    <input type="radio" id="sad2" class="sad" name="dia_mas_dolio" value="sad" data-valnumerico="10">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                    <path
                                        d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M15.5,8C16.3,8 17,8.7 17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M12,14C13.75,14 15.29,14.72 16.19,15.81L14.77,17.23C14.32,16.5 13.25,16 12,16C10.75,16 9.68,16.5 9.23,17.23L7.81,15.81C8.71,14.72 10.25,14 12,14Z">
                                    </path>
                                </svg>
                                    <br>
                                    6 Duele aún más
                                </label>
                            </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['dia_mas_dolio'] == 'super-sad')
                            <div class="d-inline-block custom-control custom-radio col-md-2">
                                <label for="super-sad2">
                                    <input type="radio" checked id="super-sad2" class="super-sad" name="dia_mas_dolio" value="super-sad" data-valnumerico="10">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                    <path
                                        d="M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22A10,10 0 0,0 22,12C22,6.47 17.5,2 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M16.18,7.76L15.12,8.82L14.06,7.76L13,8.82L14.06,9.88L13,10.94L14.06,12L15.12,10.94L16.18,12L17.24,10.94L16.18,9.88L17.24,8.82L16.18,7.76M7.82,12L8.88,10.94L9.94,12L11,10.94L9.94,9.88L11,8.82L9.94,7.76L8.88,8.82L7.82,7.76L6.76,8.82L7.82,9.88L6.76,10.94L7.82,12M12,14C9.67,14 7.69,15.46 6.89,17.5H17.11C16.31,15.46 14.33,14 12,14Z">
                                    </path>
                                </svg>
                                <br>
                                8 Duele mucho
                                </label>
                            
                            </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio col-md-2">
                            <label for="super-sad2">
                                <input type="radio" id="super-sad2" class="super-sad" name="dia_mas_dolio" value="super-sad" data-valnumerico="10">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                version="1.1" width="30%" height="30%" viewBox="0 0 24 24">
                                <path
                                    d="M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22A10,10 0 0,0 22,12C22,6.47 17.5,2 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M16.18,7.76L15.12,8.82L14.06,7.76L13,8.82L14.06,9.88L13,10.94L14.06,12L15.12,10.94L16.18,12L17.24,10.94L16.18,9.88L17.24,8.82L16.18,7.76M7.82,12L8.88,10.94L9.94,12L11,10.94L9.94,9.88L11,8.82L9.94,7.76L8.88,8.82L7.82,7.76L6.76,8.82L7.82,9.88L6.76,10.94L7.82,12M12,14C9.67,14 7.69,15.46 6.89,17.5H17.11C16.31,15.46 14.33,14 12,14Z">
                                </path>
                            </svg>
                            <br>
                                8 Duele mucho
                            </label>
                        
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['dia_mas_dolio'] == 'demasiado-sad')
                            <div class="d-inline-block custom-control custom-radio col-md-2">
                                <label for="demasiado-sad2">
                                    <input type="radio" id="demasiado-sad2" checked class="demasiado-sad" name="dia_mas_dolio" value="demasiado-sad" data-valnumerico="10">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" width="15%" height="15%" viewBox="0 0 29 29">
                                        <path
                                            d="M18.414 10.727c.17 1.304-1.623 2.46-2.236 3.932-.986 2.479 2.405 3.747 3.512 1.4.931-1.974-.454-4.225-1.276-5.332zm.108 3.412c-.407.428-.954.063-.571-.408.227-.28.472-.646.667-1.037.128.338.236 1.097-.096 1.445zm-.522-4.137l-.755.506s-.503-.948-1.746-.948c-1.207 0-1.745.948-1.745.948l-.754-.506c.281-.748 1.205-2.002 2.499-2.002 1.295 0 2.218 1.254 2.501 2.002zm-7 0l-.755.506s-.503-.948-1.746-.948c-1.207 0-1.745.948-1.745.948l-.754-.506c.281-.748 1.205-2.002 2.499-2.002 1.295 0 2.218 1.254 2.501 2.002zm1-10.002c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 22c-5.514 0-10-4.486-10-10s4.486-10 10-10 10 4.486 10 10-4.486 10-10 10zm3.582-4.057c-.303.068-.645.076-1.023-.003-.903-.19-1.741-.282-2.562-.282-.819 0-1.658.092-2.562.282-1.11.233-1.944-.24-2.255-1.015-.854-2.131 1.426-3.967 4.816-3.967 1.207 0 2.245.22 3.062.588-.291.522-.44.912-.515 1.588-1.797-.874-6.359-.542-5.752 1.118.138.377 1.614-.279 3.205-.279 1.061 0 2.039.285 2.633.373.162.634.415 1.116.953 1.597z" />
                                    </svg>
                                    <br>
                                    10 El peor dolor
                                </label>
                            </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio col-md-2">
                            <label for="demasiado-sad2">
                                <input type="radio" id="demasiado-sad2" class="demasiado-sad" name="dia_mas_dolio" value="demasiado-sad" data-valnumerico="10">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                version="1.1" width="15%" height="15%" viewBox="0 0 29 29">
                                    <path
                                        d="M18.414 10.727c.17 1.304-1.623 2.46-2.236 3.932-.986 2.479 2.405 3.747 3.512 1.4.931-1.974-.454-4.225-1.276-5.332zm.108 3.412c-.407.428-.954.063-.571-.408.227-.28.472-.646.667-1.037.128.338.236 1.097-.096 1.445zm-.522-4.137l-.755.506s-.503-.948-1.746-.948c-1.207 0-1.745.948-1.745.948l-.754-.506c.281-.748 1.205-2.002 2.499-2.002 1.295 0 2.218 1.254 2.501 2.002zm-7 0l-.755.506s-.503-.948-1.746-.948c-1.207 0-1.745.948-1.745.948l-.754-.506c.281-.748 1.205-2.002 2.499-2.002 1.295 0 2.218 1.254 2.501 2.002zm1-10.002c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 22c-5.514 0-10-4.486-10-10s4.486-10 10-10 10 4.486 10 10-4.486 10-10 10zm3.582-4.057c-.303.068-.645.076-1.023-.003-.903-.19-1.741-.282-2.562-.282-.819 0-1.658.092-2.562.282-1.11.233-1.944-.24-2.255-1.015-.854-2.131 1.426-3.967 4.816-3.967 1.207 0 2.245.22 3.062.588-.291.522-.44.912-.515 1.588-1.797-.874-6.359-.542-5.752 1.118.138.377 1.614-.279 3.205-.279 1.061 0 2.039.285 2.633.373.162.634.415 1.116.953 1.597z" />
                                </svg>
                                <br>
                                10 El peor dolor
                            </label>
                        </div>
                        @endif

                    </div>



                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app">3._Usualmente ¿Cuánto dura el dolor en la parte baja de la espalda(zona
                            lumbar)?</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['baja_zona_lumbar'] == 'Menos de 12 horas')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" checked value="Menos de 12 horas"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_12hrs">
                                    <label class="custom-control-label" for="baja_zona_lumbar_12hrs">Menos de 12 horas</label>
                                </div>
                            @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="Menos de 12 horas"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_12hrs">
                                    <label class="custom-control-label" for="baja_zona_lumbar_12hrs">Menos de 12 horas</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['baja_zona_lumbar'] == '12-24 horas')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="12-24 horas"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_1224hrs" checked>
                                    <label class="custom-control-label" for="baja_zona_lumbar_1224hrs">12-24 horas</label>
                                </div>
                            @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="12-24 horas"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_1224hrs">
                                    <label class="custom-control-label" for="baja_zona_lumbar_1224hrs">12-24 horas</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['baja_zona_lumbar'] == '1-7 días')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" checked value="1-7 días"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_17hrs">
                                    <label class="custom-control-label" for="baja_zona_lumbar_17hrs">1-7 días</label>
                                </div>
                            @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="1-7 días"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_17hrs">
                                    <label class="custom-control-label" for="baja_zona_lumbar_17hrs">1-7 días</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['baja_zona_lumbar'] == 'Mayor 1 semana')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" checked class="custom-control-input bg-secondary" value="Mayor 1 semana"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_1s">
                                    <label class="custom-control-label" for="baja_zona_lumbar_1s">Mayor 1 semana</label>
                                </div>
                            @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="Mayor 1 semana"
                                        name="baja_zona_lumbar" id="baja_zona_lumbar_1s">
                                    <label class="custom-control-label" for="baja_zona_lumbar_1s">Mayor 1 semana</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="app">4._<¿El dolor lumbar baja hacía la pierna?</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['lumbar_pierna'] == 'si')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked
                                        name="lumbar_pierna" id="lumbar_pierna_si">
                                    <label class="custom-control-label" for="lumbar_pierna_si">Si</label>
                                </div>
                            @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si"
                                        name="lumbar_pierna" id="lumbar_pierna_si">
                                    <label class="custom-control-label" for="lumbar_pierna_si">Si</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['lumbar_pierna'] == 'no')
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" checked value="no"
                                        name="lumbar_pierna" id="lumbar_pierna_no">
                                    <label class="custom-control-label" for="lumbar_pierna_no">No</label>
                                </div>
                            @else
                                <div class="custom-control custom-radio mr-1">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="lumbar_pierna" id="lumbar_pierna_no">
                                    <label class="custom-control-label" for="lumbar_pierna_no">No</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="app">5._Indique a cuál de las siguientes personas ha consultado durante el último año por su dolor de espalda:</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group"> 
                        <label for="app">Doctor:</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['doctor_consultado'] == 'si')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked
                                        name="doctor_consultado" id="doctor_consultado_si">
                                    <label class="custom-control-label" for="doctor_consultado_si">Si</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si"
                                        name="doctor_consultado" id="doctor_consultado_si">
                                    <label class="custom-control-label" for="doctor_consultado_si">Si</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['doctor_consultado'] == 'no')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" checked value="no"
                                        name="doctor_consultado" id="doctor_consultado_no">
                                    <label class="custom-control-label" for="doctor_consultado_no">No</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="doctor_consultado" id="doctor_consultado_no">
                                    <label class="custom-control-label" for="doctor_consultado_no">No</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group"> 
                        <label for="app">Fisioterapia:</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['fisioterapia_consultado'] == 'si')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked
                                        name="fisioterapia_consultado" id="fisioterapia_consultado_si">
                                    <label class="custom-control-label" for="fisioterapia_consultado_si">Si</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si"
                                        name="fisioterapia_consultado" id="fisioterapia_consultado_si">
                                    <label class="custom-control-label" for="fisioterapia_consultado_si">Si</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['fisioterapia_consultado'] == 'no')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="fisioterapia_consultado" id="fisioterapia_consultado_no" checked>
                                    <label class="custom-control-label" for="fisioterapia_consultado_no">No</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="fisioterapia_consultado" id="fisioterapia_consultado_no">
                                    <label class="custom-control-label" for="fisioterapia_consultado_no">No</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group"> 
                        <label for="app">Enfermera:</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['enfermera_consultado'] == 'si')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked
                                        name="enfermera_consultado" id="enfermera_consultado_si">
                                    <label class="custom-control-label" for="enfermera_consultado_si">Si</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si"
                                        name="enfermera_consultado" id="enfermera_consultado_si">
                                    <label class="custom-control-label" for="enfermera_consultado_si">Si</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['enfermera_consultado'] == 'no')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="enfermera_consultado" id="enfermera_consultado_no" checked>
                                    <label class="custom-control-label" for="enfermera_consultado_no">No</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="enfermera_consultado" id="enfermera_consultado_no">
                                    <label class="custom-control-label" for="enfermera_consultado_no">No</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group"> 
                        <label for="app">Otro:</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['otro_consultado'] == 'si')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked
                                        name="otro_consultado" id="otro_consultado_si">
                                    <label class="custom-control-label" for="otro_consultado_si">Si</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si"
                                        name="otro_consultado" id="otro_consultado_si">
                                    <label class="custom-control-label" for="otro_consultado_si">Si</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['otro_consultado'] == 'no')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="otro_consultado" id="otro_consultado_no" checked>
                                    <label class="custom-control-label" for="otro_consultado_no">No</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="otro_consultado" id="otro_consultado_no">
                                    <label class="custom-control-label" for="otro_consultado_no">No</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group"> 
                        <label for="app">Niguno:</label>
                        <div class="col-md-6">
                            @if (isset($fisioterapia) && $fisioterapia['otro_consultado'] == 'si')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" checked
                                        name="ninguno_consultado" id="ninguno_consultado_si">
                                    <label class="custom-control-label" for="ninguno_consultado_si">Si</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="si" name="ninguno_consultado" id="ninguno_consultado_si">
                                    <label class="custom-control-label" for="ninguno_consultado_si">Si</label>
                                </div>
                            @endif
                            @if (isset($fisioterapia) && $fisioterapia['otro_consultado'] == 'no')
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no" checked
                                        name="ninguno_consultado" id="ninguno_consultado_no">
                                    <label class="custom-control-label" for="ninguno_consultado_no">No</label>
                                </div>
                            @else
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" class="custom-control-input bg-secondary" value="no"
                                        name="ninguno_consultado" id="ninguno_consultado_no">
                                    <label class="custom-control-label" for="ninguno_consultado_no">No</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                

            </div>
        </div>
</fieldset>