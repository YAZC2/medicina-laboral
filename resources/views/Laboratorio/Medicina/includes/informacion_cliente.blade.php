<fieldset>
    <div id="informacion_cliente">
      <div id="AlertaInformacion_Cliente">
  
      </div>
      <p>Identificación de las muestras</p>
      <div class="row">
        <div class="col-md-6">
          <fieldset class="form-group">
            <label for="actividad_laboral">No. Folio Web Flow:</label>
            <input type="text" class="form-control" name="folio_web_flow" id="folio_web_flow">
          </fieldset>
        </div> 
        <div class="col-md-6">
            <fieldset class="form-group">
              <label for="actividad_laboral">Edad:</label>
              <input type="text" class="form-control" name="edad" id="edad">
            </fieldset>
        </div> 
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Carta compromiso: Anexar copia  de la carta.</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="carta_compromiso" value="si" id="carta_compromiso_si">
                    <label class="custom-control-label" for="carta_compromiso_si"
                        onclick="showControll('carta_compromiso','show')">si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="carta_compromiso"
                        value="no" id="carta_compromiso_no">
                    <label class="custom-control-label" for="carta_compromiso_no"
                        onclick="hideControll('carta_compromiso','show')">no</label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block"><b>PLAN:</b></label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="VIDA" value="si" id="VIDA_si">
                    <label class="custom-control-label" for="VIDA_si"
                        onclick="showControll('VIDA','show')">VIDA</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="GM"
                        value="no" id="GM_no">
                    <label class="custom-control-label" for="GM_no"
                        onclick="hideControll('GM','show')">GM</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="GMRP"
                        value="no" id="GMRP_no">
                    <label class="custom-control-label" for="GMRP_no"
                        onclick="hideControll('GMRP','show')">GMRP</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="REC_HUM"
                        value="no" id="REC_HUM_no">
                    <label class="custom-control-label" for="REC_HUM_no"
                        onclick="hideControll('REC_HUM','show')">REC.HUM.</label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Nombre completo de la persona a quien se le tomaron la muestras: </label>
                <input type="text" class="form-control" name="folio_web_flow" id="folio_web_flow">  
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label class="d-block">Firma de persona a la que se le tomaron las muestras: </label>
                <input type="text" class="form-control" name="folio_web_flow" id="folio_web_flow">  
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="d-block">Fumador:</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="fumador" value="si" id="fumador_si">
                    <label class="custom-control-label" for="fumador_si"
                        onclick="showControll('fumador','show')">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="fumador"
                        value="no" id="fumador_no">
                    <label class="custom-control-label" for="fumador_no"
                        onclick="hideControll('fumador','show')">No</label>
                </div>
            </div>
        </div>
        </div>
        </div>
        <div class="col-md-12">
                <label class="d-block">El solicitante se identifico con la <b>credencial oficial</b>(con fotografía): </label>  
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Expedida por: </label>
                <input type="text" class="form-control" name="expedida_por" id="expedida_por">  
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <label class="d-block">Numero </label>
                    <input type="text" class="form-control" name="numero" id="numero">  
                </div>
                <div class="col-md-6">
                    <label class="d-block">De Fecha </label>
                    <input type="text" class="form-control" name="numero" id="numero">  
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <label class="d-block"><b>Tipo de muestras:</b> </label>  
        </div>
        <div class="col-md-6">
            <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Muestra de orina:</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="orina" value="si" id="orina_si">
                    <label class="custom-control-label" for="orina_si"
                        onclick="showControll('orina','show')">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="orina"
                        value="no" id="orina_no">
                    <label class="custom-control-label" for="orina_no"
                        onclick="hideControll('orina','show')">No</label>
                </div>
            </div>
        </div> 
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Muestra de sangre:</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="sangre" value="si" id="sangre_si">
                    <label class="custom-control-label" for="sangre_si"
                        onclick="showControll('sangre','show')">Si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="sangre"
                        value="no" id="sangre_no">
                    <label class="custom-control-label" for="sangre_no"
                        onclick="hideControll('sangre','show')">No</label>
                </div>
            </div>
        </div>
    </div>
    </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="d-block"><b>Información Adicional</b> </label>  
            </div>
        </div>
     
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label class="d-block">Peso (kg) </label>
                        <input type="text" class="form-control" name="expedida_por" id="expedida_por">
                    </div> 
                    <div class="col-md-6">
                        <label class="d-block"><b>Horas de ayuno</b> </label>
                        <input type="text" class="form-control" name="expedida_por" id="expedida_por">
                    </div> 
                </div> 
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Talla(mt)</label>
                <input type="text" class="form-control" name="expedida_por" id="expedida_por">  
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="d-block"><b>DOCUMENTOS </b></label>  
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="d-block">Examen Medico</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="examen_medico" value="si" id="examen_medico_si">
                    <label class="custom-control-label" for="examen_medico_si"
                        onclick="showControll('examen_medico','show')">si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="examen_medico"
                        value="no" id="examen_medico_no">
                    <label class="custom-control-label" for="examen_medico_no"
                        onclick="hideControll('examen_medico','show')">no</label>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="d-block">Electrocardiograma</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="electrocardiagrama" value="si" id="electrocardiagrama_si">
                    <label class="custom-control-label" for="electrocardiagrama_si"
                        onclick="showControll('electrocardiagrama','show')">si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="electrocardiagrama"
                        value="no" id="electrocardiagrama_no">
                    <label class="custom-control-label" for="electrocardiagrama_no"
                        onclick="hideControll('electrocardiagrama','show')">no</label>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="d-block">Papanicolau</label>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" checked
                        name="papanicolau" value="si" id="papanicolau_si">
                    <label class="custom-control-label" for="papanicolau_si"
                        onclick="showControll('papanicolau','show')">si</label>
                </div>
                <div class="d-inline-block custom-control custom-radio mr-1">
                    <input type="radio" class="custom-control-input bg-secondary" name="papanicolau"
                        value="no" id="papanicolau_no">
                    <label class="custom-control-label" for="papanicolau_no"
                        onclick="hideControll('papanicolau','show')">no</label>
                </div>
            </div>
        </div>
      </div>
    </div>
</fieldset>