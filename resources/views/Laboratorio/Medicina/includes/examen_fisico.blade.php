<fieldset>
    <div id="examen" style="display:none;">

        <div class="row">
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">Apariencia general:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['apariencia']))
                    <input type="text" class="form-control" id="apariencia" name="apariencia" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['apariencia'] }}">
                    @else
                    <input type="text" class="form-control" id="apariencia" name="apariencia" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="temperatura">Temperatura</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['temperatura']))
                    <input type="text" class="form-control" id="temperatura" name="temperatura" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['temperatura'] }}">
                    @else
                    <input type="text" class="form-control" id="temperatura" name="temperatura" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="fc">Fc:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['fc']))
                    <input type="text" class="form-control" id="fc" name="fc" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['fc'] }}">
                    @else
                    <input type="text" class="form-control" id="fc" name="fc" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="fr">Fr:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['fr']))
                    <input type="text" class="form-control" id="fr" name="fr" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['fr'] }}">
                    @else
                    <input type="text" class="form-control" id="fr" name="fr" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label for="altura">Altura:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['altura']))
                    <input type="text" class="form-control imc" pattern="^[0-9]+(.[0-9]+)?$" id="altura" placeholder="Metros" name="altura" step="0.01" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['altura'] }}">
                    @else
                    <input type="text" class="form-control imc" pattern="^[0-9]+(.[0-9]+)?$" id="altura" placeholder="Metros" name="altura" step="0.01" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label for="peso">Peso:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['peso']))
                    <input type="text" class="form-control imc" id="peso" placeholder="Kilogramos" name="peso" pattern="^[0-9]+(.[0-9]+)?$" placeholder="" value="{{ $historial['historialfisico']['peso'] }}">
                    @else
                    <input type="text" class="form-control imc" id="peso" placeholder="Kilogramos" name="peso" pattern="^[0-9]+(.[0-9]+)?$" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-2">
                <fieldset class="form-group">
                    <label for="imc">IMC</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['imc']))
                    <input type="text" class="form-control" id="imc" pattern="^[0-9]+(.[0-9]+)?$" name="imc" placeholder="" value="{{ $historial['historialfisico']['imc'] }}">
                    @else
                    <input type="text" class="form-control" id="imc" pattern="^[0-9]+(.[0-9]+)?$" name="imc" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="presion_derecho">Presión sanguínea: Brazo derecho</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['psBrazoDerecho']))
                    <input type="text" class="form-control" id="presion_derecho" name="presion_derecho" placeholder="" value="{{ $historial['historialfisico']['psBrazoDerecho'] }}">
                    @else
                    <input type="text" class="form-control" id="presion_derecho" name="presion_derecho" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="presion_izquierdo">Brazo izquierdo</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['psBrazoIzquierdo']))
                    <input type="text" class="form-control" id="presion_izquierdo" name="presion_izquierdo" placeholder="" value="{{ $historial['historialfisico']['psBrazoIzquierdo'] }}">
                    @else
                    <input type="text" class="form-control" id="presion_izquierdo" name="presion_izquierdo" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_cabeza">Cabeza y cuero/ojos</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['cabezaCueroOjos']))
                    <input type="text" class="form-control" id="observaciones_cabeza" name="observaciones_cabeza" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['cabezaCueroOjos'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_cabeza" name="observaciones_cabeza" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_oidos">Oidos/Nariz/Boca</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['oidoNarizBoca']))
                    <input type="text" class="form-control" id="observaciones_oidos" name="observaciones_oidos" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['oidoNarizBoca'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_oidos" name="observaciones_oidos" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_dientes">Dientes/Faringe</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['dientesFaringe']))
                    <input type="text" class="form-control" id="observaciones_dientes" name="observaciones_dientes" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['dientesFaringe'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_dientes" name="observaciones_dientes" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_cuello">Cuello</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['cuello']))
                    <input type="text" class="form-control" id="observaciones_cuello" name="observaciones_cuello" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['cuello'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_cuello" name="observaciones_cuello" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_tiroides">Tiroides</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['tiroides']))
                    <input type="text" class="form-control" id="observaciones_tiroides" name="observaciones_tiroides" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['tiroides'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_tiroides" name="observaciones_tiroides" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_nodo">Nodo Linfático</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['nodoLinfatico']))
                    <input type="text" class="form-control" id="observaciones_nodo" name="observaciones_nodo" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['nodoLinfatico'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_nodo" name="observaciones_nodo" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_torax">Torax y Pulmones</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['toraxPulmones']))
                    <input type="text" class="form-control" id="observaciones_torax" name="observaciones_torax" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['toraxPulmones'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_torax" name="observaciones_torax" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_pecho">Pecho</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['pecho']))
                    <input type="text" class="form-control" id="observaciones_pecho" name="observaciones_pecho" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['pecho'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_pecho" name="observaciones_pecho" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_corazon">Corazón</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['corazon']))
                    <input type="text" class="form-control" id="observaciones_corazon" name="observaciones_corazon" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['corazon'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_corazon" name="observaciones_corazon" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_abdomen">Abdomen</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['abdomen']))
                    <input type="text" class="form-control" id="observaciones_abdomen" name="observaciones_abdomen" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['abdomen'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_abdomen" name="observaciones_abdomen" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_rectal">Rectal Digital</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['rectalDigital']))
                    <input type="text" class="form-control" id="observaciones_rectal" name="observaciones_rectal" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['rectalDigital'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_rectal" name="observaciones_rectal" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_genitales">Genitales</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['genitales']))
                    <input type="text" class="form-control" id="observaciones_genitales" name="observaciones_genitales" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['genitales'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_genitales" name="observaciones_genitales" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_columna">Columna vertebral</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['columnaVertebral']))
                    <input type="text" class="form-control" id="observaciones_columna" name="observaciones_columna" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['columnaVertebral'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_columna" name="observaciones_columna" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_piel">Piel</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['piel']))
                    <input type="text" class="form-control" id="observaciones_piel" name="observaciones_piel" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['piel'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_piel" name="observaciones_piel" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_pulso">Pulso Arteral</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['pulsoArterial']))
                    <input type="text" class="form-control" id="observaciones_pulso" name="observaciones_pulso" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['pulsoArterial'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_pulso" name="observaciones_pulso" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_extremidades">Extremidades</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['extremidades']))
                    <input type="text" class="form-control" id="observaciones_extremidades" name="observaciones_extremidades" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['extremidades'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_extremidades" name="observaciones_extremidades" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_musculoesque">Musculoesqueleto</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['musculoesqueleto']))
                    <input type="text" class="form-control" id="observaciones_musculoesque" name="observaciones_musculoesque" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['musculoesqueleto'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_musculoesque" name="observaciones_musculoesque" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_relejos">Reflejos y Neurológicos</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['reflejosNeurologicos']))
                    <input type="text" class="form-control" id="observaciones_relejos" name="observaciones_relejos" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['reflejosNeurologicos'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_relejos" name="observaciones_relejos" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_mental">Estado Mental</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['estadoMental']))
                    <input type="text" class="form-control" id="observaciones_mental" name="observaciones_mental" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['estadoMental'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_mental" name="observaciones_mental" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="observaciones_comentarios">Otros Comentarios</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['otrosComentarios']))
                    <input type="text" class="form-control" id="observaciones_comentarios" name="observaciones_comentarios" placeholder="Observaciones Detectadas" value="{{ $historial['historialfisico']['otrosComentarios'] }}">
                    @else
                    <input type="text" class="form-control" id="observaciones_comentarios" name="observaciones_comentarios" placeholder="Observaciones Detectadas">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="audiometria_hf">AUDIOMETRIA</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['audiometria_hf']))
                    <input type="text" class="form-control" id="audiometria_hf" name="audiometria_hf" placeholder="Comentarios de Audiometria" value="{{ $historial['historialfisico']['audiometria_hf'] }}">
                    @else
                    <input type="text" class="form-control" id="audiometria_hf" name="audiometria_hf" placeholder="Comentarios de Audiometria">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="espirometria_hf">ESPIROMETRIA</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['espirometria_hf']))
                    <input type="text" class="form-control" id="espirometria_hf" name="espirometria_hf" placeholder="Comentarios de Espirometria" value="{{ $historial['historialfisico']['espirometria_hf'] }}">
                    @else
                    <input type="text" class="form-control" id="espirometria_hf" name="espirometria_hf" placeholder="Comentarios de Espirometria">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="espirometria_hf">RADIOGRAFIA TORAX</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['ra_torax']))
                    <input type="text" class="form-control" id="ra_torax" name="ra_torax" placeholder="Comentarios de Radiografía Torax" value="{{ $historial['historialfisico']['ra_torax'] }}">
                    @else
                    <input type="text" class="form-control" id="ra_torax" name="ra_torax" placeholder="Comentarios de Radiografía Torax">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="espirometria_hf">RADIOGRAFIA COLUMNA</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['ra_columna']))
                    <input type="text" class="form-control" id="ra_columna" name="ra_columna" placeholder="Comentarios de Radiografía Columna" value="{{ $historial['historialfisico']['ra_columna'] }}">
                    @else
                    <input type="text" class="form-control" id="ra_columna" name="ra_columna" placeholder="Comentarios de Radiografía Columna">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="metabolico">METABOLICO</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['metabolico']))
                    <input type="text" class="form-control" id="metabolico" name="metabolico" placeholder="Metabolico" value="{{ $historial['historialfisico']['metabolico'] }}">
                    @else
                    <input type="text" class="form-control" id="metabolico" name="metabolico" placeholder="Metabolico">
                    @endif
                </fieldset>
            </div>
        </div>
        <hr>
        <p class="text-center">Agudeza Visual</p>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="agudeza_izquierda">Ojo Izquierdo</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['ojoIzquierdo']))
                    <input type="text" class="form-control" id="agudeza_izquierda" name="agudeza_izquierdo" placeholder="" value="{{ $historial['historialfisico']['ojoIzquierdo'] }}">
                    @else
                    <input type="text" class="form-control" id="agudeza_izquierda" name="agudeza_izquierdo" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="agudeza_derecho">Ojo Derecho</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['ojoDerecho']))
                    <input type="text" class="form-control" id="agudeza_derecho" name="agudeza_derecho" placeholder="" value="{{ $historial['historialfisico']['ojoDerecho'] }}">
                    @else
                    <input type="text" class="form-control" id="agudeza_derecho" name="agudeza_derecho" placeholder="">
                    @endif
                </fieldset>
            </div>
        </div>
        <hr>
        <p class="text-center">Valoración y plan de recomendaciones del personal médico</p>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="uso_epp">Uso de EPP:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['uso_epp']))
                    <input type="text" class="form-control" id="uso_epp" name="uso_epp" placeholder="" value="{{ $historial['historialfisico']['uso_epp'] }}">
                    @else
                    <input type="text" class="form-control" id="uso_epp" name="uso_epp" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="audiometria">Audiometria</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['audiometria']))
                    <input type="text" class="form-control" id="audiometria" name="audiometria" placeholder="" value="{{ $historial['historialfisico']['audiometria'] }}">
                    @else
                    <input type="text" class="form-control" id="audiometria" name="audiometria" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="respiradores_r">Respiradoes</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['respiradores']))
                    <input type="text" class="form-control" id="respiradores_r" name="respiradores_r" placeholder="" value="{{ $historial['historialfisico']['respiradores'] }}">
                    @else
                    <input type="text" class="form-control" id="respiradores_r" name="respiradores_r" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="reentrenar">Reentrenar en:</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['reentrenar']))
                    <input type="text" class="form-control" id="reentrenar" name="reentrenar" placeholder="" value="{{ $historial['historialfisico']['reentrenar'] }}">
                    @else
                    <input type="text" class="form-control" id="reentrenar" name="reentrenar" placeholder="">
                    @endif
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="otra_recomendacion">Otros</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['otrasRecomend']))
                    <input type="text" class="form-control" id="otra_recomendacion" name="otra_recomendacion" placeholder="" value="{{ $historial['historialfisico']['otrasRecomend'] }}">
                    @else
                    <input type="text" class="form-control" id="otra_recomendacion" name="otra_recomendacion" placeholder="">
                    @endif
                </fieldset>
            </div>
        </div>
        <hr>
        <p class="text-center">Recomendaciones de estilo de vida</p>
        <hr>
        @php
        $recomendaciones = [
        'Df' => 'Dejar de fumar',
        'RA' => 'Reducir consume de alcohol',
        'BP' => 'Bajar de peso',
        'TRC' => 'Tratamineto para reducir de colesterol',
        'ME' => 'Manejo de estres',
        'EPP' => 'Examen del pecho personal',
        'ETP' => 'Examen Testicular Personal',
        'RE' => 'Rutina de ejercicio',
        'PRE' => 'Programa regular de ejercicios',
        'O' => 'Otros',
        ];
        @endphp
        <div class="row">
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="reentrenar">Recomendaciones</label>
                    <select class="selectize-multiple" class="form-control" name="recomendaciones_vida[]" placeholder="Recomendaciones" multiple>
                        @if (isset($historial) && !empty($historial['historialfisico']['recomEstiloVida']))
                        @foreach ($historial['historialfisico']['recomEstiloVida'] as $key => $recomendacionesBD)
                        ///
                        @foreach ($recomendaciones as $keyBD => $value)
                        @if ($keyBD == $recomendacionesBD)
                        <option selected value={{ $keyBD }}>{{ $value }}</option>
                        @unset($recomendaciones[$keyBD]);
                        @endif
                        @endforeach
                        ////
                        @endforeach
                        @foreach ($recomendaciones as $key => $value)
                        <option value={{ $key }}>{{ $value }}</option>
                        @endforeach
                        @else
                        @foreach ($recomendaciones as $key => $value)
                        <option value={{ $key }}>{{ $value }}</option>
                        @endforeach
                        @endif

                    </select>
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="form-group">
                    <label for="diagnostico_audiologico">Diagnostico Audiologico</label>
                    @if (isset($historial) && !empty($historial['historialfisico']['diagnosticoAudio']))
                    <textarea name="diagnostico_audiologico" id="diagnostico_audiologico" class="form-control" rows="8" cols="80">{{ $historial['historialfisico']['diagnosticoAudio'] }}</textarea>
                    @else
                    <textarea name="diagnostico_audiologico" id="diagnostico_audiologico" class="form-control" rows="8" cols="80"></textarea>
                    @endif
                </fieldset>
            </div>
        </div>

    </div>
    <div id="interrogatorio" style="display:none;">

        <div id="AlertaInterrogatorio">
        </div>
        <div class="row">
            <div class="col-md-12">
                <fieldset class="form-group">
                    <h5 class="primary text-center">TRABAJOS EN ALTURAS</h5>
                    <label for="interrogatorio" class="mb-2">Dirigido a identificar factores de Riesgo para
                        realizar
                        Trabajos</label>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class='row'>
                                <div class="col-md-6">
                                    <label class="d-block">¿Alguna vez ha presentado mareos o vértigos?</label>

                                    @if (isset($historial) && $historial['admision_con']['vertigos'] == 'si')

                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" checked name="vertigos" value="si" id="vertigos_si">
                                        <label class="custom-control-label" for="vertigos_si" onclick="showControll('vertigos','show')">si</label>
                                    </div>
                                    @else
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="vertigos" value="si" id="vertigos_si">
                                        <label class="custom-control-label" for="vertigos_si" onclick="showControll('vertigos','show')">si</label>
                                    </div>
                                    @endif
                                    @if (isset($historial) && $historial['admision_con']['vertigos'] == 'no')

                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" checked name="vertigos" value="no" id="vertigos_no">
                                        <label class="custom-control-label" for="vertigos_no" onclick="hideControll('vertigos','show')">no</label>
                                    </div>
                                    @else
                                    <div class="d-inline-block custom-control custom-radio mr-1">
                                        <input type="radio" class="custom-control-input bg-secondary" name="vertigos" value="no" id="vertigos_no">
                                        <label class="custom-control-label" for="vertigos_no" onclick="hideControll('vertigos','show')">no</label>
                                    </div>
                                    @endif

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Cuándo está a nivel de piso?</label>
                                        @if (isset($historial) && $historial['admision_con']['vertigo_piso'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="vertigo_piso" value="si" id="vertigo_piso_si">
                                            <label class="custom-control-label" for="vertigo_piso_si" onclick="showControll('vertigo_piso','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="vertigo_piso" value="si" id="vertigo_piso_si">
                                            <label class="custom-control-label" for="vertigo_piso_si" onclick="showControll('vertigo_piso','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['vertigo_piso'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="vertigo_piso" value="no" id="vertigo_piso_no">
                                            <label class="custom-control-label" for="vertigo_piso_no" onclick="hideControll('vertigo_piso','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="vertigo_piso" value="no" id="vertigo_piso_no">
                                            <label class="custom-control-label" for="vertigo_piso_no" onclick="hideControll('vertigo_piso','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Cuándo sube escaleras?</label>
                                        @if (isset($historial) && $historial['admision_con']['escaleras'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="escaleras" value="si" id="escaleras_si">
                                            <label class="custom-control-label" for="escaleras_si" onclick="showControll('escaleras','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="escaleras" value="si" id="escaleras_si">
                                            <label class="custom-control-label" for="escaleras_si" onclick="showControll('escaleras','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['escaleras'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="escaleras" value="no" id="escaleras_no">
                                            <label class="custom-control-label" for="escaleras_no" onclick="hideControll('escaleras','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="escaleras" value="no" id="escaleras_no">
                                            <label class="custom-control-label" for="escaleras_no" onclick="hideControll('escaleras','show')">no</label>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Cuándo se encuentra parado en una altura mayor a 2
                                            m?</label>
                                        @if (isset($historial) && $historial['admision_con']['altura'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="altura" value="si" id="altura_si">
                                            <label class="custom-control-label" for="altura_si" onclick="showControll('altura','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="altura" value="si" id="altura_si">
                                            <label class="custom-control-label" for="altura_si" onclick="showControll('altura','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['altura'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="altura" value="no" id="altura_no">
                                            <label class="custom-control-label" for="altura_no" onclick="hideControll('altura','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="altura" value="no" id="altura_no">
                                            <label class="custom-control-label" for="altura_no" onclick="hideControll('altura','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha presentado lesiones en oídos que le hayan
                                            roto la
                                            membrana
                                            timpánica?</label>
                                        @if (isset($historial) && $historial['admision_con']['membrana'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="membrana" value="si" id="membrana_si">
                                            <label class="custom-control-label" for="membrana_si" onclick="showControll('membrana','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="membrana" value="si" id="membrana_si">
                                            <label class="custom-control-label" for="membrana_si" onclick="showControll('membrana','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['membrana'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="membrana" value="no" id="membrana_no">
                                            <label class="custom-control-label" for="membrana_no" onclick="hideControll('membrana','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="membrana" value="no" id="membrana_no">
                                            <label class="custom-control-label" for="membrana_no" onclick="hideControll('membrana','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <label><b>** Exploración médica</b></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de marcha punta - talón</label>
                                        @if (isset($historial) && $historial['admision_con']['p_talon'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="p_talon" value="si" id="p_talon_si">
                                            <label class="custom-control-label" for="p_talon_si" onclick="showControll('p_talon','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="p_talon" value="si" id="p_talon_si">
                                            <label class="custom-control-label" for="p_talon_si" onclick="showControll('p_talon','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['p_talon'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="p_talon" value="no" id="p_talon_no">
                                            <label class="custom-control-label" for="p_talon_no" onclick="hideControll('p_talon','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="p_talon" value="no" id="p_talon_no">
                                            <label class="custom-control-label" for="p_talon_no" onclick="hideControll('p_talon','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Caminar por línea</label>
                                        @if (isset($historial) && $historial['admision_con']['linea'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="linea" value="si" id="linea_si">
                                            <label class="custom-control-label" for="linea_si" onclick="showControll('linea','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="linea" value="si" id="linea_si">
                                            <label class="custom-control-label" for="linea_si" onclick="showControll('linea','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['linea'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="linea" value="no" id="linea_no">
                                            <label class="custom-control-label" for="linea_no" onclick="hideControll('linea','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="linea" value="no" id="linea_no">
                                            <label class="custom-control-label" for="linea_no" onclick="hideControll('linea','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para
                                                trabajos en
                                                alturas (
                                                SATISFACTORIO)</b></label>
                                        @if (isset($historial) && $historial['admision_con']['t_altura'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="t_altura" value="si" id="t_altura_si">
                                            <label class="custom-control-label" for="t_altura_si" onclick="showControll('t_altura','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="t_altura" value="si" id="t_altura_si">
                                            <label class="custom-control-label" for="t_altura_si" onclick="showControll('t_altura','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['t_altura'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="t_altura" value="no" id="t_altura_no">
                                            <label class="custom-control-label" for="t_altura_no" onclick="hideControll('t_altura','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="t_altura" value="no" id="t_altura_no">
                                            <label class="custom-control-label" for="t_altura_no" onclick="hideControll('t_altura','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-12">
                <fieldset class="form-group">
                    <h5 class="primary text-center">TRABAJOS EN ESPACIOS CONFINADOS</h5>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Cuando se encuentra en espacios cerrados, ¿se
                                            desespera?</label>
                                        @if (isset($historial) && $historial['admision_con']['e_cerrados'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_cerrados" value="si" id="e_cerrados_si">
                                            <label class="custom-control-label" for="e_cerrados_si" onclick="showControll('e_cerrados','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_cerrados" value="si" id="e_cerrados_si">
                                            <label class="custom-control-label" for="e_cerrados_si" onclick="showControll('e_cerrados','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['e_cerrados'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_cerrados" value="no" id="e_cerrados_no">
                                            <label class="custom-control-label" for="e_cerrados_no" onclick="hideControll('e_cerrados','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_cerrados" value="no" id="e_cerrados_no">
                                            <label class="custom-control-label" for="e_cerrados_no" onclick="hideControll('e_cerrados','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Tiene miedos (fobias) para trabajar en lugares
                                            cerrados, estrechos, con poca
                                            iluminación y poco aire?</label>
                                        @if (isset($historial) && $historial['admision_con']['fobias'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="fobias" value="si" id="fobias_si">
                                            <label class="custom-control-label" for="fobias_si" onclick="showControll('fobias','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="fobias" value="si" id="fobias_si">
                                            <label class="custom-control-label" for="fobias_si" onclick="showControll('fobias','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['fobias'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="fobias" value="no" id="fobias_no">
                                            <label class="custom-control-label" for="fobias_no" onclick="hideControll('fobias','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="fobias" value="no" id="fobias_no">
                                            <label class="custom-control-label" for="fobias_no" onclick="hideControll('fobias','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Cuando se encuentra en espacios estrechos,
                                            ¿siente que no puede respirar
                                            bien?</label>
                                        @if (isset($historial) && $historial['admision_con']['e_estrechos'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_estrechos" value="si" id="e_estrechos_si">
                                            <label class="custom-control-label" for="e_estrechos_si" onclick="showControll('e_estrechos','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_estrechos" value="si" id="e_estrechos_si">
                                            <label class="custom-control-label" for="e_estrechos_si" onclick="showControll('e_estrechos','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['e_estrechos'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_estrechos" value="no" id="e_estrechos_no">
                                            <label class="custom-control-label" for="e_estrechos_no" onclick="hideControll('e_estrechos','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_estrechos" value="no" id="e_estrechos_no">
                                            <label class="custom-control-label" for="e_estrechos_no" onclick="hideControll('e_estrechos','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para
                                                trabajos en espacios confinados
                                                (SATISFACTORIO)</b></label>
                                        @if (isset($historial) && $historial['admision_con']['e_confinados'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_confinados" value="si" id="e_confinados_si">
                                            <label class="custom-control-label" for="e_confinados_si" onclick="showControll('e_confinados','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_confinados" value="si" id="e_confinados_si">
                                            <label class="custom-control-label" for="e_confinados_si" onclick="showControll('e_confinados','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['e_confinados'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_confinados" value="no" id="e_confinados_no">
                                            <label class="custom-control-label" for="e_confinados_no" onclick="hideControll('e_confinados','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_confinados" value="no" id="e_confinados_no">
                                            <label class="custom-control-label" for="e_confinados_no" onclick="hideControll('e_confinados','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-md-12">
                <fieldset class="form-group">
                    <h5 class="primary text-center">TRABAJOS DE LEVANTAMIENTO Y MOVIMIENTO DE CARGAS</h5>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha tenido cirugías en su espalda (columna
                                            vertebral)?</label>
                                        @if (isset($historial) && $historial['admision_con']['c_vertebral'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="c_vertebral" value="si" id="c_vertebral_si">
                                            <label class="custom-control-label" for="c_vertebral_si" onclick="showControll('c_vertebral','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="c_vertebral" value="si" id="c_vertebral_si">
                                            <label class="custom-control-label" for="c_vertebral_si" onclick="showControll('c_vertebral','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['c_vertebral'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="c_vertebral" value="no" id="c_vertebral_no">
                                            <label class="custom-control-label" for="c_vertebral_no" onclick="hideControll('c_vertebral','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="c_vertebral" value="no" id="c_vertebral_no">
                                            <label class="custom-control-label" for="c_vertebral_no" onclick="hideControll('c_vertebral','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Se ha fracturado alguna vertebra?</label>
                                        @if (isset($historial) && $historial['admision_con']['f_vertebral'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="f_vertebral" value="si" id="f_vertebral_si">
                                            <label class="custom-control-label" for="f_vertebral_si" onclick="showControll('f_vertebral','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="f_vertebral" value="si" id="f_vertebral_si">
                                            <label class="custom-control-label" for="f_vertebral_si" onclick="showControll('f_vertebral','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['f_vertebral'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="f_vertebral" value="no" id="f_vertebral_no">
                                            <label class="custom-control-label" for="f_vertebral_no" onclick="hideControll('f_vertebral','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="f_vertebral" value="no" id="f_vertebral_no">
                                            <label class="custom-control-label" for="f_vertebral_no" onclick="hideControll('f_vertebral','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha padecido o padece de dolores de
                                            espalda?</label>
                                        @if (isset($historial) && $historial['admision_con']['d_espalda'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="d_espalda" value="si" id="d_espalda_si">
                                            <label class="custom-control-label" for="d_espalda_si" onclick="showControll('d_espalda','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="d_espalda" value="si" id="d_espalda_si">
                                            <label class="custom-control-label" for="d_espalda_si" onclick="showControll('d_espalda','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['d_espalda'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="d_espalda" value="no" id="d_espalda_no">
                                            <label class="custom-control-label" for="d_espalda_no" onclick="hideControll('d_espalda','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="d_espalda" value="no" id="d_espalda_no">
                                            <label class="custom-control-label" for="d_espalda_no" onclick="hideControll('d_espalda','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha tenido dolores de la ciática?</label>
                                        @if (isset($historial) && $historial['admision_con']['d_ciatica'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="d_ciatica" value="si" id="d_ciatica_si">
                                            <label class="custom-control-label" for="d_ciatica_si" onclick="showControll('d_ciatica','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="d_ciatica" value="si" id="d_ciatica_si">
                                            <label class="custom-control-label" for="d_ciatica_si" onclick="showControll('d_ciatica','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['d_ciatica'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="d_ciatica" value="no" id="d_ciatica_no">
                                            <label class="custom-control-label" for="d_ciatica_no" onclick="hideControll('d_ciatica','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="d_ciatica" value="no" id="d_ciatica_no">
                                            <label class="custom-control-label" for="d_ciatica_no" onclick="hideControll('d_ciatica','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Se le entumen sus brazos o piernas sin razón
                                            aparente?</label>
                                        @if (isset($historial) && $historial['admision_con']['entumen_pb'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="entumen_pb" value="si" id="entumen_pb_si">
                                            <label class="custom-control-label" for="entumen_pb_si" onclick="showControll('entumen_pb','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="entumen_pb" value="si" id="entumen_pb_si">
                                            <label class="custom-control-label" for="entumen_pb_si" onclick="showControll('entumen_pb','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['entumen_pb'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="entumen_pb" value="no" id="entumen_pb_no">
                                            <label class="custom-control-label" for="entumen_pb_no" onclick="hideControll('entumen_pb','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="entumen_pb" value="no" id="entumen_pb_no">
                                            <label class="custom-control-label" for="entumen_pb_no" onclick="hideControll('entumen_pb','show')">no</label>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Tiene alguna limitación de movimiento de brazos,
                                            piernas o espalda?</label>
                                        @if (isset($historial) && $historial['admision_con']['limitacion_bpe'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="limitacion_bpe" value="si" id="limitacion_bpe_si">
                                            <label class="custom-control-label" for="limitacion_bpe_si" onclick="showControll('limitacion_bpe','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="limitacion_bpe" value="si" id="limitacion_bpe_si">
                                            <label class="custom-control-label" for="limitacion_bpe_si" onclick="showControll('limitacion_bpe','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['limitacion_bpe'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="limitacion_bpe" value="no" id="limitacion_bpe_no">
                                            <label class="custom-control-label" for="limitacion_bpe_no" onclick="hideControll('limitacion_bpe','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="limitacion_bpe" value="no" id="limitacion_bpe_no">
                                            <label class="custom-control-label" for="limitacion_bpe_no" onclick="hideControll('limitacion_bpe','show')">no</label>
                                        </div>

                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Tiene algún miembro inferior (pierna) más corto
                                            que el otro?</label>
                                        @if (isset($historial) && $historial['admision_con']['m_inferior'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="m_inferior" value="si" id="m_inferior_si">
                                            <label class="custom-control-label" for="m_inferior_si" onclick="showControll('m_inferior','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="m_inferior" value="si" id="m_inferior_si">
                                            <label class="custom-control-label" for="m_inferior_si" onclick="showControll('m_inferior','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['m_inferior'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="m_inferior" value="no" id="m_inferior_no">
                                            <label class="custom-control-label" for="m_inferior_no" onclick="hideControll('m_inferior','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="m_inferior" value="no" id="m_inferior_no">
                                            <label class="custom-control-label" for="m_inferior_no" onclick="hideControll('m_inferior','show')">no</label>
                                        </div>
                                        @endif

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label><b class="secondary">** Exploración médica</b></label><br>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de curvaturas fisiológicas de
                                            columna?</label>
                                        @if (isset($historial) && $historial['admision_con']['fisiologicas'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="fisiologicas" value="si" id="fisiologicas_si">
                                            <label class="custom-control-label" for="fisiologicas_si" onclick="showControll('fisiologicas','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="fisiologicas" value="si" id="fisiologicas_si">
                                            <label class="custom-control-label" for="fisiologicas_si" onclick="showControll('fisiologicas','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['fisiologicas'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="fisiologicas" value="no" id="fisiologicas_no">
                                            <label class="custom-control-label" for="fisiologicas_no" onclick="hideControll('fisiologicas','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="fisiologicas" value="no" id="fisiologicas_no">
                                            <label class="custom-control-label" for="fisiologicas_no" onclick="hideControll('fisiologicas','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Marcha punta - talón</label>
                                        @if (isset($historial) && $historial['admision_con']['punta_t'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="punta_t" value="si" id="punta_t_si">
                                            <label class="custom-control-label" for="punta_t_si" onclick="showControll('punta_t','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="punta_t" value="si" id="punta_t_si">
                                            <label class="custom-control-label" for="punta_t_si" onclick="showControll('punta_t','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['punta_t'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="punta_t" value="no" id="punta_t_no">
                                            <label class="custom-control-label" for="punta_t_no" onclick="hideControll('punta_t','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="punta_t" value="no" id="punta_t_no">
                                            <label class="custom-control-label" for="punta_t_no" onclick="hideControll('punta_t','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Palpación de apófisis espinosas (cervicales,
                                            dorsales y lumbares)</label>
                                        @if (isset($historial) && $historial['admision_con']['apofisis'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="apofisis" value="si" id="apofisis_si">
                                            <label class="custom-control-label" for="apofisis_si" onclick="showControll('apofisis','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="apofisis" value="si" id="apofisis_si">
                                            <label class="custom-control-label" for="apofisis_si" onclick="showControll('apofisis','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['apofisis'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="apofisis" value="no" id="apofisis_no">
                                            <label class="custom-control-label" for="apofisis_no" onclick="hideControll('apofisis','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="apofisis" value="no" id="apofisis_no">
                                            <label class="custom-control-label" for="apofisis_no" onclick="hideControll('apofisis','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión (presión) de articulaciones
                                            sacroilíacas</label>
                                        @if (isset($historial) && $historial['admision_con']['sacroiliacas'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="sacroiliacas" value="si" id="sacroiliacas_si">
                                            <label class="custom-control-label" for="sacroiliacas_si" onclick="showControll('sacroiliacas','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="sacroiliacas" value="si" id="sacroiliacas_si">
                                            <label class="custom-control-label" for="sacroiliacas_si" onclick="showControll('sacroiliacas','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['sacroiliacas'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="sacroiliacas" value="no" id="sacroiliacas_no">
                                            <label class="custom-control-label" for="sacroiliacas_no" onclick="hideControll('sacroiliacas','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="sacroiliacas" value="no" id="sacroiliacas_no">
                                            <label class="custom-control-label" for="sacroiliacas_no" onclick="hideControll('sacroiliacas','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión completa de arcos de movimiento de
                                            columna, miembros inferiores y
                                            superiores.</label>
                                        @if (isset($historial) && $historial['admision_con']['m_is'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="m_is" value="si" id="m_is_si">
                                            <label class="custom-control-label" for="m_is_si" onclick="showControll('m_is','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="m_is" value="si" id="m_is_si">
                                            <label class="custom-control-label" for="m_is_si" onclick="showControll('m_is','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['m_is'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="m_is" value="no" id="m_is_no">
                                            <label class="custom-control-label" for="m_is_no" onclick="hideControll('m_is','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="m_is" value="no" id="m_is_no">
                                            <label class="custom-control-label" for="m_is_no" onclick="hideControll('m_is','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de signo Lasague y Lasague en dos
                                            tiempos.</label>
                                        @if (isset($historial) && $historial['admision_con']['Lasague'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="Lasague" value="si" id="Lasague_si">
                                            <label class="custom-control-label" for="Lasague_si" onclick="showControll('Lasague','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="Lasague" value="si" id="Lasague_si">
                                            <label class="custom-control-label" for="Lasague_si" onclick="showControll('Lasague','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['Lasague'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="Lasague" checked value="no" id="Lasague_no">
                                            <label class="custom-control-label" for="Lasague_no" onclick="hideControll('Lasague','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="Lasague" value="no" id="Lasague_no">
                                            <label class="custom-control-label" for="Lasague_no" onclick="hideControll('Lasague','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para
                                                trabajos donde tenga que
                                                levantar o empujar más de 25 kgs.(SATISFACTORIO).</b></label>
                                        @if (isset($historial) && $historial['admision_con']['reque25'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="reque25" value="si" id="reque25_si">
                                            <label class="custom-control-label" for="reque25_si" onclick="showControll('reque25','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="reque25" value="si" id="reque25_si">
                                            <label class="custom-control-label" for="reque25_si" onclick="showControll('reque25','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['reque25'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="reque25" checked value="no" id="reque25_no">
                                            <label class="custom-control-label" for="reque25_no" onclick="hideControll('reque25','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="reque25" value="no" id="reque25_no">
                                            <label class="custom-control-label" for="reque25_no" onclick="hideControll('reque25','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label><b class="secondary text-center">FUNCIÓN PULMONAR</b></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Le ha faltado respiración o ha sentido opresión
                                            en el pecho?</label>
                                        @if (isset($historial) && $historial['admision_con']['opresion'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="opresion" value="si" id="opresion_si">
                                            <label class="custom-control-label" for="opresion_si" onclick="showControll('opresion','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="opresion" value="si" id="opresion_si">
                                            <label class="custom-control-label" for="opresion_si" onclick="showControll('opresion','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['opresion'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="opresion" value="no" id="opresion_no">
                                            <label class="custom-control-label" for="opresion_no" onclick="hideControll('opresion','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="opresion" value="no" id="opresion_no">
                                            <label class="custom-control-label" for="opresion_no" onclick="hideControll('opresion','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha tosido por las mañanas en los últimos 12
                                            meses?</label>
                                        @if (isset($historial) && $historial['admision_con']['tosido'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="tosido" value="si" id="tosido_si">
                                            <label class="custom-control-label" for="tosido_si" onclick="showControll('tosido','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="tosido" value="si" id="tosido_si">
                                            <label class="custom-control-label" for="tosido_si" onclick="showControll('tosido','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['tosido'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="tosido" checked value="no" id="tosido_no">
                                            <label class="custom-control-label" for="tosido_no" onclick="hideControll('tosido','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="tosido" value="no" id="tosido_no">
                                            <label class="custom-control-label" for="tosido_no" onclick="hideControll('tosido','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block"><b>Cumple con los requerimientos médicos para
                                                trabajos donde esté expuesto a
                                                polvos o humos</b></label>
                                        @if (isset($historial) && $historial['admision_con']['polvos_humos'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="polvos_humos" value="si" id="polvos_humos_si">
                                            <label class="custom-control-label" for="polvos_humos_si" onclick="showControll('polvos_humos','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="polvos_humos" value="si" id="polvos_humos_si">
                                            <label class="custom-control-label" for="polvos_humos_si" onclick="showControll('polvos_humos','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['polvos_humos'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="polvos_humos" value="no" id="polvos_humos_no">
                                            <label class="custom-control-label" for="polvos_humos_no" onclick="hideControll('polvos_humos','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="polvos_humos" value="no" id="polvos_humos_no">
                                            <label class="custom-control-label" for="polvos_humos_no" onclick="hideControll('polvos_humos','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <h5 class="primary text-center">AUDITIVA</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha notado recientemente cambios en su capacidad
                                            auditiva?</label>
                                        @if (isset($historial) && $historial['admision_con']['c_auditiva'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="c_auditiva" value="si" id="c_auditiva_si">
                                            <label class="custom-control-label" for="c_auditiva_si" onclick="showControll('c_auditiva','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="c_auditiva" value="si" id="c_auditiva_si">
                                            <label class="custom-control-label" for="c_auditiva_si" onclick="showControll('c_auditiva','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['c_auditiva'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="c_auditiva" value="no" id="c_auditiva_no">
                                            <label class="custom-control-label" for="c_auditiva_no" onclick="hideControll('c_auditiva','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="c_auditiva" value="no" id="c_auditiva_no">
                                            <label class="custom-control-label" for="c_auditiva_no" onclick="hideControll('c_auditiva','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Ha notado zumbidos o pitidos en sus
                                            oídos?</label>
                                        @if (isset($historial) && $historial['admision_con']['o_zumbidos'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="o_zumbidos" value="si" id="o_zumbidos_si">
                                            <label class="custom-control-label" for="o_zumbidos_si" onclick="showControll('o_zumbidos','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="o_zumbidos" value="si" id="o_zumbidos_si">
                                            <label class="custom-control-label" for="o_zumbidos_si" onclick="showControll('o_zumbidos','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['o_zumbidos'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="o_zumbidos" value="no" id="o_zumbidos_no">
                                            <label class="custom-control-label" for="o_zumbidos_no" onclick="hideControll('o_zumbidos','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="o_zumbidos" value="no" id="o_zumbidos_no">
                                            <label class="custom-control-label" for="o_zumbidos_no" onclick="hideControll('o_zumbidos','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">¿Le resulta difícil participar en una
                                            conversación?</label>
                                        @if (isset($historial) && $historial['admision_con']['conversacion'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="conversacion" value="si" id="conversacion_si">
                                            <label class="custom-control-label" for="conversacion_si" onclick="showControll('conversacion','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="conversacion" value="si" id="conversacion_si">
                                            <label class="custom-control-label" for="conversacion_si" onclick="showControll('conversacion','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['conversacion'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="conversacion" value="no" id="conversacion_no">
                                            <label class="custom-control-label" for="conversacion_no" onclick="hideControll('conversacion','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="conversacion" value="no" id="conversacion_no">
                                            <label class="custom-control-label" for="conversacion_no" onclick="hideControll('conversacion','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <label><b class="secondary">** Exploración médica</b></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de conducto auditivo, buscando
                                            perforación timpánica</label>
                                        @if (isset($historial) && $historial['admision_con']['timpanica'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="timpanica" value="si" id="timpanica_si">
                                            <label class="custom-control-label" for="timpanica_si" onclick="showControll('timpanica','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="timpanica" value="si" id="timpanica_si">
                                            <label class="custom-control-label" for="timpanica_si" onclick="showControll('timpanica','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['timpanica'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="timpanica" value="no" id="timpanica_no">
                                            <label class="custom-control-label" for="timpanica_no" onclick="hideControll('timpanica','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="timpanica" value="no" id="timpanica_no">
                                            <label class="custom-control-label" for="timpanica_no" onclick="hideControll('timpanica','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Revisión de conducto auditivo, buscando tapón de
                                            cerumen o infección.</label>
                                        @if (isset($historial) && $historial['admision_con']['cerumen'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="cerumen" value="si" id="cerumen_si">
                                            <label class="custom-control-label" for="cerumen_si" onclick="showControll('cerumen','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="cerumen" value="si" id="cerumen_si">
                                            <label class="custom-control-label" for="cerumen_si" onclick="showControll('cerumen','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['cerumen'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="cerumen" checked value="no" id="cerumen_no">
                                            <label class="custom-control-label" for="cerumen_no" onclick="hideControll('cerumen','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="cerumen" value="no" id="cerumen_no">
                                            <label class="custom-control-label" for="cerumen_no" onclick="hideControll('cerumen','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block">Cumple con los requerimientos médicos para
                                            trabajos con exposición a ruido
                                            (SATISFACTORIO)</label>
                                        @if (isset($historial) && $historial['admision_con']['e_ruido'] == 'si')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" checked name="e_ruido" value="si" id="e_ruido_si">
                                            <label class="custom-control-label" for="e_ruido_si" onclick="showControll('e_ruido','show')">si</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_ruido" value="si" id="e_ruido_si">
                                            <label class="custom-control-label" for="e_ruido_si" onclick="showControll('e_ruido','show')">si</label>
                                        </div>
                                        @endif
                                        @if (isset($historial) && $historial['admision_con']['e_ruido'] == 'no')
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_ruido" checked value="no" id="e_ruido_no">
                                            <label class="custom-control-label" for="e_ruido_no" onclick="hideControll('e_ruido','show')">no</label>
                                        </div>
                                        @else
                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                            <input type="radio" class="custom-control-input bg-secondary" name="e_ruido" value="no" id="e_ruido_no">
                                            <label class="custom-control-label" for="e_ruido_no" onclick="hideControll('e_ruido','show')">no</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <div id="examenhistorial">
        <div class="row" id="">
            <label class="col-md-12 text-center">SIGNOS VITALES:</label>
            <hr>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">T/A:</label>
                    <input type="text" class="form-control" id="TAHC" name="TAHC" placeholder="T/A" value="@if(!empty($historial['historialfisico']['TAHC'])){{ $historial['historialfisico']['TAHC'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">FC:</label>
                    <input type="text" class="form-control" id="FCHC" name="FCHC" placeholder="FC" value="@if(!empty($historial['historialfisico']['FCHC'])){{ $historial['historialfisico']['FCHC'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">FR:</label>
                    <input type="text" class="form-control" id="FRHC" name="FRHC" placeholder="FR" value="@if(!empty($historial['historialfisico']['FRHC'])){{ $historial['historialfisico']['FRHC'] }}@endif">
                </fieldset>
            </div> 
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">°T:</label>
                    <input type="text" class="form-control" id="THC" name="THC" placeholder="T" value="@if(!empty($historial['historialfisico']['THC'])){{ $historial['historialfisico']['THC'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">PESO:</label>
                    <input type="text" class="form-control" id="PESO" name="PESO" placeholder="PESO" value="@if(!empty($historial['historialfisico']['PESO'])){{ $historial['historialfisico']['PESO'] }}@endif">
                </fieldset>
            </div>  
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">TALLA:</label>
                    <input type="text" class="form-control" id="TALLAHC" name="TALLAHC" placeholder="TALLA" value="@if(!empty($historial['historialfisico']['TALLAHC'])){{ $historial['historialfisico']['TALLAHC'] }}@endif">
                </fieldset>
            </div> 
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">IMC:</label>
                    <input type="text" class="form-control" id="IMChc" name="IMChc" placeholder="IMC" value="@if(!empty($historial['historialfisico']['IMChc'])){{ $historial['historialfisico']['IMChc'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">PESO IDEAL:</label>
                    <input type="text" class="form-control" id="ideal" name="ideal" placeholder="Peso ideal" value="@if(!empty($historial['historialfisico']['ideal'])){{ $historial['historialfisico']['ideal'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">CINTURA:</label>
                    <input type="text" class="form-control" id="cintura" name="cintura" placeholder="Cintura" value="@if(!empty($historial['historialfisico']['cintura'])){{ $historial['historialfisico']['cintura'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">GRASA CORPORAL:</label>
                    <input type="text" class="form-control" id="CORPORAL" name="CORPORAL" placeholder="GRASA CORPORAL" value="@if(!empty($historial['historialfisico']['CORPORAL'])){{ $historial['historialfisico']['CORPORAL'] }}@endif">
                </fieldset>
            </div>
            <label class="col-md-12 text-center">AGUDEZA VISUAL SIN LENTES</label>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">OJO DER:</label>
                    <input type="text" class="form-control" id="ODSL" name="ODSL" placeholder="" value="@if(!empty($historial['historialfisico']['ODSL'])){{ $historial['historialfisico']['ODSL'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">OJO IZQ.:</label>
                    <input type="text" class="form-control" id="OISL" name="OISL" placeholder="" value="@if(!empty($historial['historialfisico']['OISL'])){{ $historial['historialfisico']['OISL'] }}@endif">
                </fieldset>
            </div>
            <label class="col-md-12 text-center">AGUDEZA VISUAL CON LENTES</label>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">OJO DER:</label>
                    <input type="text" class="form-control" id="ODCL" name="ODCL" placeholder="" value="@if(!empty($historial['historialfisico']['ODCL'])){{ $historial['historialfisico']['ODCL'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">OJO IZQ.:</label>
                    <input type="text" class="form-control" id="OICL" name="OICL" placeholder="" value="@if(!empty($historial['historialfisico']['OICL'])){{ $historial['historialfisico']['OICL'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">DESTROSTIX:</label>
                    <input type="text" class="form-control" id="DESTROSTIX" name="DESTROSTIX" placeholder="DESTROSTIX" value="@if(!empty($historial['historialfisico']['DESTROSTIX'])){{ $historial['historialfisico']['DESTROSTIX'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">HABITUS EXTERIOR:</label>
                    <input type="text" class="form-control" id="HABITUS" name="HABITUS" placeholder="" value="@if(!empty($historial['historialfisico']['HABITUS'])){{ $historial['historialfisico']['HABITUS'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">CABEZA Y CUELLO:</label>
                    <input type="text" class="form-control" id="CUELLO" name="CUELLO" placeholder="" value="@if(!empty($historial['historialfisico']['CUELLO'])){{ $historial['historialfisico']['CUELLO'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">TORAX:</label>
                    <input type="text" class="form-control" id="TORAX" name="TORAX" placeholder="" value="@if(!empty($historial['historialfisico']['TORAX'])){{ $historial['historialfisico']['TORAX'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">ABDOMEN:</label>
                    <input type="text" class="form-control" id="ABDOMEN" name="ABDOMEN" placeholder="" value="@if(!empty($historial['historialfisico']['ABDOMEN'])){{ $historial['historialfisico']['ABDOMEN'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">MIEM.PELVICOS:</label>
                    <input type="text" class="form-control" id="PELVICOS" name="PELVICOS" placeholder="" value="@if(!empty($historial['historialfisico']['PELVICOS'])){{ $historial['historialfisico']['PELVICOS'] }}@endif">
                </fieldset>
            </div>
            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="apariencia">EXTREMIDADES:</label>
                    <input type="text" class="form-control" id="EXTREMIDADES" name="EXTREMIDADES" placeholder="" value="@if(!empty($historial['historialfisico']['EXTREMIDADES'])){{ $historial['historialfisico']['EXTREMIDADES'] }}@endif">
                </fieldset>
            </div>
        </div>
    </div>
</fieldset>