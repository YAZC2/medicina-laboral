<fieldset>
    <div id="somnolencia">
        <div class="row">
            <div class="col-md-12">
        <h5>Escala de Somnolencia de Epworth</h5>
      
        <p>Escala de sueño de Epworth</p>
        <p><b>PREGUNTA</b>¿Con que frecuencia se queda Ud. dormido en las siguientes situaciones?.
            Incluso si no ha realizado recientemente alguna de las actividades mencionadas a continuación,trate de imaginar en que medida le afectarían.</p>
        <p>Utilice la siguiente escala y elija la cifra adecuada par cada situación:</p>
        <ul>
          <li>0 = Nunca se ha dormido</li>
          <li>1 = Escasa posibilidad de dormirse</li>
          <li>2 = Moderada posibilidad de dormirse</li>
          <li>3 = Elevada posibilidad de dormirse</li>
        </ul>
           
        
        <div class="row">
            
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8">
                        <h5>Situación</h5>
                    </div>
                    <div class="col-md-4">
                        <h5>Puntuación</h5>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>Sentado y leyendo:</p>
                        
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="sentado_leyendo">
                            @if(isset($historial) && empty($historial['somnolencia']['sentado_leyendo']))
                                {{ $historial['somnolencia']['sentado_leyendo'] }}
                            @else
                                <option value="6">Seleccione una puntuación</option>
                            @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>Viendo la T.V.</p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="tv">
                        @if(isset($historial) && empty($historial['somnolencia']['tv']))
                                {{ $historial['somnolencia']['tv'] }}
                            @else
                                <option value="6">Seleccione una puntuación</option>
                            @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>Sentado,inactivo en un espectaculo(teatro) </p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="teatro">
                        @if(isset($historial) && empty($historial['somnolencia']['teatro']))
                            {{ $historial['somnolencia']['teatro'] }}
                        @else
                            <option value="6">Seleccione una puntuación</option>
                        @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>En auto,como copiloto de un viaje de una hora </p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="auto_copiloto">
                        @if(isset($historial) && empty($historial['somnolencia']['auto_copiloto']))
                            {{ $historial['somnolencia']['auto_copiloto'] }}
                        @else
                            <option value="6">Seleccione una puntuación</option>
                        @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>Recostado a media tarde </p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="recostado">
                        @if(isset($historial) && empty($historial['somnolencia']['recostado']))
                            {{ $historial['somnolencia']['recostado'] }}
                        @else
                            <option value="6">Seleccione una puntuación</option>
                        @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>Sentado y conversando con alguien</p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="conversando">
                        @if(isset($historial) && empty($historial['somnolencia']['conversando']))
                        {{ $historial['somnolencia']['conversando'] }}
                        @else
                            <option value="6">Seleccione una puntuación</option>
                        @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>Sentado despues de la comida(sin toma alcohol)</p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="sentado_comida">
                        @if(isset($historial) && empty($historial['somnolencia']['sentado_comida']))
                        {{ $historial['somnolencia']['sentado_comida'] }}
                        @else
                            <option value="6">Seleccione una puntuación</option>
                        @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
                <div class="row mb-1">
                    <div class="col-md-8">
                        <p>En su auto cuando se para durante algunos minutos debido al trafico</p>
                    </div>
                   <div class="col-md-4">
                    <select class="form-control" name="trafico">
                        @if(isset($historial) && empty($historial['somnolencia']['trafico']))
                        {{ $historial['somnolencia']['trafico'] }}
                        @else
                            <option value="6">Seleccione una puntuación</option>
                        @endif
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                   </div>
                
                </div>
            </div>
            
            
        </div>
        
    </div>
        </div>
     
</fieldset>