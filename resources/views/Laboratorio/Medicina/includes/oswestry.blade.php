<fieldset>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <p>1.-INTENSIDAD DEL DOLOR</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry1']=='5')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry1"
                                value="5" id="oswestry15">
                            <label class="custom-control-label" for="oswestry15">Actualmente no tengo dolor de columna ni
                                de
                                pierna.</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry1" value="5"
                                id="oswestry15">
                            <label class="custom-control-label" for="oswestry15">Actualmente no tengo dolor de columna ni
                                de
                                pierna.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry1']=='4')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry1"
                                value="4" id="oswestry14">
                            <label class="custom-control-label" for="oswestry14">Mi dolor de columna o pierna es muy
                                leve en este momento.</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry1" value="4"
                                id="oswestry14">
                            <label class="custom-control-label" for="oswestry14">Mi dolor de columna o pierna es muy
                                leve en este momento.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry1']=='3')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry1"
                                value="3" id="oswestry13">
                            <label class="custom-control-label" for="oswestry13">Mi dolor de columna o pierna es moderado
                                en este momento.</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry1" value="3"
                                id="oswestry13">
                            <label class="custom-control-label" for="oswestry13">Mi dolor de columna o pierna es moderado
                                en este momento.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry1']=='2')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry1"
                                value="2" id="oswestry12">
                            <label class="custom-control-label" for="oswestry12">Mi dolor de columna o pierna es intenso
                                en
                                este momento.</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry1" value="2"
                                id="oswestry12">
                            <label class="custom-control-label" for="oswestry12">Mi dolor de columna o pierna es intenso
                                en
                                este momento.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry1']=='1')
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry1"
                                value="1" id="oswestry11">
                            <label class="custom-control-label" for="oswestry11">Mi dolor es el peor imaginable en este
                                momento.</label>
                        </div>
                        @else
                        <div class="d-inline-block custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry1" value="1"
                                id="oswestry11">
                            <label class="custom-control-label" for="oswestry11">Mi dolor es el peor imaginable en este
                                momento.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>2.-ACTIVIDADES DE LA VIDA COTIDIANA(Lavarse,Vestirse,Etc)</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry2'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry2"
                                value="5" id="oswestry25">
                            <label class="custom-control-label" for="oswestry25">Las realizo sin ningpun problema</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry2" value="5"
                                id="oswestry25">
                            <label class="custom-control-label" for="oswestry25">Las realizo sin ningpun problema</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry2'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry2"
                                value="4" id="oswestry24">
                            <label class="custom-control-label" for="oswestry24">Puedo hacer de todo solo y en forma
                                normal, pero con dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry2" value="4"
                                id="oswestry24">
                            <label class="custom-control-label" for="oswestry24">Puedo hacer de todo solo y en forma
                                normal, pero con dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry2'] == '3')

                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry2"
                                value="3" id="oswestry23">
                            <label class="custom-control-label" for="oswestry23">Las realizo en forma más lenta y
                                cuidadosa por el dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry2" value="3"
                                id="oswestry23">
                            <label class="custom-control-label" for="oswestry23">Las realizo en forma más lenta y
                                cuidadosa por el dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry2'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry2"
                                value="2" id="oswestry22">
                            <label class="custom-control-label" for="oswestry22">Ocasionalmente requiero ayuda.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry2" value="2"
                                id="oswestry22">
                            <label class="custom-control-label" for="oswestry22">Ocasionalmente requiero ayuda.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry2'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry2"
                                value="1" id="oswestry21">
                            <label class="custom-control-label" for="oswestry21">Requiero ayuda a diario.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry2" value="1"
                                id="oswestry21">
                            <label class="custom-control-label" for="oswestry21">Requiero ayuda a diario.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>3.-LEVANTAR OBJETOS</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry3'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry3"
                                value="6" id="oswestry36">
                            <label class="custom-control-label" for="oswestry36">Puedo levantar objetos pesados desde el
                                suelo sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry3" value="6"
                                id="oswestry36">
                            <label class="custom-control-label" for="oswestry36">Puedo levantar objetos pesados desde el
                                suelo sin dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry3'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry3"
                                value="5" id="oswestry35">
                            <label class="custom-control-label" for="oswestry35">Puedo levantar objetos pesados desde el
                                suelo pero con dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry3" value="5"
                                id="oswestry35">
                            <label class="custom-control-label" for="oswestry35">Puedo levantar objetos pesados desde el
                                suelo pero con dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry3'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry3"
                                value="4" id="oswestry34">
                            <label class="custom-control-label" for="oswestry34">No puedo levantar objetos pesados del
                                suelo debido al dolor, pero si cargar un objeto pesado desde una mayor altura ,ej. desde
                                una mesa.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry3" value="4"
                                id="oswestry34">
                            <label class="custom-control-label" for="oswestry34">No puedo levantar objetos pesados del
                                suelo debido al dolor, pero si cargar un objeto pesado desde una mayor altura ,ej. desde
                                una mesa.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry3'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry3"
                                value="3" id="oswestry33">
                            <label class="custom-control-label" for="oswestry33">Solo puedo levantar desde el suelo
                                objetos de peso mediano.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry3" value="3"
                                id="oswestry33">
                            <label class="custom-control-label" for="oswestry33">Solo puedo levantar desde el suelo objetos
                                de peso mediano.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry3'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry3"
                                value="2" id="oswestry32">
                            <label class="custom-control-label" for="oswestry32">Solo puedo levantar desde el suelo cosas
                                muy livianas.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry3" value="2"
                                id="oswestry32">
                            <label class="custom-control-label" for="oswestry32">Solo puedo levantar desde el suelo cosas
                                muy livianas.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry3'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry3"
                                value="1" id="oswestry31">
                            <label class="custom-control-label" for="oswestry31">No puedo levantar ni cargar
                                nada.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry3" value="1"
                                id="oswestry3">
                            <label class="custom-control-label" for="oswestry3">No puedo levantar ni cargar
                                nada.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>4.-CAMINAR</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry4'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry4"
                                value="6" id="oswestry46">
                            <label class="custom-control-label" for="oswestry46">Camino todo lo que quiero sin
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry4" value="6"
                                id="oswestry46">
                            <label class="custom-control-label" for="oswestry46">Camino todo lo que quiero sin
                                dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry4'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry4"
                                value="5" id="oswestry45">
                            <label class="custom-control-label" for="oswestry45">No puedo caminar más de 1-2km. debido al
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry4" value="5"
                                id="oswestry45">
                            <label class="custom-control-label" for="oswestry45">No puedo caminar más de 1-2km. debido
                                al dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry4'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry4"
                                value="4" id="oswestry44">
                            <label class="custom-control-label" for="oswestry44">No puedo caminar más de 500-1000mt.
                                debido al dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry4" value="4"
                                id="oswestry44">
                            <label class="custom-control-label" for="oswestry44">No puedo caminar más de 500-1000mt.
                                debido al dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry4'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry4"
                                value="3" id="oswestry43">
                            <label class="custom-control-label" for="oswestry43">No puedo caminar más de 500 mt. debido
                                al dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry4" value="3"
                                id="oswestry43">
                            <label class="custom-control-label" for="oswestry43">No puedo caminar más de 500 mt. debido
                                al dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry4'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry4"
                                value="2" id="oswestry42">
                            <label class="custom-control-label" for="oswestry42">Solo puedo caminar ayudado por uno o dos
                                bastones.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry4" value="2"
                                id="oswestry42">
                            <label class="custom-control-label" for="oswestry42">Solo puedo caminar ayudado por uno o dos
                                bastones.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry4'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry4"
                                value="1" id="oswestry41">
                            <label class="custom-control-label" for="oswestry41">Estoy practicamente en cama, me cuesta
                                mucho hasta ir al baño.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry4" value="1"
                                id="oswestry41">
                            <label class="custom-control-label" for="oswestry41">Estoy practicamente en cama, me cuesta
                                mucho hasta ir al baño.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>5.-SENTARSE</p>
                    <div class="form-group">
           
                        @if (isset($fisioterapia) && $fisioterapia['oswestry5'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry5"
                                value="6" id="oswestry56">
                            <label class="custom-control-label" for="oswestry56">Me puedo sentar en cualquier silla,todo
                                el rato que quiera sin sentir dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry5" value="6"
                                id="oswestry56">
                            <label class="custom-control-label" for="oswestry56">Me puedo sentar en cualquier silla,todo el
                                rato que quiera sin sentir dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry5'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry5"
                                value="5" id="oswestry55">
                            <label class="custom-control-label" for="oswestry55">Solo en un asiento especial puedo
                                sentarme sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry5" value="5"
                                id="oswestry55">
                            <label class="custom-control-label" for="oswestry55">Solo en un asiento especial puedo
                                sentarme sin dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry5'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry5"
                                value="4" id="oswestry54">
                            <label class="custom-control-label" for="oswestry54">No puedo estar sentado más de una hora
                                sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry5" 
                                value="4" id="oswestry54">
                            <label class="custom-control-label" for="oswestry54">No puedo estar sentado más de una hora
                                sin dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry5'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry5"
                                value="3" id="oswestry53">
                            <label class="custom-control-label" for="oswestry53">No puedo estar sentado más de treinta
                                minutos sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry5" value="3"
                                id="oswestry53">
                            <label class="custom-control-label" for="oswestry53">No puedo estar sentado más de treinta
                                minutos sin dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry5'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry5"
                                value="2" id="oswestry52">
                            <label class="custom-control-label" for="oswestry52">No puedo permanecer sentado más de diez
                                minutos sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry5" value="2"
                                id="oswestry52">
                            <label class="custom-control-label" for="oswestry52">No puedo permanecer sentado más de diez
                                minutos sin dolor.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry5'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry5"
                                value="1" id="oswestry51">
                            <label class="custom-control-label" for="oswestry51">No puedo permanecer ningún instante
                                sentado sin que sienta dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry5" value="1"
                                id="oswestry51">
                            <label class="custom-control-label" for="oswestry51">No puedo permanecer ningún instante
                                sentado sin que sienta dolor.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>6.-PARARSE</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry6'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry6"
                                value="6" id="oswestry66">
                            <label class="custom-control-label" for="oswestry66">Puedo permanecer de pie lo que quiero
                                sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry6" value="6"
                                id="oswestry66">
                            <label class="custom-control-label" for="oswestry66">Puedo permanecer de pie lo que quiero
                                sin dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry6'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry6"
                                value="5" id="oswestry65">
                            <label class="custom-control-label" for="oswestry65">Puedo permanecer de pie lo que
                                quiero,aunque con dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry6" value="5"
                                id="oswestry65">
                            <label class="custom-control-label" for="oswestry65">Puedo permanecer de pie lo que
                                quiero,aunque con dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry6'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry6"
                                value="4" id="oswestry64">
                            <label class="custom-control-label" for="oswestry64">No puedo estar más de una hora parado
                                libre de dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry6" value="4"
                                id="oswestry64">
                            <label class="custom-control-label" for="oswestry64">No puedo estar más de una hora parado
                                libre de dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry6'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry6"
                                value="3" id="oswestry63">
                            <label class="custom-control-label" for="oswestry63">No puedo estar para más de treinta minutos
                                libre de dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry6" value="3"
                                id="oswestry63">
                            <label class="custom-control-label" for="oswestry63">No puedo estar parado más de treinta
                                minutos libre de dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry6'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry6"
                                value="2" id="oswestry62">
                            <label class="custom-control-label" for="oswestry62">No puedo permanecer parado más de diez
                                minutos sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry6" value="2"
                                id="oswestry62">
                            <label class="custom-control-label" for="oswestry62">No puedo permanecer parado más de diez
                                minutos sin dolor.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry6'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry6"
                                value="1" id="oswestry61">
                            <label class="custom-control-label" for="oswestry61">No puedo permanecer ningún instante de
                                pie sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry6" value="1"
                                id="oswestry61">
                            <label class="custom-control-label" for="oswestry61">No puedo permanecer ningún instante de
                                pie sin dolor.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>7.-DORMIR</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry7'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry7"
                                value="6" id="oswestry76">
                            <label class="custom-control-label" for="oswestry76">Puedo dormir bien,libre de dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry7" value="6"
                                id="oswestry76">
                            <label class="custom-control-label" for="oswestry76">Puedo dormir bien,libre de
                                dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry7'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry7"
                                value="5" id="oswestry75">
                            <label class="custom-control-label" for="oswestry75">Ocasionalmente el dolor me altera el
                                sueño.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry7" value="5"
                                id="oswestry75">
                            <label class="custom-control-label" for="oswestry75">Ocasionalmente el dolor me altera el
                                sueño.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry7'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry7"
                                value="4" id="oswestry74">
                            <label class="custom-control-label" for="oswestry74">Por el dolor no logro dormir más de 6
                                hrs seguida.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry7"
                                value="4" id="oswestry74">
                            <label class="custom-control-label" for="oswestry74">Por el dolor no logro dormir más de 6
                                hrs seguida.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry7'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry7"
                                value="3" id="oswestry73">
                            <label class="custom-control-label" for="oswestry73">Por el dolor no logro dormir más de 4
                                hrs seguida.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry7" value="3"
                                id="oswestry73">
                            <label class="custom-control-label" for="oswestry73">Por el dolor no logro dormir más de 4
                                hrs seguida.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry7'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry7"
                                value="2" id="oswestry72">
                            <label class="custom-control-label" for="oswestry72">Por el dolor no logro dormir más de 2
                                hrs seguida.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry7" value="2"
                                id="oswestry72">
                            <label class="custom-control-label" for="oswestry72">Por el dolor no logro dormir más de 2 hrs
                                seguida.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry7'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry7"
                                value="1" id="oswestry71">
                            <label class="custom-control-label" for="oswestry71">No logro dormir nada sin dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry7" value="1"
                                id="oswestry71">
                            <label class="custom-control-label" for="oswestry71">No logro dormir nada sin dolor.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>8.-ACTIVIDAD SEXUAL</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry8'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry8"
                                value="6" id="oswestry86">
                            <label class="custom-control-label" for="oswestry86">Normal, sin dolor de columna.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry8" value="6"
                                id="oswestry86">
                            <label class="custom-control-label" for="oswestry86">Normal, sin dolor de columna.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry8'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry8"
                                value="5" id="oswestry85">
                            <label class="custom-control-label" for="oswestry85">Normal, aunque con dolor ocasional de
                                columna.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry8" value="5"
                                id="oswestry85">
                            <label class="custom-control-label" for="oswestry85">Normal, aunque con dolor ocasional de
                                columna.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry8'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry8"
                                value="4" id="oswestry84">
                            <label class="custom-control-label" for="oswestry84">Casi normal pero con importante dolor de
                                columna.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry8" value="4"
                                id="oswestry84">
                            <label class="custom-control-label" for="oswestry84">Casi normal pero con importante dolor de
                                columna.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry8'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry8"
                                value="3" id="oswestry83">
                            <label class="custom-control-label" for="oswestry83">Seriamente limitada por el dolor de
                                columna.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry8" value="3"
                                id="oswestry83">
                            <label class="custom-control-label" for="oswestry83">Seriamente limitada por el dolor de
                                columna.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry8'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry8"
                                value="2" id="oswestry82">
                            <label class="custom-control-label" for="oswestry82">Casi sin actividad,por el dolor de
                                columna.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry8" value="2"
                                id="oswestry82">
                            <label class="custom-control-label" for="oswestry82">Casi sin actividad,por el dolor de
                                columna.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry8'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry8"
                                value="1" id="oswestry81">
                            <label class="custom-control-label" for="oswestry81">Sin actividad,debido a los dolores de
                                columna.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry8" value="1"
                                id="oswestry81">
                            <label class="custom-control-label" for="oswestry81">Sin actividad,debido a los dolores de
                                columna.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>9.-ACTIVIDADES SOCIALES(Fiestas,Deportes,etc)</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry9'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry9"
                                value="6" id="oswestry96">
                            <label class="custom-control-label" for="oswestry96">Sin restricciones, libres de
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry9" value="6"
                                id="oswestry96">
                            <label class="custom-control-label" for="oswestry96">Sin restricciones, libres de
                                dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry9'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry9"
                                value="5" id="oswestry95">
                            <label class="custom-control-label" for="oswestry95">Mi actividad es normal pero aumenta el
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry9" value="5"
                                id="oswestry95">
                            <label class="custom-control-label" for="oswestry95">Mi actividad es normal pero aumenta el
                                dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry9'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry9"
                                value="4" id="oswestry94">
                            <label class="custom-control-label" for="oswestry94">Mi dolor tiene poco impacto en mi
                                actividad social, excepto aquellas más enérgeticas (ej. deportes).</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry9"
                                value="4" id="oswestry94">
                            <label class="custom-control-label" for="oswestry94">Mi dolor tiene poco impacto en mi
                                actividad social, excepto aquellas más enérgeticas (ej. deportes).</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry9'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry9"
                                value="3" id="oswestry93">
                            <label class="custom-control-label" for="oswestry93">Debido al dolor, salgo muy poco.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry9" value="3"
                                id="oswestry93">
                            <label class="custom-control-label" for="oswestry93">Debido al dolor, salgo muy poco.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry9'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry9"
                                value="2" id="oswestry92">
                            <label class="custom-control-label" for="oswestry92">Debido al dolor,no salgo nunca.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry9" value="2"
                                id="oswestry92">
                            <label class="custom-control-label" for="oswestry92">Debido al dolor,no salgo nunca.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry9'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry9"
                                value="1" id="oswestry91">
                            <label class="custom-control-label" for="oswestry91">No hago nada,debido al dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry9" value="1"
                                id="oswestry91">
                            <label class="custom-control-label" for="oswestry91">No hago nada,debido al dolor.</label>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="col-md-6">
                    <p>10.-VIAJAR</p>
                    <div class="form-group">
                        @if (isset($fisioterapia) && $fisioterapia['oswestry10'] == '6')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry10"
                                value="6" id="oswestry106">
                            <label class="custom-control-label" for="oswestry106">Sin problemas,libre de dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry10" value="6"
                                id="oswestry106">
                            <label class="custom-control-label" for="oswestry106">Sin problemas,libre de dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry10'] == '5')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry10"
                                value="5" id="oswestry105">
                            <label class="custom-control-label" for="oswestry105">Sin problemas,pero me produce
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry10" value="5"
                                id="oswestry105">
                            <label class="custom-control-label" for="oswestry105">Sin problemas,pero me produce
                                dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry10'] == '4')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry10"
                                value="4" id="oswestry104">
                            <label class="custom-control-label" for="oswestry10">El dolor es severo,pero logro viajes de
                                2 horas.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry10" 
                                value="4" id="oswestry104">
                            <label class="custom-control-label" for="oswestry104">El dolor es severo,pero logro viajes de
                                2 horas.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry10'] == '3')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry10"
                                value="3" id="oswestry103">
                            <label class="custom-control-label" for="oswestry103">Puedo viajar menos de 1 hr.,por el
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry10" value="3"
                                id="oswestry103">
                            <label class="custom-control-label" for="oswestry103">Puedo viajar menos de 1 hr.,por el
                                dolor.</label>
                        </div>
                        @endif

                        @if (isset($fisioterapia) && $fisioterapia['oswestry10'] == '2')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry10"
                                value="2" id="oswestry102">
                            <label class="custom-control-label" for="oswestry102">Puedo viajar menos de 30 minutos por el
                                dolor.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry10" value="2"
                                id="oswestry102">
                            <label class="custom-control-label" for="oswestry102">Puedo viajar menos de 30 minutos por el
                                dolor.</label>
                        </div>
                        @endif
                        @if (isset($fisioterapia) && $fisioterapia['oswestry10'] == '1')
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" checked class="custom-control-input bg-secondary" name="oswestry10"
                                value="1" id="oswestry101">
                            <label class="custom-control-label" for="oswestry101">Solo viajo para ir al médico o al
                                hospital.</label>
                        </div>
                        @else
                        <div class="custom-control custom-radio mr-1">
                            <input type="radio" class="custom-control-input bg-secondary" name="oswestry10" value="1"
                                id="oswestry101">
                            <label class="custom-control-label" for="oswestry101">Solo viajo para ir al médico o al
                                hospital.</label>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
</fieldset>