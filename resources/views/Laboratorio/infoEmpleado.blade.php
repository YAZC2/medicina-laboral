@extends('layouts.laboratorioApp')

@section('title', $empleado->nombre)

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../../../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/styleScroll.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/Laboratorio/infoEmpleado.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/js/ajax/datatables/datas.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
    <div class="content">
        <section class="messages-alerts">
            @if(session('message'))
                <div class="alert alert-success" >
                {{session('message')}}
                </div>
            @elseif ($errors->any())
                <div class="alert alert-danger" >
                    <p>Ha ocurrido un error al programar los estudios</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </section>

            <div class="seccion-navegacion">
                <p class="navegacion"> <a href="{{route('lab-menu')}}">Inicio</a> / <a href="{{route('lab-infoEmpresa', $nombre_empresa)}}">Empresa</a> / <span>Información del paciente</span> </p>
            </div>
<div class="title-content">
       <h1 class="title">Información del paciente</h1>
   </div>

   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title"></h3>
    </div>
    <div class="panel-body">
      <div class="card mb-3 cardie" >
    <div class="row no-gutters">
      <div class="col-md-4 text-center">
        <img src="{{route('getLogoEmpresa', ['filename'=>$empleado->empresa->logo])}}" alt="Logo de {{ $empleado->empresa->nombre }}" class="rounded-circle img-thumbnail shadow-sm img-sized">
      </div>
      <div class="col-md-8">
        <div class="card-body cardinfo">
          <p><b>Paciente: </b> {{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}</p>
          <p><b>Empresa: </b> {{ $empleado->empresa->nombre }}</p>
        </div>
      </div>
    </div>
  </div>

        <br>

        <div class="card">
  <h5 class="card-header text-center">Estudios Programados</h5>
  <div class="card-body ">
    <form class="col-md-12" role="form" id="registro_entrada" method="post" action="{{route('registrarEntrada')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedID">

        <div class="form-row">
            <div class="form-group col-md-12">


                <div class="estudios-registro custom-checkbox">
                    @foreach ($estudios_programados as $estudio)
                        <div class="col-12 estudios_input">
                            <input type="checkbox" name="estudios_registrar[{{$loop->index}}]" class="custom-control-input" value="{{$estudio->id}}" id="id_estudio_{{ $estudio->id}}" checked>
                            <label class="custom-control-label" for="id_estudio_{{$estudio->id}}">{{ $estudio->nombre}}</label>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-personalizado text-center " id="btn_programar_estudio">Registrar entrada</button>
      </form>
  </div>
</div>
   </div>


    </div>
  </div>
@endsection

@section('specificScripts')
    <script>
        setInterval(function(){
        $('.alert-success').hide(700, "swing");
    },2000);

    setInterval(function(){
        $('.alert-danger').hide(700, "swing");
    },2000);
    </script>
@endsection
