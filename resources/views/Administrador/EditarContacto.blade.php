{{-- @extends('layouts.administradorApp') --}}
@extends('Administrador.AppLayout')


@section('title', 'Editar contacto')

@section('styles')
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"-->
    <link rel="stylesheet" href="../../../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../../../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/styleScroll.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/style_admin_menu.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/Administrador/EditarContacto_styles.css"> --}}
@endsection

@section('page_css')
<link rel="stylesheet" href="https://expedienteclinico.humanly-sw.com/dev/public/css/empresa/info_paciente.css">
    <style>
        .imagen-logo{
            object-fit: cover;height:150px;width:150px;border: none;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="alerts">
                    @if(session('message'))
                        <div class="alert alert-success" >
                        {{session('message')}}
                        </div>
                    @elseif ($errors->any())
                        <div class="alert alert-danger" >
                            <p>Ha ocurrido un error al actualizar los datos del contacto.</p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="titulo">
                    <h1>Edición del contacto de empresa</h1>
                </div>

                <div class="imagen-contacto">
                    <img src="{!! asset('/resources/sass/images/login.png') !!}" alt="Imagen de contacto" class="mr-3 rounded-circle img-thumbnail shadow-sm" width="100resources/sass/images/login.png">
                </div>

                <div class="informacion">
                    <section class="datos-empresa">
                        <h2>{{ $empresa->nombre }}</h2>
                        <p><strong>Dirección:</strong> {{ $empresa->direccion }}</p>
                        <p><strong>Giro:</strong> {{ $empresa->giro['nombre'] }}</p>
                        <p><strong>Teléfono:</strong> {{ $empresa->telefono }}</p>
                        <p><strong>Página web:</strong> {{ $empresa->pagina }}</p>
                    </section>

                    <section class="datos-contacto">
                        <form role="form" id="actualizarContacto" method="post" action="{{ route('actualizarContacto')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedID">

                            <label for="inputNombre">Nombre</label>
                            <input type="text" name="nombre"  class="form-control " value="{{$contacto->nombre}}" id="inputNombre">

                            <label for="inputEmail">Correo electrónico</label>
                            <input type="email" name="email"  class="form-control " value="{{$contacto->email}}" id="inputEmail">

                            <label for="inputTelefono">Teléfono</label>
                            <input type="text" name="telefono"  class="form-control " value="{{$contacto->telefono}}" id="inputTelefono">

                            <label for="inputPuesto">Puesto</label>
                            <input type="text" name="puesto"  class="form-control " value="{{$contacto->puesto}}" id="inputPuesto">

                            <label for="inputArea">Área</label>
                            <input type="text" name="area"  class="form-control " value="{{$contacto->area}}" id="inputArea">

                            <div class="area-guardar mt-2">
                                <button class="btn btn-primary" type="submit">Guardar cambios</button>
                                <a href="{!! url('Empresas/'.$empresa->nombre) !!}" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('specificScripts')
    <script src="../../../resources/js/Administrador/EditarContacto.js"></script>
@endsection
