@extends('Administrador.AppLayout')
@section('title')
Registro de empresa
@endsection

@section('begin_vendor_css')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.css") !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.default.css")
    !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/select2.min.css") !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/extensions/sweetalert.css") !!}">
<!-- END VENDOR CSS-->
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/animate/animate.css") !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/selectize/selectize.css") !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/checkboxes-radios.css") !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/wizard.css") !!}">
<link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/pickers/daterange/daterange.css") !!}">
@endsection
@section('css_custom')
<style>
    .app-content .wizard.wizard-circle>.steps>ul>li:before,
    .app-content .wizard.wizard-circle>.steps>ul>li:after {
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.done .step {
        border-color: #f26b3e;
        background-color: #f26b3e;
    }

    .app-content .wizard>.steps>ul>li.current .step {
        color: #f26b3e;
        border-color: #f26b3e;
    }

    .app-content .wizard>.actions>ul>li>a {
        background-color: #f26b3e;
        border-radius: .4285rem;
    }
</style>
@endsection

@section('content')
<section class="principal">
    <div class='row'>
        <div class="col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-body rounded">
                    <h4 class="card-title primary">Médicina Laboral</h4>
                    <hr style="color:#f26b3e;">
                    <p class="card-text">Estudios de Audiometría,espirometría y electrocardiogramas,realiza historial
                        clínicas.</p>
                    <div class="avatar bg-primary">
                        <span class="avatar-content">
                            <a data-id="12" style="color:#fff;" class="showEstudio" href="T_Empresas">
                                <i class="feather icon-arrow-right"></i>
                            </a>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-body rounded">
                    <h4 class="card-title primary">Imagenología</h4>
                    <hr style="color:#f26b3e;">
                    <p class="card-text">Interpreta estudios,sube imagenes DICOM y asigna a medicos.</p>
                    <div class="avatar bg-primary">
                        <span class="avatar-content">
                            @if(Auth::user()->puesto=='Tecnico Radiologo')
                            <a data-id="12" style="color:#fff;" class="showEstudio" href="https://humanly-sw.com/imagenologia/Has/Ingresa/LUIS ENRIQUE MANCILLA ALFONSO/L108/LIBRES/tecnico_radiologo/1">
                                <i class="feather icon-arrow-right"></i>
                            </a>
                            @else
                                <a data-id="12" style="color:#fff;" class="showEstudio" href="https://humanly-sw.com/imagenologia/Has/Ingresa/JESUS ALEJANDRO TORO HERNANDEZ/T904/NA/medico_radiologo/1">
                                    <i class="feather icon-arrow-right"></i>
                                </a>
                            @endif
                        </span>

                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header bg-primary text-center" style="padding: .6rem !important;">
                    <h6 class="card-title font-weight-light text-white text-center">
                        Imagenología
                    </h6>
                </div>
                <div class="card-body rounded">
                    <p class="card-text">Interpreta estudios,sube imagenes DICOM y asigna a medicos.</p>
                    <div class="avatar bg-secondary">
                        <span class="avatar-content">
                            <a data-id="12" class="showEstudio">
                                <i class="feather icon-arrow-right"></i>
                            </a>
                        </span>

                    </div>
                </div>
            </div>
        </div> --}}
        {{-- <div class='col-md-12'>
            <div class="card" id="empresas">

                <div class="card-content collapse show">
                    <div class="card-header bg-secondary">
                        <h4 class="card-title text-white">Empresas</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            </ul>
                        </div>
                        <hr>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-8 mb-1">
                                <input type="text" class="form-control search input-sm" placeholder="Buscar Empresa">
                            </div>
                            <div class="col-md-4 mb-1">
                                <button class='btn btn-primary waves-effect waves-float waves-light'
                                    id='btnagregar'>Agregar Empresa</button>
                            </div>

                            <div class='col-md-12'>
                                <div class="row list">

                                    @forelse ($empresas as $empresa)

                                    <div class="col-lg-3 ml-md-auto mr-md-auto" style='border-radius: 10px;
                                                        box-shadow: 0 1px 24px 0 rgb(34 41 47 / 17%);'>
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class='row'>
                                                        <button
                                                            class="float-right btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                                            onclick="eliminar({{ $empresa->id }})">
                                                            <i class="feather icon-trash"></i>
                                                        </button>
                                                        <button
                                                            class="float-right btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light mr-1"
                                                            onclick="cargadatos({{ $empresa->id }})">
                                                            <i class="feather icon-edit"></i>
                                                        </button>
                                                    </div>
                                                    <div class="user-avatar-section">


                                                        <div class="d-flex align-items-center flex-column">



                                                            @if ($empresa->logo != null)
                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                src="{!! asset('storage/app/empresas/' . $empresa->logo) !!} "
                                                                height="110" width="110" alt="User avatar">
                                                            @else
                                                            <img class="img-fluid rounded mt-3 mb-2"
                                                                src="{!! asset('storage/app/empresas/' . $empresa->logo) !!} "
                                                                height="110" width="110" alt="User avatar">
                                                            @endif
                                                            <div class="user-info d-block text-center nombre">
                                                                <h4 class='primary'>
                                                                    {{ $empresa->nombre }}</h4>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="card-text height-75  d-block text-center direccion">
                                                        {{ $empresa->direccion }}</p>
                                                </div>
                                            </div>
                                            <div class="card-footer text-muted mt-0">

                                                <a href="{{ $empresa->dominio }}/auth/lab-login/{{ Auth::user()->tokn }}"
                                                    class="float-right btn btn-block btn-sm btn-secondary"></i>Iniciar
                                                    Sesión</a>
                                            </div>
                                        </div>
                                    </div>

                                    @empty
                                    No hay ningun paciente registrado por el momento
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <div class="d-flex align-items-center justify-content-center">
                                <div id="anterior"></div>
                                <ul class="pagination justify-content-center m-0"></ul>
                                <div id="siguiente"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>


    <div id="agregarmodal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="bg-primary modal-header">
                    <h4 class="modal-title float-left">Nueva Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="form_empresas">
                        <div class="form-row">

                            <div class="form-group col-md-12">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" required>
                            </div>
                            <div class='form-group col-md-12'>
                                <input type="file" accept=".png,.svg,.jpg" class="custom-file-input" name="logo"
                                    id='logo' required>
                                <label for="archivo" class='custom-file-label'>Subir Logo</label>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="direccion">Dirección</label>
                                <input type="text" name="direccion" class="form-control" id="direccion">
                            </div>
                        </div>
                        <div class="button-guardar float-right">
                            <button type="submit" class="btn btn-sm btn-secondary" id="btn_empresa_modal">
                                <span class="spinner-border spinnerAdd spinner-border-sm" role="status"
                                    aria-hidden="true" style="display: none;"></span>
                                Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="editar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="bg-primary modal-header">
                    <h4 class="modal-title float-left">Editar Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="form_edit_empresa">
                        <div class="form-row">
                            <input type="hidden" name="id_e" class="form-control" id="id_e" value='' required>

                            <div class="form-group c ol-md-12">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre_e" class="form-control" id="nombre_e" value='' required>
                            </div>
                            <div class='form-group col-md-12'>
                                <input type="file" accept=".png,.svg,.jpg" class="custom-file-input" name="logo_e"
                                    id='logo_e' value=''>
                                <label for="archivo" class='custom-file-label'>Actualizar Logo</label>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="clave"> Dirección</label>
                                <input type="text" name="direccion_e" class="form-control" id="direccion_e" value=''>
                            </div>

                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-sm btn-primary" id="btnedit">
                                <span class="spinner-border  spinner-border-sm" role="status" aria-hidden="true"
                                    style="display: none;"></span>
                                Actualizar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! url("app-assets/vendors/js/extensions/sweetalert.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/extensions/jquery.steps.min.js") !!}"></script>
{{-- <script src="{!! url("app-assets/vendors/js/forms/select/selectize.min.js") !!}"></script> --}}
<script src="{!! url("app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/pickers/daterange/daterangepicker.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/validation/jquery.validate.min.js") !!}"></script>
<script src="{!! url("app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js") !!}"></script>

{{-- checkbox --}}
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/icheck/icheck.min.js") !!}"></script>
<!-- END PAGE VENDOR JS-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>

<script src="{!! asset('public/js/Laboratorio/audiometria/historialempresas.js') !!}"></script>
@endsection


@section('js_custom')
<script src="{!! url("app-assets/js/scripts/forms/checkbox-radio.js") !!}"></script>
@endsection
{{-- form_principal --}}