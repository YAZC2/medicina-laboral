
<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Health Administration Solution</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://has.humanly-sw.com/public/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/vendors.min.css') !!} ">
    @yield('begin_vendor_js')
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/bootstrap-extended.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/colors.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/components.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/themes/dark-layout.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/themes/semi-dark-layout.min.css') !!}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.min.css') !!} ">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/core/colors/palette-gradient.min.css') !!} ">
    @yield('page_css')
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/assets/css/style.css') !!}">
    @yield('css_custom')
    <!-- END: Custom CSS-->
    <style>
canvas{ display: block; vertical-align: bottom; } /* ---- particles.js container ---- */ #particles-js{ position:absolute; width: 100%; height: 100%; background-color: #cfd2e1; background-image: url("https://has.humanly-sw.com/public/vuexy/app-assets/images/pages/vuexy-login-bg.jpg"); background-repeat: no-repeat; background-size: cover; background-position: 50% 50%; } /* ---- stats.js ---- */ .count-particles{ background: #000022; position: absolute; top: 48px; left: 0; width: 80px; color: #13E8E9; font-size: .8em; text-align: left; text-indent: 4px; line-height: 14px; padding-bottom: 2px; font-family: Helvetica, Arial, sans-serif; font-weight: bold; } .js-count-particles{ font-size: 1.1em; } #stats, .count-particles{ -webkit-user-select: none; margin-top: 5px; margin-left: 5px; } #stats{ border-radius: 3px 3px 0 0; overflow: hidden; } .count-particles{ border-radius: 0 0 3px 3px; }
    </style>

  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  menu-collapsed blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <div id="particles-js"></div>
    <div class="pace  pace-inactive">
      <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
      <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
  </div>
    @yield('content')
  </body>



  <!-- BEGIN: Vendor JS-->
  <script src= "{!! asset('public/vuexy/app-assets/vendors/js/vendors.min.js') !!}"      ></script>
  <!-- BEGIN Vendor JS-->

  <!-- BEGIN: Page Vendor JS-->
  @yield('page_vendor_js')
  <!-- END: Page Vendor JS-->

  <!-- BEGIN: Theme JS-->
  <script src= "{!! asset('public/vuexy/app-assets/js/core/app-menu.min.js') !!}"></script>
  <script src= "{!! asset('public/vuexy/app-assets/js/core/app.min.js') !!}"></script>
  <script src= "{!! asset('public/vuexy/app-assets/js/scripts/components.min.js') !!}"></script>
  <script src= "{!! asset('public/vuexy/app-assets/js/scripts/customizer.min.js') !!}"></script>
  <script src= "{!! asset('public/vuexy/app-assets/js/scripts/footer.min.js') !!}"></script>
  <!-- END: Theme JS-->

  <!-- BEGIN: Page JS-->
  @yield('page_js')
  <!-- END: Page JS-->
  <!-- BEGIN: Page JS-->
  <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> <!-- stats.js lib --> 

  @yield('js_custom')
  <script>
particlesJS("particles-js", {"particles":{"number":{"value":100,"density":{"enable":true,"value_area":800}},"color":{"value":"#f15a29"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":false,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":3,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":true,"distance":150,"color":"#003571","opacity":0.4,"width":1},"move":{"enable":true,"speed":6,"direction":"none","random":false,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":true,"mode":"repulse"},"onclick":{"enable":true,"mode":"push"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":400,"size":40,"duration":2,"opacity":8,"speed":3},"repulse":{"distance":200,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});var count_particles, stats, update; stats = new Stats; stats.setMode(0); stats.domElement.style.position = 'absolute'; stats.domElement.style.left = '0px'; stats.domElement.style.top = '0px'; document.body.appendChild(stats.domElement); count_particles = document.querySelector('.js-count-particles'); update = function() { stats.begin(); stats.end(); if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) { count_particles.innerText = window.pJSDom[0].pJS.particles.array.length; } requestAnimationFrame(update); }; requestAnimationFrame(update);;  </script>
  <!-- END: Page JS-->
  <!-- END: Body-->
</html>
