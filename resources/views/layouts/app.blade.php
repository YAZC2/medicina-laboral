<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Clínica</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend/img/favicon_asesores.png" type="image/png">

    <!-- Bootstrap styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!--Styles and fonts-->
    @yield('styles')

</head>
<body class="body">
  <div class="row" id="app">
    @yield('content')
  </div>

</body>
<!-- jQuery UI 1.11.4 -->
<script src="resources/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/bootbox.min.js"></script>
<script src="resources/js/login.js"></script>
<script src="resources/js/jquery.min.js"></script>
<script src="resources/js/resetPasswordRedirect.js"></script>
</html>
