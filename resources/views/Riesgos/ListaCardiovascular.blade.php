@extends('layouts.Vuexy')
@section('title','Pacientes')
@section('begin_vendor_css')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('css_custom')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
@endsection
@section('content')
@foreach ($empresas as $empresa )
<input type="hidden" value="{{ $empresa->nombre_sass }}" id="n_sass_empresa" name="n_sass_empresa" />
<input type="hidden" value="{{ $empresa->codigo_sass }}" id="c_sass_empresa" name="c_sass_empresa" />
@endforeach
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header bg-secondary">
                    <h4 class="card-title text-white">Riesgos Cardiovasculares</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class="col md 12 text-center loadResultados" style="">
                            <div class="cargar md 12">
                                <div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>
                        <div id='tabla_dinamica'>
                        </div>
                        <div id="tabla_estudios">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>

</section>
<div class="modal fade text-left" id="Vista_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content">
            <div class="modal-header text-center bg-primary">
                <h4 class="modal-title">Riesgo CV-Abraham johan Tapia Flores</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12 text-center loadEstudios">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 text-left">
                                <p><small><b class="secondary">Factores de Riesgo</b></small></p>
                                <p><small id="edad"><b>Edad:</b> </small></p>
                                <p><small id="idpaciente"></small></p>
                                <p><small id="campoSexo"><b>Genero:</b> Masculino</small></p>


                            </div>
                            <div class="col-md-4 text-left">
                                <p><small><b class="secondary">Presión sistólica (mm de Hg)</b></small></p>
                                    <div class="row">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1" onchange="cambiar_estatus('1')">
                                            <small id="usted_fuma"><b>¿Usted Fuma?: </b> </small> 
                                            <input type="checkbox" checked="" class="custom-control-input" name="fumas" id="fumas">
                                            <label class="custom-control-label" for="fumas">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1" onchange="cambiar_estatus('1')">
                                            <small id="padece_diabetes"><b>¿Padecé Diabetes?: </b> </small> 
                                            <input type="checkbox" checked="" class="custom-control-input" name="diabetes" id="diabetes">
                                            <label class="custom-control-label" for="diabetes">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1" onchange="cambiar_estatus('1')">
                                            <small id="sufre_hipertension"><b>¿Hipertensión?: </b> </small> 
                                            <input type="checkbox" checked="" class="custom-control-input" name="hipertension" id="hipertension">
                                            <label class="custom-control-label" for="hipertension">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-4 text-left">
                                <p><small><b class="secondary">Perfil de lípidos (mg/dl)</b></small></p>
                                <p><small id="campoEmail"><b>Colesterol total (requerido): </b> 
                                    <input type="number" name="clave" class="form-control form-control-sm" value=""  minlength="5" maxlength="40" id="colesterol" required=""></small></p>
                                <p><small id="campoTelefono"><b>Colesterol HDL (requerido): </b> 
                                    <input type="number" name="clave" class="form-control form-control-sm" value="" id="hdl" required=""></small></p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row contEstudios">

                </div>


            </div>
        </div>

    </div>
</div>

@endsection


@section('page_vendor_js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   ">
</script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}">
</script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/core/app-menu.min.js"></script>
@endsection

@section('page_js')

@endsection
@section('js_custom')

<script src="{!! asset('public/js/riesgos/riesgos.js') !!}" charset="utf-8"></script>

@endsection

