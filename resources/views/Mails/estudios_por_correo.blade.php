<!DOCTYPE HTML>
<html lang="es" xml:lang="es">

<head>
  <meta charset="gb18030">
  <title>HAS</title>
  <style>
    body {
      background-color: #eee;
      font-family: Arial, Helvetica, sans-serif;
     
    }
    h4{
      color:#fff;
    }

  h5 {
      font-size: 30px;
      color: #d44f24 !important;
      margin-top:0px;
      margin-bottom: 0px;
      text-align:center;
    }

    .second {
      background-color: #002b46;
      width: 60%;
      border-radius: 10px 10px 0px 0px;
      padding-left: 40px;
      padding-right: 40px;
      text-align: center;
      font-size: 40px;
      color: #fff;
      padding-top: 1px;
      padding-bottom: 1px;
      margin:auto;
      display:block;
    }

    .tree {
      background-color: #cdcdcd;
      height: 400px;
      width: 60%;
      margin-top: -30px;
      z-index: 9;
      padding-left: 40px;
      padding-right: 40px;
      margin:auto;
      display:block;
      border-radius: 0px 0px 10px 10px;

    }

    .link {
      background-color: #d44f24;
      padding-left: 40px;
      padding-right: 40px;
      width: 90%;
      margin: auto;
      display: block;
      border-radius: 0px 0px 5px 5px;
      margin:auto;
      display:block;
    }

    .dates {
      background-color: #fff;
      width: 90%;
      margin: auto;
      display: block;
      padding-left: 40px;
      padding-right: 40px;
      margin-top: 0px;
      z-index: 10;
      height: 350px;
      border-radius: 5px 5px 5px 5px;
    }

    button {
      background-color: #d44f24;
      border-radius: 50px;
      width: 208px;
      height: 37px;
      overflow: hidden;
      font-size: 15px;
      font-weight: 500;
      color: #ffffff;
      border: none;
      outline: none;
      margin-top: 25px;
      margin-bottom: 30px;
      margin:auto;
      display: block;
      
      
    }
    a{
      text-decoration: none;
    }

    button:hover {
      border-bottom: 2px solid #122D72;
      box-shadow: 0px 2px 0 #122D72;
    }
    .Firma{
border-radius:10px;
}
  .Firma p {
    text-align: center;
    color: #122D72 !important;
    font-size: 12px !important;
    padding-left: 100px !important;
    
  }
  .Firma b {
    text-align: center;
    color: #122D72 !important;
    font-size: 12px !important;

  }
  p{
    color:#566078 !important
  }
  .linea_azul{
    background-color: #122D72 !important;
    height: 2px;
    border: none;
  

  }
  b{
    color:#566078;
  }
  .user{
    color: #122D72 !important;
    font-weight: bold;

  }
  .password{
    color: #122D72 !important;
    font-weight: bold;

  }
  
  </style>
</head>

<body>
  <div class="second">
    <div class="row">
        <div class="img">
            <img src="https://has-humanly.com/pagina_has/elementos/logo-has-blanco.svg" alt="HAS"/>
            
        </div>
        
    </div>
  </div>
  <div class="col-md-12">
    <div class="tree">
      <div class="dates">
        <br>
        <h5>Bienvenido</h5>
        <hr class="linea_azul">
        <p>
          Un Administrador de {{ $empresa->nombre }} le ha enviado un enlace para consultar sus resultados.</p>
          <p>Ahora puede consultar sus resultados a traves del siguiente enlace:</p>
      
        <a href="https://cemex.has-humanly.com/estudioscorreo/{{ $nim }}/{{ $curp }}" target="blank_"><button>Clic  Aquí</button></a></br></br>
      </div>
      
    </div>
  
    <div class="Firma">
      <p>Este es un correo enviado desde HAS.</p>
      <p><b>Diseñado y creado por Humanly Software</b></b></p>
    </div>
  </div>
</body>
</html>

