@extends('layouts.VuexyLaboratorio')

@section('title', 'Audiometría')

@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css"
    href="{!! asset('public/vuexy/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') !!}">
<link rel="stylesheet" type="text/css"
    href="{!! asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/css/daicoms/daicoms.css') !!}">
@endsection

@section('content')
<div class="col-12">

    <div class="card">
        <div class="card-header bg-secondary">
            <h4 class="card-title text-white">Resultados</h4>
        </div>
        <div class="card-content">
            <div class="card-body card-dashboard">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="loadEstudios"></div>
                        <div class="row dibujaEstudios">

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="{{ $toma.'/'.$consecutivo }}" id="Idtoma" name="Idtoma"/>

<div class="modal fade text-left" id="estudiosShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">

        <div class="modal-content">
            <div class="modal-header bg-primary ">
                <h4 class="modal-title float-left">Estudios</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body mt-2">

                <div class="col-md-12 text-center loadEstudios">
                   
                </div>


                <div class="row contEstudios">

                </div>
                <div class='col-md-12'>
                       
                    <div id='estudios'>
                    
                    </div>
                </div>
                <div class="row contImagenologia">
                        
                       
                </div>
                    
                        
                       
                        
                        
               
                


            </div>
        </div>

    </div>
</div>
@endsection

@section('page_vendor_js')
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   ">
</script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}">
</script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/core/app-menu.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
window.cornerstoneWADOImageLoader || document.write(
    '<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/js/quill.min.js"></script>
<script src="https://humanly-sw.com/imagenologiadev/template/quill-2-0/dist/js/index.js"></script>
<script src=" {!! asset('public/js/librerias-dicom/cornerstone.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/cornerstoneMath.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/cornerstoneTools.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/dicomParser.min.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/initializeWebWorkers.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/uids.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/openJPEG-FixedMemory.js') !!}   "></script>
<script src=" {!! asset('public/js/librerias-dicom/logica.js') !!}   "></script>
<script src=" {!! asset('public/js/empresa/empleado/obtenerdaicom.js') !!}   "></script>
@endsection

@section('js_custom')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
{{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="{!! asset('public/js/empresa/enviarestudios.js') !!}"></script>
<script>



</script>
@endsection