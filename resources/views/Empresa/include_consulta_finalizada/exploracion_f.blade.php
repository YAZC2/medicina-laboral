<section class="examen_fisico_section" >
  <div class="row">
    <div class="col-md-12">
      <div class="card">
       <div class="card-body" >
            @if($datos["examenFisico"] != null)
                <div class="row">
                @foreach ($array_examenFisico as $item)
                    @if ($datos["examenFisico"]->$item != "")
                    <div class="col-md-4 ">
                        <div class="border rounded p-1">
                            <h6>{{ Str::ucfirst($item) }}</h6>
                            <hr>
                            {!! $datos["examenFisico"]->$item !!}
                        </div>
                    </div>

                    @endif
                @endforeach
                </div>
            @endif
            {{-- <select class="exploracion_select" multiple="multiple" style="width:100%">
            <option value="apariencia">Apariencia general</option>
            <option value="psBrazoDerecho">Presión sanguínea - Brazo derecho</option>
            <option value="psBrazoIzquierdo">Presión sanguínea - Brazo izquierdo</option>
            <option value="cabezaOjos">Cabeza y Ojos</option>
            <option value="oidoNarizBoca">Oidos/Nariz/Boca</option>
            <option value="dientesFaringe">Dientes/Faringe</option>
            <option value="cuello">Cuello</option>
            <option value="tiroides">Tiroides</option>
            <option value="nodoLinfatico">Nodo Linfático</option>
            <option value="toraxPulmones">Torax y Pulmones</option>
            <option value="pecho">Pecho</option>
            <option value="corazon">Corazón</option>
            <option value="abdomen">Abdomen</option>
            <option value="rectalDigital">Rectal Digital</option>
            <option value="genitales">Genitales</option>
            <option value="columnaVertebral">Columna vertebral</option>
            <option value="piel">Piel</option>
            <option value="pulsoArterial">Pulso Arteral</option>
            <option value="extremidades">Extremidades</option>
            <option value="musculoesqueleto">Musculoesqueleto</option>
            <option value="reflejosNeurologicos">Reflejos y Neurológicos</option>
            <option value="estadoMental">Estado Mental</option>
            <option value="ojoIzquierdo">Ojo Izquierdo</option>
            <option value="ojoDerecho">Ojo Derecho</option>
            <option value="diagnosticoAudiologico">Diagnostico Audiologico</option>
            <option value="otro">Otro</option>
          </select> --}}
        </div>
      </div>

    </div>
  </div>
  <div class="row" id="exploracion_section">

  </div>

</section>
