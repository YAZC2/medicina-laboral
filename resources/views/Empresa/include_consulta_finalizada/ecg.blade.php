<section class="archivos">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
            <div class="card-header">
                <div class="display-5">Electrocardiograma (xml).</div>
            </div>
            <div class="card-body">
                <div class="row content_ecg">
                @if(count($historial->ecgs) > 0)
                    @foreach ($historial->ecgs as $ecg)
                        <a href="{!! route('view_ecg',['id'=>encrypt($ecg->id)]) !!}" target="_blank" id="ecg_{{ $ecg->id }}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
                            <div class="mr-50">
                                <img src="https://img.icons8.com/color/48/000000/xml-file.png"/>
                            </div>
                            <div class="user-page-info">
                                <h6 class="mb-0">Electrocardiograma</h6>
                                <span class="font-small-2">{{ \Carbon\Carbon::parse($ecg->created_at)->diffForHumans() }}</span>
                            </div>
                        </a>
                    @endforeach
                @else
                <div class="col-12">
                    <h6>Sin electrocardiograma</h6>
                </div>
                @endif
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
