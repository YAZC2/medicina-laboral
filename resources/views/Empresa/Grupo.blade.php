
@extends('layouts.Vuexy')
@section('title','Grupos')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
@endsection
@section('content')
  <section id="data-list-view" class="data-list-view-header"
   data-insert="@if(Auth::user()->usuarioPrincipal()||isset(Session::get('Grupos')['insert'])) true @else false @endif">
    <!-- DataTable starts -->
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
          <tr>
            <th>Código</th>
            <th>Grupo</th>
            <th>Fecha de Creación</th>
            <th>Fecha de Modificación</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($grupos as $value)
            <tr class="mb-1">
              <td>G-00{{ $value->id }}</td>
              <td>{{ $value->nombre }}</td>
              <td>{{ \Carbon::parse($value->created_at)->format('d-m-Y') }}</td>
              <td>{{ \Carbon::parse($value->updated_at)->format('d-m-Y') }}</td>
              <td>
                @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Grupos')['select']))
                  <a href="{{ route('grupos.show', $value->nombre)}}" class="action-view actions_button"><i class="feather icon-eye"></i>
                  </a>
                @endif

                @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Grupos')['update']))
                  <span class="action-edit actions_button" data-nombre="{{ $value->nombre }}"><i class="feather icon-edit"></i></span>
                @endif

                @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Grupos')['delete']))
                  <span class="action-delete actions_button" data-nombre="{{ $value->nombre }}"><i class="feather icon-trash"></i></span>
                @endif

              </td>
            </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    <!-- DataTable ends -->
    <!-- add new sidebar starts -->
    <div class="add-new-data-sidebar">
      <div class="overlay-bg"></div>
      <div class="add-new-data">
        <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
          <div>
            <h4 class="text-uppercase">Nuevo Grupo</h4>
          </div>
          <div class="hide-data-sidebar">
            <i class="feather icon-x"></i>
          </div>
        </div>
        <div class="data-items pb-3">
          <div class="data-fields px-2 mt-3">
            <div class="row">
              <form id="formGrupo" method="POST" class="col-md-12">
                  @csrf
                  <div class="form-group">
                    <label for="grupo">Nombre del Grupo:</label>
                    <input type="text" class="form-control" id="grupo" name="grupo" required>
                  </div>
                  <button type="submit" class="btn btn-sm btn-primary">
                    <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true"></span>
                    Guardar
                  </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- add new sidebar ends -->
  </section>


  <div id="updategrupos" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
            <h4 class="modal-title float-left" id='title_update'></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body d-block">
          <div id="load_update" class="text-center">
              <img src="resources/sass/images/load.gif" width="59%" alt="">
              <span class="d-block span cargando">Cargando...</span>
          </div>
            <form id="formGrupoUpdate" method="post" class="col-md-12">
                @csrf
                <div class="form-group">
                  <label for="grupo">Nombre del Grupo:</label>
                  <input type="text" class="form-control" id="grupo_update" name="nombre_update" required>
                  <input type="hidden" name="nombre" id="nombreGrupo" required>

                </div>
                <button type="submit" class="btn btn-secondary">Modificar</button>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class=" btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>



@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')
  <script src="{!! asset('public/js/empresa/grupos.js') !!}"></script>
@endsection
