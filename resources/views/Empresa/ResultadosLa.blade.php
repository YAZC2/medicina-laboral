@extends('layouts.Vuexy')
@section('title','Pacientes')
@section('begin_vendor_css')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('css_custom')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
@endsection
@section('content')
@foreach ($empresas as $empresa )
<input type="hidden" value="{{ $empresa->nombre_sass }}" id="n_sass_empresa" name="n_sass_empresa"/>
<input type="hidden" value="{{ $empresa->codigo_sass }}" id="c_sass_empresa" name="c_sass_empresa"/>
@endforeach
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header bg-secondary">
                    <h4 class="card-title text-white">Resultados Laboratorio</h4>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard">
                        <div class="col md 12 text-center loadResultados" style="">
                            <div class="cargar md 12">
                            </div>
                        </div>
                        <div id='tabla_dinamica'>
                        </div>
                        <div id="tabla_estudios">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>

</section>
<div class="modal fade text-left" id="estudiosShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">

        <div class="modal-content">
            <div class="modal-header">
                {{-- <h4 class="modal-title float-left">Laboratorio</h4> --}}
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body mt-2">

                <div class="col-md-12 text-center loadEstudios">
                   
                </div>


                <div class="row contEstudios">

                </div>


            </div>
        </div>

    </div>
</div>
<div class="modal fade text-left" id="correo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">

        <div class="modal-content">
            <div class="modal-header bg-primary ">
                <h4 class="modal-title float-left">Enviar por correo electronico</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body" id="correo_body">
                
                <form method="post" id="enviar_resultado" class="col-md-12">
                    <label for="inputEmail">Correo electrónico</label>
                    <input type="email" name="email" id="email" class="form-control" value="">
                    <input type="hidden" name="nim_correo" id="nim_correo" class="form-control" value="" >
                    <button type="submit" class="btn mt-2 btn-sm btn-primary waves-effect waves-light" id="btn_enviar">Enviar</button>
                  </form>
                </div>
        </div>

    </div>
</div>

@endsection


@section('page_vendor_js')
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   ">
</script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   ">
</script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}">
</script>
<script src="https://has-humanly.com/empresa_dev/public/vuexy/app-assets/js/core/app-menu.min.js"></script>
@endsection

@section('page_js')

@endsection
@section('js_custom')

<script src="{!! asset('public/js/empresa/resultados_la.js') !!}" charset="utf-8"></script>

@endsection