@extends('layouts.Vuexy')
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/animate/animate.css') !!}">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/pages/app-user.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/components.min.css') !!}">
@endsection
@section('title')
  Expediente: {{$empleado->nombre}} {{$empleado->apellido_paterno}} {{$empleado->apellido_materno}}
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/info_paciente.css') !!}">
@endsection
@section('content')
<nav aria-label="breadcrumb">

  <ol class="breadcrumb breadcrumb-chevron">
      <li class="breadcrumb-item"><a href="{{ route('empleados') }}">Empleados</a></li>

      <li class="breadcrumb-item"><a href="{{ route('empleados.show', $empleado->CURP)}}">{{$empleado->nombre}}
              {{$empleado->apellido_paterno}} {{$empleado->apellido_materno}}</a></li>
      <li class="breadcrumb-item active" aria-current="page">Historial de Antecedentes</li>
  </ol>
</nav>
<div class="col-lg-12">
  <div class="card">
    <div class="card-header bg-secondary">
      <h4 class="card-title text-white">Historial Antecedentes</h4>
    </div>
    <div class="card-body">
      <ul class="timeline">
       
        @foreach ($historial_antecedentes as $ha)
        <li class="timeline-item secondary">
          <span class="timeline-point primary">
            <i class="feather icon-clock primary"></i>
          </span>
          <div class="timeline-event">
            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
              <h6 class="primary">{{ $ha->pregunta }}</h6>
              <span class="badge badge-light-warning">{{ $ha->created_at }}</span>
            </div>
            <p>{{ $ha->respuesta }}</p>
            {{-- <div class="d-flex flex-row align-items-center">
              <img class="me-1" src="../../../app-assets/images/icons/file-icons/pdf.png" alt="invoice" height="23">
              <span>invoice.pdf</span>
            </div> --}}
          </div>
        </li>
          
        @endforeach
       
       
      </ul>
    </div>
  </div>
</div>


@include('Empresa.empleado.modals')



@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>


@endsection
@section('page_js')
  <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
  <script src=" {!! asset('public/vuexy/app-assets/js/scripts/modal/components-modal.min.js') !!} "></script>
  <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection
@section('js_custom')

  <script src="{!! asset('public/js/empresa/info_paciente.js') !!}"></script>
  <script src="https://ac-labs.com.mx/asesores/app-assets2/vendors/js/charts/echarts/echarts.js"></script>
  <script src="https://ac-labs.com.mx/asesores/app-assets2/vendors/js/charts/echarts/chart/bar.js"></script>
  <script src="{!! asset('resources/js/charts-empleado.js') !!} "></script>
  <script src="{!! asset('public/js/empresa/antecedente/antecedentes.js') !!}"></script>
  <script src="{{ asset('public/js/empresa/empleado/estudios.js') }}"></script>
  <script src="{!! asset('public/vuexy/app-assets/js/scripts/popover/popover.min.js') !!}" charset="utf-8"></script>
@endsection


