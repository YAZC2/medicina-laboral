<!-- account start -->
<div class="col-md-12">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-md-4 text-center ">
          <div class="avatar">
            @if ($empleado->imagen)
              <img class="perfilImge" src="../storage/app/pacientes/{{ $empleado->imagen }}" width="120px" height="120px" alt="avatar">
              @else
              <img class="perfilImge" src="{!! asset('public/img/2665817.jpg') !!}" height="120px" width="120px" alt="avatar">
            @endif

          </div>
          @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Pacientes')['update']))
          <form data-toggle="popover" data-placement="top" data-content="Agrega o modifica la fotografia de {{$empleado->nombre }} {{$empleado->apellido_paterno}}" data-trigger="hover" data-original-title="Fotografia del paciente" id="imageChange" method="post" class="position-absolute" enctype="multipart/form-data">
            <label class="fileContainer btn  btn-icon btn-icon rounded-circle btn-primary mr-1 waves-effect waves-light">
                <i class="feather icon-edit-2 editIcon"></i>
                <span class="spinner-border imgLoad float-rigth spinner-border-sm" role="status" aria-hidden="true" style="display:none"></span>
                <input type="hidden" name="key" value="{{ $empleado->id }}">
                <input class="inputImage" accept="image/*" name="file" type="file"/>
            </label>
          </form>
          @endif


          {{-- <button class="btn position-absolute btn-icon btn-icon rounded-circle btn-primary mr-1 waves-effect waves-light">
            <i class="feather icon-edit-2"></i>
          </button> --}}
        </div>
        <div class="col-md-8 text-center">
          <div class="row">
              <div class="col-md-12">
                <p class="mb-0 h4 font-weight-light  secondary text-center" id="campoNombreCompleto">
                  {{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno}}
                </p>
                <hr>
              </div>
              <div class="col-md-4 text-left">
               
                <p class="p" id="campoFechaNacimiento">
                  {{ \Carbon::parse($empleado->fecha_nacimiento)->toFormattedDateString() }} - {{
                  \Carbon::parse($empleado->fecha_nacimiento)->age}} años
                </p>
                <p><small class="d-block" id="campoCurp">{{ $empleado->CURP }}</small></p>
              </div>
              <div class="col-md-4 text-left">
      
                <p><small id="campoClave"><b>Clave: </b> {{ $empleado->clave }}</small></p>
                <p><small id="campoDireccion"><b>Dirección: </b>{{ ucfirst(strtolower($empleado->direccion)) }}</small></p>
                <p><small id="campoSexo"><b>Sexo: </b>{{ $empleado->genero }}</small></p>
                
                
              </div>
              <div class="col-md-4 text-left">
               
                <p><small id="campoEmail"><b>Correo: </b>{{ $empleado->email }}</small></p>
                <p><small id="campoTelefono"><b>Teléfono: </b>{{ $empleado->telefono }}</small></p>
                <p><small id="campoGrupo"><b>Grupo: </b> 
                  @if (empty($empleado->grupo->nombre))
                  Sin Grupo
                  @else
                  {{$empleado->grupo->nombre}}
                  @endif</small>
                </p>
              </div>
          </div>
          <div class="col-md-12">
            <div class=" text-center mt-1">
            @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Pacientes')['update']))
              <button class="btn btn-primary btn-sm  waves-effect waves-light" id="editar_empleado">
                <i class="feather icon-edit-1"></i>
                Editar
              </button>
            @endif
            @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Pacientes')['delete']))
              <button class="btn btn-outline-danger waves-effect btn-sm waves-light" data-id="{{ $empleado->id }}" id="eliminar_empleado">
                <i class="feather icon-trash-2"></i>
                Eliminar
              </button>
            @endif
              <button type="button" class="btn btn-outline-primary border-none btn-sm" id="headingCollapse1" data-toggle="collapse" role="button" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                {{-- <i class="feather icon-chevrons-down "></i> --}}
                Valoración Externa
              </button>
              {{-- <a class="float-right text-white mb-1" id="doc_pdf"
                href="../Historial-Clinico-Pdf/{{$paciente->CURP}}/admision_contratista" target="_blank">
                <i class="feather icon-printer "></i>
              </a> --}}
            </div>
          </div>

        </div>

      </div>

      <div class="default-collapse collapse-bordered">
        
        <div class="card collapse-header">
          <div id="collapse1" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse" style="">
            <div class="card-content">
              <div class="card-body text-center">
                <hr>
                <div class="row p-1">
                  @foreach($formatos as $formato)
                  <div class="col-md-4">
                    <h6 class="secondary">{{ $formato->titulo }}</h6>
                    <small id="campoClave"><a class="btn btn-outline-primary border-none btn-sm" id="doc_pdf"
                      href="../{{ $formato->nombre_pdf }}/{{ $empleado->CURP }}/{{ $formato->prefijo }}" target="_blank">
                      <i class="feather icon-printer "></i>
                    </a></small>
                  </div>
                     
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

{{-- <div class="col-md-6">
 <div class="card">

  <div class="card-content collapse show">
    <div class="card-body">
      <div class="estudios-programados main-title">
        <div class="contenedor">

          <div class="col-md-12 pr-0 pl-0">
              <div class="alert alert-primary">
                <h4 class="alert-heading">Programación de estudios de laboratorio</h4>
                  <p class="text-center">Una vez que se programen los estudios se daran de prealta en el laboratorio</p>
              </div>
                @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Programar Estudios')['insert']))
                <div class="col-md-12 text-center">
                    <button class="btn btn-primary btn-sm" onclick="showProgramarModal('{{ $empleado->CURP }}')">
                    Programar estudios
                    </button>
                </div> 
                @endif

          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> --}}

{{-- modales --}}

<div id="estudioModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title float-left text-white" id="objetivo-header">Estudio programado para el grupo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-row col-md-12">
          <div class="form-group col-md-12">
            <h4>Nombre</h4>
            <p id="nombre-estudio"></p>
            <h4>Fecha de inicio</h4>
            <p id="fecha-inicio"></p>
            <h4>Fecha de fin</h4>
            <p id="fecha-fin"></p>
            <form role="form" id="eliminar_estudio" method="post" action="{{route('eliminarEstudioEmpleado')}}" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDEliminar">
              <input type="hidden" name="token" value="" id="inputToken">
              <input type="hidden" name="nombreEstudio" value="" id="inputNombreEstudio">
              <input type="hidden" name="idEstudio" value="" id="inputIDEstudio">
              <input type="hidden" name="fechaInicial" value="" id="inputFechaInicialEliminar">
              <input type="hidden" name="fechaFinal" value="" id="inputFechaFinalEliminar">
              <button type="submit" class="btn btn-sm btn-secondary">Eliminar</button>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
            <!-- Modal para programar un nuevo estudio -->
<div id="programarModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title float-left">Programar estudio para {{ $empleado->nombre }}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div id="alerts_estudio">
          <!--Aquí se muestran las alertas generadas-->
        </div>
        <form class="col-md-12" role="form" id="programacion_estudios" method="post" enctype="multipart/form-data">

          {{-- <form class="col-md-12" role="form" id="programacion_estudios" method="post" action="{{route('programarEstudiosEmpleado')}}" enctype="multipart/form-data"> --}}
          @csrf
          <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDProgramar">
          <input type="hidden" name="id_grupo" value={{$empleado->grupo['id']}}>
          <div class="form-row">
            <div class="form-group col-md-12 ">
              <div class="selectores-fechas">
                <label for="inputFechaInicioEmpleado">Fecha inicial</label>
                <input type="date" name="fechaInicio" class="form-control" value="{{ \Carbon::today()->toDateString() }}" id="inputFechaInicioEmpleado" min={{ \Carbon::today()->toDateString() }} required>
              </div>
              <div class="form-group">


                <label for="inputFechaFinalEmpleado">Fecha final</label>
                <input type="date" name="fechaFinal" class="form-control" value="{{ \Carbon::today()->addWeeks(1)->toDateString() }}" id="inputFechaFinalEmpleado" min={{ \Carbon::today()->toDateString() }} required>
              </div>

              <div class="estudios-por-programar custom-checkbox ">


                <label for="">Selecciona los estudios</label>
                <select class="estudiosAll" style="width:100%;" name="estudios_programar[]" multiple tabindex="-1">
                  {{-- @foreach ($estudios_all as $estudio)
                    <option value={{ "o->estudio->id }}}">{{ $estudio->estudio->nombre}}</option>
                  @endforeach --}}
                </select>
              </div>

            </div>
          </div>
          <div class="col-md-12 text-center">
            <button type="submit" class="btn btn-primary btn-sm" id="btn_programar_estudio">Programar estudios</button>
          </div>
        </form>
        </div>

      </div>

    </div>
  </div>