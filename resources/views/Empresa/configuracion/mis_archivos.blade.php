<form id="form_update_misDatos" method="post">
    <div class="row">
        <div class="col-12">
            <h4 class="secondary">Actualizar mi información</h4>
        </div>
        <div class="col-12">
            <hr>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <div class="controls">
                    <label>Nombre</label>
                    <input type="text" class="form-control" value="{{ Auth::user()->nombre }}" name="nombre_user" id="nombre_user" required>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <label>Teléfono</label>
                    <input maxlength="15" type="text" class="form-control" value="{{ Auth::user()->telefono }}" name="telefono_user" id="telefono_user" required>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <label>Área</label>
                    <input type="text" class="form-control" value="{{ Auth::user()->area }}" name="area_user" id="area_user" required>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <div class="controls">
                    <label>Correo electrónico</label>
                    <input type="text" class="form-control" value="{{ Auth::user()->email }}" name="email_user" id="email_user" required>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <label>Puesto</label>
                    <input type="text" class="form-control" value="{{ Auth::user()->puesto }}" name="puesto_user" id="puesto_user" required>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <br />
                    <button type="button" id="btnModalPassword" class="btn btn-sm btn-outline-primary btn-md" tabindex="0"><span><i class="feather icon-lock"></i> Cambiar contraseña</span></button>
                </div>
            </div>
        </div>
        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
            <button type="submit" class="btn btn-primary submit btn-sm">
                <span class="spinner-border spinner spinner-border-sm" role="status" aria-hidden="true"></span>
                Guardar cambios
            </button>
        </div>
    </div>
</form>
{{-- Modal para editar la contraseña del usuario actual --}}
<div id="passwordModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Actualizar mi contraseña</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_update_password">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="new_password">Nueva contraseña</label>
                            <input type="password" name="new_password"  class="form-control" id="new_password" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="new_password_confirm">Confirmar contraseña</label>
                            <input type="password" name="new_password_confirm"  class="form-control" id="new_password_confirm" required>
                            <span class="invalid-feedback d-none" id="error_password" role="alert">
                        </div>
                        </span>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_updatePassword_modal">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
