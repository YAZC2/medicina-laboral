<div class='row'>
    <div class='col-md-12'>
        <div class='col-md-12'>
        <h4 class="secondary">Medicamentos</h4>
        <hr>
        </div>
        <div class="card" id="medicamentos">
            <div class="card-content collapse show" style="">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 mb-1">
                            <input type="text" class="form-control search input-sm" placeholder="Buscar Medicamentos">
                        </div>
                        <div class='col-md-4'>
                            <button id="btnopenmedicamento" type="button"
                                class="btn btn-sm btn-secondary waves-effect waves-float waves-light">+ Nuevo
                                Medicamento</button>
                        </div>
                        <div class="col-md-8 list">
                            @forelse($medicamentos as $medi)
                                <div class="twitter-feed">
                                    <div class="d-flex justify-content-start align-items-center ">
                                        <div class="col-md-12" id='tarjetamedicamentos'>
                                            <div class="user-page-info mt-1 mb-1">
                                                <h5 class="text-bold-600 mb-0 text-center primary nombre">
                                                    {{ $medi->nombre }}</h5>
                                                <h5 class="text-bold-600 mb-0 text-center secondary clave">
                                                    {{ $medi->clave }}</h5>
                                                <p class="d-block secondary text-center detalle">
                                                    {{ $medi->detalle }}
                                                </p>
                                            </div>
                                            @if (empty($medi->id_user))
                                            @else
                                                @if ($medi->id_user == Auth::user()->id)
                                                    <div class="text-center mb-1">
                                                        <button data-id="{{ $medi->id }}"
                                                            class="edit_med mr-1 btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light"
                                                            data-toggle="modal" data-target="#exampleModal">
                                                            <i class="feather icon-edit"></i>
                                                        </button>
                                                        <button data-id="{{ $medi->id }}"
                                                            class="trash_med btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light">
                                                            <i class="feather icon-trash"></i>
                                                        </button>
                                                    </div>
                                                @endif
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <hr>

                            @empty
                                <div class="alert">
                                    <div class="alert alert-danger">
                                        No hay Medicamentos
                                    </div>
                                </div>
                            @endforelse
                        </div>
                        <div class="col-md-8 mt-1">
                            <div class="d-flex align-items-center justify-content-center">
                                <div id="anterior"></div>
                                <ul class="pagination justify-content-center m-0"></ul>
                                <div id="siguiente"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="agregarmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Nuevo medicamento</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_medicamentos">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="clave">Clave</label>
                            <input type="text" name="clave" class="form-control" id="clave">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="nombre" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="detalle">Descripción</label>
                            <input type="text" name="detalle" class="form-control" id="detalle" required>
                        </div>
                    </div>
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_medicamento_modal">
                            <span class="spinner-border spinnerAdd spinner-border-sm" role="status" aria-hidden="true"
                                style="display: none;"></span>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="editar" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="bg-primary modal-header">
                <h4 class="modal-title float-left">Editar medicamento</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_edit_medicamentos">
                    <div class="form-row">
                        <input type="hidden" name="id" class="form-control" id="id" value='' required>
                        <div class="form-group col-md-12">
                            <label for="clave"> Clave</label>
                            <input type="text" name="clave_e" class="form-control" id="clave_e" value=''>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre_e" class="form-control" id="nombre_e" value='' required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="detalle">Descripción</label>
                            <input type="text" name="detalle_e" class="form-control" id="detalle_e" value='' required>
                        </div>
                    </div>
                    <div class="float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btnedit">
                            <span class="spinner-border  spinner-border-sm" role="status" aria-hidden="true"
                                style="display: none;"></span>
                            Actualizar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
