@extends('layouts.Vuexy')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href="public/vuexy/app-assets/vendors/css/calendars/fullcalendar.min.css">
  <link rel="stylesheet" type="text/css" href="public/vuexy/app-assets/vendors/css/calendars/extensions/daygrid.min.css">
  <link rel="stylesheet" type="text/css" href="public/vuexy/app-assets/vendors/css/calendars/extensions/timegrid.min.css">
  <link rel="stylesheet" type="text/css" href="public/vuexy/app-assets/vendors/css/pickers/pickadate/pickadate.css">
@endsection
@section('page_css')
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="public/vuexy/app-assets/css/plugins/calendars/fullcalendar.min.css">
@endsection
@section('title')
  Agenda
@endsection
@section('css_custom')

@endsection

@section('content')
<!-- BEGIN: Content-->
<div id="validacion_insert" data-validacion="@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Agenda')['insert'])) true @else false @endif"></div>
<section id="basic-examples">
  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-content">
          <div class="card-body">
            <div id='fc-default'></div>
          </div>
        </div>
      </div>
    </div>
    @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Consultas')['select']))
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Agendas de Hoy</h4>
          <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collapse show">
          <div class="card-body">
            <div class="table-responsive">
                  <table class="table mb-0">
                      <thead class="bg-secondary text-white">
                          <tr>
                              <th scope="col">Paciente</th>
                              <th scope="col">Hora Inicial</th>
                              <th scope="col">Hora Final</th>
                              <th scope="col">Iniciar</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($schedules as  $value)
                            <tr>
                              <th>
                                {{ $value['empleado']->nombre }} {{ $value['empleado']->apellido_paterno }} {{ $value['empleado']->apellido_materno }}
                              </th>
                              <td>{{ $value['agenda']->start_time }}</td>
                              <td>{{ $value['agenda']->end_time }}</td>
                              <td>
                                <form class="formConsulta" method="post">
                                  @csrf

                                    <input type="hidden" name="id" value="{{ $value['empleado']->id}}">
                                    <input type="hidden" name="motivo" value="{{ $value['agenda']->reason }}" required>
                                    <input type="hidden" name="idAgenda" value="{{ $value['agenda']->id }}">

                                    <button type="submit" class="btn btn-sm btn-primary agendaConsulta" name="button">
                                      <i class="feather icon-external-link"></i>
                                    </button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                       </tbody>
                  </table>
              </div>
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>
  <!-- calendar Modal starts-->
  <div class="modal fade text-left modal-calendar" tabindex="-1" role="dialog" aria-labelledby="cal-modal" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title text-text-bold-600" id="cal-modal">Agendar Consulta</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form method="post" id="appointment">
          <div class="modal-body row">

            <div class="col-md-9">
              <fieldset class="form-label-group">
                <label >Selecciona a un Paciente</label>
                <select class="select2" id="patient" style="width:100%" name="paciente" placeholder="Selecciona a un Paciente" required>
                  <option selected value="no">Selecciona un paciente</option>
                  @foreach ($empleados as $empleado)
                    <option value="{{ $empleado->id }}">
                      {{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}
                    </option>
                  @endforeach
                </select>
              </fieldset>
            </div>

            <div class="col-md-2">
              <a href="{!! route('empleados') !!}" class="btn btn-secondary" name="button">
                <i class="feather icon-user-plus"></i>
              </a>
            </div>

            <div class="col-md-12">
              <fieldset class="form-label-group">
                <input type="text" class="form-control pickadate" id="cal-fecha-date" required placeholder="Fecha" name="date" required>
                <label for="cal-start-date">Fecha</label>
              </fieldset>
            </div>

            <div class="col-md-6">
              <fieldset class="form-label-group">
                <input type="time" class="form-control" id="cal-start-date" placeholder="Fecha" name="startTime" required>
                <label for="cal-start-date">Hora de Inicial</label>
              </fieldset>
            </div>

            <div class="col-md-6">
              <fieldset class="form-label-group">
                <input type="time" class="form-control" id="cal-end-date" placeholder="" name="endTime" required>
                <label for="cal-end-date">Hora Final</label>
              </fieldset>
            </div>

            <div class="col-md-12">
              <fieldset class="form-label-group">
                <input class="form-control" id="cal-description" rows="5" placeholder="Motivo de la Consulta" name="consultation" required>
                <label for="cal-description" >Motivo de la Consulta</label>
              </fieldset>
            </div>
            <div class="col-md-6">
              <div class="custom-control custom-switch mr-2 mb-1">
                <p class="mb-0">Recordar un día antes de la cita.</p>
                <input type="checkbox" class="custom-control-input" id="customSwitch3" name="remember">
                <label class="custom-control-label" for="customSwitch3"></label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="custom-control custom-switch mr-2 mb-1">
                <p class="mb-0">Enviar correo electronico al paciente.</p>
                <input type="checkbox" class="custom-control-input" id="paciente_correo" name="patientRemember">
                <label class="custom-control-label" for="paciente_correo"></label>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button id="spinner" class="btn btn-primary  mb-1 waves-effect waves-light" type="button" disabled="">
              <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
              <span class="sr-only">Loading...</span>
            </button>
            <button type="submit" class="btn btn-primary cal-add-event waves-effect waves-light">
              Agregar Cita
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- calendar Modal ends-->
</section>

<!-- // Full calendar end -->
<div class="modal fade text-left modal-schedule" tabindex="-1" role="dialog" aria-labelledby="cal-modal" aria-modal="true">
  <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="title_modal text-white"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form method="post" id="Uappointment">
        <div class="modal-body row">
          <div class="col-md-2 text-center">
            <img class="round" src="{!! asset('storage/app/empresas/'.\Session::get('empresa')->logo) !!}" alt="avatar" height="90" width="90">
          </div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-5">
                <h6>Clave:</h6>
                <p class="clave m-0"></p>
              </div>
              <div class="col-md-6">
                <h6>Nacimiento:</h6>
                <p class="nacimiento m-0"></p>
              </div>
              <div class="col-md-5">
                <h6>Curp:</h6>
                <p class="curp m-0"></p>
              </div>
              <div class="col-md-6">
                <h6>E-mail:</h6>
                <p class="email m-0"></p>
              </div>
            </div>

            <div class="col-md-12">
              <hr>
            </div>
          </div>

          {{-- ///////////////////////////////////////// --}}
          <input type="hidden" class="date_id" name="date_id">
          <div class="col-md-12 mt-1">
            <fieldset class="form-label-group">
              <input type="text" class="form-control date pickadate"  placeholder="Fecha" name="date">
              <label for="cal-start-date">Fecha</label>
            </fieldset>
          </div>

          <div class="col-md-6">
            <fieldset class="form-label-group">
              <input type="time" class="form-control startTime" required name="startTime">
              <label for="cal-start-date">Hora de Inicial</label>
            </fieldset>
          </div>

          <div class="col-md-6">
            <fieldset class="form-label-group">
              <input type="time" class="form-control endTime" required name="endTime">
              <label for="cal-end-date">Hora Final</label>
            </fieldset>
          </div>

          <div class="col-md-12">
            <fieldset class="form-label-group">
              <input class="form-control consultation" required placeholder="Motivo de la Consulta" name="consultation">
              <label>Motivo de la Consulta</label>
            </fieldset>
          </div>
        </div>
        <div class="modal-footer">
          {{-- <button type="button" class="btn btn-icon btn-icon btn-flat-success mr-1 mb-1 waves-effect waves-light">
            <i class="feather icon-external-link"></i>

          </button> --}}


          @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Agenda')['delete']))
          <button type="button" class="btn btn-danger btn-sm deleteSchedule mr-1 mb-1">
            <i class="feather icon-trash-2"></i>
            Eliminar
          </button>
          @endif

          <button type="submit" class="btn btn-secondary btn-sm mr-1 mb-1 ">
            <i class="feather icon-save"></i>
            Guardar
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END: Content-->

<div class="modal fade text-left detailSchedule" tabindex="-1" role="dialog" aria-labelledby="cal-modal" aria-modal="true" data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">

        <div class="modal-body card m-0 p-0 overlay-img-card text-white">
          <img src="{!! asset('public/img/315891-P8RUWI-712.jpg') !!}" class="card-img" alt="card-img-6">
          <div class="card-img-overlay overlay-black d-flex flex-column justify-content-between">
            <h5 class="card-title text-white d_patient">
            </h5>
            <div class="card-content">

              <div class="row">
                <div class="col-md-6">
                  <h5 class="primary">Clave</h5>
                  <small class="d-clave text-white h6"> </small>
                </div>
                <div class="col-md-6">
                  <h5 class="primary">Curp</h5>
                  <small class="d-curp text-white h6"> </small>
                </div>
                <div class="col-md-6">
                  <h5 class="primary">E-mail</h5>
                  <small class="d-email text-white h6"> </small>
                </div>
                <div class="col-md-6">
                  <h5 class="primary">Nacimiento</h5>
                  <small class="d-nacimiento text-white h6"> </small>
                </div>
              </div>
              <div class="divider divider-primary">
                <div class="divider-text bg-primary">Detalle de la consulta</div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <h5 class="primary">Fecha</h5>
                  <small class="d-fecha text-white h6"> </small>
                </div>
                <div class="col-md-6">
                  <h5 class="primary">Hora Inicial</h5>
                  <small class="d-inicial text-white h6"> </small>
                </div>
                <div class="col-md-6">
                  <h5 class="primary">Hora Final</h5>
                  <small class="d-final text-white h6"> </small>
                </div>
                <div class="col-md-12">
                  <h5 class="primary">Motivo</h5>
                  <small class="d-motivo text-white h6"> </small>
                </div>
              </div>
            </div>
          </div>

        </div>
    </div>
  </div>
</div>
@endsection

@section('page_vendor_js')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="public/vuexy/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="public/vuexy/app-assets/vendors/js/calendar/fullcalendar.min.js"></script>
    <script src="{!! asset('public/lib/locales-all.js') !!}" charset="utf-8"></script>
    <script src="public/vuexy/app-assets/vendors/js/calendar/extensions/daygrid.min.js"></script>
    <script src="public/vuexy/app-assets/vendors/js/calendar/extensions/timegrid.min.js"></script>
    <script src="public/vuexy/app-assets/vendors/js/calendar/extensions/interactions.min.js"></script>
    <script src="public/vuexy/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
    <script src="public/vuexy/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>

@endsection
@section('page_js')
@endsection
@section('js_custom')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{!! asset('public/js/empresa/agenda/agenda.js') !!}"></script>


@endsection
