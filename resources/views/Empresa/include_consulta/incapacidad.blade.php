<section class="incapacidad">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="display-5">Incapacidad</div>
          <div class="custom-control custom-switch custom-control-inline">
              <span class="switch-label mr-1">
                Aplicar Incapacidad
              </span>
              <input type="checkbox" onchange="consultaIncapacidad()" class="custom-control-input" id="incapacidadCheck">
              <label class="custom-control-label" for="incapacidadCheck">
                <span class="switch-text-left">Si </span>
                <span class="switch-text-right">No </span>
              </label>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Fecha Inicial</label>
                <input class="form-control" onchange="consultaIncapacidad()" type="date" id="in_inicial">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Fecha Final</label>
                <input class="form-control" onchange="consultaIncapacidad()" type="date" id="in_final">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Motivo</label>
                <input class="form-control" onkeypress="consultaIncapacidad()" type="text" id="in_motivo">
              </div>
            </div>
          </div>
          <div class="quillIncapacidad">

          </div>

        </div>

      </div>
    </div>
  </div>
</section>
