@extends('layouts.Vuexy')

@section('title','Reclutamiento')


@section('begin_vendor_css')
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">

@endsection
@section('page_css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('css_custom')

@endsection
{{-- BEGIN body html --}}
@section('content')
<div class="">
  <div class="row">
    <div class="col-md-6 col-sm-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">
            Reclutamiento de Personal
            <small class="block">
              Realiza estudios a tu personal de inducción para saber si es apto al puesto.
            </small>
          </h4>
				</div>
				<div class="card-content collapse show">
					<div class="card-body">
            <form class="form" method="POST" enctype="multipart/form-data">
                @csrf
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Personal:</label>
                      <button type="button" class="btn btn-sm block btn-primary" name="button">
                        Agregar
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>


					</div>
				</div>
			</div>
		</div>


    <div class="col-md-6 col-sm-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            Enviar Personal Masivo
          </h4>
        </div>
        <div class="card-content collapse show">
          <div class="card-body">
            {{-- <form class="form" action="{!! route('excel_personal') !!}" method="POST" enctype="multipart/form-data"> --}}

          <form class="form" id="reclutamiento" method="POST" enctype="multipart/form-data">
              @csrf
            <div class="form-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Personal (xslx)</label>
                    <input type="file"  class="form-control" required  name="excel">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label >Estudios</label>
                    <select class="form-control select2" required multiple="multiple" name="grupo">
                      @foreach ($estudios as $estudio)
                        <option value="{{ $estudio->id }}">{{ $estudio->nombre }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

            </div>
              <button type="submit" class="btn btn-sm btn-primary">
                 Agregar
              </button>
          </form>
          </div>
        </div>
      </div>
    </div>

    <div class=" col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            Personal Con estudios pendientes
          </h4>
        </div>
        <div class="card-content collapse show">
          <div class="card-body">
            <table class="table table-striped table-bordered compact">
              <thead>
                <tr>
                  <th>Clave</th>
                  <th>Nombre</th>
                  <th>Apellido Paterno</th>
                  <th>Apellido Materno</th>
                  <th>Curp</th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($reclutamiento as $value)
                    <tr>
                    <td>{{ $value->clave }}</td>
                    <td>{{ $value->nombre }}</td>
                    <td>{{ $value->apellido_paterno }}</td>
                    <td>{{ $value->apellido_materno }}</td>
                    <td>{{ $value->CURP }}</td>
                    <td>
                      <span class="span span-primary">
                        Pendiente
                      </span>
                    </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class=" col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            Personal con Estudios Realizados
          </h4>
        </div>
        <div class="card-content collapse show">
          <div class="card-body">
            <table class="table table-striped table-bordered compact">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Office</th>
                  <th>Age</th>
                  <th>Start date</th>
                  <th>Salary</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tiger Nixon</td>
                  <td>System Architect</td>
                  <td>Edinburgh</td>
                  <td>61</td>
                  <td>2011/04/25</td>
                  <td>$320,800</td>
                </tr>
                <tr>
                  <td>Fiona Green</td>
                  <td>Chief Operating Officer (COO)</td>
                  <td>San Francisco</td>
                  <td>48</td>
                  <td>2010/03/11</td>
                  <td>$850,000</td>
                </tr>
                <tr>
                  <td>Shou Itou</td>
                  <td>Regional Marketing</td>
                  <td>Tokyo</td>
                  <td>20</td>
                  <td>2011/08/14</td>
                  <td>$163,000</td>
                </tr>
                <tr>
                  <td>Michelle House</td>
                  <td>Integration Specialist</td>
                  <td>Sidney</td>
                  <td>37</td>
                  <td>2011/06/02</td>
                  <td>$95,400</td>
                </tr>
                <tr>
                  <td>Suki Burks</td>
                  <td>Developer</td>
                  <td>London</td>
                  <td>53</td>
                  <td>2009/10/22</td>
                  <td>$114,500</td>
                </tr>
                <tr>
                  <td>Prescott Bartlett</td>
                  <td>Technical Author</td>
                  <td>London</td>
                  <td>27</td>
                  <td>2011/05/07</td>
                  <td>$145,000</td>
                </tr>
                <tr>
                  <td>Jennifer Acosta</td>
                  <td>Junior Javascript Developer</td>
                  <td>Edinburgh</td>
                  <td>43</td>
                  <td>2013/02/01</td>
                  <td>$75,650</td>
                </tr>
                <tr>
                  <td>Donna Snider</td>
                  <td>Customer Support</td>
                  <td>New York</td>
                  <td>27</td>
                  <td>2011/01/25</td>
                  <td>$112,000</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



  </div>

</div>
<!-- Modal -->
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="../../app-assets/vendors/js/extensions/sweetalert.min.js"></script>
<script src="../../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
{{-- checkbox --}}
<script src="../../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
<script src="../../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
<!-- BEGIN PAGE LEVEL JS-->

<script src="../../app-assets/js/scripts/tables/datatables/datatable-styling.js"></script>
<script src="{!! asset('public/js/empresa/reclutamiento.js') !!}"></script>
<!-- END PAGE LEVEL JS-->
@endsection
