<fieldset>
  <div class="row">
    <div class="row col-md-12">
      <div class="col-md-3">
        <div class="form-group">
          <label for="">¿Se le tomo muestra al paciente?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->tomaMuestraPaciente == 'Si')
      <div class="col-md-3">
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" checked value="Si" name="tomaMuestraPaciente" id="muestra_paciente_Si">
          <label class="custom-control-label" for="muestra_paciente_Si">Si</label>
        </div>
        <div class="d-inline-block custom-control custom-radio mr-1">
          <input type="radio" class="custom-control-input bg-success" checked value="No" name="tomaMuestraPaciente" id="muestra_paciente_No">
          <label class="custom-control-label" for="muestra_paciente_No">NO</label>
        </div>
      </div>
    </div>
    @else
    <div class="col-md-3">
      <div class="d-inline-block custom-control custom-radio mr-1">
        <input type="radio" class="custom-control-input bg-success" value="Si" name="tomaMuestraPaciente" id="muestra_paciente_Si">
        <label class="custom-control-label" for="muestra_paciente_Si">Si</label>
      </div>
      <div class="d-inline-block custom-control custom-radio mr-1">
        <input type="radio" class="custom-control-input bg-success" checked value="No" name="tomaMuestraPaciente" id="muestra_paciente_No">
        <label class="custom-control-label" for="muestra_paciente_No">NO</label>
      </div>
    </div>
  </div>
  @endif

  <div class="col-md-5">
    <div class="form-group">
      Laboratorio que procesara la muestra
      @if (isset($estudioCaso))
      <input type="text"  name="labProcMuestra" class="form-control" value="{{$estudioCaso->labProcMuestra}}">
      @else
      <input type="text" name="labProcMuestra" class="form-control">
      @endif
    </div>
  </div>
  @php
  $Tipo_muestra = array('EF' => 'Exudado faringeo',
  'EN' => 'Exudado Nasofaringeo',
  'Lb' => 'Lavado bronquial',
  'Bp' => 'Biopsia de pulmon',
  'ExNF' => 'Exudado Nasofaringeo Y Faringio');
  @endphp
  <div class="col-md-3">
    <div class="form-group">
      Tipo de muestra:
      <select name="tipoMuestra" class="form-control" id="tipoMuestra">
        <option value="">Seleccione una opcion</option>
        @if (isset($estudioCaso))
        @foreach ($Tipo_muestra as $key => $value)
        @if ($estudioCaso->tipoMuestra == $key)
        <option selected value={{$key}}>{{$value}}</option>
        @endif
        @endforeach
        @else
        @foreach ($Tipo_muestra as $key => $value)
        <option value={{$key}}>{{$value}}</option>
        @endforeach
        @endif
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      Fecha de toma de muestra:
      @if (isset($estudioCaso))
      <input type="date"  class="form-control" id="fechaTomaMuestra" name="fechaTomaMuestra" value="{{$estudioCaso->fechaTomaMuestra}}">
      @else
      <input type="date" class="form-control" id="fechaTomaMuestra" name="fechaTomaMuestra">
      @endif
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      Fecha del resultado:
      @if (isset($estudioCaso))
      <input type="date"  class="form-control" id="fechaResultado" name="fechaResultado" value="{{$estudioCaso->fechaResultado}}">
      @else
      <input type="date" class="form-control" id="fechaResultado" name="fechaResultado">
      @endif
    </div>
  </div>
  @php
  $resultado = array('POSITIVO' => 'POSITIVO',
  'NEGATIVO' => 'NEGATIVO');
  @endphp
  <div class="col-md-3">
    <div class="form-group">
      Resultado:
      <select name="resultados" class="form-control" id="resultados">
        <option value="">Seleccione una opcion</option>
        @if (isset($estudioCaso))
          @foreach ($resultado as $key => $value)
            @if ($estudioCaso->resultados == $value)
              <option selected value={{$value}}>{{$value}}</option>
            @endif
          @endforeach
        @else
        @foreach ($resultado as $key => $value)
        <option value={{$value}}>{{$value}}</option>
        @endforeach
        @endif
      </select>
    </div>
  </div>
  </div>
</fieldset>
