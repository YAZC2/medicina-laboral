<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Indicadores generales</title>
  {{-- icono --}}
  <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
  {{-- menu --}}
  <link rel="stylesheet" type="text/css" href="../resources/sass/css/style_empresa_menu.css">
  {{-- fontawesome --}}
  <link href="../resources/sass/fontawesome/css/all.css" rel="stylesheet">
  {{-- bootstrap4 --}}
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  {{-- datatables css  --}}
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
  {{-- css custom --}}
  <link rel="stylesheet" type="text/css" href="../resources/sass/css/Empresa/style_indicadores.css">
</head>

<body>
  <!-- End vertical navbar -->
  @include('..layouts.menuEmpresa')
  <!-- Page content holder -->
  <div class="content">

    <a href="#" class="scrollup">
      <i class="fas fa-chevron-circle-up"></i>
    </a>

    <div class="title-content">
      <h1 class="title">Indicadores generales</h1>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"></div>
      <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-sm-8 align-self-center text-center">
              Seleccione el grupo que desea visualizar
                    <select class="border-success nameGrupo" id="nomGrupo" name="nomGrupo">
                      @foreach ($grupos as $key => $item)
                        <option value={{$item->id}}>{{$item->nombre}}</option>
                      @endforeach
                    </select>
            </div>
        </div>
    </div>
      <div class="panel-body">
        <div class="row">
          <div class="col">
            <div class="area-grafica">
              <figure class="highcharts-figure">
                <div class='buttons buttons-enfermedades'>
                  <button id='todos-enfermedades' class='active'>
                    Todos
                  </button>
                  <button id='hombres-enfermedades'>
                    Hombres
                  </button>
                  <button id='mujeres-enfermedades'>
                    Mujeres
                  </button>
                </div>
                <div id="container-enfermedades"></div>
                <p class="highcharts-description"></p>
              </figure>
            </div>
          </div>
          <div class="col">
            <div class="area-grafica">
              <figure class="highcharts-figure">
                <div class='buttons buttons-imc'>
                  <button id='todos-imc' class='active'>
                    Todos
                  </button>
                  <button id='hombres-imc'>
                    Hombres
                  </button>
                  <button id='mujeres-imc'>
                    Mujeres
                  </button>
                </div>
                <div id="container-imc"></div>
                <p class="highcharts-description"></p>
              </figure>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="area-grafica">
              <figure class="highcharts-figure">
                <div id="container-generos"></div>
                <p class="highcharts-description"></p>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="../resources/js/jquery.scrollUp.js"></script>

{{-- Highcharts --}}
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script src="../resources/js/charts-indicadoresGrupos.js"></script>

</html>
