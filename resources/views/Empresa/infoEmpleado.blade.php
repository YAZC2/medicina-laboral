@extends('layouts.Vuexy')
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/animate/animate.css') !!}">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/pages/app-user.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

@endsection
@section('title')
  Expediente: {{$empleado->nombre}} {{$empleado->apellido_paterno}} {{$empleado->apellido_materno}}
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/info_paciente.css') !!}">
@endsection
@section('content')

  <input type="hidden" class="expedienteId" value="{{ $expediente_id }}">
  @if ($incapacidad)
    @php
      $inicial = Carbon::parse($incapacidad->fechaInicial)->format('d-m-Y');
      $final = Carbon::parse($incapacidad->fechaFinal)->format('d-m-Y');
    @endphp
    {{-- @if ($now->format('d-m-Y') < $final->format('d-m-Y')) --}}
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
      <h4 class="alert-heading">Paciente con incapacidad</h4>
      <div class="row">
        <div class="col-md-4">
          <p>Fecha de inició: {{ $inicial }}</p>
          <p class="float-rigth">Fecha final: {{ $final }}</p>
        </div>
        <div class="col-md-8">
          <p>Motivo: {{ $incapacidad->motivo }}</p>
          <p class="float-rigth">Detalles: {{ strip_tags($incapacidad->observacion) }}</p>
        </div>
      </div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">
          <i class="feather icon-x-circle"></i>
        </span>
      </button>
    </div>
    {{-- @endif --}}
  @endif
<section >
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-chevron">
      <li class="breadcrumb-item"><a href="{!! route('empleados') !!}">Empleados</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{$empleado->nombre}} {{$empleado->apellido_paterno}} {{$empleado->apellido_materno}}</li>
    </ol>
  </nav>
</section>
<section class="page-users-view">
  <div class="row">
    @include('Empresa.empleado.informacion')
    @include('Empresa.empleado.signosVitales')
    @include('Empresa.empleado.antecedentes')
    @include('Empresa.empleado.estudios')
  </div>
</section>

  {{-- <!-- Modal -->
@if ($historial_id!=NULL && $hc->origen == 1)
  <div class="modal fade text-left" id="historial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel1">Historial Clinico</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <iframe src="{{route('HistorialPdf',['id'=>$historial_id])}}" width="100%" height="400px"></iframe>
        </div>
      </div>
    </div>
  </div>
@endif --}}

{{-- modal modificar empleado --}}
<div id="empleadoModal" class="modal fade"  role="dialog">
  <div class="modal-dialog modal-dialog-scrollable">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary white">
      <h4 class="modal-title float-left">Editar datos de {{$empleado->nombre}}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <form method="post" id="empleado_form" class="col-md-12" >
          @csrf
          <div class="form-row">
            <div class="form-group col-md-12">
                <input type="hidden" name="empleado_id" value="{{ $encryptedID }}">

                <label for="inputClave">Clave</label>
                <input type="text" name="clave"  class="form-control " value="{{$empleado->clave}}" id="inputClave" required>

                <label for="inputNombre">Nombre(s)</label>
                <input type="text" name="nombre"  class="form-control" value="{{$empleado->nombre}}" id="inputNombre" required>

                <label for="inputPrimerApellido">Primer apellido</label>
                <input type="text" name="primerApellido"  class="form-control" value="{{$empleado->apellido_paterno}}" id="inputPrimerApellido" required>

                <label for="inputSegundoApellido">Segundo apellido</label>
                <input type="text" name="segundoApellido"  class="form-control" value="{{$empleado->apellido_materno}}" id="inputSegundoApellido" required>

                <label for="inputCurp">CURP</label>
                <input type="text" name="curp"  id="curp" class="form-control" value="{{$empleado->CURP}}" id="inputCurp" required>

                <label for="inputGrupo">Grupo</label>
                <select name="grupo" class="form-control" id="inputGrupo" required>
                  @foreach ($grupos as $grupo)
                      @if ($grupo->id == $empleado->grupo_id)
                          <option value="{{ $grupo->nombre }}" selected>{{ $grupo->nombre }}</option>
                      @else
                          <option value="{{ $grupo->nombre }}">{{ $grupo->nombre }}</option>
                      @endif
                  @endforeach
                </select>

                <label for="inputFechaNacimiento">Fecha de nacimiento</label>
                <input type="date" name="fechaNacimiento"  class="form-control" value="{{$empleado->fecha_nacimiento}}" id="inputFechaNacimiento" required>

                <label for="inputEmail">Correo electrónico</label>
                <input type="email" name="email"  class="form-control" value="{{$empleado->email}}" id="inputEmail">

                <label for="inputDireccion">Dirección</label>
                <input type="text" name="direccion"  class="form-control" value="{{$empleado->direccion}}" id="inputDireccion">

                <label for="inputTelefono">Teléfono</label>
                <input type="text" name="telefono"  class="form-control" value="{{$empleado->telefono}}" id="inputTelefono" maxlength="10">
            </div>
          </div>
          <button type="submit" class="btn btn-sm btn-primary" id="btn_enviar">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="consulta" class="modal fade"  role="dialog">
  <div class="modal-dialog modal-dialog-scrollable">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary white">
      <h4 class="modal-title float-left">Motivo de la consulta</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form action="{!! route('consulta') !!}" method="post">
          @csrf
          <div class="form-group">
            <input type="hidden" name="id" value="{{ $empleado->id }}">
            <input type="text" class="form-control" name="motivo" placeholder="Escribe el motivo de la consulta" required>
          </div>
          <hr>
          <div class="col-md-12">
            <button type="submit" class="btn btn-sm float-left btn-primary" name="button">Iniciar</button>
            <button id="consulta_cerrar" class="btn btn-sm ml-1 float-left btn-secondary" name="button">Cerrar</button>

          </div>
        </form>
    </div>
  </div>
</div>
</div>

@include('Empresa.empleado.modals')



@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>


@endsection
@section('page_js')
  <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
  <script src=" {!! asset('public/vuexy/app-assets/js/scripts/modal/components-modal.min.js') !!} "></script>
  <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection
@section('js_custom')

  <script src="{!! asset('public/js/empresa/info_paciente.js') !!}"></script>
  <script src="https://ac-labs.com.mx/asesores/app-assets2/vendors/js/charts/echarts/echarts.js"></script>
  <script src="https://ac-labs.com.mx/asesores/app-assets2/vendors/js/charts/echarts/chart/bar.js"></script>
  <script src="{!! asset('resources/js/charts-empleado.js') !!} "></script>
  <script src="{!! asset('public/js/empresa/antecedente/antecedentes.js') !!}"></script>
  <script src="{{ asset('public/js/empresa/empleado/estudios.js') }}"></script>
  <script src="{!! asset('public/vuexy/app-assets/js/scripts/popover/popover.min.js') !!}" charset="utf-8"></script>
@endsection
