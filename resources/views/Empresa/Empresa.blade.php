@extends('layouts.Vuexy')
@section('title')
    Administrador
@endsection
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">

    <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') !!}">
@endsection
@section('begin_vendor_css')
    <style media="screen">
        #container {
            overflow-x: auto !important;
        }

    </style>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="stylesheet" href="{{ asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') }}">
@endsection
@section('content')
    <section id="dashboard-analytics">
        <div class="row">
            <div class="col-xl-3 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-check text-info font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{ $consultas }}</h2>
                            <p class="mb-0 line-ellipsis">Consultas Terminadas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-success p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="feather icon-users text-success font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{ $personal }}</h2>
                            <p class="mb-0 line-ellipsis">Pacientes</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="fa fa-venus text-danger font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{ $femenino }}</h2>
                            <p class="mb-0 line-ellipsis">Femenino</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="avatar bg-rgba-primary p-50 m-0 mb-1">
                                <div class="avatar-content">
                                    <i class="fa fa-mars secondary font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="text-bold-700">{{ $masculino }}</h2>
                            <p class="mb-0 line-ellipsis">Masculino</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-6">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                    {{-- <div class="col-xl-3 col-md-4 col-sm-6">
                                        <label for="start">Estudio:</label>
                                        <select class="form-control" id="exampleFormControlSelect1" name="estudio">
                                            <option value="">Selecciona un estudio</option>
                                            <option value="109">Glucosa Serica</option>
                                          </select>
                                    </div>
                                        <div class="col-xl-3 col-md-4 col-sm-6">
                                            <label for="start">Fecha Inicial:</label>
                                            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2022-07-22" min="2020-01-01" max="2023-12-31" class="form-control pickadate-disable picker__input picker__input--active">
                                        </div>
                                        <div class="col-xl-3 col-md-4 col-sm-6">
                                            <label for="start">Fecha Final:</label>
                                            <input type="date" id="fecha_final" name="fecha_final" value="2022-07-22" min="2020-01-01" max="2023-12-31" class="form-control pickadate-disable picker__input picker__input--active">
                                        </div>
                                        <div class="col-xl-3 col-md-4 col-sm-6">
                                            
                                            <a data-id="" class="btn btn-secondary  traer_glucosas text-white mt-2 waves-effect waves-light">
                                                        Buscar</a>
                                        </div> --}}
                                        <div class="col-xl-12 col-md-6 col-sm-12 text-center">
                                            <div class="cargar"></div>
                                        </div>
                                        
                                        <div class="col-xl-6 col-md-6 col-sm-12">
                                            <div id="estudios_genero">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-md-6 col-sm-12">
                                            <div id="rango_edad_genero">
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-md-12 col-sm-12 table-responsive">
                                            <div id='tabla_dinamica' class="d-none">
                                            </div>
                                            <div id="cargando" class="">
                                                <div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
                                                <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="animate__animated animate__fadeInRight text-center col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"></h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            <div id="estudios"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="animate__animated animate__fadeInRight text-center col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"></h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            <div id="enfermedades"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="animate__animated animate__fadeInRight text-center col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"></h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            <div id="nopatologicos"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="animate__animated animate__fadeInLeft  col-md-6">
                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <figure class="highcharts-figure" style="overflow-x: auto;">
                                <div id="container"></div>
                                <p class="highcharts-description"></p>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="" text="" id="eurotrocito">


    </section>
@endsection
@section('page_vendor_js')
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}" ></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/cylinder.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/modules/treemap.js"></script>
    <script src="https://code.highcharts.com/modules/funnel3d.js"></script>
    <script src="https://code.highcharts.com/modules/pyramid3d.js"></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!} "></script>
    <script>
        function enfermedades() {
            var categories = [
                "Estreñimiento", "Soplo del corazón", "Hipertensión", "Bronquitis", "Diabetes",
                "Migraña", "Tuberculosis","Gastritis", "Piedras en la vesícula", "Piedras en el riñon", 
                "Hepatitis", "Anemia","Enfermedades de la sangre", "Varicela","Fracturas", "Epilepsia", "Otras"
            ];
           
            Highcharts.setOptions({ 
            colors: [ '#3bafda', '#f15c80']
            });
       
            Highcharts.chart('enfermedades', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Enfermedades Detectadas'
            },
            subtitle: {
                text: ''
            },
            accessibility: {
                point: {
                valueDescriptionFormat: '{index}. Enfermedad {xDescription}, {value}%.'
                }
            },
            xAxis: [{
                categories: categories,
                reversed: false,
                labels: {
                step: 1
                },
                accessibility: {
                description: 'Enfermedades (Masculino)'
                }
            }, {
                opposite: true,
                reversed: false,
                categories: categories,
                linkedTo: 0,
                labels: {
                step: 1
                },
                accessibility: {
                description: 'Enfermedades (Femenino)'
                }
            }],
            yAxis: {
                title: {
                text: null
                },
                labels: {
                formatter: function () {
                    return Math.abs(this.value) + ''
                }
                },
            },

            plotOptions: {
                series: {
                stacking: 'normal'
                }
            },

            tooltip: {
                formatter: function () {
                return '<b>' + this.series.name + ', Estudios ' + this.point.category + '</b><br/>' +
                    'Total: ' + this.point.y;
                }
            },

            series: [{
                name: 'Masculino',
                data: [
                    {{ $estrenimiento }}, {{ $soplo }}, {{ $Hipertension }},
                    {{ $Bronquitis }}, {{ $Diabetes }}, {{ $Migrana }},
                    {{ $Tuberculosis }}, {{ $Gastritis }}, {{ $Vesicula }},
                    {{ $rinon }}, {{ $Hepatitis }}, {{ $Anemia }},
                    {{ $sangre }}, {{ $Varicela }},
                    {{ $Fracturas }}, {{ $Epilepsia }}, {{ $otros }}
                ]
            }, {
                name: 'Fememnino',
                data: [
                    {{ $estrenimientoM }}, {{ $soploM }}, {{ $HipertensionM }},
                    {{ $BronquitisM }}, {{ $DiabetesM }}, {{ $MigranaM }},
                    {{ $TuberculosisM }}, {{ $GastritisM }}, {{ $VesiculaM }},
                    {{ $rinonM }}, {{ $HepatitisM }}, {{ $AnemiaM }},
                    {{ $sangreM }}, {{ $VaricelaM }},
                    {{ $FracturasM }}, {{ $EpilepsiaM }}, {{ $otrosM }}
                ]
            }]
            });
        }
        function nopatologicos() {
            Highcharts.setOptions({
                colors: [ '#f6bb42', '#8085e9','#3bafda']
            });
            Highcharts.chart('nopatologicos', {
                chart: {
                    type: 'pyramid3d',
                    options3d: {
                    enabled: true,
                    alpha: 10,
                    depth: 50,
                    viewDistance: 50
                    }
                },
                title: {
                    text: 'No patologicos'
                },
                plotOptions: {
                    series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        allowOverlap: true,
                        x: 10,
                        y: -5
                    },
                    width: '60%',
                    height: '80%',
                    center: ['50%', '45%']
                    }
                },
                series: [{
                    name: 'No patologicos',
                    data: [
                        ['TABAQUISMO {{ $cigarros }} %', {{ $cigarros }}],
                        ['ALCOHOLISMO {{ $alcholes }} % ', {{ $alcholes }}],
                        ['DROGAS {{ $droguitas }} %', {{ $droguitas }}]
                    ]
                }]
            });
        }
        function estudios() {
            Highcharts.setOptions({
                colors: [ '#3bafda', '#f6bb42','#5d7c90']
            });
            
            Highcharts.chart('estudios', {
                series: [{
                    type: "treemap"
                    , colors: ['#3bafda', '#f6bb42', '#5d7c90']
                    , layoutAlgorithm: 'stripes'
                    , alternateStartingDirection: true
                    , levels: [{
                        level: 1
                        , layoutAlgorithm: 'sliceAndDice'
                        , dataLabels: {
                            enabled: true
                            , align: 'left'
                            , verticalAlign: 'top'
                            , style: {
                                fontSize: '15px'
                                , fontWeight: 'bold'
                            }
                        }
                    }]
                    , data: [{
                        id: 'A'
                        , name: 'Espirometrías'
                       
                    }, {
                        id: 'B'
                        , name: 'Audiometrías'
                        
                    }, {
                        id: 'C'
                        , name: 'Electrocardiograma'
                        
                    }, {
                        name: 'Normales'
                        , parent: 'A'
                        , value:{{ $espironormales }}
                        ,color: '#2fc471'
                    }, {
                        name: 'Anormales'
                        , parent: 'A'
                        , value: {{ $espiroanormales }}
                        , color: '#e55253'
                    }, {
                        name: 'Normales'
                        , parent: 'B'
                        , value: {{ $audiosnormales }}
                        , color: '#2fc471'
                    }, {
                        name: 'Anormales'
                        , parent: 'B'
                        , value: {{ $audiosanormales }}
                        , color: '#e55253'
                    }, {
                        name: 'Normales'
                        , parent: 'C'
                        , value: 9
                        , color: '#2fc471'
                        
                    }, {
                        name: 'Anormales'
                        , parent: 'C'
                        , value: 3
                        , color: '#e55253'
                    }]
                }]
                , title: {
                    text: 'Audiometrías,Espirometrías y Electrocardiogramas Normales y Anormales'
                }
            });
        }
        
    </script>
@endsection

@section('js_custom')

    <script src="{!! asset('public/js/empresa/dash.js') !!}" charset="utf-8"></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!} "></script>
    <script>
  var eritrocito='9sadas'; 
function reporteOxxo(){ 
    $("#tabla_dinamica").empty(); 
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  let codigo= 'SM16';
  let empresa= 'SM CEMEX';
  data.metodo = 'empresa_la';
  data.codigo = codigo;
  data.empresa = empresa;
  data.fecha1='2022-01-01';
  data.fecha2='2022-04-30';
  // data.idExamen='109';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
        $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
    }
  }).done(res => {
    $(".cargar").empty();
    
    $('#tabla_dinamica').append(`
    <div class='row'>            
                                        <div class="col-xl-4 col-md-4 col-sm-6">
                                            <label for="start">Fecha Inicial:</label>
                                            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2022-07-22" min="2020-01-01" max="2023-12-31" class="form-control pickadate-disable picker__input picker__input--active">
                                        </div>
                                        <div class="col-xl-4 col-md-4 col-sm-6">
                                            <label for="start">Fecha Final:</label>
                                            <input type="date" id="fecha_final" name="fecha_final" value="2022-08-22" min="2020-01-01" max="2023-12-31" class="form-control pickadate-disable picker__input picker__input--active">
                                        </div>
                                        <div class="col-xl-4 col-md-4 col-sm-6">
                                            
                                            <a data-id="" class="btn btn-secondary  traer_glucosas text-white mt-2 waves-effect waves-light">
                                                        Buscar</a>
                                        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%">
    <thead>
        <th>NIM</th>
        <th>NOMBRE</th>
        <th>ERITROCITOS</th>
          <th>HEMOGLOBINA</th>
          <th>HEMATOCRITO</th>
          <th>VOLUMEN PLAQUETARIO MEDIO (VPM)</th>
          <th>LEUCOCITOS TOTALES</th>
          <th>BASOFILOS</th>
          <th>EOSINOFILOS</th>
          <th>LINFOCITOS</th>
          <th>GLUCOSA</th>
          <th>CREATININA</th>
          <th>UREA</th>
          <th>ACIDO URICO</th>
          <th>COLESTEROL</th>
          <th>TRIGLICERIDOS</th>
          <th>GLUCOSILADA</th>
        </thead>
      <tbody class="d-none"></tbody></table>`);
      
    res.pacientes.forEach((item, i) => {
       
      resultado_captura(item.Nim,item.nombre,item.apellido);
     
    });
    
    $("tbody").removeClass("d-none"); 
  }).fail(err => {

  })
}
$( document ).ready(function() {
    reporteOxxo();
    window.setTimeout(aparecetabla, 3000);
});
function nueva_tablainicial(){
    $(".table_estudios").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }

  });
}
function resultado_captura(Nim,nombre,apellido){
  data = new Object();
  nim = Nim.split('/');				
  nim1 = nim[0];			
  nim2 = nim[1];
  data.metodo = 'reporte';
  data.toma = nim1;
  data.consecutivo = nim2;
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php'
   
  }).done(res => {
   
    $(".table_estudios1").dataTable().fnDestroy();
    for (x of res) {
      $('tbody').append(`<tr>
        <td>${nombre} ${apellido}</td>
          <th>${Nim}</th>
        <td>${x.eritrocito}</td>
          <th>${x.hemoglobina}</th>
          <th>${x.hematocritico}</th>
          <th>${x.volumenplaquetario}</th>
          <th>${x.leucocitos}</th>
          <th>${x.basofilos}</th>
          <th>${x.eosinofilos}</th>
          <th>${x.linfocitos}</th>
          <th>${x.glucosa}</th>
          <th>${x.creatinina}</th>
          <th>${x.urea}</th>
          <th>${x.urico}</th>
          <th>${x.colesterol}</th>
          <th>${x.trigleceridos}</th>
          <th>${x.glucosilada}</th>
      </tr>`);
      
    
    }
    nueva_tabla();
   
  
   

  }).fail(err => {

  })

}

function aparecetabla(){
    $("#tabla_dinamica").removeClass("d-none");
    $("#cargando").addClass("d-none");
}

$(document).on('click', '.traer_glucosas', function () {
  fecha_inicial = $("input[name=fecha_inicial]").val();
  fecha_final = $("input[name=fecha_final]").val();
  estudio=$("#estudio_consulta").val();
  estudio_laboratorio(fecha_inicial, fecha_final);

});
function estudio_laboratorio(fecha_inicial, fecha_final){
    $("#tabla_dinamica").empty(); 
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  let codigo= 'SM16';
  let empresa= 'SM CEMEX';
  data.metodo = 'empresa_la';
  data.codigo = codigo;
  data.empresa = empresa;
  data.fecha1=fecha_inicial;
  data.fecha2=fecha_final;
  // data.idExamen='109';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
        $(".cargar").append(`<div class="spinner-border primary" style="width: 3rem; height: 3rem;" role="status">
      <span class="sr-only">Loading...</span>
      </div>`);
    }
  }).done(res => {
    $(".cargar").empty();
    
    $('#tabla_dinamica').append(`
    <div class='row'>            
                                        <div class="col-xl-4 col-md-4 col-sm-6">
                                            <label for="start">Fecha Inicial:</label>
                                            <input type="date" id="fecha_inicial" name="fecha_inicial" value="2022-07-22" min="2020-01-01" max="2023-12-31" class="form-control pickadate-disable picker__input picker__input--active">
                                        </div>
                                        <div class="col-xl-4 col-md-4 col-sm-6">
                                            <label for="start">Fecha Final:</label>
                                            <input type="date" id="fecha_final" name="fecha_final" value="2022-08-22" min="2020-01-01" max="2023-12-31" class="form-control pickadate-disable picker__input picker__input--active">
                                        </div>
                                        <div class="col-xl-4 col-md-4 col-sm-6">
                                            
                                            <a data-id="" class="btn btn-secondary  traer_glucosas text-white mt-2 waves-effect waves-light">
                                                        Buscar</a>
                                        </div>
    </div>`);
   
    $('#tabla_dinamica').append(`<table class="table_estudios1 table" style="width:100%">
    <thead>
        <th>NIM</th>
        <th>NOMBRE</th>
        <th>ERITROCITOS</th>
          <th>HEMOGLOBINA</th>
          <th>HEMATOCRITO</th>
          <th>VOLUMEN PLAQUETARIO MEDIO (VPM)</th>
          <th>LEUCOCITOS TOTALES</th>
          <th>BASOFILOS</th>
          <th>EOSINOFILOS</th>
          <th>LINFOCITOS</th>
          <th>GLUCOSA</th>
          <th>CREATININA</th>
          <th>UREA</th>
          <th>ACIDO URICO</th>
          <th>COLESTEROL</th>
          <th>TRIGLICERIDOS</th>
          <th>GLUCOSILADA</th>
        </thead>
      <tbody class="d-none"></tbody></table>`);
      
    res.pacientes.forEach((item, i) => {
       
      resultado_captura(item.Nim,item.nombre,item.apellido);
     
    });
    
    $("tbody").removeClass("d-none"); 
  }).fail(err => {

  })
}
function nueva_tabla() {
  $(".table_estudios1").DataTable({
    responsive: !1,
    language: {
      "decimal": "",
      "emptyTable": "No hay información",
      "info": "",
      "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": "_MENU_",
      "loadingRecords": "Cargando...",
      "processing": "Procesando...",
      "search": "Buscar",
      "zeroRecords": "Sin resultados encontrados",
      "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
      }
    }

  });


  function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

}
    </script>
   


@endsection
