@extends('layouts.VuexyLaboratorio')
@section('title', 'Resultados Empresa')
@section('begin_vendor_css')
    <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('css_custom')

@endsection
@section('content')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Buscar Empleado</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class='row'>
                                <div class='col-md-4'>
                                    <input id='id_sass' name='id_sass' class="form-control input" type="text"
                                        placeholder="Buscar..." tabindex="-1" data-search="search" value=''>
                                </div>
                                <div class='col-md-8'>
                                    <button type="button" class="btn btn-primary waves-effect waves-float waves-light"
                                        id="busca_empleado" onclick="obtener_estudios()">Buscar</button>
                                </div>
                            </div>
                            <div class='row'>
                            <div class='col-md-12 mt-2'>
                                <div id='datos'>
                                </div> 
                                <div id="estudios">
                                     <div class='col-md-12' id='busqueda'>     
                                        </div>
                                    <div class="row list"></div>
                                    <div class="col-md-12 mt-1">
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div id="anterior"></div>
                                            <ul class="pagination justify-content-center m-0"></ul>
                                            <div id="siguiente"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class='boton col-md-12'>
                                    
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('page_vendor_js')
    <script src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
    <script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
    <script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.1/list.min.js"></script>

@endsection

@section('page_js')

@endsection
@section('js_custom')

    <script src="{!! asset('public/js/empresa/empleado/resultados.js') !!}" charset="utf-8"></script>
    

@endsection
