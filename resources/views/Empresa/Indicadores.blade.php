@extends('layouts.Vuexy')

@section('content')
  <div class="panel-body">
      <div class="row">
          <div class="col-6">
              <div class="area-grafica">
                  <figure class="highcharts-figure">
                      <div class='buttons buttons-enfermedades'>
                          <button class="btn btn-sm btn-primary" id='todos-enfermedades' class='active'>
                              Todos
                          </button>
                          <button class="btn btn-sm btn-primary" id='hombres-enfermedades'>
                              Hombres
                          </button>
                          <button class="btn btn-sm btn-primary" id='mujeres-enfermedades'>
                              Mujeres
                          </button>
                      </div>
                      <div id="container-enfermedades"></div>
                      <p class="highcharts-description"></p>
                  </figure>
              </div>
          </div>
          <div class="col-6">
              <div class="area-grafica">
                  <figure class="highcharts-figure">
                      <div class='buttons buttons-imc'>
                          <button class="btn btn-sm btn-primary" id='todos-imc' class='active'>
                              Todos
                          </button>
                          <button class="btn btn-sm btn-primary" id='hombres-imc'>
                              Hombres
                          </button>
                          <button class="btn btn-sm btn-primary" id='mujeres-imc'>
                              Mujeres
                          </button>
                      </div>
                      <div id="container-imc"></div>
                      <p class="highcharts-description"></p>
                  </figure>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-12">
              <div class="area-grafica">
                  <figure class="highcharts-figure">
                      <div id="container-generos"></div>
                      <p class="highcharts-description"></p>
                    </figure>
              </div>
          </div>
      </div>
  </div>

@endsection
@section('js_custom')
  {{-- Highcharts --}}
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>

  <script src="{!! asset('js/empresa/charts_indicadores.js') !!}"></script>
@endsection
