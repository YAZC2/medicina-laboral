
$('#alert').hide();
$('#alert_success').hide();
$(".search").keyup( function() {
    var contador = 0;
    var rex = new RegExp($(this).val(), 'i');
    $(".cards_item").hide();
    $(".cards_item").filter( function() {
        if (rex.test($(this).text()))
        contador ++;
        return rex.test($(this).text());
    }).show();
    if (contador > 0) {
        $(".aviso-vacio").hide();
    } else {
        $(".aviso-vacio").show();
    }
});

$('#realizado').on('submit',function(e){
    e.preventDefault();
    $.ajax({
      type:'post',
      dataType:'json',
      data:$('#realizado').serialize(),
      url:'../EstudioRealizado',
      beforeSend:function() {
        $('#realizado').hide();
        $('#load').show();
        $('.regresar').hide();
        $('.close').hide();
      }
    }).done(function(Response) {
      console.log(Response);
        setTimeout(function() {
          $('#load').hide();
          $('#estudios').modal('hide');
          $('.regresar').show();
          $('.close').show();
          $('#realizado').show();
          document.getElementById(Response);
          var selector = '#'+Response;
          $(selector).remove();
        },2000);

    }).fail(function(error) {

      if(error.status==401){
        $('#alert').text('No hay ningun archivo para el estudio');
        $('#load').hide();
        $('#alert').show();
        setTimeout(function(){
          $('.regresar').show();
          $('.close').show();
            $('#alert').hide(400);
            $('#realizado').show();
            $('#alert').text('Algo a ocurrido, verifica tu conexión o intentalo mas tarde...');
        },2100)
      }
        $('#load').hide();
        $('#alert').show();
        setTimeout(function(){
            $('#alert').hide(700, "swing");
            $('#realizado').show();
        },2100)
    });
});

function estudioPrev(nombre, id, ingreso, curp,estudio_id,estudio){
  $('#load').hide();
  $('#estudio_title').text(estudio);
  $('#id_encrypt').val(id);
  $('#estudio_id').val(estudio_id);
  $('#id_encrypt_2').val(id);
  $('#curp').val(curp);
  $('#ingreso_encrypt').val(ingreso);
  $('#estudios').modal('show');
  $("#formuploadajax")[0].reset();
}

function finalizar(){
  $(".ocult").hide();
  $('#realizado').show();
  $('.finalizar').hide();
  $('.regresar').show();
}
function regresar(){
  $('.finalizar').show();
  $('.regresar').hide();
  $(".ocult").show();
  $('#realizado').hide();
}


$("#formuploadajax").on("submit", function(e){
 e.preventDefault();
 $.ajax({
     url: "../Dicom",
     type: "post",
     dataType: "json",
     data: $('#formuploadajax').serialize(),
     beforeSend:function() {
       $('#cerrarModal').hide();
       $('#formuploadajax').hide();
       $('#load').show();
       $('.finalizar').hide();
       $('.close').hide();
     }
 }).done(function(response){
   $('#load').hide();
   nim = $("#nim").val();
   $("#formuploadajax")[0].reset();
   $("#nim").val(nim);
   $('#alert_success').show();

   setTimeout(function() {
     $('#cerrarModal').show();
     $('.finalizar').show();
     $('.close').show();
     $('#alert_success').hide();
     $('#formuploadajax').show();
   },1800);
   }).fail(function(e){
     $('#load').hide();
     $('#alert').show();
     $('#cerrarModal').show();
     setTimeout(function() {
       $('#formuploadajax').show();
       $('.close').show();

       $('.finalizar').show();
       $('#alert').hide();
     },2000);
   });
});
