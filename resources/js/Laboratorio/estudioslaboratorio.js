//LISTA ESTUDIOS PROGRAMADOS
var options = {
  valueNames: ['folio', 'fecha', 'status'],
  page: 4,
  pagination: true,
  pagination: {
    innerWindow: 1,
    left: 1,
    right: 1,
    paginationClass: "pagination",
    item: "<li class='page-item'><a class='page-link page' ></a></li>",
  }
};

var pacientes = new List('estudios_pro', options);
//BUSCAR ESTUDIOS
function modal_estudio() {
  $('#newestudio').modal('show');
}
let estudiosempleado = [];
let folio;
let fechaalta;
function obtener_estudios() {
  $("#estudiosass").empty();
  $("#datos").empty();
  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  data.id_sass = $("input[name=nim_sass]").val();
  folio = data.id_sass;
  data.metodo = 'estudios_generales';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
    console.log(res);
    console.log("si trae");
    console.log(res.nombre);
    nombre = res.nombre;
    examenes = res.examenes;
    fechaalta = res.fechaalta;
    console.log(res.apellidos);
    apellidos = res.apellidos;
    examenes = res.examenes;
    edad = res.edad;
    if (res.codigo_sexo == 2) {
      sexo = 'Femenino';
    }
    else {
      sexo = 'Masculino';
    }

    if (nombre != '' || apellidos != '' || examenes == '[]') {
     // if (item.estudios.nombre == 'ELECTROCARDIOGRAMA DINAMICO' || item.estudios.nombre == 'ELECTROCARDIOGRAMA SOLO TRAZO' || item.estudios.nombre == 'ELECTROCARDIOGRAMA CON INTERPRETACIÓN' || item.estudios.nombre == 'AUDIOMETRIA TONAL Y AEREA' || item.estudios.nombre == 'ESPIROMETRIA' || item.estudios.nombre == 'ESPIROMETRIA CON BRONCODILATADOR') {
        
      console.log(examenes);

      examenes.forEach((item, i) => {
       estudiosempleado.push(item.estudios.id_examen);
        $("#estudiosass").append(`
                    <div class="col-lg-4 ml-md-auto mr-md-auto mt-2">
                        <div class="card">
                            <div class="text-center">
                            <img src="https://img.icons8.com/ultraviolet/40/000000/submit-resume.png"/>  
                            </div>  
                            <div class="">
                            </div>
                            <div class="">
                            <h6 class='primary nombre text-center'>${item.estudios.nombre}</h6>
                            </div>                        
                        </div>
                    </div>`);

      });
    
      $("#estudiosass").append(`
        <div class="col-md-12 text-center">
        <button id="btnagregarempleado" type="button" onclick="programarestudio();" class="btn btn-sm btn-secondary waves-effect waves-float waves-light">Programar Estudio</button>
        </div>
        `);

      console.log("El arreglo:" + estudiosempleado)
    

    }
    else {
     
      $("#datos").append(`<div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Error</h4>
        <div class="alert-body">
        No se encontró ningun paciente con ese NIM
        </div>
      </div>`)

    }

  }).fail(err => {
    $("#datos").empty();
    $("#datos").append(`<div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Error</h4>
      <div class="alert-body">
      No se encontró ningun paciente
      </div>
    </div>`);
  })

}

function programarestudio() {
  // curp = document.getElementById('curp').value;
  id_paciente = document.getElementById('id_paciente').value;
  empresa_id = document.getElementById('empresa_id').value;
  // fecha_nacimiento=document.getElementById('fecha_nacimiento').value;
  // alert(estudiosempleado);
  // alert(fechaalta);
  $.ajax({
    type: 'post',
    dataType: 'json',
    url: '../programarestudio',
    data: { id_paciente: id_paciente, estudiosempleado: estudiosempleado, folio: folio, empresa_id: empresa_id, fechaalta: fechaalta },
  }).done(function (response) {
    swal({
      position: 'top-end',
      icon: 'success',
      text: "El estudio ha sido programado correctamente",
      title: 'Paciente Registrado',
      showConfirmButton: true
    }).then(isConfirm => {
      location.reload();
    })
  }).fail(function (error) {
    var message;
    if(error.confirm='404'){
      message = "El nim ya está registrado...";
      swal('Error', message, 'error')
    }
    else{
      message = "Existe un error,llame al de sistemas...";
    swal('Error', message, 'error')
    }

    


  });
}

//VISUALIZAR ESTUDIOS PROGRAMADOS

$(document).on('click', '.showEstudio', function () {
  console.log();
  $('#estudiosShow').modal('show');
  showEstudios($(this).data('id'));
});


function daicom(tomaid, id_estudio) {
  url = "../dai2/" + tomaid + "/" + id_estudio;
  window.location.href = url;
}

function showEstudios(tomaId) {
  curp = document.getElementById('curp').value;
  $.ajax({
    type: "get",
    url: "../getEstudiosPacienteToma/" + tomaId,
    dataType: "json",
    beforeSend: () => {
      $('.loadEstudios').show();
    }
  }).done(res => {
   

    $('.loadEstudios').hide();
    $('.contEstudios').empty();
    toma = res.toma;
    
    // laboratorio=pdf_laboratorio(toma.folio);
    estudios = res.estudios;
    console.log(toma);
    console.log(estudios);
    estudios.forEach((item, i) => {
      // if (item.categoria_id == 2) {
      //   e_clase = '<a onclick="daicom(' + toma.id + "," + item.id + ')" class="btn-sm btn-secondary btn text-white"><i class="feather icon-airplay"></i></a>';
      // }
      // else {
      //   e_clase = '<a type="button" target="_blank" href="./Resultados/' + toma.id + '/' + item.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-external-link"></i></a>';
      // }
      if (item.nombre == 'ELECTROCARDIOGRAMA CON INTERPRETACIÓN') {
        e_clase = '<a type="button" target="_blank" href="../electrocardiograma/' + curp + '/' + toma.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-activity"></i></a>';
      }
      else if (item.nombre == 'ESPIROMETRIA') {

        e_clase = '<a type="button" target="_blank" href="../EstudioEspirometria/' + curp + '/' + toma.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-cast"></i></a>';
      }
      else if (item.nombre == 'AUDIOMETRIA TONAL Y AEREA') {
        e_clase = '<a type="button" target="_blank" href="../Audiometria/' + curp + '/' + toma.id + '" class="btn-sm btn-secondary btn" name="button"><i class="feather icon-headphones"></i></a>';
      }
      else{
        e_clase = `<a type="button" target="_blank" href="../Fisioterapia/${curp}/${toma.id}" class="btn-sm btn-secondary btn" name="button">
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
          width="16" height="16"
          viewBox="0 0 172 172"
          style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><path d="M95.92308,6.61538c-9.12199,0 -16.53846,7.41647 -16.53846,16.53846c0,9.12199 7.41647,16.53846 16.53846,16.53846c9.12199,0 16.53846,-7.41647 16.53846,-16.53846c0,-9.12199 -7.41647,-16.53846 -16.53846,-16.53846zM86,46.30769c-7.28726,0 -13.23077,5.94351 -13.23077,13.23077v52.92308h13.23077l8.68269,-9.30288l-8.68269,-5.99519c-2.79087,-2.22235 -3.07512,-5.73678 -2.6875,-9.50962c1.16286,1.9381 2.97176,3.61779 4.75481,4.96154l19.84615,11.99038c1.31791,0.67188 1.98978,0.62019 3.30769,0.62019c1.98978,0 4.47055,-1.31791 5.78846,-3.30769c1.98978,-2.63581 0.82692,-6.69291 -2.48077,-8.68269l-19.84615,-11.78365l-5.99519,-22.32692l10.54327,21.5l11.16346,5.375h8.68269v-17.77885l15.91827,17.77885h17.15865l-29.14904,-35.14423c-2.63581,-2.63581 -5.94351,-4.54808 -9.92308,-4.54808zM120.31731,92.61538c2.63582,3.30769 4.13462,9.25121 0.82692,13.23077c-1.98978,2.63582 -5.375,4.54808 -8.68269,4.54808c-3.97956,0 -13.23077,-4.54808 -13.23077,-4.54808l-12.19712,13.23077h-40.72596c-7.28726,0 -13.23077,5.94351 -13.23077,13.23077c0,7.28726 5.94351,13.23077 13.23077,13.23077h46.30769c3.97957,0 6.61538,-2.63581 6.61538,-6.61538c0,-3.97956 -2.63581,-6.61538 -6.61538,-6.61538l-39.69231,-3.92788l39.69231,0.62019c7.28726,0 13.23077,2.63582 13.23077,9.92308c0,2.63582 -0.7494,4.6256 -2.06731,6.61538h59.53846c4.6256,0 8.68269,-4.05709 8.68269,-8.68269c0,-4.6256 -3.23017,-7.80408 -7.85577,-8.47596l-55.40385,-3.10096l10.95673,-14.88462l29.14904,-1.86058c3.97957,0 7.85577,-4.08293 7.85577,-8.0625c0,-3.97956 -3.85036,-7.85577 -8.47596,-7.85577zM16.53846,105.84615c-9.12199,0 -16.53846,7.41647 -16.53846,16.53846c0,9.12199 7.41647,16.53846 16.53846,16.53846c9.12199,0 16.53846,-7.41647 16.53846,-16.53846c0,-9.12199 -7.41647,-16.53846 -16.53846,-16.53846zM6.61538,145.53846c-3.97956,0 -6.61538,3.30769 -6.61538,6.61538v13.23077h172v-13.23077h-138.92308l-4.54808,-4.54808c-1.31791,-1.31791 -2.76503,-2.06731 -4.75481,-2.06731z"></path></g></g></svg>
        </a>`;
      }
      if(item.nombre != 'VALORACIÓN MÉDICA'){
        $('.contEstudios').append(`
            <div class="col-md-4 mb-1">
                <div class="text-center shadow bg-white p-1  rounded">
                    <h5 class='secondary'>${item.nombre}</h5>
                  
                    <hr>
                    <small class="d-block">${toma.created_at}</small>
                    <small class="d-block">${item.seccion == null ? '' : item.seccion}</small>           
                    <hr>
                    ${e_clase}

                </div>
            </div>
        `)
      }

    });
  }).fail(err => {
    $('.loadEstudios').hide();
    console.log(err);
    swal("Error", "Algo ocurrio, Intentalo más tarde", "error");
  })
}

//EDITAR ESTUDIOS PROGRAMADOS

$(document).on('click', '.editar_estudio', function () {
  $('#edit_estudio').modal('show');
  estudios_sass_actualizados($(this).data('id'), $(this).data('nim'));

});
function estudios_sass_actualizados(id, nim) {
  $("#actualiza_es").empty();
  $("#Actualiza_es").empty();

  nombre = '';
  apellido = '';
  edad = '';
  fechaalta = '';
  estudiosempleado = [];
  folio = '';
  data = new Object();
  data.id_sass = nim;
  folio = data.id_sass;
  data.metodo = 'estudios_generales';
  
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
    console.log("si trae");
    console.log(res.nombre);
    nombre = res.nombre;
    fechaalta = res.fechaalta;
    console.log(res.apellidos);
    apellidos = res.apellidos;
    examenes = res.examenes;
    edad = res.edad;
    if (res.codigo_sexo == 2) {
      sexo = 'Femenino';
    }
    else {
      sexo = 'Masculino';
    }

    if (nombre != '' || apellidos != '') {
      console.log(examenes);

      examenes.forEach((item, i) => {
        // if (item.estudios.nombre == 'ELECTROCARDIOGRAMA DINAMICO' || item.estudios.nombre == 'ELECTROCARDIOGRAMA SOLO TRAZO' || item.estudios.nombre == 'ELECTROCARDIOGRAMA CON INTERPRETACIÓN' || item.estudios.nombre == 'AUDIOMETRIA TONAL Y AEREA' || item.estudios.nombre == 'ESPIROMETRIA' || item.estudios.nombre == 'ESPIROMETRIA CON BRONCODILATADOR') {
        estudiosempleado.push(item.estudios.id_examen);

        $("#actualiza_es").append(`
                    <div class="col-lg-4 ml-md-auto mr-md-auto mt-2">
                        <div class="card">
                            <div class="text-center">
                            <img src="https://img.icons8.com/ultraviolet/40/000000/submit-resume.png"/>
                            </div>  
                            <div class="">
                            </div>
                            <div class="">
                            <h6 class='primary nombre text-center'>${item.estudios.nombre}</h6>
                            </div>                        
                        </div>
                    </div>`);

      });
      $("#actualiza_es").append(`
        <div class="col-md-12 text-center">
        <button id="btnagregarempleado" type="button" onclick="actualiza_estudio(${id});" class="btn btn-sm btn-secondary waves-effect waves-float waves-light" onclick="">Actualizar Estudio</button>
        </div>
        `);

      console.log("El arreglo:" + estudiosempleado)

    }
    else {
      $("#Actualiza_es").empty();
      $("#Actualiza_es").append(`<div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Error</h4>
        <div class="alert-body">
        No se encontró ningun paciente con ese NIM
        </div>
      </div>`)

    }

  }).fail(err => {
    $("#Actualiza_es").empty();
    $("#Actualiza_es").append(`<div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Error</h4>
      <div class="alert-body">
      No se encontró ningun paciente con el nim ${data}
      </div>
    </div>`);
    console.log(data);
  })

}
function actualiza_estudio(id) {
  swal({
    title: "Editar",
    text: "¿Estas seguro de editar los Estudios Programados?",
    icon: "warning",
    buttons: {
      cancel: {
        text: "No",
        value: null,
        visible: true,
        className: "",
        closeModal: false,
      },
      confirm: {
        text: "Si",
        value: true,
        visible: true,
        className: "",
        closeModal: false
      }
    }
  }).then(isConfirm => {
    if (isConfirm) {
      actualizar_estudio(id);
    } else {
      swal("Cancelado", "La operación ha sido cancelada", "error");
    }
  });
}
function actualizar_estudio(id) {
  // curp = document.getElementById('curp').value;
  id_paciente = document.getElementById('id_paciente').value;
  empresa_id = document.getElementById('empresa_id').value;
  // fecha_nacimiento=document.getElementById('fecha_nacimiento').value;
  // alert(estudiosempleado);
  // alert(fechaalta);
  
  $.ajax({
    type: 'post',
    dataType: 'json',
    url: '../editar_estudio/'+id,
    data: { id_paciente: id_paciente, estudiosempleado: estudiosempleado, folio: folio, empresa_id: empresa_id, fechaalta: fechaalta },
  }).done(function (response) {
    swal({
      position: 'top-end',
      icon: 'success',
      text: "El estudio ha sido actualizado correctamente",
      title: 'Estudios Programados Actualizados',
      showConfirmButton: true
    }).then(isConfirm => {
      location.reload();
    })
  }).fail(function (error) {
    var message;

    message = "Existe un error,llame al de sistemas...";
    swal('Error', message, 'error')


  });

}
function api() {

  // data = new Object();
  // data.db='101';
  // data.client=1003560;
  // data.Id_admision=1662;
  // data.Request='get';
  // data.Episodio='1';
  $.ajax({
    type: 'get',
    //data: data,
    dataType: 'json',
    url: 'https://Ael.sass.com.mx/ws/index.asp?db=ael',
    
// url:"https://www.sass-l.com.mx/ws/medicos_ws.php?db='2'",
    // url:'https://www.sass-l.com.mx:80/ws/medicos_ws.php',
    beforeSend: () => {
    }
  }).done(res => {

    console.log(res);


  }).fail(err => {
   console.log(err);
  })

}

//ELIMINAR ESTUDIOS PROGRAMADOS

$(document).on('click', '.elimar_estudio', function () {
  eliminar($(this).data('id'));
});
function eliminar(id) {
  swal({
    title: "Eliminación",
    text: "¿Estas seguro de eliminar los datos?",
    icon: "warning",
    buttons: {
      cancel: {
        text: "No",
        value: null,
        visible: true,
        className: "",
        closeModal: false,
      },
      confirm: {
        text: "Si",
        value: true,
        visible: true,
        className: "",
        closeModal: false
      }
    }
  }).then(isConfirm => {
    if (isConfirm) {
      eliminacion(id);
    } else {
      swal("Cancelado", "La operación ha sido cancelada", "error");
    }
  });

}

function eliminacion(id) {
  // alert(id);
  $.ajax({
    type: 'POST',
    url: '../delete_estudio_pro/' + id,
    dataType: 'json',
    beforeSend: () => {
    },
    success: function (response) {
      swal({
        position: 'top-end',
        icon: 'success',
        text: 'Los datos se han eliminado correctamente',
        title: 'NIM Eliminado',
        showConfirmButton: true
      }).then(isConfirm => {
        location.reload();
      })

    }
  }).fail(function (error) {
    $('#btnedit').removeAttr('disabled');

    swal('Error', 'Ocurrio algo inesperado, intentalo más tarde', 'error');

  });
}
//PDF ESTUDIOS LABORATORIO
$(document).on('click', '.estudios_laboratorio', function () {
  pdf_laboratorio($(this).data('folio'));
});
function pdf_laboratorio(nim){
  data = new Object();
  data.id_sass ="101211024/0031";
  folio = data.id_sass;
  data.metodo = 'api';
  $.ajax({
    type: 'post',
    data: data,
    dataType: 'json',
    url: 'https://asesores.ac-labs.com.mx/sass/index.php',
    beforeSend: () => {
    }
  }).done(res => {
    console.log(res.codigo_toma);
    if(res.codigo_toma==nim){
      window.open("../labo_pdf");
    }
    else{
     
      swal("Cancelado", "Este nim no tiene ningún archivo de laboratorio", "warning");
    }

  

  }).fail(err => {
   
    console.log(data);
  })

}

