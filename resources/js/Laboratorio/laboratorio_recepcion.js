$(".campo-busqueda").keyup( function() {
    var contador = 0;
    var rex = new RegExp($(this).val(), 'i');

    $(".card-empresa").hide();

    $(".card-empresa").filter( function() {
        if (rex.test($(this).text()))
            contador ++;

        return rex.test($(this).text());
    }).show();
    
    if (contador > 0) {
        $(".aviso-vacio").hide();
    } else {
        $(".aviso-vacio").show();
    }
});