$(".number-tab-steps").steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  enableAllSteps: true,
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Guardar',
    previous: 'Anterior',
    next: 'Siguiente'
  },
  onFinished: function (event, currentIndex) {
    swal({
      title: "Confirmación de envio",
      text: "¿Seguro que desea terminar el historial clinico?",
      icon: "warning",
      buttons: {
        cancel: {
          text: "Cancelar",
          value: null,
          visible: true,
          className: "",
          closeModal: false,
        },
        confirm: {
          text: "Guardar",
          value: true,
          visible: true,
          className: "",
          closeModal: false
        }
      }
    }).then(isConfirm => {
      if (isConfirm) {
        $('#form').submit();
      } else {
        swal("Operacion Cancelada", "Puede continuar...", "error");
      }
    });

  }

});

$("#form").on('submit', (e) => {
  e.preventDefault();
  var formulario = $("#form").serialize();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    data: $("#form").serialize(),
    url: '././../savefisio'
  }).done((res) => {
    console.log(formulario);
    console.log(res);
    $('#wizard').find('a[href="#finish"]').remove();
    
    swal({
      position: 'top-end',
      icon: 'success',
      title: "Registro Correctamente",
      showConfirmButton: true
    }).then(isConfirm => {
      // window.location.href = '../Pacientes-Medico/'+res.empresa_id;
      window.location.reload();
    });
  }).fail((err) => {
    if (err.responseText.indexOf('estado_salud') > -1) {
      tpl = ` <div class="alert alert-danger" id="AlertaResultado" >
                  Faltan campos por llenar
              </div>`;
      $('#AlertaResultado').html(tpl);
      $(".alert").delay(4000).slideUp(300, function () {
        $(this).alert('close');
      });
      $('#estado_salud').css({
        'color': 'blank',
        'background': '#ff000060'
      });
    }

    if (err.status == 0) {
      swal("Algo ha ocurrido", "Verifica tu conexión de internet", "info");
    } else {
      swal("Algo ha ocurrido", "Verifica que todos los campos sean correctos...", "error");
    }
  });
});