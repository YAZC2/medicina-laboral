$(document).ready(function() {
  actualizarTabla();
});

var validacion_insert = $.trim($("#validacion").data('insert'));
var validacion_delete = $.trim($("#validacion").data('delete'));
var validacion_update = $.trim($("#validacion").data('update'));

var button_crear = {};

if(validacion_insert == "true")
{
    button_crear = {
        text: "<i class='feather icon-plus'></i> Añadir Usuario",
        action: function() {
          $('#agregarModal').modal('show');
        },
        className: "btn-outline-primary btn-sm"
    };
}

function actualizarTabla() {
  $.ajax({
    type: 'get',
    dataType: 'json',
    url: "getUsuariosSecundarios",
    success: function(users) {
      var dataSet = [];
      $.each(users, function(index, user) {
        let button_edit = '';
        let button_delete = '';
        if(validacion_update == "true")
        {
            button_edit = '<span class="action-edit actions_button" onclick="showEditarModal(' + user.id + ')"><i class="feather icon-edit"></i></span>';
        }
        if(validacion_delete == "true")
        {
            button_delete = '<span class="action-delete actions_button" onclick="eliminarUsuario(' + user.id + ')"><i class="feather icon-trash"></i></span>';
        }
        dataSet.push([
          index+1,
          user.nombre,
          user.telefono,
          user.email,
          // '<a href="configuracion-usuarios/'+user.id+'" class="text-body action-edit actions_button"><i class="feather icon-eye"></i></a>',
          button_edit,
          button_delete
        //   '<span class="action-edit actions_button" onclick="showEditarModal(' + user.id + ')"><i class="feather icon-edit"></i></span>',
        //   '<span class="action-delete actions_button" onclick="eliminarUsuario(' + user.id + ')"><i class="feather icon-trash"></i></span>'
        ]);
      });

      $('#table_estudios').dataTable().fnDestroy();

      $('#table_estudios').dataTable({
        data:dataSet,
        responsive: !1,
        dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
        language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 de 0 de 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "_MENU_",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
          }
        },

        order: [
            [1, "desc"]
        ],

        buttons: [button_crear],
        initComplete: function(t, e) {
            $(".dt-buttons .btn").removeClass("btn-secondary")
        }
      });
    }
  });
}

function showEditarModal(userID) {
  console.log(userID);
  $.ajax({
    type: 'get',
    dataType: 'json',
    url: "getUsuarioSecundario/" + userID,
    success: function(user) {
      console.log(user);
      document.querySelector('#inputUserID').value = user.id;
      document.querySelector('#inputNombre').value = user.nombre;
      document.querySelector('#inputEmail').value = user.email;
      document.querySelector('#inputTelefono').value = user.telefono;
      document.querySelector('#inputPuesto').value = user.puesto;
      document.querySelector('#inputArea').value = user.area;
      $("#inputRol").val(user.role_id);

      $('#editarModal').modal('show');
    }
  });
}

function eliminarUsuario(id) {
  swal({
      title: "Atención",
      text: "¿Seguro que deseas eliminar al usuario?",
      icon: "warning",
      buttons: {
              cancel: {
                  text: "No",
                  value: null,
                  visible: true,
                  className: "",
                  closeModal: false,
              },
              confirm: {
                  text: "Si",
                  value: true,
                  visible: true,
                  className: "",
                  closeModal: false
              }
      }
  })
  .then((isConfirm) => {
      if (isConfirm) {
        $.ajax({
          type: 'get',
          url: 'eliminarUsuarioSecundario/' + id,
          dataType: 'json',
          success: function(respuesta) {
            console.log(respuesta);
            actualizarTabla();
            swal('Eliminado','EL usuario se elimino correctamente','success');
          }
        }).fail(function(xhr, status, error) {
          swal("Error", "No se ha podido eliminar el usuario. Intente de nuevo más tarde.", "error");
        });
      } else {
          swal("Cancelado", "La operación se cancelo correctamente", "error");
      }
  })

}

// Formulario que modifica el estudio.
$('#formEditar').on('submit', function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: 'actualizarUsuarioSecundario',
    dataType: 'json',
    data: $('#formEditar').serialize(),
    success: function(respuesta) {
      setTimeout(() => {
        $('#alerts').html('');
      }, 5000);

      mensajeAlerta = `
                <div class="alert alert-success" role="alert">
                    Los cambios se han guardado correctamente
                </div>
                `;

      $('#alerts').html(mensajeAlerta);
      actualizarTabla();
    }
  }).fail(function(xhr, status, error) {
    if (xhr.status == 400) {
      mensajeAlerta = `
            <div class="alert alert-danger" role="alert">
                El correo electrónico ingresado pertenece a otro usuario registrado.
            </div>
            `;
    } else {
      mensajeAlerta = `
            <div class="alert alert-danger" role="alert">
                Revise que todos los campos estén llenos y utilice el tipo de dato correcto.
            </div>
            `;
    }

    setTimeout(() => {
      $('#alerts').html('');
    }, 5000);

    $('#alerts').html(mensajeAlerta);
  });
});

// Formulario que da de alta un nuevo estudio.
$('#formAgregar').on('submit', function(e) {
  e.preventDefault();
  swal({
    title: 'Creando usuario',
    text: 'Espere',
    icon: "../../../app-assets/images/icons/loading.gif",
    buttons: false,
  })


  $.ajax({
      type: 'post',
      url: 'agregarUsuarioSecundario',
      dataType: 'json',
      data: $('#formAgregar').serialize(),
      success: function(response) {
        setTimeout(() => {
          $('#alerta_agregar').html('');
        }, 5000);

        mensajeAlerta = `
                <div class="alert alert-success" role="alert">
                    El usuario se registró correctamente.
                </div>
                `;
        swal.close();
        $('#alerta_agregar').html(mensajeAlerta);
        actualizarTabla();
        document.querySelector('#formAgregar').reset();
        $('#agregarModal').modal('hide');
      }
    })
    .fail(function() {
      setTimeout(() => {
        $('#alerta_agregar').html('');
      }, 5000);

      mensajeAlerta = `
            <div class="alert alert-danger" role="alert">
                Revise que todos los campos estén llenos y utilice el tipo de dato correcto.
            </div>
            `;

      $('#alerta_agregar').html(mensajeAlerta);
    });
});
