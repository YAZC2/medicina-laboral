<?php

//login
use App\HistorialClinico;
use App\Resultados;
use App\User;
// use Illuminate\Routing\Route;

Auth::routes(['register' => false]);
Route::get('/', function () {
    if (Auth::user()) {
        return redirect()->action('RoleController@index');
    } else {
        return view('auth/login');
    }
});
Route::get('mostrar_pdf', function () {
    
        return view('auth/login');
    
});
Route::get('/firma', function () {
    return view('firma');
});
Route::get('/consultaodontologia', function () {
    return view('consultaodontologia');
});

// NOTE: eliminar siguiente funcion


// Route::get('resultados_create', function () {
//     $historial = HistorialClinico::all();
//     foreach ($historial as  $value) {
//         $resultado = Resultados::where('hc', $value->id)->first();
//         if ($resultado) {
//             \DB::table('resultados')
//                 ->where('id', $resultado->id)
//                 ->update([
//                     'expediente_id' => $value->expediente_id
//                 ]);
//         }
//     }
//     return "true";
// });

Route::post('validar_email', function () {
    $validacion = false;
    $user  = User::where('email', $_POST['email'])->first();
    if ($user == null) {
        $validacion = true;
    }
    return response()->json($validacion, 200);
});

Route::post('users/{id}', function ($id) {
});






// NOTE: HTDS

Route::get('/loginHtds/{nombre}/{pass}', 'Empresa\HtdsController@authenticate');
// NOTE: Administrador
Route::get('/Administrador', 'RoleController@index')->name('admin');
Route::get('/PersonasEstudio/{estudio}/{t_resultado}', 'RoleController@tipoestudios')->name('tipoestudios');
Route::get('DiagnosticosIndicador', 'Empresa\EmpresasController@indicadores');
Route::post('DiagnosticosDate', 'Empresa\EmpresasController@indicadoresDate');


Route::get('getimage/{filename}', 'RegistroEmpresaController@getImage')->name('getImage');

// Rutas administrador
Route::get('/Registro', 'RegistroEmpresaController@viewRegistro')->name('empresa')->middleware('admin');
Route::post('/registroController', 'RegistroEmpresaController@registro')->name('registro')->middleware('admin');
Route::get('/Contacto', 'RouteController@contacto')->name('contacto')->middleware('admin');
Route::get('/Estudios', 'RouteController@estudios')->name('estudios')->middleware('admin');
Route::get('/Estudios_get/{id}', 'RouteController@getEstudios')->name('getestudios')->middleware('admin');
Route::get('/Estudio/{id}', 'RouteController@getEstudio')->name('getEstudio')->middleware('admin');
Route::post('/updateEstudio', 'Administrador\EstudiosController@updateEstudio')->name('editar_estudio')->middleware('admin');
Route::post('/altaEstudio', 'Administrador\EstudiosController@alta')->name('altaEstudio')->middleware('admin');
Route::get('/Empresas', 'RouteController@verEmpresas')->name('admin-verEmpresas')->middleware('admin');
Route::get('/T_Empresas', 'RouteController@muestraEmpresas')->name('admin-muestraEmpresas')->middleware('admin');
Route::get('/Empresas/{nombreEmpresa}', 'RouteController@showEmpresa')->name('admin-showEmpresa')->middleware('admin');
Route::post('/actualizar-logo', 'Administrador\EmpresasController@cambiarLogotipo')->name('cambiarLogotipo')->middleware('admin');
Route::get('/getEstudiosEmpresa/{encryptedID}', 'Administrador\EmpresasController@getEstudiosEmpresa')->name('getEstudiosEmpresa')->middleware('admin');
Route::post('/selectorEstudiosEmpresa', 'Administrador\EmpresasController@selectorEstudiosEmpresa')->name('selectorEstudiosEmpresa')->middleware('admin');
Route::get('Empresas/{nombreEmpresa}/editar-empresa', 'Administrador\EmpresasController@editarEmpresa')->name('editarEmpresa')->middleware('admin');
Route::get('Empresas/{nombreEmpresa}/editar-contacto', 'Administrador\EmpresasController@editarContacto')->name('editarContacto')->middleware('admin');
Route::post('/actualizar-empresa', 'Administrador\EmpresasController@actualizarEmpresa')->name('actualizarEmpresa')->middleware('admin');
Route::post('/actualizar-contacto', 'Administrador\EmpresasController@actualizarContacto')->name('actualizarContacto')->middleware('admin');
Route::get('EmpresaAdmin/{nomEmpresa}', 'RoleController@admin')->name('EmpresaAdmin')->middleware('admin');
Route::get('AdminSalir', 'RoleController@salirAdmin')->name('AdminSalir')->middleware('role');

//Rutas de Empresas

Route::get('/consultasStats/{year}', 'Empresa\EmpresasController@getConsultasStats')
    ->middleware('role');

Route::get('/estudios_get_name', 'Empresa\EstudiosController@estudios')
    ->middleware('role');



Route::get('/Indicadores-Covid', 'Empresa\CovidController@covid_indicadores')->name('covid_indicador')->middleware('role');
Route::get('/get_info_covid', 'Empresa\CovidController@get_empleados_covid')->middleware('role');

//RUTAS DE EMPRESA > RUTAS DE MEDICOS
Route::get('/configuracion-usuarios', 'Empresa\AdminUsuariosController@index')->name('config-usuarios')->middleware('role');
Route::get('/configuracion-usuarios/{user_id}', 'Empresa\AdminUsuariosController@detalle_usuario')->name('config-usuarios-detalle')->middleware('role');

Route::get('/Expediente', 'RouteController@expediente')->name('expediente')->middleware('role');
Route::get('/Empleados', 'Empresa\EmpleadosController@empleados')->name('empleados')->middleware('role');
Route::get('buscarempleados', 'Empresa\EmpleadosController@buscarempleados')
    ->name('buscarempleados')
    ->middleware('role');
Route::get('/ObtenerCalculadoraC/{id}', 'RouteController@covid')->middleware('role');
Route::post('/CalculadoraC', 'RouteController@CalculadoraC')->middleware('role');

Route::get('/VistaEmpr', 'RouteController@vistaEmpresa')->name('VEmpresa')->middleware('role');
Route::get('api/empleados', 'RouteController@getEmpleados')->middleware('role');
Route::get('api/grupos', 'RouteController@getGrupos')->middleware('role');
Route::resource('empleados', 'Empresa\EmpleadosController')->middleware('role');
Route::post('eliminarEmpleado', 'Empresa\EmpleadosController@eliminarEmpleado')->name('eliminarEmpleado')->middleware('role');
Route::resource('grupos', 'Empresa\GruposController')->middleware('role');
Route::post('new_grupo', 'Empresa\GruposController@insertGrupo')->middleware('role');

Route::get('Grupos', 'Empresa\GruposController@index')->name('grupos')->middleware('role');


Route::get('delete_grupo/{nombre}', 'Empresa\GruposController@deleteGrupo')->name('deletegrupo')->middleware('role');

Route::post('updateGrupos', 'Empresa\GruposController@updateGrupo')->middleware('role');

Route::get('grupos/getEmpleadosDeGrupo/{encryptedID}', 'RouteController@getEmpleadosDeGrupo')->middleware('role');
Route::get('/AltaEmpleados', 'RouteController@AltaEmpleado')->name('alta_empleado')->middleware('role');

Route::post('/altaEmpleados', 'Empresa\EmpleadosController@insertEmpleado')->middleware('role');


Route::get('/VerEstudioProgramado', 'RouteController@getEstudioProgramado')->name('ver_programado')->middleware('role');
Route::get('empleados/Empleado/{encryptedID}', 'RouteController@getEmpleado')->name('empleado')->middleware('role');

Route::post('empleados/updateEmpleado', 'Empresa\EmpleadosController@updateEmpleado')->name('updateEmpleado')->middleware('role');

Route::post('/imgeChange', 'Empresa\EmpleadosController@updateImage')
    ->middleware('role');

Route::post('/docPatient', 'Empresa\EmpleadosController@docuementos')
    ->middleware('role');

Route::post('/deleteDoc', 'Empresa\EmpleadosController@deleteArchivo')
    ->middleware('role');

Route::get('empleados/getEstudioProgramado/{id}', 'RouteController@getEstudioProgramado')->name('getEstudioProgramado')
    ->middleware('role');

Route::post('/selectorEstudiosGrupo', 'Empresa\GruposController@selectorEstudiosGrupo')->name('selectorEstudiosGrupo')->middleware('role');
Route::post('/agregarEmpleadosGrupo', 'Empresa\GruposController@agregarEmpleadosGrupo')->name('agregarEmpleadosGrupo')->middleware('role');


Route::post('/programarEstudiosGrupo', 'Empresa\GruposController@programarEstudiosGrupo')->name('programarEstudiosGrupo')->middleware('role');
Route::post('/eliminarEstudioGrupo', 'Empresa\GruposController@eliminarEstudioGrupo')->name('eliminarEstudioGrupo')->middleware('role');
Route::post('/eliminarEstudioEmpleado', 'Empresa\EmpleadosController@eliminarEstudioEmpleado')->name('eliminarEstudioEmpleado')->middleware('role');
Route::post('/programarEstudiosEmpleadoGrupo', 'Empresa\EmpleadosController@programarEstudiosEmpleadoGrupo')->name('programarEstudiosEmpleadoGrupo')->middleware('role');
Route::post('/programarEstudiosEmpleado', 'Empresa\EmpleadosController@programarEstudiosEmpleado')->name('programarEstudiosEmpleado')->middleware('role');
Route::post('/sass_orden', 'Empresa\EmpleadosController@sass_orden');
Route::post('/sass_orden_delete', 'Empresa\EmpleadosController@deleteOrden');
//Route::post('/daicom', 'Empresa\EmpleadosController@daicom');
//Route::get('/daicom', function () {
  //  return view('Empresa.daicom');
//});
Route::get('/daicom', 'Empresa\EmpleadosController@daicom');




//RESULTATOS
Route::get('/Resultados/{id}/{id_estudio}', 'Empresa\Laboratorio\EstudiosController@tablaestudios')->name('sassResultados');
Route::get('/ShowEstudio/{id}', 'Empresa\LaboratorioController@showEstudios')->middleware('role');
Route::get('/Resultados', 'Empresa\ResultadosController@index')->name('resultados')->middleware('role');
Route::get('/ResultadosLa', 'Empresa\ResultadosController@laboratorio')->name('relaboratorio')->middleware('role');
Route::get('/ResultadosIma', 'Empresa\ResultadosController@imagenologia')->name('reimagenologia')->middleware('role');
Route::get('/Resultados1/{fecha_inicial}/{fecha_final}', 'Empresa\ResultadosController@estudios_fech');
Route::get('/Archivos/{id}', 'Empresa\ResultadosController@archivo')->name('archivo')->middleware('role');
//RIESGOS
Route::get('/RiesgoCardiovascular', 'Riesgos\CardiovascularController@cardiovascular')->name('cardiovascular')->middleware('role');
Route::get('/RiesgoDiabetes', 'Riesgos\DiabetesController@diabetes')->name('diabetes')->middleware('role');
Route::get('/RiesgoRenal', 'Riesgos\RenalController@renal')->name('renal')->middleware('role');
//GRUPOS LABORATORIO
Route::get('/grupos/getEstudiosProgramadosGrupo/{nombre}', 'RouteController@getEstudiosProgramadosGrupo')->name('getEstudiosProgramadosGrupo')->middleware('role');
Route::get('/grupos/getEstudiosProgramadosEmpleado/{curp}', 'RouteController@getEstudiosProgramadosEmpleado')->name('grupos.getEstudiosProgramadosEmpleado')->middleware('role');
Route::get('getEmpleadosSinGrupo', 'RouteController@getEmpleadosSinGrupo')->name('getEmpleadosSinGrupo')->middleware('role');
Route::get('/empleados/getEstudiosProgramadosEmpleado/{curp}', 'RouteController@getEstudiosProgramadosEmpleado')->name('empleados.getEstudiosProgramadosEmpleado')->middleware('role');
Route::get('/indicadores', 'RouteController@vistaIndicadores')->name('vistaIndicadores')->middleware('role');
Route::get('/indicadoresGrupo', 'RouteController@vistaIndicadoresGrupo')->name('vistaIndicadoresGrupo')->middleware('role');
Route::post('refresh_expedientes', 'Empresa\EmpleadosController@expediente_ant')->name('l')->middleware('role');
Route::post('expedienteActual', 'Empresa\EmpleadosController@expedienteActual')->name('expedienteActual')->middleware('role');

Route::get('getNotaExpediente/{curp}', 'Empresa\EmpleadosController@getNotaExpediente')->name('getNotaExpediente')->middleware('role');

Route::post('updateNotaExpediente', 'Empresa\EmpleadosController@updateNotaExpediente')->name('updateNotaExpediente')->middleware('role');

Route::post('addNewNota', 'Empresa\EmpleadosController@addNewNota')->name('addNewNota')->middleware('role');
Route::post('updateTitleNota', 'Empresa\EmpleadosController@updateTitleNota')->name('updateTitleNota')->middleware('role');
Route::post('deleteNota', 'Empresa\EmpleadosController@deleteNota')->name('deleteNota')->middleware('role');


Route::get('getHistorialesClinicos/{encryptedID}', 'Empresa\EmpleadosController@getHistorialesClinicos')->name('getHistorialesClinicos')->middleware('role');
Route::get('getDatosEnfermedades', 'Empresa\EmpleadosController@getDatosEnfermedades')->name('getDatosEnfermedades')->middleware('role');
Route::get('getDatosEnfermedadesGrupo/{idGrupo}', 'Empresa\EmpleadosController@getDatosEnfermedadesGrupo')->name('getDatosEnfermedadesGrupo')->middleware('role');
Route::get('getDatosIMC', 'Empresa\EmpleadosController@getDatosIMC')->name('getDatosIMC')->middleware('role');
Route::get('getDatosIMCGrupo/{idGrupo}', 'Empresa\EmpleadosController@getDatosIMCGrupo')->name('getDatosIMCGrupo')->middleware('role');
Route::get('getDatosGeneros', 'Empresa\EmpleadosController@getDatosGeneros')->name('getDatosGeneros')->middleware('role');
Route::get('getDatosGenerosGrupo/{idGrupo}', 'Empresa\EmpleadosController@getDatosGenerosGrupo')->name('getDatosGenerosGrupo')->middleware('role');
Route::get('getUsuariosSecundarios', 'Empresa\AdminUsuariosController@getUsuariosSecundarios')->name('getUsuariosSecundarios')->middleware('role');
Route::get('getUsuarioSecundario/{user_id}', 'Empresa\AdminUsuariosController@getUsuarioSecundario')->name('getUsuarioSecundario')->middleware('role');

Route::post('actualizarUsuarioSecundario', 'Empresa\AdminUsuariosController@actualizarUsuarioSecundario')->name('actualizarUsuarioSecundario')->middleware('role');

Route::post('agregarUsuarioSecundario', 'Empresa\AdminUsuariosController@agregarUsuarioSecundario')->name('agregarUsuarioSecundario')->middleware('role');

Route::get('eliminarUsuarioSecundario/{user_id}', 'Empresa\AdminUsuariosController@eliminarUsuarioSecundario')->name('eliminarUsuarioSecundario')->middleware('role');



// NOTE: Consultas
Route::post('/consultas', 'Empresa\ConsultaController@index')->name('consulta')->middleware('role');
Route::post('/consultasAgenda', 'Empresa\ConsultaController@agenda')
    ->middleware('role');   
Route::get('/consultas/{id}', 'Empresa\ConsultaController@consulta')->name('consulta_view')->middleware('role');
Route::get('/consulta/{id}', 'Empresa\ConsultaController@consulta_finalizada')->name('consulta_finalizada')->middleware('role');


// NOTE: Catalogo Nom-024 Inicial
Route::get('/cie_10', 'Empresa\ConsultaController@diagnostico')->middleware('role');
Route::post('/cie_10Insert', 'Empresa\ConsultaController@excel_subir')->middleware('role');

Route::get('/medicamentos', 'Empresa\ConsultaController@medicamentos')
    ->middleware('role');

Route::get('/procedimientos', 'Empresa\ConsultaController@procediminetos')
    ->middleware('role');

Route::get('/discapacidad', 'Empresa\ConsultaController@discapacidad')
    ->middleware('role');


Route::post('/padecimiento', 'Empresa\ConsultaController@padecimiento')
    ->middleware('role');



Route::post('/examen_fisico', 'Empresa\ConsultaController@examen_fisico')
    ->middleware('role');


// NOTE: Catalogo Nom-024 Fin
//
// NOTE: Guardar datos de consulta
Route::post('/diagnostico', 'Empresa\ConsultaController@diagnosticoH')
    ->middleware('role');




Route::post('/finalizarConsulta/{id}', 'Empresa\ConsultaController@finalizar')
    ->middleware('role');

Route::get('/getHistorial/{id}', 'Empresa\ConsultaController@getHistorial')
    ->middleware('role');

Route::get('/deleteConsulta/{id}', 'Empresa\ConsultaController@deleteConsulta')
    ->middleware('role');

Route::post('/consultaDocument', 'Empresa\ConsultaController@archivo')
    ->middleware('role');

Route::post('/deleteDocument', 'Empresa\ConsultaController@deleteArchivo')
    ->middleware('role');

Route::post('/consultaEcg', 'Empresa\ConsultaController@crear_ecg')
    ->middleware('role');

Route::post('/deleteEcg', 'Empresa\ConsultaController@eliminar_ecg')
    ->middleware('role');

Route::get('/Ecg/{id}', 'Empresa\ConsultaController@view_ecg')
    ->name('view_ecg')
    ->middleware('role');

Route::get('/ecg-series/{id}', 'Empresa\ConsultaController@series_ecg')
    ->middleware('role');


// Antecedentes
Route::post('/antecedentesGet', 'Empresa\AntecedentesController@getAntecedentes')
    ->middleware('role');

Route::post('/antecedentesResult', 'Empresa\AntecedentesController@changedAntecedente')
    ->middleware('role');

Route::post('/deleteAntecedente', 'Empresa\AntecedentesController@deleteFielAntecedente')
    ->middleware('role');

Route::post('/fieldAntecedente', 'Empresa\AntecedentesController@getFields')
    ->middleware('role');

Route::post('/medicamentoActivo', 'Empresa\AntecedentesController@medicamentoActivo')
    ->middleware('role');

Route::post('/deleteMedicaActive/{id}', 'Empresa\AntecedentesController@deleteMedicamentoA')
    ->middleware('role');

Route::get('/changeStatusAntecedentes/{id}/{status}', 'Empresa\AntecedentesController@changeStatusAntecedentes')
    ->middleware('role');

Route::get('/verhistorialantecedentes/{CURP}', 'Empresa\AntecedentesController@verhistorialantecedentes')
    ->middleware('role'); 

// BORRAR O AUMENTAR VALIDACIONES POR SEGURIDAD.
//Route::get('getHistorialClinicoCompleto/{id}', 'Empresa\EmpleadosController@getHistorialClinicoCompleto')->name('getHistorialClinicoCompleto')->middleware('role');
// BORRAR O AUMENTAR VALIDACIONES POR SEGURIDAD.
//



// NOTE: RUTAS DE 
//Rutas Estudios Medicina Laboral
Route::post('programarestudio', 'Empresa\Laboratorio\EstudiosController@programar_estudio')->name('programarestudio')->middleware('laboratorio');
Route::get('estudio/{codigo}', 'Empresa\Laboratorio\EstudiosController@estudio')->name('estudio');
Route::post('delete_estudio_pro/{id}', 'Empresa\Laboratorio\EstudiosController@delete_estudio_pro')->name('programarestudio')->middleware('laboratorio');
Route::post('editar_estudio/{id}', 'Empresa\Laboratorio\EstudiosController@editar_estudio')->name('programarestudio')->middleware('laboratorio');

//FORMATOS
Route::get('/formatos', 'Empresa\FormatosController@formato')->name('formatos')->middleware('laboratorio');
Route::post('/actualiza_estatus', 'Empresa\FormatosController@actualiza_estatus')->name('actualiza_estatus');

// RESULTADOS LABORATORIO

//Rutas Electrocardiograma
Route::get('obtenerestudios', 'ApiHas\ApiElectroController@obtenerestudios')->name('obtenerestudios');
Route::post('insertestpro', 'ApiHas\ApiElectroController@insertestpro')->name('insertestpro');

//Rutas de laboratorio
Route::get('estudiossass', 'Laboratorio\historialClinicoController@estudiossass')->name('estudiossass')->middleware('laboratorio');
Route::post('addEmpleado', 'Laboratorio\historialClinicoController@insertEmpleadoEmpresa')->name('addEmpleado')->middleware('laboratorio');
Route::get('getEmpleadoLaboratorio/{id}','Laboratorio\historialClinicoController@getEmpleadoLaboratorio')->middleware('laboratorio');
Route::post('edit_emp_laboratorio','Laboratorio\historialClinicoController@edit_emp_laboratorio');
Route::post('eliminarEmpleado','Laboratorio\historialClinicoController@eliminarEmpleado');
Route::get('pruebaLaboratorio', 'Laboratorio\LabLoginController@vistaPruebaLaboratorio')->name('pruebaLaboratorio');
Route::get('auth/lab-login/{token}', 'Laboratorio\LabLoginController@labLogin')->name('labLogin');
Route::post('auth/lab-logout', 'Laboratorio\LabLoginController@labLogout')->name('labLogout');
Route::get('lab-menu', 'Laboratorio\LabLoginController@labMenu')->name('lab-menu')->middleware('laboratorio');
Route::get('pacientes', 'Laboratorio\LabLoginController@listaPacientes')->name('pacientes')->middleware('laboratorio');
Route::get('logotipos_empresas/{filename}', 'Laboratorio\LabLoginController@getLogoEmpresa')->name('getLogoEmpresa')->middleware('laboratorio');
Route::get('getEmpresa/{nombreEmpresa}', 'Empresa\EmpresasController@getEmpresa')->name('getEmpresa')->middleware('laboratorio');
Route::get('lab-menu/empresas/{nombreEmpresa}', 'Laboratorio\LabLoginController@verEmpresa')->name('lab-infoEmpresa')->middleware('laboratorio');


Route::get('/ResultadosTotales', 'Laboratorio\LabLoginController@index2')->name('resultados_totales')->middleware('laboratorio');
Route::get('/ResultadosLaM', 'Laboratorio\LabLoginController@laboratoriomedico')->name('laboratoriomedico')->middleware('laboratorio');
Route::get('/ResultadosImaM', 'Laboratorio\LabLoginController@imagenologiamedico')->name('imagenologiamedico')->middleware('laboratorio');
// Tecnico radiologo
Route::get('PreIngresos', 'Laboratorio\IngresosController@view')->name('Ingresos')->middleware('laboratorio');
Route::get('Estudios-Imagenologia/{id}', 'Laboratorio\IngresosController@viewEstudios')->name('estudiosimagenologia')->middleware('laboratorio');

Route::get('lab-menu/empresas/empleadosProgramaaddEmpleadodos/{encryptedID}', 'Laboratorio\LabLoginController@getEmpleadosProgramados')->middleware('laboratorio');
Route::get('lab-menu/empleados/{CURP}', 'Laboratorio\LabLoginController@showEmpleadoProgramado')->name('showEmpleadoProgramado')->middleware('laboratorio');
Route::post('registrarEntradaRecepcion', 'Laboratorio\LabLoginController@registrarEntrada')->name('registrarEntrada')->middleware('laboratorio');

//Rutas de laboratorio imagenologia
Route::post('EstudioRealizado', 'Laboratorio\IngresosController@estudioRealizado')->middleware('laboratorio');
Route::post('Dicom', 'Laboratorio\IngresosController@dicom')->name('dicom')->middleware('laboratorio');

// Rutas medico radiologo
Route::get('Pacientes', 'Laboratorio\MedicoRadiologoController@radiologia')->name('pacientes')->middleware('laboratorio');
Route::post('Observaciones', 'Laboratorio\MedicoRadiologoController@guardarObservacion')->name('oldds')->middleware('laboratorio');
Route::get('Estudio_/{id}', 'Laboratorio\MedicoRadiologoController@infoEstudio')->name('estudioinfo')->middleware('laboratorio');
Route::get('Estudios/{id}', 'Laboratorio\MedicoRadiologoController@pacienteEstudios')->name('estudios_paciente')->middleware('laboratorio');

//Rutas Medicina
Route::get('Medicina', 'Laboratorio\historialClinicoController@view_empresas')->name('medicina')->middleware('laboratorio');
Route::post('add_empresa', 'Laboratorio\historialClinicoController@add_empresa')->middleware('laboratorio');
Route::post('delete_empresa/{id}','Laboratorio\historialClinicoController@delete_empresa')->middleware('laboratorio');
Route::get('getEmp/{id}','Laboratorio\historialClinicoController@getEmp')->middleware('laboratorio');
Route::post('editar_empresa', 'Laboratorio\historialClinicoController@edit_empresa')->middleware('laboratorio');
Route::get('Medicina-Pacientes/{id}', 'Laboratorio\MedicinaController@view')->name('medicina-pacientes')->middleware('laboratorio');
Route::get('Paciente/{id}', 'Laboratorio\MedicinaController@pacienteEstudios')->name('medicina_paciente')->middleware('laboratorio');
Route::get('Diagnostico/{id_ingreso}', 'Laboratorio\MedicinaController@viewDiagnostico')->name('viewDiagnostico')->middleware('laboratorio');
Route::post('MedicinaUrl', 'Laboratorio\MedicinaController@saveResult')->name('saveUrlEstudy')->middleware('laboratorio');
Route::get('Historial-Pacientes', 'Laboratorio\MedicinaController@historial_pacientes')->name('historial_pacientes')->middleware('laboratorio');
Route::get('Historial-Pacientes/{paciente_id}/estudios', 'Laboratorio\MedicinaController@historial_pacientes_estudios')->name('historial_pacientes_estudios')->middleware('laboratorio');
Route::get('formularios/{registro_entrada_id}', 'Laboratorio\MedicinaController@seleccion_formularios')->name('seleccion_formularios')->middleware('laboratorio');
Route::get('formularios/{registro_entrada_id}/{formulario}', 'Laboratorio\MedicinaController@formulario_vista')->name('vista_formulario')->middleware('laboratorio');
Route::post('save_formulario/{paso}', 'Laboratorio\MedicinaController@save_formulario')->middleware('laboratorio');
Route::post('save_formulario_2/{paso}', 'Laboratorio\MedicinaController@save_formulario_2')->middleware('laboratorio');
Route::post('save_formulario_3/{paso}', 'Laboratorio\MedicinaController@save_formulario_3')->middleware('laboratorio');
Route::get('formularios/{empresa}', 'Laboratorio\MedicinaController@seleccion_formularios')->name('seleccion_formularios')->middleware('laboratorio');



Route::get('NumPacientes', 'Laboratorio\historialClinicoController@numPaciente')->middleware('laboratorio');

Route::get('Historial-Clinico/{curp}', 'Laboratorio\HistorialFormController@view')->name('historialFormulario')->middleware('laboratorio');
Route::get('Historial-Clinico-Pdf/{id}/{formato}', 'PdfController@viewPdf')->name("HistorialPdf");

Route::get('Pacientes-Medico/{id}', 'Laboratorio\historialClinicoController@view')->name('historial')->middleware('laboratorio');
//
Route::get('Vista-Empresas', 'Laboratorio\historialClinicoController@view_empresas')->name('historial_empresas')->middleware('laboratorio');

Route::get('getArchivos/{idEmpleado}/{idEstudio}', 'Laboratorio\HistorialFormController@getArchivos')->middleware('laboratorio');
Route::get('PDF/{id}', 'PdfController@MostrarPdf')->name('resultado-pdf');
Route::post('historialForm', 'Laboratorio\HistorialFormController@historialForm')->name('formHistorial')->middleware('laboratorio');

Route::post('/Formato', 'Laboratorio\HistorialFormController@regresaformato')->name('Formato')->middleware('laboratorio');
Route::get('/AdmisionContratista/{historial}/{paciente}/{estudios}', 'Laboratorio\HistorialFormController@AdmisionContratista')->name('AdmisionContratista')->middleware('laboratorio');
//Rutas Nueva Espirometria
Route::get('EstudioEspirometria/{curp}/{nim}', 'Laboratorio\Espirometria\EspirometriaController@espirometria')
    ->name('estudio_espirometria')
    ->middleware('laboratorio');
Route::post('EstudioEspirometria/guarda_espirometria', 'Laboratorio\Espirometria\EspirometriaController@save_espirometria')
    ->middleware('laboratorio');
Route::get('Resultado_Espirometria/{curp}/{nim}', 'Laboratorio\Espirometria\EspirometriaController@view_resultado')
->name('Resultado_Espirometria');
//Ruta Electrocardiograma
Route::get('electrocardiograma/{curp}/{nim}', 'Laboratorio\Electrocardiograma\ElectrocardiogramaController@electrocardiograma')
->name('electrocardiograma')
->middleware('laboratorio');
Route::post('electrocardiograma/save_electro','Laboratorio\Electrocardiograma\ElectrocardiogramaController@save_electro')
->name('save_electro')
->middleware('laboratorio');
Route::get('resultado', 'Laboratorio\Electrocardiograma\ElectrocardiogramaController@electrocardiograma')
->name('electrocardiograma')
->middleware('laboratorio');

Route::get('trazo/{curp}/{nim}', 'Laboratorio\Electrocardiograma\ElectrocardiogramaController@trazo')
->name('trazo');
Route::get('resultado_electro/{curp}/{nim}', 'PdfController@resultado_electro')->name("resultado_electro");
Route::get('resultado_fisioterapia/{curp}/{nim}', 'PdfController@fisioterapia')->name("resultado_fisioterapia");


//Ruta Espirometria

Route::get('Espirometria/{id}', 'Laboratorio\Espirometria\EspirometriaController@index')
    ->name('espirometria')
    ->middleware('laboratorio');

Route::post('solicitud', 'Laboratorio\Espirometria\EspirometriaController@solicitud')
    ->middleware('laboratorio');

Route::get('espirometria-view/{id}', 'Laboratorio\Espirometria\EspirometriaController@viewPdf')
    ->name('pdfEspirometria')
    ->middleware('laboratorio');

Route::post('fv_file', 'Laboratorio\Espirometria\EspirometriaController@fv_file')
    ->middleware('laboratorio');

Route::post('vt_file', 'Laboratorio\Espirometria\EspirometriaController@vt_file')
    ->middleware('laboratorio');

Route::post('save_espirometria', 'Laboratorio\Espirometria\EspirometriaController@resultEstudio')
    ->middleware('laboratorio');



//Ruta Fisioterapia

Route::get('/Fisioterapia/{curp}/{nim}', 'Laboratorio\Fisioterapia\FisioterapiaController@fisio')->name('fisioterapia')->middleware('laboratorio');
Route::post('/Fisioterapia/savefisio', 'Laboratorio\Fisioterapia\FisioterapiaController@registrar_fisio')->name('savefisio')->middleware('laboratorio');


//Ruta Audiometria

Route::get('/Audiometria/{curp}/{nim}', 'Laboratorio\Audiometria\AudiometriaController@audiometria')->name('audiometria')->middleware('laboratorio');
// Route::get('Audiometria/{id}/{registro_entrada_id}', 'Laboratorio\Audiometria\AudiometriaController@index')->name('audiometria')->middleware('laboratorio');
// Route::get('/Audiometria', 'Laboratorio\Audiometria\AudiometriaController@index')->name('audiometria')->middleware('laboratorio');
Route::post('Audiometria/save_audiometria', 'Laboratorio\Audiometria\AudiometriaController@save_audiometria')->middleware('laboratorio');
Route::get('Audiometria/getResultadosAudiometria/{id}/{oido}', 'Laboratorio\Audiometria\AudiometriaController@getResultados');
Route::post('Audiometria/guardarResultado', 'Laboratorio\Audiometria\AudiometriaController@guardarResultado')->middleware('laboratorio');
Route::post('Audiometria/guardarFinal', 'Laboratorio\Audiometria\AudiometriaController@guardarFinal')->middleware('laboratorio');
Route::post('Audiometria/guardarFirma', 'Laboratorio\Audiometria\AudiometriaController@guardarFirma')->middleware('laboratorio');
//Rutas Laboratorio
Route::get('Laboratorio', 'Laboratorio\LaboratorioController@view')->name('pacienteLaboratorio')->middleware('laboratorio');
Route::get('Pacientes_/{id}', 'Laboratorio\LaboratorioController@pacienteEstudios')->name('laboratorio_paciente')->middleware('laboratorio');
Route::post('LaboratorioUrl', 'Laboratorio\LaboratorioController@saveResult')->name('finalizarEstudioLab')->middleware('laboratorio');

/*Route::post('LaboratorioFinal','Laboratorio\LaboratorioController@finishLab')->name('estudioFinal')->middleware('laboratorio');*/

// Excel
Route::get('/AltaEmpleadoGrps', 'ImportExcelController@index')->name('altagrupo')->middleware('role');
Route::post('/import_excel/import', 'ImportExcelController@import');
Route::get('/download/ListaEmpleados.xlsx', 'ImportExcelController@download')->name('download')->middleware('role');
Route::get('/EmpleadoUpdate/{curp}', 'ImportExcelController@Editar')->middleware('role');
Route::post('/Empleado_Update', 'ImportExcelController@updateEmpleado')->name('Modificacion_empleado')->middleware('role');
Route::get('/Empleado_excel/{excel}', 'ImportExcelController@Empleado_excel')->name('Empleado_excel')->middleware('role');


// PDF

Route::get('cadenacustodia', 'PdfController@cadenacustodia')->name("cadenacustodia")->middleware('role');

// Route::get('Historial-Clinico-Pdf/{id}', 'PdfController@viewPdf')->name("HistorialPdf")->middleware('role');

Route::get('Receta/{id}', 'PdfController@recetaMiedica')->name("HistorialPdf")->middleware('role');

Route::get('procedimientos/{id}', 'PdfController@procedimientos')->middleware('role');

Route::get('tratamiento/{id}', 'PdfController@tratamiento')->middleware('role');

Route::get('RecetaEmail/{id}', 'PdfController@emailMiedica')->middleware('role');

Route::post('NotasEmail', 'PdfController@emailNotas')->middleware('role');

Route::get('procedimientosEmail/{id}', 'PdfController@procedimientosEmail')->middleware('role');

Route::get('tratamientoEmail/{id}', 'PdfController@tratamientoEmail')->middleware('role');

//Rutas de estudio correo
Route::get('estudioscorreo/{toma}/{consecutivo}/{correo}', 'PdfController@estudioscorreo')->name('estudioscorreo');

Route::get('resultadoAudiometria/{id}/{curp}', 'PdfController@resultadoAudiometria');
Route::get('c_Audiometria/{idtoma}/{curp}', 'PdfController@resultadoAudiometria_correo');
Route::get('crearPDF/audiometria/{id}/{curp}', 'PdfController@resultadoAudiometria2')->middleware('laboratorio');
Route::post('correo_estudio', 'PdfController@correo_estudio');


//Covid GuardarEncuesta
Route::post('GuardarEncuesta', 'Empresa\CovidController@GuardarEncuesta')->name("covid")->middleware('role');


Route::get('Covid/{curp}', 'Empresa\CovidController@index')->name("covid")->middleware('role');

Route::get('covidtable', 'Empresa\CovidController@get_empleados_covid_Table')->name("covidtable")->middleware('role');


Route::get('Covid-Version/{curp}', 'Empresa\CovidController@index')->name("covid_2")->middleware('role');

// NOTE: chat

Route::get('Chat', 'Empresa\ChatController@index')->name("chat")->middleware('role');


// NOTE: Reclutamineto
//

Route::get('Reclutamiento', 'Empresa\ReclutamientoController@index')->name("reclutamiento")->middleware('role');
Route::post('reclutamineto_excel', 'Empresa\ReclutamientoController@import')->name('excel_personal')->middleware('role');


// NOTE: Configuracion
Route::get('Configuracion', 'Empresa\ConfiguracionController@index')->name("config")->middleware('role');

Route::post('save_configuracion', 'Empresa\ConfiguracionController@configSave')->middleware('role');

Route::post('update_empresa', 'Empresa\ConfiguracionController@update_empresa')->middleware('role');

Route::post('update_perfil', 'Empresa\ConfiguracionController@update_perfil')->middleware('role');

Route::post('update_password', 'Empresa\ConfiguracionController@update_password')->middleware('role');
Route::post('add_medicamento', 'Empresa\ConfiguracionController@add_medicamento')->middleware('role');
Route::post('edit_medicamento', 'Empresa\ConfiguracionController@edit_medicamento')->middleware('role');
Route::get('getMedicamento/{id}','Empresa\ConfiguracionController@getMedicamento')->middleware('role');
Route::post('delete_medicamento/{id}','Empresa\ConfiguracionController@delete_medicamento')->middleware('role');
Route::post('add_antecedenteForm', 'Empresa\ConfiguracionController@add_antecedenteForm')->middleware('role');

Route::post('edit_antecedenteForm', 'Empresa\ConfiguracionController@edit_antecedenteForm')->middleware('role');

Route::post('delete_antecedenteForm', 'Empresa\ConfiguracionController@delete_antecedenteForm')->middleware('role');

Route::post('add_antecedenteAnswer', 'Empresa\ConfiguracionController@add_antecedenteAnswer')->middleware('role');

Route::post('edit_antecedenteAnswer', 'Empresa\ConfiguracionController@edit_antecedenteAnswer')->middleware('role');

Route::post('delete_antecedenteAnswer', 'Empresa\ConfiguracionController@delete_antecedenteAnswer')->middleware('role');

Route::post('delete_archivo_medico', 'Empresa\ConfiguracionController@delete_archivo_medico')->middleware('role');

Route::post('update_expediente_medico', 'Empresa\ConfiguracionController@update_expediente_medico');

Route::post('edit_expediente_medico', 'Empresa\ConfiguracionController@edit_expediente_medico');

Route::get('prueba_xml', 'Empresa\ConfiguracionController@prueba_xml');


Route::post('order', 'Empresa\ConfiguracionController@order')
    ->middleware('role');
// NOTE: Agenda

Route::get('Agenda', 'Empresa\AgendaController@index')
    ->name('agenda')
    ->middleware('role');

Route::post('newSchedule', 'Empresa\AgendaController@appointment')
    ->middleware('role');


Route::get('getSchedule', 'Empresa\AgendaController@getSchedule')
    ->middleware('role');

Route::put('updateSchedule', 'Empresa\AgendaController@updateScheduler')
    ->middleware('role');

Route::get('deleteSchedule/{id}', 'Empresa\AgendaController@deleteSchedule')
    ->middleware('role');

Route::get('notification', 'Empresa\AgendaController@notification')
    ->name('notification')
    ->middleware('role');

// NOTE: Consultorio Indicadores
//


Route::get('Estadisitica-Consultorio', 'Empresa\IndicadoresController@index')
    ->name('indicadoresConsultorio')
    ->middleware('role');

Route::get('/indicadoresConsultas', 'RouteController@vistaIndicadoresConsultas')->name('vistaIndicadoresConsultas')->middleware('role');
Route::get('/vistagenero/{genero}/{antecedente}', 'Empresa\ConsultaController@vistagenero')->name('vistagenereo')->middleware('role');
// NOTE:
//

Route::get('Mircrobiologia', 'Empresa\MicrobiologiaController@index')
    ->name('microbiologia')
    ->middleware('role');



// NOTE: Accesos
Route::get('Accesos', 'Empresa\AccesosController@index')
    ->name('accesos')
    ->middleware('role');

Route::get('permisos/{id}', 'Empresa\AccesosController@permisos')
    ->middleware('role');

Route::delete('deleteAccess', 'Empresa\AccesosController@deleteAcc')
    ->middleware('role');


Route::post('addPermiso', 'Empresa\AccesosController@addPermiso')
    ->middleware('role');

Route::post('addRol', 'Empresa\AccesosController@addRol')
    ->middleware('role');


Route::post('deleteRol', 'Empresa\AccesosController@addRolDelete')
    ->middleware('role');

    
//Patient login

// ELIMINAR TODA LA PARTE DE LOGIN DEL PACIENTE
Route::get('patient', 'Empresa\PatientController@index');


// Route::post('UpdateAllEstudios', 'Administrador\EstudiosController@estudiosUpdateAll');


// Rutas de app agenda
Route::get('api/getAllEstudios', 'Administrador\EstudiosController@getAllEstudios');
Route::get('api/getEstudioId/{estudio_id}', 'Administrador\EstudiosController@getEstudioId');


// Note : Ver Resultados desde la info del empleado

Route::get('getEstudiosPacienteToma/{id}', 'Empresa\Laboratorio\EstudiosController@getEstudiosToma');
//Obtiene estudios por NIM
Route::get('obtenertomacorreo/{toma}/{consecutivo}', 'Empresa\Laboratorio\EstudiosController@obtenertomacorreo');

Route::get('getEstudiosPro/{id}', 'Empresa\Laboratorio\EstudiosController@getEstudiosPro');



//DAICOMS

Route::get('/changeStatusAntecedentes/{id}/{status}', 'Empresa\AntecedentesController@changeStatusAntecedentes');
Route::get('/prueba', function () {
    return view('Laboratorio.Medicina.formularios.seleccion');
});
Route::get('dai/{id}/{id_estudio}', 'Empresa\Laboratorio\EstudiosController@Daicoms')->name('daicoms')->middleware('role');
Route::get('dai2/{id}/{id_estudio}', 'Empresa\Laboratorio\EstudiosController@Daicoms2');
Route::post('imagenologia', 'Empresa\Laboratorio\EstudiosController@image');
Route::post('labo_pdf', 'Empresa\Laboratorio\EstudiosController@labo_pdf');
Route::get('resultado_ima', 'Empresa\Laboratorio\EstudiosController@url_lab');

//Route::get('dai1/{id}', 'Empresa\Laboratorio\EstudiosController@getEstudiosToma')->name('daicoms')->middleware('role');
Route::get('/EstudiosProgramados','Empresa\EstudiosProgramadosController@estudiospro')->name('estudio')->middleware('role');



Route::get('dai/PDF/imagenologia/interpretacion/{clase}', 'PdfController@interpretacion')->name('impresion');
Route::get('dai/PDF/imagenologia/donwload/{clase}', 'PdfController@descargarinterpretacion')->name('descargarinterpretacion');

//Medicamentos

Route::get('historialmedicamentos/{curp}', 'Empresa\ConsultaController@historialmedicamentos')->name('historialmedicamentos');

