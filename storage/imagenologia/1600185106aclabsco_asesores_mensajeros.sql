-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-09-2020 a las 10:22:59
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aclabsco_asesores_mensajeros`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recorrido`
--

CREATE TABLE `recorrido` (
  `id` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL,
  `id_mensajero` varchar(11) NOT NULL,
  `estado` varchar(20) NOT NULL COMMENT 'Activo = recorrido actual, Inactivo = recorrido sin uso',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recorrido`
--

INSERT INTO `recorrido` (`id`, `id_ruta`, `id_mensajero`, `estado`, `created_at`, `updated_at`) VALUES
(11, 10, 'A021', 'Activo', '2020-09-10 12:34:12', '2020-09-10 12:34:12'),
(10, 5, 'A059', 'Activo', '2020-09-09 13:12:34', '2020-09-09 13:12:34'),
(9, 5, 'A059', 'Inactivo', '2020-09-09 12:17:55', '2020-09-09 12:17:55'),
(8, 5, 'A059', 'Inactivo', '2020-09-04 10:12:41', '2020-09-04 10:12:41'),
(12, 11, 'A009', 'Inactivo', '2020-09-10 12:34:33', '2020-09-10 12:34:33'),
(13, 9, 'T802', 'Activo', '2020-09-10 12:34:55', '2020-09-10 12:34:55'),
(14, 8, 'T803', 'Activo', '2020-09-10 12:35:45', '2020-09-10 12:35:45'),
(15, 14, 'A031', 'Activo', '2020-09-12 14:11:28', '2020-09-12 14:11:28'),
(16, 11, 'A009', 'Inactivo', '2020-09-12 14:11:55', '2020-09-12 14:11:55'),
(17, 11, 'A009', 'Activo', '2020-09-12 14:48:45', '2020-09-12 14:48:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recorridoDia`
--

CREATE TABLE `recorridoDia` (
  `id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_ruta` int(11) NOT NULL,
  `id_mensajero` varchar(255) NOT NULL,
  `arrayWayPoints` text NOT NULL,
  `estado` int(11) DEFAULT NULL COMMENT '1 = sin curso, 2 =en curso , 3 = terminado'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recorridoDia`
--

INSERT INTO `recorridoDia` (`id`, `fecha`, `id_ruta`, `id_mensajero`, `arrayWayPoints`, `estado`) VALUES
(12, '2020-09-04 15:13:54', 5, 'A059', '[{\"id_waypoint\":\"33\",\"hora\":\"10:18:53\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"30\",\"hora\":\"11:42:28\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"2\"},{\"id_waypoint\":\"28\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"27\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"26\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"24\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"23\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"22\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"21\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"25\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"19\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"35\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"36\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"37\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"}]', 2),
(11, '2020-09-04 14:25:40', 7, 'A009', '[{\"id_waypoint\":\"41\",\"hora\":\"09:30:07\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"40\",\"hora\":\"09:33:52\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"42\",\"hora\":\"09:34:17\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"2\"},{\"id_waypoint\":\"43\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"44\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"45\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"46\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"47\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"48\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"49\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"50\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"51\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"}]', 1),
(13, '2020-09-09 17:16:22', 5, 'A059', '[{\"id_waypoint\":\"33\",\"hora\":\"12:16:53\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"30\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"2\"},{\"id_waypoint\":\"28\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"27\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"26\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"24\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"23\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"22\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"21\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"25\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"19\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"35\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"36\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"37\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"}]', 2),
(14, '2020-09-10 18:42:18', 5, 'A059', '[{\"id_waypoint\":\"33\",\"hora\":\"13:42:26\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"30\",\"hora\":\"13:43:43\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"2\"},{\"id_waypoint\":\"28\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"27\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"26\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"24\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"23\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"22\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"21\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"25\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"19\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"35\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"36\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"37\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"}]', 2),
(15, '2020-09-12 19:44:27', 14, 'A031', '[{\"id_waypoint\":\"173\",\"hora\":\"15:07:10\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"174\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"2\"},{\"id_waypoint\":\"175\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"176\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"177\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"178\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"179\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"180\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"181\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"182\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"183\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"184\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"185\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"186\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"},{\"id_waypoint\":\"187\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"15\"}]', 2),
(16, '2020-09-12 19:45:05', 11, 'a009', '[{\"id_waypoint\":\"106\",\"hora\":\"14:50:21\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"107\",\"hora\":\"15:10:18\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"2\"},{\"id_waypoint\":\"108\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"109\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"110\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"111\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"112\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"113\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"114\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"115\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"116\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"117\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"118\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"119\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"},{\"id_waypoint\":\"120\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"15\"},{\"id_waypoint\":\"121\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"16\"},{\"id_waypoint\":\"122\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"17\"},{\"id_waypoint\":\"123\",\"hora\":\"15:10:04\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"18\"},{\"id_waypoint\":\"124\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"19\"},{\"id_waypoint\":\"125\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"20\"},{\"id_waypoint\":\"126\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"21\"},{\"id_waypoint\":\"127\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"22\"},{\"id_waypoint\":\"128\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"23\"},{\"id_waypoint\":\"129\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"24\"},{\"id_waypoint\":\"130\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"25\"},{\"id_waypoint\":\"131\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"26\"},{\"id_waypoint\":\"132\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"27\"},{\"id_waypoint\":\"133\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"28\"},{\"id_waypoint\":\"134\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"29\"},{\"id_waypoint\":\"135\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"30\"},{\"id_waypoint\":\"136\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"31\"},{\"id_waypoint\":\"137\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"32\"},{\"id_waypoint\":\"138\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"33\"}]', 2),
(17, '2020-09-12 19:49:15', 5, 'A059', '[{\"id_waypoint\":\"33\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"1\"},{\"id_waypoint\":\"30\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"2\"},{\"id_waypoint\":\"28\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"27\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"26\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"24\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"23\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"22\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"21\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"25\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"19\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"35\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"36\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"37\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"}]', 2),
(18, '2020-09-14 13:53:52', 14, 'A031', '[{\"id_waypoint\":\"173\",\"hora\":\"08:55:07\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"174\",\"hora\":\"09:46:14\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"2\"},{\"id_waypoint\":\"175\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":3},{\"id_waypoint\":\"176\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":4},{\"id_waypoint\":\"177\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":5},{\"id_waypoint\":\"178\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":6},{\"id_waypoint\":\"179\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":7},{\"id_waypoint\":\"182\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":8},{\"id_waypoint\":\"181\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":9},{\"id_waypoint\":\"180\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":10},{\"id_waypoint\":\"183\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":11},{\"id_waypoint\":\"184\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":12},{\"id_waypoint\":\"185\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":13},{\"id_waypoint\":\"187\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":14},{\"id_waypoint\":\"186\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":15},{\"id_waypoint\":\"207\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":16}]', 2),
(19, '2020-09-14 13:57:32', 11, 'a009', '[{\"id_waypoint\":\"106\",\"hora\":\"08:57:48\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"1\"},{\"id_waypoint\":\"107\",\"hora\":\"09:04:44\",\"coordenada\":\"\",\"estatus\":\"Terminado\",\"prioridad\":\"2\"},{\"id_waypoint\":\"108\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"3\"},{\"id_waypoint\":\"109\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"4\"},{\"id_waypoint\":\"110\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"5\"},{\"id_waypoint\":\"111\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"6\"},{\"id_waypoint\":\"112\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"7\"},{\"id_waypoint\":\"113\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"8\"},{\"id_waypoint\":\"114\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"9\"},{\"id_waypoint\":\"115\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"10\"},{\"id_waypoint\":\"116\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"11\"},{\"id_waypoint\":\"117\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"12\"},{\"id_waypoint\":\"118\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"13\"},{\"id_waypoint\":\"119\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"14\"},{\"id_waypoint\":\"120\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"15\"},{\"id_waypoint\":\"121\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"16\"},{\"id_waypoint\":\"122\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"17\"},{\"id_waypoint\":\"123\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"18\"},{\"id_waypoint\":\"124\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"19\"},{\"id_waypoint\":\"125\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"20\"},{\"id_waypoint\":\"126\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"21\"},{\"id_waypoint\":\"127\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"22\"},{\"id_waypoint\":\"128\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"23\"},{\"id_waypoint\":\"129\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"24\"},{\"id_waypoint\":\"130\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"25\"},{\"id_waypoint\":\"131\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"26\"},{\"id_waypoint\":\"132\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"27\"},{\"id_waypoint\":\"133\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"28\"},{\"id_waypoint\":\"134\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"29\"},{\"id_waypoint\":\"135\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"30\"},{\"id_waypoint\":\"136\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"31\"},{\"id_waypoint\":\"137\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"32\"},{\"id_waypoint\":\"138\",\"hora\":\"\",\"coordenada\":\"\",\"estatus\":\"Pendiente\",\"prioridad\":\"33\"}]', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas`
--

CREATE TABLE `rutas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `inicio` varchar(550) DEFAULT NULL,
  `fin` varchar(550) DEFAULT NULL,
  `gps` varchar(550) DEFAULT NULL,
  `google_maps` text,
  `distancia` double DEFAULT NULL COMMENT 'metros',
  `tiempo` double DEFAULT NULL COMMENT 'segundos',
  `empleado` varchar(10) NOT NULL COMMENT 'clave del empleado quien creo la ruta',
  `estado` int(11) DEFAULT NULL COMMENT '1 - activo',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rutas`
--

INSERT INTO `rutas` (`id`, `nombre`, `inicio`, `fin`, `gps`, `google_maps`, `distancia`, `tiempo`, `empleado`, `estado`, `created_at`, `updated_at`) VALUES
(10, 'RUTA XOXTLA, HUEJO, CHOLULA Y SAN MARTIN', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.0646962,-98.2999019/19.0605746,-98.3057726/19.0494967,-98.299515/19.04881989999999,-98.3019473/19.0605008,-98.3096901/19.0681109,-98.3081093/19.0637108,-98.3087291/19.0645311,-98.3040223/19.1515979,-98.408954/19.2717653,-98.4407963/19.29775996027446,-98.45309952883605/19.28643229999999,-98.44006399999999/19.2832079,-98.43588299999999/19.2169978,-98.37677169999999/19.2820961,-98.4364092/19.2816843,-98.43592269999999/19.2804295,-98.43515149999999/19.2807807,-98.4365164/19.1670087,-98.30799410000002/19.0626266,-98.30001209999999/19.051095,-98.231430', 126841, 12536, 'A005', 1, '2020-09-10 10:55:03', '2020-09-10 10:55:03'),
(9, 'RUTA TECAMACHALCO', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/18.9678501,-97.9039239/18.968256,-97.90009590000001/18.968187,-97.9001954/18.9577124,-97.90781869999999/18.9637334,-97.90125959999999/18.9644066,-97.8965551/18.9636844,-97.9036861/18.9643978,-97.90689290000002/18.9585884,-97.9039079/18.8839785,-97.7268038/18.8858387,-97.7455591/18.8823111,-97.7268079/18.8854279,-97.7342652/18.8815305,-97.7348457/18.8687311,-97.7172284/18.9782722,-97.77465649999999/18.9820971,-97.7866016/18.9802435,-97.78668719999999/18.9826963,-97.785232/18.9813766,-97.7836074/19.051095,-98.231430', 173206, 15170, 'A005', 1, '2020-09-10 10:21:32', '2020-09-10 10:21:32'),
(5, 'RUTA PUEBLA 2', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.03412879999999,-98.1878765/19.034125,-98.188131/19.034147,-98.1858563/19.0345615,-98.19077949999999/19.0454561,-98.205232/19.0464199,-98.232609/19.0458481,-98.2271451/19.0588209,-98.2260647/19.0500837,-98.2164033/19.0464199,-98.232609/19.05774854078839,-98.2096041018524/19.0341244,-98.1894664/19.1190895,-98.1709094/19.01796,-98.1920019/19.051095,-98.231430', 64351, 11020, 'A005', 1, '2020-09-03 15:54:11', '2020-09-03 15:54:11'),
(7, 'Prueba', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.034147,-98.1858563/19.0339044,-98.1884522/19.04066629999999,-98.20286720000001/19.0894414,-98.130285/19.1589453,-98.40471629999999/19.0451057,-98.2370021/19.0406357,-98.2329191/19.052839,-98.2213864/19.0417696,-98.2253322/19.0377478,-98.21736290000001/19.0379883,-98.2197576/19.0410154,-98.22173819999999/19.0715049,-98.31567419999999/19.056582,-98.2240367/19.051799,-98.2249784/19.0894414,-98.130285/19.051095,-98.231430', 134070, 13144, 'A052', 1, '2020-09-03 17:59:57', '2020-09-03 17:59:57'),
(8, 'RUTA HUAMANTLA', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.323470544722113,-98.21025229259033/19.3680887,-98.2111151/19.3694177,-98.2126169/19.4190192,-98.1446251/19.4158074,-98.13529609999999/19.4138559,-98.139287/19.4136862,-98.13717/19.4118233,-98.138548/19.410671,-98.1414679/19.3294074,-98.19743229999999/19.4076279,-98.1433529/19.409108,-98.14533899999999/19.3161485,-97.92387850000001/19.2265078,-97.8063306/19.051095,-98.231430', 184314, 13632, 'A005', 1, '2020-09-09 13:20:23', '2020-09-09 13:20:23'),
(11, 'RUTA IZUCAR DE MATAMOROS', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/18.6061643,-98.4654812/18.6042597,-98.46402739999999/18.6045592,-98.46553399999999/18.6035848,-98.46433069999999/18.6012445,-98.45702690000002/18.6009139,-98.4666877/18.59703559999999,-98.467669/18.5626846,-98.4744206/18.6002699,-98.46585619999999/18.6005867,-98.46656490000001/18.6051952,-98.4571268/18.5974127,-98.45246/18.6051795,-98.4571323/18.6012198,-98.4674807/18.6012204,-98.4664377/18.6023573,-98.46627819999999/18.60780099999999,-98.4654173/18.8988324,-98.4363083/18.9008559,-98.4384931/18.9018797,-98.436016/18.9060016,-98.43553089999999/18.9064814,-98.4345053/18.9084871,-98.4336091/18.9087659,-98.43379999999999/18.9084634,-98.43382539999999/18.90838849999999,-98.4347659/18.9090148,-98.4382899/18.9098141,-98.445337/18.9074684,-98.4404112/18.9060382,-98.4355978/18.9155988,-98.43323219999999/18.9084972,-98.43486999999999/18.9110703,-98.4321049/19.051095,-98.231430', 163473, 15800, 'A005', 1, '2020-09-10 11:48:31', '2020-09-10 11:48:31'),
(12, 'RUTA TLAXCALA', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.2184443,-98.2484296/19.2144619,-98.2361751/19.2171724,-98.23867650000001/19.2161791,-98.24218630000001/19.3003934,-98.2598701/19.30741159999999,-98.24069399999999/19.3026661,-98.2377875/19.31078849999999,-98.23500159999999/19.312742,-98.23734809999999/19.3077752,-98.1913873/19.4634739,-98.42465779999999/19.3246304,-98.241619/19.329378,-98.22286249999999/19.3224884,-98.1644609/19.3158839,-98.1929845/19.3103971,-98.1940936/19.3357841,-98.2142159/19.3083201,-98.22054120000001/19.4076559,-98.14332350000001/19.051095,-98.231430', 217445, 19306, 'A005', 1, '2020-09-11 16:54:07', '2020-09-11 16:54:07'),
(13, 'RUTA PUEBLA 3', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.0367405,-98.204562/19.0315799,-98.20622019999999/18.974702,-98.23953949999999/18.997496,-98.23948039999999/19.0037703,-98.2308868/19.0037703,-98.2308868/19.0037703,-98.2308868/19.0037703,-98.2308868/19.0037703,-98.2308868/19.0369416,-98.2143567/19.0304868,-98.228433/19.0324125,-98.208034/19.038723,-98.2131402/19.038542,-98.21674279999999/19.032755976878953,-98.21178250370482/19.051095,-98.231430', 36429, 6476, 'A005', 1, '2020-09-12 13:14:40', '2020-09-12 13:14:40'),
(14, 'RUTA PUEBLA 1', '{\"lat\":19.051252000000001629587131901644170284271240234375,\"lng\":-98.2316289999999980864231474697589874267578125}', '{\"lat\":19.051095000000000112549969344399869441986083984375,\"lng\":-98.2314300000000031332092476077377796173095703125}', NULL, 'https://www.google.com/maps/dir/19.051252,-98.231629/19.0341244,-98.1894664/19.01813243462303,-98.19193752698362/19.0904031,-98.2173091/19.0833644,-98.21534840000001/19.0611468,-98.2259919/19.06070904351533,-98.19656112090148/19.0365472,-98.17352819999999/19.0237142,-98.1805206/19.0535621,-98.15711680000001/19.0454935,-98.15897299999999/19.0528714,-98.1480989/19.0696787,-98.1900764/19.0484659,-98.1791604/19.0606205,-98.19654480000001/19.0919911,-98.1994598/19.0671371,-98.2129569/19.051095,-98.231430', 77102, 12347, 'A005', 1, '2020-09-12 13:53:14', '2020-09-12 13:53:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trayecto`
--

CREATE TABLE `trayecto` (
  `id` int(11) NOT NULL,
  `id_recorrido` int(11) NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `waypoints`
--

CREATE TABLE `waypoints` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `direccion` text,
  `latitud` varchar(250) DEFAULT NULL,
  `longitud` varchar(250) DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL COMMENT 'prioridad rango total de waypoints',
  `estado` int(11) DEFAULT '1' COMMENT '1- activo, 2 eliminado',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `waypoints`
--

INSERT INTO `waypoints` (`id`, `nombre`, `id_ruta`, `direccion`, `latitud`, `longitud`, `prioridad`, `estado`, `created_at`, `updated_at`) VALUES
(33, 'HOSPITAL BETANIA', 5, '1826 Av 11 Ote Azcarate', '19.03412879999999', '-98.1878765', 1, 1, '2020-09-03 17:22:58', '2020-09-03 17:22:58'),
(30, 'LABORATORIO CLÍNICO AYALA', 5, '1826 Av 11 Ote Azcarate', '19.034125', '-98.188131', 2, 1, '2020-09-03 17:21:33', '2020-09-03 17:21:33'),
(28, 'OMEGA IMAGEN', 5, '709 Calle 24 Sur Azcarate', '19.034147', '-98.1858563', 3, 1, '2020-09-03 16:43:17', '2020-09-03 16:43:17'),
(27, 'LABORATORIO 141', 5, '1608 Av. 13 Ote. Zona Sin Asignación de Nombre de Col 39', '19.0345615', '-98.19077949999999', 4, 1, '2020-09-03 16:42:38', '2020-09-03 16:42:38'),
(26, 'CHRISTUS MUGUERZA HOSPITALES, SA DE CV (UPAEP)', 5, '715 Av 5 Pte Centro', '19.0454561', '-98.205232', 5, 1, '2020-09-03 16:41:32', '2020-09-03 16:41:32'),
(24, 'LABORATORIO 111', 5, '3711 Av. Manuel Espinosa Yglesias 31 Pte. Residencial Esmeralda', '19.0464199', '-98.232609', 6, 1, '2020-09-03 16:38:11', '2020-09-03 16:38:11'),
(23, 'CDO', 5, 'Av 27 Pte Sta Cruz los Ángeles Puebla', '19.0458481', '-98.2271451', 7, 1, '2020-09-03 16:37:03', '2020-09-03 16:37:03'),
(22, 'UNIDAD HOSPITALARIA LA PAZ', 5, '92 Av Reforma Sur La Paz', '19.0588209', '-98.2260647', 8, 1, '2020-09-03 16:35:21', '2020-09-03 16:35:21'),
(21, 'ZAUS', 5, '702 Calle 23 Sur Zona Esmeralda', '19.0500837', '-98.2164033', 9, 1, '2020-09-03 16:34:46', '2020-09-03 16:34:46'),
(25, 'CHRISTUS MUGUERZA HOSPITALES, SA DE CV (UPAEP)', 5, '3711 Av. Manuel Espinosa Yglesias 31 Pte. Residencial Esmeralda', '19.0464199', '-98.232609', 10, 1, '2020-09-03 16:40:22', '2020-09-03 16:40:22'),
(19, 'LABORATORIO VIER', 5, '1907 Calle 16 Pte. Jesús García', '19.05774854078839', '-98.2096041018524', 11, 1, '2020-09-03 16:33:52', '2020-09-03 16:33:52'),
(43, 'IMAGEN DIAGNOSTICA', 7, 'Calle 3 Sur Centro Puebla', '19.04066629999999', '-98.20286720000001', 3, 1, '2020-09-03 20:32:00', '2020-09-03 20:32:00'),
(41, 'OMEGA IMAGEN', 7, '709 Calle 24 Sur Azcarate', '19.034147', '-98.1858563', 1, 1, '2020-09-03 20:31:00', '2020-09-03 20:31:00'),
(35, 'IMAGEN DIAGNOSTICA', 5, '1110 Calle 18 Sur Barrio de Analco', '19.0341244', '-98.1894664', 12, 1, '2020-09-03 17:24:03', '2020-09-03 17:24:03'),
(36, 'LABORATORIO PIEDAD LAB', 5, '11 Tlaxcala Pte San Bartolomé', '19.1190895', '-98.1709094', 13, 1, '2020-09-03 17:25:28', '2020-09-03 17:25:28'),
(37, 'HOSPITAL LOS PILARES', 5, '4312 Calle 24 Sur Alseseca', '19.01796', '-98.1920019', 14, 1, '2020-09-03 17:25:58', '2020-09-03 17:25:58'),
(42, 'HOSPITAL BETANIA', 7, '1817 Av 11 Ote Azcarate', '19.0339044', '-98.1884522', 2, 1, '2020-09-03 20:31:29', '2020-09-03 20:31:29'),
(40, 'san aparicio', 7, 'San Aparicio Puebla Pue.', '19.0844083', '-98.164756', 1, 2, '2020-09-03 18:02:15', '2020-09-03 18:02:15'),
(44, ':)', 7, '2 de Marzo La Resurrección Noche Buena', '19.0894414', '-98.130285', 4, 1, '2020-09-03 21:49:47', '2020-09-03 21:49:47'),
(45, 'pruebas', 7, 'Huejotzingo Pue. MX', '19.1589453', '-98.40471629999999', 5, 1, '2020-09-03 21:54:52', '2020-09-03 21:54:52'),
(46, 'ultimo', 7, '3304 Blvrd Atlixco Nueva Antequera', '19.0451057', '-98.2370021', 6, 1, '2020-09-03 22:09:47', '2020-09-03 22:09:47'),
(47, 'animas', 7, 'Animas Puebla Pue.', '19.0406357', '-98.2329191', 7, 1, '2020-09-03 22:10:42', '2020-09-03 22:10:42'),
(48, 'huexotitla', 7, 'N°410 Boulevard I Zaragoza Corredor Industrial la Ciénega', '19.0706137', '-98.1740049', 8, 2, '2020-09-03 22:17:11', '2020-09-03 22:17:11'),
(49, 'masasas', 7, '102 Av 7 Pte Centro', '19.042364', '-98.2001', 9, 2, '2020-09-03 22:32:21', '2020-09-03 22:32:21'),
(50, 'jajajajaja', 7, '2 Acatzingo La Paz', '19.051799', '-98.2249784', 9, 2, '2020-09-03 22:33:14', '2020-09-03 22:33:14'),
(51, 'dormir', 7, 'Unidad Palma Puebla Pue.', '19.0537155', '-98.2330901', 11, 2, '2020-09-03 22:33:56', '2020-09-03 22:33:56'),
(52, 'BIOSCAN TORRE', 8, 'Plaza Premier Industrial Chiautempan', '19.323470544722113', '-98.21025229259033', 1, 1, '2020-09-09 13:21:23', '2020-09-09 13:21:23'),
(53, 'HOSPITAL INFANTIL DE TLAXCALA', 8, 'Apetatitlán Tlax. MX', '19.3680887', '-98.2111151', 2, 1, '2020-09-09 13:21:52', '2020-09-09 13:21:52'),
(54, 'INSTITUTO TLAXCALTECA DE ASISTENCIA ESPECIALIZADA A LA SALUD', 8, 'km 2.5 S/N 20 de Noviembre', '19.3694177', '-98.2126169', 3, 1, '2020-09-09 13:33:34', '2020-09-09 13:33:34'),
(55, 'BIOQUIN', 8, '913 Calle 5 de Febrero Centro', '19.4190192', '-98.1446251', 4, 1, '2020-09-09 13:40:19', '2020-09-09 13:40:19'),
(56, 'LABORATORIO LEZAMA', 8, '402 Av. 5 de Mayo Centro', '19.4158074', '-98.13529609999999', 5, 1, '2020-09-09 13:41:11', '2020-09-09 13:41:11'),
(57, 'LABORATORIO CLINICO REEMPE', 8, '301 Av Juárez Centro', '19.4138559', '-98.139287', 6, 1, '2020-09-09 13:43:13', '2020-09-09 13:43:13'),
(58, 'LABORATORIO SAN MARTIN DE PORRES', 8, '116 Av Libertad Centro', '19.4136862', '-98.13717', 7, 1, '2020-09-09 14:07:30', '2020-09-09 14:07:30'),
(59, 'LAB 945', 8, '806 Aquiles Serdán Jesús y San Juan', '19.4118233', '-98.138548', 8, 1, '2020-09-09 14:10:13', '2020-09-09 14:10:13'),
(60, 'BIOSCAN APIZACO', 8, '405 Calle Francisco Sarabia Centro', '19.410671', '-98.1414679', 9, 1, '2020-09-09 14:11:17', '2020-09-09 14:11:17'),
(61, 'CLINIC LAB 800', 8, '25 Emiliano Zapata San Pablo Apetatitlan', '19.3294074', '-98.19743229999999', 10, 1, '2020-09-09 14:36:39', '2020-09-09 14:36:39'),
(62, 'OMEGA IMAGEN', 8, '401 Calle Centenario Fátima', '19.4076279', '-98.1433529', 11, 1, '2020-09-09 14:39:45', '2020-09-09 14:39:45'),
(63, 'BIODIAGNOSTICS', 8, '1511-7 Av Xicohtencatl Centro', '19.409108', '-98.14533899999999', 12, 1, '2020-09-09 14:41:46', '2020-09-09 14:41:46'),
(64, 'UDN HUAMANTLA', 8, 'Centro Huamantla Tlax.', '19.3161485', '-97.92387850000001', 13, 1, '2020-09-10 10:04:16', '2020-09-10 10:04:16'),
(65, 'UDN GRAJALES', 8, 'Local D No 1 Calle 5 Sur', '19.2265078', '-97.8063306', 14, 1, '2020-09-10 10:05:09', '2020-09-10 10:05:09'),
(66, 'SONOLABS TEPEACA', 9, '10 Calle 2 Ote. Barrio del Centro', '18.9678501', '-97.9039239', 1, 1, '2020-09-10 10:22:13', '2020-09-10 10:22:13'),
(67, 'LABORATORIO DE ANALISIS CLINICOS 911', 9, '206 Calle 2 Nte. Barrio del Centro', '18.968256', '-97.90009590000001', 2, 1, '2020-09-10 10:22:42', '2020-09-10 10:22:42'),
(68, 'LABORATORIO DE ANALISIS CLINICOS SERRANO 332', 9, 'Calle 2 Nte. Barrio del Centro Tepeaca', '18.968187', '-97.9001954', 3, 1, '2020-09-10 10:23:18', '2020-09-10 10:23:18'),
(69, 'LABORATORIO  HERRERA ROJAS DAISY 919', 9, '2a. 11 Poniente Sta Cruz Tlahuiloxa Tepeaca', '18.9577124', '-97.90781869999999', 4, 1, '2020-09-10 10:24:40', '2020-09-10 10:24:40'),
(70, 'LABORATORIO PEDRO GONZALEZ  MARTINEZ 937', 9, 'Cuauhtémoc Sur El Divino Salvador Tepeaca', '18.9637334', '-97.90125959999999', 5, 1, '2020-09-10 10:25:10', '2020-09-10 10:25:10'),
(71, 'LABORATORIO ERIKA MOTA TREVIÑO 936', 9, 'Miguel Negrete Ote San Sebastián Tepeaca', '18.9644066', '-97.8965551', 6, 1, '2020-09-10 10:26:25', '2020-09-10 10:26:25'),
(72, 'LABORATORIO SAN JOAQUIN 434', 9, '206 Av. Gral. Maximino Ávila Camacho Barrio del Centro', '18.9636844', '-97.9036861', 7, 1, '2020-09-10 10:27:09', '2020-09-10 10:27:09'),
(73, 'LABORATORIO DE ANALISIS CLINICOS BIOTECK 926', 9, 'Calle 5 Pte. La Santisima Tepeaca', '18.9643978', '-97.90689290000002', 8, 1, '2020-09-10 10:29:11', '2020-09-10 10:29:11'),
(74, 'LABORATORIO ACCLAB 910', 9, '106 Av. Gral. Maximino Ávila Camacho Barrio del Centro', '18.9585884', '-97.9039079', 9, 1, '2020-09-10 10:29:49', '2020-09-10 10:29:49'),
(75, 'HOSPITAL DE JESUS', 9, '2 Calle 4 Nte. El Calvario', '18.8839785', '-97.7268038', 10, 1, '2020-09-10 10:31:21', '2020-09-10 10:31:21'),
(76, 'LAB JAIME QUIROZ ROSAS 905', 9, 'Barrio de San Juan Tecamachalco Pue.', '18.8858387', '-97.7455591', 11, 1, '2020-09-10 10:32:00', '2020-09-10 10:32:00'),
(77, 'LABORATORIO GUSTAVO CAMPOS OSORIO 918', 9, '305 Calle 6 Sur Centro', '18.8823111', '-97.7268079', 12, 1, '2020-09-10 10:32:26', '2020-09-10 10:32:26'),
(78, 'GRUPO GALENUS DEL VALLE DE TECAMACHALCO S.C 892', 9, '304 Calle 11 Sur Centro', '18.8854279', '-97.7342652', 13, 1, '2020-09-10 10:34:46', '2020-09-10 10:34:46'),
(79, 'LABORATORIO MEDICSCANNER 914', 9, '736 Av. 11 Pte. San Antonio', '18.8815305', '-97.7348457', 14, 1, '2020-09-10 10:47:54', '2020-09-10 10:47:54'),
(80, 'LABORATORIO BIOCHECK LABS  890', 9, '9 Ote. Centro Tecamachalco', '18.8687311', '-97.7172284', 15, 1, '2020-09-10 10:48:26', '2020-09-10 10:48:26'),
(81, 'LABORATORIO INMUNO ACATZINGO 35', 9, 'Calle 3 Ote. Jesús de Alonso Acatzingo de Hidalgo', '18.9782722', '-97.77465649999999', 16, 1, '2020-09-10 10:49:04', '2020-09-10 10:49:04'),
(82, 'LABORATORIO EL ANGEL 906', 9, '112 Calle 2 Pte. San Gabriel', '18.9820971', '-97.7866016', 17, 1, '2020-09-10 10:49:38', '2020-09-10 10:49:38'),
(83, 'LABORATORIO JIMENEZ BAUTISTA MARTHA LETICIA 912', 9, '130 Avenida Rodolfo Schez. Taboada Poniente San José', '18.9802435', '-97.78668719999999', 18, 1, '2020-09-10 10:51:18', '2020-09-10 10:51:18'),
(84, 'SANATORIO SANTA TERESITA DEL NIÑO JESUS S.C. 908', 9, '102 Av. 4 Pte. San Antonio', '18.9826963', '-97.785232', 19, 1, '2020-09-10 10:51:42', '2020-09-10 10:51:42'),
(85, 'FAMILY LABS ACATZINGO', 9, '6 Calle 2 Ote. San Miguel', '18.9813766', '-97.7836074', 20, 1, '2020-09-10 10:53:20', '2020-09-10 10:53:20'),
(86, 'LABORATORIO TRUJILLO LUZ MARIA', 10, '603 Av. 12 Ote. Barrio de Jesús Tlatempa', '19.0646962', '-98.2999019', 1, 1, '2020-09-10 11:03:48', '2020-09-10 11:03:48'),
(87, 'CLINICA INTEGRAL DE IMAGEN DE CHOLULA S.A. DE C.V. 501', 10, '103 Calle 2 Sur Centro', '19.0605746', '-98.3057726', 2, 1, '2020-09-10 11:05:36', '2020-09-10 11:05:36'),
(88, 'ULTRASONIDO DE DIAGNOSTICO MEDICO 195', 10, '3 Calle 3 Ote. Centro San Andrés Cholula', '19.0494967', '-98.299515', 3, 1, '2020-09-10 11:15:49', '2020-09-10 11:15:49'),
(89, 'LABORATORIO ZAPOTECAS COSIO OLIVIA 386', 10, '114 Calle 7 Pte. San Andresito', '19.04881989999999', '-98.3019473', 4, 1, '2020-09-10 11:16:21', '2020-09-10 11:16:21'),
(90, 'LABORATORIO CASTILLO ANALCO SONIA 514', 10, '506 Calle 3 Sur Barrio de Sta Maria Xixitla', '19.0605008', '-98.3096901', 5, 1, '2020-09-10 11:16:48', '2020-09-10 11:16:48'),
(91, 'LABORATORIO JAVIER  IGNACIO AVILA ALVAREZ 68', 10, 'Av 12 Pte Cholula Pue.', '19.0681109', '-98.3081093', 6, 1, '2020-09-10 11:17:22', '2020-09-10 11:17:22'),
(92, 'LABORATORIO CASIMIRA YOLANDA JUDITH FLORES NUÑEZ 314', 10, 'Av 2 Pte Centro Cholula', '19.0637108', '-98.3087291', 7, 1, '2020-09-10 11:17:55', '2020-09-10 11:17:55'),
(93, 'LABORATORIO LETICIA IBARRA ZAMUDIO 504', 10, '803 Calle 2 Nte. Centro', '19.0645311', '-98.3040223', 8, 1, '2020-09-10 11:18:20', '2020-09-10 11:18:20'),
(94, 'UDN HUEJOTZINGO', 10, '1099 La Soledad Segundo Barrio', '19.1515979', '-98.408954', 9, 1, '2020-09-10 11:25:09', '2020-09-10 11:25:09'),
(95, 'LABORATORIO AURORA GALICIA SANDOVAL 465', 10, '13 Manuel P Montes Sur El Moral', '19.2717653', '-98.4407963', 10, 1, '2020-09-10 11:26:24', '2020-09-10 11:26:24'),
(96, 'LABORATORIO DANIEL SANCHEZ NIETO 060A', 10, '32 20 de Noviembre San Cristobal Tepatlaxco', '19.29775996027446', '-98.45309952883605', 11, 1, '2020-09-10 11:26:59', '2020-09-10 11:26:59'),
(97, 'CENTRO DE DIAGNOSTICO TEXMELUCAN, SA. DE CV.  060A', 10, '306 Libertad Norte Col Centro', '19.28643229999999', '-98.44006399999999', 12, 1, '2020-09-10 11:27:47', '2020-09-10 11:27:47'),
(98, 'LABORATORIO ARELLANO NUÑEZ CATALINA 470', 10, '101 B. Juárez Poniente Col Centro', '19.2832079', '-98.43588299999999', 13, 1, '2020-09-10 11:28:19', '2020-09-10 11:28:19'),
(99, 'LABORATORIO CASCO SALGADO MARTHA 452', 10, '7 Calle Sta. Anita Sta Cruz', '19.2169978', '-98.37677169999999', 14, 1, '2020-09-10 11:29:27', '2020-09-10 11:29:27'),
(100, 'LABORATORIO CAMELA HERNANDEZ EVELYN YADIRA 460', 10, '103 J. Ma. Morelos Poniente Col Centro', '19.2820961', '-98.4364092', 15, 1, '2020-09-10 11:29:56', '2020-09-10 11:29:56'),
(101, 'LABORATORIO JUAREZ SUAREZ MARTHA 459', 10, '208 Avenida I. Zaragoza Sur Col Centro', '19.2816843', '-98.43592269999999', 16, 1, '2020-09-10 11:30:21', '2020-09-10 11:30:21'),
(102, 'LABORATORIO INSTITUTO MEDICO NATURISTA ADRIAN GUZMAN 025A', 10, '404 Libertad Sur Col Centro', '19.2804295', '-98.43515149999999', 17, 1, '2020-09-10 11:31:19', '2020-09-10 11:31:19'),
(103, 'LABORATORIO VET EXPERTS 275', 10, '302 Avenida I. Zaragoza Sur Col los Dicios', '19.2807807', '-98.4365164', 18, 1, '2020-09-10 11:31:44', '2020-09-10 11:31:44'),
(104, 'UDN XOXTLA', 10, '6 Aldama San Miguel Xoxtla', '19.1670087', '-98.30799410000002', 19, 1, '2020-09-10 11:44:41', '2020-09-10 11:44:41'),
(105, 'HOSPITAL VETERINARIO DE CHOLULA 129', 10, '611 Av. 8 Ote. Barrio de Jesús Tlatempa', '19.0626266', '-98.30001209999999', 20, 1, '2020-09-10 11:45:35', '2020-09-10 11:45:35'),
(106, 'LABORATORIO MENTADO VARGAS  IRMA 606', 11, '37 Av. Centenario Centro', '18.6061643', '-98.4654812', 1, 1, '2020-09-10 11:48:55', '2020-09-10 11:48:55'),
(107, 'LABORATORIO TECNOLAB (JOLALPAN) 653', 11, '16 Zaragoza Santiago Mihuacán', '18.6042597', '-98.46402739999999', 2, 1, '2020-09-10 11:49:16', '2020-09-10 11:49:16'),
(108, 'LABORATORIO JUAN LUIS TORIJANO CRUZ 607', 11, '102 Av. Centenario Centro', '18.6045592', '-98.46553399999999', 3, 1, '2020-09-10 11:49:41', '2020-09-10 11:49:41'),
(109, 'LABORATORIO AMIGON ARIZA ESTEBAN ANTONIO 693', 11, '9 Zaragoza Centro', '18.6035848', '-98.46433069999999', 4, 1, '2020-09-10 11:50:17', '2020-09-10 11:50:17'),
(110, 'LABORATORIO ALVAREZ ROSANO VICTOR 694', 11, 'Priv. Nicolás Bravo Independencia Barrio de San Juan Piaxtla', '18.6012445', '-98.45702690000002', 5, 1, '2020-09-10 11:50:54', '2020-09-10 11:50:54'),
(111, 'LABORATORIO MOISES RUIZ TIRADO 690', 11, '6 Independencia Centro', '18.6009139', '-98.4666877', 6, 1, '2020-09-10 11:51:16', '2020-09-10 11:51:16'),
(112, 'LABORATORIO CLINI LAB 426', 11, '408 Calle Hidalgo San Bernardo', '18.59703559999999', '-98.467669', 7, 1, '2020-09-10 11:51:35', '2020-09-10 11:51:35'),
(113, 'LABORATORIO SUSANA MENDEZ OCHOA 587', 11, '7 Calle prolongación Izúcar de Matamoros', '18.5626846', '-98.4744206', 8, 1, '2020-09-10 11:52:30', '2020-09-10 11:52:30'),
(114, 'LABORATORIO AZCUE ALARCON ADRIANA 642', 11, '20 Niño Perdido Centro', '18.6002699', '-98.46585619999999', 9, 1, '2020-09-10 11:53:00', '2020-09-10 11:53:00'),
(115, 'FAMILY LABS IZUCAR DE MATAMOROS', 11, 'Calle Hidalgo Centro Izúcar de Matamoros', '18.6005867', '-98.46656490000001', 10, 1, '2020-09-10 11:54:16', '2020-09-10 11:54:16'),
(116, 'LABORATORIO TORRES MARTINEZ IRENE 698', 11, '236 Calle de la Reforma San Diego', '18.6051952', '-98.4571268', 11, 1, '2020-09-10 11:54:43', '2020-09-10 11:54:43'),
(117, 'LABORATORIO GRUPO ROSAS LOPEZ,S.A.DE C.V. 692', 11, '80 Vicente Guerrero Morelos', '18.5974127', '-98.45246', 12, 1, '2020-09-10 12:03:45', '2020-09-10 12:03:45'),
(118, 'LABORATORIO CLINICO 691', 11, '107 Calle de la Reforma San Diego', '18.6051795', '-98.4571323', 13, 1, '2020-09-10 12:06:17', '2020-09-10 12:06:17'),
(119, 'LABORATORIO GUILLERMO RIOS VEGA 608', 11, '16 Independencia Centro', '18.6012198', '-98.4674807', 14, 1, '2020-09-10 12:07:35', '2020-09-10 12:07:35'),
(120, 'LABORATORIO VITROLAB 613', 11, '118 Calle Hidalgo Centro', '18.6012204', '-98.4664377', 15, 1, '2020-09-10 12:07:57', '2020-09-10 12:07:57'),
(121, 'LABORATORIO CORDERO AJURIA GUILLERMINA 643', 11, '15 De La Constitución Centro', '18.6023573', '-98.46627819999999', 16, 1, '2020-09-10 12:08:23', '2020-09-10 12:08:23'),
(122, 'LABORATORIO ELOINA INFANTE  CAZALES 674', 11, '1 Av. Centenario Centro', '18.60780099999999', '-98.4654173', 17, 1, '2020-09-10 12:09:03', '2020-09-10 12:09:03'),
(123, 'LABORATORIO SIMON CARRETO HURTADO 549', 11, '1905 Calle 2 Sur Francisco I. Madero', '18.8988324', '-98.4363083', 18, 1, '2020-09-10 12:16:13', '2020-09-10 12:16:13'),
(124, 'LABORATORIO LABCLONE, S.A. DE C.V. 550', 11, '301 Calle 17 Pte. Álvaro Obregón', '18.9008559', '-98.4384931', 19, 1, '2020-09-10 12:16:34', '2020-09-10 12:16:34'),
(125, 'BIOMEDICOS Y PATOLOGOS CLINICOS ASOCIADOS, S.C. 326', 11, '1161 Encino Prado Sur', '18.9018797', '-98.436016', 20, 1, '2020-09-10 12:17:08', '2020-09-10 12:17:08'),
(126, 'LABORATORIO QUIROZ ROMERO MARIA ISABEL 563', 11, 'La Paz Centro Centro', '18.9060016', '-98.43553089999999', 21, 1, '2020-09-10 12:22:32', '2020-09-10 12:22:32'),
(127, 'LABORATORIO DE LA TORRE PINTO CARMEN 558', 11, '315 Calle 2 Sur Centro', '18.9064814', '-98.4345053', 22, 1, '2020-09-10 12:22:56', '2020-09-10 12:22:56'),
(128, 'LABORATORIO MENDEL 556', 11, '101 Calle 2 Sur Centro', '18.9084871', '-98.4336091', 23, 1, '2020-09-10 12:23:22', '2020-09-10 12:23:22'),
(129, 'LABORATORIO ZAUS (ATLIXCO) 093A', 11, '102 Calle 2 Sur Centro', '18.9087659', '-98.43379999999999', 24, 1, '2020-09-10 12:23:41', '2020-09-10 12:23:41'),
(130, 'EBENEZER PLAZA DE LA SALUD 048A', 11, '101 Calle 2 Sur Centro', '18.9084634', '-98.43382539999999', 25, 1, '2020-09-10 12:25:14', '2020-09-10 12:25:14'),
(131, 'LABORATORIO GLOBULAB 622', 11, '109 Av Independencia Centro', '18.90838849999999', '-98.4347659', 26, 1, '2020-09-10 12:25:45', '2020-09-10 12:25:45'),
(132, 'LABORATORIO JARAMILLO PINEDA MARIA DEL CARMEN 554', 11, '902 Calle 3 Poniente Centro', '18.9090148', '-98.4382899', 27, 1, '2020-09-10 12:26:39', '2020-09-10 12:26:39'),
(133, 'LABORATORIO LA TRINIDAD 555', 11, '2308 Calle 5 Pte. Solares Grandes', '18.9098141', '-98.445337', 28, 1, '2020-09-10 12:28:55', '2020-09-10 12:28:55'),
(134, 'LABORATORIO MORALES TORRES ROSALIA 560', 11, '509 Calle Degollado Centro', '18.9074684', '-98.4404112', 29, 1, '2020-09-10 12:29:45', '2020-09-10 12:29:45'),
(135, 'LABORATORIO RUIZ  CANDIA MARISOL 562', 11, '504 Av Independencia Centro', '18.9060382', '-98.4355978', 30, 1, '2020-09-10 12:30:18', '2020-09-10 12:30:18'),
(136, 'LABORATORIO SONY-LAB 621', 11, '1401 Calle 3 Nte. Ahuehuete', '18.9155988', '-98.43323219999999', 31, 1, '2020-09-10 12:30:45', '2020-09-10 12:30:45'),
(137, 'LABORATORIO MEDILAB 553', 11, '106 Av Independencia Centro', '18.9084972', '-98.43486999999999', 32, 1, '2020-09-10 12:31:31', '2020-09-10 12:31:31'),
(138, 'LABORATORIO JIMENEZ CONSTANTINO AMERICA LUZ 561', 11, '601 Calle 4 Nte. Centro', '18.9110703', '-98.4321049', 33, 1, '2020-09-10 12:31:59', '2020-09-10 12:31:59'),
(139, 'LABORATORIO SANCHEZ HERNANDEZ JOSE JUSTINIANO 856', 12, 'Av. Lerdo de Tejada Segunda Secc Zacatelco', '19.2184443', '-98.2484296', 1, 1, '2020-09-11 17:16:13', '2020-09-11 17:16:13'),
(140, 'CLINICA DE ESPECIALIDADES ZARAGOZA SA DE CV 837', 12, '32 Calle Zaragoza La Venta', '19.2144619', '-98.2361751', 2, 1, '2020-09-11 17:16:40', '2020-09-11 17:16:40'),
(141, 'LABORATORIO SANCHEZ DOMINGUEZ MARIA LAURA ENGRACIA 871', 12, '7 Av Niños Heroes Centro', '19.2171724', '-98.23867650000001', 3, 1, '2020-09-11 17:17:20', '2020-09-11 17:17:20'),
(142, 'LABORATORIO QUANTICA 842', 12, '18 Av. Lerdo de Tejada Centro', '19.2161791', '-98.24218630000001', 4, 1, '2020-09-12 10:16:50', '2020-09-12 10:16:50'),
(143, 'LABORATORIO DE ANALISIS CLINICOS SAN JUAN 839', 12, '50 Calle Niños Heroes Barrio del Alto', '19.3003934', '-98.2598701', 5, 1, '2020-09-12 10:17:42', '2020-09-12 10:17:42'),
(144, 'LABORATORIO BENIGNO ALFONSO REYES FRAGOZO 771', 12, '60-B Av Independencia Centro', '19.30741159999999', '-98.24069399999999', 6, 1, '2020-09-12 10:18:24', '2020-09-12 10:18:24'),
(145, 'ANALISIS CLINICOS L.B 843', 12, '18 Beatriz Paredes La Joya', '19.3026661', '-98.2377875', 7, 1, '2020-09-12 10:19:29', '2020-09-12 10:19:29'),
(146, 'LABORATORIO ESCANDON ROCHA MARIA DOLORES 898', 12, '8 Justo Sierra San Gabriel Cuauhtla', '19.31078849999999', '-98.23500159999999', 8, 1, '2020-09-12 10:23:58', '2020-09-12 10:23:58'),
(147, 'BIOMEDIC, IMAGEN Y LABORATORIO MEDICO, S.A. DE C.V. 864', 12, '5 Av. Morelos Centro', '19.312742', '-98.23734809999999', 9, 1, '2020-09-12 10:26:05', '2020-09-12 10:26:05'),
(148, 'LABORATORIO PINTOR GUTIERREZ PEDRO 807', 12, '13 Calle Tlahuicole Texcacoac', '19.3077752', '-98.1913873', 10, 1, '2020-09-12 10:26:30', '2020-09-12 10:26:30'),
(149, 'LABORATORIO CLINICO MARGARITA SA DE CV 866', 12, '31 Calle Zitlalpopocatl Centro', '19.4634739', '-98.42465779999999', 11, 1, '2020-09-12 10:28:58', '2020-09-12 10:28:58'),
(150, 'LABORATORIO LOPEZ SILVERIO MARIA DOLORES 14', 12, '50 Av. Tlahuicole Adolfo López Mateos', '19.3246304', '-98.241619', 12, 1, '2020-09-12 10:30:51', '2020-09-12 10:30:51'),
(151, 'HOSPITAL HUMANITAS DE TLAXCALA, S.A. DE C.V. 802', 12, '10 Revolución San Buenaventura Atempa', '19.329378', '-98.22286249999999', 13, 1, '2020-09-12 10:43:56', '2020-09-12 10:43:56'),
(152, 'LABORATORIO DE ANALISIS CLINICOS DIAGNOLAB 413', 12, '71 Calle Hidalgo Tercera Secc', '19.3224884', '-98.1644609', 14, 1, '2020-09-12 10:45:02', '2020-09-12 10:45:02'),
(153, 'LABORATORIO LUNA BARRIOS ESTEFANIA 943', 12, '13 Hidalgo Nte. San Onofre', '19.3158839', '-98.1929845', 15, 1, '2020-09-12 11:21:53', '2020-09-12 11:21:53'),
(154, 'LABORATORIO MARIA LUISA ANGULO RAMIREZ 801', 12, '71 Ignacio Picazo Sur San Sebastián', '19.3103971', '-98.1940936', 16, 1, '2020-09-12 11:31:08', '2020-09-12 11:31:08'),
(155, 'HEGUGARI S. DE  R.L. DE C.V.', 12, '67 Av Juárez Cuahuatzala', '19.3357841', '-98.2142159', 17, 1, '2020-09-12 13:01:20', '2020-09-12 13:01:20'),
(156, 'OMEGA IMAGEN 125', 12, '67 Av. Juárez Miraflores', '19.3083201', '-98.22054120000001', 18, 1, '2020-09-12 13:02:23', '2020-09-12 13:02:23'),
(157, 'LABORATORIO GONZALEZ GONZALEZ JUAN GABRIEL 869 ,CLIENTE POR LLAMADO', 12, '401 Calle Centenario Fátima', '19.4076559', '-98.14332350000001', 19, 1, '2020-09-12 13:05:03', '2020-09-12 13:05:03'),
(158, 'LABORATORIO DE ANALISIS CLINICOS VEGA ANDREW S. A. DE C. V. 10', 13, '118 Av 23 Pte El Carmen', '19.0367405', '-98.204562', 1, 1, '2020-09-12 13:18:30', '2020-09-12 13:18:30'),
(159, 'LABORATORIO VERGARA ISLAS SOFIA DOLORES LILIANA 12', 13, '3307 16 de Septiembre Chulavista', '19.0315799', '-98.20622019999999', 2, 1, '2020-09-12 13:19:06', '2020-09-12 13:19:06'),
(160, 'UNIDAD HOSPITALARIA GUADALUPE HIDALGO 18', 13, '118 Calle Hermanos Serdán Guadalupe Hidalgo 2da Secc', '18.974702', '-98.23953949999999', 3, 1, '2020-09-12 13:20:27', '2020-09-12 13:20:27'),
(161, 'LABORATORIO CRUZ CORTES JOSE GUILLERMO LUIS 28', 13, '924 Av. 105 Pte. Popular Castillotla', '18.997496', '-98.23948039999999', 4, 1, '2020-09-12 13:22:29', '2020-09-12 13:22:29'),
(162, 'LABORATORIO FLORES CASTILLO PATRICIA 33', 13, '720 Tepexi Vicente Guerrero', '19.0037703', '-98.2308868', 5, 1, '2020-09-12 13:25:37', '2020-09-12 13:25:37'),
(163, 'PAVON VARGAS MARIA DE LOS ANGELES', 13, '720 Tepexi Vicente Guerrero', '19.0037703', '-98.2308868', 6, 1, '2020-09-12 13:45:15', '2020-09-12 13:45:15'),
(164, 'PAVON VARGAS MARIA DE LOS ANGELES', 13, '720 Tepexi Vicente Guerrero', '19.0037703', '-98.2308868', 8, 1, '2020-09-12 13:45:15', '2020-09-12 13:45:15'),
(165, 'PAVON VARGAS MARIA DE LOS ANGELES', 13, '720 Tepexi Vicente Guerrero', '19.0037703', '-98.2308868', 7, 1, '2020-09-12 13:45:15', '2020-09-12 13:45:15'),
(166, 'PAVON VARGAS MARIA DE LOS ANGELES', 13, '720 Tepexi Vicente Guerrero', '19.0037703', '-98.2308868', 9, 1, '2020-09-12 13:45:16', '2020-09-12 13:45:16'),
(167, 'SEMIN SIGLO XXI SA DE CV 48', 13, '3102 Calle 13 Sur Los Volcanes', '19.0369416', '-98.2143567', 10, 1, '2020-09-12 13:46:14', '2020-09-12 13:46:14'),
(168, 'UDN HOSPITAL PUEBLA', 13, '4 Privada de las Ramblas Reserva Territorial Atlixcáyotl', '19.0304868', '-98.228433', 11, 1, '2020-09-12 13:46:35', '2020-09-12 13:46:35'),
(169, 'LABORATORIO HERRERA ROMANO LISBETH 189', 13, '3309 Calle 3 Sur Chulavista', '19.0324125', '-98.208034', 12, 1, '2020-09-12 13:47:07', '2020-09-12 13:47:07'),
(170, 'LABORATORIO PAEZ DIAGNOSTICOS 220', 13, '1119 Av 27 Pte Los Volcanes', '19.038723', '-98.2131402', 13, 1, '2020-09-12 13:47:32', '2020-09-12 13:47:32'),
(171, 'LABORATORIO BIOMETER DIAGNOSTIC 393', 13, 'Av. Manuel Espinosa Yglesias 31 Pte. Los Volcanes Puebla', '19.038542', '-98.21674279999999', 14, 1, '2020-09-12 13:49:28', '2020-09-12 13:49:28'),
(172, 'LABORATORIO MEDICO LINFOLAB, SA DE C V 448', 13, '3701 Calle 7 Sur Gabriel Pastor 1ra Secc', '19.032755976878953', '-98.21178250370482', 15, 1, '2020-09-12 13:50:10', '2020-09-12 13:50:10'),
(173, 'UDN IMAGEN DIAGNOSTICA', 14, '1110 Calle 18 Sur Barrio de Analco', '19.0341244', '-98.1894664', 1, 1, '2020-09-12 13:53:48', '2020-09-12 13:53:48'),
(174, 'UDN MILITARES', 14, '4312 Calle 24 Sur Alseseca', '19.01813243462303', '-98.19193752698362', 2, 1, '2020-09-12 13:56:27', '2020-09-12 13:56:27'),
(175, 'CLINICA DE ESPECIALIDADES NUESTRA SEÑORA DE LOS ANGELES A.C.329', 14, 'San Jerónimo Caleras Puebla Pue.', '19.0904031', '-98.2173091', 3, 1, '2020-09-12 13:58:01', '2020-09-12 13:58:01'),
(176, 'NUEVO  HOSPITAL DE ATENCION INTEGRAL,S.A.DE C.V. 363', 14, '50 Calle Aquiles Serdán Sur San Felipe Hueyotlipan', '19.0833644', '-98.21534840000001', 4, 1, '2020-09-12 13:58:22', '2020-09-12 13:58:22'),
(177, 'CENTRO MEDICO NACIONAL DE BIOLOGIA MOLECULAR 127', 14, '3919 Av 2 Pte Aquiles Serdán', '19.0611468', '-98.2259919', 5, 1, '2020-09-12 14:01:43', '2020-09-12 14:01:43'),
(178, 'UDN SANTA MARIA', 14, 'Calle 34 Pte. Santa María Puebla', '19.06070904351533', '-98.19656112090148', 6, 1, '2020-09-12 14:02:46', '2020-09-12 14:02:46'),
(179, 'LABORATORIO VITAL-LAB ( PUEBLA ) 296', 14, '609 Calle 32 Nte. Bis Zona Sin Asignación de Nombre de Col 41', '19.0365472', '-98.17352819999999', 7, 1, '2020-09-12 14:04:13', '2020-09-12 14:04:13'),
(180, 'LABORATORIO E IMAGEN RADIOLOGICA ALTHE, S.A. DE C.V. 19', 14, '3421 Educadores Unidad Satélite Magisterial', '19.0454935', '-98.15897299999999', 10, 1, '2020-09-12 14:05:10', '2020-09-12 14:05:10'),
(181, 'CLINICA MEDICA Y QUIRURGICA ABC COLOMBRES 256', 14, '43 Calle Juan N. Méndez Joaquín Colombres', '19.0535621', '-98.15711680000001', 9, 1, '2020-09-12 14:05:40', '2020-09-12 14:05:40'),
(182, 'LABORATORIO SANTIAGO DE LA ROSA MARIA 132', 14, '49 Av Gral Ignacio Zaragoza Héroes de Puebla', '19.0237142', '-98.1805206', 8, 1, '2020-09-12 14:06:21', '2020-09-12 14:06:21'),
(183, 'UNIDAD HOSPITALARIA AMALUCAN 155', 14, '1875 Blvd. Xonacatepec Proletaria Lázaro Cárdenas', '19.0528714', '-98.1480989', 11, 1, '2020-09-12 14:06:40', '2020-09-12 14:06:40'),
(184, 'LABORATORIO NADIA ESTEVEZ TORRES 90', 14, '6005 Calle 7 Nte. Guadalupe Victoria', '19.0696787', '-98.1900764', 12, 1, '2020-09-12 14:07:08', '2020-09-12 14:07:08'),
(185, 'LABORATORIO ISRAEL CESAR RODRIGUEZ MARTINEZ 365', 14, '2021 Calle 28 Ote. Xonaca', '19.0484659', '-98.1791604', 13, 1, '2020-09-12 14:07:39', '2020-09-12 14:07:39'),
(186, 'LABORATORIO ROMERO ROJAS SARA 848', 14, '35 Calle Francisco I. Madero Zona Sin Asignación de Nombre de Col 1', '19.0919911', '-98.1994598', 15, 1, '2020-09-12 14:10:22', '2020-09-12 14:10:22'),
(187, 'UDN SANTA MARIA', 14, '708 Calle 34 Pte. Santa María', '19.0606205', '-98.19654480000001', 14, 1, '2020-09-12 14:11:05', '2020-09-12 14:11:05'),
(188, 'prueba', 7, 'Calle Tlaxco La Paz Puebla', '19.052839', '-98.2213864', 8, 1, '2020-09-14 09:23:36', '2020-09-14 09:23:36'),
(189, 'prueba', 7, 'Calle 27 Sur Benito Juárez Puebla', '19.0417696', '-98.2253322', 9, 1, '2020-09-14 09:23:54', '2020-09-14 09:23:54'),
(190, 'prueba', 7, 'Av. 33 Pte. Los Volcanes Puebla', '19.0377478', '-98.21736290000001', 10, 1, '2020-09-14 09:24:19', '2020-09-14 09:24:19'),
(191, 'prueba', 7, '1901 Av. 35 Pte. Reserva Territorial Atlixcáyotl', '19.0379883', '-98.2197576', 11, 1, '2020-09-14 09:24:50', '2020-09-14 09:24:50'),
(192, 'prueba', 7, 'Calle 23 Sur Benito Juárez Puebla', '19.0410154', '-98.22173819999999', 12, 1, '2020-09-14 09:25:15', '2020-09-14 09:25:15'),
(193, 'cholula', 7, 'Cholula Pue. MX', '19.0715049', '-98.31567419999999', 13, 1, '2020-09-14 09:25:33', '2020-09-14 09:25:33'),
(194, 'prueba', 7, '40 Cocotepec La Paz', '19.056582', '-98.2240367', 14, 1, '2020-09-14 09:26:00', '2020-09-14 09:26:00'),
(195, 'prueba', 7, '1516 Calle 30 Sur Dos de Abril', '19.0290767', '-98.1834877', 15, 2, '2020-09-14 09:26:19', '2020-09-14 09:26:19'),
(196, 'prueba', 7, 'Av. 13 Ote. El Carmen Puebla', '19.0383519', '-98.1983351', 16, 2, '2020-09-14 09:26:40', '2020-09-14 09:26:40'),
(197, 'calle 8 sur', 7, 'Calle 8 Sur Antigua Francisco Villa Puebla', '19.0323173', '-98.1984131', 17, 2, '2020-09-14 09:27:00', '2020-09-14 09:27:00'),
(198, 'prueba', 7, '1009 Avenida 21 Oriente Bella Vista', '19.0327845', '-98.1955017', 17, 2, '2020-09-14 09:27:23', '2020-09-14 09:27:23'),
(199, 'prueba', 7, 'Local A 1607 Av 17 Ote', '19.0327172', '-98.19160529999999', 19, 2, '2020-09-14 09:27:49', '2020-09-14 09:27:49'),
(200, 'colegio', 7, '1814 Calle 19 Ote. Azcárate', '19.0314598', '-98.1905044', 20, 2, '2020-09-14 09:28:08', '2020-09-14 09:28:08'),
(201, 'prueba', 7, '1901 Calle 24 Sur Azcarate', '19.0297942', '-98.18824459999999', 21, 2, '2020-09-14 09:28:46', '2020-09-14 09:28:46'),
(202, 'prueba', 7, '2 de Marzo La Resurrección Noche Buena', '19.0894414', '-98.130285', 22, 2, '2020-09-14 09:50:40', '2020-09-14 09:50:40'),
(203, 'jajaja', 7, 'Miguel Hidalgo Puebla Pue.', '19.0691852', '-98.1379523', 23, 2, '2020-09-14 09:51:29', '2020-09-14 09:51:29'),
(204, 'prueba 2', 7, 'Miguel Hidalgo Puebla Pue.', '19.06639665217837', '-98.13807031719666', 23, 2, '2020-09-14 09:53:41', '2020-09-14 09:53:41'),
(205, '2 de marzo', 7, 'Apizaco Tlax. MX', '19.415881', '-98.1393179', 25, 2, '2020-09-14 09:56:20', '2020-09-14 09:56:20'),
(206, 'prueba', 7, 'Tetla Tlax. MX', '19.44145499999999', '-98.09563279999999', 26, 2, '2020-09-14 09:56:54', '2020-09-14 09:56:54'),
(207, 'VALLARINO 65', 14, '3118 Calle 24 B. Pte. Valle Dorado', '19.0671371', '-98.2129569', 16, 1, '2020-09-14 10:04:05', '2020-09-14 10:04:05'),
(208, 'p', 7, '1712 Av 9 Pte Barrio de Santiago', '19.0480002', '-98.21316259999999', 15, 2, '2020-09-14 10:07:07', '2020-09-14 10:07:07'),
(209, 'Q', 7, '2 Acatzingo La Paz', '19.051799', '-98.2249784', 15, 1, '2020-09-14 10:09:39', '2020-09-14 10:09:39'),
(210, 'r', 7, '2 de Marzo La Resurrección Noche Buena', '19.0894414', '-98.130285', 16, 1, '2020-09-14 10:22:24', '2020-09-14 10:22:24');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `recorrido`
--
ALTER TABLE `recorrido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recorridoDia`
--
ALTER TABLE `recorridoDia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trayecto`
--
ALTER TABLE `trayecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `waypoints`
--
ALTER TABLE `waypoints`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `recorrido`
--
ALTER TABLE `recorrido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `recorridoDia`
--
ALTER TABLE `recorridoDia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `rutas`
--
ALTER TABLE `rutas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `trayecto`
--
ALTER TABLE `trayecto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `waypoints`
--
ALTER TABLE `waypoints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
