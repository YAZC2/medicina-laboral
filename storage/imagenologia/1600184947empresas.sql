-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 10-09-2020 a las 11:01:14
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `humanlys_expedienteClinicoEmpresas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `pagina` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Responsable de la empresa',
  `giro_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `nombre`, `direccion`, `logo`, `telefono`, `pagina`, `user_id`, `giro_id`, `created_at`, `updated_at`) VALUES
(1, 'Humanly Software', 'Colonia de los programadores', '1589505624humanly-logo.png', '2291461195', 'http://humanly-sw.com/', 2, 7, '2020-05-15 01:20:24', '2020-05-15 01:20:24'),
(2, 'Ontex México', 'Av. San Pablo Xochimehuacán 7213, La Loma, 72230 Puebla, Pue.', '1595790289descarga.png', '222 223 61', 'http://www.ontexmexico.com/', 3, 2, '2020-05-15 15:20:24', '2020-07-26 19:04:49'),
(5, 'thyssenkrupp', 'Av. 21 Poniente No. 3711. Colonia Belisario Dominguez', '1595790306SFer1DYL_400x400.jpg', '0000000000', 'ninguno', 7, 2, '2020-06-16 17:55:11', '2020-07-26 19:05:06'),
(6, 'Asesores Diagnostico Clinico', 'Av. 21 Poniente No. 3711. Colonia Belisario Dominguez', '1593701858descarga.jpg', '22 24 54 0', 'https://www.laboratorioasesores.com/', 8, 6, '2020-07-02 14:57:38', '2020-07-02 14:57:38'),
(7, 'Thyssenkrupp Xoxtla', 'Camino, A Santa Agueda 1, El Carmen, 72620 San Miguel Xoxtla, Pue.', '1595873164SFer1DYL_400x400.jpg', '222 223 70', 'https://www.thyssenkrupp-automotive-technology.com/en', 12, 2, '2020-07-25 02:10:45', '2020-07-27 18:06:04'),
(8, 'Comerdis', 'Pendiente', '1598648660dsf.png', '0000000000', 'Pendiente', 13, 1, '2020-08-28 21:04:20', '2020-09-07 14:23:44');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_ibfk_1` (`user_id`),
  ADD KEY `empresa_giro` (`giro_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
